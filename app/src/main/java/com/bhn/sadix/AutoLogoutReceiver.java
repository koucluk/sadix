package com.bhn.sadix;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bhn.sadix.util.Util;

public class AutoLogoutReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Util.Toast(context, "Auto Logout");
        Util.putPreference(context, "isLogin", false);
        context.stopService(new Intent(context, SadixService.class));
    }

}
