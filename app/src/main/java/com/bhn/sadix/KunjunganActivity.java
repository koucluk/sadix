package com.bhn.sadix;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.Data.DiscountPromoModel;
import com.bhn.sadix.Data.DiscountResultModel;
import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.adapter.AssetAdapter;
import com.bhn.sadix.adapter.AssetMutasiAdapter;
import com.bhn.sadix.adapter.AssetServiceAdapter;
import com.bhn.sadix.adapter.CapasityAdapter;
import com.bhn.sadix.adapter.CollectionPayAdapter;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.CrcOrderAdapter;
import com.bhn.sadix.adapter.CrcReportAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.adapter.MerchandiserAdapter;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.PriceMonitoringAdapter;
import com.bhn.sadix.adapter.PrinsipleAdapter;
import com.bhn.sadix.adapter.ReturDetailAdapter;
import com.bhn.sadix.adapter.StokCompetitorAdapter;
import com.bhn.sadix.adapter.StokCustomerAdapter;
import com.bhn.sadix.adapter.StokGroupAdapter;
import com.bhn.sadix.adapter.StokOnCarAdapter;
import com.bhn.sadix.adapter.TargetAdapter;
import com.bhn.sadix.adapter.TargetComAdapter;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCollector;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlDiscountPromo;
import com.bhn.sadix.database.CtrlDiscountResult;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlGroupSKUCom;
import com.bhn.sadix.database.CtrlRetur;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.database.CtrlSalesProgram;
import com.bhn.sadix.database.CtrlStockGroup;
import com.bhn.sadix.database.CtrlStokCompetitor;
import com.bhn.sadix.database.CtrlStokOutlet;
import com.bhn.sadix.database.CtrlSurvey;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.kunjungan.AssetFragment;
import com.bhn.sadix.fragment.kunjungan.AssetMutasiFragment;
import com.bhn.sadix.fragment.kunjungan.AssetServiceFragment;
import com.bhn.sadix.fragment.kunjungan.BrandingFragment;
import com.bhn.sadix.fragment.kunjungan.CRCFragment;
import com.bhn.sadix.fragment.kunjungan.CapasityFragment;
import com.bhn.sadix.fragment.kunjungan.CollectionFragment;
import com.bhn.sadix.fragment.kunjungan.CollectionTirtaFinanceFragment;
import com.bhn.sadix.fragment.kunjungan.CrcReportFragment;
import com.bhn.sadix.fragment.kunjungan.HistoryPenjualanFragment;
import com.bhn.sadix.fragment.kunjungan.InformasiFragment;
import com.bhn.sadix.fragment.kunjungan.MerchandiserFragment;
import com.bhn.sadix.fragment.kunjungan.NewEditCustomerFragment;
import com.bhn.sadix.fragment.kunjungan.NoteFragment;
import com.bhn.sadix.fragment.kunjungan.PriceMonitoringFragment;
import com.bhn.sadix.fragment.kunjungan.ReturFragment;
import com.bhn.sadix.fragment.kunjungan.SalesTargetFragment;
import com.bhn.sadix.fragment.kunjungan.StokCompetitorFragment;
import com.bhn.sadix.fragment.kunjungan.StokGroupFragment;
import com.bhn.sadix.fragment.kunjungan.StokOnCarFragment;
import com.bhn.sadix.fragment.kunjungan.StokOutletFragment;
import com.bhn.sadix.fragment.kunjungan.SurveyFragment;
import com.bhn.sadix.fragment.kunjungan.TakingOrder2Fragment;
import com.bhn.sadix.fragment.kunjungan.TakingOrder3Fragment;
import com.bhn.sadix.fragment.kunjungan.TakingOrderFragment;
import com.bhn.sadix.fragment.kunjungan.TargetComFragment;
import com.bhn.sadix.fragment.kunjungan.TargetFragment;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.AssetModel;
import com.bhn.sadix.model.AssetMutasiModel;
import com.bhn.sadix.model.AssetServiceModel;
import com.bhn.sadix.model.CRCReportModel;
import com.bhn.sadix.model.CapasityModel;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CrcOrderStokModel;
import com.bhn.sadix.model.CustBrandingModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.DiskonPromoModel;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.MerchandiserModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.ReturDetailModel;
import com.bhn.sadix.model.ReturModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.model.StockCompetitorModel;
import com.bhn.sadix.model.StockGroupModel;
import com.bhn.sadix.model.StockOutletModel;
import com.bhn.sadix.model.StokCompetitorModel;
import com.bhn.sadix.model.StokCustomerModel;
import com.bhn.sadix.model.StokOnCarModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.model.TargetComModel;
import com.bhn.sadix.model.TargetModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.BluePrint;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.ItemPhotoCustomerBrandingWidget;
import com.bhn.sadix.widget.ItemPhotoCustomerBrandingWidgetListener;
import com.bhn.sadix.widget.ItemPhotoCustomerListener;
import com.bhn.sadix.widget.ItemPhotoCustomerWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

//import com.google.firebase.crash.FirebaseCrash;

//import com.bhn.sadix.model.DiscountPromoModel;
//import com.bhn.sadix.model.DiscountResultModel;

//import com.bhn.sadix.model.PriceMonitoringModel;

//import com.bhn.sadix.model.CommonModel;

public class KunjunganActivity extends AppCompatActivity implements OnItemClickListener, ItemPhotoCustomerListener, ItemPhotoCustomerBrandingWidgetListener, KunjunganListener {
    private Toolbar toolbar;
    private Menu menu;
    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    //    public CommonModel commonModel = new CommonModel();
    public boolean isHome = true;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private MenuUtamaAdapter Menuadapter;
    private CustomerModel customerModel;
    private CustomerTypeModel customerTypeModel;
    private ComboBoxModel reasonNoScanBarcode;
    private InformasiFragment informasiFragment;
    private NoteFragment noteFragment;
    private CollectionFragment collectionFragment;
    //    private EditCustomerFragment editCustomerFragment;
    private NewEditCustomerFragment editCustomerFragment;
    private BrandingFragment brandingFragment;
    private StokOutletFragment stokOutletFragment;
    private StokCompetitorFragment stokCompetitorFragment;
    private StokOnCarFragment stokOnCarFragment;
    private TakingOrderFragment takingOrderFragment;
    private ReturFragment returFragment;
    private HistoryPenjualanFragment historyPenjualanFragment;
    private SurveyFragment surveyFragment;
    private AssetFragment assetFragment;
    private AssetServiceFragment assetServiceFragment;
    private AssetMutasiFragment assetMutasiFragment;
    private StokGroupFragment stokGroupFragment;
    private CapasityFragment capasityFragment;
    private TargetFragment targetFragment;
    private PriceMonitoringFragment priceMonitoringFragment;
    private TargetComFragment targetComFragment;
    private SalesTargetFragment salesTargetFragment;
    private MerchandiserFragment merchandiserFragment;
    private CollectionTirtaFinanceFragment collectionTirtaFinanceFragment;
    private CRCFragment crcFragment;
    private CrcReportFragment crcReportFragment;
    private TakingOrder2Fragment takingOrder2Fragment;
    private TakingOrder3Fragment takingOrder3Fragment;

    private CollectionPayAdapter collectionPayAdapter;
    private OrderDetailAdapter takingOrderAdapter;
    private AssetAdapter assetAdapter;

    private OrderDetailAdapter takingOrderDiscount;
    private OrderDetailAdapter getTakingOrderFocus;
    private OrderDetailAdapter takingOrderPromo;

    private OrderDetailAdapter orderPrincipleAdapter;
    private StokCustomerAdapter stokOutletAdapter;
    private StokCompetitorAdapter stokCompetitorAdapter;
    private StokOnCarAdapter stokOnCarAdapter;
    private ReturDetailAdapter returDetailAdapter;
    private StokGroupAdapter stokGroupAdapter;
    private CapasityAdapter capasityAdapter;
    private TargetAdapter targetAdapter;
    private PriceMonitoringAdapter priceMonitoringProdukAdapter;
    private PriceMonitoringAdapter priceMonitoringCompetitorAdapter;
    private PriceMonitoringAdapter priceListMonitoringAdapter;
    private TargetComAdapter targetComAdapter;
    private CrcOrderAdapter orderCrcAdapter;
    private CrcReportAdapter crcReportAdapter;
    //    private PrinsipleAdapter prinsipleAdapter;
    private AssetMutasiAdapter assetMutasiAdapter;
    private AssetServiceAdapter assetServiceAdapter;

    private CtrlCustomer ctrlCustomer;
    private CtrlCustomerType ctrlCustomerType;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlSKU ctrlSKU;
    private CtrlStokOutlet ctrlStokOutlet;
    private CtrlStokCompetitor ctrlStokCompetitor;
    private CtrlTakingOrder ctrlTakingOrder;
    //    private CtrlSalesProgram ctrlSalesProgram;
    private CtrlRetur ctrlRetur;
    //    private CtrlCustomerType ctrlCustomerType;
    private CtrlCommon ctrlCommon;
    private CtrlCollector ctrlCollector;
    private CtrlGroupSKUCom ctrlGroupSKUCom;
    private CtrlSKUCom ctrlSKUCom;
    private CtrlConfig ctrlConfig;
    private CtrlStockGroup ctrlStockGroup;
    private CtrlDiscountPromo ctrlDiscountPromo;
    private CtrlDiscountResult ctrlDiscountResult;
    private CtrlSalesProgram ctrlSalesProgram;
//    private CtrlState ctrlState;

    //    private DbDiskonHelper dbDiskonHelper;
    private DatabaseHelper databaseHelper;
    private DbMasterHelper dbmaster;
    private ProgressDialog progressDialog;
    private Location location;
    private Messenger mService;
    private Bundle savedInstance;
    private SharedPreferences sharedPreferences;
    final Messenger mMessenger = new Messenger(new LocationHandler());
    //    private PrinsipleModel prinsipleModel;
    private PrincipleModel principleModel;
    private Realm realmUI;
    public CommonModel commonModel;

    //FLAGS
    private boolean isInPrinsiple = false;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
            try {
                Message msg = Message.obtain(null, SadixService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    private String note;

    private ConnectionServer server;
    private ItemPhotoCustomerWidget widget;
    //    private boolean isEdit = false;
    private ItemPhotoCustomerBrandingWidget brandingWidget;
    private BluePrint bluePrint;
    private CtrlAppModul ctrlAppModul;
    private boolean isModul71 = false;
    private boolean isModul72 = false;

    private JSONArray visitNote = null;
    private boolean isBound = false;
    private int jenis;
    //    private CollectorModel collectorModelNotPayment;
    private CollectionModel collectionModelNotPayment;
    private DbAsetHelper dbAsetHelper;
    private MerchandiserAdapter merchandiserAdapter;
    private CtrlCustBranding ctrlCustBranding;
    private CtrlSurvey ctrlSurvey;
    private PrinsipleAdapter principleAdapter = null;

    @Override
    public com.bhn.sadix.model.CommonModel getCommonModel() {
        return null;
    }

    @Override
    public CustomerModel getCustomerModel() {
        return customerModel;
    }

    @Override
    public CustomerTypeModel getCustomerType() {
        return customerTypeModel;
    }

    @Override
    public OrderDetailAdapter getTakingAdapter() {
        return takingOrderAdapter;
    }

    @Override
    public OrderDetailAdapter getTakingPrincipleAdapter() {
        return orderPrincipleAdapter;
    }

    @Override
    public ReturDetailAdapter getReturAdapter() {
        return returDetailAdapter;
    }

    @Override
    public StokCustomerAdapter getStokAdapter() {
        return stokOutletAdapter;
    }

    @Override
    public StokCompetitorAdapter getStokCompetitor() {
        return stokCompetitorAdapter;
    }

    @Override
    public StokGroupAdapter getStokGroupAdapter() {
        return stokGroupAdapter;
    }

    @Override
    public CollectionPayAdapter getCollectionAdapter() {
        return collectionPayAdapter;
    }

    @Override
    public PriceMonitoringAdapter getPriceMonitoringAdapter() {
        return priceMonitoringProdukAdapter;
    }

    @Override
    public PriceMonitoringAdapter getPriceCompetitorAdapter() {
        return priceMonitoringCompetitorAdapter;
    }

    @Override
    public PrincipleModel getPrinsipleModel() {
        return principleModel;
    }

    @Override
    public StokOnCarAdapter getStokOnCarAdapter() {
        return stokOnCarAdapter;
    }

    @Override
    public CrcReportAdapter getCrcReportAdapter() {
        return crcReportAdapter;
    }

    @Override
    public String getNote() {
        return commonModel.Description;
    }

    @Override
    public void updateMenuFunction() {
        MenuItem itemBack = menu.findItem(1);
        MenuItem itemSave = menu.findItem(2);

        itemBack.setTitle("Back");
        itemSave.setTitle("Save");

        itemBack.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
//                isInPrinsiple = false;
                onBackPressed();
                return true;
            }
        });

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        itemSave.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
//                isInPrinsiple = false;
                updatePrinsipleAdapter(takingOrderFragment.getMODEL_ORDER());
                onBackPressed();
                return true;
            }
        });
    }

    private void updatePrinsipleAdapter(int model_order) {
//        ArrayList<TakingOrderModel> list = new ArrayList<TakingOrderModel>();
        for (int i = 0; i < orderPrincipleAdapter.getGroupCount(); i++) {
            if (orderPrincipleAdapter.listOrderByGroup(orderPrincipleAdapter.getGroup(i)) != null) {
                for (int j = 0; j < orderPrincipleAdapter.getChildrenCount(i); j++) {
                    final OrderDetailModel orderDetailModel = orderPrincipleAdapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {

                        final com.bhn.sadix.Data.TakingOrderModel model = new com.bhn.sadix.Data.TakingOrderModel();

                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.CustId = customerModel.CustId;
                                model.SKUId = orderDetailModel.skuModel.SKUId;
                                model.tanggal = "";
                                model.SalesProgramId = orderDetailModel.salesProgramModel.SalesProgramId;
                                model.QTY_B = orderDetailModel.QTY_B;
                                model.QTY_K = orderDetailModel.QTY_K;
                                model.DISCOUNT1 = orderDetailModel.DISCOUNT1;
                                model.DISCOUNT2 = orderDetailModel.DISCOUNT2;
                                model.DISCOUNT3 = orderDetailModel.DISCOUNT3;
                                model.TOTAL_PRICE = orderDetailModel.TOTAL_PRICE;
                                model.PAYMENT_TERM = orderDetailModel.PAYMENT_TERM;
                                model.note = orderDetailModel.note;
                                model.PRICE_B = orderDetailModel.PRICE_B;
                                model.PRICE_K = orderDetailModel.PRICE_K;
                                model.LAMA_KREDIT = orderDetailModel.LAMA_CREDIT;
                                model.DiscountType = orderDetailModel.DiscountType;
                                model.CommonID = "";
                                model.RandomID = orderDetailModel.RandomID;
                                model.PrinsipleID = principleModel.RandomID;

                                realm.copyToRealmOrUpdate(model);

                                PrincipleModel pM = realm.where(PrincipleModel.class)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .equalTo("PrinsipleId", principleModel.PrinsipleId)
                                        .findFirst();

                                pM.list.add(model);
                        /*list.add(new TakingOrderModel(
                                customerModel.CustId,
                                orderDetailModel.skuModel.SKUId,
                                "",
                                orderDetailModel.salesProgramModel.SalesProgramId,
                                orderDetailModel.QTY_B,
                                orderDetailModel.QTY_K,
                                orderDetailModel.DISCOUNT1,
                                orderDetailModel.DISCOUNT2,
                                orderDetailModel.DISCOUNT3,
                                orderDetailModel.TOTAL_PRICE,
                                orderDetailModel.PAYMENT_TERM,
                                orderDetailModel.note,
                                orderDetailModel.PRICE_B,
                                orderDetailModel.PRICE_K,
                                orderDetailModel.LAMA_CREDIT,
                                orderDetailModel.DiscountType,
                                "",
                                orderDetailModel.RandomID,
//							prinsipleModel.PrinsipleId,
                                principleModel.RandomID
                        ));*/
                            }
                        });


                    }
                }
            }
        }

        try {
//            ArrayList<DiscountPromoModel> listPromo = new ArrayList<DiscountPromoModel>();
            if (takingOrderFragment.getDiskonAdapter() != null) {
                for (int i = 0; i < takingOrderFragment.getDiskonAdapter().getCount(); i++) {
                    final JSONObject promo = takingOrderFragment.getDiskonAdapter().getItem(i);
                    if (promo.has("isChecked") && promo.getBoolean("isChecked")) {
                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                try {
                                    DiscountPromoModel discount = new DiscountPromoModel(
                                            promo.getInt("DISCOUNT_PROMO_ID"),
                                            Integer.parseInt(customerModel.CustId),
                                            commonModel.CommonID);

                                    realm.copyToRealmOrUpdate(discount);

                                    PrincipleModel pM = realm.where(PrincipleModel.class)
                                            .equalTo("CommonID", commonModel.CommonID)
                                            .equalTo("PrinsipleId", principleModel.PrinsipleId)
                                            .findFirst();

                                    pM.listPromo.add(discount);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
/*
                        *//*listPromo.add(*//*
                        ));*/

                    }
                }
            }
//            ArrayList<DiscountResultModel> listPromoResult = new ArrayList<DiscountResultModel>();
            if (takingOrderFragment.getDiskonPromoAdapter() != null) {
                for (int i = 0; i < takingOrderFragment.getDiskonPromoAdapter().getGroupCount(); i++) {
                    for (int j = 0; j < takingOrderFragment.getDiskonPromoAdapter().getChildrenCount(i); j++) {
                        final DiskonPromoModel model = takingOrderFragment.getDiskonPromoAdapter().getChild(i, j);

                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                DiscountResultModel discountResult = new DiscountResultModel(
                                        Integer.parseInt(customerModel.CustId),
                                        model.DiscountResultID,
                                        model.QTY,
                                        commonModel.CommonID,
                                        model.RandomID
                                );

                                realm.copyToRealmOrUpdate(discountResult);

                                PrincipleModel pM = realm.where(PrincipleModel.class)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .equalTo("PrinsipleId", principleModel.PrinsipleId)
                                        .findFirst();

                                pM.listPromoResult.add(discountResult);
                            }
                        });

//                        listPromoResult.add();
                    }
                }
            }

            /*prinsipleModel.listPromo = listPromo;
            prinsipleModel.listPromoResult = listPromoResult;*/

            realmUI.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    PrincipleModel pM = realm.where(PrincipleModel.class)
                            .equalTo("CommonID", commonModel.CommonID)
                            .equalTo("PrinsipleId", principleModel.PrinsipleId)
                            .findFirst();

                    pM.total_qty_b = commonModel.QTY_B;
                    pM.total_qty_k = commonModel.QTY_K;
                    pM.price = commonModel.PRICE;
                    pM.discount = commonModel.DISCOUNT;
                    pM.total_price = commonModel.S_AMOUNT;
                }
            });

//            prinsipleAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void restoreMenuFunction() {
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        });
        onCreateOptionsMenu(menu);
    }

    @Override
    public PrinsipleAdapter getPrinsipleAdapter() {
        CommonModel cmModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();

        RealmResults<PrincipleModel> models = realmUI.where(PrincipleModel.class)
                .equalTo("CommonID", cmModel.CommonID)
                .findAll();
        Log.d("Model Size", models.size() + "");
        PrinsipleAdapter adapter = new PrinsipleAdapter(this, null);
        for (PrincipleModel model : models) {
            adapter.add(model);
        }
        Log.d("Adapter Size", adapter.getCount() + "");
        if (adapter.getCount() == 0) {
            return null;
        } else {
            return adapter;
        }
//        return adapter;
//        return null;
    }

    @Override
    public void handleTO3(PrincipleModel model, int position) {
        isInPrinsiple = true;

        updateMenuFunction();
//        principleAdapter.getItem()
        principleModel = realmUI.copyFromRealm(model);

        new LoadDataPrinciple().execute(position);
    }

    @Override
    public void setPrincipleAdapter(PrinsipleAdapter adapter) {
        principleAdapter = adapter;
    }

    @Override
    public CrcOrderAdapter getCrcOrderAdapter() {
        return orderCrcAdapter;
    }

    @Override
    public AssetAdapter getAssetAdapter() {
        return assetAdapter;
    }

    @Override
    public AssetMutasiAdapter getMutasiAdapter() {
        return assetMutasiAdapter;
    }

    @Override
    public AssetServiceAdapter getAssetServiceAdapter() {
        return assetServiceAdapter;
    }

    @Override
    public void dismissAndUpdateDialog(Dialog pendingDialog) {
        pendingDialog.dismiss();
        checkPendingItem();
    }

    class LocationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SadixService.MSG_SEND_LOC:
                    Bundle bundle = msg.getData();

                    location = new Location("");
                    location.setLatitude(bundle.getDouble("lat"));
                    location.setLongitude(bundle.getDouble("long"));
                    location.setTime(bundle.getLong("time"));

                    Realm realm = Realm.getDefaultInstance();

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            CommonModel cmModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
                            try {
                                cmModel.GeoLat = String.format("%1$s", location.getLatitude());
                                cmModel.GeoLong = String.format("%1$s", location.getLongitude());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    realm.close();
                    break;
            }
        }
    }

    public void loadAsset() {
        new LoadAsset().execute();
    }

    class LoadAsset extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            populateAsset();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            assetFragment.loadAdapter();
        }
    }

    public void loadTakingOrder(int type) {
        new LoadTakingOrder().execute(type);
    }

    class LoadTakingOrder extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... integers) {
            int type = integers[0];
            if (takingOrderAdapter == null || takingOrderAdapter.getGroupCount() == 0) {
                populateTakingOrder();
            }

//            populateTakingOrder1(takingOrderAdapter);
            return type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s == 1)
                takingOrderFragment.loadAdapter(true);
            else
                takingOrder2Fragment.loadAdapter();
        }
    }

    public void loadCollector() {
        populateCollector();

        collectionFragment.loadAdapter();
    }

    public void loadPriceMonitoring() {
        new LoadPriceMonitoring().execute();
    }

    class LoadPriceMonitoring extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (priceMonitoringProdukAdapter == null || priceMonitoringProdukAdapter.getGroupCount() == 0) {
                populateTakingOrder();
            }

            if (priceMonitoringCompetitorAdapter == null || priceMonitoringCompetitorAdapter.getGroupCount() == 0) {
                populateStokCompetitor();
            }

//            populatePriceMonitoring();
//            populatePriceMonitoringComp();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            priceMonitoringFragment.loadAdapter();
            super.onPostExecute(s);
        }
    }

    public void loadRetur() {
        new LoadRetur().execute();
    }

    class LoadRetur extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (returDetailAdapter == null || returDetailAdapter.getGroupCount() == 0) {
                populateTakingOrder();
            }
//            populateReturn();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            returFragment.loadAdapter();
        }
    }

    public void loadStokComp() {
        new LoadStokCompetitor().execute();
    }

    class LoadStokCompetitor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (stokCompetitorAdapter == null || stokCompetitorAdapter.getGroupCount() == 0) {
                populateStokCompetitor();
            }
//            populateStockComp();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            stokCompetitorFragment.loadAdapter();
        }
    }

    public void loadStokGroup() {
        new LoadStokGroup().execute();
    }

    class LoadStokGroup extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (stokGroupAdapter == null || stokGroupAdapter.getCount() == 0) {
                populateTakingOrder();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            stokGroupFragment.loadAdapter();
        }
    }

    public void loadStokOnCar() {
        new LoadStokOnCar().execute();
    }

    class LoadStokOnCar extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (stokOnCarAdapter == null) {
                populateTakingOrder();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            stokOnCarFragment.loadAdapter();
        }
    }

    public void loadStokOutlet() {
        new LoadStokOutlet().execute();
    }

    class LoadStokOutlet extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (stokOutletAdapter == null || stokOutletAdapter.getGroupCount() == 0) {
                populateTakingOrder();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            stokOutletFragment.loadAdapter();
        }
    }

    public void loadSalesTarget() {
        new LoadSalesTarget().execute();
    }

    class LoadSalesTarget extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (targetAdapter == null || targetAdapter.getGroupCount() == 0) {
                populateTakingOrder();
            }

            if (targetComAdapter == null || targetComAdapter.getGroupCount() == 0) {
                populateStokCompetitor();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
//            salesTargetFragment.loadAdapter();
        }
    }

    public void loadCrc() {
        new LoadCrc().execute();
    }

    class LoadCrc extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (orderCrcAdapter == null) {
                populateTakingOrder();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            crcFragment.loadAdapter();
        }
    }

    public void loadCrcReport() {
        new LoadCrcReport().execute();
    }

    class LoadCrcReport extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (crcReportAdapter == null) {
                populateTakingOrder();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            crcReportFragment.loadAdapter();
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kunjungan);

        savedInstance = savedInstanceState;

        realmUI = Realm.getDefaultInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbAsetHelper = new DbAsetHelper(this);

        sharedPreferences = getSharedPreferences("SADIX", MODE_PRIVATE);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        jenis = getIntent().getIntExtra("jenis", 0);

        server = new ConnectionServer(this);

        CheckIfServiceIsRunning();

        databaseHelper = new DatabaseHelper(this);
        android.util.Log.e("SalesId", "" + UserModel.getInstance(this).SalesId);
        reasonNoScanBarcode = (ComboBoxModel) getIntent().getSerializableExtra("ComboBoxModel");
        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        Log.d("Customer ID", customerModel.CustId);
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        if (commonModel == null) {
            realmUI.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Log.d("Creating Common Model", "true");
                    String CommonID = UUID.randomUUID().toString();
//                    commonModel.CommonID = UUID.randomUUID().toString();
                    commonModel = realmUI.createObject(com.bhn.sadix.Data.CommonModel.class, CommonID);

                    commonModel.SC = UserModel.getInstance(getApplicationContext()).CompanyID;
                    commonModel.D = "D" + String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
                    commonModel.BeginVisitTime = Util.dateFormat.format(new Date());
                    commonModel.SalesId = UserModel.getInstance(getApplicationContext()).SalesId;
                    Log.wtf("Customer ID", customerModel.CustomerID);
                    Log.wtf("Cust ID", customerModel.CustId);

                    commonModel.CustomerId = customerModel.CustId.equals("0") ? customerModel.RandomID : customerModel.CustId;
//                    commonModel.GeoLat = location.getLatitude();
                    if (reasonNoScanBarcode != null) {
                        commonModel.Visit = 2;
                        commonModel.RVisitID = Integer.parseInt(reasonNoScanBarcode.value);
                    } else {
                        commonModel.Visit = 1;
                        commonModel.RVisitID = 0;
                    }
                    commonModel.StatusID = 0;
                    commonModel.Done = 0;
                    commonModel.CustomerRID = customerModel.RandomID;

                    commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
                }
            });

//            commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        }

        /*} else {
            commonModel = cmModel;
        }*/
//        if (commonModel == null) {
        noteFragment = NoteFragment.newInstance(commonModel.Description);
//        }

//            sharedPreferences.edit().putString("custId", commonModel.CommonID).apply();

//        editCustomerFragment  = new EditCustomerFragment(this) {
//			@Override
//			public void addPhoto(ItemPhotoCustomerWidget widget) {
//				widget.setCustomerModel(customerModel);
//				widget.addItemPhotoCustomerListener(KunjunganActivity.this);
//			}
//			@Override
//			public void selesai() {
//		        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//	            ft.replace(R.id.content_frame, informasiFragment);
//	            ft.commit();
//	            mDrawerList.setItemChecked(0, true);
//	            setTitle("Informasi");
//			}
//        };

        informasiFragment = InformasiFragment.newInstance();

        editCustomerFragment = new NewEditCustomerFragment();

        returFragment = ReturFragment.newInstance();
        takingOrderFragment = TakingOrderFragment.newInstance();
        stokOutletFragment = StokOutletFragment.newInstance();
        stokCompetitorFragment = new StokCompetitorFragment();
        takingOrder2Fragment = new TakingOrder2Fragment();//new TakingOrder2Fragment(this, customerTypeModel, customerModel);
        collectionFragment = CollectionFragment.newInstance();
        historyPenjualanFragment = HistoryPenjualanFragment.newInstance();//new HistoryPenjualanFragment(this);
        brandingFragment = BrandingFragment.newInstance();
        stokOnCarFragment = StokOnCarFragment.newInstance();//new StokOnCarFragment(this, customerModel);
        surveyFragment = SurveyFragment.newInstance();//new SurveyFragment(this);
        assetFragment = AssetFragment.newInstance();//new AssetFragment(this, commonModel);
        assetServiceFragment = AssetServiceFragment.newInstance();//new AssetServiceFragment(this, commonModel);
        assetMutasiFragment = AssetMutasiFragment.newInstance();//new AssetMutasiFragment(this, commonModel);
        stokGroupFragment = StokGroupFragment.newInstance();//new StokGroupFragment(this);

        capasityFragment = new CapasityFragment();
        targetFragment = new TargetFragment();
        targetFragment.setCustomerModel(customerModel);
        priceMonitoringFragment = new PriceMonitoringFragment();
        targetComFragment = new TargetComFragment();
        salesTargetFragment = new SalesTargetFragment();
        salesTargetFragment.setTargetFragment(targetFragment);
        salesTargetFragment.setTargetComFragment(targetComFragment);
        merchandiserFragment = new MerchandiserFragment();//new MerchandiserFragment(this, commonModel);
        crcFragment = CRCFragment.newInstance();//new CRCFragment(commonModel, customerModel, customerTypeModel);
        crcReportFragment = CrcReportFragment.newInstance();
        /*commonModel, customerModel);
            crcReportFragment.set(commonModel, customerModel);*/
        collectionTirtaFinanceFragment = new CollectionTirtaFinanceFragment();
        takingOrder3Fragment = new TakingOrder3Fragment();

            /* {
            @Override
            public void addPhoto(ItemPhotoCustomerBrandingWidget widget) {
                widget.setCustomerModel(customerModel);
                widget.addItemPhotoCustomerBrandingWidgetListener(KunjunganActivity.this);
            }
        };*/
//        brandingFragment.setCustomerModel(customerModel);
//        historyPenjualanFragment = new HistoryPenjualanFragment();//new HistoryPenjualanFragment(this);

//        historyPenjualanFragment.setCustomerModel(customerModel);

//        stokOnCarFragment.setCustomerModel(customerModel);


//        surveyFragment.setCustomerModel(customerModel);

//        assetFragment.setCommonModel(commonModel);

//        assetServiceFragment.setCommonModel(commonModel);

//        assetMutasiFragment.setCommonModel(commonModel);
//        priceMonitoringFragment.setCustomerModel(customerModel);

//        merchandiserFragment.setCommonModel(commonModel);

        /*crcFragment.setCommonModel(commonModel);
        crcFragment.setCustomerModel(customerModel);
        crcFragment.setCustomerTypeModel(customerTypeModel);*/




        /*collectionTirtaFinanceFragment.setCustomerModel(customerModel);
        collectionTirtaFinanceFragment.setCommonModel(commonModel);*/

            /*
        takingOrder3Fragment.setCustomerModel(customerModel);
        takingOrder3Fragment.setCustomerTypeModel(customerTypeModel);
        takingOrder3Fragment.setCommonModel(commonModel);*/
//        ctrlState = new CtrlState(this);

        ctrlCustBranding = new CtrlCustBranding(this);
        ctrlSurvey = new CtrlSurvey(this);
        ctrlSalesProgram = new CtrlSalesProgram(this);
        ctrlAppModul = new CtrlAppModul(this);

        Menuadapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        Menuadapter.setNotifyOnChange(true);
        if (ctrlAppModul.isModul("16"))
            Menuadapter.add(new MenuUtamaModel(1, "Informasi", getResources().getDrawable(R.drawable.information)));
        if (ctrlAppModul.isModul("17"))
            Menuadapter.add(new MenuUtamaModel(2, "Order History", getResources().getDrawable(R.drawable.history)));
        if (ctrlAppModul.isModul("18"))
            Menuadapter.add(new MenuUtamaModel(3, "Collection", getResources().getDrawable(R.drawable.collection)));
        if (ctrlAppModul.isModul("19"))
            Menuadapter.add(new MenuUtamaModel(4, "Stock Outlet", getResources().getDrawable(R.drawable.stock_outlet)));
        if (ctrlAppModul.isModul("20"))
            Menuadapter.add(new MenuUtamaModel(5, "Competitor Data", getResources().getDrawable(R.drawable.stock_competitor)));
        if (ctrlAppModul.isModul("21"))
            Menuadapter.add(new MenuUtamaModel(6, "Taking Order", getResources().getDrawable(R.drawable.taking_order)));
        if (ctrlAppModul.isModul("33"))
            Menuadapter.add(new MenuUtamaModel(14, "Asset", getResources().getDrawable(R.drawable.canvass)));
        if (ctrlAppModul.isModul("37"))
            Menuadapter.add(new MenuUtamaModel(17, "Mutasi", getResources().getDrawable(R.drawable.canvass)));
        if (ctrlAppModul.isModul("38"))
            Menuadapter.add(new MenuUtamaModel(18, "Asset Service", getResources().getDrawable(R.drawable.canvass)));
        if (ctrlAppModul.isModul("36"))
            Menuadapter.add(new MenuUtamaModel(15, "Stock Group Product", getResources().getDrawable(R.drawable.canvass)));
        if (ctrlAppModul.isModul("22"))
            Menuadapter.add(new MenuUtamaModel(7, "Canvass", getResources().getDrawable(R.drawable.canvass)));
        if (ctrlAppModul.isModul("23"))
            Menuadapter.add(new MenuUtamaModel(8, "Stock on Car", getResources().getDrawable(R.drawable.stock_on_car)));
        if (ctrlAppModul.isModul("24"))
            Menuadapter.add(new MenuUtamaModel(9, "Return", getResources().getDrawable(R.drawable.return_menu)));
        if (ctrlAppModul.isModul("25"))
            Menuadapter.add(new MenuUtamaModel(10, "Customer Branding", getResources().getDrawable(R.drawable.branding)));
        if (ctrlAppModul.isModul("26"))
            Menuadapter.add(new MenuUtamaModel(11, "Survey", getResources().getDrawable(R.drawable.survey)));
        if (ctrlAppModul.isModul("27"))
            Menuadapter.add(new MenuUtamaModel(12, "Edit Customer", getResources().getDrawable(R.drawable.edit_customer)));
        if (ctrlAppModul.isModul("28"))
            Menuadapter.add(new MenuUtamaModel(13, "Note", getResources().getDrawable(R.drawable.note)));
//        if(ctrlAppModul.isModul("34")) adapter.add(new MenuUtamaModel(16, "Report CRC", getResources().getDrawable(R.drawable.note)));

//        adapter.add(new MenuUtamaModel(19, "Capasity", getResources().getDrawable(R.drawable.note)));
        //if(ctrlAppModul.isModul("41")) adapter.add(new MenuUtamaModel(20, "Sales Target", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("41"))
            Menuadapter.add(new MenuUtamaModel(20, "Market Share", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("42"))
            Menuadapter.add(new MenuUtamaModel(21, "Price Monitoring", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("43"))
            Menuadapter.add(new MenuUtamaModel(22, "Merchandiser", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("34"))
            Menuadapter.add(new MenuUtamaModel(23, "Stock & TO", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("45"))
            Menuadapter.add(new MenuUtamaModel(24, "Report CRC", getResources().getDrawable(R.drawable.note)));
        if (ctrlAppModul.isModul("47"))
            Menuadapter.add(new MenuUtamaModel(25, "Taking Order 2", getResources().getDrawable(R.drawable.taking_order)));
        if (ctrlAppModul.isModul("48"))
            Menuadapter.add(new MenuUtamaModel(26, "Collection 2", getResources().getDrawable(R.drawable.collection)));
        if (ctrlAppModul.isModul("49"))
            Menuadapter.add(new MenuUtamaModel(27, "Taking Order 3", getResources().getDrawable(R.drawable.taking_order)));

        isModul71 = ctrlAppModul.isModul("71");
        isModul72 = ctrlAppModul.isModul("72");

//        android.util.Log.e("71", ""+ctrlAppModul.get("71").AppModulId);
//        android.util.Log.e("72", ""+ctrlAppModul.get("72").AppModulId);

        mDrawerList.setAdapter(Menuadapter);
        mDrawerList.setOnItemClickListener(this);

        mTitle = mDrawerTitle = getTitle();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        ctrlCustomerType = new CtrlCustomerType(this);
        ctrlCustomer = new CtrlCustomer(this);
        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlSKU = new CtrlSKU(this);
        ctrlStokOutlet = new CtrlStokOutlet(this);
        ctrlStokCompetitor = new CtrlStokCompetitor(this);
        ctrlTakingOrder = new CtrlTakingOrder(this);
//        ctrlSalesProgram = new CtrlSalesProgram(this);
        ctrlRetur = new CtrlRetur(this);
        ctrlCommon = new CtrlCommon(this);
        ctrlCollector = new CtrlCollector(this);
        ctrlGroupSKUCom = new CtrlGroupSKUCom(this);
        ctrlSKUCom = new CtrlSKUCom(this);
        ctrlConfig = new CtrlConfig(this);
        ctrlStockGroup = new CtrlStockGroup(this);
        ctrlDiscountPromo = new CtrlDiscountPromo(this);
        ctrlDiscountResult = new CtrlDiscountResult(this);
        dbmaster = new DbMasterHelper(this);

//        dbDiskonHelper = new DbDiskonHelper(this);

//        CollectorModel collectorModel = ctrlCollector.get(commonModel.CommonID);
//        if(collectorModel == null) {
//        	collectorModelPayment = new CollectorModel(commonModel.CommonID, "1");
//        	collectorModelNotPayment = new CollectorModel(commonModel.CommonID, "2");
//        } else {
//        	if(collectorModel.status.equals("1")) {
//        		collectorModelPayment = collectorModel;
//            	collectorModelNotPayment = new CollectorModel(commonModel.CommonID, "2");
//        	} else if(collectorModel.status.equals("2")) {
//            	collectorModelPayment = new CollectorModel(commonModel.CommonID, "1");
//            	collectorModelNotPayment = collectorModel;
//        	}
//        }
//        collectionFragment.setCollectorModelPayment(collectorModelPayment);
//        collectionFragment.setCollectorModelNotPayment(collectorModelNotPayment);

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);

        Log.d("Restore Called", "Called");
        mTitle = Menuadapter.getItem(0).menu;
        selectItem(1);

        bluePrint = new BluePrint(this);

        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });

        progressDialog.setMessage("Checking Customer Data...");
        progressDialog.show();

        checkLocationCustomer();

//        new LoadData().execute();
    }

    private void CheckIfServiceIsRunning() {
        if (SadixService.isRunning()) {
            bindService(new Intent(this, SadixService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            isBound = true;
        }
    }

    protected void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    public void keInformasi() {
        selectItem(1);
        mDrawerList.setItemChecked(0, true);
        setTitle("Informasi");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("On Pause Kunj", "Called");
        Log.d("Active Cust Id", sharedPreferences.getString("custId", "0"));
        realmUI.close();
        if (!sharedPreferences.getString("custId", "0").equals("0")) {
            sharedPreferences.edit().putString("custId", commonModel.CommonID).apply();
            sharedPreferences.edit().putBoolean("onResume", true).apply();
        }
        if (isHome) {
//            Util.Toast(this, "Save Cache");
//            saveLokal("");
        }
    }

    @Override
    protected void onResume() {
        Log.d("On Resume Kunjungan", "Called");
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
        isHome = true;
    }

    @Override
    protected void onStart() {
        Log.d("On Start Kunjungan", "Called");
        super.onStart();
        isHome = true;
        if (ctrlAppModul.isModul("50")) {
            /*if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                isHome = false;
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }*/
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("On Stop", "Called");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            jenis = savedInstanceState.getInt("jenis");
        }
        Log.d("On Restore Kunjungan", "Called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("On Restart", "Called");
    }

    @Override
    public void onBackPressed() {
        if (isInPrinsiple) {
            isInPrinsiple = false;
            restoreMenuFunction();
            super.onBackPressed();
        } else {
            close();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("On Destroy KUnj", "Called");
        realmUI.close();
        if (isBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, SadixService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            unbindService(serviceConnection);
            isBound = false;
        }

        bluePrint.close();
    }

    private void populateTakingOrder() {
        try {
            Realm realmO = Realm.getDefaultInstance();

            final CommonModel[] cmModel = new CommonModel[1];

            realmO.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    cmModel[0] = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
                }
            });

            Log.d("Common ID", cmModel[0].CommonID);

            takingOrderAdapter = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            returDetailAdapter = new ReturDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<ReturDetailModel>>());
            stokOutletAdapter = new StokCustomerAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokCustomerModel>>());
            stokOnCarAdapter = new StokOnCarAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
            stokGroupAdapter = new StokGroupAdapter(this, new ArrayList<StockGroupModel>());

            targetAdapter = new TargetAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<TargetModel>>());
            capasityAdapter = new CapasityAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<CapasityModel>>());
            priceMonitoringProdukAdapter = new PriceMonitoringAdapter(this, new ArrayList<>(), new HashMap<Object, List<PriceMonitoringModel>>());

            orderCrcAdapter = new CrcOrderAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<CrcOrderStokModel>>());
            crcReportAdapter = new CrcReportAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<CRCReportModel>>());

            List<GroupSKUModel> list = ctrlGroupSKU.list();

            for (GroupSKUModel groupSKUModel : list) {
                takingOrderAdapter.addHeader(groupSKUModel);
                returDetailAdapter.addHeader(groupSKUModel);
                stokOutletAdapter.addHeader(groupSKUModel);
                stokOnCarAdapter.addHeader(groupSKUModel);
                Log.d("Customer ID", customerModel.CustId);
                Log.d("Common ID", cmModel[0].CommonID);
                stokGroupAdapter.add(new StockGroupModel(groupSKUModel, customerModel.CustId, cmModel[0].CommonID));

                capasityAdapter.addHeader(groupSKUModel);
                targetAdapter.addHeader(groupSKUModel);
                if (isModul71/*ctrlAppModul.isModul("71")*/) {
                    Date date = new Date();
                    Log.d("SKU IN", ctrlSKU.filterByGroup(groupSKUModel));
                    String sql = "select count(*) jml from producttarget where "
                            + "CustID='" + customerModel.CustId + "' "
                            + "and SkuID in (" + ctrlSKU.filterByGroup(groupSKUModel) + ") "
                            + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                            + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                            + "and St_Target='0'";
                    Cursor cursor = dbmaster.query(sql);
                    if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                        priceMonitoringProdukAdapter.addHeader(groupSKUModel);
                    }
                    cursor.close();
                    dbmaster.close();
                } else {
                    Log.wtf("PM", "PM Added - no filter");
                    priceMonitoringProdukAdapter.addHeader(groupSKUModel);
                }
                orderCrcAdapter.addHeader(groupSKUModel);
                crcReportAdapter.addHeader(groupSKUModel);
            }
            realmO.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateCache() {
        populateTakingOrder1(takingOrderAdapter);
//        populateTakingOrder2(prinsipleAdapter);

        populateStok();
//        populatePriceMonitoring();
        populateReturn();
        populateCollectionNotPay();

        if (ctrlAppModul.isModul("33")) {
            populateAsset();
        }
        populateMerchandiser();
    }

    private void populateAsset() {
        Realm realmO = Realm.getDefaultInstance();
        final CommonModel[] cmModel = new CommonModel[1];
        realmO.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                cmModel[0] = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        assetAdapter = new AssetAdapter(this, new ArrayList<AssetModel>());
        assetMutasiAdapter = new AssetMutasiAdapter(this, new ArrayList<AssetMutasiModel>());
        assetServiceAdapter = new AssetServiceAdapter(this, new ArrayList<AssetServiceModel>());

//        List<JSONObject> assetObject = databaseHelper.list("Aset", commonModel.CommonID);
        RealmResults<com.bhn.sadix.Data.AssetModel> assetObject = realmO.where(com.bhn.sadix.Data.AssetModel.class)
                .equalTo("CommonID", cmModel[0].CommonID).equalTo("tipe", 1).findAll();
        String assetName = "init";

        try {
            for (com.bhn.sadix.Data.AssetModel object : assetObject) {
                JSONObject json = dbAsetHelper.get("select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + cmModel[0].CustomerId + " and ASETNO='" + object.AsetID + "'");

                if (!json.getString("ASETNAME").equals(assetName)) {
                    Log.d("AssetModel added", "YES");
                    AssetModel assetModel = new AssetModel(this,
                            json.getString("ASETNAME"),
                            json.getString("ASETTYPE"),
                            json.getString("ASETNO"),
                            json.getString("ASETSID"),
                            assetFragment,
                            cmModel[0].CustomerId,
                            cmModel[0].CommonID, 1
                    );
                    assetAdapter.add(assetModel);
                }

                assetName = json.getString("ASETNAME");
                /*AssetModel model = new AssetModel(this, object.getString("Name"), object.getString("AssetType"),
                        object.getString("AssetNo"), object.getString("ASSETID"), assetFragment, customerModel.CustId, commonModel.CommonID);*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.wtf("Asset Adapter", "Asset Adapter SIZE " + assetAdapter.getCount());


        RealmResults<com.bhn.sadix.Data.AssetModel> asset = realmO.where(com.bhn.sadix.Data.AssetModel.class)
                .equalTo("CommonID", cmModel[0].CommonID).equalTo("tipe", 4).findAll();
        String assetServiceName = "init";

        try {
            for (com.bhn.sadix.Data.AssetModel object : asset) {
                JSONObject json = dbAsetHelper.get("select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + cmModel[0].CustomerId + " and ASETNO='" + object.AsetID + "'");

                if (!json.getString("ASETNAME").equals(assetServiceName)) {
                    Log.d("AssetModel added", "YES");
                    AssetServiceModel assetModel = new AssetServiceModel(this,
                            json.getString("ASETNAME"),
                            json.getString("ASETTYPE"),
                            json.getString("ASETNO"),
                            json.getString("ASETSID"),
                            assetServiceFragment,
                            commonModel.CustomerId,
                            commonModel.CommonID
                    );
                    assetServiceAdapter.add(assetModel);
                }

                assetName = json.getString("ASETNAME");
                /*AssetModel model = new AssetModel(this, object.getString("Name"), object.getString("AssetType"),
                        object.getString("AssetNo"), object.getString("ASSETID"), assetFragment, customerModel.CustId, commonModel.CommonID);*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmO.close();
       /* try {
            List<JSONObject> mutasiObject = databaseHelper.list("Newasset", commonModel.CommonID);

            for (JSONObject object : mutasiObject) {
                String ASSETNAME = object.getString("AsetName");
                String ASETID = object.getString("AsetID");
//                String ASETNO = object.getString("AsetID");
                int jenis = 1;
                AssetMutasiModel model = new AssetMutasiModel(assetMutasiFragment.getActivity(), ASSETNAME, ASETID, ASETID, jenis, customerModel.CustId, new AssetMutasiModel.AssetMutasiEvent() {
                    @Override
                    public void add(AssetMutasiModel model) {

                    }

                    @Override
                    public void cancel(AssetMutasiModel model) {
                        assetMutasiAdapter.add(model);
                    }
                });

                assetMutasiAdapter.add(model);
            }

            JSONObject json = assetMutasiModel.getAssetMutasiWidget().toJSON();
            json.put("CommonID", commonModel.CommonID);
            databaseHelper.save(json, "Newasset");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    private void populateMerchandiser() {
        merchandiserAdapter = new MerchandiserAdapter(this, new ArrayList<MerchandiserModel>());
        merchandiserAdapter = new MerchandiserAdapter(this, dbAsetHelper.listMerchandiser(this, assetFragment, commonModel.CommonID));
    }

    private void populateCollectionNotPay() {
        RealmResults<CollectionModel> collectionModels = realmUI.where(CollectionModel.class)
                .equalTo("CommonID", commonModel.CommonID)
                .equalTo("status", "2")
                .findAll();

//        List<CollectorModel> list = ctrlCollector.get(commonModel.CommonID, "2");
        if (collectionModels.size() > 0) {
            collectionModelNotPayment = collectionModels.get(0);
        }
    }

    private void populateReturn() {
        try {
            for (int i = 0; i < returDetailAdapter.getGroupCount(); i++) {
                GroupSKUModel group = returDetailAdapter.getGroup(i);
                List<ReturDetailModel> list = returDetailAdapter.listOrderByGroup(group);
                if (list == null || list.size() < group.jumlah) {
                    List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                    for (SKUModel skuModel : listSKU) {
                        ReturDetailModel returDetailModel = new ReturDetailModel(skuModel);
                        if (list == null || list.indexOf(returDetailModel) == -1) {
                            ReturModel returModel = ctrlRetur.getReturModel(commonModel.CommonID, skuModel.SKUId);
                            if (returModel != null) {
                                returDetailModel.note = returModel.note;
                                returDetailModel.QTY_B = returModel.QTY_B;
                                returDetailModel.QTY_K = returModel.QTY_K;
                                returDetailModel.tipe = ctrlConfig.getByConfigClassAndConfigId("REASON_RETURN", returModel.tipe);
                                returDetailModel.RandomID = returModel.RandomID;
                            }
                            returDetailAdapter.addChild(group, returDetailModel);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Util.showDialogError(this, e.getMessage());
        }
    }

    /*private void populatePriceMonitoring() {
        for (int i = 0; i < priceMonitoringProdukAdapter.getGroupCount(); i++) {
            GroupSKUModel group = (GroupSKUModel) priceMonitoringProdukAdapter.getGroup(i);

            List<JSONObject> listPriceMonitoring = databaseHelper.list("PriceMonitoring", commonModel.CommonID);

            List<PriceMonitoringModel> list = priceMonitoringProdukAdapter.listOrderByGroup(group);

            int count = 0;

            if (list == null *//*|| list.size() < group.jumlah*//*) {
                List<SKUModel> listSKU = ctrlSKU.listByGroupStok(group);
                for (SKUModel skuModel : listSKU) {
                    if (ctrlAppModul.isModul("71")) {
                        Date date = new Date();
                        String sql = "select count(*) jml from producttarget where "
                                + "CustID='" + customerModel.CustId + "' "
                                + "and SkuID='" + skuModel.SKUId + "' "
                                + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                                + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                                + "and St_Target='0'";
                        Cursor cursor = dbmaster.query(sql);
                        if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                            PriceMonitoringModel model = new PriceMonitoringModel(skuModel);

                            for (int j = 0; j < listPriceMonitoring.size(); j++) {
                                try {
                                    if (listPriceMonitoring.get(j).getString("SKUId").equals(skuModel.SKUId)) {
                                        model.SellPrice = listPriceMonitoring.get(j).getLong("SellPrice");
                                        model.BuyPrice = listPriceMonitoring.get(j).getLong("BuyPrice");
                                        model.Qty = listPriceMonitoring.get(j).getLong("Qty");
                                        model.Note = listPriceMonitoring.get(j).getString("Note");
                                        model.LevelID = listPriceMonitoring.get(j).getInt("LevelID");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            priceMonitoringProdukAdapter.addChild(group, model);
                            android.util.Log.e("filter sku monitoring", "filter sku monitoring");
                        }
                        cursor.close();
                        dbmaster.close();
                    } else {
                        PriceMonitoringModel model = new PriceMonitoringModel(skuModel);

                        for (int j = 0; j < listPriceMonitoring.size(); j++) {
                            try {
                                if (listPriceMonitoring.get(j).getString("SKUId").equals(skuModel.SKUId)) {
                                    model.SellPrice = listPriceMonitoring.get(j).getLong("SellPrice");
                                    model.BuyPrice = listPriceMonitoring.get(j).getLong("BuyPrice");
                                    model.Qty = listPriceMonitoring.get(j).getLong("Qty");
                                    model.Note = listPriceMonitoring.get(j).getString("Note");
                                    model.LevelID = listPriceMonitoring.get(j).getInt("LevelID");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        priceMonitoringProdukAdapter.addChild(group, model);
                        android.util.Log.e("filter sku monitoring", "ga filter sku monitoring");
                    }
                    count += 1;
                }
            }
        }

        Log.wtf("Price Produk Size", "" + priceMonitoringProdukAdapter.getGroupCount());
    }*/

    private void populateStok() {
        try {
            for (int i = 0; i < stokOutletAdapter.getGroupCount(); i++) {
                GroupSKUModel group = stokOutletAdapter.getGroup(i);
                List<StokCustomerModel> list = stokOutletAdapter.listOrderByGroup(group);
                if (list == null || list.size() < group.jumlah) {
                    List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                    for (final SKUModel skuModel : listSKU) {
                        StokCustomerModel model = new StokCustomerModel(customerModel, skuModel, 0, 0, UUID.randomUUID().toString());
                        if (list == null || list.indexOf(model) == -1) {
//                            StockOutletModel stockOutletModel = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID, skuModel.SKUId);
                            final StockCustomerModel[] stkModel = new StockCustomerModel[1];
                            realmUI.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    stkModel[0] = realmUI.where(StockCustomerModel.class)
                                            .equalTo("SKUId", skuModel.SKUId)
                                            .equalTo("CommonID", commonModel.CommonID)
                                            .findFirst();
                                }
                            });

                            if (stkModel[0] != null) {
                                model.QTY_B = stkModel[0].QTY_B;
                                model.QTY_K = stkModel[0].QTY_K;
                                model.RandomID = stkModel[0].RandomID;
                            }
                            stokOutletAdapter.addChild(group, model);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(this, e.getMessage());
        }
    }

    /*private void populateTakingOrder2(PrinsipleAdapter to2eAdapter) {
        if (to2eAdapter != null) {
            for (int i = 0; i < to2eAdapter.getCount(); i++) {
                PrinsipleModel model = to2eAdapter.getItem(i);
                for (int j = 0; j < model.list.size(); j++) {
                    ArrayList<TakingOrderModel> ls = model.list;
                    if (ls != null) {
                        android.util.Log.e("kesini", "oke");
                        StringBuilder sb = new StringBuilder();
                        sb.append("(");
                        for (int is = 0; is < ls.size(); is++) {
                            if (is > 0) sb.append(",");
                            sb.append("'").append(ls.get(i).SKUId).append("'");
                        }
                        sb.append(")");
                        Log.d("SB", "" + sb);

                        for (int k = 0; i < takingOrderAdapter.getGroupCount(); i++) {
                            GroupSKUModel group = takingOrderAdapter.getGroup(k);
                            if (ctrlGroupSKU.isLoadGroup(group.GroupId, sb.toString())) {
                                List<OrderDetailModel> list = takingOrderAdapter.listOrderByGroup(group);
                                if (list == null || list.size() < group.jumlah) {
                                    List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                                    for (SKUModel skuModel : listSKU) {
                                        OrderDetailModel orderDetailModel = new OrderDetailModel(skuModel);
                                        if (list == null || list.indexOf(orderDetailModel) == -1) {
                                            TakingOrderModel takingOrderModel = null;

                                            ArrayList<TakingOrderModel> lis = model.list;
                                            if (lis != null) {
                                                int index = lis.indexOf(new TakingOrderModel(skuModel.SKUId));
                                                if (index != -1) {
                                                    takingOrderModel = lis.get(index);
                                                }
                                            }

                                            if (takingOrderModel == null) {
                                                takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, skuModel.SKUId);
                                            }
                                            if (takingOrderModel != null) {
                                                orderDetailModel.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                                                orderDetailModel.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                                                orderDetailModel.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                                                orderDetailModel.note = takingOrderModel.note;
                                                orderDetailModel.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                                                orderDetailModel.QTY_B = takingOrderModel.QTY_B;
                                                orderDetailModel.QTY_K = takingOrderModel.QTY_K;
                                                String SalesProgramId = takingOrderModel.SalesProgramId;
                                                if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                                    orderDetailModel.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                                                    if (orderDetailModel.salesProgramModel == null) {
                                                        orderDetailModel.salesProgramModel = new SalesProgramModel();
                                                    }
                                                } else {
                                                    orderDetailModel.salesProgramModel = new SalesProgramModel();
                                                }
                                                orderDetailModel.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                                                orderDetailModel.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                                                orderDetailModel.PRICE_B = takingOrderModel.PRICE_B;
                                                orderDetailModel.PRICE_K = takingOrderModel.PRICE_K;
                                                orderDetailModel.RandomID = takingOrderModel.RandomID;
                                            }
                                            takingOrderAdapter.addChild(group, orderDetailModel);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }*/


    private void populateTakingOrder1(OrderDetailAdapter to1Adapter) {
        for (int i = 0; i < to1Adapter.getGroupCount(); i++) {
            try {
                GroupSKUModel group = to1Adapter.getGroup(i);
                List<OrderDetailModel> list = to1Adapter.listOrderByGroup(group);
                if (list == null || list.size() < group.jumlah) {
                    List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                    for (SKUModel skuModel : listSKU) {
                        OrderDetailModel orderDetailModel = new OrderDetailModel(skuModel);
                        if (list == null || list.indexOf(orderDetailModel) == -1) {
                            TakingOrderModel takingOrderModel = null;
                            /*if (isPrinciple) {
                                ArrayList<TakingOrderModel> ls = listener.getPrinsipleModel().list; *//*(ArrayList<TakingOrderModel>) getActivity().getIntent().getSerializableExtra("list");*//*
                                if (ls != null) {
                                    int index = ls.indexOf(new TakingOrderModel(skuModel.SKUId));
                                    if (index != -1) {
                                        takingOrderModel = ls.get(index);
                                    }
                                }
                            }*/
                            if (takingOrderModel == null) {
                                takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, skuModel.SKUId);
                            }
                            if (takingOrderModel != null) {
                                orderDetailModel.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                                orderDetailModel.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                                orderDetailModel.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                                orderDetailModel.note = takingOrderModel.note;
                                orderDetailModel.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                                orderDetailModel.QTY_B = takingOrderModel.QTY_B;
                                orderDetailModel.QTY_K = takingOrderModel.QTY_K;
                                String SalesProgramId = takingOrderModel.SalesProgramId;
                                if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                    orderDetailModel.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                                    if (orderDetailModel.salesProgramModel == null) {
                                        orderDetailModel.salesProgramModel = new SalesProgramModel();
                                    }
                                } else {
                                    orderDetailModel.salesProgramModel = new SalesProgramModel();
                                }
                                orderDetailModel.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                                orderDetailModel.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                                orderDetailModel.PRICE_B = takingOrderModel.PRICE_B;
                                orderDetailModel.PRICE_K = takingOrderModel.PRICE_K;
                                orderDetailModel.RandomID = takingOrderModel.RandomID;
                            }
                            to1Adapter.addChild(group, orderDetailModel);
                            /*if (jenis == 1) { //all
                                if (group.FokusId.equals("1")) {
                                    adapterFocus.addChild(group, orderDetailModel);
                                }
                                if (group.PromoId.equals("1")) {
                                    adapterPromo.addChild(group, orderDetailModel);
                                }
                            } else if (jenis == 2) { //focus
                                adapterFocus.addChild(group, orderDetailModel);
                                if (group.PromoId.equals("1")) {
                                    adapterPromo.addChild(group, orderDetailModel);
                                }
                            } else if (jenis == 3) { //promo
                                adapterPromo.addChild(group, orderDetailModel);
                                if (group.FokusId.equals("1")) {
                                    adapterFocus.addChild(group, orderDetailModel);
                                }
                            }*/
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Util.showDialogError(this, e.getMessage());
            }
        }
    }

    private void populateStokCompetitor() {
        try {
            Realm realmO = Realm.getDefaultInstance();

            final CommonModel[] cmModel = new CommonModel[1];

            realmO.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    cmModel[0] = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
                }
            });

            Log.d("Common ID", cmModel[0].CommonID);

            stokCompetitorAdapter = new StokCompetitorAdapter(this, new ArrayList<GroupSKUComModel>(), new HashMap<GroupSKUComModel, List<StokCompetitorModel>>());
            priceMonitoringCompetitorAdapter = new PriceMonitoringAdapter(this, new ArrayList<Object>(), new HashMap<Object, List<PriceMonitoringModel>>());
            targetComAdapter = new TargetComAdapter(this, new ArrayList<GroupSKUComModel>(), new HashMap<GroupSKUComModel, List<TargetComModel>>());

            List<GroupSKUComModel> list = ctrlGroupSKUCom.list();
            for (GroupSKUComModel groupSKUComModel : list) {
                stokCompetitorAdapter.addHeader(groupSKUComModel);
                if (isModul72/*ctrlAppModul.isModul("72")*/) {
                    Date date = new Date();
                    String sql = "select count(*) jml from producttarget where "
                            + "CustID='" + customerModel.CustId + "' "
                            + "and SkuID in (" + ctrlSKUCom.filterByGroup(groupSKUComModel) + ") "
                            + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                            + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                            + "and St_Target='1'";
                    Cursor cursor = dbmaster.query(sql);
                    if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                        priceMonitoringCompetitorAdapter.addHeader(groupSKUComModel);
                    }
                    cursor.close();
                    dbmaster.close();
                } else {
                    priceMonitoringCompetitorAdapter.addHeader(groupSKUComModel);
                }

                targetComAdapter.addHeader(groupSKUComModel);
            }
//                List<SKUComModel> listSKU = ctrlSKUCom.listByGroup(groupSKUComModel);

                /*for (SKUComModel skuComModel : listSKU) {
                    int QTY_B = 0;
                    int QTY_K = 0;
                    double PRICE = 0;
                    double PRICE2 = 0;
                    String Note = "";
                    String RandomID = UUID.randomUUID().toString();
                    StockCompetitorModel stockCompetitorModel = ctrlStokCompetitor.getStockCompetitorModel(cmModel.CommonID, skuComModel.SKUId);
                    if (stockCompetitorModel != null) {
                        QTY_B = stockCompetitorModel.QTY_B;
                        QTY_K = stockCompetitorModel.QTY_K;
                        PRICE = stockCompetitorModel.PRICE;
                        PRICE2 = stockCompetitorModel.PRICE2;
                        Note = stockCompetitorModel.Note;
                        RandomID = stockCompetitorModel.RandomID;
                    }
                    stokCompetitorAdapter.addChild(groupSKUComModel, new StokCompetitorModel(customerModel, skuComModel, QTY_B, QTY_K, PRICE, PRICE2, Note, RandomID));

                    if (ctrlAppModul.isModul("72")) {
                        Date date = new Date();
                        String sql = "select count(*) jml from producttarget where "
                                + "CustID='" + customerModel.CustId + "' "
                                + "and SkuID='" + skuComModel.SKUId + "' "
                                + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                                + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                                + "and St_Target='1'";
                        Cursor cursor = dbmaster.query(sql);
                        if (cursor.moveToNext() && cursor.getInt(0) > 0) {

                            final PriceMonitoringModel[] model = {realmO.where(PriceMonitoringModel.class)
                                    .equalTo("SKUId", skuComModel.SKUId)
                                    .equalTo("CommonID", cmModel.CommonID)
                                    .findFirst()};

                            if (model[0] == null) {
                                model[0] = new PriceMonitoringModel(0, skuComModel.ProductName);
                                model[0].SKUId = skuComModel.SKUId;
                                final PriceMonitoringModel finalModel = model[0];
                                realmO.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        model[0] = realm.copyToRealm(finalModel);
                                    }
                                });
                            }
                            priceMonitoringCompetitorAdapter.addChild(groupSKUComModel, model[0]);
                        }
                        cursor.close();
                        dbmaster.close();
                    } else {
//                    PriceMonitoringModel model = new PriceMonitoringModel(0, skuComModel.ProductName);
                        final PriceMonitoringModel[] model = {realmO.where(PriceMonitoringModel.class)
                                .equalTo("SKUId", skuComModel.SKUId)
                                .equalTo("CommonID", cmModel.CommonID)
                                .findFirst()};

                        if (model[0] == null) {
                            Log.d("Model Not In Database", "true");
                            model[0] = new PriceMonitoringModel(0, skuComModel.ProductName);
                            model[0].SKUId = skuComModel.SKUId;
                            final PriceMonitoringModel finalModel = model[0];
                            realmO.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    model[0] = realm.copyToRealm(finalModel);
                                }
                            });
                        }
                        priceMonitoringCompetitorAdapter.addChild(groupSKUComModel, model[0]);
                    }

                    Cursor cursor = dbmaster.query("select * from productperiod order by year,month");
                    targetComAdapter.addChild(groupSKUComModel, new TargetComModel(skuComModel, cursor, dbmaster, customerModel, Util.getStringPreference(this, "SalesId")));
                    cursor.close();
                    dbmaster.close();

                    Log.wtf("PriceMonitoring Size", "" + priceMonitoringCompetitorAdapter.getGroupCount());*/
//            }
            realmO.close();
        } catch (Exception e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu.size() > 0) {
            menu.clear();
        }

        MenuItem item1 = menu.add(0, 1, 1, "Cancel");
        MenuItem item2 = menu.add(0, 2, 2, "Check out");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            item2.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }

        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
//            checkPendingItem();
            Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
                @Override
                public void onDialogCompleted(boolean answer) {
                    if (answer) {
                        Intent intent = new Intent(KunjunganActivity.this, VisitActivity.class);
                        intent.putExtra("CustomerTypeModel", customerTypeModel);
                        intent.putExtra("customerModel", customerModel);
//                            intent.putExtra("CommonModel", commonModel);
                        startActivityForResult(intent, 3);
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    private void chekOutOrder() {
        final String CallST = Util.getStringPreference(this, "CallST");
//		Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
//			@Override
//			public void onDialogCompleted(boolean answer) {
//				if(answer) {
        boolean noOrder = false;
        /*if (CallST.equals("18")) { //Collection
            if (collectionFragment.payAdapter.getCount() == 0) noOrder = true;*/
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        //Merchandiser_tipe = 5;
        //AssetService_tipe = 4;
        //Asset_tipe = 1;
        Log.d("Common Price", commonModel.PRICE + "");

        if (CallST.equals("18")) { //Collection
            if (commonModel.collectionModel.size() == 0) noOrder = true;
        } else if (CallST.equals("21") || CallST.equals("22")) {//Taking Order dan Canvass
            noOrder = commonModel.PRICE == 0;
        } else if (CallST.equals("41")) { //Target

        } else if (CallST.equals("42")) { //Price Monitoring
            noOrder = commonModel.priceMonitoringModel.size() == 0;
        } else if (CallST.equals("43")) { //Merchandiser
            if (!(realmUI.where(com.bhn.sadix.Data.AssetModel.class)
                    .equalTo("CommonID", commonModel.CommonID)
                    .equalTo("tipe", 5).findAll().size() > 0)) {
                noOrder = true;
            }
            /*MerchandiserAdapter adapter = merchandiserFragment.getAdapter();
            noOrder = true;
            if (adapter != null) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (adapter.getItem(i).isEntry) {
                        noOrder = false;
                        break;
                    }
                }
            }*/
        } else if (CallST.equals("33")) { //Asset
            if (!(realmUI.where(com.bhn.sadix.Data.AssetModel.class)
                    .equalTo("CommonID", commonModel.CommonID)
                    .equalTo("tipe", 1).findAll().size() > 0)) {
                noOrder = true;
            }
            /*if (assetFragment.getAdapter() != null) {
                if (assetFragment.getAdapter().getCount() == 0) noOrder = true;
            }*/
        } else if (CallST.equals("37")) { //Mutasi
            if (!(commonModel.assetMutasiModel.size() > 0) && !(realmUI.where(com.bhn.sadix.Data.AssetModel.class)
                    .equalTo("CommonID", commonModel.CommonID)
                    .equalTo("tipe", 4).findAll().size() > 0) &&
                    !(realmUI.where(com.bhn.sadix.Data.AssetModel.class)
                            .equalTo("CommonID", commonModel.CommonID)
                            .equalTo("tipe", 3).findAll().size() > 0))
                noOrder = true;

            /*if (assetMutasiFragment.getAdapter() != null) {
                if (assetMutasiFragment.getAdapter().getCount() == 0) noOrder = true;
            }*/
        } else if (CallST.equals("38")) { //Asset Service
            if (!(realmUI.where(com.bhn.sadix.Data.AssetModel.class)
                    .equalTo("CommonID", commonModel.CommonID)
                    .equalTo("tipe", 4).findAll().size() > 0)) {
                noOrder = true;
            }
            /*if (assetServiceFragment.getAdapter() != null) {
                if (assetServiceFragment.getAdapter().getCount() == 0) noOrder = true;
            }*/
        }

        if (noOrder) {
            noOrder();
        } else {
            if (ctrlAppModul.isModul("35")) {
                pinOrder();
            } else {
                if (ctrlAppModul.isModul("74")) {
                    if (ctrlAppModul.isModul("49")) {
                        if (takingOrder3Fragment.isOrder()) {
                            popupAlamat();
                        } else {
                            kirimOrder();
                        }
                    } else {
                        if (takingOrderAdapter.isOrder()) {
                            popupAlamat();
                        } else {
                            kirimOrder();
                        }
                    }
                } else {
                    kirimOrder();
                }
            }
        }
//				}
//			}
//		});
    }

    private void popupAlamat() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Delivery Address");
        dialog.setContentView(R.layout.popup_alamat);
        ((Spinner) dialog.findViewById(R.id.deliveryAddress)).setAdapter(new ComboBoxAdapter(this, dbmaster.listCombo("select AddID,AddAddres from custaddres where CustID='" + customerModel.CustId + "'")));
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final ComboBoxModel model = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.deliveryAddress)).getSelectedItem();
                if (model != null) {
                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            commonModel.Description = model.text;
                        }
                    });
                    kirimOrder();
                } else {
                    Util.showDialogInfo(KunjunganActivity.this, "Alamat pengiriman tidak ada!");
                }
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void pinOrder() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pin_order);
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = ((EditText) dialog.findViewById(R.id.pin)).getText().toString();
                if (pin.equals(customerModel.PinID)) {
                    dialog.dismiss();
                    kirimOrder();
                } else {
                    Util.showDialogInfo(KunjunganActivity.this, "PIN salah!");
                }
            }
        });
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void kirimOrder() {
        /*if (customerModel.GeoLat == null || customerModel.GeoLat.equals("") || customerModel.GeoLong == null || customerModel.GeoLong.equals("") || customerModel.GeoLat.equals("0") || customerModel.GeoLong.equals("0")) {
            selectItem(12);
            return;
        }*/
        AppModul modul = ctrlAppModul.get("51");
        if (modul.AppModulValue.equals("2") || modul.AppModulValue.equals("3")) {
            double GeoLong = Double.parseDouble((customerModel.GeoLong.equals("") ? "0" : customerModel.GeoLong));
            if (GeoLong < 90) {
                Util.showDialogInfo(this, "Customer wajib di edit!");
                selectItem(12);
                return;
            }
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel.ROrderID = "0";
            }
        });
        progressDialog.setMessage("Check Out Data...");
        progressDialog.show();
//    	new Checkout().execute();
        new CheckoutBackground().execute();

    }

    private void checkPendingItem() {
        boolean anyBranding = false;
        boolean anySurvey = false;
        List<CustBrandingModel> custBrandingModels = ctrlCustBranding.list(customerModel.CustId, commonModel.CommonID);
        int count = 0;

        if (custBrandingModels.size() > 0) {
            Log.d("custBrandingModel", "Size : " + custBrandingModels.size());
            anyBranding = true;
            count = custBrandingModels.size();
        }

        if (brandingFragment.isVisible() && brandingFragment.getWidgetCount() > 0) {
            Log.d("custBrandingModel", "Branding Fragment visible");
            anyBranding = true;
            count = brandingFragment.getWidgetCount();
        }

        if (ctrlSurvey.getUndoneCommonSurvey(commonModel.CommonID) != null) {
            Log.d("Survey Not Null", ctrlSurvey.getUndoneCommonSurvey(commonModel.CommonID).CommonId);
            anySurvey = true;
        }

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_pending_item, null);
        final AlertDialog pendingDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setTitle("Data belum terkirim").setCancelable(false).setView(view);
        pendingDialog = builder.create();

//        pendingDialog.setTitle("Data belum terkirim");
//        pendingDialog.
//        RecyclerView recBranding = (RecyclerView) view.findViewById(R.id.rec_branding);
        TextView txtGotoBranding = (TextView) view.findViewById(R.id.txt_goto_branding);
        TextView txtGotoSurvey = (TextView) view.findViewById(R.id.txt_goto_survey);
        TextView txtCount = (TextView) view.findViewById(R.id.txt_count_branding);
        txtCount.setText("" + count);
//        recBranding.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        txtGotoBranding.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(10);
                pendingDialog.dismiss();
            }
        });

        txtGotoSurvey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem(11);
                pendingDialog.dismiss();
            }
        });

        if (anyBranding && anySurvey) {
            view.findViewById(R.id.layout_survey).setVisibility(View.VISIBLE);
            view.findViewById(R.id.layout_branding).setVisibility(View.VISIBLE);

//            recBranding.setAdapter(new BrandingPendingAdapter(custBrandingModels, this, this, customerModel, pendingDialog, this));

            pendingDialog.setContentView(view);
            pendingDialog.show();
        } else if (anyBranding) {
            view.findViewById(R.id.layout_survey).setVisibility(View.GONE);
//            recBranding.setAdapter(new BrandingPendingAdapter(custBrandingModels, this, this, customerModel, pendingDialog, this));

            pendingDialog.setContentView(view);
            pendingDialog.show();
        } else if (anySurvey) {
            view.findViewById(R.id.layout_branding).setVisibility(View.GONE);

            pendingDialog.setContentView(view);
            pendingDialog.show();
        } else {
//            boolean pass = checkLocationCustomer();

            /*if (pass) {
                Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) {
                            Intent intent = new Intent(KunjunganActivity.this, VisitActivity.class);
                            intent.putExtra("CustomerTypeModel", customerTypeModel);
                            intent.putExtra("customerModel", customerModel);
//                            intent.putExtra("CommonModel", commonModel);
                            startActivityForResult(intent, 3);
                        }
                    }
                });
            }*/
        }
    }

    public boolean isStok() {
        for (int i = 0; i < stokOutletAdapter.getGroupCount(); i++) {
            for (int j = 0; j < stokOutletAdapter.getChildrenCount(i); j++) {
                StokCustomerModel orderDetailModel = stokOutletAdapter.getChild(i, j);
                if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private void noOrder() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("No Sales Reason");
        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_NO_SALES")));
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (customerModel.GeoLat == null || customerModel.GeoLat.equals("") || customerModel.GeoLong == null || customerModel.GeoLong.equals("")) {
                    selectItem(12);
                    return;
                }
                final ComboBoxModel coModel = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem();
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        commonModel.ROrderID = coModel.value;
                    }
                });
                progressDialog.setMessage("Check Out Data...");
                progressDialog.show();
//		    	new Checkout().execute();
                new CheckoutBackground().execute();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void close() {
        sharedPreferences.edit().putString("custId", "0").apply();
        sharedPreferences.edit().putBoolean("onResume", false).apply();
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan kunjungan?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    isHome = false;
                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<CommonModel> cmModel = realm.where(CommonModel.class).equalTo("Done", 0).findAll();
                            cmModel.deleteAllFromRealm();
                        }
                    });
//                    deleteDataCache();
                    /*if (getIntent().getBooleanExtra("isLoad", false)) {
                        startActivity(new Intent(KunjunganActivity.this, MenuUtamaActivity.class));
                    }*/

                    if (Util.getIntegerPreference(KunjunganActivity.this, "taking_from") == Util.ALL_CUSTOMER) {
                        Intent intent = new Intent(KunjunganActivity.this, AllCustomerActivity.class);
                        intent.putExtra("jenis", 1);
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(KunjunganActivity.this, ScheduleActivity.class));
                    }

                    finish();
                }
            }
        });
    }

    private void selectItem(int position) {

        boolean pass = false;

        if (position != 12) {
            pass = checkLocationCustomer();
            if (!pass) {
                position = 12;
            }
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 1:
                ft.replace(R.id.content_frame, informasiFragment);
                break;
            case 13:
                ft.replace(R.id.content_frame, noteFragment);
                break;
            case 14:
                ft.replace(R.id.content_frame, assetFragment);
                break;
            case 17:
                ft.replace(R.id.content_frame, assetMutasiFragment);
                break;
            case 18:
                ft.replace(R.id.content_frame, assetServiceFragment);
                break;
            case 3:
                ft.replace(R.id.content_frame, collectionFragment);
                break;
            case 12:
                ft.replace(R.id.content_frame, editCustomerFragment);
                break;
            case 10:
                ft.replace(R.id.content_frame, brandingFragment);
                break;
            case 4:
                ft.replace(R.id.content_frame, stokOutletFragment);
                break;
            case 5:
                ft.replace(R.id.content_frame, stokCompetitorFragment);
                break;
            case 8:
                ft.replace(R.id.content_frame, stokOnCarFragment);
                break;
            case 6:
                if (ctrlAppModul.isModul("58")) {
                    if (stokOutletFragment.getStokAdapter() == null) {
                        populateTakingOrder();
                        if (stokOutletAdapter != null) {
                            populateStok();
                        }
                        if (!isStok()) {
                            Util.showDialogInfo(this, "Isi Stok terlebih dahulu!");
                            mTitle = "Stock Outlet";
                            ft.replace(R.id.content_frame, stokOutletFragment);
                        } else {
                            takingOrderFragment = TakingOrderFragment.newInstance(false, false, 0);
//                    takingOrderFragment.setCanvas(false);
                            ft.replace(R.id.content_frame, takingOrderFragment);
                        }
                    } else if (!stokOutletFragment.isStok()) {
                        Util.showDialogInfo(this, "Isi Stok terlebih dahulu!");
                        mTitle = "Stock Outlet";
                        ft.replace(R.id.content_frame, stokOutletFragment);
                    } else {
                        takingOrderFragment = TakingOrderFragment.newInstance(false, false, 0);
//                    takingOrderFragment.setCanvas(false);
                        ft.replace(R.id.content_frame, takingOrderFragment);
                    }
                } else {
                    takingOrderFragment = TakingOrderFragment.newInstance(false, false, 0);
//                    takingOrderFragment.setCanvas(false);
                    ft.replace(R.id.content_frame, takingOrderFragment);
                }
                break;
            case 7:
                if (ctrlAppModul.isModul("58")) {
                    if (stokOutletFragment.getStokAdapter() == null) {
                        populateTakingOrder();
                        if (stokOutletAdapter != null) {
                            populateStok();
                        }
                        if (!isStok()) {
                            Util.showDialogInfo(this, "Isi Stok terlebih dahulu!");
                            mTitle = "Stock Outlet";
                            ft.replace(R.id.content_frame, stokOutletFragment);
                        }
                    } else if (!stokOutletFragment.isStok()) {
                        Util.showDialogInfo(this, "Isi Stok terlebih dahulu!");
                        mTitle = "Stock Outlet";
                        ft.replace(R.id.content_frame, stokOutletFragment);
                    } else {
                        takingOrderFragment = TakingOrderFragment.newInstance(true, false, 0);
//                    takingOrderFragment.setCanvas(false);
                        ft.replace(R.id.content_frame, takingOrderFragment);
                    }
                } else {
                    takingOrderFragment = TakingOrderFragment.newInstance(true, false, 0);
//                    takingOrderFragment.setCanvas(false);
                    ft.replace(R.id.content_frame, takingOrderFragment);
                }
                break;
            case 9:
                ft.replace(R.id.content_frame, returFragment);
                break;
            case 2:
                ft.replace(R.id.content_frame, historyPenjualanFragment);
//            historyPenjualanFragment.load();
                break;
            case 11:
                ft.replace(R.id.content_frame, surveyFragment);
                break;
            case 15:
                ft.replace(R.id.content_frame, stokGroupFragment);
                break;
            case 19:
                ft.replace(R.id.content_frame, capasityFragment);
                break;
            case 20:
                ft.replace(R.id.content_frame, salesTargetFragment);
                break;
            case 21:
                ft.replace(R.id.content_frame, priceMonitoringFragment);
                break;
            case 22:
                ft.replace(R.id.content_frame, merchandiserFragment);
                break;
            case 23:
                ft.replace(R.id.content_frame, crcFragment);
                break;
            case 24:
                ft.replace(R.id.content_frame, crcReportFragment);
                break;
            case 25:
                ft.replace(R.id.content_frame, takingOrder2Fragment);
                break;
            case 26:
                ft.replace(R.id.content_frame, collectionTirtaFinanceFragment);
                break;
            case 27:
                ft.replace(R.id.content_frame, takingOrder3Fragment);
                break;
        }
        ft.commit();
//        if(position == 12) {
//        	isEdit = true;
//        } else {
//        	if(isEdit) {
//        		isEdit = false;
//        		Util.confirmDialog(this, "Info", "Apakah perubahan data customer akan disimpan?", new ConfirmListener() {
//					@Override
//					public void onDialogCompleted(boolean answer) {
//						if(answer) {
//							editCustomerFragment.save();
//						}
//					}
//				});
//        	}
//        }

        if (pass)
            setTitle(mTitle);
        else
            setTitle("Edit Customer");

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        mTitle = Menuadapter.getItem(position).menu;
        selectItem(Menuadapter.getItem(position).id);
    }

    private void printFaktur() {
        isHome = false;
        /*Util.printCembayaran(this, bluePrint.PrinterService, commonModel, customerModel, new Util.PrintListener() {
            @Override
            public void printFinish() {
                finish();
            }

            @Override
            public void errorPrint(String message) {
                Util.okeDialog(KunjunganActivity.this, "Info", message, new OkListener() {
                    @Override
                    public void onDialogOk() {
                        finish();
                    }
                });
            }
        });*/
    }

    private void saveLokal(String tanggal) {
//		if(commonModel.EndVisitTime != null && commonModel.EndVisitTime.length() > 0) {
//			tanggal = commonModel.EndVisitTime;
//		}
        deleteDataCache();

//        commonModel.Description = noteFragment.getNote();

        /*CommonModel cmModel = takingOrderFragment.getCommonModel();

        if (cmModel == null) {
            cmModel = commonModel;
        }*/

        /*if (tanggal != null && !tanggal.equals("")) {
            Log.d("save done", "called");
            ctrlCommon.save(cmModel, 1);
        } else {
            Log.d("save undone", "called");
            ctrlCommon.save(cmModel, 0);
        }*/

        StokCustomerAdapter stokAdapter = stokOutletFragment.getStokAdapter();

        if (stokAdapter == null) {
            stokAdapter = stokOutletAdapter;
        }

        if (stokAdapter != null && stokAdapter.getGroupCount() > 0) {
            for (int i = 0; i < stokAdapter.getGroupCount(); i++) {
                if (stokAdapter.listOrderByGroup(stokAdapter.getGroup(i)) != null) {
                    for (int j = 0; j < stokAdapter.getChildrenCount(i); j++) {
                        StokCustomerModel stokCustomerModel = stokAdapter.getChild(i, j);
                        if (stokCustomerModel.QTY_K > 0 || stokCustomerModel.QTY_B > 0) {
                            ctrlStokOutlet.save(new StockOutletModel(
                                    customerModel.CustId,
                                    tanggal,
                                    stokCustomerModel.skuModel.SKUId,
                                    stokCustomerModel.QTY_B,
                                    stokCustomerModel.QTY_K,
                                    commonModel.CommonID,
                                    stokCustomerModel.RandomID
                            ));
                        }
                    }
                }
            }
            /*ctrlState.deleteStatebyModul(commonModel.CommonID, "Stok");
            ctrlState.save(commonModel.CommonID, "Stok");*/
        }

        StokCompetitorAdapter stokCompAdapter = stokCompetitorFragment.getStokCompAdapter();

        if (stokCompAdapter == null)
            stokCompAdapter = stokCompetitorAdapter;

        if (stokCompAdapter != null && stokCompAdapter.getGroupCount() > 0) {
            for (int i = 0; i < stokCompAdapter.getGroupCount(); i++) {
                for (int j = 0; j < stokCompAdapter.getChildrenCount(i); j++) {
                    StokCompetitorModel stokCompetitorModel = stokCompAdapter.getChild(i, j);
                    if (stokCompetitorModel.QTY_B > 0 || stokCompetitorModel.QTY_K > 0) {
                        ctrlStokCompetitor.save(new StockCompetitorModel(
                                customerModel.CustId,
                                tanggal,
                                stokCompetitorModel.skuModel.SKUId,
                                stokCompetitorModel.QTY_B,
                                stokCompetitorModel.QTY_K,
                                commonModel.CommonID,
                                stokCompetitorModel.PRICE,
                                stokCompetitorModel.PRICE2,
                                stokCompetitorModel.Note,
                                stokCompetitorModel.RandomID
                        ));

                    }
                }
            }
            /*ctrlState.deleteStatebyModul(commonModel.CommonID, "stokCompetitor");
            ctrlState.save(commonModel.CommonID, "stokCompetitor");*/
        }

        /*OrderDetailAdapter adapterAll = takingOrderFragment.getOrderAdapter();

        if (adapterAll == null) {
            adapterAll = takingOrderAdapter;
        }*/

        /*if (adapterAll != null && adapterAll.getGroupCount() > 0) {
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                if (adapterAll.listOrderByGroup(adapterAll.getGroup(i)) != null) {
                    for (int j = 0; j < adapterAll.getChildrenCount(i); j++) {
                        OrderDetailModel orderDetailModel = adapterAll.getChild(i, j);
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            ctrlTakingOrder.save(new TakingOrderModel(
                                    customerModel.CustId,
                                    orderDetailModel.skuModel.SKUId,
                                    tanggal,
                                    orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "",
                                    orderDetailModel.QTY_B,
                                    orderDetailModel.QTY_K,
                                    orderDetailModel.DISCOUNT1,
                                    orderDetailModel.DISCOUNT2,
                                    orderDetailModel.DISCOUNT3,
                                    orderDetailModel.TOTAL_PRICE,
                                    orderDetailModel.PAYMENT_TERM,
                                    orderDetailModel.note,
                                    orderDetailModel.PRICE_B,
                                    orderDetailModel.PRICE_K,
                                    orderDetailModel.LAMA_CREDIT,
                                    //						orderDetailModel.skuModel.DISCOUNT_TYPE,
                                    orderDetailModel.DiscountType,
                                    commonModel.CommonID,
                                    orderDetailModel.RandomID
                            ));
                        }
                    }
                }
            }*/
            /*ctrlState.deleteStatebyModul(commonModel.CommonID, "takingOrder");
            ctrlState.save(commonModel.CommonID, "takingOrder");*/


        /*OrderDetailAdapter adapterTakingOrder2 = takingOrder2Fragment.getAdapterMhs();

        if (adapterTakingOrder2 != null && adapterTakingOrder2.getGroupCount() > 0)

        {
            for (int i = 0; i < adapterTakingOrder2.getGroupCount(); i++) {
                if (adapterTakingOrder2.listOrderByGroup(adapterTakingOrder2.getGroup(i)) != null) {
                    for (int j = 0; j < adapterTakingOrder2.getChildrenCount(i); j++) {
                        OrderDetailModel orderDetailModel = adapterTakingOrder2.getChild(i, j);
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            ctrlTakingOrder.save(new TakingOrderModel(
                                    customerModel.CustId,
                                    orderDetailModel.skuModel.SKUId,
                                    tanggal,
                                    orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "",
                                    orderDetailModel.QTY_B,
                                    orderDetailModel.QTY_K,
                                    orderDetailModel.DISCOUNT1,
                                    orderDetailModel.DISCOUNT2,
                                    orderDetailModel.DISCOUNT3,
                                    orderDetailModel.TOTAL_PRICE,
                                    orderDetailModel.PAYMENT_TERM,
                                    orderDetailModel.note,
                                    orderDetailModel.PRICE_B,
                                    orderDetailModel.PRICE_K,
                                    orderDetailModel.LAMA_CREDIT,
                                    orderDetailModel.DiscountType,
                                    commonModel.CommonID,
                                    orderDetailModel.RandomID
                            ));
                        }
                    }
                }
            }
            *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "takingOrder2");
            ctrlState.save(commonModel.CommonID, "takingOrder2");*//*
        }

        adapterTakingOrder2 = takingOrder2Fragment.getAdapterNonMhs();
        if (adapterTakingOrder2 != null && adapterTakingOrder2.getGroupCount() > 0) {
            for (int i = 0; i < adapterTakingOrder2.getGroupCount(); i++) {
                if (adapterTakingOrder2.listOrderByGroup(adapterTakingOrder2.getGroup(i)) != null) {
                    for (int j = 0; j < adapterTakingOrder2.getChildrenCount(i); j++) {
                        OrderDetailModel orderDetailModel = adapterTakingOrder2.getChild(i, j);
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            ctrlTakingOrder.save(new TakingOrderModel(
                                    customerModel.CustId,
                                    orderDetailModel.skuModel.SKUId,
                                    tanggal,
                                    orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "",
                                    orderDetailModel.QTY_B,
                                    orderDetailModel.QTY_K,
                                    orderDetailModel.DISCOUNT1,
                                    orderDetailModel.DISCOUNT2,
                                    orderDetailModel.DISCOUNT3,
                                    orderDetailModel.TOTAL_PRICE,
                                    orderDetailModel.PAYMENT_TERM,
                                    orderDetailModel.note,
                                    orderDetailModel.PRICE_B,
                                    orderDetailModel.PRICE_K,
                                    orderDetailModel.LAMA_CREDIT,
                                    orderDetailModel.DiscountType,
                                    commonModel.CommonID,
                                    orderDetailModel.RandomID
                            ));
                        }
                    }
                }
            }
            *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "takingOrder2");
            ctrlState.save(commonModel.CommonID, "takingOrder2");*//*
        }*/

        /*PrinsipleAdapter prinsipleAdapter = takingOrder3Fragment.getAdapter();

        if (prinsipleAdapter == null) {
//            prinsipleAdapter = this.prinsipleAdapter;
        }

        if (prinsipleAdapter != null && prinsipleAdapter.getCount() > 0) {
            for (int i = 0; i < prinsipleAdapter.getCount(); i++) {
                PrinsipleModel prinsipleModel = prinsipleAdapter.getItem(i);
//				if(prinsipleModel.total_price > 0) {
                if (prinsipleModel.list != null) {
                    Log.d("Principle QTY b", prinsipleModel.total_qty_b + "");
                    Log.d("Principle QTY k", prinsipleModel.total_qty_k + "");
                    ctrlTakingOrder.savePrinsiple(prinsipleModel.toPrinsipleOrderModel(commonModel.CommonID));
                    for (TakingOrderModel takingOrderModel : prinsipleModel.list) {
                        takingOrderModel.tanggal = tanggal;
                        takingOrderModel.CommonID = commonModel.CommonID;
                        ctrlTakingOrder.save(takingOrderModel);
                    }
                }
                if (prinsipleModel.listPromo != null) {
                    for (DiscountPromoModel model : prinsipleModel.listPromo) {
                        model.CommonID = commonModel.CommonID;
                        ctrlDiscountPromo.save(model);
                    }
                }
                if (prinsipleModel.listPromoResult != null) {
                    for (DiscountResultModel model : prinsipleModel.listPromoResult) {
                        model.CommonID = commonModel.CommonID;
                        ctrlDiscountResult.save(model);
                    }
                }
//				}
            }
            *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "prisiple");
            ctrlState.save(commonModel.CommonID, "prinsiple");*//*
        }*/
        //----crc

        if (orderCrcAdapter != null && orderCrcAdapter.getGroupCount() > 0) {
            for (int i = 0; i < orderCrcAdapter.getGroupCount(); i++) {
                if (orderCrcAdapter.listOrderByGroup(orderCrcAdapter.getGroup(i)) != null) {
                    for (int j = 0; j < orderCrcAdapter.getChildrenCount(i); j++) {
                        OrderDetailModel orderDetailModel = orderCrcAdapter.getChild(i, j).order;
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            ctrlTakingOrder.save(new TakingOrderModel(
                                    customerModel.CustId,
                                    orderDetailModel.skuModel.SKUId,
                                    tanggal,
                                    orderDetailModel.salesProgramModel.SalesProgramId,
                                    orderDetailModel.QTY_B,
                                    orderDetailModel.QTY_K,
                                    orderDetailModel.DISCOUNT1,
                                    orderDetailModel.DISCOUNT2,
                                    orderDetailModel.DISCOUNT3,
                                    orderDetailModel.TOTAL_PRICE,
                                    orderDetailModel.PAYMENT_TERM,
                                    orderDetailModel.note,
                                    orderDetailModel.PRICE_B,
                                    orderDetailModel.PRICE_K,
                                    orderDetailModel.LAMA_CREDIT,
                                    //						orderDetailModel.skuModel.DISCOUNT_TYPE,
                                    orderDetailModel.DiscountType,
                                    commonModel.CommonID,
                                    orderDetailModel.RandomID
                            ));
                        }
                        StokCustomerModel stokCustomerModel = orderCrcAdapter.getChild(i, j).stock;
                        if (stokCustomerModel.QTY_K > 0 || stokCustomerModel.QTY_B > 0) {
                            ctrlStokOutlet.save(new StockOutletModel(
                                    customerModel.CustId,
                                    tanggal,
                                    stokCustomerModel.skuModel.SKUId,
                                    stokCustomerModel.QTY_B,
                                    stokCustomerModel.QTY_K,
                                    commonModel.CommonID,
                                    stokCustomerModel.RandomID
                            ));
                        }
                    }
                }
            }
            /*ctrlState.deleteStatebyModul(commonModel.CommonID, "crc");
            ctrlState.save(commonModel.CommonID, "crc");*/
        }

        /*ReturDetailAdapter returAdapter = returFragment.getReturAdapter();

        if (returAdapter == null)

        {
            returAdapter = returDetailAdapter;
        }

        if (returAdapter != null && returAdapter.getGroupCount() > 0) {
            for (int i = 0; i < returAdapter.getGroupCount(); i++) {
                if (returAdapter.listOrderByGroup(returAdapter.getGroup(i)) != null) {
                    for (int j = 0; j < returAdapter.getChildrenCount(i); j++) {
                        ReturDetailModel returDetailModel = returAdapter.getChild(i, j);
                        if (returDetailModel.QTY_B > 0 || returDetailModel.QTY_K > 0) {
                            ctrlRetur.save(new ReturModel(
                                    customerModel.CustId,
                                    returDetailModel.skuModel.SKUId,
                                    tanggal,
                                    returDetailModel.QTY_B,
                                    returDetailModel.QTY_K,
                                    returDetailModel.note,
                                    returDetailModel.tipe.value,
                                    commonModel.CommonID,
                                    returDetailModel.RandomID
                            ));
                        }
                    }
                }
            }
        }*/

        if (stokGroupAdapter != null && stokGroupAdapter.getCount() > 0) {
            for (int i = 0; i < stokGroupAdapter.getCount(); i++) {
                StockGroupModel model = stokGroupAdapter.getItem(i);
                if (model.Quantity > 0 || model.Quantity2 > 0) {
                    ctrlStockGroup.save(model);
                }
            }
            /*ctrlState.deleteStatebyModul(commonModel.CommonID, "stokGroup");
            ctrlState.save(commonModel.CommonID, "stokGroup");*/
        }

        try {
//			JSONObject jsonDiskon = takingOrderFragment.getDiskonPromo();
//			if(jsonDiskon != null) {
//				int total = takingOrderFragment.getDiskonPromoAdapter().getTotal();
//				if(total > 0) {
//					JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='"+jsonDiskon.getString("DISCOUNT_PROMO_ID")+"'");
//					ctrlDiscountPromo.save(new DiscountPromoModel(jsonDiskon.getInt("DISCOUNT_PROMO_ID"), Integer.parseInt(customerModel.CustId), commonModel.CommonID));
//					ctrlDiscountResult.save(new DiscountResultModel(
//						Integer.parseInt(customerModel.CustId),
//						jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID"),
//						total,
//						commonModel.CommonID
//					));
//				}
//			}
            /*if (takingOrderFragment.getDiskonAdapter() != null) {
                for (int i = 0; i < takingOrderFragment.getDiskonAdapter().getCount(); i++) {
                    JSONObject promo = takingOrderFragment.getDiskonAdapter().getItem(i);
                    if (promo.has("isChecked") && promo.getBoolean("isChecked")) {
                        ctrlDiscountPromo.save(new DiscountPromoModel(promo.getInt("DISCOUNT_PROMO_ID"), Integer.parseInt(customerModel.CustId), commonModel.CommonID*//*, promo.getString("RandomID")*//*));
                    }
                }
            }
            if (takingOrderFragment.getDiskonPromoAdapter() != null) {
                for (int i = 0; i < takingOrderFragment.getDiskonPromoAdapter().getGroupCount(); i++) {
                    for (int j = 0; j < takingOrderFragment.getDiskonPromoAdapter().getChildrenCount(i); j++) {
                        DiskonPromoModel model = takingOrderFragment.getDiskonPromoAdapter().getChild(i, j);
                        ctrlDiscountResult.save(new DiscountResultModel(
                                Integer.parseInt(customerModel.CustId),
                                model.DiscountResultID,
                                model.QTY,
                                commonModel.CommonID,
                                model.RandomID
                        ));
                    }
                }
            }*/
        } catch (Exception e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
        }
//		if(!collectorModelPayment.data1.equals("") && !collectorModelPayment.data2.equals("") /*&& !collectorModelPayment.data3.equals("")*/) {
//			ctrlCollector.save(collectorModelPayment);
//		} else if(!collectorModelNotPayment.data1.equals("") && !collectorModelNotPayment.data2.equals("") /*&& !collectorModelNotPayment.data3.equals("")*/) {
//			ctrlCollector.save(collectorModelNotPayment);
//		}
//		for (CollectorModel model : collectionFragment.getLstPay()) {
//			model.tanggal = tanggal;
//			ctrlCollector.save(model);
//		}

        /*if (collectionFragment != null && collectionFragment.payAdapter != null) {
            for (int i = 0; i < collectionFragment.payAdapter.getCount(); i++) {
                CollectorModel model = collectionFragment.payAdapter.getItem(i);
                model.tanggal = tanggal;
                ctrlCollector.save(model);
            }
            *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "collection");
            ctrlState.save(commonModel.CommonID, "collection");*//*
        } else {
            if (collectionPayAdapter != null) {
                for (int i = 0; i < collectionPayAdapter.getCount(); i++) {
                    CollectorModel model = collectionPayAdapter.getItem(i);
                    model.tanggal = tanggal;
                    ctrlCollector.save(model);
                }
                *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "collection");
                ctrlState.save(commonModel.CommonID, "collection");*//*
            }
        }*/

        /*if (collectionFragment != null && collectionFragment.collectorModelNotPayment != null)

        {
            if (!collectionFragment.collectorModelNotPayment.data1.equals("") && !collectionFragment.collectorModelNotPayment.data2.equals("")) {
                collectionFragment.collectorModelNotPayment.tanggal = tanggal;
                ctrlCollector.save(collectionFragment.collectorModelNotPayment);
                *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "collection");
                ctrlState.save(commonModel.CommonID, "collection");*//*
            }
        } else

        {
            if (collectorModelNotPayment != null) {
                if (!collectorModelNotPayment.data1.equals("") && !collectorModelNotPayment.data2.equals("")) {
                    collectorModelNotPayment.tanggal = tanggal;
                    ctrlCollector.save(collectionFragment.collectorModelNotPayment);
                    *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "collection");
                    ctrlState.save(commonModel.CommonID, "collection");*//*
                }
            }
        }*/

        /*if (ctrlAppModul.isModul("33"))

        {//asset
            try {
                AssetAdapter assetAdapter = assetFragment.getAdapter();

                if (assetAdapter != null) {

                    Log.d("AssetAdapter is NULL", "NO");

                    ctrlCommon.deleteAssetData(commonModel.CommonID);

                    for (int i = 0; i < assetAdapter.getCount(); i++) {
                        AssetModel assetModel = assetAdapter.getItem(i);
                        for (int j = 0; j < assetModel.getAssetWidget().getAdapter().getCount(); j++) {
                            AssetWidget.AssetView assetView = (AssetView) assetModel.getAssetWidget().getAdapter().get(j);
                            for (AssetDetailWidget assetDetail : assetView.getListWidget()) {
                                if (assetDetail.isType5()) {
                                    RadioGroup rg = (RadioGroup) assetDetail.findViewById(R.id.radioGroup);
                                    for (int k = 0; k < rg.getChildCount(); k++) {
                                        View view = rg.getChildAt(k);
                                        if (view instanceof AssetType5Widget) {
                                            AssetType5Widget asset = (AssetType5Widget) view;
                                            JSONObject json = asset.toJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "5");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                        }
                                    }
                                } else {
                                    LinearLayout layout = (LinearLayout) assetDetail.findViewById(R.id.layout_detail);
                                    for (int k = 0; k < layout.getChildCount(); k++) {
                                        View view = layout.getChildAt(k);
                                        if (view instanceof AssetType1Widget) {
                                            AssetType1Widget asset = (AssetType1Widget) view;
                                            JSONObject json = asset.toJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "1");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                        } else if (view instanceof AssetType2Widget) {
                                            AssetType2Widget asset = (AssetType2Widget) view;
                                            JSONObject json = asset.toJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "2");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                        } else if (view instanceof AssetType3Widget) {
                                            AssetType3Widget asset = (AssetType3Widget) view;
                                            JSONObject json = asset.toJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "3");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                        } else if (view instanceof AssetType4Widget) {
                                            AssetType4Widget asset = (AssetType4Widget) view;
                                            JSONObject json = asset.toSaveJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "4");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                            asset.saveFile();
                                        } else if (view instanceof AssetType6Widget) {
                                            AssetType6Widget asset = (AssetType6Widget) view;
                                            JSONObject json = asset.toJSON();
//                                            json.put("CommonID", commonModel.CommonID);
                                            json.put("jenisWidget", "6");
                                            json.put("tipe", "1");
                                            databaseHelper.save(json, "Aset");
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Log.d("Asset Adapter is NULL", "YES");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        /*if (ctrlAppModul.isModul("37"))

        {//asset mutasi
            try {
                AssetMutasiAdapter assetMutasiAdapter = assetMutasiFragment.getAdapter();
                if (assetMutasiAdapter != null) {

                    ctrlCommon.deleteAssetMutasiData(commonModel.CommonID);
                    ctrlCommon.deleteAssetMutasiData(commonModel.CommonID, "3");

                    for (int i = 0; i < assetMutasiAdapter.getCount(); i++) {
                        AssetMutasiModel assetMutasiModel = assetMutasiAdapter.getItem(i);
                        if (assetMutasiModel.getAssetMutasiWidget().getJenis() == 1) {//pemasangan
                            JSONObject json = assetMutasiModel.getAssetMutasiWidget().toJSON();
                            json.put("CommonID", commonModel.CommonID);
                            databaseHelper.save(json, "Newasset");
                        } else if (assetMutasiModel.getAssetMutasiWidget().getJenis() == 2) {//penarikan
                            JSONObject json = assetMutasiModel.getAssetMutasiWidget().toJSON();
                            json.put("CommonID", commonModel.CommonID);
                            json.put("jenisWidget", "6");
                            json.put("tipe", "3");
                            databaseHelper.save(json, "Aset");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (ctrlAppModul.isModul("38"))*/

        /*{//asset service
            try {
                AssetServiceAdapter assetServiceAdapter = assetServiceFragment.getAdapter();
                if (assetServiceAdapter != null) {
                    for (int i = 0; i < assetServiceAdapter.getCount(); i++) {
                        AssetServiceModel assetServiceModel = assetServiceAdapter.getItem(i);
                        List<AssetDetailWidget> list = assetServiceModel.getAssetWidgetService().getListWidget();
                        for (AssetDetailWidget assetDetailWidget : list) {
                            if (assetDetailWidget.isType5()) {
                                RadioGroup rg = (RadioGroup) assetDetailWidget.findViewById(R.id.radioGroup);
                                for (int k = 0; k < rg.getChildCount(); k++) {
                                    View view = rg.getChildAt(k);
                                    if (view instanceof AssetType5Widget) {
                                        AssetType5Widget asset = (AssetType5Widget) view;
                                        JSONObject json = asset.toJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "5");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                    }
                                }
                            } else {
                                LinearLayout layout = (LinearLayout) assetDetailWidget.findViewById(R.id.layout_detail);
                                for (int k = 0; k < layout.getChildCount(); k++) {
                                    View view = layout.getChildAt(k);
                                    if (view instanceof AssetType1Widget) {
                                        AssetType1Widget asset = (AssetType1Widget) view;
                                        JSONObject json = asset.toJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "1");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                    } else if (view instanceof AssetType2Widget) {
                                        AssetType2Widget asset = (AssetType2Widget) view;
                                        JSONObject json = asset.toJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "2");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                    } else if (view instanceof AssetType3Widget) {
                                        AssetType3Widget asset = (AssetType3Widget) view;
                                        JSONObject json = asset.toJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "3");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                    } else if (view instanceof AssetType4Widget) {
                                        AssetType4Widget asset = (AssetType4Widget) view;
                                        JSONObject json = asset.toSaveJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "4");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                        asset.saveFile();
                                    } else if (view instanceof AssetType6Widget) {
                                        AssetType6Widget asset = (AssetType6Widget) view;
                                        JSONObject json = asset.toJSON();
                                        json.put("CommonID", commonModel.CommonID);
                                        json.put("jenisWidget", "6");
                                        json.put("tipe", "4");
                                        databaseHelper.save(json, "Aset");
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        /*if (ctrlAppModul.isModul("43"))

        {//Merchandiser
            try {
                MerchandiserAdapter merchandiserAdapter;

                if (merchandiserFragment != null && merchandiserFragment.getAdapter() != null) {
                    merchandiserAdapter = merchandiserFragment.getAdapter();
                    Log.d("Merchand F not null", merchandiserAdapter.getCount() + "get from fragment");
                } else {
                    merchandiserAdapter = this.merchandiserAdapter;
                    Log.d("Merchand not null", merchandiserAdapter.getCount() + "");
                }

                if (merchandiserAdapter != null && merchandiserAdapter.getCount() > 0) {
                    for (int i = 0; i < merchandiserAdapter.getCount(); i++) {
                        MerchandiserModel merchandiserModel = merchandiserAdapter.getItem(i);
                        if (merchandiserModel.isEntry) {
                            for (int j = 0; j < merchandiserModel.getAssetWidget().getAdapter().getCount(); j++) {
                                AssetWidget.AssetView assetView = (AssetView) merchandiserModel.getAssetWidget().getAdapter().get(j);
                                if (assetView.getListWidget() != null) {
                                    for (AssetDetailWidget assetDetail : assetView.getListWidget()) {
                                        if (assetDetail.isType5()) {
                                            RadioGroup rg = (RadioGroup) assetDetail.findViewById(R.id.radioGroup);
                                            for (int k = 0; k < rg.getChildCount(); k++) {
                                                View view = rg.getChildAt(k);
                                                if (view instanceof AssetType5Widget) {
                                                    AssetType5Widget asset = (AssetType5Widget) view;
                                                    JSONObject json = asset.toJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "5");
                                                    json.put("tipe", "5");
                                                    databaseHelper.save(json, "Aset");
                                                }
                                            }
                                        } else {
                                            LinearLayout layout = (LinearLayout) assetDetail.findViewById(R.id.layout_detail);
                                            for (int k = 0; k < layout.getChildCount(); k++) {
                                                View view = layout.getChildAt(k);
                                                if (view instanceof AssetType1Widget) {
                                                    AssetType1Widget asset = (AssetType1Widget) view;
                                                    JSONObject json = asset.toJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "1");
                                                    json.put("tipe", "5");
                                                    databaseHelper.save(json, "Aset");
                                                } else if (view instanceof AssetType2Widget) {
                                                    AssetType2Widget asset = (AssetType2Widget) view;
                                                    JSONObject json = asset.toJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "2");
                                                    json.put("tipe", "5");
                                                    databaseHelper.save(json, "Aset");
                                                } else if (view instanceof AssetType3Widget) {
                                                    AssetType3Widget asset = (AssetType3Widget) view;
                                                    JSONObject json = asset.toJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "2");
                                                    json.put("tipe", "5");
                                                    databaseHelper.save(json, "Aset");
                                                } else if (view instanceof AssetType4Widget) {
                                                    android.util.Log.e("kesini", "kesini");
                                                    AssetType4Widget asset = (AssetType4Widget) view;
                                                    JSONObject json = asset.toSaveJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "4");
                                                    json.put("tipe", "5");
                                                    android.util.Log.e("json", "" + json.toString());
                                                    databaseHelper.save(json, "Aset");
                                                    asset.saveFile();
                                                } else if (view instanceof AssetType6Widget) {
                                                    AssetType6Widget asset = (AssetType6Widget) view;
                                                    JSONObject json = asset.toJSON();
                                                    json.put("CommonID", commonModel.CommonID);
                                                    json.put("jenisWidget", "6");
                                                    json.put("tipe", "5");
                                                    databaseHelper.save(json, "Aset");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
//                    ctrlState.save(commonModel.CommonID, "Merchandise");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        if (ctrlAppModul.isModul("41"))

        { // target
            try {
                for (int i = 0; i < targetAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < targetAdapter.getChildrenCount(i); j++) {
                        TargetModel targetModel = targetAdapter.getChild(i, j);
                        for (TargetModel.Detail detail : targetModel.details) {
                            if (detail.Price > 0 || detail.Qty > 0) {
                                JSONObject data = detail.toJson(commonModel.CommonID);
//								data.put("CommonID", commonModel.CommonID);
                                databaseHelper.save(data, "TargetProduk");
                            }
                        }
                    }
                }

                if (targetAdapter != null && targetAdapter.getGroupCount() > 0) {
                    /*ctrlState.deleteStatebyModul(commonModel.CommonID, "target");
                    ctrlState.save(commonModel.CommonID, "target");*/
                }

                for (int i = 0; i < targetComAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < targetComAdapter.getChildrenCount(i); j++) {
                        TargetComModel targetModel = targetComAdapter.getChild(i, j);
                        for (TargetComModel.Detail detail : targetModel.details) {
                            if (detail.Price > 0 || detail.Qty > 0) {
                                JSONObject data = detail.toJson(commonModel.CommonID);
//								data.put("CommonID", commonModel.CommonID);
                                databaseHelper.save(data, "TargetProduk");
                            }
                        }
                    }
                }

                if (targetComAdapter != null && targetComAdapter.getGroupCount() > 0) {
                    /*ctrlState.deleteStatebyModul(commonModel.CommonID, "targetComp");
                    ctrlState.save(commonModel.CommonID, "targetComp");*/
                }
            } catch (Exception e) {
//                FirebaseCrash.log("Checked - " + e.getMessage());
                e.printStackTrace();
            }
        }
        if (ctrlAppModul.isModul("42"))

        { //price monitoring
            try {
                if (priceMonitoringFragment != null && priceMonitoringFragment.getListMonitoringPrice() != null) {
                    priceListMonitoringAdapter = priceMonitoringFragment.getListMonitoringPrice();
                    Log.wtf("Price List F !null", "" + priceListMonitoringAdapter.getGroupCount());

                    /*if (priceListMonitoringAdapter.getGroupCount() > 0) {
                        *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "priceMonitoring");
                        ctrlState.save(commonModel.CommonID, "priceMonitoring");*//*
                    }

                    for (int i = 0; i < priceListMonitoringAdapter.getGroupCount(); i++) {
                        for (int j = 0; j < priceListMonitoringAdapter.getChildrenCount(i); j++) {
                            PriceMonitoringModel priceMonitoringModel = priceListMonitoringAdapter.getChild(i, j);
                            if (priceMonitoringModel.BuyPrice > 0 || priceMonitoringModel.SellPrice > 0 || priceMonitoringModel.Qty > 0) {
                                JSONObject data = priceMonitoringModel.toJson(commonModel.CommonID);
//							data.put("CommonID", commonModel.CommonID);
                                databaseHelper.save(data, "PriceMonitoring");
                            }
                        }
                    }*/
                } else {
                    if (priceMonitoringFragment != null && priceMonitoringFragment.getPriceMonitoringProdukAdapter() != null) {
                        priceMonitoringProdukAdapter = priceMonitoringFragment.getPriceMonitoringProdukAdapter();
                        Log.d("F !null", "" + priceMonitoringProdukAdapter.getGroupCount());
                    }

                    /*if (priceMonitoringProdukAdapter.getGroupCount() > 0) {
                        *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "priceMonitoring");
                        ctrlState.save(commonModel.CommonID, "priceMonitoring");*//*
                    }*/

                    /*Log.wtf("Price Produk F !null", "" + priceMonitoringProdukAdapter.getGroupCount());

                    for (int i = 0; i < priceMonitoringProdukAdapter.getGroupCount(); i++) {
                        for (int j = 0; j < priceMonitoringProdukAdapter.getChildrenCount(i); j++) {
                            PriceMonitoringModel priceMonitoringModel = priceMonitoringProdukAdapter.getChild(i, j);
                            if (priceMonitoringModel.BuyPrice > 0 || priceMonitoringModel.SellPrice > 0 || priceMonitoringModel.Qty > 0) {
                                JSONObject data = priceMonitoringModel.toJson(commonModel.CommonID);
//							data.put("CommonID", commonModel.CommonID);
                                databaseHelper.save(data, "PriceMonitoring");
                            }
                        }
                    }

                    if (priceMonitoringFragment != null && priceMonitoringFragment.getPriceMonitoringCompetitorAdapter() != null) {
                        priceMonitoringCompetitorAdapter = priceMonitoringFragment.getPriceMonitoringCompetitorAdapter();
                        Log.d("F !null", "" + priceMonitoringProdukAdapter.getGroupCount());
                    }

                    if (priceMonitoringCompetitorAdapter.getGroupCount() > 0) {
                        *//*ctrlState.deleteStatebyModul(commonModel.CommonID, "priceMonitoring");
                        ctrlState.save(commonModel.CommonID, "priceMonitoring");*//*
                    }
                    Log.wtf("Price Comp F !null", "" + priceMonitoringCompetitorAdapter.getGroupCount());

                    for (int i = 0; i < priceMonitoringCompetitorAdapter.getGroupCount(); i++) {
                        for (int j = 0; j < priceMonitoringCompetitorAdapter.getChildrenCount(i); j++) {
                            PriceMonitoringModel priceMonitoringModel = priceMonitoringCompetitorAdapter.getChild(i, j);
                            if (priceMonitoringModel.BuyPrice > 0 || priceMonitoringModel.SellPrice > 0 || priceMonitoringModel.Qty > 0) {
                                JSONObject data = priceMonitoringModel.toJson(commonModel.CommonID);
//							data.put("CommonID", commonModel.CommonID);
                                databaseHelper.save(data, "PriceMonitoring");
                            }
                        }
                    }*/
                }
            } catch (Exception e) {
//                FirebaseCrash.log("Checked - " + e.getMessage());
                e.printStackTrace();
            }
        }
        try

        {
            if (visitNote != null) {
                for (int i = 0; i < visitNote.length(); i++) {
                    databaseHelper.save(visitNote.getJSONObject(i), "VisitNote");
                }
            }
        } catch (Exception e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
        }
    }


    private void deleteDataCache() {
//        CommonModel model = ctrlCommon.getCommonModel(commonModel.CommonID);
      /*  if (model != null) {
            Log.d("CommonModel", "Deleted");
//			ctrlCommon.deleteByCommonID(model.CommonID);
//			ctrlStokOutlet.deleteByCommonID(model.CommonID);
//			ctrlStokCompetitor.deleteByCommonID(model.CommonID);
//			ctrlTakingOrder.deleteByCommonID(model.CommonID);
//			ctrlRetur.deleteByCommonID(model.CommonID);
//			ctrlCollector.deleteByCommonID(model.CommonID);
            ctrlCommon.deleteAllDataPending(commonModel.CommonID);
        }*/
    }

    @Override
    public void takePicture(ItemPhotoCustomerWidget widget) {
        this.widget = widget;
//		Intent intent = new Intent(this, PhotoActivity.class);
        isHome = false;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    @Override
    public void send(ItemPhotoCustomerWidget widget) {
        editCustomerFragment.send(widget);
    }

    @Override
    public void delete(final ItemPhotoCustomerWidget widget) {
//		Util.confirmDialog(this, "Info", "Apakah photo ingin dihapus?", new ConfirmListener() {
//			@Override
//			public void onDialogCompleted(boolean answer) {
//				if(answer) {
//					editCustomerFragment.remove(widget);
//				}
//			}
//		});
        editCustomerFragment.delete(widget);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
//				byte[] data = intent.getByteArrayExtra("data");
//				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
//					bitmap = Bitmap.createScaledBitmap(bitmap, Util.Width, Util.Height, false);
//					bitmap = ScalingUtilities.createScaledBitmap(bitmap, Util.Width, Util.Height, ScalingUtilities.ScalingLogic.FIT);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    widget.setImage(bitmap);
                    widget.setData(bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
//                    widget.saveKeLokal();
                } catch (Exception e) {
//                    FirebaseCrash.log("Checked - " + e.getMessage());
                    e.printStackTrace();
                    Util.showDialogError(this, e.getMessage());
                }

            }
        } else if (requestCode == 2) {
            brandingFragment.needToSave = true;
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
//					bitmap = Bitmap.createScaledBitmap(bitmap, Util.Width, Util.Height, false);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    brandingWidget.setImage(bitmap);
                    brandingWidget.setData(bao.toByteArray());
                    bao.flush();
                    bao.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showDialogError(this, e.getMessage());
                }
            }
        } else if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                try {
                    String sjson = intent.getStringExtra("data");
                    android.util.Log.e("sjson", "" + sjson + " visit...");
                    visitNote = new JSONArray(sjson);
                    chekOutOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void takePicture(ItemPhotoCustomerBrandingWidget widget) {
        this.brandingWidget = widget;
        isHome = false;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        brandingFragment.needToSave = false;
        startActivityForResult(intent, 2);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        Log.d("Fragment Attached", "Called");
    }

    @Override
    public void send(ItemPhotoCustomerBrandingWidget widget) {
        brandingFragment.remove(widget, false);
    }

    @Override
    public void delete(final ItemPhotoCustomerBrandingWidget widget) {
        Util.confirmDialog(this, "Info", "Apakah photo ingin dihapus?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    final Realm realmO = Realm.getDefaultInstance();
                    try {
                        realmO.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.where(CustomerBrandingModel.class)
                                        .equalTo("RandomID", widget.brandingModel.RandomID)
                                        .findFirst().deleteFromRealm();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    brandingFragment.remove(widget, true);
                    realmO.close();
                }
            }
        });
    }

    private String getNoFaktur() {
        String noFaktur = UserModel.getInstance(this).FormatInv;
        String bintang = "";
        int jml = 0;
        for (int i = 0; i < noFaktur.length(); i++) {
            if (noFaktur.charAt(i) == '*') {
                bintang += "\\*";
                jml++;
            }
        }
        int LastInv = Util.getIntegerPreference(this, "LastInv") + 1;
        String fLastInv = "";
        for (int i = 0; i < (jml - String.valueOf(LastInv).length()); i++) {
            fLastInv += "0";
        }
        fLastInv = fLastInv + String.valueOf(LastInv);
        Date date = new Date();
        SimpleDateFormat blnForrmat = new SimpleDateFormat("MM");
        SimpleDateFormat thnForrmat = new SimpleDateFormat("yyyy");
        String bln = "";
        switch (Integer.parseInt(blnForrmat.format(date))) {
            case 1:
                bln = "I";
                break;
            case 2:
                bln = "II";
                break;
            case 3:
                bln = "III";
                break;
            case 4:
                bln = "IV";
                break;
            case 5:
                bln = "V";
                break;
            case 6:
                bln = "VI";
                break;
            case 7:
                bln = "VII";
                break;
            case 8:
                bln = "VIII";
                break;
            case 9:
                bln = "IX";
                break;
            case 10:
                bln = "X";
                break;
            case 11:
                bln = "XI";
                break;
            case 12:
                bln = "XII";
                break;
        }
//		noFaktur = String.valueOf(LastInv) + "/" + bln + "/" + thnForrmat.format(date);
        noFaktur = noFaktur.replaceAll("%SALESID%", UserModel.getInstance(this).SalesId);
        noFaktur = noFaktur.replaceAll("%TAHUN%", thnForrmat.format(date));
        noFaktur = noFaktur.replaceAll("%BULANR%", bln);
        noFaktur = noFaktur.replaceAll(bintang, fLastInv);
        Util.putPreference(this, "LastInv", LastInv);
        return noFaktur;
    }


    public NewEditCustomerFragment getEditCustomerFragment() {
        return editCustomerFragment;
    }

    public class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            Log.d("Calling Adapter", "Background");
            String result = null;
            try {
                populateCollector();
                populateTakingOrder();
                populateStokCompetitor();
                note = commonModel.Description;
//		        ctrlCommon.deleteAllDataPending(commonModel.CommonID);
            } catch (Exception e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Calling Adapter", "Done : Taking Order : " + takingOrderAdapter.getGroupCount());
//            takingOrderFragment.setAdapter(takingOrderAdapter);
//            takingOrder2Fragment.setAdapter(takingOrderAdapter);
//            stokOutletFragment.setAdapter(stokOutletAdapter);
//            stokCompetitorFragment.setAdapter(stokCompetitorAdapter);
//            stokOnCarFragment.setAdapter(stokOnCarAdapter);
//            returFragment.setAdapter(returDetailAdapter);
//            stokGroupFragment.setAdapter(stokGroupAdapter);

            capasityFragment.setAdapter(capasityAdapter);
            targetFragment.setAdapter(targetAdapter);
//            priceMonitoringFragment.setPriceMonitoringCompetitorAdapter(priceMonitoringCompetitorAdapter);
//            priceMonitoringFragment.setPriceMonitoringProdukAdapter(priceMonitoringProdukAdapter);
//            crcFragment.setAdapter(orderCrcAdapter);
            crcReportFragment.setAdapter(crcReportAdapter);
            targetComFragment.setAdapter(targetComAdapter);

            progressDialog.dismiss();

            if (savedInstance != null) {
                Log.d("GOTO", getSharedPreferences("SADIX", MODE_PRIVATE).getInt("last_visit", 0) + "");
//                selectItem(getSharedPreferences("SADIX", MODE_PRIVATE).getInt("last_visit", 0));
            }

            if (result != null) {
                Util.showDialogInfo(KunjunganActivity.this, result);
            }

            checkLocationCustomer();

            super.onPostExecute(result);
        }

    }

    private boolean checkLocationCustomer() {
        boolean pass = false;

        Log.d("customer Long", customerModel.GeoLong);
        Log.d("customer Lat", customerModel.GeoLat);

        if (customerModel.GeoLong == null || customerModel.GeoLong.equals("0") ||
                customerModel.GeoLat == null || customerModel.GeoLat.equals("0")) {

            progressDialog.dismiss();
            Util.showDialogInfo(this, "Lokasi customer belum dilengkapi, harap lengkapi terlebih dahulu!");
            selectItem(12);
        } else {
            progressDialog.dismiss();
            pass = true;
        }

        return pass;
    }

    private void populateCollector() {
        final Realm realmO = Realm.getDefaultInstance();
        realmO.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                List<CollectionModel> collectorModels = realmO.copyFromRealm(realmO.where(CollectionModel.class)
                        .equalTo("CommonID", commonModel.CommonID)
                        .findAll());

                collectionPayAdapter = new CollectionPayAdapter(KunjunganActivity.this, collectorModels) {

                    @Override
                    public void updateRemove() {
                        collectionTirtaFinanceFragment.totalBayar();
                    }
                };
            }
        });

        realmO.close();

    }

    private class CheckoutBackground extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            final String tanggal = Util.dateFormat.format(new Date());
            try {
                collectionFragment.setData();

                final Realm realmO = Realm.getDefaultInstance();
//                final CommonModel cmModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
                realmO.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel cmModel = realmO.where(CommonModel.class).equalTo("Done", 0).findFirst();

                        cmModel.EndVisitTime = tanggal;
                        cmModel.GeoLat = location != null ? String.format("%1$s", location.getLatitude()) : "0";
                        cmModel.GeoLong = location != null ? String.format("%1$s", location.getLongitude()) : "0";
                        cmModel.LogTime = Util.dateFormat.format(location != null ? new Date(location.getTime()) : new Date());
                        if (takingOrderFragment.isCanvas()) {
                            cmModel.InvNO = getNoFaktur();
                            Log.d("Invoice", cmModel.InvNO);
                        } else {
                            cmModel.InvNO = "";
                        }
                        cmModel.Done = 1;
                        if (cmModel.Description == null || cmModel.Description.trim().equals("")) {
                            cmModel.Description = noteFragment.getNote();
                        }

                        cmModel.Status = "0";

                        RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModel = cmModel.takingOrderModel;
                        RealmList<ReturnModel> returnModel = cmModel.returnModel;
                        RealmList<CollectionModel> collectionModel = cmModel.collectionModel;
                        RealmList<StockCustomerModel> stockCustomerModel = cmModel.stockCustomerModel;
                        RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModel = cmModel.stockCompetitorModel;

                        Log.d("TO Size", takingOrderModel.size() + "");
                        ListIterator<com.bhn.sadix.Data.TakingOrderModel> it = takingOrderModel.listIterator();

                        while (it.hasNext()) {
                            com.bhn.sadix.Data.TakingOrderModel model = it.next();
                            model.tanggal = tanggal;

                            if (Util.getStringPreference(KunjunganActivity.this, "CallST").equals("22")) {
                                SKUModel skuModel = ctrlSKU.get(model.SKUId);

                                if (model.QTY_B > 0) {
                                    skuModel.QBESAR -= model.QTY_B;
                                }

                                if (model.QTY_K > 0) {
                                    if (model.QTY_K > skuModel.QKECIL) {
                                        if (skuModel.QBESAR > 0) {
                                            skuModel.QBESAR -= 1;
                                            skuModel.QKECIL += (int) (skuModel.CONVER - model.QTY_K);
                                        }
                                    } else {
                                        skuModel.QKECIL -= model.QTY_K;
                                    }
                                }

                                ctrlSKU.updateStokOrder(skuModel);
                            }

                            realmO.copyToRealmOrUpdate(model);
                        }

                        ListIterator<ReturnModel> rt = returnModel.listIterator();
                        while (it.hasNext()) {
                            ReturnModel model = rt.next();
                            model.tanggal = tanggal;

                            realmO.copyToRealmOrUpdate(model);
                        }

                        ListIterator<CollectionModel> cl = collectionModel.listIterator();
                        while (cl.hasNext()) {
                            CollectionModel model = cl.next();
                            model.tanggal = tanggal;

                            realmO.copyToRealmOrUpdate(model);
                        }

                        ListIterator<StockCustomerModel> sd = stockCustomerModel.listIterator();
                        while (sd.hasNext()) {
                            StockCustomerModel model = sd.next();
                            model.tanggal = tanggal;

                            realmO.copyToRealmOrUpdate(model);
                        }

                        ListIterator<com.bhn.sadix.Data.StockCompetitorModel> sc = stockCompetitorModel.listIterator();
                        while (sc.hasNext()) {
                            com.bhn.sadix.Data.StockCompetitorModel model = sc.next();
                            model.tanggal = tanggal;

                            realmO.copyToRealmOrUpdate(model);
                        }

                        realmO.copyToRealmOrUpdate(cmModel);
                    }
                });

                if (salesTargetFragment != null)
                    salesTargetFragment.saveProducttarget();
//                saveLokal(tanggal);

                realmO.close();
            } catch (Exception e) {
//                FirebaseCrash.report(e);
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("custId", "0");
            editor.putBoolean("onResume", false);
            editor.apply();

            CommonModel model = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();

            if (model == null) {
                Log.wtf("Pending CommonModel", "null");
            } else {
                Log.wtf("Pending CommonModel", model.CommonID);
            }

//            Toast.makeText(KunjunganActivity.this, sharedPreferences.getString("custId", "0"), Toast.LENGTH_SHORT).show();

            if (takingOrderFragment.isCanvas()) {
                Util.confirmDialog(KunjunganActivity.this, "Info", "Nomor Faktur " + commonModel.InvNO + ", Apakah Faktur ingin dicetak?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) {
                            isHome = false;
                            bluePrint.open();
                            printFaktur();
                        } else {
                            finish();
                        }
                    }
                });
            } else {
                Util.Toast(KunjunganActivity.this, "Data berhasil disimpan!");
                isHome = false;
                Intent intent;
                if (Util.getIntegerPreference(KunjunganActivity.this, "taking_from") == Util.ALL_CUSTOMER) {
                    intent = new Intent(KunjunganActivity.this, AllCustomerActivity.class);
                    intent.putExtra("jenis", sharedPreferences.getInt("jenis", 1));
                } else {
                    intent = new Intent(KunjunganActivity.this, ScheduleActivity.class);
//                    intent.putExtra("jenis", sharedPreferences.getInt("jenis", 1));
                }
                startActivity(intent);
                finish();
            }
            super.onPostExecute(result);
        }
    }

    class LoadDataPrinciple extends AsyncTask<Integer, Void, Integer> {
        int error = 0x990;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(Integer... ints) {

            int result = ints[0];
            Log.d("SENT POS", result + "");

            try {
                orderPrincipleAdapter = new OrderDetailAdapter(KunjunganActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
                List<GroupSKUModel> list = ctrlGroupSKU.listByPrinsiple(principleModel.PrinsipleId);
                for (GroupSKUModel groupSKUModel : list) {
                    orderPrincipleAdapter.addHeader(groupSKUModel);
                }
            } catch (Exception e) {
//                FirebaseCrash.report(e);
                e.printStackTrace();
                result = error;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer s) {
            progressDialog.dismiss();
            if (s == error) {
                Util.showDialogInfo(KunjunganActivity.this, "Error Calling Data : " + s);
            } else {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, TakingOrderFragment.newInstance(false, true, s));
                ft.addToBackStack(null);
                ft.commit();
            }
            super.onPostExecute(s);
        }
    }
}

