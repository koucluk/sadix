package com.bhn.sadix;

import java.util.ArrayList;

import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.model.MenuUtamaModel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LogMenuActivity extends AppCompatActivity implements OnItemClickListener {
    private MenuUtamaAdapter adapter;
    private CtrlAppModul ctrlAppModul;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ctrlAppModul = new CtrlAppModul(this);
        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Log Transaction", getResources().getDrawable(R.drawable.log_transaction)));
        if (ctrlAppModul.isModul("27"))
            adapter.add(new MenuUtamaModel(2, "Log Customers Data", getResources().getDrawable(R.drawable.log_cust_data)));
        if (ctrlAppModul.isModul("27"))
            adapter.add(new MenuUtamaModel(3, "Log Customers Photo", getResources().getDrawable(R.drawable.log_cust_photo)));
        if (ctrlAppModul.isModul("25"))
            adapter.add(new MenuUtamaModel(4, "Log Branding Photo", getResources().getDrawable(R.drawable.log_branding)));
        if (ctrlAppModul.isModul("26"))
            adapter.add(new MenuUtamaModel(6, "Log Data Survey", getResources().getDrawable(R.drawable.log_survey)));
        if (ctrlAppModul.isModul("50"))
            adapter.add(new MenuUtamaModel(5, "Log Journey", getResources().getDrawable(R.drawable.log_journey)));
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = null;
        MenuUtamaModel model = adapter.getItem(position);
        if (model.id == 1) {
            intent = new Intent(this, LogDataActivity.class);
        } else if (model.id == 2) {
            intent = new Intent(this, LogDataCustomerActivity.class);
        } else if (model.id == 3) {
            intent = new Intent(this, LogPhotoCustomerActivity.class);
        } else if (model.id == 4) {
            intent = new Intent(this, LogPhotoCustomerBrandingActivity.class);
        } else if (model.id == 5) {
            intent = new Intent(this, LogGPSActivity.class);
        } else if (model.id == 6) {
            intent = new Intent(this, LogSurveyActivity.class);
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

}
