package com.bhn.sadix;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import android.annotation.SuppressLint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class CustomerLocationActivity extends AppCompatActivity {
    private CustomerModel customerModel;
    private WebView webView;
    private ProgressBar progress;

    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            if (location == null) {
                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        progress.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                    }
                });
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl("http://maps.google.com/maps?saddr=" + String.format("%1$s", loc.getLatitude()) +
                        "," + String.format("%1$s", loc.getLongitude()) + "&daddr=" + customerModel.GeoLat +
                        "," + customerModel.GeoLong + "&mode=driving");
            }
            location = loc;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.webView);
        progress = (ProgressBar) findViewById(R.id.progress);

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustId);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + customerModel.CustomerCrLimit + " | Sisa Credit Limit : " + customerModel.CustomerCrBalance);
        }
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
