package com.bhn.sadix.photo;

import android.graphics.Bitmap;

public interface TakePictureListener {
	public void takePicture(Bitmap bitmap, byte[] data);
}
