package com.bhn.sadix.photo;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
	private Context context;
	private TakePictureListener listener;
	private File file;
	
	public final static int KAMERA_DEPAN = Camera.CameraInfo.CAMERA_FACING_FRONT;
	public final static int KAMERA_BELAKANG = Camera.CameraInfo.CAMERA_FACING_BACK;
	
	private AutoFocusManager autoFocusManager;

    public CameraPreview(Context context, int cameraId) {
        super(context);
        this.context = context;
        loadCamera(cameraId);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }
    
    private void message(String message) {
    	Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    
    private void loadCamera(int cameraId){
    	mCamera = null;
        try {
            for (int camNo = 0; camNo < Camera.getNumberOfCameras(); camNo++) {
            	Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(camNo, camInfo);
                if (camInfo.facing==cameraId) {
                	mCamera = Camera.open(camNo);
                    break;
                }
            }
            if(mCamera == null) {
            	mCamera = Camera.open();// attempt to get a Camera instance
            }
            mCamera.setDisplayOrientation(90);
            
            Camera.Parameters params = mCamera.getParameters();
            
//            params.set("jpeg-quality", 100);
//            params.set("orientation", "landscape");
//            params.set("orientation", "portrait");
//            params.set("rotation", 90);
//            params.setPictureFormat(PixelFormat.JPEG);

//            List<Camera.Size> sizes = params.getSupportedPreviewSizes();
//            for (Camera.Size size : sizes) {
//				if(size.width > 799 && size.width < 899) {
//		            params.setPreviewSize(size.width, size.height);
//		            params.setPictureSize(size.width, size.height);
//		            break;
//				}
//			}
            
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            params.setPictureFormat(ImageFormat.JPEG);
            params.setPreviewSize(600, 400);
            params.setPictureSize(600, 400);
            mCamera.setParameters(params);
        }
        catch (Exception e){
        	message(e.getMessage());
        }
    }
    
    public void addTakePictureListener(TakePictureListener listener) {
    	this.listener = listener;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
            
            autoFocusManager = new AutoFocusManager(context, mCamera);
            autoFocusManager.start();
        } catch (IOException e) {
        	message(e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
            
            autoFocusManager = new AutoFocusManager(context, mCamera);
            autoFocusManager.start();
        } catch (Exception e){
        	message(e.getMessage());
        }
    }

    public File getFile() {
		return file;
	}

	public void onPause() {
        autoFocusManager.stop();
        autoFocusManager = null;
		
    	mCamera.release();
    	mCamera = null;
    }
    
    public Camera getCamera() {
		return mCamera;
	}

	public void takePicture() {
    	mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }
	ShutterCallback shutterCallback = new ShutterCallback() {
		public void onShutter() {
		}
	};

	/** Handles data for raw picture */
	PictureCallback rawCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
		}
	};

	/** Handles data for jpeg picture */
	PictureCallback jpegCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
			Bitmap bitmap = null;
			try {
				bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
				int w = bitmap.getWidth();
	            int h = bitmap.getHeight();
	            Matrix mtx = new Matrix();
	            mtx.postRotate(90);
	            bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
			} catch (Exception e) {
	        	message(e.getMessage());
			}
			if(listener != null) {
				listener.takePicture(bitmap, data);
			}
			mCamera.startPreview();
			
            autoFocusManager = new AutoFocusManager(context, mCamera);
            autoFocusManager.start();
		}
	};
}