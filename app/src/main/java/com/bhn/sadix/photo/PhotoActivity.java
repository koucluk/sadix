package com.bhn.sadix.photo;

import java.io.ByteArrayOutputStream;

import com.bhn.sadix.R;

import android.os.Build;
import android.os.Bundle;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PhotoActivity extends AppCompatActivity implements TakePictureListener {
    private CameraPreview cv;
    private FrameLayout alParent;
    private byte[] data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        alParent = (FrameLayout) findViewById(R.id.frame_camera);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Photo")
                    .setIcon(android.R.drawable.ic_menu_camera)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            menu.add(0, 2, 2, "Save")
                    .setIcon(android.R.drawable.ic_menu_save)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        } else {
            menu.add(0, 1, 1, "Photo")
                    .setIcon(android.R.drawable.ic_menu_camera);
            menu.add(0, 2, 2, "Save")
                    .setIcon(android.R.drawable.ic_menu_save);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case 1:
                onClick(null);
                break;
            case 2:
                save();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void load() {
        try {
            alParent.removeAllViews();
            cv = new CameraPreview(this, CameraPreview.KAMERA_BELAKANG);
            cv.addTakePictureListener(this);
            alParent.addView(cv);
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, "Unable to find camera. Closing.", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        load();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cv != null) {
            cv.onPause();
            cv = null;
        }
    }

    private void save() {
        try {
            if (data != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                int w = bitmap.getWidth();
                int h = bitmap.getHeight();
                Matrix mtx = new Matrix();
                mtx.postRotate(90);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                data = bao.toByteArray();
                bao.flush();
                bao.close();
                bao = null;
                bitmap = null;
            }
            Intent intent = new Intent();
            intent.putExtra("data", data);
            setResult(RESULT_OK, intent);
            data = null;
            finish();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void takePicture(Bitmap bitmap, byte[] data) {
        this.data = data;
        ((TextView) findViewById(R.id.hasil)).setBackgroundDrawable(new BitmapDrawable(bitmap));
        ((TextView) findViewById(R.id.hasil)).setVisibility(View.VISIBLE);
        ((FrameLayout) findViewById(R.id.frame_camera)).setVisibility(View.GONE);
    }

    public void onClick(View view) {
        if (((FrameLayout) findViewById(R.id.frame_camera)).getVisibility() == View.GONE) {
            ((TextView) findViewById(R.id.hasil)).setVisibility(View.GONE);
            ((FrameLayout) findViewById(R.id.frame_camera)).setVisibility(View.VISIBLE);
        } else {
            cv.takePicture();
        }
    }
}
