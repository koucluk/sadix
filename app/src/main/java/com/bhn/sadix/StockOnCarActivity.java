package com.bhn.sadix;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.adapter.StokOnCarAdapter;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.StokOnCarModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class StockOnCarActivity extends AppCompatActivity {
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private ProgressDialog progressDialog;
    private StokOnCarAdapter adapter;
    private StokOnCarAdapter adapterOrder;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlSKU ctrlSKU;
    private View vStok;
    private View vOrder;
    private DatabaseHelper db;
    private TakingOrderModel takingOrderModel;
    Realm realm;

    Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHelper(this);
        setContentView(R.layout.activity_stock_on_car);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        realm = Realm.getDefaultInstance();


        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((SegmentedRadioGroup) findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_stok) {
                    loadStok();
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_order) {
                    loadOrder();
                    mPager.setCurrentItem(1);
                }
            }
        });
        findViewById(R.id.tab_stok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadStok();
                mPager.setCurrentItem(0);
            }
        });
        findViewById(R.id.tab_order).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadOrder();
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    loadStok();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_stok);
                } else if (position == 1) {
                    loadOrder();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_order);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);
        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);

        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlSKU = new CtrlSKU(this);

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
    }

    /*protected void loadOrder() {
        progressDialog.show();
        new LoadOrderData().execute();
    }*/

    protected void loadStok() {
        try {
            if (vStok != null) {
                ((ExpandableListView) vStok.findViewById(R.id.list)).setAdapter(adapter);
            }
        } catch (Exception e) {
            Util.showDialogError(this, e.getMessage());
        }
    }

    private void load() {
        progressDialog.show();
        new LoadData().execute();
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                adapter = new StokOnCarAdapter(StockOnCarActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
                List<GroupSKUModel> list = ctrlGroupSKU.list();
                for (GroupSKUModel groupSKUModel : list) {
                    adapter.addHeader(groupSKUModel, false);
                    List<SKUModel> listSKU = ctrlSKU.listByGroupStok(groupSKUModel);
                    for (SKUModel skuModel : listSKU) {
                        StokOnCarModel stockOutletModel = new StokOnCarModel(skuModel, skuModel.QBESAR, skuModel.QKECIL);
                        adapter.addChild(groupSKUModel, stockOutletModel, false);
                    }
                }
            } catch (Exception e) {
                Log.e("Exception1", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (vStok != null) {
                ((ExpandableListView) vStok.findViewById(R.id.list)).setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            super.onPostExecute(result);
        }
    }

    protected void loadOrder() {
        try {
            progressDialog.show();
            adapterOrder = new StokOnCarAdapter(StockOnCarActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
            for (int i = 0; i < adapter.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapter.getGroup(i);
                adapterOrder.addHeader(groupSKUModel, false);
                boolean flag = false;
                for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                    StokOnCarModel orderDetailModel = adapter.getChild(i, j);

                    RealmResults takingResult = realm.where(TakingOrderModel.class).equalTo("SKUId", orderDetailModel.skuModel.SKUId).findAll();

                    int QTY_B = takingResult.sum("QTY_B").intValue();
                    int QTY_K = takingResult.sum("QTY_K").intValue();

//                    List<JSONObject> list = db.list("select sum(QTY_B) QTY_B,sum(QTY_K) QTY_K from taking_order where SKUId='" + orderDetailModel.skuModel.SKUId + "'");
                    if (takingResult.size() > 0) {
                        try {
//                            JSONObject sku = list.get(0);

                            Log.d("QTY_B", QTY_B + "");

                            if (QTY_B > 0 || QTY_K > 0) {
                                adapterOrder.addChild(groupSKUModel, new StokOnCarModel(
                                        orderDetailModel.skuModel,
                                        QTY_B,
                                        QTY_K
                                ), false);
                                flag = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//						if(orderDetailModel.skuModel.QOBESAR > 0 || orderDetailModel.skuModel.QOKECIL > 0) {
//							adapterOrder.addChild(groupSKUModel, new StokOnCarModel(
//									orderDetailModel.skuModel,
//									orderDetailModel.skuModel.QOBESAR,
//									orderDetailModel.skuModel.QOKECIL
//								), false);
//							flag = true;
//						}
                }
                if (flag == false) {
                    adapterOrder.removeHeader(groupSKUModel, false);
                }
            }

            progressDialog.dismiss();
            ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
            adapterOrder.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
//            Log.e("Exception2", e.getMessage());
        }
    }

    private class LoadOrderData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                adapterOrder = new StokOnCarAdapter(StockOnCarActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    GroupSKUModel groupSKUModel = adapter.getGroup(i);
                    adapterOrder.addHeader(groupSKUModel, false);
                    boolean flag = false;
                    for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                        StokOnCarModel orderDetailModel = adapter.getChild(i, j);
                        List<JSONObject> list = db.list("select sum(QTY_B) QTY_B,sum(QTY_K) QTY_K from taking_order where SKUId='" + orderDetailModel.skuModel.SKUId + "'");
                        if (list.size() > 0) {
                            try {
                                JSONObject sku = list.get(0);

                                Log.d("QTY_B", sku.getString("QTY_B"));

                                if (Integer.parseInt(sku.getString("QTY_B")) > 0 || sku.getInt("QTY_K") > 0) {
                                    adapterOrder.addChild(groupSKUModel, new StokOnCarModel(
                                            orderDetailModel.skuModel,
                                            sku.getInt("QTY_B"),
                                            sku.getInt("QTY_K")
                                    ), false);
                                    flag = true;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//						if(orderDetailModel.skuModel.QOBESAR > 0 || orderDetailModel.skuModel.QOKECIL > 0) {
//							adapterOrder.addChild(groupSKUModel, new StokOnCarModel(
//									orderDetailModel.skuModel, 
//									orderDetailModel.skuModel.QOBESAR, 
//									orderDetailModel.skuModel.QOKECIL
//								), false);
//							flag = true;
//						}
                    }
                    if (flag == false) {
                        adapterOrder.removeHeader(groupSKUModel, false);
                    }
                }
            } catch (Exception e) {
                Log.e("Exception2", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (vOrder != null) {
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                adapterOrder.notifyDataSetChanged();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) StockOnCarActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);
            if (position == 0) {
                vStok = v;
                vStok.findViewById(R.id.list).setVisibility(View.VISIBLE);
                if (adapter == null) {
                    load();
                } else {
                    loadStok();
                }
            } else if (position == 1) {
                vOrder = v;
                vOrder.findViewById(R.id.list).setVisibility(View.VISIBLE);
                loadOrder();
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

}
