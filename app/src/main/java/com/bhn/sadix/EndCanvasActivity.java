package com.bhn.sadix;

import java.util.Date;

import org.json.JSONObject;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

public class EndCanvasActivity extends AppCompatActivity implements ConnectionEvent {
    private DatabaseHelper db;
    private CtrlSKU ctrlSKU;
    private JSONObject json;
    private ConnectionServer server;

    Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_canvas);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        db = new DatabaseHelper(this);
        ctrlSKU = new CtrlSKU(this);
        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
        findViewById(R.id.btn_log).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActivityLog();
            }
        });
        try {
            Cursor cursor = db.Query("select sum(QTY_B) Qty_L,sum(QTY_K) Qty,sum(PRICE) TPrice,"
                    + "sum(DISCOUNT) TDiscon,sum(PRICE)-sum(DISCOUNT) GPrice,count(*) TOutlet "
                    + "from common where SalesId='" + UserModel.getInstance(this).SalesId + "'");
            if (cursor.moveToNext()) {
                json = new JSONObject();
                json.put("Qty_L", cursor.getInt(0));
                json.put("Qty", cursor.getInt(1));
                json.put("TPrice", cursor.getDouble(2));
                json.put("TDiscon", cursor.getDouble(3));
                json.put("GPrice", cursor.getDouble(4));
                json.put("TOutlet", cursor.getInt(5));
            }
            if (json != null) {
                ((EditText) findViewById(R.id.Qty_L)).setText(Util.NUMBER_FORMAT.format(json.getInt("Qty_L")));
                ((EditText) findViewById(R.id.Qty)).setText(Util.NUMBER_FORMAT.format(json.getInt("Qty")));
                ((EditText) findViewById(R.id.TPrice)).setText(Util.numberFormat.format(json.getDouble("TPrice")));
                ((EditText) findViewById(R.id.TDiscon)).setText(Util.numberFormat.format(json.getDouble("TDiscon")));
                ((EditText) findViewById(R.id.GPrice)).setText(Util.numberFormat.format(json.getDouble("GPrice")));
                ((EditText) findViewById(R.id.TOutlet)).setText(Util.NUMBER_FORMAT.format(json.getInt("TOutlet")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onActivityLog() {
        startActivity(new Intent(this, LogDataActivity.class));
    }

    private void saveData() {
        try {
            json.put("SalesID", Integer.parseInt(UserModel.getInstance(this).SalesId));
            json.put("CanvasID", Util.getStringPreference(this, "CanvasNo"));
            json.put("Remark", ((EditText) findViewById(R.id.Remark)).getText().toString());
            json.put("LogTime", Util.dateFormat.format(new Date()));
        } catch (Exception e) {
            error(e.getMessage());
        }
        android.util.Log.e("json", "" + json.toString());
        server.setUrl(Util.getServerUrl(this) + "endcanvas");
//		server.setUrl("http://sdx.x-locate.com/SadixApi2/endcanvas");
        server.requestJSONObjectNoTimeOut(json);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.getInt("ServerID") > 0) {
                Util.okeDialog(this, "Info", json.getString("Message"), new OkListener() {
                    @Override
                    public void onDialogOk() {
                        ctrlSKU.clearDataEndCanvas();
                        finish();
                    }
                });
            } else {
                Util.showDialogInfo(this, json.getString("Message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
