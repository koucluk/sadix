package com.bhn.sadix;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.bhn.sadix.adapter.CustomerRecyclerAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

public class ScheduleActivity extends AppCompatActivity implements OnItemClickListener, CustomerRecyclerAdapter.ClickItem, SearchView.OnQueryTextListener {
    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private MenuUtamaAdapter adapter;

    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private static CtrlCustomer ctrlCustomer;
    private static String day = "";
    private static String filter = null;
    private static CtrlAppModul ctrlAppModul;
    //    private static CtrlCommon ctrlCommon;
    private static String sday;
    public static Realm realmUI;
    Context context;

    private Toolbar toolbar;
    private CtrlCustomerType ctrlCustomerType;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        realmUI = Realm.getDefaultInstance();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ctrlCustomer = new CtrlCustomer(this);
        ctrlAppModul = new CtrlAppModul(this);

        ctrlCustomerType = new CtrlCustomerType(this);

        Realm realmUI = Realm.getDefaultInstance();

        com.bhn.sadix.Data.CommonModel cmModel = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        if (cmModel != null) {
            startActivity(new Intent(this, MenuUtamaActivity.class));
            Log.d("Common ID", cmModel.CommonID);
            Intent intent = new Intent(this, KunjunganActivity.class);
            CustomerModel customerModel = ctrlCustomer.get(cmModel.CustomerId);
            Log.d("Ini last CID ", customerModel.CustId);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
            intent.putExtra("isLoad", true);
            startActivity(intent);

            finish();
        } else {
            Log.d("NO AVAIL COMMON", "YES");
        }

        context = this;
//        ctrlCommon = new CtrlCommon(this);

        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Sunday", getResources().getColor(R.color.black), true, "Minggu"));
        adapter.add(new MenuUtamaModel(2, "Monday", getResources().getColor(R.color.black), true, "Senin"));
        adapter.add(new MenuUtamaModel(3, "Tuesday", getResources().getColor(R.color.black), true, "Selasa"));
        adapter.add(new MenuUtamaModel(4, "Wednesday", getResources().getColor(R.color.black), true, "Rabu"));
        adapter.add(new MenuUtamaModel(5, "Thursday", getResources().getColor(R.color.black), true, "Kamis"));
        adapter.add(new MenuUtamaModel(6, "Friday", getResources().getColor(R.color.black), true, "Jumat"));
        adapter.add(new MenuUtamaModel(7, "Saturday", getResources().getColor(R.color.black), true, "Sabtu"));
        adapter.add(new MenuUtamaModel(8, "Close", getResources().getColor(R.color.red), true, "Close"));
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(this);
//    	int index = adapter.getPosition(new MenuUtamaModel(new SimpleDateFormat("EEEE").format(new Date())));
//    	if(index != -1) {
//	    	MenuUtamaModel model = adapter.getItem(index);
//	    	if(model != null) {
//	    		day = "D" + model.id;
//	    		((RadioButton)findViewById(R.id.tab_day)).setText(model.caption);
//	    	}
//    	}
        int hariini = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        day = "D" + hariini;
        if (hariini - 1 > -1) {
            MenuUtamaModel model = adapter.getItem(hariini - 1);
            if (model != null) {
                sday = model.caption;
                ((RadioButton) findViewById(R.id.tab_day)).setText(model.caption);
            }
        }

        mTitle = mDrawerTitle = getTitle();

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

        ((SegmentedRadioGroup) findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_day) {
                    mPager.setCurrentItem(0);
                    if (pagerAdapter.schedule.countData() == 0) {
                        pagerAdapter.schedule.load();
                    }
                } else if (checkedId == R.id.tab_all_cust) {
                    mPager.setCurrentItem(1);
                    if (pagerAdapter.all_cus.countData() == 0) {
                        pagerAdapter.all_cus.load();
                    }
                }
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_day);
                    if (pagerAdapter.schedule.countData() == 0) {
                        pagerAdapter.schedule.load();
                    }
                } else if (position == 1) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_all_cust);
                    if (pagerAdapter.all_cus.countData() == 0) {
                        pagerAdapter.all_cus.load();
                    }
                }
            }
        };

        mPager.setOnPageChangeListener(ViewPagerListener);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);

        ((RadioButton) findViewById(R.id.tab_all_cust)).setText("All Customer (" + String.valueOf(ctrlCustomer.count()) + ")");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (((RadioButton) findViewById(R.id.tab_day)).isChecked()) {
            if (pagerAdapter.schedule != null && pagerAdapter.schedule.adapterCus != null) {
                pagerAdapter.schedule.adapterCus.notifyDataSetChanged();
            }
        } else {
            if (pagerAdapter.all_cus != null && pagerAdapter.all_cus.adapterCus != null) {
                pagerAdapter.all_cus.adapterCus.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        searchView.setOnQueryTextListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(1, 1, 1, "Search")
                    .setIcon(android.R.drawable.ic_menu_search)
                    .setActionView(searchView)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        } else {
            menu.add(1, 1, 1, "Search");
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filter = query;
        if (((RadioButton) findViewById(R.id.tab_day)).isChecked()) {
            pagerAdapter.schedule.load();
        } else {
            pagerAdapter.all_cus.load();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (filter != null && newText.equals("")) {
            filter = null;
            if (((RadioButton) findViewById(R.id.tab_day)).isChecked()) {
                pagerAdapter.schedule.load();
            } else {
                pagerAdapter.all_cus.load();
            }
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
        if (model.id == 8) {
            finish();
        } else {
            day = "D" + model.id;
            sday = model.caption;
            ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_day);
            ((RadioButton) findViewById(R.id.tab_day)).setText(model.caption);
            pagerAdapter.schedule.load();

        }
    }

    @Override
    public void clickedItem(int position, CustomerModel model) {
//        CustomerModel model = adapterCus.getItem(position);
        Intent intent = new Intent(ScheduleActivity.this, DetailCustomerActivity.class);

        if (ctrlAppModul.isModul("56")) {
//                        CommonModel commonModel = ctrlCommon.getByCustomerAndDate(model.CustId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            com.bhn.sadix.Data.CommonModel commonModel = realmUI
                    .where(com.bhn.sadix.Data.CommonModel.class)
                    .equalTo("CustomerId", model.CustId)
                    .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                    .equalTo("Done", 1).findFirst();

            if (commonModel != null) {
//							Util.showDialogInfo(ScheduleActivity.this, "Customer " + model.CustomerName + " sudah dikunjungi!");
//							return;
                intent.putExtra("MSG", "Customer " + model.CustomerName + " sudah dikunjungi!");
            }
        }
//					Intent intent = new Intent(ScheduleActivity.this, DetailCustomerActivity.class);
        intent.putExtra("CustomerModel", model);
        Util.putPreference(ScheduleActivity.this, "taking_from", Util.SCHEDULE);
        startActivity(intent);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 2;
        FragmentSchedule schedule = new FragmentSchedule();
        FragmentAll all_cus = new FragmentAll();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return schedule;
                case 1:
                    return all_cus;
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    public static class FragmentSchedule extends Fragment {
        private View view;
        //        private CustomerAdapter adapterCus;
        private CustomerRecyclerAdapter adapterCus;
        private ProgressDialog progressDialog;
        private RecyclerView list;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            progressDialog = new ProgressDialog(getActivity()) {
                @Override
                public boolean onKeyDown(int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                        dismiss();
                    }
                    return super.onKeyDown(keyCode, event);
                }

            };
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_schedule_item, container, false);

            list = (RecyclerView) view.findViewById(R.id.list);
            list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

            /*((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                }
            });*/

            load();
            return view;
        }

        public void load() {
            new LoadData().execute();
        }

        public int countData() {
            return (adapterCus == null ? 0 : adapterCus.getItemCount());
        }

        private class LoadData extends AsyncTask<Void, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    if (view != null) {
                        List<CustomerModel> model;
                        if (filter != null && filter.equals("") == false) {
                            model = ctrlCustomer.listByDayAndName(day, filter);
                            adapterCus = new CustomerRecyclerAdapter(getActivity(), model);
                        } else {
                            model = ctrlCustomer.listByDay(day);
                            adapterCus = new CustomerRecyclerAdapter(getActivity(), model);
                        }
//                        adapterCus.setNotifyOnChange(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                progressDialog.dismiss();
                if (result == null) {
                    if (adapterCus != null) {
                        list.setAdapter(adapterCus);
//                    ((ListView) view.findViewById(R.id.list)).setAdapter(adapterCus);
                        ((RadioButton) getActivity().findViewById(R.id.tab_day)).setText(sday + " (" + String.valueOf(adapterCus.getItemCount()) + ")");
                    }
                } else {
                    Util.okeDialog(getActivity(), "INFO", "Data yang didownload tidak lengkap, harap login kembali!", new OkListener() {

                        @Override
                        public void onDialogOk() {
                            getActivity().finish();
                        }
                    });
                }
                super.onPostExecute(result);
            }
        }
    }

    public static class FragmentAll extends Fragment {
        private View view;
        //        private CustomerAdapter adapterCus;
        private CustomerRecyclerAdapter adapterCus;
        private ProgressDialog progressDialog;
        private RecyclerView list;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            progressDialog = new ProgressDialog(getActivity()) {
                @Override
                public boolean onKeyDown(int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                        dismiss();
                    }
                    return super.onKeyDown(keyCode, event);
                }

            };
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            view = inflater.inflate(R.layout.fragment_schedule_item, container, false);

            list = (RecyclerView) view.findViewById(R.id.list);
            list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

            /*((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    CustomerModel model = adapterCus.getItem(position);
                    Intent intent = new Intent(getActivity(), DetailCustomerActivity.class);
                    if (ctrlAppModul.isModul("56")) {
//                        CommonModel commonModel = ctrlCommon.getByCustomerAndDate(model.CustId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

                        com.bhn.sadix.Data.CommonModel commonModel = realmUI
                                .where(com.bhn.sadix.Data.CommonModel.class)
                                .equalTo("CustomerId", model.CustId)
                                .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                                .equalTo("Done", 1).findFirst();

                        if (commonModel != null) {
//							Util.showDialogInfo(ScheduleActivity.this, "Customer " + model.CustomerName + " sudah dikunjungi!");
//							return;
                            intent.putExtra("MSG", "Customer " + model.CustomerName + " sudah dikunjungi!");
                        }
                    }
//					Intent intent = new Intent(ScheduleActivity.this, DetailCustomerActivity.class);
                    intent.putExtra("CustomerModel", model);
                    startActivity(intent);
                }
            });*/
            return view;
        }

        public void load() {
            new LoadData().execute();
        }

        public int countData() {
            return (adapterCus == null ? 0 : adapterCus.getItemCount());
        }

        private class LoadData extends AsyncTask<Void, Void, String> {
            /*public LoadData() {
                progressDialog.show();
            }*/

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog.show();
            }

            @Override
            protected String doInBackground(Void... params) {
                try {
                    if (view != null) {
                        List<CustomerModel> model;
                        if (filter != null && filter.equals("") == false) {
                            model = ctrlCustomer.listByName(filter);
                            adapterCus = new CustomerRecyclerAdapter(getActivity(), model);
                        } else {
                            model = ctrlCustomer.list();
                            adapterCus = new CustomerRecyclerAdapter(getActivity(), model);
                        }
//                        adapterCus.setNotifyOnChange(true);
                    }
                    return null;
                } catch (Exception e) {
                    return "" + e.getMessage();
                }
            }

            @Override
            protected void onPostExecute(String result) {
                progressDialog.dismiss();
                if (result == null) {
                    if (adapterCus != null) {
                        list.setAdapter(adapterCus);
//                        ((ListView) view.findViewById(R.id.list)).setAdapter(adapterCus);
                        ((RadioButton) getActivity().findViewById(R.id.tab_all_cust)).setText("All Customer (" + String.valueOf(adapterCus.getItemCount()) + ")");
                    }
                } else {
                    Util.okeDialog(getActivity(), "INFO", "Data yang didownload tidak lengkap, harap login kembali!", new OkListener() {

                        @Override
                        public void onDialogOk() {
                            getActivity().finish();
                        }
                    });
                }
                super.onPostExecute(result);
            }
        }
    }

}
