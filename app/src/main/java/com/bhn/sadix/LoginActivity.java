package com.bhn.sadix;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.UpdateApp;
import com.bhn.sadix.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;

import io.realm.Realm;

public class LoginActivity extends AppCompatActivity implements ConnectionEvent, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private ConnectionServer server;
    private CtrlAppModul ctrlAppModul;
    private CtrlCommon ctrlCommon;
    private CtrlCustomer ctrlCustomer;
    private CtrlCustomerType ctrlCustomerType;
    private int versionCode = 0;
    private String CmId;

    LocationRequest mLocationRequestHigh;
    GoogleApiClient mGoogleApiClient;
    Location location;
    Toolbar toolbar;

    protected static final int REQUEST_CODE_RESOLUTION = 1;

    /*private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location loc) {
            location = loc;
            ((TextView) findViewById(R.id.long_lat)).setText("Location:" + String.format("%1$s", location.getLatitude()) + "," + String.format("%1$s", location.getLongitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };*/
    private int jenis = 0;
    private boolean mIsInResolution;

    private static final String KEY_IN_RESOLUTION = "is_in_resolution";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //cek kolom dan tabel

        if (savedInstanceState != null) {
            mIsInResolution = savedInstanceState.getBoolean(KEY_IN_RESOLUTION, false);
        }

        Log.d("On Create Login", "Called");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        Util.existAndCreateKolom("TargetProduk", "StTarget", new DatabaseHelper(this).getWritableDatabase(), "integer");
        Util.existAndCreateTable("Salestarget", new DatabaseHelper(this).getWritableDatabase(), DatabaseHelper.Salestarget);
        Util.existAndCreateKolom("common", "CustomerRID", new DatabaseHelper(this).getWritableDatabase(), "varchar(50)");
        Util.existAndCreateTable("SalesNote", new DatabaseHelper(this).getWritableDatabase(), DatabaseHelper.SalesNote);
        Util.existAndCreateKolom("Newasset", "NoSeri", new DatabaseHelper(this).getWritableDatabase(), "varchar(50)");
        Util.existAndCreateTable("Prinsiple", new DatabaseHelper(this).getWritableDatabase(), DatabaseHelper.Prinsiple);
        Util.existAndCreateKolom("taking_order", "PrinsipleID", new DatabaseHelper(this).getWritableDatabase(), "varchar(50)");
        Util.existAndCreateTable("VisitNote", new DatabaseHelper(this).getWritableDatabase(), DatabaseHelper.VisitNote);
        //==============================================================================================================
        setContentView(R.layout.activity_login);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            getSupportActionBar().setTitle("Sadix-" + versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            Util.Toast(this, e.getMessage());
        }
//        setTitle(R.string.title_activity_login);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ctrlAppModul = new CtrlAppModul(this);
        ctrlCommon = new CtrlCommon(this);
        ctrlCustomer = new CtrlCustomer(this);
        ctrlCustomerType = new CtrlCustomerType(this);
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
        }
        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        /*locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }*/

        CmId = getSharedPreferences("SADIX", MODE_PRIVATE).getString("custId", "0");
    }

    private PendingResult<LocationSettingsResult> requestLocation() {
        mLocationRequestHigh = new LocationRequest();
        mLocationRequestHigh.setInterval(5000);
        mLocationRequestHigh.setFastestInterval(2500);
        mLocationRequestHigh.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequestHigh);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        return result;
    }

    @Override
    protected void onStart() {
        Log.d("On Start Login", "Called");
        super.onStart();

        mGoogleApiClient.connect();

        Log.d("connect gAPI", "reached");

        File file = new File(Environment.getExternalStorageDirectory(), "Sadix");
        if (!file.exists()) {
            file.mkdir();
            file = new File(Environment.getExternalStorageDirectory() + "/Sadix", "img");
            if (!file.exists()) {
                file.mkdir();
            }
            file = null;
        } else {
            file = new File(Environment.getExternalStorageDirectory() + "/Sadix", "img");
            if (!file.exists()) {
                file.mkdir();
            }
            file = null;
        }
        if (UserModel.getInstance(this).isSavePassword) {
            ((EditText) findViewById(R.id.user_name)).setText(UserModel.getInstance(this).UserName);
            ((EditText) findViewById(R.id.password)).setText(UserModel.getInstance(this).Password);
            ((CheckBox) findViewById(R.id.ck_save_password)).setChecked(UserModel.getInstance(this).isSavePassword);
        }

        Log.d("checking file", "reached");
//		android.util.Log.e("tes",Util.getStringPreference(this, "DAY_OF_LOGOUT")+"="+Util.getBooleanPreference(this, "isLogin"));
        if (Util.getBooleanPreference(this, "isLogin")) {
            if (Util.getStringPreference(this, "DAY_OF_LOGOUT").equals(Util.FORMAT_LOGOUT.format(new Date()))) {
                LocationManager locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (ctrlAppModul.isModul("50")) {
                    if (locMan.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    } else {
                        Realm realm = Realm.getDefaultInstance();
                        com.bhn.sadix.Data.CommonModel model = realm.where(com.bhn.sadix.Data.CommonModel.class)
                                .equalTo("Done", 0)
                                .findFirst();
                        if (model == null) {
                            startActivity(new Intent(this, MenuUtamaActivity.class));
                        } else {
                            Log.d("Common ID", model.CommonID);
                            Intent intent = new Intent(this, KunjunganActivity.class);
                            CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
                            Log.d("Ini last CID ", customerModel.CustId);
                            intent.putExtra("CustomerModel", customerModel);
                            intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
                            intent.putExtra("isLoad", true);
                            startActivity(intent);
                        }

                        realm.close();
                    }
                    finish();
                } else {
                    Realm realm = Realm.getDefaultInstance();
                    com.bhn.sadix.Data.CommonModel model = realm.where(com.bhn.sadix.Data.CommonModel.class)
                            .equalTo("Done", 0)
                            .findFirst();
                    if (model == null) {
                        startActivity(new Intent(this, MenuUtamaActivity.class));
                    } else {
                        Log.d("Common ID", model.CommonID);
                        Intent intent = new Intent(this, KunjunganActivity.class);
                        CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
                        Log.d("Ini last CID ", customerModel.CustId);
                        intent.putExtra("CustomerModel", customerModel);
                        intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
                        intent.putExtra("isLoad", true);
                        startActivity(intent);
                    }

                    realm.close();
                    finish();
                }
            } else {
                Log.d("DIFF DAY", "IN");
                Realm realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.where(CommonModel.class).equalTo("Done", 0).findAll().deleteAllFromRealm();
                    }
                });

                Util.putPreference(this, "isLogin", false);
                stopService(new Intent(this, SadixService.class));

                realm.close();
            }
        } else {
            Log.d("LOGIN FALSE", "IN");
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(CommonModel.class).equalTo("Done", 0).findAll().deleteAllFromRealm();
                }
            });

            realm.close();
            loadConfig();
        }
    }

    private void loadConfig() {
        jenis = 2;
//        String url = Util.getServerUrl(this) + "getconfig/imei/" + Util.getIMEI(this);
        String url = "http://sdx.x-locate.com/SadixApi2/getconfig/imei/" + Util.getIMEI(this);
        Log.d("URL Config", url);
        server.setUrl(url);
        server.requestJSONObject(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case Util.REQUEST_CHECK_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        mGoogleApiClient.connect();
//                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(this, "Nyalakan GPS untuk memulai Sadix", Toast.LENGTH_SHORT).show();
                        break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void login() {
        if (location == null && Util.getBooleanPreference(this, "isGPS")) {
            Util.showDialogInfo(this, "Lokasi belum ditemukan!");
            return;
        }
        Util.putPreference(this, "UserName", ((EditText) findViewById(R.id.user_name)).getText().toString());
        Util.putPreference(this, "Password", ((EditText) findViewById(R.id.password)).getText().toString());
        if (((CheckBox) findViewById(R.id.ck_save_password)).isChecked()) {
            Util.putPreference(this, "isSavePassword", ((CheckBox) findViewById(R.id.ck_save_password)).isChecked());
        }
        JSONObject json = new JSONObject();
        try {
            android.util.Log.e("IMEI", Util.getIMEI(this));
            json.put("IMEI", Util.getIMEI(this));
            json.put("UserName", ((EditText) findViewById(R.id.user_name)).getText().toString());
            json.put("Password", ((EditText) findViewById(R.id.password)).getText().toString());
            int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            json.put("AppVersion", versionCode);
            json.put("OprName", Util.getNetworkOperatorName(this));
            if (location != null) {
                json.put("GeoLong", location.getLongitude());
                json.put("GeoLat", location.getLatitude());
            } else {
                json.put("GeoLong", 0);
                json.put("GeoLat", 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
//		server.setUrl(Util.SERVER_URL+"userlogin");
        jenis = 1;

//        server.setUrl("http://sdx.x-locate.com/SadixApi2/userlogin");
        server.setUrl(Util.getServerUrl(this) + "/userlogin");
        server.requestJSONObject(json);
    }

    private void loadLogin() {
        Util.putPreference(this, "UserName", ((EditText) findViewById(R.id.user_name)).getText().toString());
        Util.putPreference(this, "Password", ((EditText) findViewById(R.id.password)).getText().toString());
        if (((CheckBox) findViewById(R.id.ck_save_password)).isChecked()) {
            Util.putPreference(this, "isSavePassword", ((CheckBox) findViewById(R.id.ck_save_password)).isChecked());
        }
        JSONObject json = new JSONObject();
        try {
            json.put("IMEI", Util.getIMEI(this));
            json.put("UserName", ((EditText) findViewById(R.id.user_name)).getText().toString());
            json.put("Password", ((EditText) findViewById(R.id.password)).getText().toString());
            int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            json.put("AppVersion", versionCode);
        } catch (Exception e) {
            error(e.getMessage());
        }
        jenis = 1;
        server.setUrl(Util.getServerUrl(this) + "userlogin");
//		server.requestJSONObject(json);
        server.requestJSONObjectNoTimeOut(json);
    }

    @Override
    public void error(String error) {
        if (error.startsWith("Koneksi gagal")) {
            Util.showDialogError(this, "Koneksi gagal, silahkan mencari mencari tempat yang ada koneksi");
        } else {
            Util.showDialogError(this, error);
        }
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (jenis == 1) {
                if (json.has("LoginException")) {
                    if (json.getJSONObject("LoginException").has("Result") && json.getJSONObject("LoginException").getString("Result").equals("OK")) {
                        JSONObject login = json.getJSONObject("Login");
                        int AppVersion = login.getInt("AppVersion");
                        final String PathUrl = login.getString("PathUrl");
                        if (AppVersion > versionCode) {
                            Util.okeDialog(this, "Info", "Ada update versi terbaru!", new OkListener() {
                                @Override
                                public void onDialogOk() {
                                    new UpdateApp(LoginActivity.this).execute(PathUrl);
                                }
                            });
                        } else {
                            if (login.has("PathSvr") && login.getString("PathSvr") != null && !login.getString("PathSvr").trim().equals("") && !login.getString("PathSvr").trim().toLowerCase().equals("null")) {
                                Util.putPreference(this, "PathSvr", login.getString("PathSvr"));
                                server.lanjutLoading();
                                loadLogin();
                            } else {
                                Util.putPreference(this, "UserName", ((EditText) findViewById(R.id.user_name)).getText().toString());
                                Util.putPreference(this, "Password", ((EditText) findViewById(R.id.password)).getText().toString());
                                Util.putPreference(this, "SalesId", login.getString("SalesId"));
                                Util.putPreference(this, "PathUrl", login.getString("PathUrl"));
                                Util.putPreference(this, "Interval", login.getString("Interval"));
                                Util.putPreference(this, "FullName", login.getString("FullName"));
                                Util.putPreference(this, "AppVersion", login.getString("AppVersion"));
                                Util.putPreference(this, "IMEI", login.getString("IMEI"));
                                Util.putPreference(this, "SalesType", login.getString("SalesType"));
                                Util.putPreference(this, "CompanyName", login.getString("CompanyName"));
                                Util.putPreference(this, "CompanyID", login.getString("CompanyID"));
                                Util.putPreference(this, "WHStart", login.getString("WHStart"));
                                Util.putPreference(this, "WHStop", login.getString("WHStop"));
                                Util.putPreference(this, "FormatInv", login.getString("FormatInv"));
                                Util.putPreference(this, "LastInv", login.getInt("LastInv"));
                                Util.putPreference(this, "CustLevel", login.getInt("CustLevel"));
                                Util.putPreference(this, "CallST", login.getString("CallST"));
                                Util.putPreference(this, "CanvasNo", login.getString("CanvasNo"));
                                Util.putPreference(this, "isLogin", true);
                                if (login.has("IsClear")) {
                                    if (login.getInt("IsClear") == 1) {
                                        DatabaseHelper.clearData(this);
                                        com.bhn.sadix.dropper.database.DatabaseHelper.clearData(this);
                                    }
                                }
                                if (login.has("DbSku")) {
                                    Util.putPreference(this, "DbSku", login.getString("DbSku"));
                                }
                                if (login.has("DbCustomer")) {
                                    Util.putPreference(this, "DbCustomer", login.getString("DbCustomer"));
                                }
                                if (login.has("DbAset")) {
                                    Util.putPreference(this, "DbAset", login.getString("DbAset"));
                                }
                                if (login.has("DbDiskon")) {
                                    Util.putPreference(this, "DbDiskon", login.getString("DbDiskon"));
                                }
                                Util.putPreference(this, "isSavePassword", ((CheckBox) findViewById(R.id.ck_save_password)).isChecked());

                                ctrlAppModul.deleteAll();
                                JSONArray AppModul = json.getJSONArray("AppModul");
                                for (int i = 0; i < AppModul.length(); i++) {
                                    JSONObject obj = AppModul.getJSONObject(i);
                                    ctrlAppModul.save(new com.bhn.sadix.model.AppModul(obj.getString("AppModulId"), obj.getString("AppModulValue")));
                                }

                                Util.putPreference(this, "DAY_OF_LOGOUT", login.getString("LoginDate"));

                                Util.putPreference(this, "isLoadData", true);
                                LocationManager locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                                if (ctrlAppModul.isModul("50")) {
                                    if (locMan.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                                        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(this, MenuUtamaActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                } else {
                                    Intent intent = new Intent(this, MenuUtamaActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    } else {
                        Util.showDialogInfo(this, "Username atau password salah");
                    }
                } else {
                    Util.showDialogInfo(this, "Username atau password salah");
                }
            } else if (jenis == 2) {
                if (json.getString("Result").equalsIgnoreCase("OK")) {
                    if (json.getInt("gps") == 1) {
                        Util.putPreference(this, "isGPS", true);
                    } else {
                        Util.putPreference(this, "isGPS", false);
                    }
                    Util.putPreference(this, "PathSvr", json.getString("svr"));
                    if (json.has("ver")) {
                        int ver = json.getInt("ver");
                        try {
                            int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                            if (ver > versionCode) {
                                Util.updateDialog(this, new OkListener() {
                                    @Override
                                    public void onDialogOk() {
                                        Uri uri = Uri.parse("market://details?id=" + getPackageName());
                                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                        // To count with Play market backstack, After pressing back button,
                                        // to taken back to our application, we need to add following flags to intent.
                                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                                        try {
                                            startActivity(goToMarket);
                                        } catch (ActivityNotFoundException e) {
                                            startActivity(new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Util.showDialogInfo(this, json.getString("Message"));
                }
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        final PendingResult<LocationSettingsResult> locationResult = requestLocation();

        locationResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates states = locationSettingsResult.getLocationSettingsStates();

                try {
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                            if (location != null) {
                                Log.d("Get Loc Success - Long", location.getLongitude() + "");
                                Log.d("Get Loc Success - Lat", location.getLatitude() + "");
                                ((TextView) findViewById(R.id.long_lat)).setText("Location:" + String.format("%1$s", location.getLatitude()) + "," + String.format("%1$s", location.getLongitude()));
                            }
                            startLocationUpdates();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(LoginActivity.this, Util.REQUEST_CHECK_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mGoogleApiClient.connect();
                }
            }
        });
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequestHigh, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        retryConnecting();
    }

    public void retryConnecting() {
        mIsInResolution = false;
        if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Toast.makeText(this, "Google Service not connected\n" + result.toString(), Toast.LENGTH_SHORT).show();
        Log.i("google client", "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // Show a localized error dialog.
            GooglePlayServicesUtil.getErrorDialog(
                    result.getErrorCode(), this, 0, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            retryConnecting();
                        }
                    }).show();
            return;
        }
        // If there is an existing resolution error being displayed or a resolution
        // activity has started before, do nothing and wait for resolution
        // progress to be completed.
        if (mIsInResolution) {
            return;
        }
        mIsInResolution = true;
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e("resolution exception", "Exception while starting resolution activity", e);
            retryConnecting();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IN_RESOLUTION, mIsInResolution);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if (location != null) {
            Log.d("Get Loc Success - Long", location.getLongitude() + "");
            Log.d("Get Loc Success - Lat", location.getLatitude() + "");
            ((TextView) findViewById(R.id.long_lat)).setText("Location:" + String.format("%1$s", location.getLatitude()) + "," + String.format("%1$s", location.getLongitude()));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            stopRequestUpdate();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("On Resume Login", "Called");
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
//            startLocationUpdates();
        }
    }

    private void stopRequestUpdate() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}
