package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class CommonDropperModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2957943364620253147L;
	public String DropperID;
	public String SalesId;
	public String WHId;
	public String QRCode;
	public String QRReasonID;
	public String GeoLat;
	public String GeoLong;
	public String LogTime;
	public String BeginDropTime;
	public String EndDropTime;
	public String Description;
	public String Status;
	public CommonDropperModel() {
		Status = "0";
	}
	public CommonDropperModel(String dropperID, String salesId, String wHId,
			String qRCode, String qRReasonID, String geoLat, String geoLong,
			String logTime, String beginDropTime, String endDropTime,
			String description) {
		DropperID = dropperID;
		SalesId = salesId;
		WHId = wHId;
		QRCode = qRCode;
		QRReasonID = qRReasonID;
		GeoLat = geoLat;
		GeoLong = geoLong;
		LogTime = logTime;
		BeginDropTime = beginDropTime;
		EndDropTime = endDropTime;
		Description = description;
		Status = "0";
	}
	public CommonDropperModel(Cursor cursor) {
		DropperID = cursor.getString(0);
		SalesId = cursor.getString(1);
		WHId = cursor.getString(2);
		QRCode = cursor.getString(3);
		QRReasonID = cursor.getString(4);
		GeoLat = cursor.getString(5);
		GeoLong = cursor.getString(6);
		LogTime = cursor.getString(7);
		BeginDropTime = cursor.getString(8);
		EndDropTime = cursor.getString(9);
		Description = cursor.getString(10);
		Status = cursor.getString(11);
	}

}
