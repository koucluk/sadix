package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class EndReturnModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5673738969942315107L;
	public String ReturnID;
	public String SKUId;
	public int Quantity;
	public int Quantity2;
	public String DropperID;
	public EndReturnModel() {
	}
	public EndReturnModel(String returnID, String sKUId, int quantity,
			int quantity2, String dropperID) {
		ReturnID = returnID;
		SKUId = sKUId;
		Quantity = quantity;
		Quantity2 = quantity2;
		DropperID = dropperID;
	}
	public EndReturnModel(Cursor cursor) {
		ReturnID = cursor.getString(0);
		SKUId = cursor.getString(1);
		Quantity = cursor.getInt(2);
		Quantity2 = cursor.getInt(3);
		DropperID = cursor.getString(4);
	}

}
