package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class DropperWHModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6407018192763625094L;
	public String DropperID;
	public String DropperWHID;
	public String WHID;
	public String WHName;
	public String WHAddres;
	public String WHLong;
	public String WHLat;
	public DropperWHModel() {
	}
	public DropperWHModel(String dropperID, String dropperWHID, String wHID,
			String wHName, String wHAddres, String wHLong, String wHLat) {
		DropperID = dropperID;
		DropperWHID = dropperWHID;
		WHID = wHID;
		WHName = wHName;
		WHAddres = wHAddres;
		WHLong = wHLong;
		WHLat = wHLat;
	}
	public DropperWHModel(Cursor cursor) {
		DropperID = cursor.getString(0);
		DropperWHID = cursor.getString(1);
		WHID = cursor.getString(2);
		WHName = cursor.getString(3);
		WHAddres = cursor.getString(4);
		WHLong = cursor.getString(5);
		WHLat = cursor.getString(6);
	}

}
