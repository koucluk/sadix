package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class CustomerDropperModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5090098168319936840L;
	public String DropperID;
	public String CustID;
	public String CustName;
	public String CustAddres;
	public String CustLong;
	public String CustLat;
	public String DateServer;
	public CustomerDropperModel() {
	}
	public CustomerDropperModel(String dropperID, String custID,
			String custName, String custAddres, String custLong,
			String custLat, String dateServer) {
		DropperID = dropperID;
		CustID = custID;
		CustName = custName;
		CustAddres = custAddres;
		CustLong = custLong;
		CustLat = custLat;
		DateServer = dateServer;
	}
	public CustomerDropperModel(Cursor cursor) {
		DropperID = cursor.getString(0);
		CustID = cursor.getString(1);
		CustName = cursor.getString(2);
		CustAddres = cursor.getString(3);
		CustLong = cursor.getString(4);
		CustLat = cursor.getString(5);
		DateServer = cursor.getString(6);
	}

}
