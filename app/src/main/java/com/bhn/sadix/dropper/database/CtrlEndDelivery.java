package com.bhn.sadix.dropper.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.dropper.model.EndDeliveryModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlEndDelivery {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "EndDelivery";
	public CtrlEndDelivery(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(EndDeliveryModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DeliveryID", model.DeliveryID);
			cv.put("DropperID", model.DropperID);
			cv.put("SKUId", model.SKUId);
			cv.put("Quantity", model.Quantity);
			cv.put("Quantity2", model.Quantity2);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(EndDeliveryModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DeliveryID", model.DeliveryID);
			cv.put("Quantity", model.Quantity);
			cv.put("Quantity2", model.Quantity2);
			db.update(table, cv, "DropperID=? and SKUId=?", new String []{model.DropperID, model.SKUId});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public EndDeliveryModel get(String DropperID, String SKUId) {
		EndDeliveryModel modul = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where DropperID=? and SKUId=?", new String[]{DropperID, SKUId});
		if(cursor.moveToNext()) {
			modul = new EndDeliveryModel(cursor);
		}
		cursor.close();
		close();
		return modul;
	}
	public List<EndDeliveryModel> list() {
		List<EndDeliveryModel> list = new ArrayList<EndDeliveryModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new EndDeliveryModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
