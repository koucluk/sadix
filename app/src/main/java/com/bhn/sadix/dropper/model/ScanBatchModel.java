package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class ScanBatchModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8517219197798726730L;
	public String BatchId;
	public String SKUId;
	public String DropperID;
	public ScanBatchModel() {
	}
	public ScanBatchModel(String batchId, String sKUId, String dropperID) {
		BatchId = batchId;
		SKUId = sKUId;
		DropperID = dropperID;
	}
	public ScanBatchModel(Cursor cursor) {
		BatchId = cursor.getString(0);
		SKUId = cursor.getString(1);
		DropperID = cursor.getString(2);
	}

}
