package com.bhn.sadix.dropper.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.dropper.model.ScanBatchModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlScanBatch {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "ScanBatch";
	public CtrlScanBatch(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(ScanBatchModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DropperID", model.DropperID);
			cv.put("BatchId", model.BatchId);
			cv.put("SKUId", model.SKUId);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<ScanBatchModel> list() {
		List<ScanBatchModel> list = new ArrayList<ScanBatchModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new ScanBatchModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deletePerDropperID(String DropperID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where DropperID=?", new String[]{DropperID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
