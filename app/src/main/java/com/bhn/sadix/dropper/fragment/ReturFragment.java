package com.bhn.sadix.dropper.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.dropper.adapter.ReturAdapter;
import com.bhn.sadix.dropper.database.CtrlReturnDropper;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.dropper.model.GroupProdukModel;
import com.bhn.sadix.dropper.model.ProdukModel;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReturFragment extends Fragment {
    private View view = null;
    private Context context;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private View vProduct;
    private View vDropper;
    private ReturAdapter productAdapter;
    private ReturAdapter dropperAdapter;
    private CtrlReturnDropper ctrlReturnDropper;
    private CtrlConfig ctrlConfig;
    private CustomerDropperModel customerDropperModel;
    private DropperWHModel dropperWHModel;

    //	public ReturFragment(Context context, CustomerDropperModel customerDropperModel, DropperWHModel dropperWHModel) {
//		this.context = context;
//		this.customerDropperModel = customerDropperModel;
//		this.dropperWHModel = dropperWHModel;
//		ctrlReturnDropper = new CtrlReturnDropper(context);
//		ctrlConfig = new CtrlConfig(context);
//	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlReturnDropper = new CtrlReturnDropper(context);
        ctrlConfig = new CtrlConfig(context);
    }

    public void setCustomerDropperModel(CustomerDropperModel customerDropperModel) {
        this.customerDropperModel = customerDropperModel;
    }

    public void setDropperWHModel(DropperWHModel dropperWHModel) {
        this.dropperWHModel = dropperWHModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_retur_dropper, container, false);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab_retur)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_return_product) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_retur_dropper) {
                    mPager.setCurrentItem(1);
                }
            }
        });

        mPager = (ViewPager) view.findViewById(R.id.pager_retur);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab_retur)).check(R.id.tab_return_product);
                    loadProduct();
                } else if (position == 1) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab_retur)).check(R.id.tab_retur_dropper);
                    loadDropper();
                }
                super.onPageSelected(position);
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);
        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);

        return view;
    }

    public ReturAdapter getProductAdapter() {
        return productAdapter;
    }

    private void loadDropper() {
        try {
            dropperAdapter = new ReturAdapter(context, new ArrayList<GroupProdukModel>(), new HashMap<GroupProdukModel, List<ProdukModel>>());
            for (int i = 0; i < productAdapter.getGroupCount(); i++) {
                GroupProdukModel groupProdukModel = productAdapter.getGroup(i);
                dropperAdapter.addHeader(groupProdukModel);
                boolean flag = false;
                for (int j = 0; j < productAdapter.getChildrenCount(i); j++) {
                    ProdukModel produkModel = productAdapter.getChild(i, j);
                    if (produkModel.JMLBESAR > 0 || produkModel.JMLKECIL > 0) {
                        dropperAdapter.addChild(groupProdukModel, produkModel);
                        flag = true;
                    }
                }
                if (flag == false) {
                    dropperAdapter.removeHeader(groupProdukModel);
                }
            }
            if (vDropper != null) {
                ((ExpandableListView) vDropper.findViewById(R.id.list)).setAdapter(dropperAdapter);
                dropperAdapter.notifyDataSetInvalidated();
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadProduct() {
        if (productAdapter == null) {
            productAdapter = new ReturAdapter(context, new ArrayList<GroupProdukModel>(), new HashMap<GroupProdukModel, List<ProdukModel>>());
            List<GroupProdukModel> listGroup = ctrlReturnDropper.listGroupProduk(customerDropperModel);
            for (GroupProdukModel groupProdukModel : listGroup) {
                List<ProdukModel> listProduct = ctrlReturnDropper.listProduk(groupProdukModel, customerDropperModel, dropperWHModel);
                productAdapter.addHeader(groupProdukModel);
                productAdapter.addChild(groupProdukModel, listProduct);
            }
        }
        ((ExpandableListView) vProduct.findViewById(R.id.list)).setAdapter(productAdapter);
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);
            if (position == 0) {
                vProduct = v;
                vProduct.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vProduct.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        addJumlah(productAdapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                loadProduct();
            } else if (position == 1) {
                vDropper = v;
                vDropper.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vDropper.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        addJumlah(dropperAdapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                loadDropper();
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    private void addJumlah(final ProdukModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_dropper);

        dialog.setTitle(model.ProductName);
        ((Spinner) dialog.findViewById(R.id.DropperReason)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("REASON_NO_VISIT")));
        if (model.DropperReason != null) {
            int index = ((ComboBoxAdapter) ((Spinner) dialog.findViewById(R.id.DropperReason)).getAdapter()).indexOf(new ComboBoxModel(model.DropperReason));
            ((Spinner) dialog.findViewById(R.id.DropperReason)).setSelection(index);
        }
        ((EditText) dialog.findViewById(R.id.Note)).setText(model.Note);
        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(String.valueOf(model.QBESAR));
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(String.valueOf(model.QKECIL));
        ((EditText) dialog.findViewById(R.id.jml_1)).setHint(model.SBESAR);
        ((EditText) dialog.findViewById(R.id.jml_2)).setHint(model.SKECIL);

        if (model.JMLBESAR > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.JMLBESAR));
        }
        if (model.JMLKECIL > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.JMLKECIL));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                int JMLBESAR = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                int JMLKECIL = Integer.parseInt(cek.equals("") ? "0" : cek);
                model.JMLBESAR = JMLBESAR;
                model.JMLKECIL = JMLKECIL;
                model.DropperReason = ((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.DropperReason)).getSelectedItem()).value;
                model.Note = ((EditText) dialog.findViewById(R.id.Note)).getText().toString();
                productAdapter.notifyDataSetChanged();
                dropperAdapter.notifyDataSetChanged();
                calculateAmount();
                loadDropper();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void calculateAmount() {
        int JMLBESAR = 0;
        int JMLKECIL = 0;
        for (int i = 0; i < productAdapter.getGroupCount(); i++) {
            for (int j = 0; j < productAdapter.getChildrenCount(i); j++) {
                ProdukModel produkModel = productAdapter.getChild(i, j);
                JMLBESAR += produkModel.JMLBESAR;
                JMLKECIL += produkModel.JMLKECIL;
            }
        }
        if (view != null) {
            ((TextView) view.findViewById(R.id.total_retur)).setText(JMLBESAR + "," + JMLKECIL);
        }
    }

}
