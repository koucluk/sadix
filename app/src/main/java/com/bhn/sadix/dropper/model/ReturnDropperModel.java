package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class ReturnDropperModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7289842118871524912L;
	public String DropperWHID;
	public String ReturnID;
	public String SKUId;
	public String GroupName;
	public String ProductName;
	public String SBESAR;
	public String SKECIL; 
	public int QBESAR;
	public int QKECIL;
	public String DropperID;
	public ReturnDropperModel() {
	}
	public ReturnDropperModel(String dropperWHID, String returnID,
			String sKUId, String groupName, String productName, String sBESAR,
			String sKECIL, int qBESAR, int qKECIL, String dropperID) {
		DropperWHID = dropperWHID;
		ReturnID = returnID;
		SKUId = sKUId;
		GroupName = groupName;
		ProductName = productName;
		SBESAR = sBESAR;
		SKECIL = sKECIL;
		QBESAR = qBESAR;
		QKECIL = qKECIL;
		DropperID = dropperID;
	}
	public ReturnDropperModel(Cursor cursor) {
		DropperWHID = cursor.getString(0);
		ReturnID = cursor.getString(1);
		SKUId = cursor.getString(2);
		GroupName = cursor.getString(3);
		ProductName = cursor.getString(4);
		SBESAR = cursor.getString(5);
		SKECIL = cursor.getString(6);
		QBESAR = cursor.getInt(7);
		QKECIL = cursor.getInt(8);
		DropperID = cursor.getString(9);
	}

}
