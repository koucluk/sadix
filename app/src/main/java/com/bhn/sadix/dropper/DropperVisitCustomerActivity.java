package com.bhn.sadix.dropper;

import java.util.ArrayList;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class DropperVisitCustomerActivity extends AppCompatActivity implements OnItemClickListener {
    private CustomerDropperModel customerDropperModel;
    private DropperWHModel dropperWHModel;
    private MenuUtamaAdapter adapter;
    private CtrlConfig ctrlConfig;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropper_visit_customer);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Scan QR Code", getResources().getDrawable(R.drawable.scan_qr_code)));
        adapter.add(new MenuUtamaModel(2, "By Pass QR Code", getResources().getDrawable(R.drawable.by_pass_scan_qr)));
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
        customerDropperModel = (CustomerDropperModel) getIntent().getSerializableExtra("CustomerDropperModel");
        if (customerDropperModel != null) {
            ((TextView) findViewById(R.id.CustomerName)).setText(customerDropperModel.CustName + " - " + customerDropperModel.CustID);
            ((TextView) findViewById(R.id.CustomerAddress)).setText(customerDropperModel.CustAddres);
        }
        dropperWHModel = (DropperWHModel) getIntent().getSerializableExtra("DropperWHModel");
        if (dropperWHModel != null) {
            ((TextView) findViewById(R.id.GudangName)).setText(dropperWHModel.WHName);
            ((TextView) findViewById(R.id.GudangAddress)).setText(dropperWHModel.WHAddres);
        }
        ctrlConfig = new CtrlConfig(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        if (model.id == 2) {
            byPassQRCodeDialog();
        } else if (model.id == 1) {
            scanBarcode();
        }
    }

    private void scanBarcode() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String kode = intent.getStringExtra("SCAN_RESULT");
//		        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                String[] pecahKode = kode.split("[|]");
                if (pecahKode.length == 3) {
                    if (pecahKode[0].equalsIgnoreCase(UserModel.getInstance(this).CompanyID) && pecahKode[2].equalsIgnoreCase(customerDropperModel.CustID)) {
                        Intent kunjungan = new Intent(this, DropperKunjunganActivity.class);
                        kunjungan.putExtra("CustomerDropperModel", customerDropperModel);
                        kunjungan.putExtra("DropperWHModel", dropperWHModel);
                        startActivity(kunjungan);
                        finish();
                    } else {
                        Util.Toast(this, "Barcode tidak sama");
                    }
                }
            }
        }
    }

    private void byPassQRCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("By Pass Reason");
        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_BC_NOSCAN")));
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Intent intent = new Intent(DropperVisitCustomerActivity.this, DropperKunjunganActivity.class);
                intent.putExtra("ComboBoxModel", (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem());
                intent.putExtra("CustomerDropperModel", customerDropperModel);
                intent.putExtra("DropperWHModel", dropperWHModel);
                startActivity(intent);
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
