package com.bhn.sadix.dropper.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.bhn.sadix.R;
import com.bhn.sadix.dropper.model.DropperWHModel;

public class EditGudangFragment extends Fragment {
	private View view = null;
	private Context context;
	private DropperWHModel dropperWHModel;
//	public EditGudangFragment(Context context, DropperWHModel dropperWHModel) {
//		this.context = context;
//		this.dropperWHModel = dropperWHModel;
//	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.context = getActivity();
	}
	public void setDropperWHModel(DropperWHModel dropperWHModel) {
		this.dropperWHModel = dropperWHModel;
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_gudang, container, false);
        ((Button)view.findViewById(R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				save();
			}
		});
        return view;
    }
	@Override
	public void onPause() {
		super.onPause();
		setData();
	}
	public void setData() {
		if(view != null) {
			dropperWHModel.WHName = ((EditText)view.findViewById(R.id.WHName)).getText().toString();
			dropperWHModel.WHAddres = ((EditText)view.findViewById(R.id.WHAddres)).getText().toString();
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		setValue();
	}
	private void setValue() {
		if(view != null) {
			((EditText)view.findViewById(R.id.WHName)).setText(dropperWHModel.WHName);
			((EditText)view.findViewById(R.id.WHAddres)).setText(dropperWHModel.WHAddres);
		}
	}
	private void save() {
		
	}

}
