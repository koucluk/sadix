package com.bhn.sadix.dropper.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

public class ProdukButchModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8793991200028247718L;
	public GroupProdukModel groupProdukModel;
	public String SKUId;
	public String ProductName;
	public List<String> list =  new ArrayList<String>();
	public ProdukButchModel() {
	}
	public ProdukButchModel(GroupProdukModel groupProdukModel, Cursor cursor) {
		this.groupProdukModel = groupProdukModel;
		SKUId = cursor.getString(0);
		ProductName = cursor.getString(1);
	}

}
