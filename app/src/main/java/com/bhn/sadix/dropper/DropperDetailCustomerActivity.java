package com.bhn.sadix.dropper;

import java.util.ArrayList;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.util.Util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class DropperDetailCustomerActivity extends AppCompatActivity implements OnItemClickListener {
    private CustomerDropperModel customerDropperModel;
    private DropperWHModel dropperWHModel;
    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
//        	commonModel.GeoLat = String.format("%1$s", location.getLatitude());
//        	commonModel.GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private MenuUtamaAdapter adapter;
    private CtrlConfig ctrlConfig;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropper_detail_customer);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Visit", getResources().getDrawable(R.drawable.visit)));
        adapter.add(new MenuUtamaModel(2, "Not Visit", getResources().getDrawable(R.drawable.not_visit)));
        adapter.add(new MenuUtamaModel(3, "Location", getResources().getDrawable(R.drawable.location)));
//        adapter.add(new MenuUtamaModel(4, "Show Photo", getResources().getDrawable(R.drawable.show_photo)));
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
        customerDropperModel = (CustomerDropperModel) getIntent().getSerializableExtra("CustomerDropperModel");
        if (customerDropperModel != null) {
            ((TextView) findViewById(R.id.CustomerName)).setText(customerDropperModel.CustName + " - " + customerDropperModel.CustID);
            ((TextView) findViewById(R.id.CustomerAddress)).setText(customerDropperModel.CustAddres);
        }
        dropperWHModel = (DropperWHModel) getIntent().getSerializableExtra("DropperWHModel");
        if (dropperWHModel != null) {
            ((TextView) findViewById(R.id.GudangName)).setText(dropperWHModel.WHName);
            ((TextView) findViewById(R.id.GudangAddress)).setText(dropperWHModel.WHAddres);
        }
        ctrlConfig = new CtrlConfig(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        if (model.id == 2) {
            notVisitDialog();
        } else if (model.id == 1) {
            Intent intent = new Intent(this, DropperVisitCustomerActivity.class);
            intent.putExtra("CustomerDropperModel", customerDropperModel);
            intent.putExtra("DropperWHModel", dropperWHModel);
            startActivity(intent);
            finish();
        } else if (model.id == 3) {
            if (location != null) {
                String uri = "http://maps.google.com/maps?saddr=" + String.format("%1$s", location.getLatitude()) +
                        "," + String.format("%1$s", location.getLongitude()) + "&daddr=" + dropperWHModel.WHLat +
                        "," + dropperWHModel.WHLong + "&mode=driving";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                finish();
            } else {
                Util.showDialogInfo(this, "Lokasi anda belum terdeteksi GPS!");
            }
        } //else if(model.id == 4) {
//			Intent intent = new Intent(this, ShowPhotoActivity.class);
//			intent.putExtra("CustomerModel", customerModel);
//			intent.putExtra("GudangModel", gudangModel);
//			startActivity(intent);
//			finish();
//		}
    }

    private void notVisitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("Not Visit Reason");
        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_NO_VISIT")));
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
