package com.bhn.sadix.dropper;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by map05 on 2/8/2017.
 */

public class CustomFormatter implements IValueFormatter {

    private DecimalFormat mFormat;

    public CustomFormatter() {
        mFormat = new DecimalFormat("###,###,##0.00"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        // write your logic here
        return mFormat.format(value) + " %"; // e.g. append a dollar-sign
    }
}