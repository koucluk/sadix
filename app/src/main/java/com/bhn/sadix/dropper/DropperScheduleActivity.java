package com.bhn.sadix.dropper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.dropper.adapter.DropperSchedulAdapter;
import com.bhn.sadix.dropper.database.CtrlCustomerDropper;
import com.bhn.sadix.dropper.database.CtrlDropperWH;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.makeramen.segmented.SegmentedRadioGroup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class DropperScheduleActivity extends AppCompatActivity implements OnItemClickListener {
    private ViewPager mPager;
    private View vScheduler;
    private View vAll;
    private ViewPagerAdapter pagerAdapter;
    private DropperSchedulAdapter adapterAll;
    private DropperSchedulAdapter adapterScheduler;
    private CtrlCustomerDropper ctrlCustomerDropper;
    private CtrlDropperWH ctrlDropperWH;
    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropper_schedule);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

/*
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ctrlCustomerDropper = new CtrlCustomerDropper(this);
        ctrlDropperWH = new CtrlDropperWH(this);

        ((SegmentedRadioGroup) findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_day) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_all_cust) {
                    mPager.setCurrentItem(1);
                }
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_day);
                } else if (position == 1) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_all_cust);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);
        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
    }

    private void loadAll() {
        if (vAll != null) {
            adapterAll = new DropperSchedulAdapter(this, new ArrayList<CustomerDropperModel>(), new HashMap<CustomerDropperModel, List<DropperWHModel>>());
//			List<CustomerDropperModel> list = ctrlCustomerDropper.list();
//			for (CustomerDropperModel customerDropperModel : list) {
//				adapterAll.addHeader(customerDropperModel);
//				List<DropperWHModel> listWH = ctrlDropperWH.list(customerDropperModel);
//				for (DropperWHModel dropperWHModel : listWH) {
//					adapterAll.addChild(customerDropperModel, dropperWHModel);
//				}
//			}
            ((ExpandableListView) vAll.findViewById(R.id.list)).setAdapter(adapterAll);
        }
    }

    private void loadSchedule() {
        if (vScheduler != null) {
            if (adapterScheduler == null) {
                adapterScheduler = new DropperSchedulAdapter(this, new ArrayList<CustomerDropperModel>(), new HashMap<CustomerDropperModel, List<DropperWHModel>>());
                List<CustomerDropperModel> list = ctrlCustomerDropper.list();
                for (CustomerDropperModel customerDropperModel : list) {
                    adapterScheduler.addHeader(customerDropperModel);
                    List<DropperWHModel> listWH = ctrlDropperWH.list(customerDropperModel);
                    for (DropperWHModel dropperWHModel : listWH) {
                        adapterScheduler.addChild(customerDropperModel, dropperWHModel);
                    }
                }
                ((ExpandableListView) vScheduler.findViewById(R.id.list)).setAdapter(adapterScheduler);
            }
        }
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.item_dropper_schedule, null);
            if (position == 0) {
                vScheduler = v;
                ((ExpandableListView) vScheduler.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        CustomerDropperModel customerDropperModel = adapterScheduler.getGroup(groupPosition);
                        DropperWHModel dropperWHModel = adapterScheduler.getChild(groupPosition, childPosition);
                        pilihGudang(customerDropperModel, dropperWHModel);
                        return false;
                    }
                });
                loadSchedule();
            } else if (position == 1) {
                vAll = v;
                ((ExpandableListView) vAll.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        CustomerDropperModel customerDropperModel = adapterAll.getGroup(groupPosition);
                        DropperWHModel dropperWHModel = adapterAll.getChild(groupPosition, childPosition);
                        pilihGudang(customerDropperModel, dropperWHModel);
                        return false;
                    }
                });
                loadAll();
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    private void pilihGudang(CustomerDropperModel customerDropperModel, DropperWHModel dropperWHModel) {
        Intent intent = new Intent(this, DropperDetailCustomerActivity.class);
        intent.putExtra("CustomerDropperModel", customerDropperModel);
        intent.putExtra("DropperWHModel", dropperWHModel);
        startActivity(intent);
    }

}
