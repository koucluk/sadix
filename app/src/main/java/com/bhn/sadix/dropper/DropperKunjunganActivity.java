package com.bhn.sadix.dropper;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.dropper.adapter.BatchAdapter;
import com.bhn.sadix.dropper.adapter.DropperAdapter;
import com.bhn.sadix.dropper.adapter.ReturAdapter;
import com.bhn.sadix.dropper.database.CtrlCommonDropper;
import com.bhn.sadix.dropper.database.CtrlEndDelivery;
import com.bhn.sadix.dropper.database.CtrlEndReturn;
import com.bhn.sadix.dropper.database.CtrlScanBatch;
import com.bhn.sadix.dropper.fragment.DropperFragment;
import com.bhn.sadix.dropper.fragment.EditGudangFragment;
import com.bhn.sadix.dropper.fragment.NoteFragment;
import com.bhn.sadix.dropper.fragment.ReturFragment;
import com.bhn.sadix.dropper.fragment.ScanBatchFragment;
import com.bhn.sadix.dropper.fragment.ScanBatchFragment.ScanBatchListener;
import com.bhn.sadix.dropper.model.CommonDropperModel;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.dropper.model.EndDeliveryModel;
import com.bhn.sadix.dropper.model.EndReturnModel;
import com.bhn.sadix.dropper.model.ProdukButchModel;
import com.bhn.sadix.dropper.model.ProdukModel;
import com.bhn.sadix.dropper.model.ScanBatchModel;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class DropperKunjunganActivity extends AppCompatActivity implements OnItemClickListener, ScanBatchListener {
    private CustomerDropperModel customerDropperModel;
    private DropperWHModel dropperWHModel;
    private ComboBoxModel reasonNoScanBarcode;
    private CommonDropperModel commonDropperModel;

    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private MenuUtamaAdapter adapter;
    private final int SCAN_BATCH = 1;

    private DropperFragment dropperFragment;
    private ReturFragment returFragment;
    private ScanBatchFragment scanBatchFragment;
    private NoteFragment noteFragment;
    private EditGudangFragment editGudangFragment;

    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            commonDropperModel.GeoLat = String.format("%1$s", location.getLatitude());
            commonDropperModel.GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private ConnectionServer server;
    private CtrlCommonDropper ctrlCommonDropper;
    private ProgressDialog progressDialog;
    private CtrlEndDelivery ctrlEndDelivery;
    private CtrlEndReturn ctrlEndReturn;
    private CtrlScanBatch ctrlScanBatch;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropper_kunjungan);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        customerDropperModel = (CustomerDropperModel) getIntent().getSerializableExtra("CustomerDropperModel");
        dropperWHModel = (DropperWHModel) getIntent().getSerializableExtra("DropperWHModel");
        reasonNoScanBarcode = (ComboBoxModel) getIntent().getSerializableExtra("ComboBoxModel");
        if (customerDropperModel != null) {
            ((TextView) findViewById(R.id.CustomerName)).setText(customerDropperModel.CustName + " - " + customerDropperModel.CustID);
            ((TextView) findViewById(R.id.CustomerAddress)).setText(customerDropperModel.CustAddres);
        }
        if (dropperWHModel != null) {
            ((TextView) findViewById(R.id.GudangName)).setText(dropperWHModel.WHName);
            ((TextView) findViewById(R.id.GudangAddress)).setText(dropperWHModel.WHName);
        }
        server = new ConnectionServer(this);
        ctrlCommonDropper = new CtrlCommonDropper(this);
        ctrlEndDelivery = new CtrlEndDelivery(this);
        ctrlEndReturn = new CtrlEndReturn(this);
        ctrlScanBatch = new CtrlScanBatch(this);

        commonDropperModel = (CommonDropperModel) getIntent().getSerializableExtra("CommonDropperModel");
        if (commonDropperModel == null) {
            commonDropperModel = new CommonDropperModel();
            commonDropperModel.DropperID = customerDropperModel.DropperID;
            commonDropperModel.SalesId = UserModel.getInstance(this).SalesId;
            commonDropperModel.WHId = dropperWHModel.DropperWHID;
            commonDropperModel.BeginDropTime = Util.dateFormat.format(new Date());
            if (reasonNoScanBarcode != null) {
                commonDropperModel.QRCode = "2";
                commonDropperModel.QRReasonID = reasonNoScanBarcode.value;
            } else {
                commonDropperModel.QRCode = "1";
                commonDropperModel.QRReasonID = "0";
            }
        }

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Dropper", getResources().getDrawable(R.drawable.information)));
        adapter.add(new MenuUtamaModel(2, "Retur", getResources().getDrawable(R.drawable.information)));
        adapter.add(new MenuUtamaModel(3, "Edit Wirehouse", getResources().getDrawable(R.drawable.information)));
        adapter.add(new MenuUtamaModel(4, "Scan Batch", getResources().getDrawable(R.drawable.information)));
        adapter.add(new MenuUtamaModel(5, "Note", getResources().getDrawable(R.drawable.information)));
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(this);
        mTitle = mDrawerTitle = getTitle();
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                super.onDrawerOpened(drawerView);
            }
        };

        dropperFragment = new DropperFragment();//new DropperFragment(this, customerDropperModel, dropperWHModel);
        dropperFragment.setCustomerDropperModel(customerDropperModel);
        dropperFragment.setDropperWHModel(dropperWHModel);
        returFragment = new ReturFragment();//new ReturFragment(this, customerDropperModel, dropperWHModel);
        returFragment.setCustomerDropperModel(customerDropperModel);
        returFragment.setDropperWHModel(dropperWHModel);
        scanBatchFragment = new ScanBatchFragment();/*new ScanBatchFragment(this, customerDropperModel) {
            @Override
			public void addBatch() {
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			    startActivityForResult(intent, SCAN_BATCH);
			}
		};*/
        scanBatchFragment.setListener(this);
        scanBatchFragment.setCustomerDropperModel(customerDropperModel);
        noteFragment = new NoteFragment();//new NoteFragment(this);
        editGudangFragment = new EditGudangFragment();//new EditGudangFragment(this, dropperWHModel);
        editGudangFragment.setDropperWHModel(dropperWHModel);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (savedInstanceState == null) {
            mTitle = adapter.getItem(0).menu;
            selectItem(adapter.getItem(0).id);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        close();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Check out")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Check out");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        } else if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
            chekOutOrder();
        }
        return super.onOptionsItemSelected(item);
    }

    private void chekOutOrder() {
        Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    new Checkout().execute();
                }
            }
        });
        ;
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan dropper?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    finish();
                }
            }
        });
    }

    private void selectItem(int position) {
        getSupportFragmentManager().popBackStack();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 1:
                ft.replace(R.id.content_frame, dropperFragment);
                break;
            case 2:
                ft.replace(R.id.content_frame, returFragment);
                break;
            case 4:
                ft.replace(R.id.content_frame, scanBatchFragment);
                break;
            case 5:
                ft.replace(R.id.content_frame, noteFragment);
                break;
            case 3:
                ft.replace(R.id.content_frame, editGudangFragment);
                break;
        }
        ft.addToBackStack(null);
        ft.commit();
        mDrawerList.setItemChecked(position, true);
        setTitle(mTitle);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        mTitle = adapter.getItem(position).menu;
        selectItem(adapter.getItem(position).id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		Util.showDialogInfo(this, "requestCode="+requestCode+",resultCode="+resultCode+",intent="+intent);
        if (requestCode == SCAN_BATCH || requestCode == 65537) {
            if (resultCode == RESULT_OK) {
                String kode = intent.getStringExtra("SCAN_RESULT");
//		        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                scanBatchFragment.addBatch(kode);
            }
        }
    }

    private class Checkout extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            String tanggal = Util.dateFormat.format(new Date());
            try {
                commonDropperModel.Description = noteFragment.getNote();
                commonDropperModel.EndDropTime = tanggal;
                commonDropperModel.GeoLat = location != null ? String.format("%1$s", location.getLatitude()) : "0";
                commonDropperModel.GeoLong = location != null ? String.format("%1$s", location.getLongitude()) : "0";
                commonDropperModel.LogTime = Util.dateFormat.format(location != null ? new Date(location.getTime()) : new Date());

                JSONObject CommonDropper = new JSONObject();
                CommonDropper.put("BeginDropTime", commonDropperModel.BeginDropTime);
                CommonDropper.put("GeoLat", Double.parseDouble(commonDropperModel.GeoLat.equals("") ? "0" : commonDropperModel.GeoLat));
                CommonDropper.put("DropperID", Integer.parseInt(commonDropperModel.DropperID));
                CommonDropper.put("GeoLong", Double.parseDouble(commonDropperModel.GeoLong.equals("") ? "0" : commonDropperModel.GeoLong));
                CommonDropper.put("EndDropTime", commonDropperModel.EndDropTime);
                CommonDropper.put("LogTime", commonDropperModel.LogTime);
                CommonDropper.put("SalesId", Integer.parseInt(commonDropperModel.SalesId));
                CommonDropper.put("QRCode", Integer.parseInt(commonDropperModel.QRCode));
                CommonDropper.put("Description", commonDropperModel.Description);
                CommonDropper.put("WHId", Integer.parseInt(commonDropperModel.WHId));
                CommonDropper.put("QRReasonID", Integer.parseInt(commonDropperModel.QRReasonID));

                JSONArray EndDelivery = new JSONArray();
                DropperAdapter dropperAdapter = dropperFragment.getDropperAdapter();
                for (int i = 0; i < dropperAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < dropperAdapter.getChildrenCount(i); j++) {
                        ProdukModel produkModel = dropperAdapter.getChild(i, j);
                        if (produkModel.JMLBESAR > 0 || produkModel.JMLKECIL > 0) {
                            JSONObject json = new JSONObject();
                            json.put("DeliveryID", Integer.parseInt(produkModel.DeliveryID));
                            json.put("SKUId", Integer.parseInt(produkModel.SKUId));
                            json.put("Quantity", produkModel.JMLBESAR);
                            json.put("Quantity2", produkModel.JMLKECIL);
                            EndDelivery.put(json);
                        }
                    }
                }

                JSONArray EndReturn = new JSONArray();
                ReturAdapter returAdapter = returFragment.getProductAdapter();
                for (int i = 0; i < returAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < returAdapter.getChildrenCount(i); j++) {
                        ProdukModel produkModel = returAdapter.getChild(i, j);
                        if (produkModel.JMLBESAR > 0 || produkModel.JMLKECIL > 0) {
                            JSONObject json = new JSONObject();
                            json.put("ReturnID", Integer.parseInt(produkModel.DeliveryID));
                            json.put("SKUId", Integer.parseInt(produkModel.SKUId));
                            json.put("Quantity", produkModel.JMLBESAR);
                            json.put("Quantity2", produkModel.JMLKECIL);
                            EndReturn.put(json);
                        }
                    }
                }

                JSONArray ScanBatch = new JSONArray();
                BatchAdapter batchAdapter = scanBatchFragment.getAdapter();
                for (int i = 0; i < batchAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < batchAdapter.getChildrenCount(i); j++) {
                        ProdukButchModel produkButchModel = batchAdapter.getChild(i, j);
                        for (String BatchId : produkButchModel.list) {
                            JSONObject json = new JSONObject();
                            json.put("SKUId", Integer.parseInt(produkButchModel.SKUId));
                            json.put("BatchId", BatchId);
                            ScanBatch.put(json);
                        }
                    }
                }

                JSONObject jsonSend = new JSONObject();
                jsonSend.put("CommonDropper", CommonDropper);
                jsonSend.put("EndDelivery", EndDelivery);
                jsonSend.put("EndReturn", EndReturn);
                jsonSend.put("ScanBatch", ScanBatch);
                saveLokal();

                server.setUrl(Util.getServerUrl(DropperKunjunganActivity.this) + "endvisit");
                result = server.requestJSONObjectNonThread(jsonSend);
                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        commonDropperModel.Status = "1";
                        ctrlCommonDropper.update(commonDropperModel);
                        result = jsonRespon.getString("Message");
                    } else {
                        result = null;
                    }
                }
            } catch (Exception e) {
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.Toast(DropperKunjunganActivity.this, result);
            } else {
                Util.okeDialog(DropperKunjunganActivity.this, "Info", "Koneksi gagal, data akan tersimpan diinternal memory", new OkListener() {
                    @Override
                    public void onDialogOk() {
                        finish();
                    }
                });
            }
        }
    }

    private void saveLokal() {
        try {
            ctrlCommonDropper.save(commonDropperModel);
            DropperAdapter dropperAdapter = dropperFragment.getDropperAdapter();
            for (int i = 0; i < dropperAdapter.getGroupCount(); i++) {
                for (int j = 0; j < dropperAdapter.getChildrenCount(i); j++) {
                    ProdukModel produkModel = dropperAdapter.getChild(i, j);
                    if (produkModel.JMLBESAR > 0 || produkModel.JMLKECIL > 0) {
                        ctrlEndDelivery.save(new EndDeliveryModel(
                                produkModel.DeliveryID,
                                produkModel.SKUId,
                                produkModel.JMLBESAR,
                                produkModel.JMLKECIL,
                                customerDropperModel.DropperID
                        ));
                    }
                }
            }
            ReturAdapter returAdapter = returFragment.getProductAdapter();
            for (int i = 0; i < returAdapter.getGroupCount(); i++) {
                for (int j = 0; j < returAdapter.getChildrenCount(i); j++) {
                    ProdukModel produkModel = returAdapter.getChild(i, j);
                    if (produkModel.JMLBESAR > 0 || produkModel.JMLKECIL > 0) {
                        ctrlEndReturn.save(new EndReturnModel(
                                produkModel.DeliveryID,
                                produkModel.SKUId,
                                produkModel.JMLBESAR,
                                produkModel.JMLKECIL,
                                customerDropperModel.DropperID
                        ));
                    }
                }
            }
            BatchAdapter batchAdapter = scanBatchFragment.getAdapter();
            for (int i = 0; i < batchAdapter.getGroupCount(); i++) {
                for (int j = 0; j < batchAdapter.getChildrenCount(i); j++) {
                    ProdukButchModel produkButchModel = batchAdapter.getChild(i, j);
                    for (String BatchId : produkButchModel.list) {
                        ctrlScanBatch.save(new ScanBatchModel(
                                BatchId,
                                produkButchModel.SKUId,
                                customerDropperModel.DropperID
                        ));
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void addBatch() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, SCAN_BATCH);
    }

}
