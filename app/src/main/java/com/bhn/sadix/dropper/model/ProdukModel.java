package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class ProdukModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -131519862977975473L;
	public GroupProdukModel groupProdukModel;
	public String SKUId;
	public String ProductName;
	public String SBESAR;
	public String SKECIL; 
	public int QBESAR;
	public int QKECIL;
	public int JMLBESAR;
	public int JMLKECIL;
	public String DropperReason;
	public String Note;
	public String DeliveryID;
	public ProdukModel() {
	}
	public ProdukModel(GroupProdukModel groupProdukModel, String sKUId,
			String productName, String sBESAR, String sKECIL,
			int qBESAR, int qKECIL, String deliveryID) {
		this.groupProdukModel = groupProdukModel;
		SKUId = sKUId;
		ProductName = productName;
		SBESAR = sBESAR;
		SKECIL = sKECIL;
		QBESAR = qBESAR;
		QKECIL = qKECIL;
		DeliveryID = deliveryID;
	}
	public ProdukModel(GroupProdukModel groupProdukModel, Cursor cursor) {
		this.groupProdukModel = groupProdukModel;
		SKUId = cursor.getString(0);
		ProductName = cursor.getString(1);
		SBESAR = cursor.getString(2);
		SKECIL = cursor.getString(3);
		QBESAR = cursor.getInt(4);
		QKECIL = cursor.getInt(5);
		DeliveryID = cursor.getString(6);
	}

}
