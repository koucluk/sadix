package com.bhn.sadix.dropper.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String dbName="DropperDB.db";

	public DatabaseHelper(Context context) {
		super(context, dbName, null,1);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE CustomerDropper (DropperID varchar(50),CustID varchar(50),CustName varchar(100)," +
				"CustAddres varchar(200),CustLong varchar(50),CustLat varchar(50),DateServer varchar(50))");
		db.execSQL("CREATE TABLE DropperWH (DropperID varchar(50),DropperWHID varchar(50),WHID varchar(50)," +
				"WHName varchar(100),WHAddres varchar(200),WHLong varchar(50),WHLat varchar(50))");
		db.execSQL("CREATE TABLE DeliveryDropper (DropperWHID varchar(50),DeliveryID varchar(50)," +
				"SKUId varchar(50),GroupName varchar(100),ProductName varchar(100),SBESAR varchar(50),SKECIL varchar(50)," +
				"QBESAR float,QKECIL float,DropperID varchar(50))");
		db.execSQL("CREATE TABLE ReturnDropper (DropperWHID varchar(50),ReturnID varchar(50)," +
				"SKUId varchar(50),GroupName varchar(100),ProductName varchar(100),SBESAR varchar(50),SKECIL varchar(50)," +
				"QBESAR float,QKECIL float,DropperID varchar(50))");
		
		db.execSQL("CREATE TABLE CommonDropper (DropperID varchar(50),SalesId varchar(50)," +
				"WHId varchar(50),QRCode varchar(5),QRReasonID varchar(5),GeoLat varchar(50),GeoLong varchar(50)," +
				"LogTime varchar(50),BeginDropTime varchar(50),EndDropTime varchar(50),Description varchar(200)," +
				"Status varchar(5))");
		db.execSQL("CREATE TABLE EndDelivery (DeliveryID varchar(50),SKUId varchar(50)," +
				"Quantity float,Quantity2 float,DropperID varchar(50))");
		db.execSQL("CREATE TABLE EndReturn (ReturnID varchar(50),SKUId varchar(50)," +
				"Quantity float,Quantity2 float,DropperID varchar(50))");
		db.execSQL("CREATE TABLE ScanBatch (BatchId varchar(100),SKUId varchar(50),DropperID varchar(50))");
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	public static void clearData(Context context) {
		DatabaseHelper ctrlDb = new DatabaseHelper(context);
		SQLiteDatabase db = null;
		try {
			db = ctrlDb.getWritableDatabase();
	        db.execSQL("delete from CustomerDropper");
	        db.execSQL("delete from DropperWH");
	        db.execSQL("delete from DeliveryDropper");
	        db.execSQL("delete from ReturnDropper");
	        db.execSQL("delete from CommonDropper");
	        db.execSQL("delete from EndDelivery");
	        db.execSQL("delete from EndReturn");
	        db.execSQL("delete from ScanBatch");
	        db.close();
		} catch (Exception e) {
		}
	}
	public static void clearDataMaster(Context context) {
		DatabaseHelper ctrlDb = new DatabaseHelper(context);
		SQLiteDatabase db = null;
		try {
			db = ctrlDb.getWritableDatabase();
	        db.execSQL("delete from CustomerDropper");
	        db.execSQL("delete from DropperWH");
	        db.execSQL("delete from DeliveryDropper");
	        db.execSQL("delete from ReturnDropper");
	        db.close();
		} catch (Exception e) {
		}
	}
}
