package com.bhn.sadix.dropper.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bhn.sadix.R;

public class NoteFragment extends Fragment {
	private View view = null;
	private String Note;
	private Context context;
//	public NoteFragment(Context context) {
//		this.context = context;
//	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.context = getActivity();
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_note_dropper, container, false);
        return view;
    }
	@Override
	public void onPause() {
		super.onPause();
		if(view != null) {
			Note = ((EditText)view.findViewById(R.id.Note)).getText().toString();
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		if(view != null) {
			((EditText)view.findViewById(R.id.Note)).setText(Note);
		}
	}
	public String getNote() {
		if(view != null) {
			Note = ((EditText)view.findViewById(R.id.Note)).getText().toString();
		}
		return Note;
	}

}
