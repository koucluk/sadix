package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class EndDeliveryModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 341489051406111889L;
	public String DeliveryID;
	public String SKUId;
	public int Quantity;
	public int Quantity2;
	public String DropperID;
	public EndDeliveryModel() {
	}
	public EndDeliveryModel(String deliveryID, String sKUId, int quantity,
			int quantity2, String dropperID) {
		DeliveryID = deliveryID;
		SKUId = sKUId;
		Quantity = quantity;
		Quantity2 = quantity2;
		DropperID = dropperID;
	}
	public EndDeliveryModel(Cursor cursor) {
		DeliveryID = cursor.getString(0);
		SKUId = cursor.getString(1);
		Quantity = cursor.getInt(2);
		Quantity2 = cursor.getInt(3);
		DropperID = cursor.getString(4);
	}

}
