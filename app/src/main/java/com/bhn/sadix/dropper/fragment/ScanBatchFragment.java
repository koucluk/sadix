package com.bhn.sadix.dropper.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.bhn.sadix.R;
import com.bhn.sadix.dropper.adapter.BatchAdapter;
import com.bhn.sadix.dropper.database.CtrlDeliveryDropper;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.GroupProdukModel;
import com.bhn.sadix.dropper.model.ProdukButchModel;

public class ScanBatchFragment extends Fragment {
    private View view = null;
    private Context context;
    private BatchAdapter adapter;
    private CustomerDropperModel customerDropperModel;
    private ProdukButchModel produkButc;
    private CtrlDeliveryDropper ctrlDeliveryDropper;
    private ScanBatchListener listener;

    //	public ScanBatchFragment(Context context, CustomerDropperModel customerDropperModel) {
//		this.context = context;
//		this.customerDropperModel = customerDropperModel;
//		adapter = new BatchAdapter(this.context, new ArrayList<GroupProdukModel>(), new HashMap<GroupProdukModel, List<ProdukButchModel>>()) {
//			@Override
//			public void addScanBatch(ProdukButchModel produkButchModel) {
//				produkButc = produkButchModel;
//				addBatch();
//			}
//		};
//		ctrlDeliveryDropper = new CtrlDeliveryDropper(this.context);
//	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        adapter = new BatchAdapter(this.context, new ArrayList<GroupProdukModel>(), new HashMap<GroupProdukModel, List<ProdukButchModel>>()) {
            @Override
            public void addScanBatch(ProdukButchModel produkButchModel) {
                produkButc = produkButchModel;
                if (listener != null) {
                    listener.addBatch();
                }
            }
        };
        ctrlDeliveryDropper = new CtrlDeliveryDropper(this.context);
    }

    public void setListener(ScanBatchListener listener) {
        this.listener = listener;
    }

    public void setCustomerDropperModel(CustomerDropperModel customerDropperModel) {
        this.customerDropperModel = customerDropperModel;
    }

    public BatchAdapter getAdapter() {
        return adapter;
    }

    private void loadProduct() {
        if (adapter.getGroupCount() == 0) {
            List<GroupProdukModel> listGroup = ctrlDeliveryDropper.listGroupProduk(customerDropperModel);
            for (GroupProdukModel groupProdukModel : listGroup) {
                List<ProdukButchModel> listProduct = ctrlDeliveryDropper.listProdukButch(groupProdukModel, customerDropperModel);
                adapter.addHeader(groupProdukModel);
                adapter.addChild(groupProdukModel, listProduct);
            }
        }
        ((ExpandableListView) view.findViewById(R.id.list)).setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_scan_batch, container, false);
        loadProduct();
        return view;
    }

    public void addBatch(String batch) {
        produkButc.list.add(batch);
        adapter.notifyDataSetChanged();
    }

    public interface ScanBatchListener {
        public void addBatch();
    }
}
