package com.bhn.sadix.dropper.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
 
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class DropperSchedulAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<CustomerDropperModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<CustomerDropperModel, List<DropperWHModel>> _listDataChild;
 
    public DropperSchedulAdapter(Context context, List<CustomerDropperModel> listDataHeader,
            HashMap<CustomerDropperModel, List<DropperWHModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(CustomerDropperModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(CustomerDropperModel brandModel, DropperWHModel productModel) {
    	List<DropperWHModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<DropperWHModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(CustomerDropperModel brandModel, List<DropperWHModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public DropperWHModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final DropperWHModel childText = (DropperWHModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_dropper_scheduler, null);
        }
        TextView GudangName = (TextView) convertView.findViewById(R.id.GudangName);
        TextView GudangAddress = (TextView) convertView.findViewById(R.id.GudangAddress);
        GudangName.setText(childText.WHName);
        GudangAddress.setText(childText.WHAddres);
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<DropperWHModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public CustomerDropperModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        CustomerDropperModel headerTitle = (CustomerDropperModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_dropper_scheduler_group, null);
        }
        TextView CustomerName = (TextView) convertView.findViewById(R.id.CustomerName);
        TextView CustomerAddress = (TextView) convertView.findViewById(R.id.CustomerAddress);
        CustomerName.setText(headerTitle.CustName + " - " + headerTitle.CustID);
        CustomerAddress.setText(headerTitle.CustAddres);
        return convertView;
    }

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(CustomerDropperModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
}