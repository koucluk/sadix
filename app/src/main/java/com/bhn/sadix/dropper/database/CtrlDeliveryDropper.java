package com.bhn.sadix.dropper.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DeliveryDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.dropper.model.GroupProdukModel;
import com.bhn.sadix.dropper.model.ProdukButchModel;
import com.bhn.sadix.dropper.model.ProdukModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlDeliveryDropper {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "DeliveryDropper";
	public CtrlDeliveryDropper(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(DeliveryDropperModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DropperID", model.DropperID);
			cv.put("DropperWHID", model.DropperWHID);
			cv.put("DeliveryID", model.DeliveryID);
			cv.put("SKUId", model.SKUId);
			cv.put("GroupName", model.GroupName);
			cv.put("ProductName", model.ProductName);
			cv.put("SBESAR", model.SBESAR);
			cv.put("SKECIL", model.SKECIL);
			cv.put("QBESAR", model.QBESAR);
			cv.put("QKECIL", model.QKECIL);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public DeliveryDropperModel get(String DeliveryID) {
		DeliveryDropperModel modul = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where DeliveryID=?", new String[]{DeliveryID});
		if(cursor.moveToNext()) {
			modul = new DeliveryDropperModel(cursor);
		}
		cursor.close();
		close();
		return modul;
	}
	public List<DeliveryDropperModel> list() {
		List<DeliveryDropperModel> list = new ArrayList<DeliveryDropperModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new DeliveryDropperModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<GroupProdukModel> listGroupProduk(CustomerDropperModel customerDropperModel) {
		List<GroupProdukModel> list = new ArrayList<GroupProdukModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select GroupName from " + table + " where DropperID=? group by GroupName", 
				new String[]{customerDropperModel.DropperID});
		while(cursor.moveToNext()) {
			list.add(new GroupProdukModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<ProdukModel> listProduk(GroupProdukModel groupProdukModel, CustomerDropperModel customerDropperModel, DropperWHModel dropperWHModel) {
		List<ProdukModel> list = new ArrayList<ProdukModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select SKUId,ProductName,SBESAR,SKECIL,QBESAR,QKECIL,DeliveryID from " + table + " where GroupName=? and DropperID=? and DropperWHID=?", 
				new String[]{groupProdukModel.nama, customerDropperModel.DropperID, dropperWHModel.DropperWHID});
		while(cursor.moveToNext()) {
			list.add(new ProdukModel(groupProdukModel, cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<ProdukButchModel> listProdukButch(GroupProdukModel groupProdukModel, CustomerDropperModel customerDropperModel) {
		List<ProdukButchModel> list = new ArrayList<ProdukButchModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select SKUId,ProductName from " + table + " where GroupName=? and DropperID=? group by SKUId,ProductName", 
				new String[]{groupProdukModel.nama, customerDropperModel.DropperID});
		while(cursor.moveToNext()) {
			list.add(new ProdukButchModel(groupProdukModel, cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deletePerDropperID(String DropperID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where DropperID=?", new String[]{DropperID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
