package com.bhn.sadix.dropper.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.dropper.model.CommonDropperModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlCommonDropper {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "CommonDropper";
	public CtrlCommonDropper(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(CommonDropperModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DropperID", model.DropperID);
			cv.put("SalesId", model.SalesId);
			cv.put("WHId", model.WHId);
			cv.put("QRCode", model.QRCode);
			cv.put("QRReasonID", model.QRReasonID);
			cv.put("GeoLat", model.GeoLat);
			cv.put("GeoLong", model.GeoLong);
			cv.put("LogTime", model.LogTime);
			cv.put("BeginDropTime", model.BeginDropTime);
			cv.put("EndDropTime", model.EndDropTime);
			cv.put("Description", model.Description);
			cv.put("Status", model.Status);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(CommonDropperModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("SalesId", model.SalesId);
			cv.put("WHId", model.WHId);
			cv.put("QRCode", model.QRCode);
			cv.put("QRReasonID", model.QRReasonID);
			cv.put("GeoLat", model.GeoLat);
			cv.put("GeoLong", model.GeoLong);
			cv.put("LogTime", model.LogTime);
			cv.put("BeginDropTime", model.BeginDropTime);
			cv.put("EndDropTime", model.EndDropTime);
			cv.put("Description", model.Description);
			cv.put("Status", model.Status);
			db.update(table, cv, "DropperID=?", new String []{model.DropperID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public CommonDropperModel get(String DropperID) {
		CommonDropperModel modul = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where DropperID=?", new String[]{DropperID});
		if(cursor.moveToNext()) {
			modul = new CommonDropperModel(cursor);
		}
		cursor.close();
		close();
		return modul;
	}
	public List<CommonDropperModel> list() {
		List<CommonDropperModel> list = new ArrayList<CommonDropperModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new CommonDropperModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
