package com.bhn.sadix.dropper.model;

import java.io.Serializable;

import android.database.Cursor;

public class GroupProdukModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2721547563779613807L;
	public String nama;
	public GroupProdukModel() {
	}
	public GroupProdukModel(String nama) {
		this.nama = nama;
	}
	public GroupProdukModel(Cursor cursor) {
		nama = cursor.getString(0);
	}

}
