package com.bhn.sadix.dropper.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlCustomerDropper {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "CustomerDropper";
	public CtrlCustomerDropper(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(CustomerDropperModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("DropperID", model.DropperID);
			cv.put("CustID", model.CustID);
			cv.put("CustName", model.CustName);
			cv.put("CustAddres", model.CustAddres);
			cv.put("CustLong", model.CustLong);
			cv.put("CustLat", model.CustLat);
			cv.put("DateServer", model.DateServer);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public CustomerDropperModel get(String DropperID) {
		CustomerDropperModel modul = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where DropperID=?", new String[]{DropperID});
		if(cursor.moveToNext()) {
			modul = new CustomerDropperModel(cursor);
		}
		cursor.close();
		close();
		return modul;
	}
	public List<CustomerDropperModel> list() {
		List<CustomerDropperModel> list = new ArrayList<CustomerDropperModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new CustomerDropperModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
