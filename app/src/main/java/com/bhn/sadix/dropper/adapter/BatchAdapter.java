package com.bhn.sadix.dropper.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.dropper.model.GroupProdukModel;
import com.bhn.sadix.dropper.model.ProdukButchModel;
import com.bhn.sadix.util.Util;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
 
public abstract class BatchAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupProdukModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupProdukModel, List<ProdukButchModel>> _listDataChild;
 
    public BatchAdapter(Context context, List<GroupProdukModel> listDataHeader,
            HashMap<GroupProdukModel, List<ProdukButchModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(GroupProdukModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupProdukModel brandModel, ProdukButchModel productModel) {
    	List<ProdukButchModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<ProdukButchModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupProdukModel brandModel, List<ProdukButchModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public ProdukButchModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ProdukButchModel childText = (ProdukButchModel) getChild(groupPosition, childPosition);
        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = infalInflater.inflate(R.layout.list_batch_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        ImageButton btn_add = (ImageButton) convertView.findViewById(R.id.btn_add);
        final LinearLayout panel_batch = (LinearLayout) convertView.findViewById(R.id.panel_batch);
        txtListChild.setText(childText.ProductName);
        btn_add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addScanBatch(childText);
			}
		});
        panel_batch.removeAllViews();
        for (String sbatch : childText.list) {
        	final LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_list_batch, null);
        	final TextView batch = (TextView) layout.findViewById(R.id.batch);
            final ImageButton btn_delete = (ImageButton) layout.findViewById(R.id.btn_delete);
            batch.setText(sbatch);
            btn_delete.setOnClickListener(new View.OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				panel_batch.removeView(layout);
    				childText.list.remove(batch.getText().toString());
    				notifyDataSetChanged();
    			}
    		});
            panel_batch.addView(layout);
		}
        if(childText.list.size() > 0) {
        	panel_batch.setVisibility(View.VISIBLE);
        } else {
        	panel_batch.setVisibility(View.GONE);
        }
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<ProdukButchModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupProdukModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupProdukModel headerTitle = (GroupProdukModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_batch_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.nama);
        return convertView;
    }
	@Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
	public void removeHeader(GroupProdukModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
	public abstract void addScanBatch(ProdukButchModel produkButchModel);
}