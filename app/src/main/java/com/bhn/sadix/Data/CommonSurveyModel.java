package com.bhn.sadix.Data;

import io.realm.RealmObject;

/**
 * Created by caksono on 06/04/17.
 */

public class CommonSurveyModel extends RealmObject {
    
    public String CommonId;
    public String CustomerId;
    public String SalesId;
    public String GeoLat;
    public String GeoLong;
    public String BeginSurveyTime;
    public String EndSurveyTime;
    public String FormId;
    public String nmCustomer;
    public String nmForm;
    public int done;

    public CommonSurveyModel() {
    }
}
