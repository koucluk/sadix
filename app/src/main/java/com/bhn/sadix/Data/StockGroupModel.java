package com.bhn.sadix.Data;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 12/04/17.
 */

public class StockGroupModel extends RealmObject {

    public String GroupId;
    public String CustomerId;
    public int Quantity;
    public int Quantity2;
    public String CommonID;

    @PrimaryKey
    public String RandomID;

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("GroupId", Integer.parseInt(GroupId));
            json.put("CustomerId", Integer.parseInt(CustomerId));
            json.put("Quantity", Quantity);
            json.put("Quantity2", Quantity2);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";
        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(GroupId);
        buffer.append(SEPARATOR);
        buffer.append(CustomerId);
        buffer.append(SEPARATOR);
        buffer.append(Quantity);
        buffer.append(SEPARATOR);
        buffer.append(Quantity2);

        return buffer.toString();
    }
}
