package com.bhn.sadix.Data;

import org.json.JSONObject;

import java.util.UUID;

import io.realm.RealmObject;

/**
 * Created by caksono on 05/04/17.
 */

public class DiscountPromoModel extends RealmObject {

    public int DiscountPromoID;
    public int CustomerId;
    public String CommonID;
    public String RandomID;

    public DiscountPromoModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public DiscountPromoModel(int discountPromoID, int customerId, String commonID) {
        this();
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("DiscountPromoID", DiscountPromoID);
            json.put("CustomerId", CustomerId);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";
        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(DiscountPromoID);
        buffer.append(SEPARATOR);
        buffer.append(CustomerId);
        return buffer.toString();
    }
}
