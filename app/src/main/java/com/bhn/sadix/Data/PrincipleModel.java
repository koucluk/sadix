package com.bhn.sadix.Data;

import android.database.Cursor;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class PrincipleModel extends RealmObject {
    public String PrinsipleId;
    public String PrinsipleName;

    @PrimaryKey
    public String RandomID;

    public int total_qty_b = 0;
    public int total_qty_k = 0;
    public double price = 0;
    public double discount = 0;
    public double total_price = 0;

    public int PrinsipleType;
    public double CreditLimit;
    public double CreditTOP;

    public String CommonID;

    public RealmList<TakingOrderModel> list;
    public RealmList<DiscountPromoModel> listPromo;
    public RealmList<DiscountResultModel> listPromoResult;

    public PrincipleModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public PrincipleModel(String prinsipleId, String prinsipleName) {
        this();
        this.PrinsipleId = prinsipleId;
        this.PrinsipleName = prinsipleName;
    }

    public PrincipleModel(Cursor c) {
        this();
        PrinsipleId = c.getString(0);
        PrinsipleName = c.getString(1);
    }
}
