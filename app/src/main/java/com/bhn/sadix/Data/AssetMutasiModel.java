package com.bhn.sadix.Data;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 06/04/17.
 */

public class AssetMutasiModel extends RealmObject {

    public String AsetName;
    public String AsetID;
    public int MerkID;
    public int TypeID;
    public String Remark;
    public String CommonID;

    @PrimaryKey
    public String RandomID;

    public String NoSeri;

    public AssetMutasiModel() {
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("AsetName", AsetName);
            json.put("AsetID", AsetID);
            json.put("MerkID", MerkID);
            json.put("TypeID", TypeID);
            json.put("Remark", Remark);
            json.put("NoSeri", NoSeri);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";

        StringBuffer buffer = new StringBuffer();

        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(AsetName);
        buffer.append(SEPARATOR);
        buffer.append(AsetID);
        buffer.append(SEPARATOR);
        buffer.append(MerkID);
        buffer.append(SEPARATOR);
        buffer.append(TypeID);
        buffer.append(SEPARATOR);
        buffer.append(Remark);
        buffer.append(SEPARATOR);
        buffer.append(NoSeri);
        return buffer.toString();
    }
}
