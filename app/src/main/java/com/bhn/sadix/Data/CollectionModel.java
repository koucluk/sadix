package com.bhn.sadix.Data;

import org.json.JSONObject;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class CollectionModel extends RealmObject {
    public String CustId;

    public String status;

    public String data1;

    public String data2;

    public String data3;

    public String data4;

    public String data5;

    public String data6;

    public String tanggal;
    public String CommonID;

    @PrimaryKey
    public String RandomID;

    //kaga ada di db
    public String payMethod;

    public CollectionModel() {
        RandomID = UUID.randomUUID().toString();
        this.CustId = "";
        this.status = "";
        this.data1 = "";
        this.data2 = "";
        this.data3 = "";
        this.CommonID = "";
        this.data4 = "";
        this.data5 = "";
        this.data6 = "";
    }

    public CollectionModel(String CommonId, String status) {
        this();
        CommonID = CommonId;
        this.status = status;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("StatusId", status);
            json.put("Data1", data1);
            json.put("Data2", data2);
            json.put("Data3", data3);
            json.put("Data4", data4);
            json.put("Data5", data5);
            json.put("Data6", data6);
            json.put("TransactionTime", tanggal);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";
        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(status);
        buffer.append(SEPARATOR);
        buffer.append(data1);
        buffer.append(SEPARATOR);
        buffer.append(data2);
        buffer.append(SEPARATOR);
        buffer.append(data3);
        buffer.append(SEPARATOR);
        buffer.append(data4);
        buffer.append(SEPARATOR);
        buffer.append(data5);
        buffer.append(SEPARATOR);
        buffer.append(data6);
        buffer.append(SEPARATOR);
        buffer.append(tanggal);

        return buffer.toString();
    }
}
