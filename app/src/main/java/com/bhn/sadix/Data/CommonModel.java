package com.bhn.sadix.Data;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class CommonModel extends RealmObject {

    public String SC;

    @Index
    public String CustomerId;
    public String BeginVisitTime;
    public String EndVisitTime;
    public String LogTime;
    public String SalesId;

    public double S_AMOUNT;

    public String Description = "";
    public String GeoLat;
    public String GeoLong;

    public int QTY_B;
    public int QTY_K;

    public double PRICE;

    public double DISCOUNT;
    public int Visit;
    public int RVisitID;
    public String InvNO;
    public String Status;

    @PrimaryKey
    public String CommonID;
    
    public String D;
    public String ROrderID;
    public int StatusID;
    public String CustomerRID;
    public int Done;

    public RealmList<TakingOrderModel> takingOrderModel;
    public RealmList<ReturnModel> returnModel;
    public RealmList<PriceMonitoringModel> priceMonitoringModel;
    public RealmList<CollectionModel> collectionModel;
    public RealmList<StockCustomerModel> stockCustomerModel;
    public RealmList<StockCompetitorModel> stockCompetitorModel;
    public RealmList<AssetModel> assetModel;
    public RealmList<AssetMutasiModel> assetMutasiModel;

    public CommonModel() {

    }
}
