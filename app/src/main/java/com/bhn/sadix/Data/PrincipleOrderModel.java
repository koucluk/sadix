package com.bhn.sadix.Data;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 05/04/17.
 */

public class PrincipleOrderModel extends RealmObject {

    @PrimaryKey
    public int PrinsipleId;
    public double G_AMOUNT;
    public double D_VALUE;
    public double S_AMOUNT;
    public int total_k;
    public int total_b;
    public String CommonID;

    public String RandomID;

    public PrincipleOrderModel() {
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("PrinsipleId", PrinsipleId);
            json.put("G_AMOUNT", G_AMOUNT);
            json.put("D_VALUE", D_VALUE);
            json.put("S_AMOUNT", S_AMOUNT);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";
        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(PrinsipleId);
        buffer.append(SEPARATOR);
        buffer.append(G_AMOUNT);
        buffer.append(SEPARATOR);
        buffer.append(D_VALUE);
        buffer.append(SEPARATOR);
        buffer.append(S_AMOUNT);
        
        return buffer.toString();
    }
}
