package com.bhn.sadix.Data;

import org.json.JSONObject;

import io.realm.RealmObject;

/**
 * Created by caksono on 05/04/17.
 */

public class DiscountResultModel extends RealmObject {

    public int CustomerId;
    public int DiscountResultID;
    public int DiscountResultValue;
    public String CommonID;
    public String RandomID;


    public DiscountResultModel() {
    }

    public DiscountResultModel(int customerId, int discountResultID,
                               int discountResultValue, String commonID, String RandomID) {
        CustomerId = customerId;
        DiscountResultID = discountResultID;
        DiscountResultValue = discountResultValue;
        CommonID = commonID;
        this.RandomID = RandomID;
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("CustomerId", CustomerId);
            json.put("DiscountResultID", DiscountResultID);
            json.put("DiscountResultValue", DiscountResultValue);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";

        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(CustomerId);
        buffer.append(SEPARATOR);
        buffer.append(DiscountResultID);
        buffer.append(SEPARATOR);
        buffer.append(DiscountResultValue);

        return buffer.toString();
    }
}
