package com.bhn.sadix.Data;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class ReturnModel extends RealmObject {

    @Index
    public String CustId;

    @Index
    public String SKUId;

    public String tanggal;
    public int QTY_B;
    public int QTY_K;
    public String note;
    public String tipe;

    public String CommonID;

    @PrimaryKey
    public String RandomID;

    public ReturnModel() {
    }
}
