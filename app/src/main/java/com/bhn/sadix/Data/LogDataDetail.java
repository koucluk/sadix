package com.bhn.sadix.Data;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by caksono on 05/04/17.
 */

public class LogDataDetail {

    public List<StockCustomerModel> stockOutletModel;
    public List<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModel;
    public List<com.bhn.sadix.Data.TakingOrderModel> takingOrderModel;
    public List<ReturnModel> returModel;
    public List<CollectionModel> collectorModel;
    //    public RealmList<PriceMonitoringModel> listPriceMonitoring;
    public List<PriceMonitoringModel> listPriceMonitoring;

    Realm realm;

    public LogDataDetail(RealmList<StockCustomerModel> stockOutletModel,
                         RealmList<StockCompetitorModel> stockCompetitorModel,
                         RealmList<TakingOrderModel> takingOrderModel,
                         RealmList<ReturnModel> returModel,
                         RealmList<CollectionModel> collectorModel,
                         RealmList<PriceMonitoringModel> listPriceMonitoring) {

        realm = Realm.getDefaultInstance();

        this.stockOutletModel = realm.copyFromRealm(stockOutletModel);
        this.stockCompetitorModel = realm.copyFromRealm(stockCompetitorModel);
        this.takingOrderModel = realm.copyFromRealm(takingOrderModel);
        this.returModel = realm.copyFromRealm(returModel);
        this.collectorModel = realm.copyFromRealm(collectorModel);
        this.listPriceMonitoring = realm.copyFromRealm(listPriceMonitoring);
    }

    public LogDataDetail(List<StockCustomerModel> stockOutletModel,
                         List<StockCompetitorModel> stockCompetitorModel,
                         List<TakingOrderModel> takingOrderModel,
                         List<ReturnModel> returModel,
                         List<CollectionModel> collectorModel,
                         List<PriceMonitoringModel> listPriceMonitoring) {

        realm = Realm.getDefaultInstance();

        this.stockOutletModel = stockOutletModel;
        this.stockCompetitorModel = stockCompetitorModel;
        this.takingOrderModel = takingOrderModel;
        this.returModel = returModel;
        this.collectorModel = collectorModel;
        this.listPriceMonitoring = listPriceMonitoring;
    }
}
