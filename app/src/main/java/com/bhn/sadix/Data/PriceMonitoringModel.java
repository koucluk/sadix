package com.bhn.sadix.Data;

import org.json.JSONObject;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class PriceMonitoringModel extends RealmObject {

    public String SKUId;
    public int ProductID;

    /*public SKUModel skuModel;
    public SKUComModel skuComModel;*/

    public String ProductName;
    public long Qty;
    public long BuyPrice;
    public long SellPrice;
    public int LevelID;
    public String Note;
    public String CommonID;

    @PrimaryKey
    private String RandomID;

    public PriceMonitoringModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public PriceMonitoringModel(int ProductId, String ProductName) {
        this();
        ProductID = ProductId;
        this.ProductName = ProductName;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("SKUId", SKUId);
            json.put("ProductID", ProductID);
            json.put("ProductName", ProductName);
            json.put("Qty", Qty);
            json.put("BuyPrice", BuyPrice);
            json.put("SellPrice", SellPrice);
            json.put("LevelID", LevelID);
            json.put("Note", Note);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public String toCSV() {
        String SEPARATOR = ";";
        StringBuffer buffer = new StringBuffer();
        buffer.append(RandomID);
        buffer.append(SEPARATOR);
        buffer.append(CommonID);
        buffer.append(SEPARATOR);
        buffer.append(SKUId);
        buffer.append(SEPARATOR);
        buffer.append(ProductID);
        buffer.append(SEPARATOR);
        buffer.append(ProductName);
        buffer.append(SEPARATOR);
        buffer.append(Qty);
        buffer.append(SEPARATOR);
        buffer.append(BuyPrice);
        buffer.append(SEPARATOR);
        buffer.append(SellPrice);
        buffer.append(SEPARATOR);
        buffer.append(LevelID);
        buffer.append(SEPARATOR);
        buffer.append(Note);

        return buffer.toString();
    }
}
