package com.bhn.sadix.Data;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class CustomerBrandingModel extends RealmObject {

    @PrimaryKey
    public String RandomID;

    public String SalesID;
    public String CustomerID;
    public String BrandingID;
    public String PhotoPic;
    public String GeoLong;
    public String GeoLat;
    public String Description;
    public String NmCust;
    public String NmBranding;
    public String CommonID;
    public int Done;

    public CustomerBrandingModel() {
        RandomID = UUID.randomUUID().toString();
    }
}
