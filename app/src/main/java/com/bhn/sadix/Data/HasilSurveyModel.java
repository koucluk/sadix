package com.bhn.sadix.Data;

import io.realm.RealmObject;

/**
 * Created by caksono on 06/04/17.
 */

public class HasilSurveyModel extends RealmObject {
    public String FormId;
    public String CommonId;
    public String ListID;
    public String ListValue;
    public String SurveyId;

    public HasilSurveyModel() {
    }
}
