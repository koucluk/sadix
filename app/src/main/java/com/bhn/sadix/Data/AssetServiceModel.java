package com.bhn.sadix.Data;

import io.realm.RealmObject;

/**
 * Created by caksono on 04/04/17.
 */

public class AssetServiceModel extends RealmObject {

    public String ASETNAME;
    public String ASETTYPE;
    public String ASETNO;
    public String ASETID;
    private String CommonID;

    public AssetServiceModel() {
    }
}
