package com.bhn.sadix.Data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 05/04/17.
 */

public class StockCompetitorModel extends RealmObject {

    public String CustId;
    public String tanggal;
    public String SKUId;
    public int QTY_B;
    public int QTY_K;
    public String CommonID;
    public double PRICE;
    public double PRICE2;
    public String Note;
    
    @PrimaryKey
    public String RandomID;

    public StockCompetitorModel() {
    }
}
