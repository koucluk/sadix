package com.bhn.sadix.Data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class AssetModel extends RealmObject {

    public String AsetID; /*",ASETNO);*/
    public int Idx;
    public String Name;
    public String GroupAset;
    public int TypeMnt;
    public int Posisi;
    public String Value;
    public String CommonID;
    public String NameFile;
    public int tipe;
    public int JenisWidget;

    @PrimaryKey
    public String RandomID;

    public AssetModel() {
    }

    public AssetModel(String RandomId) {
        RandomID = RandomId;
    }
}
