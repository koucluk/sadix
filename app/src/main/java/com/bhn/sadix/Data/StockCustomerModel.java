package com.bhn.sadix.Data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 05/04/17.
 */

public class StockCustomerModel extends RealmObject {

    @SerializedName("CustomerId")
    public String CustId;
    public String tanggal;

    public String SKUId;
    
    public String SKUName;

    @SerializedName("Quantity2")
    public int QTY_B;

    @SerializedName("Quantity")
    public int QTY_K;

    public String CommonID;

    @PrimaryKey
    public String RandomID;

    public StockCustomerModel() {
    }
}
