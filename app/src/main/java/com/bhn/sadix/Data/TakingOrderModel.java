package com.bhn.sadix.Data;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by caksono on 04/04/17.
 */

public class TakingOrderModel extends RealmObject {

    private static final long serialVersionUID = -5325268679869994823L;
    @Index
    public String CustId;

    @Index
    public String SKUId;
    public String tanggal;
    public String SalesProgramId;
    public int QTY_B;
    public int QTY_K;
    public double DISCOUNT1;
    public double DISCOUNT2;
    public double DISCOUNT3;
    public double TOTAL_PRICE;
    public String PAYMENT_TERM;
    public String note;

    public double PRICE_B;
    public double PRICE_K;
    public int LAMA_KREDIT;
    public String DiscountType;
    public String CommonID;

    @PrimaryKey
    public String RandomID;

    public String PrinsipleID = "";

    public TakingOrderModel() {
    }

    public TakingOrderModel(String sKUId) {
        SKUId = sKUId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof com.bhn.sadix.model.TakingOrderModel) {
            com.bhn.sadix.model.TakingOrderModel model = (com.bhn.sadix.model.TakingOrderModel) o;
            return (model.SKUId.equals(SKUId));
        }
        return false;
    }
}
