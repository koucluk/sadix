package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.LogGpsModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlLogGps {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "log_gps";
	public CtrlLogGps(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(LogGpsModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("GeoLat", model.GeoLat);
			cv.put("GeoLong", model.GeoLong);
			cv.put("IMEI", model.IMEI);
			cv.put("LogTime", model.LogTime);
			cv.put("SalesID", model.SalesID);
			cv.put("SC", model.SC);
			cv.put("statusLog", model.statusLog);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<LogGpsModel> list() {
		List<LogGpsModel> list = new ArrayList<LogGpsModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new LogGpsModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void delete(String LogTime) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where LogTime=?", new String[]{LogTime});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
	public LogGpsModel get() {
		LogGpsModel data = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		if(cursor.moveToNext()) {
			data = new LogGpsModel(cursor);
		}
		cursor.close();
		close();
		return data;
	}

}
