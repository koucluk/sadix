package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String dbName = "SadixDB.db";

    public static final String Prinsiple = "CREATE TABLE Prinsiple (PrinsipleId integer,G_AMOUNT float,D_VALUE float,S_AMOUNT float,CommonID varchar(50),RandomID varchar(50))"; /*,K_TOTAL integer,B_TOTAL integer*/
    public static final String Salestarget = "CREATE TABLE Salestarget (LogTime varchar(50),SKUId integer,Bln integer,Thn integer,Qty integer,Price float,CustID integer,SalesID integer,StTarget integer)";
    public static final String SalesNote = "CREATE TABLE SalesNote (ID varchar(50), SalesID integer,NoteID integer,Description varchar(250),LogDate varchar(50))";
    public static final String VisitNote = "CREATE TABLE VisitNote (VisitId integer,ItemId integer,ItemValue text,CommonID varchar(50),RandomID varchar(50))";

    public DatabaseHelper(Context context) {
        super(context, dbName, null, 20);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//		db.execSQL("CREATE TABLE salesinformation (Header varchar(100), Content varchar(200), DateValidity varchar(50))");
//		db.execSQL("CREATE TABLE customertype (CustomerTypeId varchar(50), Description varchar(200), SC varchar(50), DiskonType varchar(10), DiskonMax float)");  
//		db.execSQL("CREATE TABLE schedulecustomer (CustId varchar(50),CustomerName varchar(100)," +
//				"CustomerAddress varchar(200),CustomerEmail varchar(100),CustomerPhone varchar(20),CustomerFax varchar(20)," +
//				"CustomerNPWP varchar(100),OwnerName varchar(100),OwnerAddress varchar(100),OwnerEmail varchar(100)," +
//				"OwnerPhone varchar(20),CustomerTypeId varchar(50),GeoLong varchar(50),GeoLat varchar(50)," +
//				"D1 varchar(10),D2 varchar(10),D3 varchar(10),D4 varchar(10),D5 varchar(10),D6 varchar(10),D7 varchar(10)," +
//				"CustomerCrLimit float,CustomerCrBalance float,isNew varchar(2),isEdit varchar(2), isNOO varchar(2)," +
//				"OwnerBirthPlace varchar(100),OwnerBirthDate varchar(50),OwnerIdNo varchar(50),CustomerID varchar(50))");   
//		db.execSQL("CREATE TABLE group_sku (GroupId varchar(50), GroupName varchar(200), PromoId varchar(2), FokusId varchar(2))"); 
//		db.execSQL("CREATE TABLE sales_program (SalesProgramId varchar(50), SalesProgramDescription varchar(200), GroupId varchar(50))"); 
//		db.execSQL("CREATE TABLE sku (SKUId varchar(50),GroupId varchar(50)," +
//				"ProductName varchar(150),HET_PRICE float,SBESAR varchar(50),SKECIL varchar(50)," +
//				"CONVER float,HET_PRICE_L float,DISCOUNT_1_TYPE varchar(50),DISCOUNT_1_MAX float," +
//				"DISCOUNT_2_TYPE varchar(50),DISCOUNT_2_MAX float,DISCOUNT_3_TYPE varchar(50),DISCOUNT_3_MAX float, DISCOUNT_TYPE varchar(2)," +
//				"QBESAR float,QKECIL float, QOBESAR float,QOKECIL float)"); 
//		db.execSQL("CREATE TABLE config (ConfigId varchar(50), ConfigName varchar(100), ConfigClass varchar(50))");
//		db.execSQL("CREATE TABLE form_survey (FormId varchar(50),FormName varchar(200),FormRemark varchar(200))");
//		db.execSQL("CREATE TABLE form_cust_type (FormId varchar(50),Id varchar(50))");  
//		db.execSQL("CREATE TABLE survey (FormId varchar(50),SurveyId varchar(50),SurveyName text,SurveyType varchar(50))");  
//		db.execSQL("CREATE TABLE survey_opt (SurveyId varchar(50),ListID varchar(50),ListValue varchar(200))");  

        db.execSQL("CREATE TABLE AppModul (AppModulId varchar(50), AppModulValue varchar(50))");
        db.execSQL("CREATE TABLE group_sku_com (GroupId varchar(50), GroupName varchar(200))");
        db.execSQL("CREATE TABLE sku_com (SKUId varchar(50), ProductName varchar(200), GroupId varchar(50),SBESAR varchar(50),SKECIL varchar(50))");

        //tabel transaksi
        db.execSQL("CREATE TABLE taking_order (CustId varchar(50), SKUId varchar(50), tanggal varchar(50), SalesProgramId varchar(50), " +
                "QTY_B integer, QTY_K integer, DISCOUNT1 float, DISCOUNT2 float, DISCOUNT3 float, TOTAL_PRICE float, PAYMENT_TERM varchar(2)," +
                "note varchar(200), PRICE_B float, PRICE_K float, LAMA_KREDIT integer, DiscountType varchar(5), CommonID varchar(50),RandomID varchar(50),PrinsipleID varchar(50))");
        db.execSQL("CREATE TABLE retur (CustId varchar(50), SKUId varchar(50), tanggal varchar(50), QTY_B integer, QTY_K integer, note varchar(200), tipe varchar(50), CommonID varchar(50),RandomID varchar(50))");
        db.execSQL("CREATE TABLE common (SC varchar(10), CustomerId varchar(50), BeginVisitTime varchar(50), EndVisitTime varchar(50), " +
                "LogTime varchar(50),SalesId varchar(50),S_AMOUNT float,Description varchar(200)," +
                "GeoLat varchar(50),GeoLong varchar(50), QTY_B integer, QTY_K integer, PRICE float, DISCOUNT float," +
                "Visit varchar(50), RVisitID varchar(50), InvNO varchar(50), Status varchar(5), CommonID varchar(50),D varchar(50), " +
                "ROrderID varchar(10),StatusID integer, CustomerRID varchar(50))"); /*, Done integer*/
        db.execSQL("CREATE TABLE collector (CustId varchar(50), status varchar(5), data1 varchar(50), data2 varchar(50), data3 varchar(200), tanggal varchar(50), CommonID varchar(50), data4 varchar(50), data5 varchar(50), data6 varchar(50),RandomID varchar(50))");
        db.execSQL("CREATE TABLE image_customer (CustId varchar(50), type varchar(50), namaFile varchar(100))");
        db.execSQL("CREATE TABLE stock_outlet (CustId varchar(50), tanggal varchar(50), SKUId varchar(50), QTY_B integer, QTY_K integer, CommonID varchar(50),RandomID varchar(50))");
        db.execSQL("CREATE TABLE stock_competitor (CustId varchar(50), tanggal varchar(50), SKUId varchar(50), QTY_B integer, QTY_K integer, CommonID varchar(50), PRICE float, PRICE2 float, Note varchar(200),RandomID varchar(50))");

        db.execSQL("CREATE TABLE log_gps (SalesID varchar(50),IMEI varchar(50),LogTime varchar(50)," +
                "SC varchar(50),GeoLong float,GeoLat float,statusLog integer)");
        db.execSQL("CREATE TABLE cust_photo (SalesID varchar(50),CustomerID varchar(50),PhotoTypeID varchar(50),PhotoPic varchar(50),PhotoCat varchar(10),RandomID varchar(50))");
        db.execSQL("CREATE TABLE branding_photo (SalesID varchar(50),CustomerID varchar(50),BrandingID varchar(50)," +
                "PhotoPic varchar(50),GeoLong varchar(50),GeoLat varchar(50),Description varchar(200))"); /*, Done integer, CommonId varchar(200)*/

        db.execSQL("CREATE TABLE CommonSurvey (CommonId varchar(50),CustomerId varchar(50),SalesId varchar(50),GeoLat varchar(50)," +
                "GeoLong varchar(50),BeginSurveyTime varchar(50),EndSurveyTime varchar(50),FormId varchar(50), Done integer)"); /*, Done integer*/
        db.execSQL("CREATE TABLE HasilSurvey (CommonId varchar(50),FormId varchar(50),SurveyId varchar(50),ListID varchar(50),ListValue varchar(200))");

        //db modul asset
        db.execSQL("CREATE TABLE GroupStock (GroupId varchar(50),CustomerId varchar(50),Quantity integer,Quantity2 integer, CommonID varchar(50),RandomID varchar(50))");
//		db.execSQL("CREATE TABLE CommonAsset (GeoLat float,BeginAsetTime varchar(50),CustomerId integer,EndAsetTime varchar(50),GeoLong float,SalesId integer, CommonID varchar(50))");
        db.execSQL("CREATE TABLE Aset (Name varchar(50),AsetID varchar(50),GroupAset varchar(50),Idx integer,TypeMnt integer,Posisi integer,Value varchar(200), CommonID varchar(50), jenisWidget varchar(10), tipe varchar(10), namaFile varchar(50),RandomID varchar(50))");
        db.execSQL("CREATE TABLE Newasset (AsetName varchar(50),AsetID varchar(50),MerkID integer,TypeID integer,Remark varchar(200), CommonID varchar(50),RandomID varchar(50),NoSeri varchar(50))");

        //db modul diskon
        db.execSQL("CREATE TABLE DiscountPromo (DiscountPromoID integer,CustomerId integer, CommonID varchar(50),RandomID varchar(50))");
        db.execSQL("CREATE TABLE DiscountResult (CustomerId integer,DiscountResultID integer,DiscountResultValue integer, CommonID varchar(50),RandomID varchar(50))");

        //db modul target
        db.execSQL("CREATE TABLE TargetProduk (SKUId varchar(50),Bln integer,Thn integer,Qty integer,Price integer, CommonID varchar(50),RandomID varchar(50),StTarget integer)");
        //db modul price monitoring
        db.execSQL("CREATE TABLE PriceMonitoring (SKUId varchar(50),ProductID integer,BuyPrice integer,SellPrice integer,Qty integer,LevelID integer,Note varchar(200), CommonID varchar(50),RandomID varchar(50))");

//        db.execSQL("CREATE TABLE DataState (CommonID varchar(50), Modul varchar(50))");

        db.execSQL(Salestarget);
        db.execSQL(SalesNote);
        db.execSQL(Prinsiple);
        db.execSQL(VisitNote);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE IF EXISTS salesinformation");
//        db.execSQL("DROP TABLE IF EXISTS customertype");
//        db.execSQL("DROP TABLE IF EXISTS schedulecustomer");
//        db.execSQL("DROP TABLE IF EXISTS group_sku");
//        db.execSQL("DROP TABLE IF EXISTS sales_program");
//        db.execSQL("DROP TABLE IF EXISTS sku");
//        db.execSQL("DROP TABLE IF EXISTS stock_outlet");
//        db.execSQL("DROP TABLE IF EXISTS stock_competitor");
//        db.execSQL("DROP TABLE IF EXISTS taking_order");
//        db.execSQL("DROP TABLE IF EXISTS retur");
//        db.execSQL("DROP TABLE IF EXISTS config");
//        db.execSQL("DROP TABLE IF EXISTS common");
//        db.execSQL("DROP TABLE IF EXISTS collector");
//        db.execSQL("DROP TABLE IF EXISTS image_customer");
//        db.execSQL("DROP TABLE IF EXISTS group_sku_com");
//        db.execSQL("DROP TABLE IF EXISTS sku_com");
//        db.execSQL("DROP TABLE IF EXISTS AppModul");
//        db.execSQL("DROP TABLE IF EXISTS form_survey");
//        db.execSQL("DROP TABLE IF EXISTS form_cust_type");
//        db.execSQL("DROP TABLE IF EXISTS survey");
//        db.execSQL("DROP TABLE IF EXISTS survey_opt");
//
//        db.execSQL("DROP TABLE IF EXISTS log_gps");
//        db.execSQL("DROP TABLE IF EXISTS cust_photo");
//        db.execSQL("DROP TABLE IF EXISTS branding_photo");
//        onCreate(db);

        //db version 2
//		db.execSQL("ALTER TABLE schedulecustomer ADD isNOO varchar(2)");

        //db version 3
//		db.execSQL("ALTER TABLE common ADD ROrderID varchar(10)");
//		db.execSQL("ALTER TABLE schedulecustomer ADD OwnerBirthPlace varchar(100)");
//		db.execSQL("ALTER TABLE schedulecustomer ADD OwnerBirthDate varchar(50)");
//		db.execSQL("ALTER TABLE schedulecustomer ADD OwnerIdNo varchar(50)");


        //db version 4
//		db.execSQL("ALTER TABLE schedulecustomer ADD CustomerID varchar(50)");

        //db version 5
        //db modul asset
//		db.execSQL("CREATE TABLE GroupStock (GroupId varchar(50),CustomerId varchar(50),Quantity integer,Quantity2 integer, CommonID varchar(50))");
//		db.execSQL("CREATE TABLE CommonAsset (GeoLat float,BeginAsetTime varchar(50),CustomerId integer,EndAsetTime varchar(50),GeoLong float,SalesId integer, CommonID varchar(50))");
//		db.execSQL("CREATE TABLE Aset (Name varchar(50),AsetID integer,GroupAset varchar(50),Idx integer,TypeMnt integer,Value varchar(200), CommonID varchar(50))");

        //db modul diskon
//		db.execSQL("CREATE TABLE DiscountPromo (DiscountPromoID integer,CustomerId integer, CommonID varchar(50))");
//		db.execSQL("CREATE TABLE DiscountResult (CustomerId integer,DiscountResultID integer,DiscountResultValue integer, CommonID varchar(50))");

        //db versi 6
//		db.execSQL("CREATE TABLE Newasset (AsetName varchar(50),AsetID varchar(50),MerkID integer,TypeID integer,Remark varchar(200), CommonID varchar(50))");
//		db.execSQL("drop table CommonAsset");

        //db versi 6 asset
//		db.execSQL("ALTER TABLE Aset ADD Posisi integer");

        //db versi 7 penambahan di stock_competitor
//		db.execSQL("ALTER TABLE stock_competitor ADD PRICE float");
//		db.execSQL("ALTER TABLE stock_competitor ADD PRICE2 float");
//		db.execSQL("ALTER TABLE stock_competitor ADD Note varchar(200)");

        //db versi 8 penambahan di stock_competitor
//		db.execSQL("ALTER TABLE common ADD StatusID integer");
        //db versi 9 penambahan di stock_competitor
        /*db.execSQL("ALTER TABLE cust_photo ADD PhotoCat varchar(10)");
        db.execSQL("ALTER TABLE cust_photo ADD RandomID varchar(50)");*/

        //db versi 11
///*//
//        db.execSQL("ALTER TABLE common ADD Done integer");
//        db.execSQL("CREATE TABLE DataState (CommonID varchar(50), Modul varchar(50), State integer)");
        /*db.execSQL("ALTER TABLE cust_photo ADD PhotoCat varchar(10)");
        db.execSQL("ALTER TABLE cust_photo ADD RandomID varchar(50)");*/
//        db.execSQL("ALTER TABLE branding_photo ADD Done integer");

        //db versi 16
        db.execSQL("ALTER TABLE Prinsiple ADD K_TOTAL integer");
        db.execSQL("ALTER TABLE Prinsiple ADD B_TOTAL integer");
        db.execSQL("ALTER TABLE CommonSurvey ADD Done integer");
        db.execSQL("ALTER TABLE common ADD Done integer");
        db.execSQL("ALTER TABLE branding_photo ADD Done integer");
        db.execSQL("ALTER TABLE branding_photo ADD CommonId varchar(200)");
    }

    public static void clearData(Context context) {
        DatabaseHelper ctrlDb = new DatabaseHelper(context);
        SQLiteDatabase db = null;
        try {
            db = ctrlDb.getWritableDatabase();
//	        db.execSQL("delete from salesinformation");
//	        db.execSQL("delete from customertype");
//	        db.execSQL("delete from schedulecustomer");
//	        db.execSQL("delete from group_sku");
//	        db.execSQL("delete from sales_program");
//	        db.execSQL("delete from sku");
//	        db.execSQL("delete from config");
//	        db.execSQL("delete from form_survey");
//	        db.execSQL("delete from form_cust_type");
//	        db.execSQL("delete from survey");
//	        db.execSQL("delete from survey_opt");

            db.execSQL("delete from stock_outlet");
            db.execSQL("delete from stock_competitor");
            db.execSQL("delete from AppModul");

            //tabel transaksi
            db.execSQL("delete from taking_order");
            db.execSQL("delete from retur");
            db.execSQL("delete from common");
            db.execSQL("delete from collector");
            db.execSQL("delete from image_customer");
            db.execSQL("delete from group_sku_com");
            db.execSQL("delete from sku_com");
            db.execSQL("delete from log_gps");
            db.execSQL("delete from cust_photo");
            db.execSQL("delete from branding_photo");
            db.execSQL("delete from CommonSurvey");
            db.execSQL("delete from HasilSurvey");

            db.execSQL("delete from GroupStock");
            db.execSQL("delete from Aset");
            db.execSQL("delete from Newasset");
            db.execSQL("delete from DiscountPromo");
            db.execSQL("delete from DiscountResult");
            db.execSQL("delete from TargetProduk");
            db.execSQL("delete from PriceMonitoring");
            db.execSQL("delete from Salestarget");
            db.execSQL("delete from SalesNote");
            db.execSQL("delete from Prinsiple");
            db.execSQL("delete from VisitNote");
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean save(JSONObject json, String table) {
        boolean flag = false;
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            Iterator<String> it = json.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                cv.put(key, json.getString(key));
            }
            db.insert(table, null, cv);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public List<JSONObject> list(String table, String CommonID) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        SQLiteDatabase db = getWritableDatabase();
        try {
            String sql = "select * from " + table + " where CommonID=?";
            Cursor cursor = db.rawQuery(sql, new String[]{CommonID});
            while (cursor.moveToNext()) {
                JSONObject json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
                list.add(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<JSONObject> list(String sql) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                JSONObject json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
                list.add(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public JSONArray getJSONArray(String sql) {
        JSONArray list = new JSONArray();
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                JSONObject json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
                list.put(json);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
        return list;
    }

    public JSONObject getAwalData(String tabel) {
        JSONObject json = null;
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("select * from " + tabel, null);
            if (cursor.moveToNext()) {
                json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
        return json;
    }

    public JSONObject getMerchandiser(String table, String CommonID, String Idx) {
        JSONObject json = null;
        SQLiteDatabase db = getWritableDatabase();
        try {
            String sql = "select * from " + table + " where CommonID=? and Idx=?";
            Cursor cursor = db.rawQuery(sql, new String[]{CommonID, Idx});
            if (cursor.moveToNext()) {
                json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    json.put(cursor.getColumnName(i), cursor.getString(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
        return json;
    }

    private SQLiteDatabase db;

    public Cursor Query(String sql) {
        db = getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }

    public void close() {
        if (db != null) {
            db.close();
        }
    }

    public void deleteByID(String table, String kolomid, String valueId) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL("delete from " + table + " where " + kolomid + "='" + valueId + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
    }
}
