package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlGroupSKU {
    private DbSkuHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "group_sku";

    public CtrlGroupSKU(Context context) {
        this.context = context;
        ctrlDb = new DbSkuHelper(context);
    }

    public void save(GroupSKUModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("GroupId", model.GroupId);
            cv.put("GroupName", model.GroupName);
            cv.put("PromoId", model.PromoId);
            cv.put("FokusId", model.FokusId);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<GroupSKUModel> list() {
        List<GroupSKUModel> list = new ArrayList<GroupSKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.*,count(*) jumlah from " + table + " " +
                "a left join sku b on a.GroupId=b.GroupId " +
                "group by a.GroupId,a.GroupName,a.PromoId,a.FokusId", null);
        while (cursor.moveToNext()) {
            list.add(new GroupSKUModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<GroupSKUModel> listByPrinsiple(/*PrincipleModel prinsipleModel*/ String pID) {
        List<GroupSKUModel> list = new ArrayList<GroupSKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.*,count(*) jumlah from " + table + " a " +
                " left join sku b on a.GroupId=b.GroupId " +
                " where a.PrinsipleId='" + /*prinsipleModel.PrinsipleId*/ pID + "' " +
                "group by a.GroupId,a.GroupName,a.PromoId,a.FokusId", null);
        while (cursor.moveToNext()) {
            list.add(new GroupSKUModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<GroupSKUModel> listStokOnCar() {
        List<GroupSKUModel> list = new ArrayList<GroupSKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.GroupId,a.GroupName,a.PromoId,a.FokusId, count(*) jumlah from " + table + " a " +
                "inner join sku b on a.GroupId=b.GroupId " +
                "where b.QOBESAR > 0 or b.QOKECIL > 0 " +
                "group by a.GroupId,a.GroupName,a.PromoId,a.FokusId", null);
        while (cursor.moveToNext()) {
            list.add(new GroupSKUModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public boolean isOrderGroup(String GroupId, String SKUIdInOrder) {
        boolean flag = false;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.GroupId from " + table + " a " +
                "inner join sku b on a.GroupId=b.GroupId " +
                "where b.SKUId in (?) and a.GroupId=? " +
                "group by a.GroupId", new String[]{SKUIdInOrder, GroupId});
        flag = cursor.moveToNext();
        cursor.close();
        close();
        return flag;
    }

    public GroupSKUModel getBySKUId(String SKUId) {
        GroupSKUModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.*,0 from " + table + " a " +
                "inner join sku b on a.GroupId=b.GroupId " +
                "where b.SKUId=? ", new String[]{SKUId});
        if (cursor.moveToNext()) {
            model = new GroupSKUModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public boolean isLoadGroup(String groupId, String skuid) {
        boolean isLoad = false;
        db = ctrlDb.getWritableDatabase();
        android.util.Log.e("sql", "select count(*) jml from " + table + " a " +
                "inner join sku b on a.GroupId=b.GroupId " +
                "where a.GroupId='" + groupId + "' and b.SKUId in " + skuid);
        Cursor cursor = db.rawQuery("select count(*) jml from " + table + " a " +
                "inner join sku b on a.GroupId=b.GroupId " +
                "where a.GroupId='" + groupId + "' and b.SKUId in " + skuid, null);
        if (cursor.moveToNext()) {
            if (cursor.getInt(0) > 0) {
                isLoad = true;
            }
        }
        cursor.close();
        close();
        return isLoad;
    }

}
