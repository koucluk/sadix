package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class CtrlCommon {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "common";

    public CtrlCommon(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(CommonModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerId", model.CustomerId);
            cv.put("BeginVisitTime", model.BeginVisitTime);
            cv.put("Description", model.Description);
            cv.put("EndVisitTime", model.EndVisitTime);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("LogTime", model.LogTime);
            cv.put("SalesId", model.SalesId);
            cv.put("SC", model.SC);
            cv.put("S_AMOUNT", model.S_AMOUNT);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("PRICE", model.PRICE);
            cv.put("DISCOUNT", model.DISCOUNT);
            cv.put("Visit", model.Visit);
            cv.put("RVisitID", model.RVisitID);
            cv.put("InvNO", model.InvNO);
            cv.put("Status", model.Status);
            cv.put("CommonID", model.CommonID);
            cv.put("D", model.D);
            cv.put("ROrderID", model.ROrderID);
            cv.put("StatusID", model.StatusID);
            cv.put("CustomerRID", model.CustomerRID);
            cv.put("Done", 1);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void save(CommonModel model, int done) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerId", model.CustomerId);
            cv.put("BeginVisitTime", model.BeginVisitTime);
            cv.put("Description", model.Description);
            cv.put("EndVisitTime", model.EndVisitTime);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("LogTime", model.LogTime);
            cv.put("SalesId", model.SalesId);
            cv.put("SC", model.SC);
            cv.put("S_AMOUNT", model.S_AMOUNT);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("PRICE", model.PRICE);
            cv.put("DISCOUNT", model.DISCOUNT);
            cv.put("Visit", model.Visit);
            cv.put("RVisitID", model.RVisitID);
            cv.put("InvNO", model.InvNO);
            cv.put("Status", model.Status);
            cv.put("CommonID", model.CommonID);
            cv.put("D", model.D);
            cv.put("ROrderID", model.ROrderID);
            cv.put("StatusID", model.StatusID);
            cv.put("CustomerRID", model.CustomerRID);
            cv.put("Done", done);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(CommonModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("BeginVisitTime", model.BeginVisitTime);
            cv.put("Description", model.Description);
            cv.put("EndVisitTime", model.EndVisitTime);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("LogTime", model.LogTime);
            cv.put("SalesId", model.SalesId);
            cv.put("SC", model.SC);
            cv.put("S_AMOUNT", model.S_AMOUNT);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("PRICE", model.PRICE);
            cv.put("DISCOUNT", model.DISCOUNT);
            cv.put("Visit", model.Visit);
            cv.put("RVisitID", model.RVisitID);
            cv.put("InvNO", model.InvNO);
            cv.put("Status", model.Status);
            cv.put("CustomerId", model.CustomerId);
            cv.put("D", model.D);
            cv.put("ROrderID", model.ROrderID);
            cv.put("StatusID", model.StatusID);
            cv.put("CustomerRID", model.CustomerRID);
            cv.put("Done", 1);
            db.update(table, cv, "CommonID=?", new String[]{model.CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(CommonModel model, int done) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("BeginVisitTime", model.BeginVisitTime);
            cv.put("Description", model.Description);
            cv.put("EndVisitTime", model.EndVisitTime);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("LogTime", model.LogTime);
            cv.put("SalesId", model.SalesId);
            cv.put("SC", model.SC);
            cv.put("S_AMOUNT", model.S_AMOUNT);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("PRICE", model.PRICE);
            cv.put("DISCOUNT", model.DISCOUNT);
            cv.put("Visit", model.Visit);
            cv.put("RVisitID", model.RVisitID);
            cv.put("InvNO", model.InvNO);
            cv.put("Status", model.Status);
            cv.put("CustomerId", model.CustomerId);
            cv.put("D", model.D);
            cv.put("ROrderID", model.ROrderID);
            cv.put("StatusID", model.StatusID);
            cv.put("CustomerRID", model.CustomerRID);
            cv.put("Done", done);
            db.update(table, cv, "CommonID=?", new String[]{model.CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public CommonModel getCommonModel(String CommonID) {
        CommonModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and CustomerId is not null", new String[]{CommonID});
        if (cursor.moveToNext()) {
            model = new CommonModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public CommonModel getCommonModel() {
        CommonModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where EndVisitTime is null", null);
        if (cursor.moveToNext()) {
            model = new CommonModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public List<CommonModel> list() {
        List<CommonModel> list = new ArrayList<CommonModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CustomerId is not null", null);
        while (cursor.moveToNext()) {
            list.add(new CommonModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CommonModel> listPerhari(int day) {
        List<CommonModel> list = new ArrayList<CommonModel>();
        try {
            Calendar cal = Calendar.getInstance();
            int cDay = cal.get(Calendar.DAY_OF_WEEK);
//			if(day > cDay) {
            day = day - cDay;
            cal.add(Calendar.DAY_OF_MONTH, day);
//			} else if(day < cDay) {
//				cDay = cDay - day;
//				cal.add(Calendar.DAY_OF_MONTH, -cDay);
//			}
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + table + " where BeginVisitTime like '" + new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()) + "%'", null);
            while (cursor.moveToNext()) {
                list.add(new CommonModel(cursor));
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CommonModel> listByStatus(String Status) {
        List<CommonModel> list = new ArrayList<CommonModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where Status=? and CustomerId is not null order by Status", new String[]{Status});
        while (cursor.moveToNext()) {
            list.add(new CommonModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public CommonModel getPendingCust() {
        CommonModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where Status=? and EndVisitTime is not null and CustomerId is not null order by Status", new String[]{"0"});
        if (cursor.moveToNext()) {
            model = new CommonModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public CommonModel getByCustomer(String custId) {
        CommonModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CustomerId=?", new String[]{custId});
        if (cursor.moveToNext()) {
            model = new CommonModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public CommonModel getByCustomerAndDate(String custId, String date) {
        CommonModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CustomerId=? and BeginVisitTime like '" + date + "%' and Done = 1", new String[]{custId});
        if (cursor.moveToNext()) {
            model = new CommonModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public void deleteAllDataPending(String commonID) {
        try {

            Realm realm = Realm.getDefaultInstance();
            RealmResults<com.bhn.sadix.Data.CommonModel> commonResult = realm
                    .where(com.bhn.sadix.Data.CommonModel.class)
                    .equalTo("CommonID", commonID)
                    .findAll();

            commonResult.deleteAllFromRealm();

            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from common where CommonID=?", new String[]{commonID});
            db.execSQL("delete from retur where CommonID=?", new String[]{commonID});
            db.execSQL("delete from collector where CommonID=?", new String[]{commonID});
            db.execSQL("delete from stock_outlet where CommonID=?", new String[]{commonID});
            db.execSQL("delete from stock_competitor where CommonID=?", new String[]{commonID});
            db.execSQL("delete from taking_order where CommonID=?", new String[]{commonID});
            db.execSQL("delete from GroupStock where CommonID=?", new String[]{commonID});
            db.execSQL("delete from DiscountPromo where CommonID=?", new String[]{commonID});
            db.execSQL("delete from DiscountResult where CommonID=?", new String[]{commonID});
//            db.execSQL("delete from Aset where CommonID=?", new String[]{commonID});
//            db.execSQL("delete from Newasset where CommonID=?", new String[]{commonID});
            db.execSQL("delete from Prinsiple where CommonID=?", new String[]{commonID});
            db.execSQL("delete from VisitNote where CommonID=?", new String[]{commonID});
            db.execSQL("delete from PriceMonitoring where CommonID=?", new String[]{commonID});
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAssetData(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from Aset where CommonID=?", new String[]{CommonID});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAssetMutasiData(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from Newasset where CommonID=?", new String[]{CommonID});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAssetMutasiData(String CommonID, String tipe) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from Aset where CommonID=? and tipe=?", new String[]{CommonID, tipe});
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject get(String sql) {
        JSONObject json = null;
        SQLiteDatabase db = ctrlDb.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    json.put(cursor.getColumnName(i), cursor.getString(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public void updateCustomerIdByCustomerRandomId(CustomerModel customerModel) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerId", customerModel.CustId);
            db.update(table, cv, "CustomerRID=?", new String[]{customerModel.RandomID});
            android.util.Log.e("CustomerId", "" + customerModel.CustId);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int pendingData() {
        int pending = 0;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) jml from " + table + " where Status=? and EndVisitTime is not null", new String[]{"0"});
        if (cursor.moveToNext()) {
            pending = cursor.getInt(0);
        }
        cursor.close();
        close();
        return pending;
    }

}
