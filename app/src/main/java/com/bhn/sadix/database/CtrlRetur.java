package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.ResumeOrderDetailModul;
import com.bhn.sadix.model.ResumeOrderModel;
import com.bhn.sadix.model.ReturModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlRetur {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "retur";
    private CtrlSKU ctrlSKU;
    private CtrlCustomer ctrlCustomer;

    public CtrlRetur(Context context) {
        this.context = context;
        ctrlSKU = new CtrlSKU(context);
        ctrlDb = new DatabaseHelper(context);
        ctrlCustomer = new CtrlCustomer(context);
    }

    public void save(ReturModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustId", model.CustId);
            cv.put("SKUId", model.SKUId);
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("note", model.note);
            cv.put("tipe", model.tipe);
            cv.put("CommonID", model.CommonID);
            cv.put("RandomID", model.RandomID);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(ReturModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("SKUId", model.SKUId);
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("note", model.note);
            cv.put("tipe", model.tipe);
            cv.put("CustId", model.CustId);
            cv.put("CommonID", model.CommonID);
            db.update(table, cv, "RandomID=?", new String[]{model.RandomID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    //	public ReturModel getReturModel(String CustId, String SKUId) {
//		ReturModel model = null;
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=? and SKUId=?", new String[]{CustId,SKUId});
//		while(cursor.moveToNext()) {
//			model = new ReturModel(cursor);
//		}
//		cursor.close();
//		close();
//		return model;
//	}
    public ReturModel getReturModel(String CommonID, String SKUId) {
        ReturModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and SKUId=?", new String[]{CommonID, SKUId});
        while (cursor.moveToNext()) {
            model = new ReturModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    //	public List<ReturModel> getReturModel(String CustId) {
//		List<ReturModel> list = new ArrayList<ReturModel>();
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=?", new String[]{CustId});
//		while(cursor.moveToNext()) {
//			list.add(new ReturModel(cursor));
//		}
//		cursor.close();
//		close();
//		return list;
//	}
    public List<ReturModel> getReturModel(String CommonID) {
        List<ReturModel> list = new ArrayList<ReturModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
        while (cursor.moveToNext()) {
            list.add(new ReturModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<ReturModel> list() {
        List<ReturModel> list = new ArrayList<ReturModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new ReturModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    //    public void deleteByCustomer(String CustId) {
//		try {
//			db=ctrlDb.getWritableDatabase();
//	        db.execSQL("delete from " + table + " where CustId=?", new String[]{CustId});
//			close();
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//    }
    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<ResumeOrderModel> listResumeOrder(String tanggal) {
        List<ResumeOrderModel> list = new ArrayList<ResumeOrderModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (tanggal != null && !tanggal.equals("")) {
            /*cursor = db.rawQuery(
                    "select a.SKUId,b.ProductName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K " +
					"from retur a inner join sku b on a.SKUId=b.SKUId " +
					"where a.tanggal like '" + tanggal + "%' " +
					"group by a.SKUId,b.ProductName"
			, null);*/

            cursor = db.rawQuery(
                    "select SKUId,SKUId ProductName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K, " +
                            "from retur " +
                            "where tanggal like '" + tanggal + "%' " +
                            "group by SKUId"
                    , null);
        } else {
            /*cursor = db.rawQuery(
                    "select a.SKUId,b.ProductName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K " +
					"from retur a inner join sku b on a.SKUId=b.SKUId " +
					"group by a.SKUId,b.ProductName"
			, null);*/
            cursor = db.rawQuery(
                    "select SKUId,SKUId ProductName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K " +
                            "from retur " +
                            "group by SKUId"
                    , null);
        }
        while (cursor.moveToNext()) {
            ResumeOrderModel model = new ResumeOrderModel(cursor);
            SKUModel skuModel = ctrlSKU.get(model.SKUId);
            model.ProductName = skuModel.ProductName;
//            list.add(new ResumeOrderModel(cursor));
            list.add(model);
        }
        cursor.close();
        close();
        return list;
    }

    public List<ResumeOrderDetailModul> listResumeOrderDetail(String tanggal, String SKUId) {
        List<ResumeOrderDetailModul> list = new ArrayList<ResumeOrderDetailModul>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (tanggal != null && !tanggal.equals("")) {
            /*cursor = db.rawQuery(
                    "select a.SKUId,b.ProductName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K " +
                            "from retur a inner join schedulecustomer b on a.SKUId=b.SKUId " +
                            "where a.SKUId='" + SKUId + "' and a.tanggal like '" + tanggal + "%' " +
                            "group by a.SKUId,b.ProductName"
                    , null);*/
            cursor = db.rawQuery(
                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K " +
                            "from retur " +
                            "where SKUId='" + SKUId + "' and tanggal like '" + tanggal + "%' " +
                            "group by CustId"
                    , null);
        } else {
            cursor = db.rawQuery(
                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K " +
                            "from retur where SKUId='" + SKUId + "' " +
                            "group by CustId"
                    , null);
        }
        while (cursor.moveToNext()) {
            try {
                ResumeOrderDetailModul model = new ResumeOrderDetailModul(cursor);
                Log.d("CustId", model.CustId);
                CustomerModel custModel = ctrlCustomer.get(model.CustId);
                Log.d("Customer Name", custModel.CustomerName);
                model.CustomerName = custModel.CustomerName;
                list.add(model);
            } catch (Exception e) {

            }
        }
        cursor.close();
        close();
        return list;
    }

}
