package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.FormSurveyModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlFormSurvey {
    private DbMasterHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "form_survey";

    public CtrlFormSurvey(Context context) {
        this.context = context;
        ctrlDb = new DbMasterHelper(context);
    }

    public void save(FormSurveyModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("FormId", model.FormId);
            cv.put("FormName", model.FormName);
            cv.put("FormRemark", model.FormRemark);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<FormSurveyModel> list() {
        List<FormSurveyModel> list = new ArrayList<FormSurveyModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new FormSurveyModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public FormSurveyModel get(String FormId) {
        FormSurveyModel skuModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where FormId=?", new String[]{FormId});
        if (cursor.moveToNext()) {
            skuModel = new FormSurveyModel(cursor);
        }
        cursor.close();
        close();
        return skuModel;
    }

    //	public List<FormSurveyModel> list(CustomerModel customerModel) {
//		List<FormSurveyModel> list = new ArrayList<FormSurveyModel>();
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table , null);
//		while(cursor.moveToNext()) {
//			FormSurveyModel formSurveyModel = new FormSurveyModel(cursor);
//			Cursor cursor2 = db.rawQuery("select count(*) jml from form_cust_type where FormId=?", new String[]{formSurveyModel.FormId});
//			cursor2.moveToNext();
//			int jml = cursor2.getInt(0);
//			if(jml > 0) {
//				cursor2.close();
//				cursor2 = db.rawQuery("select count(*) jml from form_cust_type where FormId=? and Id=?", new String[]{formSurveyModel.FormId,customerModel.CustomerTypeId});
//				cursor2.moveToNext();
//				jml = cursor2.getInt(0);
//				if(jml > 0) {
//					list.add(formSurveyModel);
//				}
//				cursor2.close();
//			} else {
//				cursor2.close();
//				list.add(formSurveyModel);
//			}
//		}
//		cursor.close();
//		close();
//		return list;
//	}
    public List<FormSurveyModel> list(CustomerModel customerModel) {
        List<FormSurveyModel> list = new ArrayList<FormSurveyModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select a.* from form_survey a " +
                        "inner join form_cust_type b " +
                        "on a.FormId=b.FormId where b.Id=?",
                new String[]{customerModel.CustomerTypeId}
        );
        while (cursor.moveToNext()) {
            FormSurveyModel formSurveyModel = new FormSurveyModel(cursor);
            list.add(formSurveyModel);
        }
        cursor.close();
        close();
        return list;
    }

}
