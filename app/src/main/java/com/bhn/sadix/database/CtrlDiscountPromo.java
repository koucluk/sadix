package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.DiscountPromoModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlDiscountPromo {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "DiscountPromo";

    public CtrlDiscountPromo(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(DiscountPromoModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerId", model.CustomerId);
            cv.put("DiscountPromoID", model.DiscountPromoID);
            cv.put("CommonID", model.CommonID);
            cv.put("RandomID", model.RandomID);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(DiscountPromoModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerId", model.CustomerId);
            cv.put("DiscountPromoID", model.DiscountPromoID);
            db.update(table, cv, "RandomID=?", new String[]{model.RandomID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public DiscountPromoModel getDiscountPromoModel(String CommonID, String DiscountPromoID) {
        DiscountPromoModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and DiscountPromoID=?", new String[]{CommonID, DiscountPromoID});
        while (cursor.moveToNext()) {
            model = new DiscountPromoModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public List<DiscountPromoModel> getDiscountPromoModel(String CommonID) {
        List<DiscountPromoModel> list = new ArrayList<DiscountPromoModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
        while (cursor.moveToNext()) {
            list.add(new DiscountPromoModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<DiscountPromoModel> list() {
        List<DiscountPromoModel> list = new ArrayList<DiscountPromoModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new DiscountPromoModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

}
