package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.SurveyOptModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlSurveyOpt {
    private DbMasterHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "survey_opt";

    public CtrlSurveyOpt(Context context) {
        this.context = context;
        ctrlDb = new DbMasterHelper(context);
    }

    public void save(SurveyOptModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("ListID", model.ListID);
            cv.put("SurveyId", model.SurveyId);
            cv.put("ListValue", model.ListValue);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SurveyOptModel> list() {
        List<SurveyOptModel> list = new ArrayList<SurveyOptModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new SurveyOptModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public SurveyOptModel get(String SurveyId, String ListID) {
        SurveyOptModel skuModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where SurveyId=? and ListID=? order by sort_order asc", new String[]{SurveyId, ListID});
        if (cursor.moveToNext()) {
            skuModel = new SurveyOptModel(cursor);
        }
        cursor.close();
        close();
        return skuModel;
    }

    public List<SurveyOptModel> listBySurveyId(String SurveyId) {
        List<SurveyOptModel> list = new ArrayList<SurveyOptModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where SurveyId=? order by sort_order asc", new String[]{SurveyId});
        while (cursor.moveToNext()) {
            list.add(new SurveyOptModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

}
