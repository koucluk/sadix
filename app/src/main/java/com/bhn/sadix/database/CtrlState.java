package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caksono on 03/04/17.
 */

public class CtrlState {
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "DataState";

    public CtrlState(Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context);
    }

    public void save(String commonID, String modul) {
        try {
            db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CommonID", commonID);
            cv.put("Modul", modul);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Cursor> list(String commonId) {
        List<Cursor> list = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID = '" + commonId + "'", null);
        while (cursor.moveToNext()) {
            list.add(cursor);
        }
        cursor.close();
        close();
        return list;
    }

    public List<Cursor> listPending(String commonId) {
        List<Cursor> list = new ArrayList<>();
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID = '" + commonId + "' and State = 0", null);
        while (cursor.moveToNext()) {
            list.add(cursor);
        }
        cursor.close();
        close();
        return list;
    }

    public void deleteState(String CommonID) {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID = '" + CommonID + "'");
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteStatebyModul(String CommonID, String modul) {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID = '" + CommonID + "' and Modul = '" + modul + "'");
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAllState() {
        try {
            db = dbHelper.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void close() {
        if (db != null) db.close();
    }
}
