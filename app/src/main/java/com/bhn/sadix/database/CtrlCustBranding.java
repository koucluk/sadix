package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bhn.sadix.model.CustBrandingModel;
import com.bhn.sadix.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CtrlCustBranding {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "branding_photo";
    private CtrlCustomer ctrlCustomer;
    private CtrlConfig ctrlConfig;

    public CtrlCustBranding(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
        ctrlCustomer = new CtrlCustomer(context);
        ctrlConfig = new CtrlConfig(context);
    }

    public void save(CustBrandingModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerID", model.CustomerID);
            cv.put("PhotoPic", model.PhotoPic);
            cv.put("BrandingID", model.BrandingID);
            cv.put("Description", model.Description);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("SalesID", model.SalesID);
            cv.put("Done", model.done);
            cv.put("CommonId", model.CommonID);
            Log.wtf("Saved", "model done " + model.done);

            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CustBrandingModel> list(String CustomerID, String CommonId) {
        List<CustBrandingModel> list = new ArrayList<CustBrandingModel>();
        db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select a.*,b.CustomerName,c.ConfigName from " + table + " a " +
//				"inner join schedulecustomer b on a.CustomerID=b.CustId " +
//				"inner join config c on a.BrandingID=c.ConfigId and c.ConfigClass=?", new String[]{"BRANDING_CATEGORY"});
        Cursor cursor = db.rawQuery("select * from " + table + " where CustomerID = '" + CustomerID + "' " +
                "and Done = 0 and CommonId='" + CommonId + "'", null);
        while (cursor.moveToNext()) {
            CustBrandingModel model = new CustBrandingModel(cursor);
//			model.NmCust = cursor.getString(7);
//			model.NmBranding = cursor.getString(8);
            model.NmCust = ctrlCustomer.get(model.CustomerID).CustomerName;
            model.NmBranding = ctrlConfig.getByConfigClassAndConfigId("BRANDING_CATEGORY", model.BrandingID).text;
            list.add(model);
        }
        cursor.close();
        close();
        return list;
    }

    public List<CustBrandingModel> list() {
        List<CustBrandingModel> list = new ArrayList<CustBrandingModel>();
        db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select a.*,b.CustomerName,c.ConfigName from " + table + " a " +
//				"inner join schedulecustomer b on a.CustomerID=b.CustId " +
//				"inner join config c on a.BrandingID=c.ConfigId and c.ConfigClass=?", new String[]{"BRANDING_CATEGORY"});
        Cursor cursor = db.rawQuery("select * from " + table + " where Done = 1", null);
        while (cursor.moveToNext()) {
            CustBrandingModel model = new CustBrandingModel(cursor);
//			model.NmCust = cursor.getString(7);
//			model.NmBranding = cursor.getString(8);
            Log.d("get Pending", "Cust Id " + model.CustomerID);
            model.NmCust = ctrlCustomer.get(model.CustomerID).CustomerName;
            model.NmBranding = ctrlConfig.getByConfigClassAndConfigId("BRANDING_CATEGORY", model.BrandingID).text;
            list.add(model);
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    /**/
    public void delete(String CustomerID, String BrandingID, String CommonId, boolean deleteFile) {
        Log.d("Deleting...", "CustomerID : " + CustomerID + " BrandingID : " + BrandingID + " CommonID : " + CommonId);
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CustomerID=? and BrandingID=? and CommonId=?", new String[]{CustomerID, BrandingID, CommonId});  /*and Done = 0*/
            if (deleteFile) {
                File file = new File(Util.LOKASI_IMAGE, CustomerID + "_" + BrandingID + ".jpg");
                file.delete();
            }
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void delete(String CustomerID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CustomerID=? and Done = 0", new String[]{CustomerID});
            /*File file = new File(Util.LOKASI_IMAGE, CustomerID + "_" + BrandingID + ".jpg");
            file.delete();*/
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public CustBrandingModel getPending() {
        CustBrandingModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where Done = 1", null);
        if (cursor.moveToNext()) {
            model = new CustBrandingModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

}
