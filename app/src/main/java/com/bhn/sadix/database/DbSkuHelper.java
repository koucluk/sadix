package com.bhn.sadix.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bhn.sadix.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DbSkuHelper extends SQLiteOpenHelper {

    private final String PATH_MASTER_DATA = Util.LOKASI + Util.DBSKU;
    public static String DB_PATH;
    public static String DB_NAME = Util.DBSKU;
    public SQLiteDatabase database;
    public final Context context;

    public SQLiteDatabase getDb() {
        return database;
    }

    public DbSkuHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        String packageName = context.getPackageName();
        DB_PATH = String.format("//data//data//%s//databases//", packageName);
    }

    public boolean isFileMasterData() {
        File file = new File(PATH_MASTER_DATA);
        boolean cek = file.exists();
        file = null;
        return cek;
    }

    public void deleteMasterData() {
        File file = new File(PATH_MASTER_DATA);
        if (file.exists()) {
            file.delete();
        }
        file = null;
    }

    public void createDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            Log.i(this.getClass().toString(), "Database already exists");
        }
    }

    public void importDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDb = null;
        try {
            String path = DB_PATH + DB_NAME;
            checkDb = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            Log.e(this.getClass().toString(), "Error while checking db");
        }
        if (checkDb != null) {
            checkDb.close();
        }
        return checkDb != null;
    }

    private void copyDataBase() throws IOException {
        InputStream externalDbStream = new FileInputStream(PATH_MASTER_DATA);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream localDbStream = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = externalDbStream.read(buffer)) > 0) {
            localDbStream.write(buffer, 0, bytesRead);
        }
        localDbStream.close();
        externalDbStream.close();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String path = DB_PATH + DB_NAME;
        if (database == null) {
            createDataBase();
            database = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }
        return database;
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
            database = null;
        }
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public Cursor query(String sql) {
        Cursor cursor = null;
        cursor = openDataBase().rawQuery(sql, null);
        return cursor;
    }
}
