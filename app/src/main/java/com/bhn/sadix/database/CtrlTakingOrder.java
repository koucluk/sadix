package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.PrinsipleOrderModel;
import com.bhn.sadix.model.ResumeOrderDetailModul;
import com.bhn.sadix.model.ResumeOrderModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlTakingOrder {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "taking_order";
    private CtrlSKU ctrlSKU;
    private CtrlCustomer ctrlCustomer;

    public CtrlTakingOrder(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
        ctrlSKU = new CtrlSKU(context);
        ctrlCustomer = new CtrlCustomer(context);
    }

    public void save(TakingOrderModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustId", model.CustId);
            cv.put("SKUId", model.SKUId);
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("note", model.note);
            cv.put("PAYMENT_TERM", model.PAYMENT_TERM);
            cv.put("SalesProgramId", model.SalesProgramId);
            cv.put("DISCOUNT1", model.DISCOUNT1);
            cv.put("DISCOUNT2", model.DISCOUNT2);
            cv.put("DISCOUNT3", model.DISCOUNT3);
            cv.put("TOTAL_PRICE", model.TOTAL_PRICE);
            cv.put("DiscountType", model.DiscountType);
            cv.put("CommonID", model.CommonID);
            cv.put("LAMA_KREDIT", model.LAMA_KREDIT);
            cv.put("PRICE_B", model.PRICE_B);
            cv.put("PRICE_K", model.PRICE_K);
            cv.put("RandomID", model.RandomID);
            cv.put("PrinsipleID", model.PrinsipleID);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(TakingOrderModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("SKUId", model.SKUId);
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("note", model.note);
            cv.put("PAYMENT_TERM", model.PAYMENT_TERM);
            cv.put("SalesProgramId", model.SalesProgramId);
            cv.put("DISCOUNT1", model.DISCOUNT1);
            cv.put("DISCOUNT2", model.DISCOUNT2);
            cv.put("DISCOUNT3", model.DISCOUNT3);
            cv.put("TOTAL_PRICE", model.TOTAL_PRICE);
            cv.put("DiscountType", model.DiscountType);
            cv.put("CustId", model.CustId);
            cv.put("LAMA_KREDIT", model.LAMA_KREDIT);
            cv.put("PRICE_B", model.PRICE_B);
            cv.put("PRICE_K", model.PRICE_K);
            cv.put("RandomID", model.RandomID);
            cv.put("PrinsipleID", model.PrinsipleID);
            db.update(table, cv, "CommonID=?", new String[]{model.CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public TakingOrderModel getTakingOrderModel(String CommonID, String SKUId) {
        TakingOrderModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and SKUId=?", new String[]{CommonID, SKUId});
        if (cursor.moveToNext()) {
            model = new TakingOrderModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public List<TakingOrderModel> getTakingOrderModel(String CommonID) {
        List<TakingOrderModel> list = new ArrayList<TakingOrderModel>();
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
            while (cursor.moveToNext()) {
                list.add(new TakingOrderModel(cursor));
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return list;
    }

    public List<TakingOrderModel> getTakingOrderPrinsiple(String CommonID, String PrincipleID) {
        List<TakingOrderModel> list = null;
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and PrinsipleID=?", new String[]{CommonID, PrincipleID});
            while (cursor.moveToNext()) {
                list.add(new TakingOrderModel(cursor));
            }
            cursor.close();
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getSKUIdTakingOrder(String CommonID) {
        String list = null;
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select SKUId from " + table + " where CommonID=?", new String[]{CommonID});
            while (cursor.moveToNext()) {
                if (list == null) {
                    list = "'" + cursor.getString(0) + "'";
                } else {
                    list = ",'" + cursor.getString(0) + "'";
                }
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return list;
    }

    public List<TakingOrderModel> list() {
        List<TakingOrderModel> list = new ArrayList<TakingOrderModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new TakingOrderModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<ResumeOrderModel> listResumeOrder(String tanggal) {
        List<ResumeOrderModel> list = new ArrayList<ResumeOrderModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (tanggal != null && !tanggal.equals("")) {
//			cursor = db.rawQuery(
//					"select a.SKUId,b.ProductName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K,sum(a.TOTAL_PRICE) TOTAL_PRICE " +
//					"from taking_order a inner join sku b on a.SKUId=b.SKUId " +
//					"where a.tanggal like '" + tanggal + "%' " +
//					"group by a.SKUId,b.ProductName"
//			, null);
            cursor = db.rawQuery(
                    "select SKUId,SKUId ProductName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K,sum(TOTAL_PRICE) TOTAL_PRICE " +
                            "from taking_order " +
                            "where tanggal like '" + tanggal + "%' " +
                            "group by SKUId"
                    , null);
        } else {
//			cursor = db.rawQuery(
//					"select a.SKUId,b.ProductName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K,sum(a.TOTAL_PRICE) TOTAL_PRICE " +
//					"from taking_order a inner join sku b on a.SKUId=b.SKUId " +
//					"group by a.SKUId,b.ProductName"
//			, null);
            cursor = db.rawQuery(
                    "select SKUId,SKUId ProductName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K,sum(TOTAL_PRICE) TOTAL_PRICE " +
                            "from taking_order " +
                            "group by SKUId"
                    , null);
        }
        while (cursor.moveToNext()) {
            ResumeOrderModel model = new ResumeOrderModel(cursor);
            SKUModel skuModel = ctrlSKU.get(model.SKUId);
            model.ProductName = skuModel.ProductName;
            list.add(model);
        }
        cursor.close();
        close();
        return list;
    }

    public List<ResumeOrderDetailModul> listResumeOrderDetail(String tanggal, String SKUId) {
        List<ResumeOrderDetailModul> list = new ArrayList<ResumeOrderDetailModul>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (tanggal != null && !tanggal.equals("")) {
//			cursor = db.rawQuery(
//					"select a.SKUId,b.CustomerName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K,sum(a.TOTAL_PRICE) TOTAL_PRICE " +
//					"from taking_order a inner join schedulecustomer b on a.CustId=b.CustId " +
//					"where a.SKUId='"+SKUId+"' and a.tanggal like '" + tanggal + "%' " +
//					"group by a.SKUId,b.CustomerName"
//			, null);
            cursor = db.rawQuery(
                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K,sum(TOTAL_PRICE) TOTAL_PRICE " +
                            "from taking_order " +
                            "where SKUId='" + SKUId + "' and tanggal like '" + tanggal + "%' " +
                            "group by SKUId"
                    , null);
        } else {
//			cursor = db.rawQuery(
//					"select CustId,b.CustomerName,sum(a.QTY_B) QTY_B,sum(a.QTY_K) QTY_K,sum(a.TOTAL_PRICE) TOTAL_PRICE " +
//					"from taking_order a inner join schedulecustomer b on a.CustId=b.CustId where a.SKUId='"+SKUId+"' " +
//					"group by a.CustId,b.CustomerName"
//			, null);
            cursor = db.rawQuery(
                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K,sum(TOTAL_PRICE) TOTAL_PRICE " +
                            "from taking_order where SKUId='" + SKUId + "' " +
                            "group by CustId"
                    , null);
        }
        while (cursor.moveToNext()) {
            try {
                ResumeOrderDetailModul model = new ResumeOrderDetailModul(cursor);
                Log.d("CustId", model.CustId);
                CustomerModel custModel = ctrlCustomer.get(model.CustId);
                model.CustomerName = custModel.CustomerName;
                list.add(model);
            } catch (Exception e) {

            }
        }
        cursor.close();
        close();
        return list;
    }

    public Cursor listOrder(String CommonID) {
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        cursor = db.rawQuery(
                "select SKUId,QTY_B,QTY_K,TOTAL_PRICE,DISCOUNT1, DISCOUNT2,DISCOUNT3,DiscountType,PRICE_B,PRICE_K " +
                        "from taking_order " +
                        "where CommonID=?"
                , new String[]{CommonID});
        return cursor;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public String getSKUInOrder(String CommonID) {
        String skuOrder = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select SKUId from " + table + " where CommonID=?", new String[]{CommonID});
        while (cursor.moveToNext()) {
            if (skuOrder == null) {
                skuOrder = "'" + cursor.getString(0) + "'";
            } else {
                skuOrder += ",'" + cursor.getString(0) + "'";
            }
        }
        cursor.close();
        close();
        return skuOrder;
    }

    public Cursor orderTotal(String CommonID) {
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        cursor = db.rawQuery(
                "select QTY_B,QTY_K,S_AMOUNT,PRICE,DISCOUNT " +
                        "from common " +
                        "where CommonID=?"
                , new String[]{CommonID});
        return cursor;
    }

    public void savePrinsiple(PrinsipleOrderModel prinsipleOrderModel) {
        try {
            Log.d("Saving Prinsiple B", prinsipleOrderModel.total_b + "");
            Log.d("Saving Prinsiple K", prinsipleOrderModel.total_k + "");
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CommonID", prinsipleOrderModel.CommonID);
            cv.put("RandomID", prinsipleOrderModel.RandomID);
            cv.put("D_VALUE", prinsipleOrderModel.D_VALUE);
            cv.put("G_AMOUNT", prinsipleOrderModel.G_AMOUNT);
            cv.put("PrinsipleId", prinsipleOrderModel.PrinsipleId);
            cv.put("S_AMOUNT", prinsipleOrderModel.S_AMOUNT);
            cv.put("K_TOTAL", prinsipleOrderModel.total_k);
            cv.put("B_TOTAL", prinsipleOrderModel.total_b);
            db.insert("Prinsiple", null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deletePrinsipleByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from Prinsiple where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<PrinsipleOrderModel> getPrinsipleOrderModel(String CommonID) {
        List<PrinsipleOrderModel> list = new ArrayList<>();
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from Prinsiple where CommonID=?", new String[]{CommonID});
            while (cursor.moveToNext()) {
                list.add(new PrinsipleOrderModel(cursor));
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return list;
    }

    public PrinsipleOrderModel getPrinsipleOrderModel(String CommonID, String PrincipleID) {
        PrinsipleOrderModel list = null;
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from Prinsiple where CommonID=? and PrinsipleId=?", new String[]{CommonID, PrincipleID});
            while (cursor.moveToNext()) {
                list = new PrinsipleOrderModel(cursor);
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return list;
    }

}
