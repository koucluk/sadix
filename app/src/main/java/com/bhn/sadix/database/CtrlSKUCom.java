package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlSKUCom {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "sku_com";

    public CtrlSKUCom(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(SKUComModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("SKUId", model.SKUId);
            cv.put("ProductName", model.ProductName);
            cv.put("GroupId", model.GroupId);
            cv.put("SBESAR", model.SBESAR);
            cv.put("SKECIL", model.SKECIL);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SKUComModel> list() {
        List<SKUComModel> list = new ArrayList<SKUComModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new SKUComModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SKUComModel> listByGroup(GroupSKUComModel groupSKUComModel) {
        List<SKUComModel> list = new ArrayList<SKUComModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String[]{groupSKUComModel.GroupId});
        while (cursor.moveToNext()) {
            SKUComModel skuModel = new SKUComModel(cursor);
            skuModel.groupSKUComModel = groupSKUComModel;
            list.add(skuModel);
        }
        cursor.close();
        close();
        return list;
    }

    public String filterByGroup(GroupSKUComModel groupSKUComModel) {
        StringBuilder sb = new StringBuilder();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String[]{groupSKUComModel.GroupId});
        while (cursor.moveToNext()) {
            SKUComModel skuModel = new SKUComModel(cursor);
            if (sb.length() == 0) {
                sb.append("'").append(skuModel.SKUId).append("'");
            } else {
                sb.append(",").append("'").append(skuModel.SKUId).append("'");
            }
        }
        cursor.close();
        close();
        return sb.toString();
    }

    public SKUComModel get(String SKUId) {
        SKUComModel skuModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where SKUId=?", new String[]{SKUId});
        if (cursor.moveToNext()) {
            skuModel = new SKUComModel(cursor);
        }
        cursor.close();
        close();
        return skuModel;
    }

}
