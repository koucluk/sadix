package com.bhn.sadix.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.MerchandiserModel;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetWidget.AssetWidgetListener;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DbAsetHelper extends SQLiteOpenHelper {

    private final String PATH_MASTER_DATA = Util.LOKASI + Util.DBASSET;
    public static String DB_PATH;
    public static String DB_NAME = Util.DBASSET;
    public SQLiteDatabase database;
    public final Context context;

    public SQLiteDatabase getDb() {
        return database;
    }

    public DbAsetHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        String packageName = context.getPackageName();
        DB_PATH = String.format("//data//data//%s//databases//", packageName);
    }

    public boolean isFileMasterData() {
        File file = new File(PATH_MASTER_DATA);
        boolean cek = file.exists();
        file = null;
        return cek;
    }

    public void deleteMasterData() {
        File file = new File(PATH_MASTER_DATA);
        if (file.exists()) {
            file.delete();
        }
        file = null;
    }

    public void createDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            Log.i(this.getClass().toString(), "Database already exists");
        }
    }

    public void importDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDb = null;
        try {
            String path = DB_PATH + DB_NAME;
            checkDb = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            Log.e(this.getClass().toString(), "Error while checking db");
        }
        if (checkDb != null) {
            checkDb.close();
        }
        return checkDb != null;
    }

    private void copyDataBase() throws IOException {
        InputStream externalDbStream = new FileInputStream(PATH_MASTER_DATA);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream localDbStream = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = externalDbStream.read(buffer)) > 0) {
            localDbStream.write(buffer, 0, bytesRead);
        }
        localDbStream.close();
        externalDbStream.close();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String path = DB_PATH + DB_NAME;
        if (database == null) {
            createDataBase();
            database = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }
        return database;
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
            database = null;
        }
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public Cursor query(String sql) {
        Cursor cursor = null;
        cursor = openDataBase().rawQuery(sql, null);
        return cursor;
    }

    public boolean isData(String sql) {
        Cursor cursor = query(sql);
        boolean data = cursor.moveToNext();
        close();
        return data;
    }

    public List<JSONObject> list(String sql) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        try {
            Cursor cursor = query(sql);
            while (cursor.moveToNext()) {
                JSONObject json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    json.put(cursor.getColumnName(i), cursor.getString(i));
                }
                list.add(json);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        return list;
    }

    public JSONObject get(String sql) {
        android.util.Log.e("sql", sql);
        JSONObject json = null;
        try {
            Cursor cursor = query(sql);
            if (cursor.moveToNext()) {
                json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    json.put(cursor.getColumnName(i), cursor.getString(i));
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        return json;
    }

    public List<ComboBoxModel> listCombo(String sql) {
        List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        try {
            Cursor cursor = query(sql);
            while (cursor.moveToNext()) {
                list.add(new ComboBoxModel(cursor));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        return list;
    }

    public List<ComboBoxModel> listComboPenarikan(String sql) {
        List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        try {
            Cursor cursor = query(sql);
            while (cursor.moveToNext()) {
                ComboBoxModel model = new ComboBoxModel();
                model.value = cursor.getString(0);
                model.text = cursor.getString(1);
                model.tmp1 = cursor.getString(2);
                list.add(model);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        close();
        return list;
    }

    public boolean saveAssetData(int CUSTID, String ASETNO, int ASETTYPE, int ASETMERK, String ASETNAME, String NoSeri) {
        boolean flag = false;
        try {
            ContentValues cv = new ContentValues();
            cv.put("CUSTID", CUSTID);
            cv.put("ASETSID", 0);
            cv.put("ASETTYPE", ASETTYPE);
            cv.put("ASETMERK", ASETMERK);
            cv.put("ASETNAME", ASETNAME);
            cv.put("ASETNOSERI", NoSeri);
            if (!isData("select * from ASSET_DATA where ASETNO='" + ASETNO + "'")) {
                android.util.Log.e("action", "save");
                cv.put("ASETNO", ASETNO);
                openDataBase().insert("ASSET_DATA", null, cv);
            } else {
                android.util.Log.e("action", "edit");
                openDataBase().update("ASSET_DATA", cv, "ASETNO=?", new String[]{String.valueOf(ASETNO)});
            }
            close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean deleteAsset(String aSETID) {
        boolean flag = false;
        try {
            openDataBase().execSQL("delete from ASSET_DATA where ASETSID='" + aSETID + "'");
            close();
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public List<MerchandiserModel> listMerchandiser(Context context, AssetWidgetListener listener, String CommonID) {
        List<MerchandiserModel> list = new ArrayList<MerchandiserModel>();
        try {
            Cursor cursor = query("select * from ASSET_CONFIG where TYPE='0'");
            while (cursor.moveToNext()) {
                list.add(new MerchandiserModel((Activity) context, listener, cursor, CommonID));
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.util.Log.e("list", "" + list.size());
        close();
        return list;
    }
}
