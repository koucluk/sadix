package com.bhn.sadix.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.bhn.sadix.util.Util;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbDiskonHelper extends SQLiteOpenHelper {

	private final String PATH_MASTER_DATA = Util.LOKASI+Util.DBDISKON;
	public static String DB_PATH;
	public static String DB_NAME = Util.DBDISKON;
	public SQLiteDatabase database;
	public final Context context;
	
	public SQLiteDatabase getDb() {
		return database;
	}
	public DbDiskonHelper(Context context) {
		super(context, DB_NAME, null, 1);
		this.context = context;
		String packageName = context.getPackageName();
		DB_PATH = String.format("//data//data//%s//databases//", packageName);
	}
	public boolean isFileMasterData() {
		File file = new File(PATH_MASTER_DATA);
		boolean cek = file.exists();
		file = null;
		return cek;
	}
	public void deleteMasterData() {
		File file = new File(PATH_MASTER_DATA);
		if(file.exists()) {
			file.delete();
		}
		file = null;
	}
	public void createDataBase() {
		boolean dbExist = checkDataBase();
		if (!dbExist) {
			this.getReadableDatabase();
			try {
				copyDataBase();
			} catch (IOException e) {
				Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
			}
		} else {
			Log.i(this.getClass().toString(), "Database already exists");
		}
	}
	public void importDataBase() {
		boolean dbExist = checkDataBase();
		if (!dbExist) {
			this.getReadableDatabase();
			try {
				copyDataBase();
				deleteMasterData();
			} catch (IOException e) {
				Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
			}
		} else {
			this.getReadableDatabase();
			try {
				copyDataBase();
				deleteMasterData();
			} catch (IOException e) {
				Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
			}
		}
	}
	private boolean checkDataBase() {
		SQLiteDatabase checkDb = null;
		try {
			String path = DB_PATH + DB_NAME;
			checkDb = SQLiteDatabase.openDatabase(path, null,
					SQLiteDatabase.OPEN_READONLY);
		} catch (SQLException e) {
			Log.e(this.getClass().toString(), "Error while checking db");
		}
		if (checkDb != null) {
			checkDb.close();
		}
		return checkDb != null;
	}
	private void copyDataBase() throws IOException {
		InputStream externalDbStream = new FileInputStream(PATH_MASTER_DATA);
		String outFileName = DB_PATH + DB_NAME;
		OutputStream localDbStream = new FileOutputStream(outFileName);
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = externalDbStream.read(buffer)) > 0) {
			localDbStream.write(buffer, 0, bytesRead);
		}
		localDbStream.close();
		externalDbStream.close();
	}
	public SQLiteDatabase openDataBase() throws SQLException {
		String path = DB_PATH + DB_NAME;
		if (database == null) {
			createDataBase();
			database = SQLiteDatabase.openDatabase(path, null,
				SQLiteDatabase.OPEN_READWRITE);
		}
		return database;
	}
	@Override
	public synchronized void close() {
		if (database != null) {
			database.close();
			database = null;
		}
		super.close();
	}
	@Override
	public void onCreate(SQLiteDatabase db) {}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
	
	public Cursor query(String sql) {
		Cursor cursor = null;
		cursor = openDataBase().rawQuery(sql, null);
		return cursor;
	}
	public int count(String sql) {
		int count = 0;
		try {
			Cursor cursor = query("select count(*) jml " + sql);
			if(cursor.moveToNext()) {
				count = cursor.getInt(0);
			}
			cursor.close();
		} catch (Exception e) {
		}
		close();
		return count;
	}
	public List<JSONObject> list(String sql) {
		List<JSONObject> list = new ArrayList<JSONObject>();
		try {
			Cursor cursor = query(sql);
			while(cursor.moveToNext()) {
				JSONObject json = new JSONObject();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					json.put(cursor.getColumnName(i), cursor.getString(i));
				}
				list.add(json);
			}
			cursor.close();
		} catch (Exception e) {
		}
		close();
		return list;
	}
	public JSONObject get(String sql) {
		JSONObject json = null;
		try {
			Cursor cursor = query(sql);
			if(cursor.moveToNext()) {
				json = new JSONObject();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					json.put(cursor.getColumnName(i), cursor.getString(i));
				}
			}
			cursor.close();
		} catch (Exception e) {
		}
		close();
		return json;
	}
}
