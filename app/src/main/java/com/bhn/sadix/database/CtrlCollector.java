package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CollectorModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlCollector {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "collector";

    public CtrlCollector(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(CollectorModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustId", model.CustId);
            cv.put("status", model.status);
            cv.put("data1", model.data1);
            cv.put("data2", model.data2);
            cv.put("data3", model.data3);
            cv.put("tanggal", model.tanggal);
            cv.put("CommonID", model.CommonID);
            cv.put("data4", model.data4);
            cv.put("data5", model.data5);
            cv.put("data6", model.data6);
            cv.put("RandomID", model.RandomID);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(CollectorModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("status", model.status);
            cv.put("data1", model.data1);
            cv.put("data2", model.data2);
            cv.put("data3", model.data3);
            cv.put("tanggal", model.tanggal);
            cv.put("CustId", model.CustId);
            cv.put("data4", model.data4);
            cv.put("data5", model.data5);
            cv.put("data6", model.data6);
            db.update(table, cv, "RandomID=?", new String[]{model.RandomID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    //	public void delete(CollectorModel model) {
//		try {
//			db = ctrlDb.getWritableDatabase();
//			db.delete(table, "CustId=?", new String []{model.CustId});
//			close();
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//	}
//    public void deleteByCustomer(String CustId) {
//		try {
//			db=ctrlDb.getWritableDatabase();
//	        db.execSQL("delete from " + table + " where CustId=?", new String[]{CustId});
//			close();
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//    }
    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CollectorModel> list() {
        List<CollectorModel> list = new ArrayList<CollectorModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new CollectorModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    //	public CollectorModel get(String CommonID) {
//		CollectorModel model = null;
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
//		if(cursor.moveToNext()) {
//			model = new CollectorModel(cursor);
//		}
//		cursor.close();
//		close();
//		return model;
//	}
    public List<CollectorModel> get(String CommonID) {
        List<CollectorModel> list = new ArrayList<CollectorModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
        while (cursor.moveToNext()) {
            list.add(new CollectorModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CollectorModel> get(String CommonID, String status) {
        List<CollectorModel> list = new ArrayList<CollectorModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and status=?", new String[]{CommonID, status});
        while (cursor.moveToNext()) {
            list.add(new CollectorModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

}
