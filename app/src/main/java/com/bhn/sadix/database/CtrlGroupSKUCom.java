package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlGroupSKUCom {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "group_sku_com";

    public CtrlGroupSKUCom(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(GroupSKUComModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("GroupId", model.GroupId);
            cv.put("GroupName", model.GroupName);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<GroupSKUComModel> list() {
        List<GroupSKUComModel> list = new ArrayList<GroupSKUComModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new GroupSKUComModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

}
