package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.StockOutletModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlStokOutlet {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "stock_outlet";
	public CtrlStokOutlet(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(StockOutletModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustId", model.CustId);
			cv.put("SKUId", model.SKUId);
			cv.put("tanggal", model.tanggal);
			cv.put("QTY_B", model.QTY_B);
			cv.put("QTY_K", model.QTY_K);
			cv.put("CommonID", model.CommonID);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(StockOutletModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("tanggal", model.tanggal);
			cv.put("QTY_B", model.QTY_B);
			cv.put("QTY_K", model.QTY_K);
			cv.put("CustId", model.CustId);
			db.update(table, cv, "CommonID=? and SKUId=?", new String []{model.CommonID, model.SKUId});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
//	public StockOutletModel getStockOutletModel(String CustId, String SKUId) {
//		StockOutletModel model = null;
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=? and SKUId=?", new String[]{CustId,SKUId});
//		while(cursor.moveToNext()) {
//			model = new StockOutletModel(cursor);
//		}
//		cursor.close();
//		close();
//		return model;
//	}
	public StockOutletModel getStockOutletModel(String CommonID, String SKUId) {
		StockOutletModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and SKUId=?", new String[]{CommonID,SKUId});
		while(cursor.moveToNext()) {
			model = new StockOutletModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
//	public List<StockOutletModel> getStockOutletModel(String CustId) {
//		List<StockOutletModel> model = new ArrayList<StockOutletModel>();
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=?", new String[]{CustId});
//		while(cursor.moveToNext()) {
//			model.add(new StockOutletModel(cursor));
//		}
//		cursor.close();
//		close();
//		return model;
//	}
	public List<StockOutletModel> getStockOutletModel(String CommonID) {
		List<StockOutletModel> model = new ArrayList<StockOutletModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
		while(cursor.moveToNext()) {
			model.add(new StockOutletModel(cursor));
		}
		cursor.close();
		close();
		return model;
	}
	public List<StockOutletModel> list() {
		List<StockOutletModel> list = new ArrayList<StockOutletModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new StockOutletModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
//    public void deleteByCustomer(String CustId) {
//		try {
//			db=ctrlDb.getWritableDatabase();
//	        db.execSQL("delete from " + table + " where CustId=?", new String[]{CustId});
//			close();
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//    }
    public void deleteByCommonID(String CommonID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
