package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.InformasiModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlInformasi {
	private DbMasterHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "salesinformation";
	public CtrlInformasi(Context context) {
		this.context = context;
		ctrlDb = new DbMasterHelper(context);
	}
	public void save(InformasiModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("Content", model.Content);
			cv.put("DateValidity", model.DateValidity);
			cv.put("Header", model.Header);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<InformasiModel> list() {
		List<InformasiModel> list = new ArrayList<InformasiModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustID = 0", null);
		while(cursor.moveToNext()) {
			list.add(new InformasiModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<InformasiModel> list(CustomerModel customerModel) {
		List<InformasiModel> list = new ArrayList<InformasiModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustID='"+customerModel.CustId+"' or CustID='0' order by CustID desc", null);
		while(cursor.moveToNext()) {
			list.add(new InformasiModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
