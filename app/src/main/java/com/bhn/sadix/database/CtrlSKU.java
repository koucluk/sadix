package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CtrlSKU {
    private DbSkuHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "sku";

    public CtrlSKU(Context context) {
        this.context = context;
        ctrlDb = new DbSkuHelper(context);
    }

    public void save(SKUModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("DISCOUNT_1_TYPE", model.DISCOUNT_1_TYPE);
            cv.put("DISCOUNT_2_TYPE", model.DISCOUNT_2_TYPE);
            cv.put("DISCOUNT_3_TYPE", model.DISCOUNT_3_TYPE);
            cv.put("GroupId", model.GroupId);
            cv.put("ProductName", model.ProductName);
            cv.put("SBESAR", model.SBESAR);
            cv.put("SKECIL", model.SKECIL);
            cv.put("SKUId", model.SKUId);
            cv.put("CONVER", model.CONVER);
            cv.put("DISCOUNT_1_MAX", model.DISCOUNT_1_MAX);
            cv.put("DISCOUNT_2_MAX", model.DISCOUNT_2_MAX);
            cv.put("DISCOUNT_3_MAX", model.DISCOUNT_3_MAX);
            cv.put("HET_PRICE", model.HET_PRICE);
            cv.put("HET_PRICE_L", model.HET_PRICE_L);
            cv.put("DISCOUNT_TYPE", model.DISCOUNT_TYPE);
            cv.put("QBESAR", model.QBESAR);
            cv.put("QKECIL", model.QKECIL);
            cv.put("QOBESAR", model.QOBESAR);
            cv.put("QOKECIL", model.QOKECIL);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(SKUModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("DISCOUNT_1_TYPE", model.DISCOUNT_1_TYPE);
            cv.put("DISCOUNT_2_TYPE", model.DISCOUNT_2_TYPE);
            cv.put("DISCOUNT_3_TYPE", model.DISCOUNT_3_TYPE);
            cv.put("GroupId", model.GroupId);
            cv.put("ProductName", model.ProductName);
            cv.put("SBESAR", model.SBESAR);
            cv.put("SKECIL", model.SKECIL);
            cv.put("CONVER", model.CONVER);
            cv.put("DISCOUNT_1_MAX", model.DISCOUNT_1_MAX);
            cv.put("DISCOUNT_2_MAX", model.DISCOUNT_2_MAX);
            cv.put("DISCOUNT_3_MAX", model.DISCOUNT_3_MAX);
            cv.put("HET_PRICE", model.HET_PRICE);
            cv.put("HET_PRICE_L", model.HET_PRICE_L);
            cv.put("DISCOUNT_TYPE", model.DISCOUNT_TYPE);
            cv.put("QBESAR", model.QBESAR);
            cv.put("QKECIL", model.QKECIL);
//			cv.put("QOBESAR", model.QOBESAR);
//			cv.put("QOKECIL", model.QOKECIL);
            db.update(table, cv, "SKUId=?", new String[]{model.SKUId});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void updateStokOrder(SKUModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("QBESAR", model.QBESAR);
            cv.put("QKECIL", model.QKECIL);
            cv.put("QOBESAR", model.QOBESAR);
            cv.put("QOKECIL", model.QOKECIL);
            db.update(table, cv, "SKUId=?", new String[]{model.SKUId});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SKUModel> list() {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new SKUModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SKUModel> listByGroupStok(GroupSKUModel groupSKUModel) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String[]{groupSKUModel.GroupId});
        while (cursor.moveToNext()) {
            SKUModel skuModel = new SKUModel(cursor);
            skuModel.groupSKUModel = groupSKUModel;
            list.add(skuModel);
        }
        cursor.close();
        close();
        return list;
    }

    public List<SKUModel> listByGroup(GroupSKUModel groupSKUModel, String CustGroupId) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        if (CustGroupId == null) {
            CustGroupId = "0";
        }
        db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String []{groupSKUModel.GroupId});
        Cursor cursor = db.rawQuery("select a.SKUId,a.GroupId,a.ProductName,case when b.SkuID is null then a.HET_PRICE else b.HET_PRICE end HET_PRICE,"
                + "a.SBESAR,a.SKECIL,a.CONVER,case when b.SkuID is null then a.HET_PRICE_L else b.HET_PRICE_L end HET_PRICE_L,a.DISCOUNT_1_TYPE,a.DISCOUNT_1_MAX,"
                + "a.DISCOUNT_2_TYPE,a.DISCOUNT_2_MAX,a.DISCOUNT_3_TYPE,a.DISCOUNT_3_MAX,a.DISCOUNT_TYPE,a.QBESAR,a.QKECIL,a.QOBESAR,a.QOKECIL,a.SKUCODE "
                + "from sku a left join custgroup_sku b on a.SKUId=b.SkuID and b.CustGroupId=? where a.GroupId=?", new String[]{CustGroupId, groupSKUModel.GroupId});
        while (cursor.moveToNext()) {
            SKUModel skuModel = new SKUModel(cursor);
            skuModel.groupSKUModel = groupSKUModel;
            list.add(skuModel);
        }
        cursor.close();
        close();
        return list;
    }

    public List<SKUModel> listByGroupStokOnCar(GroupSKUModel groupSKUModel) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=? and QOBESAR > 0 or QOKECIL > 0", new String[]{groupSKUModel.GroupId});
        while (cursor.moveToNext()) {
            SKUModel skuModel = new SKUModel(cursor);
            skuModel.groupSKUModel = groupSKUModel;
            list.add(skuModel);
        }
        cursor.close();
        close();
        return list;
    }

    public SKUModel get(String SKUId) {
        SKUModel skuModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where SKUId=?", new String[]{SKUId});
        if (cursor.moveToNext()) {
            skuModel = new SKUModel(cursor);
        }
        cursor.close();
        close();
        return skuModel;
    }

    public boolean isSearchSKU(String groupId, String search) {
        boolean isSearch = false;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) jml from " + table + " where GroupId=? and ProductName like '%" + search.replaceAll("'", "''") + "%'", new String[]{groupId});
        cursor.moveToNext();
        int jumlah = cursor.getInt(0);
        if (jumlah > 0) {
            isSearch = true;
        }
        cursor.close();
        close();
        return isSearch;
    }

    public List<SKUModel> searchSKU(String search, String CustGroupId) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
//		String sql = "select a.*,b.PromoId,b.FokusId from " + table + " a " +
//				"inner join group_sku b on a.GroupId=b.GroupId " +
//				"where 1=1 ";
        String sql = "select a.SKUId,a.GroupId,a.ProductName,case when b.SkuID is null then a.HET_PRICE else b.HET_PRICE end HET_PRICE,"
                + "a.SBESAR,a.SKECIL,a.CONVER,case when b.SkuID is null then a.HET_PRICE_L else b.HET_PRICE_L end HET_PRICE_L,a.DISCOUNT_1_TYPE,"
                + "a.DISCOUNT_1_MAX,a.DISCOUNT_2_TYPE,a.DISCOUNT_2_MAX,a.DISCOUNT_3_TYPE,a.DISCOUNT_3_MAX,a.DISCOUNT_TYPE,a.QBESAR,a.QKECIL,a.QOBESAR,"
                + "a.QOKECIL,a.SKUCODE,c.PromoId,c.FokusId "
                + "from sku a "
                + "left join custgroup_sku b on a.SKUId=b.SkuID and b.CustGroupId='" + CustGroupId + "' "
                + "inner join group_sku c on a.GroupId=c.GroupId where 1=1 ";
        if (search != null && !search.trim().equals("")) {
            sql += " and (a.ProductName like '%" + search + "%' or a.SKUCODE like '%" + search + "%')";
        } else {
            sql += " limit 100";
        }
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            SKUModel sku = new SKUModel(cursor);
            sku.PromoId = cursor.getString(19);
            sku.FokusId = cursor.getString(20);
            list.add(sku);
        }
        cursor.close();
        close();
        return list;
    }

    public List<SKUModel> searchSKUByGroup(String search, String GroupId) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
//		String sql = "select a.*,b.PromoId,b.FokusId from " + table + " a " +
//				"inner join group_sku b on a.GroupId=b.GroupId " +
//				"where 1=1 ";
        String sql = "select a.SKUId,a.GroupId,a.ProductName,a.HET_PRICE,"
                + "a.SBESAR,a.SKECIL,a.CONVER,a.HET_PRICE_L,a.DISCOUNT_1_TYPE,"
                + "a.DISCOUNT_1_MAX,a.DISCOUNT_2_TYPE,a.DISCOUNT_2_MAX,a.DISCOUNT_3_TYPE,a.DISCOUNT_3_MAX,a.DISCOUNT_TYPE,a.QBESAR,a.QKECIL,a.QOBESAR,"
                + "a.QOKECIL,a.SKUCODE "
                + "from sku a ";
        if (search != null && !search.trim().equals("")) {
            sql += " where (a.ProductName like '%" + search + "%' or a.SKUId like '%" + search + "%') and a.GroupId = '" + GroupId + "'";
        } else {
            sql += " limit 100";
        }
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            SKUModel sku = new SKUModel(cursor);
            list.add(sku);
        }
        cursor.close();
        close();
        return list;
    }

    public List<SKUModel> searchSKUByPrinsipel(String search, String CustGroupId, PrincipleModel prinsipleModel) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        db = ctrlDb.getWritableDatabase();
//		String sql = "select a.*,b.PromoId,b.FokusId from " + table + " a " +
//				"inner join group_sku b on a.GroupId=b.GroupId " +
//				"where 1=1 ";
        String sql = "select a.SKUId,a.GroupId,a.ProductName,case when b.SkuID is null then a.HET_PRICE else b.HET_PRICE end HET_PRICE,"
                + "a.SBESAR,a.SKECIL,a.CONVER,case when b.SkuID is null then a.HET_PRICE_L else b.HET_PRICE_L end HET_PRICE_L,a.DISCOUNT_1_TYPE,"
                + "a.DISCOUNT_1_MAX,a.DISCOUNT_2_TYPE,a.DISCOUNT_2_MAX,a.DISCOUNT_3_TYPE,a.DISCOUNT_3_MAX,a.DISCOUNT_TYPE,a.QBESAR,a.QKECIL,a.QOBESAR,"
                + "a.QOKECIL,a.SKUCODE,c.PromoId,c.FokusId "
                + "from sku a "
                + "left join custgroup_sku b on a.SKUId=b.SkuID and b.CustGroupId='" + CustGroupId + "' "
                + "inner join group_sku c on a.GroupId=c.GroupId where c.PrinsipleId='" + prinsipleModel.PrinsipleId + "' ";
        if (search != null && !search.trim().equals("")) {
            sql += " and (a.ProductName like '%" + search + "%' or a.SKUCODE like '%" + search + "%')";
        } else {
            sql += " limit 100";
        }
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            SKUModel sku = new SKUModel(cursor);
            sku.PromoId = cursor.getString(19);
            sku.FokusId = cursor.getString(20);
            list.add(sku);
        }
        cursor.close();
        close();
        return list;
    }

    public HashMap<String, Double> getPriceByGroupCUstomer(String CustGroupId, String SkuID) {
        HashMap<String, Double> data = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select HET_PRICE,HET_PRICE_L from custgroup_sku where CustGroupId=? and SkuID=?", new String[]{CustGroupId, SkuID});
        if (cursor.moveToNext()) {
            data = new HashMap<String, Double>();
            data.put("HET_PRICE", cursor.getDouble(0));
            data.put("HET_PRICE_L", cursor.getDouble(1));
        }
        cursor.close();
        close();
        return data;
    }

    public void clearDataEndCanvas() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("update sku set QBESAR=0,QKECIL=0");
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String filterByGroup(GroupSKUModel groupSKUModel) {
        StringBuilder sb = new StringBuilder();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String[]{groupSKUModel.GroupId});
        while (cursor.moveToNext()) {
            SKUModel skuModel = new SKUModel(cursor);
            if (sb.length() == 0) {
                sb.append("'").append(skuModel.SKUId).append("'");
            } else {
                sb.append(",").append("'").append(skuModel.SKUId).append("'");
            }
        }
        cursor.close();
        close();
        return sb.toString();
    }

    public List<SKUModel> listByGroupTakingOrder2(GroupSKUModel groupSKUModel, String CustGroupId, int jenis, String CustomerTypeId) {
        List<SKUModel> list = new ArrayList<SKUModel>();
        if (CustGroupId == null) {
            CustGroupId = "0";
        }
        db = ctrlDb.getWritableDatabase();
        String sql = "select a.SKUId,a.GroupId,a.ProductName,a.HET_PRICE,"
                + "a.SBESAR,a.SKECIL,a.CONVER,a.HET_PRICE_L,a.DISCOUNT_1_TYPE,a.DISCOUNT_1_MAX,"
                + "a.DISCOUNT_2_TYPE,a.DISCOUNT_2_MAX,a.DISCOUNT_3_TYPE,a.DISCOUNT_3_MAX,a.DISCOUNT_TYPE,a.QBESAR,a.QKECIL,a.QOBESAR,a.QOKECIL,a.SKUCODE "
                + "from sku a "
                + "inner join sku_config b on a.SKUId=b.skuid and b.code=1 "
                + "inner join sku_config c on a.SKUId=c.skuid and c.code=4 "
                + "where a.GroupId=?";
        if (jenis == 2) {
            sql += " and b.value='1' and c.value='" + CustomerTypeId + "'";
        } else if (jenis == 3) {
            sql += " and b.value='0' and c.value='" + CustomerTypeId + "'";
        }
        Cursor cursor = db.rawQuery(sql, new String[]{groupSKUModel.GroupId});
        while (cursor.moveToNext()) {
            SKUModel skuModel = new SKUModel(cursor);
            skuModel.groupSKUModel = groupSKUModel;
            list.add(skuModel);
        }
        cursor.close();
        close();
        return list;
    }

}
