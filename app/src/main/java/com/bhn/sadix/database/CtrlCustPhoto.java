package com.bhn.sadix.database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.CustPhotoModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlCustPhoto {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "cust_photo";
	private CtrlCustomer ctrlCustomer;
	private CtrlConfig ctrlConfig;
	public CtrlCustPhoto(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
		ctrlCustomer = new CtrlCustomer(context);
		ctrlConfig = new CtrlConfig(context);
	}
	public void save(CustPhotoModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerID", model.CustomerID);
			cv.put("PhotoPic", model.PhotoPic);
			cv.put("PhotoTypeID", model.PhotoTypeID);
			cv.put("SalesID", model.SalesID);
			cv.put("RandomID", model.RandomID);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<CustPhotoModel> list() {
		List<CustPhotoModel> list = new ArrayList<CustPhotoModel>();
		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select a.*,b.CustomerName,c.ConfigName from " + table + " a " +
//				"inner join schedulecustomer b on a.CustomerID=b.CustId " +
//				"inner join config c on a.PhotoTypeID=c.ConfigId and c.ConfigClass=?", new String[]{"PHOTO_TYPE"});
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			CustPhotoModel model = new CustPhotoModel(cursor);
//			model.NmCust = cursor.getString(4);
//			model.NmPhoto = cursor.getString(5);
			model.NmCust = ctrlCustomer.get(model.CustomerID).CustomerName;
			model.NmPhoto = ctrlConfig.getByConfigClassAndConfigId("PHOTO_TYPE", model.PhotoTypeID).text;
			list.add(model);
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void delete(String CustomerID, String PhotoTypeID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where CustomerID=? and PhotoTypeID=?", new String[]{CustomerID,PhotoTypeID});
	        File file = new File(Util.LOKASI_IMAGE, CustomerID+"_"+PhotoTypeID+".jpg");
	        file.delete();
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
	public CustPhotoModel getPending() {
		CustPhotoModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		if(cursor.moveToNext()) {
			model = new CustPhotoModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public void updateCustomerIdByCustomerRandomId(CustomerModel customerModel) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerID", customerModel.CustId);
			db.update(table, cv, "RandomID=?", new String []{customerModel.RandomID});
			close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
