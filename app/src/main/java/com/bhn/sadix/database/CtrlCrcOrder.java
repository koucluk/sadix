package com.bhn.sadix.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CrcOrderModel;

import java.util.ArrayList;
import java.util.List;

public class CtrlCrcOrder {
    private DbMasterHelper ctrlDb;
    private SQLiteDatabase db;
    private final String table = "crcorder";

    public CtrlCrcOrder(Context context) {
        ctrlDb = new DbMasterHelper(context);
    }

    public List<CrcOrderModel> list() {
        List<CrcOrderModel> list = new ArrayList<CrcOrderModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new CrcOrderModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public CrcOrderModel get(int CustID, int SkuID) {
        CrcOrderModel model = new CrcOrderModel();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CustID=? and SkuID=?", new String[]{String.valueOf(CustID), String.valueOf(SkuID)});
        if (cursor.moveToNext()) {
            model = new CrcOrderModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public List<CrcOrderModel> list(String CustID, String SkuID) {
        List<CrcOrderModel> list = new ArrayList<CrcOrderModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CustID=? and SkuID=?", new String[]{CustID, SkuID});
        while (cursor.moveToNext()) {
            list.add(new CrcOrderModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

}
