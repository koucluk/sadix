package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.FormCustTypeModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlFormCustType {
	private DbMasterHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "form_cust_type";
	public CtrlFormCustType(Context context) {
		this.context = context;
		ctrlDb = new DbMasterHelper(context);
	}
	public void save(FormCustTypeModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("FormId", model.FormId);
			cv.put("Id", model.Id);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<FormCustTypeModel> list() {
		List<FormCustTypeModel> list = new ArrayList<FormCustTypeModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new FormCustTypeModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
	public FormCustTypeModel get(String FormId, String Id) {
		FormCustTypeModel skuModel = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where FormId=? and Id=?", new String []{FormId,Id});
		if(cursor.moveToNext()) {
			skuModel = new FormCustTypeModel(cursor);
		}
		cursor.close();
		close();
		return skuModel;
	}

}
