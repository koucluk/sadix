package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CommonSurveyModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.FormSurveyModel;
import com.bhn.sadix.model.HasilSurveyModel;
import com.bhn.sadix.model.SurveyModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlSurvey {
    private DbMasterHelper ctrlDb;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "survey";

    public CtrlSurvey(Context context) {
        this.context = context;
        ctrlDb = new DbMasterHelper(context);
        databaseHelper = new DatabaseHelper(context);
    }

    public void save(SurveyModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("FormId", model.FormId);
            cv.put("SurveyId", model.SurveyId);
            cv.put("SurveyName", model.SurveyName);
            cv.put("SurveyType", model.SurveyType);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<SurveyModel> list() {
        List<SurveyModel> list = new ArrayList<SurveyModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new SurveyModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<SurveyModel> list(FormSurveyModel formSurveyModel) {
        List<SurveyModel> list = new ArrayList<SurveyModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where FormId=? order by sort_order asc", new String[]{formSurveyModel.FormId});
        while (cursor.moveToNext()) {
            list.add(new SurveyModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public SurveyModel get(String SurveyId) {
        SurveyModel skuModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where SurveyId=?", new String[]{SurveyId});
        if (cursor.moveToNext()) {
            skuModel = new SurveyModel(cursor);
        }
        cursor.close();
        close();
        return skuModel;
    }

    public void saveCommonSurvey(CommonSurveyModel model) {
        try {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("BeginSurveyTime", model.BeginSurveyTime);
            cv.put("CommonId", model.CommonId);
            cv.put("CustomerId", model.CustomerId);
            cv.put("EndSurveyTime", model.EndSurveyTime);
            cv.put("GeoLat", model.GeoLat);
            cv.put("GeoLong", model.GeoLong);
            cv.put("SalesId", model.SalesId);
            cv.put("FormId", model.FormId);
            cv.put("Done", model.done);
            db.insert("CommonSurvey", null, cv);
            db.close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void saveHasilSurvey(HasilSurveyModel model) {
        try {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("FormId", model.FormId);
            cv.put("CommonId", model.CommonId);
            cv.put("ListID", model.ListID);
            cv.put("ListValue", model.ListValue);
            cv.put("SurveyId", model.SurveyId);
            db.insert("HasilSurvey", null, cv);
            db.close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<List<String>> getHasilSurvey(String commonID, String FormID, String SurveyID) {
        List<List<String>> hasil = new ArrayList<>();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select ListID, ListValue from HasilSurvey where CommonId=? and " +
                "FormId=? and SurveyId=?", new String[]{commonID, FormID, SurveyID});

        while (cursor.moveToNext()) {
            List<String> result = new ArrayList<>();
            String ListID = cursor.getString(0);
            String ListValue = cursor.getString(1);

            result.add(ListID);
            result.add(ListValue);

            hasil.add(result);
        }

        return hasil;
    }

    public List<CommonSurveyModel> listCommon() {
        List<CommonSurveyModel> list = new ArrayList<CommonSurveyModel>();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select a.*,b.CustomerName,c.FormName from CommonSurvey a " +
//				"inner join schedulecustomer b on a.CustomerId=b.CustId " +
//				"inner join form_survey c on a.FormId=c.FormId", null);
        Cursor cursor = db.rawQuery("select * from CommonSurvey where Done = 1", null);
        CtrlFormSurvey ctrlForm = new CtrlFormSurvey(context);
        CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
        while (cursor.moveToNext()) {
            CommonSurveyModel model = new CommonSurveyModel(cursor);
//			model.nmCustomer = cursor.getString(8);
//			model.nmForm = cursor.getString(9);
            FormSurveyModel form = ctrlForm.get(model.FormId);
            model.nmForm = form.FormName;
            CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
            model.nmCustomer = customerModel.CustomerName;
            list.add(model);
        }
        cursor.close();
        db.close();
        return list;
    }

    public CommonSurveyModel getCommonSurvey() {
        CommonSurveyModel skuModel = null;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from CommonSurvey where Done = 1", null);
        if (cursor.moveToNext()) {
            skuModel = new CommonSurveyModel(cursor);
        }
        cursor.close();
        db.close();
        return skuModel;
    }

    public CommonSurveyModel getUndoneCommonSurvey(String commonId) {
        CommonSurveyModel skuModel = null;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from CommonSurvey where CommonId=? and Done = 0", new String[]{commonId});
        if (cursor.moveToNext()) {
            skuModel = new CommonSurveyModel(cursor);
        }
        cursor.close();
        db.close();
        return skuModel;
    }

    public void deleteCommonSurvey(String commonId) {
        try {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            db.execSQL("delete from CommonSurvey where CommonId=?", new String[]{commonId});
            db.execSQL("delete from HasilSurvey where CommonId=?", new String[]{commonId});
            db.close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<HasilSurveyModel> listHasilSurvey(String commonId) {
        List<HasilSurveyModel> list = new ArrayList<HasilSurveyModel>();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from HasilSurvey where CommonId=?", new String[]{commonId});
        while (cursor.moveToNext()) {
            list.add(new HasilSurveyModel(cursor));
        }
        cursor.close();
        db.close();
        return list;
    }

}
