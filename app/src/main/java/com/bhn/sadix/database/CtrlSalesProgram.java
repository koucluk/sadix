package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlSalesProgram {
	private DbSkuHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "sales_program";
	public CtrlSalesProgram(Context context) {
		this.context = context;
		ctrlDb = new DbSkuHelper(context);
	}
	public void save(SalesProgramModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("SalesProgramId", model.SalesProgramId);
			cv.put("SalesProgramDescription", model.SalesProgramDescription);
			cv.put("GroupId", model.GroupId);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<SalesProgramModel> list() {
		List<SalesProgramModel> list = new ArrayList<SalesProgramModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new SalesProgramModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public SalesProgramModel get(String SalesProgramId) {
		SalesProgramModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where SalesProgramId=?", new String[]{SalesProgramId});
		while(cursor.moveToNext()) {
			model = new SalesProgramModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public List<SalesProgramModel> listByGropID(String GroupId) {
		List<SalesProgramModel> list = new ArrayList<SalesProgramModel>();
		list.add(new SalesProgramModel("0", "", ""));
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where GroupId=?", new String[]{GroupId});
		while(cursor.moveToNext()) {
			list.add(new SalesProgramModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
