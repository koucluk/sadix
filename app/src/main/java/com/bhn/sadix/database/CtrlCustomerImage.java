package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.CustomerImageModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlCustomerImage {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "image_customer";
	public CtrlCustomerImage(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(CustomerImageModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustId", model.CustId);
			cv.put("type", model.type);
			cv.put("namaFile", model.namaFile);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(CustomerImageModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("type", model.type);
			cv.put("namaFile", model.namaFile);
			db.update(table, cv, "CustId=?", new String []{model.CustId});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
    public void deleteByCustomer(String CustId) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where CustId=?", new String[]{CustId});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
	public List<CustomerImageModel> list() {
		List<CustomerImageModel> list = new ArrayList<CustomerImageModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new CustomerImageModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public CustomerImageModel get(String CustId) {
		CustomerImageModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=?", new String[]{CustId});
		if(cursor.moveToNext()) {
			model = new CustomerImageModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
