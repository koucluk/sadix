package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.StockCompetitorModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlStokCompetitor {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "stock_competitor";

    public CtrlStokCompetitor(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(StockCompetitorModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustId", model.CustId);
            cv.put("SKUId", model.SKUId);
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("CommonID", model.CommonID);
            cv.put("PRICE", model.PRICE);
            cv.put("PRICE2", model.PRICE2);
            cv.put("Note", model.Note);
            cv.put("RandomID", model.RandomID);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(StockCompetitorModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("tanggal", model.tanggal);
            cv.put("QTY_B", model.QTY_B);
            cv.put("QTY_K", model.QTY_K);
            cv.put("CustId", model.CustId);
            cv.put("PRICE", model.PRICE);
            cv.put("PRICE2", model.PRICE2);
            cv.put("Note", model.Note);
            cv.put("RandomID", model.RandomID);
            db.update(table, cv, "CommonID=? and SKUId=?", new String[]{model.CommonID, model.SKUId});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    //	public StockCompetitorModel getStockCompetitorModel(String CustId, String SKUId) {
//		StockCompetitorModel model = null;
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=? and SKUId=?", new String[]{CustId,SKUId});
//		while(cursor.moveToNext()) {
//			model = new StockCompetitorModel(cursor);
//		}
//		cursor.close();
//		close();
//		return model;
//	}
    public StockCompetitorModel getStockCompetitorModel(String CommonID, String SKUId) {
        StockCompetitorModel model = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and SKUId=?", new String[]{CommonID, SKUId});
        while (cursor.moveToNext()) {
            model = new StockCompetitorModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    //	public List<StockCompetitorModel> getStockCompetitorModel(String CustId) {
//		List<StockCompetitorModel> model = new ArrayList<StockCompetitorModel>();
//		db = ctrlDb.getWritableDatabase();
//		Cursor cursor = db.rawQuery("select * from " + table + " where CustId=?", new String[]{CustId});
//		while(cursor.moveToNext()) {
//			model.add(new StockCompetitorModel(cursor));
//		}
//		cursor.close();
//		close();
//		return model;
//	}
    public List<StockCompetitorModel> getStockCompetitorModel(String CommonID) {
        List<StockCompetitorModel> model = new ArrayList<StockCompetitorModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
        while (cursor.moveToNext()) {
            model.add(new StockCompetitorModel(cursor));
        }
        cursor.close();
        close();
        return model;
    }

    public List<StockCompetitorModel> list() {
        List<StockCompetitorModel> list = new ArrayList<StockCompetitorModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new StockCompetitorModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    //    public void deleteByCustomer(String CustId) {
//		try {
//			db=ctrlDb.getWritableDatabase();
//	        db.execSQL("delete from " + table + " where CustId=?", new String[]{CustId});
//			close();
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//    }
    public void deleteByCommonID(String CommonID) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

}
