package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.DiscountResultModel;
import com.bhn.sadix.util.Util;

public class CtrlDiscountResult {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "DiscountResult";
	public CtrlDiscountResult(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(DiscountResultModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerId", model.CustomerId);
			cv.put("DiscountResultID", model.DiscountResultID);
			cv.put("DiscountResultValue", model.DiscountResultValue);
			cv.put("RandomID", model.RandomID);
			cv.put("CommonID", model.CommonID);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(DiscountResultModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerId", model.CustomerId);
			cv.put("DiscountResultID", model.DiscountResultID);
			cv.put("DiscountResultValue", model.DiscountResultValue);
			db.update(table, cv, "RandomID=?", new String []{model.RandomID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public DiscountResultModel getDiscountResultModel(String CommonID, String DiscountResultID) {
		DiscountResultModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and DiscountResultID=?", new String[]{CommonID,DiscountResultID});
		while(cursor.moveToNext()) {
			model = new DiscountResultModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public List<DiscountResultModel> getDiscountResultModel(String CommonID) {
		List<DiscountResultModel> list = new ArrayList<DiscountResultModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
		while(cursor.moveToNext()) {
			list.add(new DiscountResultModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<DiscountResultModel> list() {
		List<DiscountResultModel> list = new ArrayList<DiscountResultModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new DiscountResultModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteByCommonID(String CommonID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
