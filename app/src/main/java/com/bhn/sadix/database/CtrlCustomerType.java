package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class CtrlCustomerType {
	private DbMasterHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "customertype";
	public CtrlCustomerType(Context context) {
		this.context = context;
		ctrlDb = new DbMasterHelper(context);
	}
	public void save(CustomerTypeModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerTypeId", model.CustomerTypeId);
			cv.put("Description", model.Description);
			cv.put("SC", model.SC);
			cv.put("DiskonType", model.DiskonType);
			cv.put("DiskonMax", model.DiskonMax);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public List<CustomerTypeModel> list() {
		List<CustomerTypeModel> list = new ArrayList<CustomerTypeModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new CustomerTypeModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public CustomerTypeModel get(String CustomerTypeId) {
		CustomerTypeModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustomerTypeId=?", new String[]{CustomerTypeId});
		if(cursor.moveToNext()) {
			model = new CustomerTypeModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public List<ComboBoxModel> listComboBox() {
		List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select CustomerTypeId,Description from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new ComboBoxModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
