package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlCustomer {
    private DbMasterHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private CtrlAppModul ctrlAppModul;
    private final String table = "schedulecustomer";

    public CtrlCustomer(Context context) {
        this.context = context;
        ctrlAppModul = new CtrlAppModul(context);
        ctrlDb = new DbMasterHelper(context);
    }

    public void save(CustomerModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustId", model.CustId);
            cv.put("CustomerAddress", model.CustomerAddress);
            cv.put("CustomerEmail", model.CustomerEmail);
            cv.put("CustomerFax", model.CustomerFax);
            cv.put("CustomerName", model.CustomerName);
            cv.put("CustomerNPWP", model.CustomerNPWP);
            cv.put("CustomerPhone", model.CustomerPhone);
            cv.put("OwnerName", model.OwnerName);
            cv.put("OwnerAddress", model.OwnerAddress);
            cv.put("OwnerEmail", model.OwnerEmail);
            cv.put("OwnerPhone", model.OwnerPhone);
            cv.put("GeoLong", model.GeoLong);
            cv.put("GeoLat", model.GeoLat);
            cv.put("CustomerTypeId", model.CustomerTypeId);
            cv.put("D1", model.D1);
            cv.put("D2", model.D2);
            cv.put("D3", model.D3);
            cv.put("D4", model.D4);
            cv.put("D5", model.D5);
            cv.put("D6", model.D6);
            cv.put("D7", model.D7);
            cv.put("CustomerCrLimit", model.CustomerCrLimit);
            cv.put("CustomerCrBalance", model.CustomerCrBalance);
            cv.put("isNew", model.isNew);
            cv.put("isEdit", model.isEdit);
            cv.put("isNOO", model.isNOO);
            cv.put("OwnerBirthPlace", model.OwnerBirthPlace);
            cv.put("OwnerBirthDate", model.OwnerBirthDate);
            cv.put("OwnerIdNo", model.OwnerIdNo);
            cv.put("CustomerID", model.CustomerID);

//			cv.put("CustGroupID", model.CustGroupID);
//			cv.put("CustGroupName", model.CustGroupName);

            cv.put("OwnerReligion", model.OwnerReligion);
            cv.put("OwnerHobby", model.OwnerHobby);
            cv.put("CustLevel", model.CustomerLevel);
            cv.put("CustUpID", model.CustomerUpLineID);
            cv.put("CustLegal", model.CustomerLegal);
            cv.put("RandomID", model.RandomID);
            cv.put("Desz", model.Description);

            cv.put("CustomerNpwpName", model.CustomerNpwpName);
            cv.put("CustomerNpwpAddress", model.CustomerNpwpAddress);

            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public void update(CustomerModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustomerAddress", model.CustomerAddress);
            cv.put("CustomerEmail", model.CustomerEmail);
            cv.put("CustomerFax", model.CustomerFax);
            cv.put("CustomerName", model.CustomerName);
            cv.put("CustomerNPWP", model.CustomerNPWP);
            cv.put("CustomerPhone", model.CustomerPhone);
            cv.put("OwnerName", model.OwnerName);
            cv.put("OwnerAddress", model.OwnerAddress);
            cv.put("OwnerEmail", model.OwnerEmail);
            cv.put("OwnerPhone", model.OwnerPhone);
            cv.put("GeoLong", model.GeoLong);
            cv.put("GeoLat", model.GeoLat);
            cv.put("CustomerTypeId", model.CustomerTypeId);
            cv.put("D1", model.D1);
            cv.put("D2", model.D2);
            cv.put("D3", model.D3);
            cv.put("D4", model.D4);
            cv.put("D5", model.D5);
            cv.put("D6", model.D6);
            cv.put("D7", model.D7);
            cv.put("CustomerCrLimit", model.CustomerCrLimit);
            cv.put("CustomerCrBalance", model.CustomerCrBalance);
            cv.put("isNew", model.isNew);
            cv.put("isEdit", model.isEdit);
            cv.put("isNOO", model.isNOO);
            cv.put("OwnerBirthPlace", model.OwnerBirthPlace);
            cv.put("OwnerBirthDate", model.OwnerBirthDate);
            cv.put("OwnerIdNo", model.OwnerIdNo);
            cv.put("CustomerID", model.CustomerID);

            cv.put("OwnerReligion", model.OwnerReligion);
            cv.put("OwnerHobby", model.OwnerHobby);
            cv.put("CustLevel", model.CustomerLevel);
            cv.put("CustUpID", model.CustomerUpLineID);
            cv.put("CustLegal", model.CustomerLegal);
            cv.put("RandomID", model.RandomID);
            cv.put("Desz", model.Description);

            cv.put("CustomerNpwpName", model.CustomerNpwpName);
            cv.put("CustomerNpwpAddress", model.CustomerNpwpAddress);
            if (model.isNOO != null && model.isNOO.equals("1")) {
                cv.put("CustId", model.CustId);
                db.update(table, cv, "RandomID=?", new String[]{model.RandomID});
            } else {
                cv.put("RandomID", model.RandomID);
                db.update(table, cv, "CustId=?", new String[]{model.CustId});
            }
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public CustomerModel get(String CustId) {
        CustomerModel customerModel = null;
        try {
            db = ctrlDb.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + table + " where CustId=? or RandomID=?", new String[]{CustId, CustId});
            if (cursor.moveToNext()) {
                customerModel = new CustomerModel(cursor);
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return customerModel;
    }

    public void delete(CustomerModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            db.delete(table, "CustId=?", new String[]{model.CustId});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CustomerModel> list() {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (ctrlAppModul.isModul("52")) {
//			cursor = db.rawQuery("select * from " + table + " where isNew=?", new String[]{"0"});
            cursor = db.rawQuery("select * from " + table + "  order by isNOO desc", null);
        } else {
//			cursor = db.rawQuery("select * from " + table + " where isNew=? and isNOO=?", new String[]{"0","0"});
            cursor = db.rawQuery("select * from " + table + " where isNOO=? order by isNOO desc", new String[]{"0"});
        }
//		Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CustomerModel> listByName(String name) {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (ctrlAppModul.isModul("52")) {
//			cursor = db.rawQuery("select * from " + table + " where CustomerName like '%"+name+"%' and isNew=?", new String[]{"0"});
            cursor = db.rawQuery("select * from " + table + " where CustomerName like '%" + name + "%' or CustomerID like '%" + name + "%' order by isNOO desc", null);
        } else {
//			cursor = db.rawQuery("select * from " + table + " where CustomerName like '%"+name+"%' and isNew=? and isNOO=?", new String[]{"0", "0"});
            cursor = db.rawQuery("select * from " + table + " where (CustomerName like '%" + name + "%' or CustomerID like '%" + name + "%') and isNOO=? order by isNOO desc", new String[]{"0"});
        }
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CustomerModel> listCustomerLevel(int CustomerLevel) {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (ctrlAppModul.isModul("52")) {
            cursor = db.rawQuery("select * from " + table + " where CustLevel=?", new String[]{String.valueOf((CustomerLevel - 1))});
        } else {
//			cursor = db.rawQuery("select * from " + table + " where isNew=? and isNOO=?", new String[]{"0","0"});
            cursor = db.rawQuery("select * from " + table + " where isNOO=? and CustLevel=?", new String[]{"0", String.valueOf((CustomerLevel - 1))});
        }
//		Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CustomerModel> listByDay(String day) {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (ctrlAppModul.isModul("52")) {
            cursor = db.rawQuery("select * from " + table + " where " + day + "=? and isNew=? order by isNOO desc", new String[]{"true", "0"});
        } else {
            cursor = db.rawQuery("select * from " + table + " where " + day + "=? and isNew=? and isNOO=? order by isNOO desc", new String[]{"true", "0", "0"});
        }
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<CustomerModel> listByDayAndName(String day, String name) {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = null;
        if (ctrlAppModul.isModul("52")) {
            cursor = db.rawQuery("select * from " + table + " where " + day + "=? and isNew=? and CustomerName like '%" + name + "%' or CustomerID like '%" + name + "%' order by isNOO desc", new String[]{"true", "0"});
        } else {
            cursor = db.rawQuery("select * from " + table + " where " + day + "=? and isNew=? and CustomerName like '%" + name + "%' or CustomerID like '%" + name + "%' and isNOO=? order by isNOO desc", new String[]{"true", "0", "0"});
        }
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public CustomerModel getGagalKirimEdit() {
        CustomerModel customerModel = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where isEdit=?", new String[]{"1"});
        if (cursor.moveToNext()) {
            customerModel = new CustomerModel(cursor);
        }
        cursor.close();
        close();
        return customerModel;
    }

    public List<CustomerModel> listCustEdit() {
        List<CustomerModel> list = new ArrayList<CustomerModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where isEdit=?", new String[]{"1"});
        while (cursor.moveToNext()) {
            list.add(new CustomerModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public int count() {
        int count = 0;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) jml from " + table, null);
        if (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        close();
        return count;
    }

    public int countPerhari(int hari) {
        int count = 0;
        try {
            db = ctrlDb.getWritableDatabase();
            String field = "";
//			int hari = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            field = "D" + hari;
            Cursor cursor = db.rawQuery("select count(*) jml from " + table + " where " + field + "=?", new String[]{"true"});
            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }
            cursor.close();
            close();
        } catch (Exception e) {
        }
        return count;
    }

    public int getCountNOO(int day) {
        int count = 0;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) jml from " + table + " where isNOO=? and D" + day + "=?", new String[]{"1", "true"});
        if (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        close();
        return count;
    }

    public void saveCustAddress(CustAdddress model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustID", model.CustID);
            cv.put("AddID", model.AddID);
            cv.put("AddName", model.AddName);
            cv.put("AddType", model.AddType);
            cv.put("AddAddres", model.AddAddres);
            cv.put("AddPhone", model.AddPhone);
            cv.put("AddFax", model.AddFax);
            cv.put("AddEmail", model.AddEmail);
            cv.put("AddRemark", model.AddRemark);
            cv.put("GeoLong", model.GeoLong);
            cv.put("GeoLat", model.GeoLat);
            cv.put("RandomID", model.RandomID);
            cv.put("CustRandomID", model.CustRandomID);

            db.insert("custaddres", null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CustAdddress> listCustAddress(String CustID) {
        List<CustAdddress> list = new ArrayList<CustAdddress>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from custaddres where CustID=?", new String[]{CustID});
        while (cursor.moveToNext()) {
            list.add(new CustAdddress(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void saveCustPIC(CustPIC model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("CustID", model.CustID);
            cv.put("PicID", model.PicID);
            cv.put("PicName", model.PicName);
            cv.put("PicType", model.PicType);
            cv.put("PicIdNo", model.PicIdNo);
            cv.put("PicAddres", model.PicAddres);
            cv.put("PicPhone", model.PicPhone);
            cv.put("PicFax", model.PicFax);
            cv.put("PicEmail", model.PicEmail);
            cv.put("PicRemark", model.PicRemark);
            cv.put("PicBirthPlace", model.PicBirthPlace);
            cv.put("PicBirthDate", model.PicBirthDate);
            cv.put("PicReligion", model.PicReligion);
            cv.put("PicHobby", model.PicHobby);
            cv.put("RandomID", model.RandomID);
            cv.put("CustRandomID", model.CustRandomID);

            db.insert("custpic", null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<CustPIC> listCustPIC(String CustID) {
        List<CustPIC> list = new ArrayList<CustPIC>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from custpic where CustID=?", new String[]{CustID});
        while (cursor.moveToNext()) {
            list.add(new CustPIC(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public int pendingData() {
        int pending = 0;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) jml from " + table + " where isEdit=?", new String[]{"1"});
        if (cursor.moveToNext()) {
            pending = cursor.getInt(0);
        }
        cursor.close();
        close();
        return pending;
    }

}
