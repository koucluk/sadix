package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlAppModul {
    private DatabaseHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "AppModul";

    public CtrlAppModul(Context context) {
        this.context = context;
        ctrlDb = new DatabaseHelper(context);
    }

    public void save(AppModul model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("AppModulId", model.AppModulId);
            cv.put("AppModulValue", model.AppModulValue);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public AppModul get(String AppModulId) {
        AppModul modul = null;
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where AppModulId=?", new String[]{AppModulId});
        if (cursor.moveToNext()) {
            modul = new AppModul(cursor);
        }
        cursor.close();
        close();
        return modul;
    }

    public boolean isModul(String AppModulId) {
        AppModul appModul = get(AppModulId);
        if (appModul != null) {
            return (appModul.AppModulValue != null && appModul.AppModulValue.equals("1"));
        } else {
            return false;
        }
    }

    public List<AppModul> list() {
        List<AppModul> list = new ArrayList<AppModul>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new AppModul(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public int getInt(String AppModulId) {
        AppModul appModul = get(AppModulId);
        if (appModul != null) {
            return Integer.parseInt(appModul.AppModulValue);
        } else {
            return 0;
        }
    }

}
