package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.ConfigModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.List;

public class CtrlConfig {
    private DbMasterHelper ctrlDb;
    private SQLiteDatabase db;
    private Context context;
    private final String table = "config";

    public CtrlConfig(Context context) {
        this.context = context;
        ctrlDb = new DbMasterHelper(context);
    }

    public void save(ConfigModel model) {
        try {
            db = ctrlDb.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("ConfigId", model.ConfigId);
            cv.put("ConfigName", model.ConfigName);
            cv.put("ConfigClass", model.ConfigClass);
            db.insert(table, null, cv);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<ConfigModel> list() {
        List<ConfigModel> list = new ArrayList<ConfigModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table, null);
        while (cursor.moveToNext()) {
            list.add(new ConfigModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public List<ComboBoxModel> listComboBox(String ConfigClass) {
        List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select ConfigId,ConfigName from " + table + " where ConfigClass=?", new String[]{ConfigClass});
        while (cursor.moveToNext()) {
            list.add(new ComboBoxModel(cursor));
        }
        cursor.close();
        close();
        return list;
    }

    public ComboBoxModel getByConfigClassAndConfigId(String ConfigClass, String ConfigId) {
        ComboBoxModel model = new ComboBoxModel();
        db = ctrlDb.getWritableDatabase();
        Cursor cursor = db.rawQuery("select ConfigId,ConfigName from " + table + " where ConfigClass=? and ConfigId=?", new String[]{ConfigClass, ConfigId});
        if (cursor.moveToNext()) {
            model = new ComboBoxModel(cursor);
        }
        cursor.close();
        close();
        return model;
    }

    public void close() {
        if (db != null) db.close();
    }

    public void deleteAll() {
        try {
            db = ctrlDb.getWritableDatabase();
            db.execSQL("delete from " + table);
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

}
