package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.StockGroupModel;
import com.bhn.sadix.util.Util;

public class CtrlStockGroup {
	private DatabaseHelper ctrlDb;
	private SQLiteDatabase db;
	private Context context;
	private final String table = "GroupStock";
	public CtrlStockGroup(Context context) {
		this.context = context;
		ctrlDb = new DatabaseHelper(context);
	}
	public void save(StockGroupModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerId", model.CustomerId);
			cv.put("GroupId", model.GroupId);
			cv.put("Quantity", model.Quantity);
			cv.put("Quantity2", model.Quantity2);
			cv.put("CommonID", model.CommonID);
			cv.put("RandomID", model.RandomID);
			db.insert(table, null, cv);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public void update(StockGroupModel model) {
		try {
			db = ctrlDb.getWritableDatabase();
			ContentValues cv=new ContentValues();
			cv.put("CustomerId", model.CustomerId);
			cv.put("GroupId", model.GroupId);
			cv.put("Quantity", model.Quantity);
			cv.put("Quantity2", model.Quantity2);
			db.update(table, cv, "RandomID=?", new String []{model.RandomID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
	}
	public StockGroupModel getStockGroupModel(String CommonID, String SKUId) {
		StockGroupModel model = null;
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=? and GroupId=?", new String[]{CommonID,SKUId});
		while(cursor.moveToNext()) {
			model = new StockGroupModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public List<StockGroupModel> getStockGroupModel(String CommonID) {
		List<StockGroupModel> list = new ArrayList<StockGroupModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CommonID=?", new String[]{CommonID});
		while(cursor.moveToNext()) {
			list.add(new StockGroupModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public List<StockGroupModel> list() {
		List<StockGroupModel> list = new ArrayList<StockGroupModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new StockGroupModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}
    public void deleteByCommonID(String CommonID) {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table + " where CommonID=?", new String[]{CommonID});
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }
    public void deleteAll() {
		try {
			db=ctrlDb.getWritableDatabase();
	        db.execSQL("delete from " + table);
			close();
		} catch (Exception e) {
			Util.showDialogError(context, e.getMessage());
		}
    }

}
