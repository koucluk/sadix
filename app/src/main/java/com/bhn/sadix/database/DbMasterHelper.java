package com.bhn.sadix.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DbMasterHelper extends SQLiteOpenHelper {

    private final String PATH_MASTER_DATA = Util.LOKASI + Util.DBMASTER;
    public static String DB_PATH;
    public static String DB_NAME = Util.DBMASTER;
    public SQLiteDatabase database;
    public final Context context;

    public SQLiteDatabase getDb() {
        return database;
    }

    public DbMasterHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        String packageName = context.getPackageName();
        DB_PATH = String.format("//data//data//%s//databases//", packageName);
    }

    public boolean isFileMasterData() {
        File file = new File(PATH_MASTER_DATA);
        boolean cek = file.exists();
        file = null;
        return cek;
    }

    public void deleteMasterData() {
        File file = new File(PATH_MASTER_DATA);
        if (file.exists()) {
            file.delete();
        }
        file = null;
    }

    public void createDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            Log.i(this.getClass().toString(), "Database already exists");
        }
    }

    public void importDataBase() {
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
                deleteMasterData();
            } catch (IOException e) {
                Log.e(this.getClass().toString(), "Copying error");
//				throw new Error("Error copying database!");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDb = null;
        try {
            String path = DB_PATH + DB_NAME;
            checkDb = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {
            Log.e(this.getClass().toString(), "Error while checking db");
        }
        if (checkDb != null) {
            checkDb.close();
        }
        return checkDb != null;
    }

    private void copyDataBase() throws IOException {
        InputStream externalDbStream = new FileInputStream(PATH_MASTER_DATA);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream localDbStream = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = externalDbStream.read(buffer)) > 0) {
            localDbStream.write(buffer, 0, bytesRead);
        }
        localDbStream.close();
        externalDbStream.close();
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String path = DB_PATH + DB_NAME;
        if (database == null) {
            createDataBase();
            database = SQLiteDatabase.openDatabase(path, null,
                    SQLiteDatabase.OPEN_READWRITE);
        }
        return database;
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
            database = null;
        }
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public Cursor query(String sql) {
        Cursor cursor = null;
        cursor = openDataBase().rawQuery(sql, null);
        return cursor;
    }

    public boolean save(JSONObject json, String table) {
        boolean flag = false;
        try {
            ContentValues cv = new ContentValues();
            Iterator<String> it = json.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                cv.put(key, json.getString(key));
            }
            openDataBase().insert(table, null, cv);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean isProducttarget(JSONObject json) {
        boolean flag = false;
        try {
            Cursor cursor = openDataBase().rawQuery("select count(*) jml from producttarget where SkuID=? and Month=? and Year=? and CustID=?",
                    new String[]{json.getString("SkuID"), json.getString("Month"), json.getString("Year"), json.getString("CustID")});
            if (cursor.moveToNext()) {
                int jml = cursor.getInt(0);
                if (jml > 0) {
                    flag = true;
                }
            }
            cursor.close();
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public void updateProducttarget(JSONObject json) {
        try {
            ContentValues cv = new ContentValues();
            cv.put("Qty", json.getInt("Qty"));
            cv.put("Het", json.getDouble("Het"));
            openDataBase().update("producttarget", cv, "SkuID=? and Month=? and Year=? and CustID=?",
                    new String[]{json.getString("SkuID"), json.getString("Month"), json.getString("Year"), json.getString("CustID")});
            close();
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    public List<ComboBoxModel> listCombo(String sql) {
        List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        try {
            Cursor cursor = openDataBase().rawQuery(sql, null);
            while (cursor.moveToNext()) {
                list.add(new ComboBoxModel(cursor));
            }
            cursor.close();
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public JSONObject queryJSON(String sql) {
        JSONObject json = null;
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (db != null) {
            db.close();
        }
        return json;
    }

    public List<JSONObject> queryListJSON(String sql) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        SQLiteDatabase db = getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                JSONObject json = new JSONObject();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if (cursor.getType(i) == Cursor.FIELD_TYPE_BLOB) {
                        json.put(cursor.getColumnName(i), cursor.getBlob(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_FLOAT) {
                        json.put(cursor.getColumnName(i), cursor.getFloat(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_INTEGER) {
                        json.put(cursor.getColumnName(i), cursor.getInt(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_STRING) {
                        json.put(cursor.getColumnName(i), cursor.getString(i));
                    } else if (cursor.getType(i) == Cursor.FIELD_TYPE_NULL) {
                        json.put(cursor.getColumnName(i), "");
                    }
                }
                list.add(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
