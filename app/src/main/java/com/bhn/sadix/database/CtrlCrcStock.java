package com.bhn.sadix.database;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bhn.sadix.model.CrcStockModel;

public class CtrlCrcStock {
	private DbMasterHelper ctrlDb;
	private SQLiteDatabase db;
	private final String table = "crcstock";
	public CtrlCrcStock(Context context) {
		ctrlDb = new DbMasterHelper(context);
	}
	public List<CrcStockModel> list() {
		List<CrcStockModel> list = new ArrayList<CrcStockModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table, null);
		while(cursor.moveToNext()) {
			list.add(new CrcStockModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public CrcStockModel get(int CustID, int SkuID) {
		CrcStockModel model = new CrcStockModel();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustID=? and SkuID=?", new String[] {String.valueOf(CustID),String.valueOf(SkuID)});
		if(cursor.moveToNext()) {
			model = new CrcStockModel(cursor);
		}
		cursor.close();
		close();
		return model;
	}
	public List<CrcStockModel> list(String CustID, String SkuID) {
		List<CrcStockModel> list = new ArrayList<CrcStockModel>();
		db = ctrlDb.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + table + " where CustID=? and SkuID=?", new String[] {CustID,SkuID});
		while(cursor.moveToNext()) {
			list.add(new CrcStockModel(cursor));
		}
		cursor.close();
		close();
		return list;
	}
	public void close() {
		if(db != null) db.close();
	}

}
