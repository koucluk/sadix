package com.bhn.sadix;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bhn.sadix.adapter.AssetServiceAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.model.AssetServiceModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetDetailWidget;
import com.bhn.sadix.widget.AssetServiceWidget.AssetServiceWidgetListener;
import com.bhn.sadix.widget.AssetType1Widget;
import com.bhn.sadix.widget.AssetType2Widget;
import com.bhn.sadix.widget.AssetType3Widget;
import com.bhn.sadix.widget.AssetType4Widget;
import com.bhn.sadix.widget.AssetType5Widget;
import com.bhn.sadix.widget.AssetType6Widget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServiceAssetActivity extends AppCompatActivity implements OnItemClickListener, AssetServiceWidgetListener, ConnectionEvent {
    private AssetServiceAdapter adapter;
    private AssetType4Widget type4;
    private AssetType6Widget type6;
    private CtrlAppModul ctrlAppModul;
    private ConnectionServer server;
    private CustomerModel customerModel;
    private int jenis;
    private CtrlCustomerType ctrlCustomerType;
    private String BeginAsetTime;

    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlAppModul = new CtrlAppModul(this);
        ctrlCustomerType = new CtrlCustomerType(this);
        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        CustomerTypeModel customerTypeModel = ctrlCustomerType.get(customerModel.CustomerTypeId);
        setContentView(R.layout.activity_service_asset);
        adapter = new AssetServiceAdapter(this, new ArrayList<AssetServiceModel>());
        adapter.setNotifyOnChange(true);
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + customerModel.CustomerCrLimit + " | Sisa Credit Limit : " + customerModel.CustomerCrBalance);
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }
        BeginAsetTime = Util.dateFormat.format(new Date());
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });
    }

    private void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "ADD")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "SEND")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "ADD");
            menu.add(0, 2, 2, "SEND");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            add();
        } else if (item.getItemId() == 2) {
            sendAsset();
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendAsset() {
        try {
            jenis = 2;
            JSONObject jsonSend = new JSONObject();
            JSONArray Asset = new JSONArray();
            JSONArray Newasset = new JSONArray();
            for (int i = 0; i < adapter.getCount(); i++) {
                AssetServiceModel assetServiceModel = adapter.getItem(i);
                List<AssetDetailWidget> list = assetServiceModel.getAssetWidgetService().getListWidget();
                for (AssetDetailWidget assetDetailWidget : list) {
                    if (assetDetailWidget.isType5()) {
                        RadioGroup rg = (RadioGroup) assetDetailWidget.findViewById(R.id.radioGroup);
                        for (int k = 0; k < rg.getChildCount(); k++) {
                            View view = rg.getChildAt(k);
                            if (view instanceof AssetType5Widget) {
                                AssetType5Widget asset = (AssetType5Widget) view;
                                Asset.put(asset.toJSON());
                            }
                        }
                    } else {
                        LinearLayout layout = (LinearLayout) assetDetailWidget.findViewById(R.id.layout_detail);
                        /*for (int k = 0; k < layout.getChildCount(); k++) {
                            View view = layout.getChildAt(k);
                            if (view instanceof AssetType1Widget) {
                                AssetType1Widget asset = (AssetType1Widget) view;
                                Asset.put(asset.toJSON());
                            } else if (view instanceof AssetType2Widget) {
                                AssetType2Widget asset = (AssetType2Widget) view;
                                Asset.put(asset.toJSON());
                            } else if (view instanceof AssetType3Widget) {
                                AssetType3Widget asset = (AssetType3Widget) view;
                                Asset.put(asset.toJSON());
                            } else if (view instanceof AssetType4Widget) {
                                AssetType4Widget asset = (AssetType4Widget) view;
                                Asset.put(asset.toJSON());
                            } else if (view instanceof AssetType5Widget) {
                                AssetType5Widget asset = (AssetType5Widget) view;
                                Asset.put(asset.toJSON());
                            }
                        }*/
                    }
                }
            }
            String EndAsetTime = Util.dateFormat.format(new Date());
            JSONObject Common = new JSONObject();
            Common.put("BeginAsetTime", BeginAsetTime);
            Common.put("EndAsetTime", EndAsetTime);
            Common.put("CustomerId", Integer.parseInt(customerModel.CustId));
            Common.put("SalesId", Integer.parseInt(Util.getStringPreference(this, "SalesId")));
            if (location != null) {
                Common.put("GeoLat", location.getLatitude());
                Common.put("GeoLong", location.getLongitude());
            } else {
                Common.put("GeoLat", 0);
                Common.put("GeoLong", 0);
            }

            jsonSend.put("Common", Common);
            jsonSend.put("Asset", Asset);
            jsonSend.put("Newasset", Newasset);
            server.setUrl(Util.getServerUrl(this) + "endaset");
            server.requestJSONObject(jsonSend);
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                String AssetNumber = data.getStringExtra("SCAN_RESULT");
                searchAsset(AssetNumber);
            }
        } else if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    type4.setBitmap(bitmap, bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(this, e.getMessage());
                }
            }
        } else if (requestCode == 6) {
            String kode = data.getStringExtra("SCAN_RESULT");
//	        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            type6.setScanText(kode);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void add() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_asset);
        dialog.setTitle("ADD ASSET");
        ((Button) dialog.findViewById(R.id.btn_scanbarcode)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                try {
                    startActivityForResult(intent, 4);
                } catch (Exception e) {
                    Util.showDialogInfo(ServiceAssetActivity.this, "ZXing Scanner Not Found, Please Install First!");
                }
            }
        });
        ((Button) dialog.findViewById(R.id.btn_input)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                inputAssetNumber();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void inputAssetNumber() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_input_asset);
        dialog.setTitle("INPUT ASSET NUMBER");
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String AssetNumber = ((EditText) dialog.findViewById(R.id.AssetNumber)).getText().toString();
                dialog.dismiss();
                searchAsset(AssetNumber);
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void searchAsset(String assetNumber) {
        try {
            int index = adapter.indexOf(new AssetServiceModel(assetNumber));
            if (index == -1) {
                requestAsset(assetNumber);
            } else {
                adapter.getItem(index).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestAsset(String assetNumber) {
        jenis = 1;
        server.setUrl(Util.getServerUrl(this) + "getasset/AssetNo/" + assetNumber + "/SalesId/" + Util.getStringPreference(this, "SalesId"));
        server.requestJSONObject(null);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        adapter.getItem(position).show();
    }

    @Override
    public void photoType4(AssetType4Widget type4) {
        this.type4 = type4;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 5);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (jenis == 1) {
                if (json.getString("AsetName").equals("Error")) {
                    Util.showDialogInfo(this, "Nomor Asset tidak ditemukan!");
                } else {
                    AssetServiceModel assetModel = new AssetServiceModel(this,
                            json.getString("AsetName"),
                            json.getString("TypeID"),
                            json.getString("AsetNO"),
                            json.getString("AsetIDX"),
                            this,
                            customerModel.CustId,
                            "X"
                    );
                    adapter.add(assetModel);
                    assetModel.show();
                }
            } else if (jenis == 2) {

            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void scanType6(AssetType6Widget type6) {
        this.type6 = type6;
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, 6);
    }

}
