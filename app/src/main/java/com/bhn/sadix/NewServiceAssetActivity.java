package com.bhn.sadix;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetDetailWidget;
import com.bhn.sadix.widget.AssetServiceWidget;
import com.bhn.sadix.widget.AssetServiceWidget.AssetServiceWidgetListener;
import com.bhn.sadix.widget.AssetType4Widget;
import com.bhn.sadix.widget.AssetType5Widget;
import com.bhn.sadix.widget.AssetType6Widget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

public class NewServiceAssetActivity extends AppCompatActivity implements AssetServiceWidgetListener, ConnectionEvent {
    private AssetType4Widget type4;
    private AssetType6Widget type6;
    private ConnectionServer server;
    private String BeginAsetTime;
    private LocationManager locationManager;
    private Location location;
    private CtrlAppModul ctrlAppModul;
    private AssetServiceWidget assetServiceWidget;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlAppModul = new CtrlAppModul(this);
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        assetServiceWidget = new AssetServiceWidget(this,
                getIntent().getStringExtra("ASETNAME"),
                getIntent().getStringExtra("ASETTYPE"),
                getIntent().getStringExtra("ASETID"),
                this,
                "0",
                getIntent().getStringExtra("ASETNO"),
                "XY"
        );
        setContentView(assetServiceWidget);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((Button) assetServiceWidget.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((Button) assetServiceWidget.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAsset();
            }
        });
        BeginAsetTime = Util.dateFormat.format(new Date());
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            close();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        close();
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan servis asset?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    finish();
                }
            }
        });
    }

    private void sendAsset() {
        try {
            JSONObject jsonSend = new JSONObject();
            JSONArray Asset = new JSONArray();
            JSONArray Newasset = new JSONArray();
            List<AssetDetailWidget> list = assetServiceWidget.getListWidget();
            for (AssetDetailWidget assetDetailWidget : list) {
                if (assetDetailWidget.isType5()) {
                    RadioGroup rg = (RadioGroup) assetDetailWidget.findViewById(R.id.radioGroup);
                    for (int k = 0; k < rg.getChildCount(); k++) {
                        View view = rg.getChildAt(k);
                        if (view instanceof AssetType5Widget) {
                            AssetType5Widget asset = (AssetType5Widget) view;
                            Asset.put(asset.toJSON());
                        }
                    }
                } else {
                    LinearLayout layout = (LinearLayout) assetDetailWidget.findViewById(R.id.layout_detail);
                    /*for (int k = 0; k < layout.getChildCount(); k++) {
                        View view = layout.getChildAt(k);
						if(view instanceof AssetType1Widget) {
							AssetType1Widget asset = (AssetType1Widget) view;
							Asset.put(asset.toJSON());
						} else if(view instanceof AssetType2Widget) {
							AssetType2Widget asset = (AssetType2Widget) view;
							Asset.put(asset.toJSON());
						} else if(view instanceof AssetType3Widget) {
							AssetType3Widget asset = (AssetType3Widget) view;
							Asset.put(asset.toJSON());
						} else if(view instanceof AssetType4Widget) {
							AssetType4Widget asset = (AssetType4Widget) view;
							Asset.put(asset.toJSON());
						} else if(view instanceof AssetType5Widget) {
							AssetType5Widget asset = (AssetType5Widget) view;
							Asset.put(asset.toJSON());
						}
					}*/
                }
            }
            String EndAsetTime = Util.dateFormat.format(new Date());
            JSONObject Common = new JSONObject();
            Common.put("BeginAsetTime", BeginAsetTime);
            Common.put("EndAsetTime", EndAsetTime);
            Common.put("CustomerId", 0);
            Common.put("SalesId", Integer.parseInt(Util.getStringPreference(this, "SalesId")));
            if (location != null) {
                Common.put("GeoLat", location.getLatitude());
                Common.put("GeoLong", location.getLongitude());
            } else {
                Common.put("GeoLat", 0);
                Common.put("GeoLong", 0);
            }

            jsonSend.put("Common", Common);
            jsonSend.put("Asset", Asset);
            jsonSend.put("Newasset", Newasset);
            server.setUrl(Util.getServerUrl(this) + "endaset");
            server.requestJSONObject(jsonSend);
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    type4.setBitmap(bitmap, bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(this, e.getMessage());
                }
            }
        } else if (requestCode == 6) {
            String kode = data.getStringExtra("SCAN_RESULT");
//	        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            type6.setScanText(kode);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void photoType4(AssetType4Widget type4) {
        this.type4 = type4;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 5);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.getString("Result").equals("INSERT_OK")) {
                Util.Toast(this, json.getString("Message"));
                finish();
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void scanType6(AssetType6Widget type6) {
        this.type6 = type6;
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, 6);
    }

}
