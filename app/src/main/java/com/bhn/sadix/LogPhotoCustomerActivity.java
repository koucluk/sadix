package com.bhn.sadix;

import java.util.List;

import org.json.JSONArray;

import com.bhn.sadix.adapter.CustPhotoAdapter;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.model.CustPhotoModel;
import com.bhn.sadix.util.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LogPhotoCustomerActivity extends AppCompatActivity implements OnItemClickListener {
    private CtrlCustPhoto ctrlCustPhoto;
    private CustPhotoAdapter adapter;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctrlCustPhoto = new CtrlCustPhoto(this);
        adapter = new CustPhotoAdapter(this, ctrlCustPhoto.list());
        adapter.setNotifyOnChange(true);
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Export")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Export");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            new ExportData(this).execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CustPhotoModel model = adapter.getItem(position);
        new LoadData(this, model).execute();
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        private CustPhotoModel model;
        private ProgressDialog progressDialog;
        private Context context;

        public LoadData(Context context, CustPhotoModel model) {
            this.model = model;
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            return Util.sendUlangCustPhoto(context, model);
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new CustPhotoAdapter(LogPhotoCustomerActivity.this, ctrlCustPhoto.list());
            adapter.setNotifyOnChange(true);
            ((ListView) findViewById(R.id.list)).setAdapter(adapter);
            progressDialog.dismiss();
            if (result == null) {
                Util.showDialogInfo(context, "Data berhasil dikirim");
            } else {
                Util.showDialogError(context, result);
            }
            super.onPostExecute(result);
        }
    }

    private class ExportData extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        private Context context;

        public ExportData(Context context) {
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Export Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                List<CustPhotoModel> list = ctrlCustPhoto.list();
                JSONArray array = new JSONArray();
                for (CustPhotoModel custPhotoModel : list) {
                    array.put(Util.convertJSONCustPhoto(context, custPhotoModel));
                }
                Util.saveFile(array.toString().getBytes(), "ExpoprtPhotoCustomer.txt");
            } catch (Exception e) {
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogError(LogPhotoCustomerActivity.this, result);
            } else {
                Util.showDialogInfo(LogPhotoCustomerActivity.this, "Export data berhasil!");
            }
            super.onPostExecute(result);
        }
    }
}
