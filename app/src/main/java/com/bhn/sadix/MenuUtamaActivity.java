package com.bhn.sadix;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhn.sadix.adapter.MenuHomeAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlGroupSKUCom;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.database.DbDiskonHelper;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.database.DbSkuHelper;
import com.bhn.sadix.dropper.CustomFormatter;
import com.bhn.sadix.dropper.DropperScheduleActivity;
import com.bhn.sadix.dropper.database.CtrlCustomerDropper;
import com.bhn.sadix.dropper.database.CtrlDeliveryDropper;
import com.bhn.sadix.dropper.database.CtrlDropperWH;
import com.bhn.sadix.dropper.database.CtrlReturnDropper;
import com.bhn.sadix.dropper.model.CustomerDropperModel;
import com.bhn.sadix.dropper.model.DeliveryDropperModel;
import com.bhn.sadix.dropper.model.DropperWHModel;
import com.bhn.sadix.dropper.model.ReturnDropperModel;
import com.bhn.sadix.model.Chart;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.DataChart;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetType4Widget;
import com.bhn.sadix.widget.XValueFormatter;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

//import android.support.v4.app.ActionBarDrawerToggle;
//import android.support.v4.widget.DrawerLayout;
//import com.bhn.sadix.util.BarChart;
//import com.bhn.sadix.util.PieChart;

public class MenuUtamaActivity extends AppCompatActivity implements OnItemClickListener, ConnectionEvent {

    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuHomeAdapter adapter;
    private ProgressDialog progressDialog;
    private ConnectionServer server;

    private CtrlCustomerType ctrlCustomerType;
    private CtrlCustomer ctrlCustomer;
    private CtrlGroupSKUCom ctrlGroupSKUCom;
    private CtrlSKUCom ctrlSKUCom;
    private CtrlAppModul ctrlAppModul;

    //ctrl dropper
    private CtrlCustomerDropper ctrlCustomerDropper;
    private CtrlDropperWH ctrlDropperWH;
    private CtrlDeliveryDropper ctrlDeliveryDropper;
    private CtrlReturnDropper ctrlReturnDropper;
    private CtrlCommon ctrlCommon;

    private DbSkuHelper dbSkuHelper;
    private DbMasterHelper dbMasterHelper;
    private DbAsetHelper dbAsetHelper;
    private DbDiskonHelper dbDiskonHelper;

    private int jenis = 0;
    private AssetType4Widget type4;
    Toolbar toolbar;

    public static boolean isActive = false;

//	private LocationManager locationManager;
//    private LocationListener locationListener = new LocationListener() {
//        public void onLocationChanged(Location loc) {
////        	menuFragment.setPosisi(loc);
//        }
//        public void onStatusChanged(String provider, int status, Bundle extras) {}
//        public void onProviderEnabled(String provider) {}
//        public void onProviderDisabled(String provider) {}
//    };
//    private MenuFragment menuFragment = new MenuFragment();

//	private LinearLayout m_LinLayout;
//    private ScrollView m_Scroll;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

//	    JSONObject json = Util.convertJSONEndVisit(this, null);
//	    if(json != null) android.util.Log.e("json", json.toString());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            /*toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                        mDrawerLayout.closeDrawer(mDrawerList);
                    } else {
                        mDrawerLayout.openDrawer(mDrawerList);
                    }
                }
            });*/
            getSupportActionBar().setTitle("Sadix-" + versionName);
        } catch (NameNotFoundException e) {
            Util.Toast(this, e.getMessage());
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ctrlCustomerType = new CtrlCustomerType(this);
        ctrlCustomer = new CtrlCustomer(this);

        Realm realmUI = Realm.getDefaultInstance();

        com.bhn.sadix.Data.CommonModel model = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        if (model != null) {
            startActivity(new Intent(this, MenuUtamaActivity.class));
            Log.d("Common ID", model.CommonID);
            Intent intent = new Intent(this, KunjunganActivity.class);
            CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
            Log.d("Ini last CID ", customerModel.CustId);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
            intent.putExtra("isLoad", true);
            startActivity(intent);

            finish();
        } else {
            Log.d("NO AVAIL COMMON", "YES");
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        adapter = new MenuHomeAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);

//	    ((ListView)findViewById(R.id.list)).setAdapter(adapter);
//	    ((ListView)findViewById(R.id.list)).setOnItemClickListener(this);

        ctrlAppModul = new CtrlAppModul(this);
        if (ctrlAppModul.isModul("16"))
            adapter.add(new MenuUtamaModel(1, "Informasi", getResources().getDrawable(R.drawable.information)));
        adapter.add(new MenuUtamaModel(0, "Activity", null));
        if (ctrlAppModul.isModul("1"))
            adapter.add(new MenuUtamaModel(2, "All Customer", getResources().getDrawable(R.drawable.all_customer)));
        if (ctrlAppModul.isModul("2"))
            adapter.add(new MenuUtamaModel(3, "Schedule", getResources().getDrawable(R.drawable.schedule)));
        if (ctrlAppModul.isModul("6"))
            adapter.add(new MenuUtamaModel(7, "Stock Van", getResources().getDrawable(R.drawable.stock_on_car)));
        if (ctrlAppModul.isModul("3"))
            adapter.add(new MenuUtamaModel(4, "New Customer", getResources().getDrawable(R.drawable.new_customer)));
        if (ctrlAppModul.isModul("30"))
            adapter.add(new MenuUtamaModel(11, "Scan QR Code", getResources().getDrawable(R.drawable.scan_qr_code)));
        if (ctrlAppModul.isModul("31"))
            adapter.add(new MenuUtamaModel(12, "Dropper", getResources().getDrawable(R.drawable.scan_qr_code)));
        if (ctrlAppModul.isModul("39"))
            adapter.add(new MenuUtamaModel(14, "Asset Service", getResources().getDrawable(R.drawable.scan_qr_code)));
        if (ctrlAppModul.isModul("40"))
            adapter.add(new MenuUtamaModel(15, "Order", getResources().getDrawable(R.drawable.scan_qr_code)));
        if (ctrlAppModul.isModul("46"))
            adapter.add(new MenuUtamaModel(17, "Sales Note", getResources().getDrawable(R.drawable.scan_qr_code)));

        adapter.add(new MenuUtamaModel(0, "Summary", null));
        if (ctrlAppModul.isModul("4"))
            adapter.add(new MenuUtamaModel(5, "Summary Order", getResources().getDrawable(R.drawable.summary_order)));
        if (ctrlAppModul.isModul("5"))
            adapter.add(new MenuUtamaModel(6, "Summary Return", getResources().getDrawable(R.drawable.summary_return)));
        if (ctrlAppModul.isModul("29"))
            adapter.add(new MenuUtamaModel(10, "Summary Visit", getResources().getDrawable(R.drawable.visit)));
        adapter.add(new MenuUtamaModel(0, "Log & Logout", null));
        if (ctrlAppModul.isModul("75"))
            adapter.add(new MenuUtamaModel(13, "Refresh Data", getResources().getDrawable(R.drawable.log_data)));
        if (ctrlAppModul.isModul("7"))
            adapter.add(new MenuUtamaModel(9, "Log Data", getResources().getDrawable(R.drawable.log_data)));
        if (ctrlAppModul.isModul("44"))
            adapter.add(new MenuUtamaModel(16, "End Canvas", getResources().getDrawable(R.drawable.logout)));
        if (ctrlAppModul.isModul("76"))
            adapter.add(new MenuUtamaModel(100, "Price List", null));
        if (ctrlAppModul.isModul("8"))
            adapter.add(new MenuUtamaModel(8, "Logout", getResources().getDrawable(R.drawable.logout)));

        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(this);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

//        ctrlInformasi = new CtrlInformasi(this);
//        ctrlCustomerType = new CtrlCustomerType(this);
//        ctrlCustomer = new CtrlCustomer(this);
//        ctrlGroupSKU = new CtrlGroupSKU(this);
//        ctrlSalesProgram = new CtrlSalesProgram(this);
//        ctrlSKU = new CtrlSKU(this);
//        ctrlConfig = new CtrlConfig(this);
        ctrlGroupSKUCom = new CtrlGroupSKUCom(this);
        ctrlSKUCom = new CtrlSKUCom(this);
//        ctrlFormSurvey = new CtrlFormSurvey(this);
//        ctrlFormCustType = new CtrlFormCustType(this);
//        ctrlSurvey = new CtrlSurvey(this);
//        ctrlSurveyOpt = new CtrlSurveyOpt(this);
        ctrlCommon = new CtrlCommon(this);
        dbSkuHelper = new DbSkuHelper(this);
        dbMasterHelper = new DbMasterHelper(this);
        dbAsetHelper = new DbAsetHelper(this);
        dbDiskonHelper = new DbDiskonHelper(this);

        //new ctrl dropper
        ctrlCustomerDropper = new CtrlCustomerDropper(this);
        ctrlDropperWH = new CtrlDropperWH(this);
        ctrlDeliveryDropper = new CtrlDeliveryDropper(this);
        ctrlReturnDropper = new CtrlReturnDropper(this);

        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
//				if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
//					dismiss();
//				}
                return super.onKeyDown(keyCode, event);
            }
        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");

        Log.e("DbSku", Util.getStringPreference(this, "DbSku"));
        Log.e("DbCustomer", Util.getStringPreference(this, "DbCustomer"));
        Log.e("DbAset", Util.getStringPreference(this, "DbAset"));
        Log.e("DbDiskon", Util.getStringPreference(this, "DbDiskon"));

//		Log.e("isLoadData", "" + getIntent().getBooleanExtra("isLoadData", false));
        if (Util.getBooleanPreference(this, "isLoadData")/*getIntent().getBooleanExtra("isLoadData", false)*/) {
            Log.e("isLoadData", "kesini");
            progressDialog.show();
            new LoadData().execute();
        }

//		getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, menuFragment);

        if (Util.isRunService(this) == false) {
            startService(new Intent(this, SadixService.class));
        }

        ((TextView) findViewById(R.id.selamat_datang)).setText("Selamat Datang " + UserModel.getInstance(this).FullName);
        if (ctrlAppModul.isModul("32")) {
            loadGraph();
        } else {
            findViewById(R.id.pie_chart).setVisibility(View.GONE);
            findViewById(R.id.update_data).setVisibility(View.GONE);
        }

//		BarChart barChart = new BarChart(this, "TesA", "TesX", "TesY", "Tes Aja");
//		barChart.addItem("Tes1", 0, 1);
//		barChart.addItem("Tes2", 1, 10);
//		barChart.addItem("Tes3", 2, 20);
//		barChart.repaint();
//		((LinearLayout)findViewById(R.id.pie_chart)).addView(barChart.getView());
    }

    public void handleGoogleServiceResult() {

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (ctrlAppModul.isModul("32")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                menu.add(0, 1, 1, "REFRESH")
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            } else {
                menu.add(0, 1, 1, "REFRESH");
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        } else */
        if (item.getItemId() == 1) {
            loadChart();
        }
        return true;
    }

    private void loadChart() {
        try {
            jenis = 1;
            server.setUrl(Util.getServerUrl(MenuUtamaActivity.this) + "getgraph/SalesId/" + UserModel.getInstance(MenuUtamaActivity.this).SalesId);
            server.requestJSONObject(null);
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        Intent intent = null;
        if (model.id == 2) {
            intent = new Intent(this, AllCustomerActivity.class);
            intent.putExtra("jenis", 1);
        } else if (model.id == 4) {
//			intent = new Intent(this, NewCuctomerActivity.class);
            intent = new Intent(this, NewCustomerNOOActivity.class);
        } else if (model.id == 3) {
            intent = new Intent(this, ScheduleActivity.class);
        } else if (model.id == 1) {
            intent = new Intent(this, InformationActivity.class);
        } else if (model.id == 8) {
//			finish();
            logout();
        } else if (model.id == 7) {
            intent = new Intent(this, StockOnCarActivity.class);
        } else if (model.id == 5) {
            intent = new Intent(this, ResumeOrderActivity.class);
        } else if (model.id == 6) {
            intent = new Intent(this, SummeryReturnActivity.class);
        } else if (model.id == 9) {
            intent = new Intent(this, LogMenuActivity.class);
        } else if (model.id == 10) {
            intent = new Intent(this, SummeryVisitActivity.class);
        } else if (model.id == 11) {
            scanBarcode();
        } else if (model.id == 12) {
            intent = new Intent(this, DropperScheduleActivity.class);
        } else if (model.id == 13) {
            login();
        } else if (model.id == 14) {
//			intent = new Intent(this, AllCustomerActivity.class);
//			intent.putExtra("jenis", 2);
            addNewAsset();
        } else if (model.id == 15) {
            intent = new Intent(this, AllCustomerActivity.class);
            intent.putExtra("jenis", 3);
        } else if (model.id == 16) {
            intent = new Intent(this, EndCanvasActivity.class);
        } else if (model.id == 17) {
            intent = new Intent(this, SalesNoteActivity.class);
        } else if (model.id == 100) {
            intent = new Intent(this, PriceListActivity.class);
        }

        if (intent != null) {
            startActivity(intent);
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    private void login() {
        jenis = 2;
        JSONObject json = new JSONObject();
        try {
            android.util.Log.e("IMEI", Util.getIMEI(this));
            json.put("IMEI", Util.getIMEI(this));
            json.put("UserName", Util.getStringPreference(this, "UserName"));
            json.put("Password", Util.getStringPreference(this, "Password"));
        } catch (Exception e) {
            error(e.getMessage());
        }
        server.setUrl("http://sdx.x-locate.com/SadixApi2/userlogin");
        server.requestJSONObject(json);
    }

    private void loadLogin() {
        jenis = 2;
        JSONObject json = new JSONObject();
        try {
            json.put("IMEI", Util.getIMEI(this));
            json.put("UserName", Util.getStringPreference(this, "UserName"));
            json.put("Password", Util.getStringPreference(this, "Password"));
        } catch (Exception e) {
            error(e.getMessage());
        }
        server.setUrl(Util.getServerUrl(this) + "userlogin");
        server.requestJSONObjectNoTimeOut(json);
    }

    private void logout() {
        Util.confirmDialog(this, "Info", "Apakah anda ingin logout?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    Util.putPreference(MenuUtamaActivity.this, "PathSvr", null);
                    Util.putPreference(MenuUtamaActivity.this, "isLogin", false);
//					stopService(new Intent(MenuUtamaActivity.this, SadixService.class));
                    startActivity(new Intent(MenuUtamaActivity.this, LoginActivity.class));
                    finish();
                }
            }
        });
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                String DbSku = Util.getStringPreference(MenuUtamaActivity.this, "DbSku");
                if (DbSku != null) {
                    URL url = new URL(DbSku);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    c.connect();
                    File file = new File(Util.LOKASI);
                    file.mkdirs();
                    File outputFile = new File(file, Util.DBSKU);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(outputFile, true);
                    InputStream is = c.getInputStream();
                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);
                    }
                    fos.close();
                    is.close();
                    dbSkuHelper.importDataBase();
                }

                String DbCustomer = Util.getStringPreference(MenuUtamaActivity.this, "DbCustomer");
                if (DbCustomer != null) {
                    URL url = new URL(DbCustomer);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    c.connect();
                    File file = new File(Util.LOKASI);
                    file.mkdirs();
                    File outputFile = new File(file, Util.DBMASTER);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(outputFile, true);
                    InputStream is = c.getInputStream();
                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);
                    }
                    fos.close();
                    is.close();
                    dbMasterHelper.importDataBase();
                }

                if (ctrlAppModul.isModul("64")) {
                    String DbAset = Util.getStringPreference(MenuUtamaActivity.this, "DbAset");
                    if (DbAset != null) {
                        URL url = new URL(DbAset);
                        HttpURLConnection c = (HttpURLConnection) url.openConnection();
                        c.connect();
                        File file = new File(Util.LOKASI);
                        file.mkdirs();
                        File outputFile = new File(file, Util.DBASSET);
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }
                        FileOutputStream fos = new FileOutputStream(outputFile, true);
                        InputStream is = c.getInputStream();
                        byte[] buffer = new byte[1024];
                        int len1 = 0;
                        while ((len1 = is.read(buffer)) != -1) {
                            fos.write(buffer, 0, len1);
                        }
                        fos.close();
                        is.close();
                        dbAsetHelper.importDataBase();
                    }
                }

                if (ctrlAppModul.isModul("61")) {
                    String DbDiskon = Util.getStringPreference(MenuUtamaActivity.this, "DbDiskon");
                    if (DbDiskon != null) {
                        URL url = new URL(DbDiskon);
                        HttpURLConnection c = (HttpURLConnection) url.openConnection();
                        c.connect();
                        File file = new File(Util.LOKASI);
                        file.mkdirs();
                        File outputFile = new File(file, Util.DBDISKON);
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }
                        FileOutputStream fos = new FileOutputStream(outputFile, true);
                        InputStream is = c.getInputStream();
                        byte[] buffer = new byte[1024];
                        int len1 = 0;
                        while ((len1 = is.read(buffer)) != -1) {
                            fos.write(buffer, 0, len1);
                        }
                        fos.close();
                        is.close();
                        dbDiskonHelper.importDataBase();
                    }
                }

                if (ctrlAppModul.isModul("32")) {
                    server.setUrl(Util.getServerUrl(MenuUtamaActivity.this) + "getgraph/SalesId/" + UserModel.getInstance(MenuUtamaActivity.this).SalesId);
                    result = server.requestJSONObjectNonThreadNoTimeOut(null);
                    Util.putPreference(MenuUtamaActivity.this, Util.GRAPH, result);
                    Util.putPreference(MenuUtamaActivity.this, Util.GRAPH_TIME, Util.FormatStringDate.format(new Date()));
                }

                if (ctrlAppModul.isModul("20") || ctrlAppModul.isModul("42")) {
                    server.setUrl(Util.getServerUrl(MenuUtamaActivity.this) + "getskucompetitors/SalesId/" + UserModel.getInstance(MenuUtamaActivity.this).SalesId);
                    result = server.requestJSONObjectNonThreadNoTimeOut(null);
                    if (result != null) {
                        JSONArray array = new JSONArray(result);
                        ctrlGroupSKUCom.deleteAll();
                        ctrlSKUCom.deleteAll();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            ctrlGroupSKUCom.save(new GroupSKUComModel(
                                    obj.getString("GroupId"),
                                    obj.getString("GroupName")
                            ));
                            JSONArray arraySKU = obj.getJSONArray("SKU");
                            for (int j = 0; j < arraySKU.length(); j++) {
                                JSONObject objSKU = arraySKU.getJSONObject(j);
                                ctrlSKUCom.save(new SKUComModel(
                                        objSKU.getString("SKUId"),
                                        objSKU.getString("ProductName"),
                                        obj.getString("GroupId"),
                                        objSKU.getString("SBESAR"),
                                        objSKU.getString("SKECIL")
                                ));
                            }
                        }
                    }
                }

                if (ctrlAppModul.isModul("31")) {
                    //get data dropper
                    server.setUrl(Util.getServerUrl(MenuUtamaActivity.this) + "getdropper/SalesId/" + UserModel.getInstance(MenuUtamaActivity.this).SalesId);
                    result = server.requestJSONObjectNonThreadNoTimeOut(null);
                    if (result != null) {
                        JSONArray jsonDropper = new JSONArray(result);
                        com.bhn.sadix.dropper.database.DatabaseHelper.clearDataMaster(MenuUtamaActivity.this);
                        for (int i = 0; i < jsonDropper.length(); i++) {
                            JSONObject jsonDropperCustomer = jsonDropper.getJSONObject(i);
                            ctrlCustomerDropper.save(new CustomerDropperModel(
                                    jsonDropperCustomer.getString("DropperID"),
                                    jsonDropperCustomer.getString("CustID"),
                                    jsonDropperCustomer.getString("CustName"),
                                    jsonDropperCustomer.getString("CustAddres"),
                                    jsonDropperCustomer.getString("CustLong"),
                                    jsonDropperCustomer.getString("CustLat"),
                                    jsonDropperCustomer.getString("DateServer")
                            ));
                            JSONArray jsonDropperWHArr = jsonDropperCustomer.getJSONArray("DropperWH");
                            for (int j = 0; j < jsonDropperWHArr.length(); j++) {
                                JSONObject jsonDropperWHA = jsonDropperWHArr.getJSONObject(j);
                                ctrlDropperWH.save(new DropperWHModel(
                                        jsonDropperCustomer.getString("DropperID"),
                                        jsonDropperWHA.getString("DropperWHID"),
                                        jsonDropperWHA.getString("WHID"),
                                        jsonDropperWHA.getString("WHName"),
                                        jsonDropperWHA.getString("WHAddres"),
                                        jsonDropperWHA.getString("WHLong"),
                                        jsonDropperWHA.getString("WHLat")
                                ));
                                JSONArray jsonDeliveryDropperArr = jsonDropperWHA.getJSONArray("DeliveryDropper");
                                for (int k = 0; k < jsonDeliveryDropperArr.length(); k++) {
                                    JSONObject jsonDeliveryDropper = jsonDeliveryDropperArr.getJSONObject(k);
                                    ctrlDeliveryDropper.save(new DeliveryDropperModel(
                                            jsonDropperWHA.getString("DropperWHID"),
                                            jsonDeliveryDropper.getString("DeliveryID"),
                                            jsonDeliveryDropper.getString("SKUId"),
                                            jsonDeliveryDropper.getString("GroupName"),
                                            jsonDeliveryDropper.getString("ProductName"),
                                            jsonDeliveryDropper.getString("SBESAR"),
                                            jsonDeliveryDropper.getString("SKECIL"),
                                            jsonDeliveryDropper.getInt("QBESAR"),
                                            jsonDeliveryDropper.getInt("QKECIL"),
                                            jsonDropperCustomer.getString("DropperID")
                                    ));
                                }
                                JSONArray jsonReturnDropperrArr = jsonDropperWHA.getJSONArray("ReturnDropper");
                                for (int k = 0; k < jsonReturnDropperrArr.length(); k++) {
                                    JSONObject jsonReturnDropper = jsonReturnDropperrArr.getJSONObject(k);
                                    ctrlReturnDropper.save(new ReturnDropperModel(
                                            jsonDropperWHA.getString("DropperWHID"),
                                            jsonReturnDropper.getString("ReturnID"),
                                            jsonReturnDropper.getString("SKUId"),
                                            jsonReturnDropper.getString("GroupName"),
                                            jsonReturnDropper.getString("ProductName"),
                                            jsonReturnDropper.getString("SBESAR"),
                                            jsonReturnDropper.getString("SKECIL"),
                                            jsonReturnDropper.getInt("QBESAR"),
                                            jsonReturnDropper.getInt("QKECIL"),
                                            jsonDropperCustomer.getString("DropperID")
                                    ));
                                }
                            }
                        }
                    }
                }

                result = null;
            } catch (Exception e) {
                e.printStackTrace();
                result = e.getMessage();
                Log.e("error", ">>" + result);
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.okeDialog(MenuUtamaActivity.this, "Info", /*"Koneksi gagal, silahkan mencari mencari tempat yang ada koneksi"*/result, new OkListener() {
                    @Override
                    public void onDialogOk() {
                        Util.putPreference(MenuUtamaActivity.this, "isLogin", false);
                        stopService(new Intent(MenuUtamaActivity.this, SadixService.class));
                        startActivity(new Intent(MenuUtamaActivity.this, LoginActivity.class));
                        finish();
                    }
                });
            } else {
                loadGraph();
                Util.putPreference(MenuUtamaActivity.this, "isLoadData", false);
//				getIntent().putExtra("isLoadData", false);
//				Log.e("else isLoadData", "" + getIntent().getBooleanExtra("isLoadData", false));
            }
            super.onPostExecute(result);
        }
    }

    //	class MenuFragment extends SherlockFragment {
//		private View view = null;
//		@Override
//	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//	        view = inflater.inflate(R.layout.fragment_menu_utama, container, false);
//	        return view;
//	    }
//		public void setPosisi(Location loc) {
//            ((TextView)view.findViewById(R.id.posisis)).setText("Posisi:\nlong:"+String.format("%1$s", loc.getLongitude())+
//            		"\nlat:"+String.format("%1$s", loc.getLatitude()));
//		}
//	}
    private void scanBarcode() {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            Util.showDialogInfo(this, "ZXing Scanner Not Found, Please Install First!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String kode = intent.getStringExtra("SCAN_RESULT");
//		        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                String[] pecahKode = kode.split("[|]");
                if (pecahKode.length == 3) {
                    if (pecahKode[0].equalsIgnoreCase(UserModel.getInstance(this).CompanyID)) {
                        CustomerModel customerModel = ctrlCustomer.get(pecahKode[2]);
//						Intent intentCustDetail = new Intent(this, DetailCustomerActivity.class);
                        Intent intentAct = null;
                        if (customerModel != null) {
                            if (ctrlAppModul.isModul("56")) {
                                CommonModel commonModel = ctrlCommon.getByCustomerAndDate(customerModel.CustId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                                if (commonModel != null) {
//									Util.showDialogInfo(this, "Customer " + customerModel.CustomerName + " sudah dikunjungi!");
//									return;
//									intentCustDetail.putExtra("MSG", "Customer " + customerModel.CustomerName + " sudah dikunjungi!");

                                    intentAct = new Intent(this, DetailCustomerActivity.class);
                                    intentAct.putExtra("MSG", "Customer " + customerModel.CustomerName + " sudah dikunjungi!");
                                } else {
                                    intentAct = new Intent(this, KunjunganActivity.class);
                                }
                            } else {
                                intentAct = new Intent(this, KunjunganActivity.class);
                            }
//							Intent kunjungan = new Intent(this, KunjunganActivity.class);
//							kunjungan.putExtra("CustomerModel", customerModel);
//							startActivity(kunjungan);
                            if (intentAct != null) {
                                if (intentAct.getStringExtra("MSG") == null) {
                                    intentAct.putExtra("CustomerModel", customerModel);
                                    intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
                                    startActivity(intentAct);
                                } else {
                                    intentAct.putExtra("CustomerModel", customerModel);
                                    intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
                                    final Intent tmpIntentAct = intentAct;
                                    Util.okeDialog(this, "Info", intentAct.getStringExtra("MSG"), new OkListener() {
                                        @Override
                                        public void onDialogOk() {
                                            startActivity(tmpIntentAct);
                                        }
                                    });
                                }
                            }
                        } else {
                            Util.Toast(this, "Barcode tidak ditemukan");
                        }
                    } else {
                        Util.Toast(this, "Barcode tidak ditemukan");
                    }
                }
            }
        } else if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String AssetNumber = intent.getStringExtra("SCAN_RESULT");
                requestAsset(AssetNumber);
            }
        } else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    type4.setBitmap(bitmap, bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showDialogError(this, e.getMessage());
                }
            }
        }
    }

    @Override
    public void error(String error) {
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        if (jenis == 1) {
            Util.putPreference(MenuUtamaActivity.this, Util.GRAPH, respon);
            Util.putPreference(MenuUtamaActivity.this, Util.GRAPH_TIME, Util.FormatStringDate.format(new Date()));
            loadGraph();
        } else if (jenis == 2) {
            try {
                JSONObject json = new JSONObject(respon);
                if (json.has("LoginException")) {
                    if (json.getJSONObject("LoginException").has("Result") && json.getJSONObject("LoginException").getString("Result").equals("OK")) {
                        JSONObject login = json.getJSONObject("Login");
                        if (login.has("PathSvr") && login.getString("PathSvr") != null && !login.getString("PathSvr").trim().equals("") && !login.getString("PathSvr").trim().toLowerCase().equals("null")) {
                            Util.putPreference(this, "PathSvr", login.getString("PathSvr"));
                            server.lanjutLoading();
                            loadLogin();
                        } else {
                            if (login.has("IsClear")) {
                                if (login.getInt("IsClear") == 1) {
                                    DatabaseHelper.clearData(this);
                                    com.bhn.sadix.dropper.database.DatabaseHelper.clearData(this);
                                }
                            }
                            if (login.has("DbSku")) {
                                Util.putPreference(this, "DbSku", login.getString("DbSku"));
                            }
                            if (login.has("DbCustomer")) {
                                Util.putPreference(this, "DbCustomer", login.getString("DbCustomer"));
                            }
                            if (login.has("DbAset")) {
                                Util.putPreference(this, "DbAset", login.getString("DbAset"));
                            }
                            if (login.has("DbDiskon")) {
                                Util.putPreference(this, "DbDiskon", login.getString("DbDiskon"));
                            }
                            ctrlAppModul.deleteAll();
                            JSONArray AppModul = json.getJSONArray("AppModul");
                            for (int i = 0; i < AppModul.length(); i++) {
                                JSONObject obj = AppModul.getJSONObject(i);
                                ctrlAppModul.save(new com.bhn.sadix.model.AppModul(obj.getString("AppModulId"), obj.getString("AppModulValue")));
                            }
                            progressDialog.show();
                            new LoadData().execute();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                error(e.getMessage());
            }
        } else if (jenis == 3) {
            try {
                JSONObject json = new JSONObject(respon);
                if (json.getString("AsetName").equals("Error")) {
                    Util.showDialogInfo(this, "Nomor Asset tidak ditemukan!");
                } else {
                    Intent intent = new Intent(this, NewServiceAssetActivity.class);
                    intent.putExtra("ASETNAME", json.getString("AsetName"));
                    intent.putExtra("ASETTYPE", json.getString("TypeID"));
                    intent.putExtra("ASETNO", json.getString("AsetNO"));
                    intent.putExtra("ASETID", json.getString("AsetIDX"));
                    startActivity(intent);

//					AssetServiceModel assetModel = new AssetServiceModel(this, 
//						json.getString("AsetName"), 
//						json.getString("TypeID"), 
//						json.getString("AsetNO"), 
//						json.getString("AsetIDX"), 
//						new AssetServiceWidget.AssetServiceWidgetListener() {
//							@Override
//							public void photoType4(AssetType4Widget type4) {
//								MenuUtamaActivity.this.type4 = type4;
//								Intent intent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//								startActivityForResult(intent, 2);
//							}
//						},
//						"0");
//					assetModel.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadGraph() {
        try {
            ((TextView) findViewById(R.id.update_data)).setText("Last Update " + Util.getStringPreference(this, Util.GRAPH_TIME));
            String sgraph = Util.getStringPreference(this, Util.GRAPH);
            if (sgraph != null) {
                JSONArray jsonGraph = new JSONArray(sgraph);
                Gson gson = new Gson();
                Chart[] charts = gson.fromJson(sgraph, Chart[].class);
                Log.d("Chart Length", charts.length + "");
                ((LinearLayout) findViewById(R.id.pie_chart)).removeAllViews();
                for (int i = 0; i < charts.length; i++) {

                    Chart chart = charts[i];
                    /*LinearLayout view = new LinearLayout(this);
                    LinearLayout.LayoutParams param_view = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            600
                    );
                    view.setLayoutParams(param_view);*/

                    if (chart.getTypeChat() == 1) {
                        PieChart pie = new PieChart(getApplicationContext());

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 600);
                        params.setMargins(0, 20, 0, 20);
                        pie.setLayoutParams(params);

                        pie.setBackgroundResource(android.R.color.white);
                        pie.setCenterText(chart.getTitle());
                        pie.getDescription().setEnabled(false);
                        pie.setDragDecelerationFrictionCoef(0.95f);
                        pie.setDrawHoleEnabled(true);
                        pie.setUsePercentValues(true);
                        pie.setTransparentCircleColor(Color.WHITE);
                        pie.setHoleRadius(58f);
                        pie.setTransparentCircleRadius(61f);
                        pie.setDrawCenterText(true);
                        pie.setRotationAngle(0);
                        pie.setRotationEnabled(true);
                        pie.setHighlightPerTapEnabled(true);

                        ((LinearLayout) findViewById(R.id.pie_chart)).addView(pie);

                        List<DataChart> data = chart.getData();
                        ArrayList<PieEntry> entries = new ArrayList<>();
                        int[] colors = new int[data.size()];

                        for (int j = 0; j < data.size(); j++) {
                            DataChart dataChart = data.get(j);

                            entries.add(new PieEntry(dataChart.getValue(), dataChart.getLabel()));

                            colors[j] = Color.parseColor(dataChart.getColor());
                        }

                        PieDataSet deviceSet = new PieDataSet(entries, "");
                        deviceSet.setSliceSpace(3f);
                        deviceSet.setSelectionShift(5f);

                        deviceSet.setColors(colors);

                        deviceSet.setValueLinePart1OffsetPercentage(80.f);
                        deviceSet.setValueLinePart1Length(0.2f);
                        deviceSet.setValueLinePart2Length(0.4f);
                        deviceSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

                        PieData deviceData = new PieData(deviceSet);
                        deviceData.setValueFormatter(new CustomFormatter());
                        deviceData.setValueTextSize(12f);
                        deviceData.setValueTextColor(Color.BLACK);
                        pie.setData(deviceData);

                        pie.highlightValues(null);
                        pie.invalidate();
//                        view.addView(pieChart.getView());
                    } else if (chart.getTypeChat() == 0) {
                        HorizontalBarChart barChart = new HorizontalBarChart(getApplicationContext());

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 600);
                        params.setMargins(0, 20, 0, 20);
                        barChart.setLayoutParams(params);

                        barChart.setDrawBarShadow(false);
                        barChart.setDrawValueAboveBar(true);
                        barChart.getDescription().setEnabled(false);
                        barChart.setPinchZoom(false);
                        barChart.setDrawGridBackground(false);

                        XAxis xl = barChart.getXAxis();
                        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xl.setDrawAxisLine(true);
                        xl.setDrawGridLines(false);
                        xl.setAxisMinimum(0f);
                        xl.setGranularity(10f);
                        xl.setGranularityEnabled(true);
//                        xl.setLabelRotationAngle(-35f);
                        /*
                        xl.setGranularity(50f);
                        xl.setGranularityEnabled(true);*/

                        YAxis yl = barChart.getAxisLeft();
                        yl.setDrawAxisLine(true);
                        yl.setDrawGridLines(true);
                        yl.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        yl.setInverted(true);

                        YAxis yr = barChart.getAxisRight();
                        yr.setDrawAxisLine(true);
                        yr.setDrawGridLines(false);
                        yr.setAxisMinimum(0f);

                        barChart.setFitBars(true);
                        barChart.animateY(2500);

//                        BarChart barChart = new BarChart(this, graph.getString("Title"), "", "", "");
                        List<DataChart> dataCharts = chart.getData();
                        ArrayList<BarEntry> entries = new ArrayList<>();
                        ArrayList<String> labels = new ArrayList<>();
                        int[] colors = new int[dataCharts.size()];

                        float barWidth = 9f;
                        float spaceForBar = 10f;
                        for (int j = 0; j < dataCharts.size(); j++) {
                            DataChart dataChart = dataCharts.get(j);
                            entries.add(new BarEntry((j + 1) * spaceForBar, dataChart.getValue(), dataChart.getLabel()));
                            labels.add(dataChart.getLabel());
                            colors[j] = Color.parseColor(dataChart.getColor());
                        }

                        xl.setValueFormatter(new XValueFormatter(labels));

                        BarDataSet set = new BarDataSet(entries, chart.getTitle());
                        set.setColors(colors);
                        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
                        dataSets.add(set);

                        BarData data = new BarData(dataSets);
                        data.setValueTextSize(10f);
                        data.setBarWidth(barWidth);
                        barChart.setData(data);
                        barChart.getLegend().setEnabled(false);

                        TextView txtTitle = new TextView(this);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                        layoutParams.setMargins(5, 10, 5, 5);

                        txtTitle.setLayoutParams(layoutParams);
                        txtTitle.setText("Data " + chart.getTitle());
                        txtTitle.setTypeface(null, Typeface.BOLD);
                        txtTitle.setTextSize(18);
                        txtTitle.setGravity(Gravity.CENTER);

                        ((LinearLayout) findViewById(R.id.pie_chart)).addView(txtTitle);
                        ((LinearLayout) findViewById(R.id.pie_chart)).addView(barChart);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    private void addNewAsset() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_asset);
        dialog.setTitle("ADD ASSET");
        ((Button) dialog.findViewById(R.id.btn_scanbarcode)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                startActivityForResult(intent, 1);
            }
        });
        ((Button) dialog.findViewById(R.id.btn_input)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                inputAssetNumber();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void inputAssetNumber() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_input_asset);
        dialog.setTitle("INPUT ASSET NUMBER");
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String AssetNumber = ((EditText) dialog.findViewById(R.id.AssetNumber)).getText().toString();
                dialog.dismiss();
                requestAsset(AssetNumber);
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void requestAsset(String assetNumber) {
        jenis = 3;
        server.setUrl(Util.getServerUrl(this) + "getasset/AssetNo/" + assetNumber + "/SalesId/" + Util.getStringPreference(this, "SalesId"));
        server.requestJSONObject(null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = true;
    }

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }*/
}
