package com.bhn.sadix;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bhn.sadix.adapter.PriceListAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase;

public class PriceListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    SearchView searchView;
    ExpandableListView priceList;

    Toolbar toolbar;
    private CtrlGroupSKU ctrlGroupSKU;

    PriceListAdapter adapter;
    PriceListAdapter searchAdapter;

    ArrayList<SKUModel> allSku;
    List<SKUModel> queryingSku;
    List<GroupSKUModel> allGroup;
    private CtrlSKU ctrlSKU;
    private String Filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);

        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlSKU = new CtrlSKU(this);
        adapter = new PriceListAdapter(this, new ArrayList<GroupSKUModel>(), null) {
            @Override
            public void onShowDialog(SKUModel skuModel) {
                showDialog(skuModel);
            }
        };

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MenuUtamaActivity.isActive) {
                    finish();
                } else {
                    /*Intent intent = new Intent(AllCustomerActivity.this, MenuUtamaActivity.class);
                    startActivity(intent);*/
                }
            }
        });

        getSupportActionBar().setTitle("Price List");

        priceList = (ExpandableListView) findViewById(R.id.list_price);

        priceList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                populateSKUByGroup(groupPosition, adapter.getGroup(groupPosition));
                return false;
            }
        });

        loadAdapter();
    }

    private void populateSKUByGroup(int i, GroupSKUModel group) {
        List<SKUModel> listSKU = ctrlSKU.listByGroup(group, group.GroupId);

        int size = adapter.getChildrenCount(i);
        if (size == 0 /*|| size < group.jumlah*/) {
            for (SKUModel skuModel : listSKU) {
                adapter.addChild(group, skuModel);
            }
        }
    }

    private void loadAdapter() {
        allGroup = ctrlGroupSKU.list();

        for (GroupSKUModel groupSKUModel : allGroup) {
            adapter.addHeader(groupSKUModel);
        }

        priceList.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        searchView.setOnQueryTextListener(this);
        MenuItem item = menu.add(1, 1, 1, "Search")
                .setIcon(android.R.drawable.ic_menu_search);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            item.setActionView(searchView)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Filter = query;
        if (!query.equals("")) {
            searchList(query);
        } else {
            setToDefaultAdapter();
        }
        return false;
    }

    private void setToDefaultAdapter() {
        priceList.setAdapter(adapter);
        priceList.invalidate();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!newText.equals("")) {
            searchList(newText);
        } else {
            setToDefaultAdapter();
        }
        return false;
    }

    private void searchList(String newText) {
        searchAdapter = new PriceListAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<SKUModel>>(), newText) {
            @Override
            public void onShowDialog(SKUModel skuModel) {
                showDialog(skuModel);
            }
        };

        for (GroupSKUModel group : allGroup) {
            queryingSku = new ArrayList<>();
            queryingSku = ctrlSKU.searchSKUByGroup(newText, group.GroupId);
            Log.d("Query Size", queryingSku.size() + "");
            if (queryingSku.size() > 0) {
                searchAdapter.addHeader(group);
                searchAdapter.addChild(group, queryingSku);
            }
        }

        priceList.setAdapter(searchAdapter);
        priceList.invalidate();

        for (int i = 0; i < searchAdapter.getGroupCount(); i++) {
            priceList.expandGroup(i);
        }
    }

    @NonNull
    public void showDialog(final SKUModel skuModel) {
        AlertDialog dl = null;
        AlertDialog.Builder dlBuilder = new AlertDialog.Builder(PriceListActivity.this);
        final View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_price_list, null);
        dlBuilder.setView(v);
        dlBuilder.setTitle("");
        dlBuilder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        ((TextView) v.findViewById(R.id.title)).setText(skuModel.ProductName);
        ((TextView) v.findViewById(R.id.SKUCODE)).setText(skuModel.SKUCODE);

        dl = dlBuilder.create();
        dl.show();

        Thread t = new Thread() {
            public void run() {
                try {
                    Bitmap bitmap = null;
                    String data = ConnectionServer.requestJSONObjectNonThread((JSONObject) null, Util.getServerUrl(PriceListActivity.this) + "getskupic/SKUid/" + skuModel.SKUId, true);
                    final JSONObject json = new JSONObject(data);
                    if (json.has("SKUPicture")) {
                        byte[] img = Base64.decode(json.getString("SKUPicture"));
                        bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                        if (bitmap != null) {
                            final Bitmap finalBitmap1 = bitmap;
                            v.post(new Runnable() {
                                @Override
                                public void run() {
                                    v.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                    v.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                    ((ImageViewTouch) v.findViewById(R.id.img)).setDisplayType(ImageViewTouchBase.DisplayType.FIT_IF_BIGGER);
                                    ((ImageViewTouch) v.findViewById(R.id.img)).setImageBitmap(finalBitmap1);
                                }
                            });
                        } else {
                            v.post(new Runnable() {
                                @Override
                                public void run() {
                                    v.findViewById(R.id.progressBar).setVisibility(View.GONE);
//                                    v.findViewById(R.id.text_error).setVisibility(View.VISIBLE);
                                    v.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                    ((ImageViewTouch) v.findViewById(R.id.img)).setDisplayType(ImageViewTouchBase.DisplayType.NONE);
                                    ((ImageViewTouch) v.findViewById(R.id.img)).setImageResource(R.drawable.ic_broken_image_black_24dp);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                        v.findViewById(R.id.img).setAlpha(0.3f);
                                    }
                                    ((TextView) v.findViewById(R.id.img_desc)).setText("Gambar produk belum tersedia");
//                                    v.findViewById(R.id.img).setVisibility(View.GONE);
                                }
                            });
                        }
                    } else {
                        v.post(new Runnable() {
                            @Override
                            public void run() {
                                v.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                v.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                ((ImageViewTouch) v.findViewById(R.id.img)).setDisplayType(ImageViewTouchBase.DisplayType.NONE);
                                ((ImageViewTouch) v.findViewById(R.id.img)).setImageResource(R.drawable.ic_broken_image_black_24dp);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    v.findViewById(R.id.img).setAlpha(0.3f);
                                }
                                ((TextView) v.findViewById(R.id.img_desc)).setText("Gambar produk belum tersedia");
                            }
                        });
                    }

                    final Bitmap finalBitmap = bitmap;
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            if (json.has("SKUDesc")) {
                                try {
                                    Log.d("Desc", json.getString("SKUDesc"));

                                    String desc;
                                    if (finalBitmap != null) {
                                        desc = json.getString("SKUDesc").equals("null") ? "" : json.getString("SKUDesc");
                                        ((TextView) v.findViewById(R.id.img_desc)).setText(desc);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.d("Desc", "Desc Not Available");
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            v.findViewById(R.id.progressBar).setVisibility(View.GONE);
                            v.findViewById(R.id.img).setVisibility(View.VISIBLE);
                            ((ImageViewTouch) v.findViewById(R.id.img)).setDisplayType(ImageViewTouchBase.DisplayType.NONE);
                            ((ImageViewTouch) v.findViewById(R.id.img)).setImageResource(R.drawable.ic_broken_image_black_24dp);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                v.findViewById(R.id.img).setAlpha(0.3f);
                            }
                            ((TextView) v.findViewById(R.id.img_desc)).setText("Gambar produk belum tersedia");
                        }
                    });
                }
            }
        };
        t.start();
    }
}