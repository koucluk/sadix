package com.bhn.sadix.fragment.kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.SalesProgramAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCrcOrder;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSalesProgram;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbSkuHelper;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.CrcOrderModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

public class TakingOrder2Fragment extends Fragment {
    private View view = null;
    private OrderDetailAdapter adapterAll = null;
    private OrderDetailAdapter adapterOrder = null;
    private OrderDetailAdapter adapterMhs = null;
    private OrderDetailAdapter adapterNonMhs = null;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private CtrlSalesProgram ctrlSalesProgram;
    private Context context;
    private ConnectionServer server;
    private CustomerTypeModel customerTypeModel;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    private CustomerModel customerModel;
    private CtrlAppModul ctrlAppModul;
    private CtrlSKU ctrlSKU;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlCrcOrder ctrlCrcOrder;

    private View vMhs;
    private View vNonMhs;
    private View vOrder;

    Realm realmUI;

    private DbSkuHelper dbSku;

    private KunjunganListener listener;

    public static TakingOrder2Fragment newInstance() {
        Bundle args = new Bundle();

        TakingOrder2Fragment fragment = new TakingOrder2Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    public OrderDetailAdapter getAdapterMhs() {
        return adapterMhs;
    }

    public OrderDetailAdapter getAdapterNonMhs() {
        return adapterNonMhs;
    }

    //	public TakingOrder2Fragment(Context context, CustomerTypeModel customerTypeModel, CustomerModel customerModel) {
//    	this.context = context;
//    	this.customerTypeModel = customerTypeModel;
//    	this.customerModel = customerModel;
//    	ctrlSalesProgram = new CtrlSalesProgram(this.context);
//    	ctrlAppModul = new CtrlAppModul(this.context);
//    	ctrlSKU = new CtrlSKU(this.context);
//    	ctrlTakingOrder = new CtrlTakingOrder(this.context);
//    	ctrlCrcOrder = new CtrlCrcOrder(this.context);
//    	dbSku = new DbSkuHelper(this.context);
//    	server = new ConnectionServer(this.context);
//    	adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
//    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlSalesProgram = new CtrlSalesProgram(this.context);
        ctrlAppModul = new CtrlAppModul(this.context);
        ctrlSKU = new CtrlSKU(this.context);
        ctrlTakingOrder = new CtrlTakingOrder(this.context);
        ctrlCrcOrder = new CtrlCrcOrder(this.context);
        dbSku = new DbSkuHelper(this.context);
        server = new ConnectionServer(this.context);
        realmUI = Realm.getDefaultInstance();
//        commonModel = listener.getCommonModel();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        customerModel = listener.getCustomerModel();
        customerTypeModel = listener.getCustomerType();

        adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
//        adapterAll = listener.getTakingAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_taking_order2, container, false);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_mhs) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_non_mhs) {
                    mPager.setCurrentItem(1);
                } else if (checkedId == R.id.tab_order) {
                    mPager.setCurrentItem(2);
                }
            }
        });

        mPager = (ViewPager) view.findViewById(R.id.pager_taking_order);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_mhs);
                } else if (position == 1) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_non_mhs);
                } else if (position == 2) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_order);
                }
                super.onPageSelected(position);
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);
        if (customerTypeModel != null) {
            String title = customerTypeModel.DiskonType.equals("2") ? "(Rp)" : "(%)";
            ((TextView) view.findViewById(R.id.lbl_discount)).setText("Discount " + title);
        }
        ((EditText) view.findViewById(R.id.discount)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateAmount();
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerTypeModel = listener.getCustomerType();
//        commonModel = listener.getCommonModel();
        customerModel = listener.getCustomerModel();

        if (adapterAll == null) {
            ((KunjunganActivity) getActivity()).loadTakingOrder(2);
        }
    }

    public void loadAdapter() {
        adapterAll = listener.getTakingAdapter();
        initAdapter();

        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);
    }

    private void loadOrder() {
        try {
            adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            for (int i = 0; i < adapterMhs.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapterMhs.getGroup(i);
                adapterOrder.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < adapterMhs.getChildrenCount(i); j++) {
                    final OrderDetailModel orderDetailModel = adapterMhs.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterOrder.addChild(groupSKUModel, orderDetailModel);

                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                boolean needToAdd = false;
                                if (!(realm.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .equalTo("SKUId", orderDetailModel.skuModel.SKUId).findAll().size() > 0)) {
                                    needToAdd = true;
                                }
                                com.bhn.sadix.Data.TakingOrderModel tm = new com.bhn.sadix.Data.TakingOrderModel();

                                tm.CustId = customerModel.CustId;
                                tm.SKUId = orderDetailModel.skuModel.SKUId;
                                tm.tanggal = "";
                                tm.SalesProgramId = orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "";
                                tm.QTY_B = orderDetailModel.QTY_B;
                                tm.QTY_K = orderDetailModel.QTY_K;
                                tm.DISCOUNT1 = orderDetailModel.DISCOUNT1;
                                tm.DISCOUNT2 = orderDetailModel.DISCOUNT2;
                                tm.DISCOUNT3 = orderDetailModel.DISCOUNT3;
                                tm.TOTAL_PRICE = orderDetailModel.TOTAL_PRICE;
                                tm.PAYMENT_TERM = orderDetailModel.PAYMENT_TERM;
                                tm.note = orderDetailModel.note;
                                tm.PRICE_B = orderDetailModel.PRICE_B;
                                tm.PRICE_K = orderDetailModel.PRICE_K;
                                tm.LAMA_KREDIT = orderDetailModel.LAMA_CREDIT;
                                Log.d("DIscount Type Mhs", orderDetailModel.DiscountType);
                                tm.DiscountType = orderDetailModel.DiscountType;
                                tm.CommonID = commonModel.CommonID;
                                tm.RandomID = orderDetailModel.RandomID;

                                realm.copyToRealmOrUpdate(tm);

                                if (needToAdd)
                                    commonModel.takingOrderModel.add(tm);
                            }
                        });

                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterOrder.removeHeader(groupSKUModel);
                }
            }
            for (int i = 0; i < adapterNonMhs.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapterNonMhs.getGroup(i);
                int index = adapterOrder.indexOfHeader(groupSKUModel);
                if (index == -1) {
                    adapterOrder.addHeader(groupSKUModel);
                } else {
                    groupSKUModel = adapterOrder.getGroup(index);
                }
                boolean flag = false;
                for (int j = 0; j < adapterNonMhs.getChildrenCount(i); j++) {
                    final OrderDetailModel orderDetailModel = adapterNonMhs.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterOrder.addChild(groupSKUModel, orderDetailModel);

                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                boolean needToAdd = false;
                                if (!(realm.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .equalTo("SKUId", orderDetailModel.skuModel.SKUId).findAll().size() > 0)) {
                                    needToAdd = true;
                                }
                                com.bhn.sadix.Data.TakingOrderModel tm = new com.bhn.sadix.Data.TakingOrderModel();

                                tm.CustId = customerModel.CustId;
                                tm.SKUId = orderDetailModel.skuModel.SKUId;
                                tm.tanggal = "";
                                tm.SalesProgramId = orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "";
                                tm.QTY_B = orderDetailModel.QTY_B;
                                tm.QTY_K = orderDetailModel.QTY_K;
                                tm.DISCOUNT1 = orderDetailModel.DISCOUNT1;
                                tm.DISCOUNT2 = orderDetailModel.DISCOUNT2;
                                tm.DISCOUNT3 = orderDetailModel.DISCOUNT3;
                                tm.TOTAL_PRICE = orderDetailModel.TOTAL_PRICE;
                                tm.PAYMENT_TERM = orderDetailModel.PAYMENT_TERM;
                                tm.note = orderDetailModel.note;
                                tm.PRICE_B = orderDetailModel.PRICE_B;
                                tm.PRICE_K = orderDetailModel.PRICE_K;
                                tm.LAMA_KREDIT = orderDetailModel.LAMA_CREDIT;
                                Log.d("DIscount Type NonMhs", orderDetailModel.DiscountType);
                                tm.DiscountType = orderDetailModel.DiscountType;
                                tm.CommonID = commonModel.CommonID;
                                tm.RandomID = orderDetailModel.RandomID;

                                realm.copyToRealmOrUpdate(tm);

                                if (needToAdd)
                                    commonModel.takingOrderModel.add(tm);
                            }
                        });
                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterOrder.removeHeader(groupSKUModel);
                }
            }
            if (vOrder != null) {
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                adapterOrder.notifyDataSetInvalidated();
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
        if (adapterAll != null) {
            initAdapter();

            ViewPagerAdapter pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);
            loadOrder();
            calculateAmount();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
//        calculateAmount();
    }

    private void calculateAmount() {
        int QTY_B = 0;
        int QTY_K = 0;
        double PRICE = 0;
        double DISCOUNT = 0;
        double S_AMOUNT = 0;
        for (int i = 0; i < adapterMhs.getGroupCount(); i++) {
            for (int j = 0; j < adapterMhs.getChildrenCount(i); j++) {
                OrderDetailModel orderDetailModel = adapterMhs.getChild(i, j);
                QTY_B += orderDetailModel.QTY_B;
                QTY_K += orderDetailModel.QTY_K;
                PRICE += orderDetailModel.TOTAL_PRICE;
            }
        }
        for (int i = 0; i < adapterNonMhs.getGroupCount(); i++) {
            for (int j = 0; j < adapterNonMhs.getChildrenCount(i); j++) {
                OrderDetailModel orderDetailModel = adapterNonMhs.getChild(i, j);
                QTY_B += orderDetailModel.QTY_B;
                QTY_K += orderDetailModel.QTY_K;
                PRICE += orderDetailModel.TOTAL_PRICE;
            }
        }
        String cek = ((EditText) view.findViewById(R.id.discount)).getText().toString();
        DISCOUNT = Double.parseDouble(cek.equals("") ? "0" : cek);
        if (customerTypeModel != null) {
            if (DISCOUNT > customerTypeModel.DiskonMax) {
                String max = "";
                if (customerTypeModel.DiskonType.equals("2")) {
                    max = "Rp." + customerTypeModel.DiskonMax;
                } else {
                    max = "" + customerTypeModel.DiskonMax + "%";
                }
                Util.showDialogInfo(context, "Nilai melebihi batas masimum " + max);
                ((EditText) view.findViewById(R.id.discount)).setText(null);
                return;
            } else {
                if (customerTypeModel.DiskonType.equals("2")) {
                    S_AMOUNT = PRICE - DISCOUNT;
                } else {
                    S_AMOUNT = PRICE - (PRICE * DISCOUNT / 100);
                }
            }
        }

        final int finalQTY_B = QTY_B;
        final int finalQTY_K = QTY_K;
        final double finalPRICE = PRICE;
        final double finalDISCOUNT = DISCOUNT;
        final double finalS_AMOUNT = S_AMOUNT;

        try {
            realmUI.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    CommonModel cmModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();

                    cmModel.QTY_B = finalQTY_B;
                    cmModel.QTY_K = finalQTY_K;
                    cmModel.PRICE = finalPRICE;
                    cmModel.DISCOUNT = finalDISCOUNT;
                    cmModel.S_AMOUNT = finalS_AMOUNT;

                    if (view != null) {
                        ((TextView) view.findViewById(R.id.total_qty)).setText(cmModel.QTY_B + "," + cmModel.QTY_K);
                        ((TextView) view.findViewById(R.id.price)).setText(Util.numberFormat.format(cmModel.PRICE));
                        ((TextView) view.findViewById(R.id.total_price)).setText(Util.numberFormat.format(cmModel.S_AMOUNT));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void total(final View dialog, final OrderDetailModel model) {
        double total = totalValue(dialog, model);
        if (total < 0) {
            Util.showDialogInfo(context, "Diskon anda melebihi total harga!");
            ((EditText) dialog.findViewById(R.id.discount_1)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_2)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_3)).setText("0");
            ((TextView) dialog.findViewById(R.id.total_price)).setText("0");
        } else {
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(total));
        }
    }

    private double totalValue(final View dialog, final OrderDetailModel model) {
        try {
            boolean isDiskonBesar = ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked();
            String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
            int jml1 = Integer.parseInt(cek.equals("") ? "0" : cek);
            cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
            int jml2 = Integer.parseInt(cek.equals("") ? "0" : cek);

            double diskon = 0;

            //diskon 1
            cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
            double diskon1 = 0;
            if (model.skuModel.DISCOUNT_1_TYPE.equals("2")) {
                diskon1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon1 > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(context, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(context, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                } else {
                    diskon1 = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) * persen / 100;
                }
            }
            diskon = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) - diskon1;
            //diskon 2
            cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
            double diskon2 = 0;
            if (model.skuModel.DISCOUNT_2_TYPE.equals("2")) {
                diskon2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon2 > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(context, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(context, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                } else {
                    diskon2 = diskon * persen / 100;
                }
            }
            diskon = diskon - diskon2;
            //hitung total
            double total = 0;
//			if(model.skuModel.DISCOUNT_TYPE.equals("1")) {
            if (isDiskonBesar) {
//				double het_b = model.skuModel.HET_PRICE_L;
//				het_b -= diskon1;
//				het_b -= diskon2;
                total = (jml1 * diskon) + (jml2 * model.skuModel.HET_PRICE);
            } else {
//				double het_k = model.skuModel.HET_PRICE;
//				het_k -= diskon1;
//				het_k -= diskon2;
                double qty_k = jml1 * model.skuModel.CONVER;
                qty_k += jml2;
//				total = het_k*qty_k;
                total = diskon * qty_k;
            }
            //diskon 3
            cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
            double diskon3 = 0;
            if (model.skuModel.DISCOUNT_3_TYPE.equals("2")) {
                diskon3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon3 > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(context, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(context, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                } else {
                    diskon3 = total * persen / 100;
                }
            }
            total = total - diskon3;
            return total;
        } catch (Exception e) {
            return 0;
        }

    }

    private void addJumlah(final OrderDetailModel model) {
        final Dialog dl = new Dialog(getActivity());
        dl.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dl.setContentView(R.layout.dialog_new_taking_order2);
        ((TextView) dl.findViewById(R.id.title)).setText(model.skuModel.ProductName);

        if (model.Promo == 1) {
            dl.findViewById(R.id.panel_promo).setVisibility(View.VISIBLE);
            ((TextView) dl.findViewById(R.id.desc_promo)).setText(model.KeteranganPromo);
        } else {
            dl.findViewById(R.id.panel_promo).setVisibility(View.GONE);
        }

        final ViewPager mPager = (ViewPager) dl.findViewById(R.id.pager);
        mPager.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(View collection, final int position) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = null;
                if (position == 0) {
                    v = inflater.inflate(R.layout.dialog_taking_order, null);
                    showDataTakingOrder(v, model, dl);
                } else if (position == 1) {
                    v = inflater.inflate(R.layout.dialog_taking_order_image, null);
                    loadImage(model, v);
                }
                ((ViewPager) collection).addView(v, 0);
                return v;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == ((View) object);
            }

            @Override
            public void destroyItem(View collection, int position, Object view) {
                ((ViewPager) collection).removeView((View) view);
            }

            @Override
            public Parcelable saveState() {
                return null;
            }

            @Override
            public int getCount() {
                return 2;
            }
        });

        dl.findViewById(R.id.title).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        dl.findViewById(R.id.gambar).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });

        dl.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dl.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void loadImage(final OrderDetailModel model, final View image) {
        if (model.bitmap == null) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        String data = ConnectionServer.requestJSONObjectNonThread((JSONObject) null, Util.getServerUrl(context) + "getskupic/SKUid/" + model.skuModel.SKUId, true);
                        JSONObject json = new JSONObject(data);
                        if (json.has("SKUPicture")) {
                            byte[] img = Base64.decode(json.getString("SKUPicture"));
                            model.bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                            if (model.bitmap != null) {
                                image.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                        image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                        ((ImageViewTouch) image.findViewById(R.id.img)).setDisplayType(DisplayType.FIT_IF_BIGGER);
                                        ((ImageViewTouch) image.findViewById(R.id.img)).setImageBitmap(model.bitmap);
                                    }
                                });
                            } else {
                                image.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                        image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        } else {
                            image.post(new Runnable() {
                                @Override
                                public void run() {
                                    image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                    image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                }
                            });
                        }

                        if (json.has("SKUDesc")) {
                            Log.d("Desc", json.getString("SKUDesc"));
                            model.SkuDesc = json.getString("SKUDesc").equals("null") ? "-" : json.getString("SKUDesc");
                            ((TextView) image.findViewById(R.id.img_desc)).setText(json.getString("SKUDesc"));
                        } else {
                            Log.d("Desc", "Desc Not Available");
                            image.findViewById(R.id.img_desc).setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        image.post(new Runnable() {
                            @Override
                            public void run() {
                                image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            };
            t.start();
        } else {
            ((TextView) image.findViewById(R.id.img_desc)).setText(model.SkuDesc);
            image.findViewById(R.id.progressBar).setVisibility(View.GONE);
            image.findViewById(R.id.img).setVisibility(View.VISIBLE);
            ((ImageViewTouch) image.findViewById(R.id.img)).setDisplayType(DisplayType.FIT_IF_BIGGER);
            ((ImageViewTouch) image.findViewById(R.id.img)).setImageBitmap(model.bitmap);
            ((TextView) image.findViewById(R.id.img_desc)).setText(model.SkuDesc);
        }
    }

    private void showDataTakingOrder(final View dialog, final OrderDetailModel model, final Dialog dl) {
        try {
            if (customerModel.CustTop == 0) {
                ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
                ((RadioButton) dialog.findViewById(R.id.credit)).setEnabled(false);
                ;
                ((RadioButton) dialog.findViewById(R.id.cash)).setEnabled(false);
            } else {
                ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
            }

            if (ctrlAppModul.isModul("34")) {
                CrcOrderModel crcOrderModel = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
                if (crcOrderModel != null) {
                    if (model.QTY_B == 0) {
                        ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(crcOrderModel.QTY_L));
                    }
                    if (model.QTY_K == 0) {
                        ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(crcOrderModel.QTY));
                    }
                }
            }

            int visibility = ctrlAppModul.isModul("53") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_1).setVisibility(visibility);
            visibility = ctrlAppModul.isModul("54") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_2).setVisibility(visibility);
            visibility = ctrlAppModul.isModul("55") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_3).setVisibility(visibility);

            AppModul appModul = ctrlAppModul.get("57");
            if (appModul != null) {
                if (appModul.AppModulValue.equals("0")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul.AppModulValue.equals("1")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul.AppModulValue.equals("2")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                } else if (appModul.AppModulValue.equals("3")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                }
            }
            AppModul appModul68 = ctrlAppModul.get("68");
            if (appModul68 != null) {
                if (appModul68.AppModulValue.equals("0")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul68.AppModulValue.equals("1")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                }
            }

            ((EditText) dialog.findViewById(R.id.jml_1)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.jml_2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_1)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_3)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((RadioGroup) dialog.findViewById(R.id.rb_diskon)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    total(dialog, model);
                }
            });
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(model.TOTAL_PRICE));
            ((RadioGroup) dialog.findViewById(R.id.payment_term)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.cash) {
                        dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
                        ((EditText) dialog.findViewById(R.id.lama_credit)).setText(null);
                    } else if (checkedId == R.id.credit) {
                        dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
                        ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
                    }
                }
            });
            if (customerModel.CustTop > 0) {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setKeyListener(null);
            }
            ((Button) dialog.findViewById(R.id.stok_1)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    stok(model, dialog);
                }
            });
            ((Button) dialog.findViewById(R.id.stok_2)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    stok(model, dialog);
                }
            });

            String title = model.skuModel.DISCOUNT_1_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_1)).setText("Discount1 " + title);
            title = model.skuModel.DISCOUNT_2_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_2)).setText("Discount2 " + title);
            title = model.skuModel.DISCOUNT_3_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_3)).setText("Add Discount " + title);

            ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
            ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
            ((Spinner) dialog.findViewById(R.id.SalesProgram)).setAdapter(new SalesProgramAdapter(context, ctrlSalesProgram.listByGropID(model.skuModel.GroupId)));
            if (model.salesProgramModel != null) {
                int index = ((SalesProgramAdapter) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getAdapter()).indexOf(model.salesProgramModel);
                ((Spinner) dialog.findViewById(R.id.SalesProgram)).setSelection(index);
            }
            if (customerModel.CustTop > 0 && (model.PAYMENT_TERM == null || model.PAYMENT_TERM.equals("") || model.PAYMENT_TERM.equals("2"))) {
                ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
                dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
            } else if (model.PAYMENT_TERM != null && model.PAYMENT_TERM.equals("1")) {
                ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
                dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
            }
            ((EditText) dialog.findViewById(R.id.note)).setText(model.note);
            ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
            ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
            ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setText(model.skuModel.SBESAR);
            ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setText(model.skuModel.SKECIL);

            ((TextView) dialog.findViewById(R.id.price_1)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE_L));
            ((TextView) dialog.findViewById(R.id.price_2)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE));
            if (model.LAMA_CREDIT > 0) {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(model.LAMA_CREDIT));
            } else {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
            }

            if (model.QTY_B > 0) {
                ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
            }
            if (model.QTY_K > 0) {
                ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
            }
            if (model.DISCOUNT1 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_1)).setText(String.valueOf(model.DISCOUNT1));
            }
            if (model.DISCOUNT2 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_2)).setText(String.valueOf(model.DISCOUNT2));
            }
            if (model.DISCOUNT3 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_3)).setText(String.valueOf(model.DISCOUNT3));
            }
            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dl.dismiss();
                }
            });
            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                    int QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                    int QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);

                    model.QTY_B = QTY_B;
                    model.QTY_K = QTY_K;
                    model.salesProgramModel = (SalesProgramModel) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getSelectedItem();
                    cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
                    model.DISCOUNT1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
                    model.DISCOUNT2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
                    model.DISCOUNT3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    model.note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();
                    if (((RadioButton) dialog.findViewById(R.id.cash)).isChecked()) {
                        model.PAYMENT_TERM = "1";
                    } else if (((RadioButton) dialog.findViewById(R.id.credit)).isChecked()) {
                        model.PAYMENT_TERM = "2";
                    }
                    cek = ((EditText) dialog.findViewById(R.id.lama_credit)).getText().toString();
                    model.LAMA_CREDIT = Integer.parseInt(cek.equals("") ? "0" : cek);
                    model.PRICE_B = model.skuModel.HET_PRICE_L;
                    model.PRICE_K = model.skuModel.HET_PRICE;
                    model.TOTAL_PRICE = totalValue(dialog, model);

                    //tambahan untuk tipe diskon
                    if (((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked()) {
                        model.DiscountType = "1";
                    } else if (((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).isChecked()) {
                        model.DiscountType = "0";
                    }


//					adapter.notifyDataSetChanged();
                    adapterMhs.notifyDataSetChanged();
                    adapterNonMhs.notifyDataSetChanged();

                    loadOrder();

                    calculateAmount();
                    dl.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stok(final OrderDetailModel model, final View dialog) {
        server.addConnectionEvent(new ConnectionEvent() {
            @Override
            public void messageServer(String respon) {
                try {
                    JSONObject json = new JSONArray(respon).getJSONObject(0);
                    model.STOK_B = json.getInt("StockQty_L");
                    model.STOK_K = json.getInt("StockQty");
                    ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
                    ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
                } catch (Exception e) {
                    error(e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Util.showDialogError(context, error);
            }
        });
        server.setUrl(Util.getServerUrl(context) + "getSKUstock/SalesId/" + UserModel.getInstance(context).SalesId + "/SKUId/" + model.skuModel.SKUId);
        server.requestJSONObject(null);
    }

    private void initAdapter() {
        try {
            adapterMhs = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            adapterNonMhs = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapterAll.getGroup(i);
//                populateSKUByGroup();
                String sql = "select count(*) jml from SKu a "
                        + "inner join sku_config b on a.SKUId=b.skuid and b.code=1 "
                        + "inner join sku_config c on a.SKUId=c.skuid and c.code=4 "
                        + "where a.GroupId='" + groupSKUModel.GroupId + "' "
                        + "and b.value='1' "
                        + "and c.value='" + customerTypeModel.CustomerTypeId + "'";
                Cursor cursor = dbSku.query(sql);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) {
                        adapterMhs.addHeader(groupSKUModel.copy());
                    }
                }
//				sql = "select count(*) jml from SKu a left join sku_config b on a.SKUId=b.skuid "
//						+ "where a.GroupId='"+groupSKUModel.GroupId+"' and b.code=1 and b.value='0'";
                sql = "select count(*) jml from SKu a "
                        + "inner join sku_config b on a.SKUId=b.skuid and b.code=1 "
                        + "inner join sku_config c on a.SKUId=c.skuid and c.code=4 "
                        + "where a.GroupId='" + groupSKUModel.GroupId + "' "
                        + "and b.value='0' "
                        + "and c.value='" + customerTypeModel.CustomerTypeId + "'";
                cursor = dbSku.query(sql);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) {
                        adapterNonMhs.addHeader(groupSKUModel.copy());
                    }
                }
            }

            /*for (int j = 0; j < adapterMhs.getGroupCount(); j++) {
                populateSKUByGroup(2, adapterMhs.getGroup(j), adapterMhs);
            }

            for (int j = 0; j < adapterNonMhs.getGroupCount(); j++) {
                populateSKUByGroup(3, adapterNonMhs.getGroup(j), adapterNonMhs);
            }
*/
            loadOrder();
            calculateAmount();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    /*public void setAdapter(OrderDetailAdapter adapter) {
        this.adapter = adapter;
    }*/

    private void populateSKUByGroup(int jenis, GroupSKUModel group, OrderDetailAdapter adapter) {
        try {
            List<OrderDetailModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroupTakingOrder2(group, customerModel.CustGroupID, jenis, customerTypeModel.CustomerTypeId);
                for (SKUModel skuModel : listSKU) {
                    OrderDetailModel orderDetailModel = new OrderDetailModel(skuModel);
                    if (list == null || list.indexOf(orderDetailModel) == -1) {

                        com.bhn.sadix.Data.TakingOrderModel takingOrderModel = realmUI.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                .equalTo("CommonID", commonModel.CommonID).equalTo("SKUId", skuModel.SKUId).findFirst();

                        /*TakingOrderModel
                                takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, skuModel.SKUId);*/
                        if (takingOrderModel != null) {
                            orderDetailModel.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                            orderDetailModel.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                            orderDetailModel.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                            orderDetailModel.note = takingOrderModel.note;
                            orderDetailModel.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                            orderDetailModel.QTY_B = takingOrderModel.QTY_B;
                            orderDetailModel.QTY_K = takingOrderModel.QTY_K;
                            String SalesProgramId = takingOrderModel.SalesProgramId;
                            if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                orderDetailModel.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                            }
                            orderDetailModel.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                            orderDetailModel.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                            orderDetailModel.PRICE_B = takingOrderModel.PRICE_B;
                            orderDetailModel.PRICE_K = takingOrderModel.PRICE_K;
                            orderDetailModel.RandomID = takingOrderModel.RandomID;
                            orderDetailModel.DiscountType = takingOrderModel.DiscountType;
                        }
                        Cursor cursor = dbSku.query("select * from sku_config where skuid='" + skuModel.SKUId + "' and code > 1");
                        while (cursor.moveToNext()) {
                            if (cursor.getInt(1) == 2) {
                                orderDetailModel.Promo = cursor.getInt(2);
                            } else if (cursor.getInt(1) == 3) {
                                orderDetailModel.NewProduct = cursor.getInt(2);
                            } else if (cursor.getInt(1) == 5) {
                                orderDetailModel.KeteranganPromo = cursor.getString(2);
                            } else if (cursor.getInt(1) == 6) {
                                orderDetailModel.skuModel.HET_PRICE = Double.parseDouble(cursor.getString(2));
                            }
                        }
                        adapter.addChild(group, orderDetailModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 3;

        @Override
        public Object instantiateItem(View collection, final int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);
            if (position == 0) {
                vMhs = v;
                vMhs.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vMhs.findViewById(R.id.list)).setAdapter(adapterMhs);
                ((ExpandableListView) vMhs.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                ((ExpandableListView) vMhs.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(2, adapter.getGroup(groupPosition), adapter);
                        return false;
                    }
                });
            } else if (position == 1) {
                vNonMhs = v;
                vNonMhs.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vNonMhs.findViewById(R.id.list)).setAdapter(adapterNonMhs);
                ((ExpandableListView) vNonMhs.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                ((ExpandableListView) vNonMhs.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(3, adapter.getGroup(groupPosition), adapter);
                        return false;
                    }
                });
            } else if (position == 2) {
                vOrder = v;
                vOrder.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
