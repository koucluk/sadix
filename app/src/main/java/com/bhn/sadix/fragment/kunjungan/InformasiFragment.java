package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlInformasi;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.InformasiModel;
import com.bhn.sadix.widget.InformationWidget;

import java.util.List;


public class InformasiFragment extends Fragment {
    private CtrlInformasi ctrlInformasi;
    private View view = null;
    private CustomerModel customerModel;
    private KunjunganListener listener;

    public static InformasiFragment newInstance() {
        Bundle args = new Bundle();

        InformasiFragment fragment = new InformasiFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlInformasi = new CtrlInformasi(getActivity());
        customerModel = listener.getCustomerModel();
    }

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ctrlInformasi = new CtrlInformasi(getActivity());
        customerModel = listener.getCustomerModel();

        List<InformasiModel> list = ctrlInformasi.list(customerModel);
        for (InformasiModel informasiModel : list) {
            ((LinearLayout) view.findViewById(R.id.container)).addView(new InformationWidget(getActivity(), informasiModel));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_informasi, container, false);
        view.findViewById(R.id.toolbar).setVisibility(View.GONE);
//        List<InformasiModel> list = ctrlInformasi.list();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
