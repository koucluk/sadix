package com.bhn.sadix.fragment.kunjungan;


import android.support.v4.app.Fragment;

import com.bhn.sadix.connection.ConnectionEvent;

public abstract class EditCustomerFragment extends Fragment implements ConnectionEvent {
    /*private View view = null;
    private ConnectionServer server;
	private CustomerModel customerModel;
	private CtrlCustomer ctrlCustomer;
	private CtrlCustomerType ctrlCustomerType;
	private Context context;
	private CtrlAppModul ctrlAppModul;
	private LocationManager locationManager;
	private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
        	location = loc;
        	if(view != null) {
	            ((TextView)view.findViewById(R.id.Lokasi)).setText("Longitude / Latitude : "+String.format("%1$s", location.getLongitude())+
	            		" / "+String.format("%1$s", location.getLatitude()));
        	}
        }
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        public void onProviderEnabled(String provider) {}
        public void onProviderDisabled(String provider) {}
    };
//	public EditCustomerFragment(Context context) {
//		this.context = context;
//		ctrlCustomer = new CtrlCustomer(context);
//		ctrlAppModul = new CtrlAppModul(context);
//		ctrlCustomerType = new CtrlCustomerType(context);
//		server = new ConnectionServer(context);
//		server.addConnectionEvent(this);
//	    locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//	    try {
//			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
//			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//		} catch (Exception e) {
//			Util.Toast(context, e.getMessage());
//		}
//	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.context = getActivity();
		ctrlCustomer = new CtrlCustomer(context);
		ctrlAppModul = new CtrlAppModul(context);
		ctrlCustomerType = new CtrlCustomerType(context);
		server = new ConnectionServer(context);
		server.addConnectionEvent(this);
	    locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	    try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		} catch (Exception e) {
			Util.Toast(context, e.getMessage());
		}
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_edit_customer, container, false);
        ((ImageButton)view.findViewById(R.id.btn_add_photo)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addPhoto();
			}
		});
        ((ImageButton)view.findViewById(R.id.btn_save)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				save();
			}
		});
        ((ImageButton)view.findViewById(R.id.btn_tgl)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popupBirthDate();
			}
		});
        ((Spinner)view.findViewById(R.id.CustomerTypeId)).setAdapter(new ComboBoxAdapter(context, ctrlCustomerType.listComboBox()));
        AppModul modul = ctrlAppModul.get("51");
//        if(ctrlAppModul.isModul("51")) {
//        	((ImageButton)view.findViewById(R.id.btn_save)).setVisibility(View.VISIBLE);
//        } else {
//        	((ImageButton)view.findViewById(R.id.btn_save)).setVisibility(View.GONE);
//        }
        if(modul != null) {
	        if(modul.AppModulValue.equals("1")) {
	        	((ImageButton)view.findViewById(R.id.btn_save)).setVisibility(View.VISIBLE);
	        } else if(modul.AppModulValue.equals("0")) {
	        	((ImageButton)view.findViewById(R.id.btn_save)).setVisibility(View.GONE);
	        	((ImageButton)view.findViewById(R.id.btn_add_photo)).setVisibility(View.GONE);
	        	edittableTextBox(false);
	        } else if(modul.AppModulValue.equals("2")) {
	        	((ImageButton)view.findViewById(R.id.btn_save)).setVisibility(View.VISIBLE);
	        	edittableTextBox(false);
	        }
        }
        viewCustomer();
        return view;
    }
	private void edittableTextBox(boolean isedit) {
		try {
			if(!isedit) {
				((EditText)view.findViewById(R.id.CustomerName)).setKeyListener(null);
				((EditText)view.findViewById(R.id.CustomerAddress)).setKeyListener(null);
				((EditText)view.findViewById(R.id.CustomerPhone)).setKeyListener(null);
				((EditText)view.findViewById(R.id.CustomerFax)).setKeyListener(null);
				((EditText)view.findViewById(R.id.CustomerNPWP)).setKeyListener(null);
				((EditText)view.findViewById(R.id.CustomerEmail)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerName)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerAddress)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerPhone)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerBirthPlace)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerBirthDate)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerIdNo)).setKeyListener(null);
				((EditText)view.findViewById(R.id.OwnerEmail)).setKeyListener(null);
				((Spinner)view.findViewById(R.id.CustomerTypeId)).setEnabled(isedit);
				((ImageButton)view.findViewById(R.id.btn_tgl)).setOnClickListener(null);
			}
		} catch (Exception e) {
		}
	}
	private void popupBirthDate() {
		try {
//			String tglLahir = ((EditText)findViewById(R.id.OwnerBirthDate)).getText().toString();
			final Calendar cal = Calendar.getInstance();
//			if(tglLahir.trim().equals("")) {
//				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
//			} else {
//				String[] arr = tglLahir.split("-");
//				cal.set(Integer.parseInt(arr[0]), Integer.parseInt(arr[1])-1, Integer.parseInt(arr[2]));
//			}
			final DatePickerDialog tglDialog = new DatePickerDialog(context, new OnDateSetListener() {
		    	@Override
				public void onDateSet(DatePicker v, int year, int month, int day) {
					Calendar calData = Calendar.getInstance();
					calData.set(year, month, day);
				    ((EditText)view.findViewById(R.id.OwnerBirthDate)).setText(Util.dateFormatBirthDate.format(calData.getTime()));
				}
			}, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			tglDialog.show();
		} catch (Exception e) {
			Util.Toast(context, e.getMessage());
		}
	}
	private void viewCustomer() {
		if(view != null && customerModel != null) {
			((EditText)view.findViewById(R.id.CustomerName)).setText(customerModel.CustomerName);
			((EditText)view.findViewById(R.id.CustomerAddress)).setText(customerModel.CustomerAddress);
			((EditText)view.findViewById(R.id.CustomerEmail)).setText(customerModel.CustomerEmail);
			((EditText)view.findViewById(R.id.CustomerFax)).setText(customerModel.CustomerFax);
			((EditText)view.findViewById(R.id.CustomerNPWP)).setText(customerModel.CustomerNPWP);
			((EditText)view.findViewById(R.id.CustomerPhone)).setText(customerModel.CustomerPhone);
			((EditText)view.findViewById(R.id.OwnerAddress)).setText(customerModel.OwnerAddress);
			((EditText)view.findViewById(R.id.OwnerEmail)).setText(customerModel.OwnerEmail);
			((EditText)view.findViewById(R.id.OwnerName)).setText(customerModel.OwnerName);
			((EditText)view.findViewById(R.id.OwnerPhone)).setText(customerModel.OwnerPhone);
			((EditText)view.findViewById(R.id.OwnerBirthPlace)).setText(customerModel.OwnerBirthPlace);
			((EditText)view.findViewById(R.id.OwnerBirthDate)).setText(customerModel.OwnerBirthDate);
			((EditText)view.findViewById(R.id.OwnerIdNo)).setText(customerModel.OwnerIdNo);
			int index = ((ComboBoxAdapter)((Spinner)view.findViewById(R.id.CustomerTypeId)).getAdapter()).indexOf(new ComboBoxModel(customerModel.CustomerTypeId));
			((Spinner)view.findViewById(R.id.CustomerTypeId)).setSelection(index);
		}
	}
	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}
	public void save() {
		if(isValid() == false) {
			return;
		}
		LinearLayout panel_photo = ((LinearLayout)view.findViewById(R.id.panel_photo));
		if(panel_photo.getChildCount() == 0) {
			Util.showDialogInfo(context, "Photo wajib diisi!");
			return;
		}
		for (int i = 0; i < panel_photo.getChildCount(); i++) {
			View view = panel_photo.getChildAt(i);
			if(view instanceof ItemPhotoCustomerWidget) {
				ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
				if(item.isAmbilPhoto() == false) {
					Util.showDialogInfo(context, "Photo belum diambil!");
					return;
				}
			}
		}
		try {
			customerModel.CustomerAddress = ((EditText)view.findViewById(R.id.CustomerAddress)).getText().toString();
			customerModel.CustomerEmail = ((EditText)view.findViewById(R.id.CustomerEmail)).getText().toString();
			customerModel.CustomerFax = ((EditText)view.findViewById(R.id.CustomerFax)).getText().toString();
			customerModel.CustomerName = ((EditText)view.findViewById(R.id.CustomerName)).getText().toString();
			customerModel.CustomerNPWP = ((EditText)view.findViewById(R.id.CustomerNPWP)).getText().toString();
			customerModel.CustomerPhone = ((EditText)view.findViewById(R.id.CustomerPhone)).getText().toString();
			customerModel.CustomerTypeId = ((ComboBoxModel)((Spinner)view.findViewById(R.id.CustomerTypeId)).getSelectedItem()).value;
			customerModel.GeoLat = location == null ? "0" : String.format("%1$s", location.getLatitude());
			customerModel.GeoLong = location == null ? "0" : String.format("%1$s", location.getLongitude());
			customerModel.isEdit = "1";
			customerModel.OwnerAddress = ((EditText)view.findViewById(R.id.OwnerAddress)).getText().toString();
			customerModel.OwnerEmail = ((EditText)view.findViewById(R.id.OwnerEmail)).getText().toString();
			customerModel.OwnerName = ((EditText)view.findViewById(R.id.OwnerName)).getText().toString();
			customerModel.OwnerPhone = ((EditText)view.findViewById(R.id.OwnerPhone)).getText().toString();
			customerModel.OwnerBirthPlace = ((EditText)view.findViewById(R.id.OwnerBirthPlace)).getText().toString();
			customerModel.OwnerBirthDate = ((EditText)view.findViewById(R.id.OwnerBirthDate)).getText().toString();
			customerModel.OwnerIdNo = ((EditText)view.findViewById(R.id.OwnerIdNo)).getText().toString();
			
			JSONObject json = new JSONObject();
			json.put("CustomerId", Integer.parseInt(customerModel.CustId));
			json.put("CustomerName", customerModel.CustomerName);
			json.put("CustomerAddress", customerModel.CustomerAddress);
			json.put("CustomerEmail", customerModel.CustomerEmail);
			json.put("CustomerPhone", customerModel.CustomerPhone);
			json.put("CustomerFax", customerModel.CustomerFax);
			json.put("CustomerNpwp", customerModel.CustomerNPWP);
			json.put("OwnerName", customerModel.OwnerName);
			json.put("OwnerAddress", customerModel.OwnerAddress);
			json.put("OwnerEmail", customerModel.OwnerEmail);
			json.put("OwnerPhone", customerModel.OwnerPhone);
			json.put("OwnerBirthPlace", customerModel.OwnerBirthPlace);
			json.put("OwnerBirthDate", customerModel.OwnerBirthDate);
			json.put("OwnerIdNo", customerModel.OwnerIdNo);
			json.put("CustomerTypeId", Integer.parseInt(customerModel.CustomerTypeId));
			json.put("GeoLat", Double.parseDouble(customerModel.GeoLat));
			json.put("GeoLong", Double.parseDouble(customerModel.GeoLong));
			json.put("StatusId", 2);
			json.put("SalesId", Integer.parseInt(UserModel.getInstance(context).SalesId));
			json.put("Picture", "xxx.jpg");
			
			server.setUrl(Util.getServerUrl(context) + "postcustdata");
			server.requestJSONObject(json);
		} catch (Exception e) {
			error(e.getMessage());
		}
	}
	public abstract void addPhoto(ItemPhotoCustomerWidget widget);
	public abstract void selesai();
	public void remove(ItemPhotoCustomerWidget widget) {
		((LinearLayout)view.findViewById(R.id.panel_photo)).removeView(widget);
	}
	private void addPhoto() {
		ItemPhotoCustomerWidget widget = new ItemPhotoCustomerWidget(getActivity());
		((LinearLayout)view.findViewById(R.id.panel_photo)).addView(widget);
		addPhoto(widget);
		
//		ScrollView mScrollView = (ScrollView) view.findViewById(R.id.scroll);
//		mScrollView.scrollTo(0, mScrollView.getBottom());
		
		widget.takePicture();
	}
	@Override
	public void error(String error) {
		Util.showDialogError(context, error);
		ctrlCustomer.update(customerModel);
		new KirimGambar(context, true).execute();
//		selesai();
	}
	@Override
	public void messageServer(String respon) {
		try {
			JSONObject json = new JSONObject(respon);
			if(json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
				customerModel.isEdit = "0";
				ctrlCustomer.update(customerModel);
				Util.Toast(context, json.getString("Message"));
//				selesai();
				new KirimGambar(context, false).execute();
			} else {
				Util.showDialogInfo(context, json.getString("Message"));
				ctrlCustomer.update(customerModel);
				new KirimGambar(context, true).execute();
//				selesai();
			}
		} catch (Exception e) {
//			error(e.getMessage());
			Util.showDialogError(context, e.getMessage());
			ctrlCustomer.update(customerModel);
			new KirimGambar(context, true).execute();
//			selesai();
		}
	}
	class KirimGambar extends AsyncTask<String,Void,Void> {
		private ProgressDialog progressDialog;
		private boolean isSave;
		public KirimGambar(Context context, boolean isSave) {
			this.isSave = isSave;
		    progressDialog = new ProgressDialog(context);
		    progressDialog.setCancelable(false);
			progressDialog.setMessage("Kirim gambar...");
			progressDialog.show();
		}
		@Override
		protected Void doInBackground(String... params) {
			context.runOn
			LinearLayout layout = (LinearLayout)view.findViewById(R.id.panel_photo);
			for (int i = 0; i < layout.getChildCount(); i++) {
				View view = layout.getChildAt(i);
				if(view instanceof ItemPhotoCustomerWidget) {
					ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
					if(isSave) {
						item.saveKeLokal();
					} else {
						String respon = ConnectionServer.requestJSONObjectNonThread(item.getJsonSendData(), Util.getServerUrl(context)+"photodata", false);
						try {
							JSONObject json = new JSONObject(respon);
							if((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
								item.saveKeLokal();
							}
						} catch (Exception e) {
						}
					}
				}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void respon) {
			progressDialog.dismiss();
			selesai();
		}
	}
	private boolean isValid() {
		String valid = ((EditText)view.findViewById(R.id.CustomerName)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Customer Name harus diisi!");
			return false;
		}
		valid = ((EditText)view.findViewById(R.id.CustomerAddress)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Customer Address harus diisi!");
			return false;
		}
		valid = ((EditText)view.findViewById(R.id.CustomerPhone)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Customer Phone harus diisi!");
			return false;
		}
		valid = ((EditText)view.findViewById(R.id.OwnerName)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Owner Name harus diisi!");
			return false;
		}
		valid = ((EditText)view.findViewById(R.id.OwnerAddress)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Owner Address harus diisi!");
			return false;
		}
		valid = ((EditText)view.findViewById(R.id.OwnerPhone)).getText().toString();
		if(valid.trim().equals("")) {
			Util.showDialogInfo(context, "Owner Phone harus diisi!");
			return false;
		}
		return true;
	}*/

}
