package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ViewPagerAdapter;
import com.bhn.sadix.model.CustomerModel;
import com.makeramen.segmented.SegmentedRadioGroup;

public class ReportCRCFragment extends Fragment {
	private View view;
	private Context context;
	private CustomerModel customerModel;
	private ViewPagerAdapter adapter;
//	public ReportCRCFragment(Context context, CustomerModel customerModel) {
//		this.context = context;
//		this.customerModel = customerModel;
//		adapter = new ViewPagerAdapter(this.context);
//		adapter.add(new CRCPagerView());
//		adapter.add(new CRCPagerView());
//		adapter.add(new CRCPagerView());
//	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.context = getActivity();
		adapter = new ViewPagerAdapter(this.context);
		adapter.add(new CRCPagerView());
		adapter.add(new CRCPagerView());
		adapter.add(new CRCPagerView());
	}
	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjunga_report_crc, container, false);
		((SegmentedRadioGroup)view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(checkedId == R.id.tab_last_order) {
					((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(0);
				} else if(checkedId == R.id.tab_last_stcok) {
					((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(1);
				} else if(checkedId == R.id.tab_crc) {
					((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(2);
				}
			}
		});
        ((RadioButton)view.findViewById(R.id.tab_last_order)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(0);
			}
		});
        ((RadioButton)view.findViewById(R.id.tab_last_stcok)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(1);
			}
		});
        ((RadioButton)view.findViewById(R.id.tab_crc)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ViewPager)view.findViewById(R.id.pager)).setCurrentItem(2);
			}
		});
        ((ViewPager)view.findViewById(R.id.pager)).setAdapter(adapter);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if(position == 0) {
                	((SegmentedRadioGroup)view.findViewById(R.id.tab)).check(R.id.tab_last_order);
                } else if(position == 1) {
                	((SegmentedRadioGroup)view.findViewById(R.id.tab)).check(R.id.tab_last_stcok);
                } else if(position == 2) {
                	((SegmentedRadioGroup)view.findViewById(R.id.tab)).check(R.id.tab_crc);
                }
            }
        };
        ((ViewPager)view.findViewById(R.id.pager)).setOnPageChangeListener(ViewPagerListener);
        return view;
    }
	
	public class CRCPagerView implements ViewPagerAdapter.PageItem {
		private View view;
		@Override
		public int getLayout() {
			return R.layout.item_fragment_report_crc;
		}
		@Override
		public void onStart() {
		}
		@Override
		public void onPause() {
		}
		@Override
		public void setView(View view) {
			this.view = view;
		}
		@Override
		public View getView() {
			return view;
		}
	}
}
