package com.bhn.sadix.fragment.kunjungan;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.CollectionPayAdapter;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlCollector;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;

//import com.bhn.sadix.model.CommonModel;

public class CollectionFragment extends Fragment implements ConnectionEvent {
    private View view = null;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    private ViewPager mPager;
    private Context context;
    private ViewPagerAdapterCollector pagerAdapter;
    //    public CollectorModel collectorModelNotPayment;
    public CollectionModel collectionModelNotPayment;
    private DatePickerDialog birthDateDialog;
    private View vPayment;
    private View vNotPayment;
    private View vInvoice;
    private CtrlConfig ctrlConfig;
    private CustomerModel customerModel;
    private CommonModel commonModel;
    private CtrlCollector ctrlCollector;
    private ConnectionServer server;
    int jenis = 0;
    private Realm realmUI;

    public CollectionPayAdapter payAdapter;
    private DbMasterHelper dbMasterHelper;

    private KunjunganListener listener;

    public static CollectionFragment newInstance() {
        CollectionFragment fragment = new CollectionFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlConfig = new CtrlConfig(context);
        ctrlCollector = new CtrlCollector(context);
        dbMasterHelper = new DbMasterHelper(this.context);
        server = new ConnectionServer(getActivity());
        /*payAdapter = new CollectionPayAdapter(context, null);
        payAdapter.setNotifyOnChange(true);*/
        server.addConnectionEvent(this);
//        commonModel = listener.getCommonModel();
        realmUI = Realm.getDefaultInstance();

        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();

        customerModel = listener.getCustomerModel();
//        collectorModelNotPayment = new CollectorModel(commonModel.CommonID, "2");
    }

    //	public CollectionFragment(Context context, CommonModel commonModel) {
//		this.context = context;
//		this.commonModel = commonModel;
//		ctrlConfig = new CtrlConfig(context);
//		ctrlCollector = new CtrlCollector(context);
////		lstPay = new ArrayList<CollectorModel>();
//		payAdapter = new CollectionPayAdapter(context, new ArrayList<CollectorModel>());
//		payAdapter.setNotifyOnChange(true);
//		collectorModelNotPayment = new CollectorModel(commonModel.CommonID, "2");
//		dbMasterHelper = new DbMasterHelper(this.context);
//	}
    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public void setCommonModel(CommonModel commonModel) {
        this.commonModel = commonModel;
    }*/

    //	public void setCollectorModelPayment(CollectorModel collectorModelPayment) {
//		this.collectorModelPayment = collectorModelPayment;
//	}
//	public void setCollectorModelNotPayment(CollectorModel collectorModelNotPayment) {
//		this.collectorModelNotPayment = collectorModelNotPayment;
//	}
    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
        setData();
    }

    public void setData() {
        try {
//			if(vPayment != null) {
//				collectorModelPayment.data1 = ((EditText)vPayment.findViewById(R.id.data1)).getText().toString();
//				collectorModelPayment.data2 = ((ComboBoxModel)((Spinner)vPayment.findViewById(R.id.data2)).getSelectedItem()).value;
//				collectorModelPayment.data3 = ((EditText)vPayment.findViewById(R.id.data3)).getText().toString();

//				lstPay.clear();
//				LinearLayout panel_pay = (LinearLayout)vPayment.findViewById(R.id.panel_pay);
//				for (int i = 0; i < panel_pay.getChildCount(); i++) {
//					ItemCollectionPayWidget widget = (ItemCollectionPayWidget) panel_pay.getChildAt(i);
//					widget.setData();
//					lstPay.add(widget.getModel());
//				}
//			}
            if (vNotPayment != null) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        collectionModelNotPayment.data1 = ((ComboBoxModel) ((Spinner) vNotPayment.findViewById(R.id.data1)).getSelectedItem()).value;
                        collectionModelNotPayment.data2 = ((EditText) vNotPayment.findViewById(R.id.data2)).getText().toString();
                        collectionModelNotPayment.data3 = ((EditText) vNotPayment.findViewById(R.id.data3)).getText().toString();
                    }
                });
//				lstPay.add(collectorModelNotPayment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showTanggal() {
        try {
            String tglLahir = ((EditText) vNotPayment.findViewById(R.id.data2)).getText().toString();
            final Calendar cal = Calendar.getInstance();
            if (tglLahir.trim().equals("")) {
                cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
            } else {
                String[] arr = tglLahir.split("-");
                cal.set(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]) - 1, Integer.parseInt(arr[2]));
            }
            birthDateDialog = new DatePickerDialog(context, new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    ((EditText) vNotPayment.findViewById(R.id.data2)).setText(format.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

            birthDateDialog.show();
        } catch (Exception e) {
            Util.Toast(context, e.getMessage());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_collection, container, false);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_pay) {
                    mPager.setCurrentItem(1);
                } else if (checkedId == R.id.tab_not_pay) {
                    mPager.setCurrentItem(2);
                } else if (checkedId == R.id.tab_invoice) {
                    mPager.setCurrentItem(0);
                }
            }
        });
        if (customerModel != null) {
            ((TextView) view.findViewById(R.id.CustomerCrLimit)).setText(Util.numberFormat.format(customerModel.CustomerCrLimit));
            ((TextView) view.findViewById(R.id.CustomerCrBalance)).setText(Util.numberFormat.format(customerModel.CustomerCrLimit - customerModel.CustomerCrBalance));
        }

        view.findViewById(R.id.icon_refresh).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh();
            }
        });

        mPager = (ViewPager) view.findViewById(R.id.pager_collector);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 1) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_pay);
                } else if (position == 2) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_not_pay);
                } else if (position == 0) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_invoice);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        commonModel = listener.getCommonModel();
        customerModel = listener.getCustomerModel();

        if (listener.getCollectionAdapter() == null) {
            ((KunjunganActivity) getActivity()).loadCollector();
        }
    }

    public void loadAdapter() {
        payAdapter = listener.getCollectionAdapter();

        if (listener.getCollectionAdapter() == null) {
            ((KunjunganActivity) getActivity()).loadCollector();
        } else {
            payAdapter.setNotifyOnChange(true);

            pagerAdapter = new ViewPagerAdapterCollector();
            mPager.setAdapter(pagerAdapter);

            refresh();
        }
    }

    private void refresh() {
        jenis = 1;
        server.setUrl(Util.getServerUrl(getActivity()) + "getoutletbalance/OutletId/" + customerModel.CustId);
        server.requestJSONObject(null);
    }

    private void viewPayCollection(final CollectionModel model, final int jenis) {
//    	try {
//			if(vPayment != null) {
//				((LinearLayout)vPayment.findViewById(R.id.panel_pay)).addView(new ItemCollectionPayWidget(context, new CollectorModel(commonModel.CommonID, "1")));
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_collection_pay);

        ((Spinner) dialog.findViewById(R.id.data2)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("PAYMENT_METHOD")));
        ((EditText) dialog.findViewById(R.id.data1)).setText(model.data1);
        int index = ((ComboBoxAdapter) ((Spinner) dialog.findViewById(R.id.data2)).getAdapter()).indexOf(new ComboBoxModel(model.data2));
        ((Spinner) dialog.findViewById(R.id.data2)).setSelection(index);
        ((EditText) dialog.findViewById(R.id.data3)).setText(model.data3);
        ((EditText) dialog.findViewById(R.id.data4)).setText(model.data4);
        ((EditText) dialog.findViewById(R.id.data5)).setText(model.data5);
        ((EditText) dialog.findViewById(R.id.data6)).setText(model.data6);

        dialog.findViewById(R.id.btn_tgl).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTanggal(dialog);
            }
        });

        ((Spinner) dialog.findViewById(R.id.data2)).setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ComboBoxModel model = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.data2)).getSelectedItem();
                if (model != null) {
                    if (model.text.equals("GIRO")) {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.nomor_giro).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.data5).setVisibility(View.VISIBLE);
                        ((TextView) dialog.findViewById(R.id.tgl_lbl)).setText("Tanggal Jatuh Tempo");
                    } else if (model.text.equals("TRANSFER")) {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.nomor_giro).setVisibility(View.GONE);
                        dialog.findViewById(R.id.data5).setVisibility(View.GONE);
                        ((TextView) dialog.findViewById(R.id.tgl_lbl)).setText("Tanggal Transfer");
                    } else {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String validate = ((EditText) dialog.findViewById(R.id.data1)).getText().toString();
                if (validate.equals("") || validate.equals("0")) {
                    Util.showDialogInfo(context, "Today Payment tidak boleh kosong atau 0!");
                    return;
                }
                final ComboBoxModel combo = ((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.data2)).getSelectedItem());
                if (combo.text.equals("GIRO")) {
                    validate = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(context, "Tanggal jatuh tempo harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data5)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(context, "Nomor Giro harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(context, "Bank harus diisi!");
                        return;
                    }
                } else if (combo.text.equals("TRANSFER")) {
                    validate = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(context, "Tanggal Transfer harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(context, "Bank harus diisi!");
                        return;
                    }
                }
                /*realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {*/
                if (jenis == 0) {
                    payAdapter.add(model);
                }

                model.data1 = ((EditText) dialog.findViewById(R.id.data1)).getText().toString();
                if (combo.text.equals("GIRO")) {
                }
                model.data2 = combo.value;
                model.data3 = ((EditText) dialog.findViewById(R.id.data3)).getText().toString();
                model.data4 = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                model.data5 = ((EditText) dialog.findViewById(R.id.data5)).getText().toString();
                model.data6 = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();

                model.payMethod = combo.text;
                saveToRealm(model, jenis);
                   /* }
                });*/

                /*if (jenis == 0) {
                    payAdapter.add(model);
                }*/

//                payAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void saveToRealm(final CollectionModel model, final int avail) {
        Log.d("Model-RandomID", model.RandomID);
        /*commonModel.collectionModel.add(model);*/
        boolean needToSave = false;

        /*if (realmUI.where(CollectionModel.class)
                .equalTo("CommonID", commonModel.CommonID)
                .equalTo("RandomID", model.RandomID)
                .findFirst() == null) {
            Log.d("model added", "true");
            needToSave = true;
        }

        final boolean finalNeedToSave = needToSave;*/

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmUI.copyToRealmOrUpdate(model);

                if (avail == 0) {
                    Log.d("model added", "true");
                    commonModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
                    commonModel.collectionModel.add(model);
                }
            }
        });

    }

    private void popupTanggal(final Dialog dialog) {
        try {
            final Calendar cal = Calendar.getInstance();
            final DatePickerDialog tglDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    ((EditText) dialog.findViewById(R.id.data4)).setText(Util.dateFormatBirthDate.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            tglDialog.show();
        } catch (Exception e) {
            Util.Toast(getActivity(), e.getMessage());
        }
    }

    @Override
    public void error(String error) {

    }

    @Override
    public void messageServer(String respon) {
        android.util.Log.e("respon", "" + respon);
        try {
            if (jenis == 1) {
                JSONObject json = new JSONObject(respon);
                /*Double CreditLimit = json.getDouble("OutletCreditLimit");
                Double SisaCreditBalance = json.getDouble("OutletCreditUsed");
                ((TextView) view.findViewById(R.id.CreditLimit)).setText(Util.numberFormat.format(CreditLimit));
                ((TextView) view.findViewById(R.id.SisaCreditBalance)).setText(Util.numberFormat.format(SisaCreditBalance));
                ((TextView) view.findViewById(R.id.CreditBalance)).setText(Util.numberFormat.format(CreditLimit - SisaCreditBalance));
                JSONArray Notif = json.getJSONArray("Notif");*/
                /*listNotification.clear();
                for (int i = 0; i < Notif.length(); i++) {
                    JSONObject notifMsg = Notif.getJSONObject(i);
                    listNotification.add(new ComboBoxModel(notifMsg.getString("NotifId"), notifMsg.getString("NotifValue")));
                }*/

                LayoutInflater inflater = LayoutInflater.from(getActivity());
                JSONArray InvItem = json.getJSONArray("InvItem");
                TableLayout tbl_invoice = (TableLayout) view.findViewById(R.id.tabel);
                for (int i = (tbl_invoice.getChildCount() - 1); i > 0; i--) {
                    tbl_invoice.removeViewAt(i);
                }

                int total_invoice = 0;
                for (int i = 0; i < InvItem.length(); i++) {
                    JSONObject Inv = InvItem.getJSONObject(i);
                    TableRow row = (TableRow) inflater.inflate(R.layout.row_collection_tirta, null);
                    ((TextView) row.findViewById(R.id.row_invoiceno)).setText(Inv.getString("InvNo"));
                    ((TextView) row.findViewById(R.id.row_dudate)).setText(Inv.getString("InvDu"));
                    ((TextView) row.findViewById(R.id.row_amount)).setText(Util.numberFormat.format(Inv.getDouble("InvAm")));
                    double sisa = Inv.getDouble("InvAm") - Inv.getDouble("InvRest");
                    ((TextView) row.findViewById(R.id.row_sisa)).setText(Util.numberFormat.format(sisa));
                    tbl_invoice.addView(row);
                    total_invoice += sisa;
                }
//                ((TextView) view.findViewById(R.id.total_invoice)).setText("Total : " + Util.numberFormat.format(total_invoice));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //	public List<CollectorModel> getLstPay() {
//		return lstPay;
//	}
    class ViewPagerAdapterCollector extends PagerAdapter {
        final int PAGE_COUNT = 3;

        @Override
        public Object instantiateItem(View collection, int position) {
            View v = null;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (position == 1) {
                vPayment = inflater.inflate(R.layout.fragment_main_collection_pay, null);

//	    		((Spinner)vPayment.findViewById(R.id.data2)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("PAYMENT_METHOD")));
//	    		((EditText)vPayment.findViewById(R.id.data1)).setText(collectorModelPayment.data1);
//	    		int index = ((ComboBoxAdapter)((Spinner)vPayment.findViewById(R.id.data2)).getAdapter()).indexOf(new ComboBoxModel(collectorModelPayment.data2));
//	    		((Spinner)vPayment.findViewById(R.id.data2)).setSelection(index);
//	    		((EditText)vPayment.findViewById(R.id.data3)).setText(collectorModelPayment.data3);
                vPayment.findViewById(R.id.addPay).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final CollectionModel model = new CollectionModel(commonModel.CommonID, "1");
                        /*realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model[0] = realmUI.copyToRealmOrUpdate(new CollectionModel(commonModel.CommonID, "1"));
                            }
                        });*/
                        viewPayCollection(model, 0);
                    }
                });
//	    		List<CollectorModel> list = ctrlCollector.get(commonModel.CommonID, "1");
//	    		for (CollectorModel collectorModel : list) {
//	    			((LinearLayout)vPayment.findViewById(R.id.panel_pay)).addView(new ItemCollectionPayWidget(context, collectorModel));
//				}
                ((ListView) vPayment.findViewById(R.id.list)).setAdapter(payAdapter);
                ((ListView) vPayment.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        viewPayCollection(/*realmUI.copyFromRealm(*/payAdapter.getItem(position), 1);
                    }
                });
                v = vPayment;
            } else if (position == 2) {
                vNotPayment = inflater.inflate(R.layout.fragment_collection_not_pay, null);
                ((Spinner) vNotPayment.findViewById(R.id.data1)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("NO_PAYMENT_REASON")));
                vNotPayment.findViewById(R.id.btn_tanggal).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTanggal();
                    }
                });

                RealmResults<CollectionModel> list = realmUI.where(CollectionModel.class)
                        .equalTo("CommonID", commonModel.CommonID)
                        .equalTo("status", "2")
                        .findAll();

//                List<CollectorModel> list = ctrlCollector.get(commonModel.CommonID, "2");
                if (list.size() > 0) {
                    collectionModelNotPayment = list.get(0);
                } else {
                    collectionModelNotPayment = new CollectionModel(commonModel.CommonID, "2");
                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realmUI.copyToRealmOrUpdate(collectionModelNotPayment);
                        }
                    });
                }
                int index = ((ComboBoxAdapter) ((Spinner) vNotPayment.findViewById(R.id.data1)).getAdapter()).indexOf(new ComboBoxModel(collectionModelNotPayment.data1));
                ((Spinner) vNotPayment.findViewById(R.id.data1)).setSelection(index);
                ((EditText) vNotPayment.findViewById(R.id.data2)).setText(collectionModelNotPayment.data2);
                ((EditText) vNotPayment.findViewById(R.id.data3)).setText(collectionModelNotPayment.data3);
                v = vNotPayment;
            } else if (position == 0) {
                vInvoice = inflater.inflate(R.layout.fragment_invoice, null);
                /*Cursor cursor = dbMasterHelper.query("select * from custcollect where custid='" + customerModel.CustId + "'");
                double total = 0;
                int i = 0;
                while (cursor.moveToNext()) {
                    View row = inflater.inflate(R.layout.row_invoice, null);
                    ((TextView) row.findViewById(R.id.no_invoice)).setText(cursor.getString(1));
                    ((TextView) row.findViewById(R.id.due_date)).setText(cursor.getString(2));
                    double amount = cursor.getDouble(3);
                    ((TextView) row.findViewById(R.id.amount)).setText(Util.numberFormat.format(amount));
                    if (i % 2 == 0) {
                        row.setBackgroundResource(R.color.login_font_color_1);
                    } else {
                        row.setBackgroundResource(R.color.login_font_color_2);
                    }
                    ((TableLayout) vInvoice.findViewById(R.id.tabel)).addView(row);
                    total += amount;
                }
                View row = inflater.inflate(R.layout.row_invoice, null);
                ((TextView) row.findViewById(R.id.no_invoice)).setText("");
                ((TextView) row.findViewById(R.id.due_date)).setText("Total");
                ((TextView) row.findViewById(R.id.amount)).setText(Util.numberFormat.format(total));
                row.setBackgroundResource(R.color.extra_call);
                ((TableLayout) vInvoice.findViewById(R.id.tabel)).addView(row);
                cursor.close();
                dbMasterHelper.close();*/
                refresh();
                v = vInvoice;
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            final View v = (View) view;
            if (position == 2) {
                final ComboBoxModel model = ((ComboBoxModel) ((Spinner) v.findViewById(R.id.data1)).getSelectedItem());
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (model != null) {
                            collectionModelNotPayment.data1 = model.value;
                        } else {
                            collectionModelNotPayment.data1 = "";
                        }
                        collectionModelNotPayment.data2 = ((EditText) v.findViewById(R.id.data2)).getText().toString();
                        collectionModelNotPayment.data3 = ((EditText) v.findViewById(R.id.data3)).getText().toString();
                    }
                });

            }
            ((ViewPager) collection).removeView(v);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI = Realm.getDefaultInstance();
    }
}
