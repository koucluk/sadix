package com.bhn.sadix.fragment.customer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.NewCustomerNOOActivity;
import com.bhn.sadix.PopUpCustomer;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.fragment.kunjungan.NewEditCustomerFragment;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.ItemPhotoCustomerListener;
import com.bhn.sadix.widget.ItemPhotoCustomerWidget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FragmentCustomer extends BaseFragment implements ItemPhotoCustomerListener {
    private CtrlCustomerType ctrlCustomerType;
    private CtrlCustomer ctrlCustomer;
    private CtrlAppModul ctrlAppModul;
    private CtrlCustPhoto ctrlCustPhoto;
    //    private CustomerModel customerModel;
    private ComboBoxAdapter adapter;
    private ItemPhotoCustomerWidget widget;
    private CtrlConfig ctrlConfig;
    private PopUpCustomer popUpCustomer;
    private CustomerModel custUpLine;
    private ComboBoxAdapter adapterCustLevel;
    private ItemPhotoCustomerListener listener;

    private String BeginTime;

    private List<ItemPhotoCustomerWidget> listWidget = new ArrayList<ItemPhotoCustomerWidget>();

    public FragmentCustomer() {
        super(R.layout.fragment_kunjungan_edit_customer, "Customer");
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public void addItemPhotoCustomerListener(ItemPhotoCustomerListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlCustomer = new CtrlCustomer(getActivity());
        ctrlCustomerType = new CtrlCustomerType(getActivity());
        ctrlAppModul = new CtrlAppModul(getActivity());
        ctrlConfig = new CtrlConfig(getActivity());
        ctrlCustPhoto = new CtrlCustPhoto(getActivity());
        if (popUpCustomer == null) {
            popUpCustomer = new PopUpCustomer(getActivity(), new PopUpCustomer.CustomerListener() {
                @Override
                public void selectCustomer(CustomerModel cust) {
                    custUpLine = cust;
                    ((EditText) view.findViewById(R.id.CustomerUpLineID)).setText(custUpLine.CustomerName);
                }
            });
        }
        adapterCustLevel = new ComboBoxAdapter(getActivity(), new ArrayList<ComboBoxModel>());
        adapterCustLevel.setNotifyOnChange(true);
        adapterCustLevel.add(new ComboBoxModel("0", "NA"));
        int CustLevel = Util.getIntegerPreference(getActivity(), "CustLevel");
        for (int i = 1; i <= CustLevel; i++) {
            adapterCustLevel.add(new ComboBoxModel(String.valueOf(i), "LEVEL " + i));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*if (getActivity() instanceof NewCustomerNOOActivity) {
            ((TextView) view.findViewById(R.id.lblOwnerHobby)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerBirthDate)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerReligion)).setTextColor(Color.RED);
        } else if (getActivity() instanceof KunjunganActivity && customerModel.isNOO.equals("1")) {
            ((TextView) view.findViewById(R.id.lblOwnerHobby)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerBirthDate)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerReligion)).setTextColor(Color.RED);
        }*/
    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        /*if (getActivity() instanceof NewCustomerNOOActivity) {
            ((TextView) view.findViewById(R.id.lblOwnerHobby)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerBirthDate)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerReligion)).setTextColor(Color.RED);
        } else if (getActivity() instanceof KunjunganActivity && customerModel.isNOO.equals("1")) {
            ((TextView) view.findViewById(R.id.lblOwnerHobby)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerBirthDate)).setTextColor(Color.RED);
            ((TextView) view.findViewById(R.id.lblOwnerReligion)).setTextColor(Color.RED);
        }*/
        BeginTime = Util.dateFormat.format(new Date());
        view.findViewById(R.id.btn_add_photo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addPhoto();
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        view.findViewById(R.id.btn_tgl).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupBirthDate((EditText) view.findViewById(R.id.OwnerBirthDate));
            }
        });
        view.findViewById(R.id.btn_up_cust).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				String CustomerLevel = ((EditText)view.findViewById(R.id.CustomerLevel)).getText().toString();
                String CustomerLevel = ((ComboBoxModel) ((Spinner) view.findViewById(R.id.CustomerLevel)).getSelectedItem()).value;
//				if(CustomerLevel.equals("") || CustomerLevel.equals("0")) {
//					info("Customer Level tidak boleh kosong atau tidak boleh 0!");
//				} else {
                popUpCustomer.setCustomerLevel(Integer.parseInt(CustomerLevel));
                popUpCustomer.show();
//				}
            }
        });
        adapter = new ComboBoxAdapter(getActivity(), ctrlCustomerType.listComboBox());
        adapter.setNotifyOnChange(true);
        ((Spinner) view.findViewById(R.id.CustomerTypeId)).setAdapter(adapter);
        ((Spinner) view.findViewById(R.id.CustomerLegal)).setAdapter(new ComboBoxAdapter(getActivity(), ctrlConfig.listComboBox("LEGAL_STATUS")));
        ((Spinner) view.findViewById(R.id.CustomerLevel)).setAdapter(adapterCustLevel);
        ((Spinner) view.findViewById(R.id.CustomerLevel)).setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String CustomerLevel = ((ComboBoxModel) ((Spinner) view.findViewById(R.id.CustomerLevel)).getSelectedItem()).value;
                if (CustomerLevel.equals("") || CustomerLevel.equals("0") || CustomerLevel.equals("1")) {
                    custUpLine = null;
                    ((EditText) view.findViewById(R.id.CustomerUpLineID)).setText(null);
                    view.findViewById(R.id.btn_up_cust).setEnabled(false);
                } else {
                    view.findViewById(R.id.btn_up_cust).setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        if (getActivity() instanceof KunjunganActivity) {
            AppModul modul = ctrlAppModul.get("51");
            if (modul != null) {
                if (modul.AppModulValue.equals("1")) {
                    view.findViewById(R.id.btn_save).setVisibility(View.VISIBLE);
                } else if (modul.AppModulValue.equals("0")) {
                    view.findViewById(R.id.btn_save).setVisibility(View.GONE);
                    view.findViewById(R.id.btn_add_photo).setVisibility(View.GONE);
                    edittableTextBox(false);
                } else if (modul.AppModulValue.equals("2")) {
                    view.findViewById(R.id.btn_save).setVisibility(View.VISIBLE);
                    edittableTextBox(false);
                } else if (modul.AppModulValue.equals("3")) {
                    view.findViewById(R.id.btn_save).setVisibility(View.VISIBLE);
                }
            }
//        	viewCustomer();
        }
    }

    private void viewCustomer() {
        if (view != null && customerModel != null) {
            ((EditText) view.findViewById(R.id.CustomerName)).setText(customerModel.CustomerName);
            ((EditText) view.findViewById(R.id.CustomerAddress)).setText(customerModel.CustomerAddress);
            ((EditText) view.findViewById(R.id.CustomerEmail)).setText(customerModel.CustomerEmail);
            ((EditText) view.findViewById(R.id.CustomerFax)).setText(customerModel.CustomerFax);
            ((EditText) view.findViewById(R.id.CustomerNPWP)).setText(customerModel.CustomerNPWP);
            ((EditText) view.findViewById(R.id.CustomerPhone)).setText(customerModel.CustomerPhone);
            ((EditText) view.findViewById(R.id.OwnerAddress)).setText(customerModel.OwnerAddress);
            ((EditText) view.findViewById(R.id.OwnerEmail)).setText(customerModel.OwnerEmail);
            ((EditText) view.findViewById(R.id.OwnerName)).setText(customerModel.OwnerName);
            ((EditText) view.findViewById(R.id.OwnerPhone)).setText(customerModel.OwnerPhone);
            ((EditText) view.findViewById(R.id.OwnerBirthPlace)).setText(customerModel.OwnerBirthPlace);
            ((EditText) view.findViewById(R.id.OwnerBirthDate)).setText(customerModel.OwnerBirthDate);
            ((EditText) view.findViewById(R.id.OwnerIdNo)).setText(customerModel.OwnerIdNo);
            ((EditText) view.findViewById(R.id.OwnerHobby)).setText(customerModel.OwnerHobby);
            ((EditText) view.findViewById(R.id.Description)).setText(customerModel.Description);
//			if(customerModel.CustomerLevel > 0) {
//				((EditText)view.findViewById(R.id.CustomerLevel)).setText(Util.NUMBER_FORMAT.format(customerModel.CustomerLevel));
//			}
            if (customerModel.CustomerUpLineID > 0) {
                custUpLine = ctrlCustomer.get(Util.NUMBER_FORMAT.format(customerModel.CustomerUpLineID));
                if (custUpLine != null) {
                    ((EditText) view.findViewById(R.id.CustomerUpLineID)).setText(String.valueOf(custUpLine.CustomerName));
                }
            }
            if (customerModel.OwnerReligion != null) {
                if (customerModel.OwnerReligion.equals("Islam")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(0);
                } else if (customerModel.OwnerReligion.equals("Kristen Protestan")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(1);
                } else if (customerModel.OwnerReligion.equals("Katolik")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(2);
                } else if (customerModel.OwnerReligion.equals("Hindu")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(3);
                } else if (customerModel.OwnerReligion.equals("Buddha")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(4);
                } else if (customerModel.OwnerReligion.equals("Kong Hu Cu")) {
                    ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(5);
                }
            }
            int index = ((ComboBoxAdapter) ((Spinner) view.findViewById(R.id.CustomerTypeId)).getAdapter()).indexOf(new ComboBoxModel(customerModel.CustomerTypeId));
            ((Spinner) view.findViewById(R.id.CustomerTypeId)).setSelection(index);
            index = ((ComboBoxAdapter) ((Spinner) view.findViewById(R.id.CustomerLegal)).getAdapter()).indexOf(new ComboBoxModel(String.valueOf(customerModel.CustomerLegal)));
            ((Spinner) view.findViewById(R.id.CustomerLegal)).setSelection(index);

            index = adapterCustLevel.indexOf(new ComboBoxModel(String.valueOf(customerModel.CustomerLevel)));
            ((Spinner) view.findViewById(R.id.CustomerLevel)).setSelection(index);

            ((EditText) view.findViewById(R.id.CustomerNpwpName)).setText(customerModel.CustomerNpwpName);
            ((EditText) view.findViewById(R.id.CustomerNpwpAddress)).setText(customerModel.CustomerNpwpAddress);
        }
    }

    private void edittableTextBox(boolean isedit) {
        try {
            if (!isedit) {
                ((EditText) view.findViewById(R.id.CustomerName)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerAddress)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerPhone)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerFax)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerNPWP)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerEmail)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerName)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerAddress)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerPhone)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerBirthPlace)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerBirthDate)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerIdNo)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.OwnerEmail)).setKeyListener(null);
                view.findViewById(R.id.CustomerTypeId).setEnabled(isedit);
                view.findViewById(R.id.btn_tgl).setEnabled(isedit);

                ((EditText) view.findViewById(R.id.OwnerHobby)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.Description)).setKeyListener(null);
//				((EditText)view.findViewById(R.id.CustomerLevel)).setKeyListener(null);
                view.findViewById(R.id.CustomerLevel).setEnabled(isedit);
                ((EditText) view.findViewById(R.id.CustomerUpLineID)).setKeyListener(null);
                view.findViewById(R.id.OwnerReligion).setEnabled(isedit);
                view.findViewById(R.id.CustomerLegal).setEnabled(isedit);
                view.findViewById(R.id.btn_up_cust).setEnabled(isedit);

                ((EditText) view.findViewById(R.id.CustomerNpwpName)).setKeyListener(null);
                ((EditText) view.findViewById(R.id.CustomerNpwpAddress)).setKeyListener(null);
            }
        } catch (Exception e) {
        }
    }

    private void popupBirthDate(final EditText OwnerBirthDate) {
        try {
            final Calendar cal = Calendar.getInstance();
            final DatePickerDialog tglDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    OwnerBirthDate.setText(Util.dateFormatBirthDate.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            tglDialog.show();
        } catch (Exception e) {
            Util.Toast(getActivity(), e.getMessage());
        }
    }

    private void save() {
        if (getActivity() instanceof KunjunganActivity) {
            if (((KunjunganActivity) getActivity()).getEditCustomerFragment().location == null) {
                Util.showDialogInfo(getActivity(), "Lokasi tidak ditemukan!");
                return;
            }
        } else if (getActivity() instanceof NewCustomerNOOActivity) {
            if (((NewCustomerNOOActivity) getActivity()).location == null) {
                Util.showDialogInfo(getActivity(), "Lokasi tidak ditemukan!");
                return;
            }
        }
        if (isValid() == false) {
            return;
        }
        LinearLayout panel_photo = ((LinearLayout) view.findViewById(R.id.panel_photo));
        if (panel_photo.getChildCount() == 0) {
            Util.showDialogInfo(getActivity(), "Photo wajib diisi!");
            return;
        }
        for (int i = 0; i < panel_photo.getChildCount(); i++) {
            View view = panel_photo.getChildAt(i);
            if (view instanceof ItemPhotoCustomerWidget) {
                ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                if (item.isAmbilPhoto() == false) {
                    Util.showDialogInfo(getActivity(), "Photo belum diambil!");
                    return;
                }
            }
        }
        setValueData(true);
        try {
            JSONObject json = new JSONObject();
            json.put("CustomerId", Integer.parseInt(customerModel.CustId));

            String oriName = customerModel.CustomerName;

            String name = oriName.substring(oriName.lastIndexOf(']') + 1).replaceAll(" ", "");

            json.put("CustomerName", name);
            json.put("CustomerAddress", customerModel.CustomerAddress);
            json.put("CustomerEmail", customerModel.CustomerEmail);
            json.put("CustomerPhone", customerModel.CustomerPhone);
            json.put("CustomerFax", customerModel.CustomerFax);
            json.put("CustomerNpwp", customerModel.CustomerNPWP);
            json.put("OwnerName", customerModel.OwnerName);
            json.put("OwnerAddress", customerModel.OwnerAddress);
            json.put("OwnerEmail", customerModel.OwnerEmail);
            json.put("OwnerPhone", customerModel.OwnerPhone);
            json.put("OwnerBirthPlace", customerModel.OwnerBirthPlace);
            json.put("OwnerBirthDate", customerModel.OwnerBirthDate);
            json.put("OwnerIdNo", customerModel.OwnerIdNo);
            json.put("CustomerTypeId", Integer.parseInt(customerModel.CustomerTypeId));
            json.put("GeoLat", Double.parseDouble(customerModel.GeoLat.equals("") ? "0" : customerModel.GeoLat));
            json.put("GeoLong", Double.parseDouble(customerModel.GeoLong.equals("") ? "0" : customerModel.GeoLong));
            if (getActivity() instanceof NewCustomerNOOActivity) {
                json.put("StatusId", 1);
            } else if (getActivity() instanceof KunjunganActivity) {
                json.put("StatusId", 2);
            }
            json.put("SalesId", Integer.parseInt(UserModel.getInstance(getActivity()).SalesId));
            json.put("Picture", "xxx.jpg");

            json.put("OwnerReligion", customerModel.OwnerReligion);
            json.put("OwnerHobby", customerModel.OwnerHobby);
            json.put("Description", customerModel.Description);
            json.put("CustomerLevel", customerModel.CustomerLevel);
            if (custUpLine != null) {
                customerModel.CustomerUpLineID = Integer.parseInt(custUpLine.CustId);
            }
            json.put("CustomerUpLineID", customerModel.CustomerUpLineID);
            json.put("CustomerLegal", customerModel.CustomerLegal);
            json.put("RandomID", customerModel.RandomID);
            String EndTime = Util.dateFormat.format(new Date());
            json.put("BeginTime", BeginTime);
            json.put("EndTime", EndTime);

            json.put("CustomerNpwpName", customerModel.CustomerNpwpName);
            json.put("CustomerNpwpAddress", customerModel.CustomerNpwpAddress);

            if (getActivity() instanceof KunjunganActivity) {
                KunjunganActivity activity = (KunjunganActivity) getActivity();
                if (ctrlAppModul.isModul("67"))
                    json.put("Address", activity.getEditCustomerFragment().getAddress());
                else json.put("Address", new JSONArray());

                if (ctrlAppModul.isModul("66"))
                    json.put("Pic", activity.getEditCustomerFragment().getPic());
                else json.put("Pic", new JSONArray());
            } else if (getActivity() instanceof NewCustomerNOOActivity) {
                NewCustomerNOOActivity activity = (NewCustomerNOOActivity) getActivity();
                if (ctrlAppModul.isModul("67")) json.put("Address", activity.getAddress());
                else json.put("Address", new JSONArray());

                if (ctrlAppModul.isModul("66")) json.put("Pic", activity.getPic());
                else json.put("Pic", new JSONArray());
            }
//			android.util.Log.e("json", ""+json.toString());
            server.setUrl(Util.getServerUrl(getActivity()) + "postcustdetail");
            server.requestJSONObject(json);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogInfo(getActivity(), e.getMessage());
        }
    }

    private void setValueData(boolean isSave) {
        Calendar cal = Calendar.getInstance();

        int dayNow = cal.get(Calendar.DAY_OF_WEEK);

//        Log.d("DayNow", "is " + dayNow);

        if (getActivity() instanceof NewCustomerNOOActivity) {
            customerModel.CustId = "0";
            customerModel.CustomerCrBalance = 0;
            customerModel.CustomerCrLimit = 0;
            customerModel.isNew = "1";
            customerModel.isNOO = "1";

            customerModel.D1 = "false";
            customerModel.D2 = "false";
            customerModel.D3 = "false";
            customerModel.D4 = "false";
            customerModel.D5 = "false";
            customerModel.D6 = "false";
            customerModel.D7 = "false";

            /*switch (dayNow) {
                case 1:
                    customerModel.D1 = "true";
                    customerModel.D2 = "false";
                    customerModel.D3 = "false";
                    customerModel.D4 = "false";
                    customerModel.D5 = "false";
                    customerModel.D6 = "false";
                    customerModel.D7 = "false";
                    break;
                case 2:
                    customerModel.D1 = "false";
                    customerModel.D2 = "true";
                    customerModel.D3 = "false";
                    customerModel.D4 = "false";
                    customerModel.D5 = "false";
                    customerModel.D6 = "false";
                    customerModel.D7 = "false";
                    break;
                case 3:
                    customerModel.D1 = "false";
                    customerModel.D2 = "false";
                    customerModel.D3 = "true";
                    customerModel.D4 = "false";
                    customerModel.D5 = "false";
                    customerModel.D6 = "false";
                    customerModel.D7 = "false";
                    break;
                case 4:
                    customerModel.D1 = "false";
                    customerModel.D2 = "false";
                    customerModel.D3 = "false";
                    customerModel.D4 = "true";
                    customerModel.D5 = "false";
                    customerModel.D6 = "false";
                    customerModel.D7 = "false";
                    break;
                case 5:
                    customerModel.D1 = "false";
                    customerModel.D2 = "false";
                    customerModel.D3 = "false";
                    customerModel.D4 = "false";
                    customerModel.D5 = "true";
                    customerModel.D6 = "false";
                    customerModel.D7 = "false";
                    break;
                case 6:
                    customerModel.D1 = "false";
                    customerModel.D2 = "false";
                    customerModel.D3 = "false";
                    customerModel.D4 = "false";
                    customerModel.D5 = "false";
                    customerModel.D6 = "true";
                    customerModel.D7 = "false";
                    break;
                case 7:
                    customerModel.D1 = "false";
                    customerModel.D2 = "false";
                    customerModel.D3 = "false";
                    customerModel.D4 = "false";
                    customerModel.D5 = "false";
                    customerModel.D6 = "false";
                    customerModel.D7 = "true";
                    break;
            }*/
        }

        customerModel.isEdit = "1";
        customerModel.CustomerAddress = ((EditText) view.findViewById(R.id.CustomerAddress)).getText().toString();
        customerModel.CustomerEmail = ((EditText) view.findViewById(R.id.CustomerEmail)).getText().toString();
        customerModel.CustomerFax = ((EditText) view.findViewById(R.id.CustomerFax)).getText().toString();
//        Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
        customerModel.CustomerName = ((EditText) view.findViewById(R.id.CustomerName)).getText().toString();
        customerModel.CustomerNPWP = ((EditText) view.findViewById(R.id.CustomerNPWP)).getText().toString();
        customerModel.CustomerPhone = ((EditText) view.findViewById(R.id.CustomerPhone)).getText().toString();
        customerModel.CustomerTypeId = ((ComboBoxModel) ((Spinner) view.findViewById(R.id.CustomerTypeId)).getSelectedItem()).value;
        if (isSave) {
            if (getActivity() instanceof KunjunganActivity) {
                Location location = ((KunjunganActivity) getActivity()).getEditCustomerFragment().location;
                customerModel.GeoLat = location == null ? "" : String.format("%1$s", location.getLatitude());
                customerModel.GeoLong = location == null ? "" : String.format("%1$s", location.getLongitude());
            } else if (getActivity() instanceof NewCustomerNOOActivity) {
                Location location = ((NewCustomerNOOActivity) getActivity()).location;
                customerModel.GeoLat = location == null ? "" : String.format("%1$s", location.getLatitude());
                customerModel.GeoLong = location == null ? "" : String.format("%1$s", location.getLongitude());
            }
        }
        customerModel.OwnerAddress = ((EditText) view.findViewById(R.id.OwnerAddress)).getText().toString();
        customerModel.OwnerEmail = ((EditText) view.findViewById(R.id.OwnerEmail)).getText().toString();
        customerModel.OwnerName = ((EditText) view.findViewById(R.id.OwnerName)).getText().toString();
        customerModel.OwnerPhone = ((EditText) view.findViewById(R.id.OwnerPhone)).getText().toString();
        customerModel.OwnerBirthPlace = ((EditText) view.findViewById(R.id.OwnerBirthPlace)).getText().toString();
        customerModel.OwnerBirthDate = ((EditText) view.findViewById(R.id.OwnerBirthDate)).getText().toString();
        customerModel.OwnerIdNo = ((EditText) view.findViewById(R.id.OwnerIdNo)).getText().toString();

        customerModel.OwnerReligion = ((Spinner) view.findViewById(R.id.OwnerReligion)).getSelectedItem().toString();
        customerModel.OwnerHobby = ((EditText) view.findViewById(R.id.OwnerHobby)).getText().toString();
        customerModel.Description = ((EditText) view.findViewById(R.id.Description)).getText().toString();
//		String CustomerLevel = ((EditText)view.findViewById(R.id.CustomerLevel)).getText().toString();
        String CustomerLevel = ((ComboBoxModel) ((Spinner) view.findViewById(R.id.CustomerLevel)).getSelectedItem()).value;
        customerModel.CustomerLevel = Integer.parseInt((CustomerLevel.equals("") ? "0" : CustomerLevel));
        if (custUpLine != null) {
            customerModel.CustomerUpLineID = Integer.parseInt(custUpLine.CustId);
        }
        customerModel.CustomerLegal = Integer.parseInt(
                ((ComboBoxModel) ((Spinner) view.findViewById(R.id.CustomerLegal)).getSelectedItem()).value
        );

        customerModel.CustomerNpwpName = ((EditText) view.findViewById(R.id.CustomerNpwpName)).getText().toString();
        customerModel.CustomerNpwpAddress = ((EditText) view.findViewById(R.id.CustomerNpwpAddress)).getText().toString();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((LinearLayout) view.findViewById(R.id.panel_photo)).removeAllViews();
        setValueData(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        for (ItemPhotoCustomerWidget itemPhotoCustomerWidget : listWidget) {
            ((LinearLayout) view.findViewById(R.id.panel_photo)).addView(itemPhotoCustomerWidget);
        }
//		setData();
        viewCustomer();
    }

//	private void setData() {	
//		((EditText)view.findViewById(R.id.CustomerAddress)).setText(customerModel.CustomerAddress);
//		((EditText)view.findViewById(R.id.CustomerEmail)).setText(customerModel.CustomerEmail);
//		((EditText)view.findViewById(R.id.CustomerFax)).setText(customerModel.CustomerFax);
//		((EditText)view.findViewById(R.id.CustomerName)).setText(customerModel.CustomerName);
//		((EditText)view.findViewById(R.id.CustomerNPWP)).setText(customerModel.CustomerNPWP);
//		((EditText)view.findViewById(R.id.CustomerPhone)).setText(customerModel.CustomerPhone);
//		((EditText)view.findViewById(R.id.OwnerAddress)).setText(customerModel.OwnerAddress);
//		((EditText)view.findViewById(R.id.OwnerEmail)).setText(customerModel.OwnerEmail);
//		((EditText)view.findViewById(R.id.OwnerName)).setText(customerModel.OwnerName);
//		((EditText)view.findViewById(R.id.OwnerPhone)).setText(customerModel.OwnerPhone);
//		((EditText)view.findViewById(R.id.OwnerBirthPlace)).setText(customerModel.OwnerBirthPlace);
//		((EditText)view.findViewById(R.id.OwnerBirthDate)).setText(customerModel.OwnerBirthDate);
//		((EditText)view.findViewById(R.id.OwnerIdNo)).setText(customerModel.OwnerIdNo);
//		String OwnerReligion = (String) ((Spinner)view.findViewById(R.id.OwnerReligion)).getSelectedItem();
//		if(OwnerReligion != null) {
//			if(OwnerReligion.equals("Islam")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(0);
//			if(OwnerReligion.equals("Kristen Protestan")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(1);
//			if(OwnerReligion.equals("Katolik")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(2);
//			if(OwnerReligion.equals("Hindu")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(3);
//			if(OwnerReligion.equals("Buddha")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(4);
//			if(OwnerReligion.equals("Kong Hu Cu")) ((Spinner)view.findViewById(R.id.OwnerReligion)).setSelection(5);
//		}
//		((EditText)view.findViewById(R.id.OwnerHobby)).setText(customerModel.OwnerHobby);
//		((EditText)view.findViewById(R.id.Description)).setText(customerModel.Description);
//		((EditText)view.findViewById(R.id.CustomerLevel)).setText(String.valueOf(customerModel.CustomerLevel));
//		((EditText)view.findViewById(R.id.CustomerUpLineID)).setText(String.valueOf(customerModel.CustomerUpLineID));
//		
//		ComboBoxAdapter adapter = (ComboBoxAdapter) ((Spinner)view.findViewById(R.id.CustomerTypeId)).getAdapter();
//		int index = adapter.indexOf(new ComboBoxModel(customerModel.CustomerTypeId));
//		if(index != -1) {
//			((Spinner)view.findViewById(R.id.CustomerTypeId)).setSelection(index);
//		}
//		adapter = (ComboBoxAdapter) ((Spinner)view.findViewById(R.id.CustomerLegal)).getAdapter();
//		index = adapter.indexOf(new ComboBoxModel(String.valueOf(customerModel.CustomerLegal)));
//		if(index != -1) {
//			((Spinner)view.findViewById(R.id.CustomerLegal)).setSelection(index);
//		}
//	}

    private boolean isValid() {
        if (getActivity() instanceof KunjunganActivity) {
            if (((KunjunganActivity) getActivity()).getEditCustomerFragment().location == null) {
                Util.showDialogInfo(getActivity(), "Lokasi tidak ditemukan!");
                return false;
            }
        } else if (getActivity() instanceof NewCustomerNOOActivity) {
            if (((NewCustomerNOOActivity) getActivity()).location == null) {
                Util.showDialogInfo(getActivity(), "Lokasi tidak ditemukan!");
                return false;
            }
        }
//        AppModul modul = ctrlAppModul.get("51");
//        if(modul.AppModulValue.equals("2")) {
//        	Location location = null;
//    		if(getActivity() instanceof KunjunganActivity) {
//    			location = ((KunjunganActivity)getActivity()).getEditCustomerFragment().location;
//    		} else if(getActivity() instanceof NewCustomerNOOActivity) {
//    			location = ((NewCustomerNOOActivity)getActivity()).location;
//    		}
//    		if(location != null) {
//    			if(location.getLongitude() < 90) {
//    				
//    			}
//    		}
//        }
        if (getActivity() instanceof NewCustomerNOOActivity) {
            /*String valid = ((EditText) view.findViewById(R.id.OwnerBirthDate)).getText().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Birth Date harus diisi!");
                return false;
            }*/
            /*valid = ((Spinner) view.findViewById(R.id.OwnerReligion)).getSelectedItem().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Religion harus diisi!");
                return false;
            }*/
            /*valid = ((EditText) view.findViewById(R.id.OwnerHobby)).getText().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Hobby harus diisi!");
                return false;
            }*/
        } else if (getActivity() instanceof KunjunganActivity && customerModel.isNOO.equals("1")) {
            /*String valid = ((EditText) view.findViewById(R.id.OwnerBirthDate)).getText().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Birth Date harus diisi!");
                return false;
            }*/
            /*valid = ((Spinner) view.findViewById(R.id.OwnerReligion)).getSelectedItem().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Religion harus diisi!");
                return false;
            }*/
            /*valid = ((EditText) view.findViewById(R.id.OwnerHobby)).getText().toString();
            if (valid.trim().equals("")) {
                Util.showDialogInfo(getActivity(), "Owner Hobby harus diisi!");
                return false;
            }*/
        }
        String valid = ((EditText) view.findViewById(R.id.CustomerName)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Customer Name harus diisi!");
            return false;
        }
        valid = ((EditText) view.findViewById(R.id.CustomerAddress)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Customer Address harus diisi!");
            return false;
        }
        /*valid = ((EditText) view.findViewById(R.id.CustomerPhone)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Customer Phone harus diisi!");
            return false;
        }*/
        /*valid = ((EditText) view.findViewById(R.id.OwnerName)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Owner Name harus diisi!");
            return false;
        }*/
        /*valid = ((EditText) view.findViewById(R.id.OwnerAddress)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Owner Address harus diisi!");
            return false;
        }*/
        /*valid = ((EditText) view.findViewById(R.id.OwnerPhone)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(getActivity(), "Owner Phone harus diisi!");
            return false;
        }*/
        return true;
    }

    private void addPhoto() {
        ItemPhotoCustomerWidget widget = new ItemPhotoCustomerWidget(getActivity());
        widget.addItemPhotoCustomerListener(listener == null ? this : listener);
        widget.setCustomerModel(customerModel);
        listWidget.add(widget);
        ((LinearLayout) view.findViewById(R.id.panel_photo)).addView(widget);
        widget.takePicture();
    }

    @Override
    public void takePicture(ItemPhotoCustomerWidget widget) {
        this.widget = widget;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    @Override
    public void send(ItemPhotoCustomerWidget widget) {
        ((LinearLayout) view.findViewById(R.id.panel_photo)).removeView(widget);
        listWidget.remove(widget);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    widget.setImage(bitmap);
                    widget.setData(bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showDialogError(getActivity(), e.getMessage());
                }
            } else {
                ((LinearLayout) view.findViewById(R.id.panel_photo)).removeView(widget);
                listWidget.remove(widget);
            }
        }
    }

    @Override
    public void delete(final ItemPhotoCustomerWidget widget) {
        Util.confirmDialog(getActivity(), "Info", "Apakah photo ingin dihapus?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    ((LinearLayout) view.findViewById(R.id.panel_photo)).removeView(widget);
                    listWidget.remove(widget);
                }
            }
        });
    }

    class KirimGambar extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;
        private boolean isSave;

        public KirimGambar(Context context, boolean isSave) {
            this.isSave = isSave;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Kirim gambar...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            LinearLayout layout = (LinearLayout) view.findViewById(R.id.panel_photo);
            for (int i = 0; i < layout.getChildCount(); i++) {
                View view = layout.getChildAt(i);
                if (view instanceof ItemPhotoCustomerWidget) {
                    ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                    if (isSave) {
                        item.saveKeLokal();
                    } else {
                        String respon = ConnectionServer.requestJSONObjectNonThread(item.getJsonSendData(), Util.getServerUrl(getActivity()) + "photodata", false);
                        try {
                            JSONObject json = new JSONObject(respon);
                            if ((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
                                item.saveKeLokal();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
            List<CustAdddress> listAddr = null;//((NewCustomerNOOActivity)getActivity()).getAddressByList();
            List<CustPIC> listPic = null;//((NewCustomerNOOActivity)getActivity()).getPicByList();
            if (getActivity() instanceof KunjunganActivity) {
                listAddr = ((KunjunganActivity) getActivity()).getEditCustomerFragment().getAddressByList();
                listPic = ((KunjunganActivity) getActivity()).getEditCustomerFragment().getPicByList();
            } else if (getActivity() instanceof NewCustomerNOOActivity) {
                listAddr = ((NewCustomerNOOActivity) getActivity()).getAddressByList();
                listPic = ((NewCustomerNOOActivity) getActivity()).getPicByList();
            }

            for (CustAdddress custAdddress : listAddr) {
                if (isSave) {
                    custAdddress.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
                } else {
                    String respon = ConnectionServer.requestJSONObjectNonThread(custAdddress.getJsonSendImage(UserModel.getInstance(getActivity()).SalesId), Util.getServerUrl(getActivity()) + "photodata", false);
                    try {
                        JSONObject json = new JSONObject(respon);
                        if ((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
                            custAdddress.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            for (CustPIC custPIC : listPic) {
                if (isSave) {
                    custPIC.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
                } else {
                    String respon = ConnectionServer.requestJSONObjectNonThread(custPIC.getJsonSendImage(UserModel.getInstance(getActivity()).SalesId), Util.getServerUrl(getActivity()) + "photodata", false);
                    try {
                        JSONObject json = new JSONObject(respon);
                        if ((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
                            custPIC.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
                        }
                    } catch (Exception e) {
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void respon) {
            progressDialog.dismiss();
            if (getActivity() instanceof NewCustomerNOOActivity) {
                getActivity().finish();
            } else if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).keInformasi();
            }
        }
    }

    public void setLcation(Location location) {
        if (view != null) {
            ((TextView) view.findViewById(R.id.Lokasi)).setText("Longitude / Latitude : " + String.format("%1$s", location.getLongitude()) +
                    " / " + String.format("%1$s", location.getLatitude()));
        }
    }

    @Override
    public void error(String error) {
        saveLokal();
        Util.okeDialog(getActivity(), "Info", error, new OkListener() {
            @Override
            public void onDialogOk() {
                if (getActivity() instanceof NewCustomerNOOActivity) {
                    getActivity().finish();
                } else if (getActivity() instanceof KunjunganActivity) {
                    ((KunjunganActivity) getActivity()).keInformasi();
                }
            }
        });
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                if (customerModel.isNOO != null && customerModel.isNOO.equals("1")) {
                    if (json.has("ServerID") && json.getInt("ServerID") > 0) {
                        customerModel.CustId = json.getString("ServerID");
                        customerModel.isNew = "0";
                    }
                }
                customerModel.isEdit = "0";
//				ctrlCustomer.save(customerModel);
                saveCustomer(true);
                Util.Toast(getActivity(), json.getString("Message"));
//				finish();
                new KirimGambar(getActivity(), false).execute();
            } else {
                saveCustomer(false);
                Util.showDialogInfo(getActivity(), json.getString("Message"));
            }
        } catch (Exception e) {
//			error(e.getMessage());
            Util.showDialogInfo(getActivity(), e.getMessage());
        }
    }

    private void saveCustomer(boolean success) {
        if (getActivity() instanceof NewCustomerNOOActivity) {
            ctrlCustomer.save(customerModel);
        } else if (getActivity() instanceof KunjunganActivity) {
            ctrlCustomer.update(customerModel);
        }
//		ctrlCustomer.save(customerModel);

        Location location = null;
        List<CustAdddress> list = null;
        List<CustPIC> listpic = null;
        if (getActivity() instanceof KunjunganActivity) {
            NewEditCustomerFragment customerFragment = ((KunjunganActivity) getActivity()).getEditCustomerFragment();
            location = customerFragment.location;
            if (ctrlAppModul.isModul("67")) list = customerFragment.getAddressByList();
            if (ctrlAppModul.isModul("66")) listpic = customerFragment.getPicByList();
        } else if (getActivity() instanceof NewCustomerNOOActivity) {
            NewCustomerNOOActivity activity = (NewCustomerNOOActivity) getActivity();
            location = activity.location;
            if (ctrlAppModul.isModul("67")) list = activity.getAddressByList();
            if (ctrlAppModul.isModul("66")) listpic = activity.getPicByList();
        }
        if (ctrlAppModul.isModul("67")) {
            for (CustAdddress custAdddress : list) {
                custAdddress.CustID = Integer.parseInt(customerModel.CustId);
                if (location != null) {
                    custAdddress.GeoLat = String.format("%1$s", location.getLatitude());//location.getLatitude();
                    custAdddress.GeoLong = String.format("%1$s", location.getLongitude());//location.getLongitude();
                }
                ctrlCustomer.saveCustAddress(custAdddress);
                custAdddress.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
            }
        }
        if (ctrlAppModul.isModul("66")) {
            for (CustPIC custPIC : listpic) {
                custPIC.CustID = Integer.parseInt(customerModel.CustId);
                ctrlCustomer.saveCustPIC(custPIC);
                custPIC.saveImage(ctrlCustPhoto, UserModel.getInstance(getActivity()).SalesId);
            }
        }
    }

    private void saveLokal() {
//		ctrlCustomer.save(customerModel);
        saveCustomer(false);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.panel_photo);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View view = layout.getChildAt(i);
            if (view instanceof ItemPhotoCustomerWidget) {
                ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                item.saveKeLokal();
            }
        }
        Util.Toast(getActivity(), "Data disimpan dilokal");
    }

}
