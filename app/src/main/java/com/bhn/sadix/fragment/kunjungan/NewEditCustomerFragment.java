package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.fragment.customer.FragmentAdapter;
import com.bhn.sadix.fragment.customer.FragmentCustomer;
import com.bhn.sadix.fragment.customer.FragmnetAddAddress;
import com.bhn.sadix.fragment.customer.FragmnetAddPIC;
import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.ItemPhotoCustomerListener;
import com.bhn.sadix.widget.ItemPhotoCustomerWidget;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class NewEditCustomerFragment extends BaseFragment implements FragmnetAddAddress.AddPhotoInterface, FragmnetAddPIC.AddPhotoInterface {
    private FragmentAdapter adapter;
    private FragmentCustomer fragmentCustomer;
    private FragmnetAddAddress fragmnetAddAddress;
    private FragmnetAddPIC fragmnetAddPIC;
    private ViewPager pager;
    //    private CustomerModel customerModel;
    private LocationManager locationManager;
    public Location location;
    private CtrlCustomer ctrlCustomer;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            fragmentCustomer.setLcation(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    private ImageView profileImg;
    private CustAdddress custAdddress;
    private CtrlAppModul ctrlAppModul;
    private CustPIC custPIC;
    private ImageView picImg;

    public NewEditCustomerFragment(/*ItemPhotoCustomerListener listener*/) {
        super(R.layout.activity_new_customer_noo, "");

    }

    /*public void setItemPhotoCustomerListener(ItemPhotoCustomerListener listener) {
        fragmentCustomer.addItemPhotoCustomerListener(listener);
    }*/

    /*public CustomerModel getCustomerModel() {
        return customerModel;
//    }*/

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
        fragmentCustomer.setCustomerModel(this.customerModel);
        fragmnetAddAddress.setCustomerModel(this.customerModel);
        fragmnetAddPIC.setCustomerModel(this.customerModel);
    }*/

    private void loadDataDetail() {
        if (customerModel != null) {
            if (fragmnetAddAddress != null) {
                fragmnetAddAddress.setList(ctrlCustomer.listCustAddress(customerModel.CustId));
            }
            if (fragmnetAddPIC != null) {
                fragmnetAddPIC.setList(ctrlCustomer.listCustPIC(customerModel.CustId));
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlCustomer = new CtrlCustomer(getActivity());
        ctrlAppModul = new CtrlAppModul(getActivity());
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        loadDataDetail();
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(getActivity(), e.getMessage());
        }
    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fragmentCustomer = new FragmentCustomer();
        if (getActivity() instanceof KunjunganActivity)
            fragmentCustomer.addItemPhotoCustomerListener((ItemPhotoCustomerListener) getActivity());
        fragmnetAddAddress = new FragmnetAddAddress();
        fragmnetAddAddress.addAddPhotoInterface(this);
        fragmnetAddPIC = new FragmnetAddPIC();
        fragmnetAddPIC.addAddPhotoInterface(this);

        pager = (ViewPager) view.findViewById(R.id.pager);
        adapter = new FragmentAdapter(getChildFragmentManager(), new ArrayList<BaseFragment>());
        adapter.add(fragmentCustomer);
        if (ctrlAppModul.isModul("67")) adapter.add(fragmnetAddAddress);
        if (ctrlAppModul.isModul("66")) adapter.add(fragmnetAddPIC);
        pager.setAdapter(adapter);
    }

    public JSONArray getAddress() {
        if (fragmnetAddAddress.getAdapter() != null) {
            return fragmnetAddAddress.getAdapter().toJsonArray(location);
        } else {
            return new JSONArray();
        }
    }

    public List<CustAdddress> getAddressByList() {
        if (fragmnetAddAddress.getAdapter() != null) {
            return fragmnetAddAddress.getAdapter().getList();
        } else {
            return new ArrayList<CustAdddress>(0);
        }
    }

    public JSONArray getPic() {
        if (fragmnetAddPIC.getAdapter() != null) {
            return fragmnetAddPIC.getAdapter().toJsonArray();
        } else {
            return new JSONArray();
        }
    }

    public List<CustPIC> getPicByList() {
        if (fragmnetAddPIC.getAdapter() != null) {
            return fragmnetAddPIC.getAdapter().getList();
        } else {
            return new ArrayList<CustPIC>(0);
        }
    }

    public void send(ItemPhotoCustomerWidget widget) {
        fragmentCustomer.send(widget);
    }

    public void delete(ItemPhotoCustomerWidget widget) {
        fragmentCustomer.delete(widget);
    }

    @Override
    public void addPhoto(CustAdddress item, ImageView img) {
        custAdddress = item;
        profileImg = img;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    custAdddress.bitmap = bitmap;
                    custAdddress.data = bao.toByteArray();
                    profileImg.setImageBitmap(bitmap);
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(getActivity(), e.getMessage());
                }
            }
        } else if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    custPIC.bitmap = bitmap;
                    custPIC.data = bao.toByteArray();
                    picImg.setImageBitmap(bitmap);
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(getActivity(), e.getMessage());
                }
            }
        }
    }

    @Override
    public void addPhoto(CustPIC item, ImageView img) {
        custPIC = item;
        picImg = img;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 200);
    }
}
