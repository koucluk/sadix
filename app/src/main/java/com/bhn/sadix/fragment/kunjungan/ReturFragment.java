package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.SearchSKUActivity;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.ReturDetailAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlRetur;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.ReturDetailModel;
import com.bhn.sadix.model.ReturModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

//import com.bhn.sadix.model.CommonModel;

//import com.bhn.sadix.model.CommonModel;

public class ReturFragment extends Fragment {
    private View view;
    private ViewPager mPager;
    private ReturDetailAdapter adapter;
    private ReturDetailAdapter adapterRetur;
    private ViewPagerAdapter pagerAdapter;
    private CtrlConfig ctrlConfig;
    private Context context;
    private CtrlRetur ctrlRetur;
    private CtrlSKU ctrlSKU;
    private ExpandableListView listAll;
    private ExpandableListView listRetur;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    private CustomerModel customerModel;
    KunjunganListener listener;
    private Realm realmUI;

    public static ReturFragment newInstance() {
        return new ReturFragment();
    }

    public ReturDetailAdapter getReturAdapter() {
        return adapter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlConfig = new CtrlConfig(context);
        ctrlRetur = new CtrlRetur(context);
        ctrlSKU = new CtrlSKU(context);

        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_retur, container, false);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_all_product) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_retur) {
                    mPager.setCurrentItem(1);
                }
            }
        });
        ((RadioButton) view.findViewById(R.id.tab_all_product)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        ((RadioButton) view.findViewById(R.id.tab_retur)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });

//        commonModel = listener.getCommonModel();
        customerModel = listener.getCustomerModel();

        mPager = (ViewPager) view.findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    loadAllProduk();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_all_product);
                } else if (position == 1) {
                    loadRetur();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_retur);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);

        view.findViewById(R.id.btn_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((KunjunganActivity) context).isHome = false;
                Intent intent = new Intent(context, SearchSKUActivity.class);
                intent.putExtra("CustGroupId", customerModel.CustGroupID);
                startActivityForResult(intent, 3);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*if (commonModel == null) {
            commonModel = listener.getCommonModel();
        }*/

        if (customerModel == null) {
            customerModel = listener.getCustomerModel();
        }

        if (adapter == null) {
            ((KunjunganActivity) getActivity()).loadRetur();
        }
    }

    public void loadAdapter() {
        adapter = listener.getReturAdapter();

        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);
        loadRetur();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                SKUModel model = (SKUModel) data.getSerializableExtra("data");
                addOrderItemSearch(new ReturDetailModel(model));
            }
        }
        ((KunjunganActivity) context).isHome = true;
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addOrderItemSearch(ReturDetailModel returDetailModel) {
        try {
            int index = adapter.getPosisionHeader(new GroupSKUModel(returDetailModel.skuModel.GroupId));
            List<ReturDetailModel> list = adapter.listOrderByGroup(adapter.getGroup(index));
            if (list != null) {
                int indexProduk = list.indexOf(returDetailModel);
                if (indexProduk == -1) {
                    ReturModel returModel = ctrlRetur.getReturModel(commonModel.CommonID, returDetailModel.skuModel.SKUId);
                    if (returModel != null) {
                        returDetailModel.note = returModel.note;
                        returDetailModel.QTY_B = returModel.QTY_B;
                        returDetailModel.QTY_K = returModel.QTY_K;
                        returDetailModel.tipe = ctrlConfig.getByConfigClassAndConfigId("REASON_RETURN", returModel.tipe);
                        returDetailModel.RandomID = returModel.RandomID;
                    }
                    adapter.addChild(adapter.getGroup(index), returDetailModel);
                    addJumlah(returDetailModel);
                } else {
                    addJumlah(list.get(indexProduk));
                }
            } else {
                ReturModel returModel = ctrlRetur.getReturModel(commonModel.CommonID, returDetailModel.skuModel.SKUId);
                if (returModel != null) {
                    returDetailModel.note = returModel.note;
                    returDetailModel.QTY_B = returModel.QTY_B;
                    returDetailModel.QTY_K = returModel.QTY_K;
                    returDetailModel.tipe = ctrlConfig.getByConfigClassAndConfigId("REASON_RETURN", returModel.tipe);
                    returDetailModel.RandomID = returModel.RandomID;
                }
                adapter.addChild(adapter.getGroup(index), returDetailModel);
                addJumlah(returDetailModel);
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadAllProduk() {
        try {
            listAll.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadRetur() {
        try {
            adapterRetur = new ReturDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<ReturDetailModel>>());
            for (int i = 0; i < adapter.getGroupCount(); i++) {

//                populateSKUByGroup(adapter.getGroup(i));

                GroupSKUModel groupSKUModel = adapter.getGroup(i);
                adapterRetur.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                    ReturDetailModel orderDetailModel = adapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterRetur.addChild(groupSKUModel, orderDetailModel);
                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterRetur.removeHeader(groupSKUModel);
                }
            }
            adapterRetur.notifyDataSetChanged();
            if (listRetur != null)
                listRetur.setAdapter(adapterRetur);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void addJumlah(final ReturDetailModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_retur);
        dialog.setTitle(model.skuModel.ProductName);

        ((Spinner) dialog.findViewById(R.id.tipe)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("REASON_RETURN")));
        if (model.tipe != null) {
            int index = ((ComboBoxAdapter) ((Spinner) dialog.findViewById(R.id.tipe)).getAdapter()).indexOf(model.tipe);
            ((Spinner) dialog.findViewById(R.id.tipe)).setSelection(index);
        }

        ((EditText) dialog.findViewById(R.id.note)).setText(model.note);
        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        if (model.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
        }
        if (model.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                model.QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                model.QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);
                model.tipe = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.tipe)).getSelectedItem();
                model.note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();

                saveToRealm(model);
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void saveToRealm(final ReturDetailModel model) {

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                boolean needToAdd = false;
                if (!(realm.where(com.bhn.sadix.Data.ReturnModel.class)
                        .equalTo("CommonID", commonModel.CommonID)
                        .equalTo("SKUId", model.skuModel.SKUId).findAll().size() > 0)) {
                    needToAdd = true;
                }
                ReturnModel rtModel = new ReturnModel();

                rtModel.CustId = customerModel.CustId;
                rtModel.SKUId = model.skuModel.SKUId;
                rtModel.tanggal = "";
                rtModel.QTY_B = model.QTY_B;
                rtModel.QTY_K = model.QTY_K;
                rtModel.note = model.note;
                rtModel.tipe = model.tipe.value;
                rtModel.CommonID = commonModel.CommonID;
                rtModel.RandomID = model.RandomID;

                realm.copyToRealmOrUpdate(rtModel);

                if (needToAdd)
                    commonModel.returnModel.add(rtModel);
            }
        });
    }

    protected boolean populateSKUByGroup(GroupSKUModel group) {
        boolean flag = false;
        try {
            List<ReturDetailModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    ReturDetailModel returDetailModel = new ReturDetailModel(skuModel);
                    if (list == null || list.indexOf(returDetailModel) == -1) {
//                        ReturModel returModel = ctrlRetur.getReturModel(commonModel.CommonID, skuModel.SKUId);
                        ReturnModel model = realmUI.where(ReturnModel.class)
                                .equalTo("CommonID", commonModel.CommonID)
                                .equalTo("SKUId", skuModel.SKUId)
                                .findFirst();

                        if (model != null) {
                            returDetailModel.note = model.note;
                            returDetailModel.QTY_B = model.QTY_B;
                            returDetailModel.QTY_K = model.QTY_K;
                            returDetailModel.tipe = ctrlConfig.getByConfigClassAndConfigId("REASON_RETURN", model.tipe);
                            returDetailModel.RandomID = model.RandomID;
                        }
                        adapter.addChild(group, returDetailModel);
                    }
                }
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
        return flag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);

            if (position == 0) {
                listAll = ((ExpandableListView) v.findViewById(R.id.list));
                listAll.setVisibility(View.VISIBLE);
                listAll.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        ReturDetailAdapter adapter = (ReturDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                listAll.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        ReturDetailAdapter adapter = (ReturDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(adapter.getGroup(groupPosition));
                        return false;
                    }
                });
                loadAllProduk();
            } else {
                listRetur = ((ExpandableListView) v.findViewById(R.id.list));
                listRetur.setVisibility(View.VISIBLE);
                listRetur.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        ReturDetailAdapter adapter = (ReturDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                listRetur.setAdapter(adapterRetur);
                loadRetur();
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
    }
}
