package com.bhn.sadix.fragment.kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Spinner;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.PriceMonitoringAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.fragment.customer.FragmentAdapter;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

//import com.bhn.sadix.model.PriceMonitoringModel;

public class PriceMonitoringFragment extends BaseFragment {
    private static PriceMonitoringAdapter priceMonitoringProdukAdapter;
    private static PriceMonitoringAdapter priceMonitoringCompetitorAdapter = null;
    private static CtrlSKU ctrlSKU;
    private static DbMasterHelper dbmaster;
    private static CtrlAppModul ctrlAppModul;
    private static Context context;
    private static CommonModel cmModel;
    private FragmentAdapter adapter;
    private ViewPager pager;
    private Competitor competitor;
    private Produk produk;
    private ListPriceMonitoring listPriceMonitoring;
    private PriceMonitoringAdapter listMonitoringPrice;
    private KunjunganListener listener;
    private Realm realmUI;

    public PriceMonitoringFragment() {
        super(R.layout.fragment_price_monitoring, "");
    }

    private static void onPopUp(final PriceMonitoringModel model) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_price_monitoring);
        if (model.ProductID == 1) {
            dialog.setTitle(model.ProductName);
//			dialog.findViewById(R.id.layout_lavel).setVisibility(View.GONE);
        } else if (model.ProductID == 0) {
            dialog.setTitle(model.ProductName);
//			dialog.findViewById(R.id.layout_lavel).setVisibility(View.VISIBLE);
            ComboBoxAdapter adapterLevel = new ComboBoxAdapter(context, new ArrayList<ComboBoxModel>());
            adapterLevel.setNotifyOnChange(true);
            adapterLevel.add(new ComboBoxModel("0", "NA"));
            int CustLevel = Util.getIntegerPreference(context, "CustLevel");
            for (int i = 1; i <= CustLevel; i++) {
                adapterLevel.add(new ComboBoxModel(String.valueOf(i), "LEVEL " + i));
            }
            ((Spinner) dialog.findViewById(R.id.LevelID)).setAdapter(adapterLevel);
            int index = adapterLevel.indexOf(new ComboBoxModel(Util.NUMBER_FORMAT.format(model.LevelID)));
            ((Spinner) dialog.findViewById(R.id.LevelID)).setSelection(index);
        }
        if (model.Qty > 0) {
            ((EditText) dialog.findViewById(R.id.Qty)).setText(Util.NUMBER_FORMAT.format(model.Qty));
        }
        if (model.SellPrice > 0) {
            ((EditText) dialog.findViewById(R.id.SellPrice)).setText(Util.NUMBER_FORMAT.format(model.SellPrice));
        }
        if (model.BuyPrice > 0) {
            ((EditText) dialog.findViewById(R.id.BuyPrice)).setText(Util.NUMBER_FORMAT.format(model.BuyPrice));
        }
        ((EditText) dialog.findViewById(R.id.Note)).setText(model.Note);
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = ((EditText) dialog.findViewById(R.id.BuyPrice)).getText().toString();
                model.BuyPrice = Long.parseLong(s.equals("") ? "0" : s);
                s = ((EditText) dialog.findViewById(R.id.SellPrice)).getText().toString();
                model.SellPrice = Long.parseLong(s.equals("") ? "0" : s);
                s = ((EditText) dialog.findViewById(R.id.Qty)).getText().toString();
                model.Qty = Long.parseLong(s.equals("") ? "0" : s);
                model.Note = ((EditText) dialog.findViewById(R.id.Note)).getText().toString();
//				if(model.Qty == 0) {
//					info("Qty tidak boleh kosong atau 0!");
//					return;
//				}
//				if(model.BuyPrice == 0) {
//					info("Buy Price tidak boleh kosong atau 0!");
//					return;
//				}
//				if(model.SellPrice == 0) {
//					info("Sell Price tidak boleh kosong atau 0!");
//					return;
//				}
                if (model.ProductID == 0) {
                    model.LevelID = Integer.parseInt(((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.LevelID)).getSelectedItem()).value);
                } else {
                    model.LevelID = 0;
                }

                model.CommonID = cmModel.CommonID;

                saveToRealm(model);
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private static void saveToRealm(final PriceMonitoringModel model) {
        boolean needToAdd = false;
        Realm realm = Realm.getDefaultInstance();
        if (!(realm.where(PriceMonitoringModel.class)
                .equalTo("CommonID", cmModel.CommonID)
                .equalTo("SKUId", model.SKUId).findAll().size() > 0)) {
            needToAdd = true;
        }

        final boolean finalNeedToAdd = needToAdd;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                PriceMonitoringModel objectModel = realm.copyToRealmOrUpdate(model);

                if (finalNeedToAdd) {
                    Log.d("PM Added", "true");
                    cmModel.priceMonitoringModel.add(objectModel);
                }
            }
        });
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    @Override
    public void onResume() {
        realmUI = Realm.getDefaultInstance();
        if (pager != null) {
            pager.setCurrentItem(0);
        }
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        ctrlSKU = new CtrlSKU(getActivity());
        dbmaster = new DbMasterHelper(getActivity());
        ctrlAppModul = new CtrlAppModul(getActivity());

        realmUI = Realm.getDefaultInstance();
        produk = new Produk();
        competitor = new Competitor();
        listPriceMonitoring = new ListPriceMonitoring();
        adapter = new FragmentAdapter(getChildFragmentManager(), new ArrayList<BaseFragment>());
        cmModel = realmUI.where(CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        pager = (ViewPager) view.findViewById(R.id.pager);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        if (priceMonitoringCompetitorAdapter == null || priceMonitoringProdukAdapter == null)
        ((KunjunganActivity) getActivity()).loadPriceMonitoring();
//        else
//            loadAdapter();
    }

    public void loadAdapter() {
        priceMonitoringProdukAdapter = listener.getPriceMonitoringAdapter();
        priceMonitoringCompetitorAdapter = listener.getPriceCompetitorAdapter();

        adapter.add(competitor);
        adapter.add(produk);
        adapter.add(listPriceMonitoring);

        pager.setAdapter(adapter);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    loadOrder();
                }
                super.onPageSelected(position);
            }
        };
        pager.setOnPageChangeListener(ViewPagerListener);

        loadOrder();
    }

    private void loadOrder() {
        try {
            Realm realm = Realm.getDefaultInstance();
            CommonModel cmModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();

            listMonitoringPrice = new PriceMonitoringAdapter(getActivity(), new ArrayList<Object>(), new HashMap<Object, List<PriceMonitoringModel>>());
            for (int i = 0; i < priceMonitoringProdukAdapter.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = (GroupSKUModel) priceMonitoringProdukAdapter.getGroup(i);
                listMonitoringPrice.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < priceMonitoringProdukAdapter.getChildrenCount(i); j++) {
                    PriceMonitoringModel priceMonitoringModel = priceMonitoringProdukAdapter.getChild(i, j);
                    PriceMonitoringModel pm = null;
                    if ((pm = realm.where(PriceMonitoringModel.class)
                            .equalTo("SKUId", priceMonitoringModel.SKUId)
                            .equalTo("CommonID", cmModel.CommonID)
                            .findFirst()) != null) {
                        if (pm.BuyPrice > 0 || pm.SellPrice > 0 || pm.Qty > 0) {
                            listMonitoringPrice.addChild(groupSKUModel, pm);
                            flag = true;
                        }
                    }
                }
                if (flag == false) {
                    listMonitoringPrice.removeHeader(groupSKUModel);
                }
            }
            for (int i = 0; i < priceMonitoringCompetitorAdapter.getGroupCount(); i++) {
                GroupSKUComModel groupSKUModel = (GroupSKUComModel) priceMonitoringCompetitorAdapter.getGroup(i);
                listMonitoringPrice.addHeader(groupSKUModel);
                boolean flag = false;
                Realm re = Realm.getDefaultInstance();
                for (int j = 0; j < priceMonitoringCompetitorAdapter.getChildrenCount(i); j++) {
                    PriceMonitoringModel priceMonitoringModel = /*realm.copyFromRealm(*/priceMonitoringCompetitorAdapter.getChild(i, j)/*)*/;
                    PriceMonitoringModel pm = null;
                    if ((pm = re.where(PriceMonitoringModel.class)
                            .equalTo("SKUId", priceMonitoringModel.SKUId)
                            .equalTo("CommonID", cmModel.CommonID)
                            .findFirst()) != null) {
                        if (pm.BuyPrice > 0 || pm.SellPrice > 0 || pm.Qty > 0) {
                            listMonitoringPrice.addChild(groupSKUModel, pm);
                            flag = true;
                        }
                    }
                }
                if (flag == false) {
                    listMonitoringPrice.removeHeader(groupSKUModel);
                }
            }
            if (listPriceMonitoring.getList() != null) {
                listPriceMonitoring.getList().setAdapter(listMonitoringPrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getActivity(), e.getMessage());
        }
    }

    /*public JSONArray toJsonArray(String CommonID) {
        JSONArray array = new JSONArray();
        try {
            for (int i = 0; i < priceMonitoringProdukAdapter.getGroupCount(); i++) {
                for (int j = 0; j < priceMonitoringProdukAdapter.getChildrenCount(i); j++) {
                    PriceMonitoringModel priceMonitoringModel = priceMonitoringProdukAdapter.getChild(i, j);
                    if (priceMonitoringModel.BuyPrice > 0 || priceMonitoringModel.SellPrice > 0 || priceMonitoringModel.Qty > 0) {
                        array.put(priceMonitoringModel.toJson(CommonID));
                    }
                }
            }
            for (int i = 0; i < priceMonitoringCompetitorAdapter.getGroupCount(); i++) {
                for (int j = 0; j < priceMonitoringCompetitorAdapter.getChildrenCount(i); j++) {
                    PriceMonitoringModel priceMonitoringModel = priceMonitoringCompetitorAdapter.getChild(i, j);
                    if (priceMonitoringModel.BuyPrice > 0 || priceMonitoringModel.SellPrice > 0 || priceMonitoringModel.Qty > 0) {
                        array.put(priceMonitoringModel.toJson(CommonID));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }*/

    public PriceMonitoringAdapter getPriceMonitoringProdukAdapter() {
        return priceMonitoringProdukAdapter;
    }

    /*public void setPriceMonitoringProdukAdapter(
            PriceMonitoringAdapter priceMonitoringProdukAdapter) {
        this.priceMonitoringProdukAdapter = priceMonitoringProdukAdapter;
    }*/

    public PriceMonitoringAdapter getPriceMonitoringCompetitorAdapter() {
        return priceMonitoringCompetitorAdapter;
    }

    public PriceMonitoringAdapter getListMonitoringPrice() {
        return listMonitoringPrice;
    }

    /*public void setPriceMonitoringCompetitorAdapter(
            PriceMonitoringAdapter priceMonitoringCompetitorAdapter) {
        this.priceMonitoringCompetitorAdapter = priceMonitoringCompetitorAdapter;
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    public static class Produk extends BaseFragment {
        private ExpandableListView list;

        public Produk() {
            super(R.layout.fragment_price_monitoring_produk, "Produk");
        }

        @Override
        public void onCreateFragment(Bundle savedInstanceState) {
            list = (ExpandableListView) super.view.findViewById(R.id.list);
            if (priceMonitoringProdukAdapter != null) {
                list.setAdapter(priceMonitoringProdukAdapter);

                /*for (int i = 0; i < priceMonitoringProdukAdapter.getGroupCount(); i++) {
                    populateSKUByGroup((GroupSKUModel) priceMonitoringProdukAdapter.getGroup(i));
                }*/
            }
            list.setOnChildClickListener(new OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    PriceMonitoringAdapter adapter = (PriceMonitoringAdapter) parent.getExpandableListAdapter();
                    onPopUp(adapter.getChild(groupPosition, childPosition));
                    return false;
                }
            });
            list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    PriceMonitoringAdapter adapter = (PriceMonitoringAdapter) parent.getExpandableListAdapter();
                    populateSKUByGroup((GroupSKUModel) adapter.getGroup(groupPosition));
                    return false;
                }
            });
        }

        private void populateSKUByGroup(GroupSKUModel group) {
            Realm realm = Realm.getDefaultInstance();
            try {
//				String sqlx = "select * from producttarget where "
//						+ "CustID='"+customerModel.CustId+"' "
//						+ "and St_Target='0'";
//				Cursor cursorx = dbmaster.query(sqlx);
//				while(cursorx.moveToNext()) {
//					android.util.Log.e("skuid", ""+cursorx.getString(1));
//				}
                List<PriceMonitoringModel> list = priceMonitoringProdukAdapter.listOrderByGroup(group);
                if (list == null /*|| list.size() < group.jumlah*/) {
                    List<SKUModel> listSKU = ctrlSKU.listByGroupStok(group);
                    for (SKUModel skuModel : listSKU) {
                        if (ctrlAppModul.isModul("71")) {
                            Date date = new Date();
                            String sql = "select count(*) jml from producttarget where "
                                    + "CustID='" + customerModel.CustId + "' "
                                    + "and SkuID='" + skuModel.SKUId + "' "
                                    + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                                    + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                                    + "and St_Target='0'";
                            Cursor cursor = dbmaster.query(sql);
                            if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                                final PriceMonitoringModel[] model = new PriceMonitoringModel[1];
                                if ((model[0] = realm.where(PriceMonitoringModel.class)
                                        .equalTo("SkuID", skuModel.SKUId)
                                        .equalTo("CommonID", cmModel.CommonID)
                                        .findFirst()) == null) {
                                    model[0] = new PriceMonitoringModel(1, skuModel.ProductName);
                                    model[0].SKUId = skuModel.SKUId;
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            model[0] = realm.copyToRealm(model[0]);
                                        }
                                    });
                                }

                                priceMonitoringProdukAdapter.addChild(group, model[0]);
                                android.util.Log.e("filter sku monitoring", "filter sku monitoring");
                            }
                            cursor.close();
                            dbmaster.close();
                        } else {
                            final PriceMonitoringModel[] model = new PriceMonitoringModel[1];
                            Log.d("SKU ID", skuModel.SKUId);

                            model[0] = realm.where(PriceMonitoringModel.class)
                                    .equalTo("SKUId", skuModel.SKUId)
                                    .equalTo("CommonID", cmModel.CommonID)
                                    .findFirst();

                            if (model[0] == null) {
                                model[0] = new PriceMonitoringModel(1, skuModel.ProductName);
                                model[0].SKUId = skuModel.SKUId;
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        model[0] = realm.copyToRealm(model[0]);
                                    }
                                });
                            }
//                            PriceMonitoringModel model = new PriceMonitoringModel(1, skuModel.ProductName);
                            priceMonitoringProdukAdapter.addChild(group, model[0]);

                            android.util.Log.e("ga filter", "ga filter sku monitoring");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Util.showDialogError(getActivity(), e.getMessage());
            }
        }
    }

    public static class Competitor extends BaseFragment {
        private ExpandableListView list;
        private CtrlSKUCom ctrlSKUCom;

        Realm realmO;

        public Competitor() {
            super(R.layout.fragment_price_monitoring_competitor, "Competitor");
            realmO = Realm.getDefaultInstance();
        }

        @Override
        public void onCreateFragment(Bundle savedInstanceState) {
            list = (ExpandableListView) super.view.findViewById(R.id.list);

            if (priceMonitoringCompetitorAdapter != null) {
                list.setAdapter(priceMonitoringCompetitorAdapter);
            }

            ctrlSKUCom = new CtrlSKUCom(getActivity());

            list.setOnChildClickListener(new OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    PriceMonitoringAdapter adapter = (PriceMonitoringAdapter) parent.getExpandableListAdapter();
                    onPopUp(adapter.getChild(groupPosition, childPosition));
                    return false;
                }
            });

            list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    PriceMonitoringAdapter adapter = (PriceMonitoringAdapter) parent.getExpandableListAdapter();
                    populateSKUByGroup((GroupSKUComModel) adapter.getGroup(groupPosition));
                    return false;
                }
            });
        }

        private void populateSKUByGroup(GroupSKUComModel group) {
            List<SKUComModel> listSKU = ctrlSKUCom.listByGroup(group);
            /*List<PriceMonitoringModel> listA = priceMonitoringCompetitorAdapter.listOrderByGroup(group);*/
            Log.d("List SKU Comp", "Size " + listSKU.size());
            for (SKUComModel skuModel : listSKU) {
                if (ctrlAppModul.isModul("72")) {
                    Date date = new Date();
                    String sql = "select count(*) jml from producttarget where "
                            + "CustID='" + customerModel.CustId + "' "
                            + "and SkuID='" + skuModel.SKUId + "' "
                            + "and Month='" + new SimpleDateFormat("MM").format(date) + "' "
                            + "and Year='" + new SimpleDateFormat("yyyy").format(date) + "' "
                            + "and St_Target='1'";
                    Cursor cursor = dbmaster.query(sql);
                    if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                        final PriceMonitoringModel[] model = {realmO.where(PriceMonitoringModel.class)
                                .equalTo("SKUId", skuModel.SKUId)
                                .equalTo("CommonID", cmModel.CommonID)
                                .findFirst()};

                        if (model[0] == null) {
                            model[0] = new PriceMonitoringModel(0, skuModel.ProductName);
                            model[0].SKUId = skuModel.SKUId;
                            final PriceMonitoringModel finalModel = model[0];
                            realmO.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    model[0] = realm.copyToRealm(finalModel);
                                }
                            });
                        }
                        priceMonitoringCompetitorAdapter.addChild(group, model[0]);
                        cursor.close();
                        dbmaster.close();
                    }

                } else {
                    final PriceMonitoringModel[] model = {realmO.where(PriceMonitoringModel.class)
                            .equalTo("SKUId", skuModel.SKUId)
                            .equalTo("CommonID", cmModel.CommonID)
                            .findFirst()};

                    if (model[0] == null) {
                        Log.d("Model Not In Database", "true");
                        model[0] = new PriceMonitoringModel(0, skuModel.ProductName);
                        model[0].SKUId = skuModel.SKUId;
                        final PriceMonitoringModel finalModel = model[0];
                        realmO.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model[0] = realm.copyToRealm(finalModel);
                            }
                        });
                    }
                    priceMonitoringCompetitorAdapter.addChild(group, model[0]);

                }
            }
        }
    }

    public static class ListPriceMonitoring extends BaseFragment {
        private ExpandableListView list;

        public ListPriceMonitoring() {
            super(R.layout.fragment_price_monitoring_list, "List");
        }

        @Override
        public void onCreateFragment(Bundle savedInstanceState) {
            list = (ExpandableListView) super.view.findViewById(R.id.list);
            list.setOnChildClickListener(new OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    PriceMonitoringAdapter adapter = (PriceMonitoringAdapter) parent.getExpandableListAdapter();
                    onPopUp(adapter.getChild(groupPosition, childPosition));
                    return false;
                }
            });
        }

        public ExpandableListView getList() {
            return list;
        }
    }
}
