package com.bhn.sadix.fragment.kunjungan;

import java.util.List;

import org.json.JSONArray;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.CapasityAdapter;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.model.CapasityModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

public class CapasityFragment extends BaseFragment {
    private CapasityAdapter adapter;
    private ExpandableListView list;
    private CtrlSKU ctrlSKU;


    public CapasityFragment() {
        super(R.layout.fragment_capasity, "");
    }

    public void setAdapter(CapasityAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlSKU = new CtrlSKU(getActivity());
    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        list = (ExpandableListView) view.findViewById(R.id.list);
        if (adapter != null) {
            list.setAdapter(adapter);
        }
        list.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CapasityAdapter adapter = (CapasityAdapter) parent.getExpandableListAdapter();
                onPopUp(adapter.getChild(groupPosition, childPosition));
                return false;
            }
        });
        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CapasityAdapter adapter = (CapasityAdapter) parent.getExpandableListAdapter();
                populateSKUByGroup(adapter.getGroup(groupPosition));
                return false;
            }
        });
    }

    private void populateSKUByGroup(GroupSKUModel group) {
        try {
            List<CapasityModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroupStok(group);
                for (SKUModel skuModel : listSKU) {
                    CapasityModel model = new CapasityModel(skuModel);
                    adapter.addChild(group, model);
                }
            }
        } catch (Exception e) {
            Util.showDialogError(getActivity(), e.getMessage());
        }
    }

    private void onPopUp(final CapasityModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_capasity);
        dialog.setTitle(model.skuModel.ProductName);
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setData(model, dialog);
                dialog.dismiss();
            }
        });

        if (model.details.get(0).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_januari)).setText(Util.NUMBER_FORMAT.format(model.details.get(0).Qty));
        }
        if (model.details.get(0).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_januari)).setText(Util.NUMBER_FORMAT.format(model.details.get(0).Price));
        }

        if (model.details.get(1).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_februari)).setText(Util.NUMBER_FORMAT.format(model.details.get(1).Qty));
        }
        if (model.details.get(1).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_februari)).setText(Util.NUMBER_FORMAT.format(model.details.get(1).Price));
        }

        if (model.details.get(2).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_maret)).setText(Util.NUMBER_FORMAT.format(model.details.get(2).Qty));
        }
        if (model.details.get(2).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_maret)).setText(Util.NUMBER_FORMAT.format(model.details.get(2).Price));
        }

        if (model.details.get(3).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_april)).setText(Util.NUMBER_FORMAT.format(model.details.get(3).Qty));
        }
        if (model.details.get(3).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_april)).setText(Util.NUMBER_FORMAT.format(model.details.get(3).Price));
        }

        if (model.details.get(4).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_mei)).setText(Util.NUMBER_FORMAT.format(model.details.get(4).Qty));
        }
        if (model.details.get(4).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_mei)).setText(Util.NUMBER_FORMAT.format(model.details.get(4).Price));
        }

        if (model.details.get(5).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_juni)).setText(Util.NUMBER_FORMAT.format(model.details.get(5).Qty));
        }
        if (model.details.get(5).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_juni)).setText(Util.NUMBER_FORMAT.format(model.details.get(5).Price));
        }

        if (model.details.get(6).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_juli)).setText(Util.NUMBER_FORMAT.format(model.details.get(6).Qty));
        }
        if (model.details.get(6).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_juli)).setText(Util.NUMBER_FORMAT.format(model.details.get(6).Price));
        }

        if (model.details.get(7).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_agustus)).setText(Util.NUMBER_FORMAT.format(model.details.get(7).Qty));
        }
        if (model.details.get(7).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_agustus)).setText(Util.NUMBER_FORMAT.format(model.details.get(7).Price));
        }

        if (model.details.get(8).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_september)).setText(Util.NUMBER_FORMAT.format(model.details.get(8).Qty));
        }
        if (model.details.get(8).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_september)).setText(Util.NUMBER_FORMAT.format(model.details.get(8).Price));
        }

        if (model.details.get(9).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_oktober)).setText(Util.NUMBER_FORMAT.format(model.details.get(9).Qty));
        }
        if (model.details.get(9).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_oktober)).setText(Util.NUMBER_FORMAT.format(model.details.get(9).Price));
        }

        if (model.details.get(10).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_november)).setText(Util.NUMBER_FORMAT.format(model.details.get(10).Qty));
        }
        if (model.details.get(10).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_november)).setText(Util.NUMBER_FORMAT.format(model.details.get(10).Price));
        }

        if (model.details.get(11).Qty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_desember)).setText(Util.NUMBER_FORMAT.format(model.details.get(11).Qty));
        }
        if (model.details.get(11).Price > 0) {
            ((EditText) dialog.findViewById(R.id.price_desember)).setText(Util.NUMBER_FORMAT.format(model.details.get(11).Price));
        }

        ((EditText) dialog.findViewById(R.id.qty_januari)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_januari)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_februari)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_februari)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_maret)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_maret)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_april)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_april)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_mei)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_mei)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_juni)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_juni)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_juli)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_juli)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_agustus)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_agustus)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_september)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_september)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_oktober)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_oktober)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_november)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_november)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.qty_desember)).addTextChangedListener(new EventText(model, dialog));
        ((EditText) dialog.findViewById(R.id.price_desember)).addTextChangedListener(new EventText(model, dialog));

        total(model, dialog);

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    class EventText implements TextWatcher {
        private CapasityModel model;
        private Dialog dialog;

        public EventText(CapasityModel model, Dialog dialog) {
            this.model = model;
            this.dialog = dialog;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            setData(model, dialog);
            total(model, dialog);
        }
    }

    private void total(CapasityModel model, Dialog dialog) {
        int totalqty = 0;
        int totalprice = 0;
        for (CapasityModel.Detail detail : model.details) {
            totalqty += detail.Qty;
            totalprice += detail.Price;
        }
        if (totalqty > 0) {
            ((EditText) dialog.findViewById(R.id.qty_total)).setText(Util.NUMBER_FORMAT.format(totalqty));
        }
        if (totalprice > 0) {
            ((EditText) dialog.findViewById(R.id.price_total)).setText(Util.NUMBER_FORMAT.format(totalprice));
        }
    }

    private void setData(CapasityModel model, Dialog dialog) {
        String s = ((EditText) dialog.findViewById(R.id.qty_januari)).getText().toString();
        model.details.get(0).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_januari)).getText().toString();
        model.details.get(0).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_februari)).getText().toString();
        model.details.get(1).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_februari)).getText().toString();
        model.details.get(1).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_maret)).getText().toString();
        model.details.get(2).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_maret)).getText().toString();
        model.details.get(2).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_april)).getText().toString();
        model.details.get(3).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_april)).getText().toString();
        model.details.get(3).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_mei)).getText().toString();
        model.details.get(4).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_mei)).getText().toString();
        model.details.get(4).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_juni)).getText().toString();
        model.details.get(5).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_juni)).getText().toString();
        model.details.get(5).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_juli)).getText().toString();
        model.details.get(6).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_juli)).getText().toString();
        model.details.get(6).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_agustus)).getText().toString();
        model.details.get(7).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_agustus)).getText().toString();
        model.details.get(7).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_september)).getText().toString();
        model.details.get(8).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_september)).getText().toString();
        model.details.get(8).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_oktober)).getText().toString();
        model.details.get(9).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_oktober)).getText().toString();
        model.details.get(9).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_november)).getText().toString();
        model.details.get(10).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_november)).getText().toString();
        model.details.get(10).Price = Long.parseLong(s.equals("") ? "0" : s);

        s = ((EditText) dialog.findViewById(R.id.qty_desember)).getText().toString();
        model.details.get(11).Qty = Long.parseLong(s.equals("") ? "0" : s);
        s = ((EditText) dialog.findViewById(R.id.price_desember)).getText().toString();
        model.details.get(11).Price = Long.parseLong(s.equals("") ? "0" : s);
    }

    public JSONArray toJsonArray() {
        JSONArray array = new JSONArray();
        try {
            for (int i = 0; i < adapter.getGroupCount(); i++) {
                for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                    CapasityModel capasityModel = adapter.getChild(i, j);
                    for (CapasityModel.Detail detail : capasityModel.details) {
                        if (detail.Price > 0 || detail.Qty > 0) {
                            array.put(detail.toJson());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

}
