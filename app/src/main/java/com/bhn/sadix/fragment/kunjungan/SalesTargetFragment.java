package com.bhn.sadix.fragment.kunjungan;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.fragment.customer.FragmentAdapter;
import com.bhn.sadix.model.TargetComModel;
import com.bhn.sadix.model.TargetModel;

import org.json.JSONArray;

import java.util.ArrayList;

public class SalesTargetFragment extends BaseFragment {
    private FragmentAdapter adapter;
    private ViewPager pager;
    private TargetFragment targetFragment;
    private TargetComFragment targetComFragment;
    private DbMasterHelper ctrlDb;

    public SalesTargetFragment() {
        super(R.layout.fragment_sales_target, "");
    }

    public void setTargetFragment(TargetFragment targetFragment) {
        this.targetFragment = targetFragment;
    }

    public void setTargetComFragment(TargetComFragment targetComFragment) {
        this.targetComFragment = targetComFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlDb = new DbMasterHelper(getActivity());
        adapter = new FragmentAdapter(getChildFragmentManager(), new ArrayList<BaseFragment>());
        adapter.add(targetFragment);
        adapter.add(targetComFragment);
    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        pager = (ViewPager) view.findViewById(R.id.pager);
        pager.setAdapter(adapter);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }
        };
        pager.setOnPageChangeListener(ViewPagerListener);
    }

    public JSONArray toJsonArray(String CommonID) {
        JSONArray array = new JSONArray();
        try {
            for (int i = 0; i < targetFragment.getAdapter().getGroupCount(); i++) {
                for (int j = 0; j < targetFragment.getAdapter().getChildrenCount(i); j++) {
                    TargetModel targetModel = targetFragment.getAdapter().getChild(i, j);
                    for (TargetModel.Detail detail : targetModel.details) {
                        if (detail.Price > 0 || detail.Qty > 0) {
                            array.put(detail.toJson(CommonID));
                        }
                    }
                }
            }
            for (int i = 0; i < targetComFragment.getAdapter().getGroupCount(); i++) {
                for (int j = 0; j < targetComFragment.getAdapter().getChildrenCount(i); j++) {
                    TargetComModel targetModel = targetComFragment.getAdapter().getChild(i, j);
                    for (TargetComModel.Detail detail : targetModel.details) {
                        if (detail.Price > 0 || detail.Qty > 0) {
                            array.put(detail.toJson(CommonID));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

    public void saveProducttarget() {
        try {
            for (int i = 0; i < targetFragment.getAdapter().getGroupCount(); i++) {
                for (int j = 0; j < targetFragment.getAdapter().getChildrenCount(i); j++) {
                    TargetModel targetModel = targetFragment.getAdapter().getChild(i, j);
                    for (TargetModel.Detail detail : targetModel.details) {
                        if (detail.Price > 0 || detail.Qty > 0) {
                            ctrlDb.save(detail.toSaveSalesTarget(), "producttarget");
                        }
                    }
                }
            }
            for (int i = 0; i < targetComFragment.getAdapter().getGroupCount(); i++) {
                for (int j = 0; j < targetComFragment.getAdapter().getChildrenCount(i); j++) {
                    TargetComModel targetModel = targetComFragment.getAdapter().getChild(i, j);
                    for (TargetComModel.Detail detail : targetModel.details) {
                        if (detail.Price > 0 || detail.Qty > 0) {
                            ctrlDb.save(detail.toSaveSalesTarget(), "producttarget");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
