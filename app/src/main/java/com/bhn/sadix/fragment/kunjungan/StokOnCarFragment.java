package com.bhn.sadix.fragment.kunjungan;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.StokOnCarAdapter;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.StokOnCarModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.HashMap;
import java.util.List;

public class StokOnCarFragment extends Fragment {
    private View view = null;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private ProgressDialog progressDialog;
    private StokOnCarAdapter adapter;
    private StokOnCarAdapter adapterOrder;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlSKU ctrlSKU;
    private View vStok;
    private View vOrder;
    private Context context;
    private CustomerModel customerModel;
    private KunjunganListener listener;

    //	public StokOnCarFragment(Context context, CustomerModel customerModel) {
//		this.context = context;
//		this.customerModel = customerModel;
//        ctrlGroupSKU = new CtrlGroupSKU(context);
//        ctrlSKU = new CtrlSKU(context);
//        progressDialog = new ProgressDialog(context) {
//			@Override
//			public boolean onKeyDown(int keyCode, KeyEvent event) {
//				if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
//					dismiss();
//				}
//				return super.onKeyDown(keyCode, event);
//			}
//			
//		};
//		progressDialog.setCancelable(false);
//		progressDialog.setMessage("Loading Data...");
//	}

    public static StokOnCarFragment newInstance() {
        StokOnCarFragment fragment = new StokOnCarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlGroupSKU = new CtrlGroupSKU(context);
        ctrlSKU = new CtrlSKU(context);
        progressDialog = new ProgressDialog(context) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }
        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
        customerModel = listener.getCustomerModel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_stock_on_car, container, false);
        view.findViewById(R.id.toolbar).setVisibility(View.GONE);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_stok) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_order) {
                    mPager.setCurrentItem(1);
                }
            }
        });
        ((RadioButton) view.findViewById(R.id.tab_stok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        ((RadioButton) view.findViewById(R.id.tab_order)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) view.findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
//					loadStok();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_stok);
                } else if (position == 1) {
//                	loadOrder();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_order);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = listener.getStokOnCarAdapter();

        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokOnCar();
            }
        }
    }

    public void loadAdapter() {
        adapter = listener.getStokOnCarAdapter();

        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokOnCar();
            }
        } else {
            pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);
        }
    }

    protected void populateSKUByGroup(GroupSKUModel group) {
        try {
            if (adapter.listOrderByGroup(group) == null) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    StokOnCarModel stockOutletModel = new StokOnCarModel(skuModel, skuModel.QBESAR, skuModel.QKECIL);
                    adapter.addChild(group, stockOutletModel, false);
                }
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    protected void populateSKUByGroupOrder(GroupSKUModel group) {
        try {
            if (adapterOrder.listOrderByGroup(group) == null) {
                List<SKUModel> listSKU = ctrlSKU.listByGroupStokOnCar(group);
                for (SKUModel skuModel : listSKU) {
                    StokOnCarModel stockOutletModel = new StokOnCarModel(skuModel, skuModel.QOBESAR, skuModel.QOKECIL);
                    adapterOrder.addChild(group, stockOutletModel, false);
                }
            }
        } catch (Exception e) {
            Util.showDialogError(context, e.getMessage());
        }
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);
            if (position == 0) {
                vStok = v;
                vStok.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vStok.findViewById(R.id.list)).setAdapter(adapter);
                ((ExpandableListView) vStok.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        populateSKUByGroup(adapter.getGroup(groupPosition));
                        return false;
                    }
                });
                populateSku(adapter.getGroupCount(), 0);
//	    		if(adapter == null) {
//	    			load();
//	    		} else {
//	    			loadStok();
//	    		}
            } else if (position == 1) {
                vOrder = v;
                vOrder.findViewById(R.id.list).setVisibility(View.VISIBLE);
                adapterOrder = new StokOnCarAdapter(context, ctrlGroupSKU.listStokOnCar(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        populateSKUByGroupOrder(adapterOrder.getGroup(groupPosition));
                        return false;
                    }
                });
                populateSku(adapterOrder.getGroupCount(), 1);
//	    		loadOrder();
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    private void populateSku(int groupCount, int i) {
        if (i == 0) {
            for (int j = 0; j < groupCount; j++) {
                populateSKUByGroup(adapter.getGroup(j));
            }
        } else {
            for (int j = 0; j < groupCount; j++) {
                populateSKUByGroupOrder(adapter.getGroup(j));
            }
        }
    }
//	protected void loadOrder() {
//		progressDialog.show();
//		new LoadOrderData().execute();
//	}
//	protected void loadStok() {
//		try {
//			if(vStok != null) {
//				((ExpandableListView)vStok.findViewById(R.id.list)).setAdapter(adapter);
//			}
//		} catch (Exception e) {
//			Util.showDialogError(context, e.getMessage());
//		}
//	}
//	private void load() {
//		progressDialog.show();
//		new LoadData().execute();
//	}
//	private class LoadData extends AsyncTask<Void, Void, String> {
//		@Override
//		protected String doInBackground(Void... params) {
//			try {
//				adapter = new StokOnCarAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
//				List<GroupSKUModel> list = ctrlGroupSKU.list();
//				for (GroupSKUModel groupSKUModel : list) {
//					adapter.addHeader(groupSKUModel, false);
//					List<SKUModel> listSKU = ctrlSKU.listByGroup(groupSKUModel);
//					for (SKUModel skuModel : listSKU) {
//						StokOnCarModel stockOutletModel = new StokOnCarModel(skuModel, skuModel.QBESAR, skuModel.QKECIL);
//						adapter.addChild(groupSKUModel, stockOutletModel, false);
//					}
//				}
//			} catch (Exception e) {
//				Log.e("Exception1", e.getMessage());
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(String result) {
//			progressDialog.dismiss();
//			if(vStok != null) {
//				((ExpandableListView)vStok.findViewById(R.id.list)).setAdapter(adapter);
//				adapter.notifyDataSetChanged();
//			}
//			super.onPostExecute(result);
//		}
//	}
//	private class LoadOrderData extends AsyncTask<Void, Void, String> {
//		@Override
//		protected String doInBackground(Void... params) {
//			try {
//				adapterOrder = new StokOnCarAdapter(context, ctrlGroupSKU.listStokOnCar(), new HashMap<GroupSKUModel, List<StokOnCarModel>>());
//				for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
//					GroupSKUModel groupSKUModel = adapterOrder.getGroup(i);
//					List<SKUModel> list = ctrlSKU.listByGroupStokOnCar(groupSKUModel);
//					for (SKUModel skuModel : list) {
//						adapterOrder.addChild(groupSKUModel, new StokOnCarModel(
//							skuModel, 
//							skuModel.QOBESAR, 
//							skuModel.QOKECIL
//						), false);
//					}
//				}
//			} catch (Exception e) {
//				Log.e("Exception2", e.getMessage());
//			}
//			return null;
//		}
//		@Override
//		protected void onPostExecute(String result) {
//			progressDialog.dismiss();
//			if(vOrder != null) {
//				((ExpandableListView)vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
//				adapterOrder.notifyDataSetChanged();
//			}
//			super.onPostExecute(result);
//		}
//	}


    @Override
    public void onResume() {
        super.onResume();
        loadAdapter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
