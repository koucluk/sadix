package com.bhn.sadix.fragment.customer;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.CustPICAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;

public class FragmnetAddPIC extends BaseFragment {
	private CustPICAdapter adapter;
	private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;
	
	private CustPIC custPIC;
    private ImageViewTouch profileImg;
    private CustomerModel customerModel;
    private List<CustPIC> list;
    private AddPhotoInterface event;
	public FragmnetAddPIC() {
		super(R.layout.fragment_cust_pic, "PIC");
	}

	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}

	public List<CustPIC> getList() {
		return list;
	}

	public void setList(List<CustPIC> list) {
		this.list = list;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctrlConfig = new CtrlConfig(getActivity());
		adapter = new CustPICAdapter(getActivity(), new ArrayList<CustPIC>());
		adapter.setNotifyOnChange(true);
		ctrlAppModul = new CtrlAppModul(getActivity());
		if(list != null) {
			adapter.setList(list);
		}
	}
	public void addAddPhotoInterface(AddPhotoInterface event) {
		this.event = event;
	}

	@Override
	public void onCreateFragment(Bundle savedInstanceState) {
		((ListView)view.findViewById(R.id.list)).setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				showCustPIC(adapter.getItem(position), false);
			}
		});
		((ListView)view.findViewById(R.id.list)).setAdapter(adapter);
		view.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addCustPIC();
			}
		});
	}

	private void addCustPIC() {
		CustPIC custPIC = new CustPIC();
//		adapter.add(custPIC);
		showCustPIC(custPIC, true);
	}

	public CustPICAdapter getAdapter() {
		return adapter;
	}

	private void showCustPIC(final CustPIC item, final boolean isAdd) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.from_add_cust_pic);
		profileImg = (ImageViewTouch) dialog.findViewById(R.id.profileImg);
		profileImg.setDisplayType( DisplayType.FIT_IF_BIGGER );
		if(item.bitmap != null) {
			profileImg.setImageBitmap(item.bitmap);
		}
		custPIC = item;
		dialog.findViewById(R.id.btn_photo).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(event == null) {
					Intent intent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent, 200);
				} else {
					event.addPhoto(item, (ImageView) dialog.findViewById(R.id.profileImg));
				}
			}
		});
		dialog.setTitle("ADD PIC");
		final EditText PicBirthDate = ((EditText)dialog.findViewById(R.id.PicBirthDate));
		((EditText)dialog.findViewById(R.id.PicName)).setText(item.PicName);
		((EditText)dialog.findViewById(R.id.PicIdNo)).setText(item.PicIdNo);
		((EditText)dialog.findViewById(R.id.PicAddres)).setText(item.PicAddres);
		((EditText)dialog.findViewById(R.id.PicPhone)).setText(item.PicPhone);
		((EditText)dialog.findViewById(R.id.PicFax)).setText(item.PicFax);
		((EditText)dialog.findViewById(R.id.PicEmail)).setText(item.PicEmail);
		((EditText)dialog.findViewById(R.id.PicRemark)).setText(item.PicRemark);
		((EditText)dialog.findViewById(R.id.PicBirthPlace)).setText(item.PicBirthPlace);
		PicBirthDate.setText(item.PicBirthDate);
		((EditText)dialog.findViewById(R.id.PicHobby)).setText(item.PicHobby);
		ComboBoxAdapter adapter = new ComboBoxAdapter(getActivity(), ctrlConfig.listComboBox("PIC_TYPE"));
		((Spinner)dialog.findViewById(R.id.PicType)).setAdapter(adapter);
		int index = adapter.indexOf(new ComboBoxModel(String.valueOf(item.PicType)));
		if(index != -1) {
			((Spinner)dialog.findViewById(R.id.PicType)).setSelection(index);
		}
		String PicReligion = (String) ((Spinner)dialog.findViewById(R.id.PicReligion)).getSelectedItem();
		if(PicReligion != null) {
			if(PicReligion.equals("Islam")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(0);
			if(PicReligion.equals("Kristen Protestan")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(1);
			if(PicReligion.equals("Katolik")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(2);
			if(PicReligion.equals("Hindu")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(3);
			if(PicReligion.equals("Buddha")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(4);
			if(PicReligion.equals("Kong Hu Cu")) ((Spinner)dialog.findViewById(R.id.PicReligion)).setSelection(5);
		}
        ((ImageButton)dialog.findViewById(R.id.btn_tgl)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popupBirthDate(PicBirthDate);
			}
		});
		((Button)dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		((Button)dialog.findViewById(R.id.btn_save)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				item.CustRandomID = customerModel.RandomID;
				item.PicBirthDate = ((EditText)dialog.findViewById(R.id.PicBirthDate)).getText().toString();
				item.PicName = ((EditText)dialog.findViewById(R.id.PicName)).getText().toString();
				item.PicIdNo = ((EditText)dialog.findViewById(R.id.PicIdNo)).getText().toString();
				item.PicAddres = ((EditText)dialog.findViewById(R.id.PicAddres)).getText().toString();
				item.PicPhone = ((EditText)dialog.findViewById(R.id.PicPhone)).getText().toString();
				item.PicFax = ((EditText)dialog.findViewById(R.id.PicFax)).getText().toString();
				item.PicEmail = ((EditText)dialog.findViewById(R.id.PicEmail)).getText().toString();
				item.PicRemark = ((EditText)dialog.findViewById(R.id.PicRemark)).getText().toString();
				item.PicBirthPlace = ((EditText)dialog.findViewById(R.id.PicBirthPlace)).getText().toString();
				item.PicHobby = ((EditText)dialog.findViewById(R.id.PicHobby)).getText().toString();
				item.PicType = Integer.parseInt(((ComboBoxModel)((Spinner)dialog.findViewById(R.id.PicType)).getSelectedItem()).value);
				item.PicReligion = ((Spinner)dialog.findViewById(R.id.PicReligion)).getSelectedItem().toString();
				if(isAdd) {
					FragmnetAddPIC.this.adapter.add(item);
				} else {
					FragmnetAddPIC.this.adapter.notifyDataSetChanged();
				}
				dialog.dismiss();
			}
		});
		dialog.show();
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
	}
	private void popupBirthDate(final EditText text) {
		try {
//			String tglLahir = ((EditText)findViewById(R.id.OwnerBirthDate)).getText().toString();
			final Calendar cal = Calendar.getInstance();
//			if(tglLahir.trim().equals("")) {
//				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
//			} else {
//				String[] arr = tglLahir.split("-");
//				cal.set(Integer.parseInt(arr[0]), Integer.parseInt(arr[1])-1, Integer.parseInt(arr[2]));
//			}
			final DatePickerDialog tglDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
		    	@Override
				public void onDateSet(DatePicker view, int year, int month, int day) {
					Calendar calData = Calendar.getInstance();
					calData.set(year, month, day);
					text.setText(Util.dateFormatBirthDate.format(calData.getTime()));
				}
			}, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			tglDialog.show();
		} catch (Exception e) {
			Util.Toast(getActivity(), e.getMessage());
		}
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if(requestCode == 200) {
			if(resultCode == Activity.RESULT_OK) {
				try {
					Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
					bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
					ByteArrayOutputStream bao = new ByteArrayOutputStream();
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
					custPIC.bitmap = bitmap;
					custPIC.data = bao.toByteArray();
					profileImg.setImageBitmap(bitmap);
					bao.flush();
					bao.close();
					bao = null;
				} catch (Exception e) {
					Util.showDialogError(getActivity(), e.getMessage());
				}
			}
		}
	}
	public interface AddPhotoInterface {
		public void addPhoto(CustPIC item, ImageView img);
	}

}
