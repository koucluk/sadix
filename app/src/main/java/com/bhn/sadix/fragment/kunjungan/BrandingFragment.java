package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.model.CustBrandingModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.widget.ItemPhotoCustomerBrandingWidget;
import com.bhn.sadix.widget.ItemPhotoCustomerBrandingWidgetListener;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

//import com.bhn.sadix.model.CommonModel;

public class BrandingFragment extends Fragment {
    public static boolean needToSave = true;
    private View view = null;
    private CustomerModel customerModel;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    private ItemPhotoCustomerBrandingWidgetListener listener;
    private KunjunganListener kunjunganListener;
    private LinearLayout panelFoto;
    private List<CustBrandingModel> listBrandingModel;
    private CtrlCustBranding ctrlCustBranding;
    private Realm realmUI;

    public static BrandingFragment newInstance() {
        BrandingFragment fragment = new BrandingFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customerModel = kunjunganListener.getCustomerModel();
        ctrlCustBranding = new CtrlCustBranding(getActivity());
        realmUI = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_customer_branding, container, false);
        panelFoto = ((LinearLayout) view.findViewById(R.id.panel_photo));
        ((ImageButton) view.findViewById(R.id.btn_add_photo)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addPhoto();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerModel = kunjunganListener.getCustomerModel();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        loadCache();
    }

    private void loadCache() {
//        listBrandingModel = ctrlCustBranding.list(customerModel.CustId, commonModel.CommonID);

        RealmResults<CustomerBrandingModel> brandingModels = realmUI.where(CustomerBrandingModel.class)
                .equalTo("CustomerID", customerModel.CustId)
                .equalTo("CommonID", commonModel.CommonID)
                .equalTo("Done", 0)
                .findAll();

        if (brandingModels != null && brandingModels.size() > 0) {
            Log.wtf("listBranding cache", "not null - " + brandingModels.size());
            for (CustomerBrandingModel model : brandingModels) {
                ItemPhotoCustomerBrandingWidget widget = new ItemPhotoCustomerBrandingWidget(getActivity());
//                widget.setCommonModel(commonModel);
                widget.setCustomerModel(customerModel);
                widget.addItemPhotoCustomerBrandingWidgetListener(listener);
                widget.setBrandingModel(model);

                panelFoto.addView(widget);
            }
        } else {
            Log.wtf("listBranding cache", "is null");
        }
    }

    public void addPhoto(ItemPhotoCustomerBrandingWidget widget) {

    }

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }*/

    public void remove(ItemPhotoCustomerBrandingWidget widget, boolean removeDb) {
        /*if (removeDb)
            ctrlCustBranding.delete(customerModel.CustId, widget.getBrandingID(), commonModel.CommonID, true);*/
        ((LinearLayout) view.findViewById(R.id.panel_photo)).removeView(widget);
    }

    private void addPhoto() {
        ItemPhotoCustomerBrandingWidget widget = new ItemPhotoCustomerBrandingWidget(getActivity());
        widget.setCustomerModel(customerModel);
//        widget.setCommonModel(commonModel);
        widget.addItemPhotoCustomerBrandingWidgetListener(listener);

        panelFoto.addView(widget);
    }

    public int getWidgetCount() {
        return panelFoto.getChildCount();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("On Pause", "Called");
        if (needToSave) {
            if (panelFoto.getChildCount() > 0) {
                for (int i = 0; i < panelFoto.getChildCount(); i++) {
                    Log.d("On Detach", "saving widget " + i);
                    ItemPhotoCustomerBrandingWidget widget = (ItemPhotoCustomerBrandingWidget) panelFoto.getChildAt(i);
                    if (!widget.isDone) {
                        Log.d("Branding save", "save to lokal from fragment pause");
                        widget.saveLokal(false, i);
                    }
                }
            }
        }

        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI = Realm.getDefaultInstance();
//        loadCache();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ItemPhotoCustomerBrandingWidgetListener) context;
        kunjunganListener = (KunjunganListener) context;
    }

    /*@Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("On Destroy", "Called");
    }*/

    /*@Override
    public void onDestroyView() {
        Log.d("On Detach", "panel Foto child " + panelFoto.getChildCount());
        Log.d("On DestroyView", "Called");
        if (panelFoto.getChildCount() > 0) {
            for (int i = 0; i < panelFoto.getChildCount(); i++) {
                Log.d("On Detach", "saving widget " + i);
                ItemPhotoCustomerBrandingWidget widget = (ItemPhotoCustomerBrandingWidget) panelFoto.getChildAt(i);
                widget.saveLokal();
            }
        }
        super.onDestroyView();
    }*/

    /*@Override
    public void onDetach() {
        Log.d("On Detach", "Called");
        super.onDetach();
    }*/
}
