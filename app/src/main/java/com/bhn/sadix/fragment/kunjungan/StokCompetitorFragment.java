package com.bhn.sadix.fragment.kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.StokCompetitorAdapter;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.database.CtrlStokCompetitor;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.StokCompetitorModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class StokCompetitorFragment extends Fragment {
    private View view = null;
    private StokCompetitorAdapter adapterAll = null;
    private StokCompetitorAdapter adapterStok = null;
    private StokCompetitorAdapter adapterSearch = null;
    private ExpandableListView listAll;
    private ExpandableListView listStok;
    private Context context;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;

    private KunjunganListener listener;

    private CommonModel commonModel;
    private CustomerModel customerModel;
    Realm realmUI;
    private CtrlSKUCom ctrlSKUCom;
    private com.bhn.sadix.database.CtrlStokCompetitor ctrlStokCompetitor;

    public StokCompetitorAdapter getStokCompAdapter() {
        return adapterAll;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_stok_com, container, false);

        realmUI = Realm.getDefaultInstance();

        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        customerModel = listener.getCustomerModel();

        ctrlSKUCom = new CtrlSKUCom(getActivity());
        ctrlStokCompetitor = new CtrlStokCompetitor(getActivity());

        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_all_product) {
//					flageStok = false;
//					loadAllProduk();
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_stok) {
//					flageStok = true;
//					loadStok();
                    mPager.setCurrentItem(1);
                }
            }
        });
        view.findViewById(R.id.tab_all_product).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				flageStok = false;
//				loadAllProduk();
                mPager.setCurrentItem(0);
            }
        });
        view.findViewById(R.id.tab_stok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//				flageStok = true;
//				loadStok();
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) view.findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                ((EditText) view.findViewById(R.id.txt_search)).setText(null);
                if (position == 0) {
//                	flageStok = false;
                    loadAllProduk();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_all_product);
                } else if (position == 1) {
//                	flageStok = true;
                    loadStok();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_stok);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);

        view.findViewById(R.id.btn_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPager.getCurrentItem() == 1) {
                    loadStok();
                } else {
                    loadAllProduk();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapterAll = listener.getStokCompetitor();
        customerModel = listener.getCustomerModel();
        if (adapterAll == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokComp();
            }
        }
    }

    public void loadAdapter() {

        adapterAll = listener.getStokCompetitor();

        if (adapterAll == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokComp();
            }
        } else {
            pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);
        }
    }

    private void loadAllProduk() {
        try {
            String search = ((EditText) view.findViewById(R.id.txt_search)).getText().toString();
            if (adapterAll != null) {
                if (search.trim().equals("")) {
//					list.setAdapter(adapter);
                    listAll.setAdapter(adapterAll);
                } else {
                    adapterSearch = new StokCompetitorAdapter(context, new ArrayList<GroupSKUComModel>(), new HashMap<GroupSKUComModel, List<StokCompetitorModel>>());
                    for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                        GroupSKUComModel groupSKUModel = adapterAll.getGroup(i);
                        adapterSearch.addHeader(groupSKUModel);
                        boolean flag = false;
                        for (int j = 0; j < adapterAll.getChildrenCount(i); j++) {
                            StokCompetitorModel orderDetailModel = adapterAll.getChild(i, j);
                            if (orderDetailModel.skuModel.ProductName.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                                adapterSearch.addChild(groupSKUModel, orderDetailModel);
                                flag = true;
                            }
                        }
                        if (flag == false) {
                            adapterSearch.removeHeader(groupSKUModel);
                        }
                    }
//					list.setAdapter(adapterSearch);
                    listAll.setAdapter(adapterSearch);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
        loadAdapter();
    }

    private void loadStok() {
        try {
            String search = ((EditText) view.findViewById(R.id.txt_search)).getText().toString();
            adapterStok = new StokCompetitorAdapter(context, new ArrayList<GroupSKUComModel>(), new HashMap<GroupSKUComModel, List<StokCompetitorModel>>());
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                GroupSKUComModel groupSKUComModel = adapterAll.getGroup(i);
                adapterStok.addHeader(groupSKUComModel);
                boolean flag = false;
                for (int j = 0; j < adapterAll.getChildrenCount(i); j++) {
                    final StokCompetitorModel orderDetailModel = adapterAll.getChild(i, j);
                    if (search.trim().equals("")) {
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            adapterStok.addChild(groupSKUComModel, orderDetailModel);

                            realmUI.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    boolean needToSave = false;

                                    if (realm.where(StockCustomerModel.class)
                                            .equalTo("CommonID", commonModel.CommonID)
                                            .equalTo("SKUId", orderDetailModel.skuModel.SKUId)
                                            .findFirst() == null)
                                        needToSave = true;

                                    com.bhn.sadix.Data.StockCompetitorModel model = new com.bhn.sadix.Data.StockCompetitorModel();

                                    model.CustId = orderDetailModel.customerModel.CustId;
                                    model.tanggal = "";
                                    model.SKUId = orderDetailModel.skuModel.SKUId;
                                    model.QTY_B = orderDetailModel.QTY_B;
                                    model.QTY_K = orderDetailModel.QTY_K;
                                    model.CommonID = commonModel.CommonID;
                                    model.PRICE = orderDetailModel.PRICE;
                                    model.PRICE2 = orderDetailModel.PRICE2;
                                    model.Note = orderDetailModel.Note;
                                    model.RandomID = orderDetailModel.RandomID;

                                    realm.copyToRealmOrUpdate(model);

                                    if (needToSave)
                                        commonModel.stockCompetitorModel.add(model);
                                }
                            });

                            flag = true;
                        }
                    } else {
                        if (orderDetailModel.skuModel.ProductName.toLowerCase().indexOf(search.trim().toLowerCase()) != -1) {
                            if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                                adapterStok.addChild(groupSKUComModel, orderDetailModel);

                                realmUI.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        boolean needToSave = false;

                                        if (realm.where(StockCustomerModel.class)
                                                .equalTo("CommonID", commonModel.CommonID)
                                                .equalTo("SKUId", orderDetailModel.skuModel.SKUId)
                                                .findFirst() == null)
                                            needToSave = true;

                                        com.bhn.sadix.Data.StockCompetitorModel model = new com.bhn.sadix.Data.StockCompetitorModel();

                                        model.CustId = orderDetailModel.customerModel.CustId;
                                        model.tanggal = "";
                                        model.SKUId = orderDetailModel.skuModel.SKUId;
                                        model.QTY_B = orderDetailModel.QTY_B;
                                        model.QTY_K = orderDetailModel.QTY_K;
                                        model.CommonID = commonModel.CommonID;
                                        model.PRICE = orderDetailModel.PRICE;
                                        model.PRICE2 = orderDetailModel.PRICE2;
                                        model.Note = orderDetailModel.Note;
                                        model.RandomID = orderDetailModel.RandomID;

                                        realm.copyToRealmOrUpdate(model);

                                        if (needToSave)
                                            commonModel.stockCompetitorModel.add(model);
                                    }
                                });

                                flag = true;
                            }
                        }
                    }
                }
                if (flag == false) {
                    adapterStok.removeHeader(groupSKUComModel);
                }
            }
//			list.setAdapter(adapterStok);
            listStok.setAdapter(adapterStok);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

//    public void setAdapter(StokCompetitorAdapter adapter) {
//        this.adapter = adapter;
//    }

    private void addJumlah(final StokCompetitorModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_stok_com);
        dialog.setTitle(model.skuModel.ProductName);
        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);

        ((TextView) dialog.findViewById(R.id.lbl_prc_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_prc_2)).setText(model.skuModel.SKECIL);

        if (model.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
        }
        if (model.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
        }
        if (model.PRICE > 0) {
            ((EditText) dialog.findViewById(R.id.price_1)).setText(String.valueOf(model.PRICE));
        }
        if (model.PRICE2 > 0) {
            ((EditText) dialog.findViewById(R.id.price_2)).setText(String.valueOf(model.PRICE2));
        }
        ((EditText) dialog.findViewById(R.id.note)).setText(String.valueOf(model.Note));
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] cek = {((EditText) dialog.findViewById(R.id.jml_1)).getText().toString()};
                /*realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {*/
                model.QTY_B = Integer.parseInt(cek[0].equals("") ? "0" : cek[0]);
                cek[0] = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                model.QTY_K = Integer.parseInt(cek[0].equals("") ? "0" : cek[0]);
                cek[0] = ((EditText) dialog.findViewById(R.id.price_1)).getText().toString();
                model.PRICE = Double.parseDouble(cek[0].equals("") ? "0" : cek[0]);
                cek[0] = ((EditText) dialog.findViewById(R.id.price_2)).getText().toString();
                model.PRICE2 = Double.parseDouble(cek[0].equals("") ? "0" : cek[0]);
                model.Note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();
                   /* }
                });*/

                adapterAll.notifyDataSetChanged();
                loadStok();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //context = KunjunganActivity
        listener = (KunjunganListener) context;
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);

//	    	list = ((ExpandableListView)v.findViewById(R.id.list));
//	    	if(flageStok) {
//		        if(adapterStok != null) {
//		        	list.setAdapter(adapterStok);
//		        }
//	    	} else {
//		        if(adapter != null) {
//		        	list.setAdapter(adapter);
//		        }
//	    	}
//	        list.setOnChildClickListener(new OnChildClickListener() {
//				@Override
//				public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//					if(flageStok) {
//						addJumlah(adapterStok.getChild(groupPosition, childPosition));
//					} else {
//						addJumlah(adapter.getChild(groupPosition, childPosition));
//					}
//					return false;
//				}
//			});

            if (position == 0) {
                listAll = ((ExpandableListView) v.findViewById(R.id.list));
                listAll.setVisibility(View.VISIBLE);
                listAll.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        StokCompetitorAdapter adapter = (StokCompetitorAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                listAll.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View view, int i, long l) {
                        StokCompetitorAdapter adapter = (StokCompetitorAdapter) parent.getExpandableListAdapter();
                        populateStok(adapter.getGroup(i));
                        return false;
                    }
                });
                loadAllProduk();
            } else {
                listStok = ((ExpandableListView) v.findViewById(R.id.list));
                listStok.setVisibility(View.VISIBLE);
                listStok.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        StokCompetitorAdapter adapter = (StokCompetitorAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                loadStok();
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        public void populateStok(GroupSKUComModel groupSKUComModel) {

            List<SKUComModel> listSKU = ctrlSKUCom.listByGroup(groupSKUComModel);

            for (SKUComModel skuComModel : listSKU) {
                int QTY_B = 0;
                int QTY_K = 0;
                double PRICE = 0;
                double PRICE2 = 0;
                String Note = "";
                String RandomID = UUID.randomUUID().toString();
                com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModel = realmUI.where(com.bhn.sadix.Data.StockCompetitorModel.class)
                        .equalTo("CommonID", commonModel.CommonID)
                        .equalTo("SKUId", skuComModel.SKUId).findFirst();

//                StockCompetitorModel stockCompetitorModel = ctrlStokCompetitor.getStockCompetitorModel(commonModel.CommonID, skuComModel.SKUId);

                if (stockCompetitorModel != null) {
                    QTY_B = stockCompetitorModel.QTY_B;
                    QTY_K = stockCompetitorModel.QTY_K;
                    PRICE = stockCompetitorModel.PRICE;
                    PRICE2 = stockCompetitorModel.PRICE2;
                    Note = stockCompetitorModel.Note;
                    RandomID = stockCompetitorModel.RandomID;
                }
                adapterAll.addChild(groupSKUComModel, new StokCompetitorModel(customerModel, skuComModel, QTY_B, QTY_K, PRICE, PRICE2, Note, RandomID));
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }
}
