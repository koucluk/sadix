package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.MerchandiserAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetType4Widget;
import com.bhn.sadix.widget.AssetType6Widget;
import com.bhn.sadix.widget.AssetWidget.AssetWidgetListener;

import java.io.ByteArrayOutputStream;

import io.realm.Realm;

public class MerchandiserFragment extends BaseFragment implements OnItemClickListener, AssetWidgetListener {
    private KunjunganListener listener;
    private Context context;
    private DbAsetHelper dbAsetHelper;

    private CtrlAppModul ctrlAppModul;
    private MerchandiserAdapter adapter;
    private AssetType4Widget type4;
    private AssetType6Widget type6;
    private CommonModel cmModel;
    private Realm realmUI;

    public MerchandiserFragment() {
        super(R.layout.fragment_kunjungan_merchandiser, "");
    }

//	public MerchandiserFragment(Context context, CommonModel commonModel) {
//		this();
////		super(R.layout.fragment_kunjungan_merchandiser, "");
//		this.context = context;
//		this.commonModel = commonModel;
//		dbAsetHelper = new DbAsetHelper(this.context);
//		ctrlAppModul = new CtrlAppModul(this.context);
//		adapter = new MerchandiserAdapter(context, dbAsetHelper.listMerchandiser(this.context, this, commonModel.CommonID));
//		adapter.setNotifyOnChange(true);
//	}

    public static MerchandiserFragment newInstance() {
        Bundle args = new Bundle();

        MerchandiserFragment fragment = new MerchandiserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        dbAsetHelper = new DbAsetHelper(this.context);
        ctrlAppModul = new CtrlAppModul(this.context);
    }

    /*public void setCommonModel(CommonModel commonModel) {
        this.commonModel = commonModel;
    }*/

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        realmUI = Realm.getDefaultInstance();
        cmModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new MerchandiserAdapter(context, dbAsetHelper.listMerchandiser(this.context, this, cmModel.CommonID));
        adapter.setNotifyOnChange(true);

        ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    type4.setBitmap(bitmap, bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(context, e.getMessage());
                }
            }
        } else if (requestCode == 6) {
            if (resultCode == Activity.RESULT_OK) {
                String kode = data.getStringExtra("SCAN_RESULT");
                //	        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                type6.setScanText(kode);
            }
        }
        ((KunjunganActivity) context).isHome = true;
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        adapter.getItem(position).show();
    }

    @Override
    public void photoType4(AssetType4Widget type4) {
        this.type4 = type4;
        ((KunjunganActivity) context).isHome = false;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 5);
    }

    public MerchandiserAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void scanType6(AssetType6Widget type6) {
        ((KunjunganActivity) context).isHome = false;
        this.type6 = type6;
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, 6);
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI.close();
    }
}
