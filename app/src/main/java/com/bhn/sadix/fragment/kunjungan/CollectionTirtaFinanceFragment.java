package com.bhn.sadix.fragment.kunjungan;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.CollectionPayAdapter;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

//import com.bhn.sadix.model.CommonModel;

public class CollectionTirtaFinanceFragment extends Fragment implements ConnectionEvent {
    private View view = null;
    private DbMasterHelper db;
    private CustomerModel customerModel;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    //	private LayoutInflater inflater;
    private CtrlConfig ctrlConfig;
    //	private List<BayarTagihanWidget> list = new ArrayList<BayarTagihanWidget>();
//	private List<Object[]> listData = new ArrayList<Object[]>();
    private double CreditLimit = 0;
    private double SisaCreditBalance = 0;
    private ConnectionServer server;
    private int jenis = 0;
    public CollectionPayAdapter payAdapter;
    private List<ComboBoxModel> listNotification = new ArrayList<ComboBoxModel>();
    private String NotifId = null;
    private int pinServer = 0;
    private JSONArray InvItem = null;
    private double total_invoice = 0;
    private double total_pembayaran = 0;
    private KunjunganListener listener;
    private Context context;
    Realm realmUI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DbMasterHelper(getActivity());
        ctrlConfig = new CtrlConfig(getActivity());
//		inflater = LayoutInflater.from(getActivity());
        server = new ConnectionServer(getActivity());
        server.addConnectionEvent(this);
        realmUI = Realm.getDefaultInstance();

        commonModel = realmUI.where(CommonModel.class)
                .equalTo("Done", 0).findFirst();

        if (payAdapter == null) {
            payAdapter = new CollectionPayAdapter(getActivity(), new ArrayList<CollectionModel>()) {

                @Override
                public void updateRemove() {
                    totalBayar();
                    ((TextView) view.findViewById(R.id.total_pembayaran)).setText("Total : " + Util.numberFormat.format(total_pembayaran));
                }
            };
            payAdapter.setNotifyOnChange(true);
        }
    }

    //	@Override
//	public void onDestroyView() {
//		LinearLayout layout_bayar = ((LinearLayout)view.findViewById(R.id.layout_bayar));
//		listData.clear();
//		for (int i = 0; i < layout_bayar.getChildCount(); i++) {
//			LinearLayout bayar = (LinearLayout) layout_bayar.getChildAt(i);
//			Object[] data = new Object[2];
//			data[0] = ((Spinner)bayar.findViewById(R.id.jenis_bayar)).getSelectedItem();
//			data[1] = ((EditText)bayar.findViewById(R.id.amount_bayar)).getText().toString();
//			listData.add(data);
//			Util.showDialogInfo(getActivity(), ((ComboBoxModel)data[0]).text);
//			Util.showDialogInfo(getActivity(), data[1].toString());
//		}
//		((LinearLayout)view.findViewById(R.id.layout_bayar)).removeAllViews();
//		super.onDestroyView();
//	}
    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public void setCommonModel(CommonModel commonModel) {
        this.commonModel = commonModel;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_collection_tirta_finance, container, false);
//        Cursor cursor = db.query("select * from custcollect where custid='"+customerModel.CustId+"'");
//        double total_invoice = 0;
//        while (cursor.moveToNext()) {
//			TableRow row = (TableRow) inflater.inflate(R.layout.row_collection_tirta, null);
//			((TextView)row.findViewById(R.id.row_invoiceno)).setText(cursor.getString(1));
//			((TextView)row.findViewById(R.id.row_dudate)).setText(cursor.getString(2));
//			((TextView)row.findViewById(R.id.row_amount)).setText(Util.numberFormat.format(cursor.getDouble(3)));
//			double sisa = cursor.getDouble(3)-cursor.getDouble(4);
//			((TextView)row.findViewById(R.id.row_sisa)).setText(Util.numberFormat.format(sisa));
//			((TableLayout)view.findViewById(R.id.tbl_invoice)).addView(row);
//			total_invoice += sisa;
//		}
//		((TextView)view.findViewById(R.id.total_invoice)).setText("Total : " + Util.numberFormat.format(total_invoice));
//        cursor.close();
//        db.close();
        view.findViewById(R.id.btn_tambah).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tambah();
            }
        });
        view.findViewById(R.id.btn_bayar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bayar();
            }
        });
        view.findViewById(R.id.btn_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });
        ((ListView) view.findViewById(R.id.list)).setAdapter(payAdapter);
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                viewPayCollection(payAdapter.getItem(position), 1);
            }
        });
        ((EditText) view.findViewById(R.id.pin)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String sPin = s.toString();
                sPin = sPin.equals("") ? "0" : sPin;
                if (Integer.parseInt(sPin) == pinServer) {
                    ((CheckBox) view.findViewById(R.id.ck_pin)).setChecked(true);
                } else {
                    ((CheckBox) view.findViewById(R.id.ck_pin)).setChecked(false);
                }
            }
        });
        view.findViewById(R.id.btn_send_pin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupSendPin();
            }
        });

        try {
            if (InvItem != null && InvItem.length() > 0) {
                TableLayout tbl_invoice = (TableLayout) view.findViewById(R.id.tbl_invoice);
                for (int i = (tbl_invoice.getChildCount() - 1); i > 0; i--) {
                    tbl_invoice.removeViewAt(i);
                }
                total_invoice = 0;
                for (int i = 0; i < InvItem.length(); i++) {
                    JSONObject Inv = InvItem.getJSONObject(i);
                    TableRow row = (TableRow) inflater.inflate(R.layout.row_collection_tirta, null);
                    ((TextView) row.findViewById(R.id.row_invoiceno)).setText(Inv.getString("InvNo"));
                    ((TextView) row.findViewById(R.id.row_dudate)).setText(Inv.getString("InvDu"));
                    ((TextView) row.findViewById(R.id.row_amount)).setText(Util.numberFormat.format(Inv.getDouble("InvAm")));
                    double sisa = Inv.getDouble("InvAm") - Inv.getDouble("InvRest");
                    ((TextView) row.findViewById(R.id.row_sisa)).setText(Util.numberFormat.format(sisa));
                    tbl_invoice.addView(row);
                    total_invoice += sisa;
                }
                ((TextView) view.findViewById(R.id.total_invoice)).setText("Total : " + Util.numberFormat.format(total_invoice));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerModel = listener.getCustomerModel();

        if (CreditLimit == 0) {
            refresh();
        } else {
            ((TextView) view.findViewById(R.id.CreditLimit)).setText(Util.numberFormat.format(CreditLimit));
            ((TextView) view.findViewById(R.id.SisaCreditBalance)).setText(Util.numberFormat.format(SisaCreditBalance));
            ((TextView) view.findViewById(R.id.CreditBalance)).setText(Util.numberFormat.format(CreditLimit - SisaCreditBalance));
        }
        refresh();
    }

    private void popupSendPin() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_send_pin);
        dialog.setTitle("SEND PIN");
        ((Spinner) dialog.findViewById(R.id.notification)).setAdapter(new ComboBoxAdapter(getActivity(), listNotification));
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                NotifId = ((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.notification)).getSelectedItem()).value;
                kirimNotifikasi();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void bayar() {
        if (payAdapter.getCount() == 0) {
            Util.showDialogInfo(getActivity(), "Pembayaran belum ada!");
        } else {
            if (!cekCreditLine()) {
                if (view.findViewById(R.id.panel_pin).getVisibility() == View.VISIBLE) {
                    if (pinServer == 0) {
                        Util.showDialogInfo(getActivity(), "Anda belum send pin!");
                    } else {
                        String sPin = ((EditText) view.findViewById(R.id.pin)).getText().toString();
                        sPin = sPin.equals("") ? "0" : sPin;
                        if (Integer.parseInt(sPin) == pinServer) {
                            Util.confirmDialog(getActivity(), "INFO", "Kirim data?", new ConfirmListener() {
                                @Override
                                public void onDialogCompleted(boolean answer) {
                                    if (answer) kirimBayar();
                                }
                            });
                        } else {
                            Util.showDialogInfo(getActivity(), "PIN tidak valid!");
                        }
                    }
                } else {
                    view.findViewById(R.id.panel_pin).setVisibility(View.VISIBLE);
                    ((Button) view.findViewById(R.id.btn_tambah)).setText("Cancel");
                    view.findViewById(R.id.tbl_invoice).setVisibility(View.GONE);
                    view.findViewById(R.id.total_invoice).setVisibility(View.GONE);
                    payAdapter.setEdit(false);
                }
            } else {
                Util.confirmDialog(getActivity(), "INFO", "Kirim data?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) kirimBayar();
                    }
                });
            }
        }
    }

    private void kirimNotifikasi() {
        try {
            JSONObject json = new JSONObject();
            json.put("SalesId", UserModel.getInstance(getActivity()).SalesId);
            json.put("CustID", customerModel.CustId);
            json.put("NotifId", NotifId);
            jenis = 3;
            server.setUrl(Util.getServerUrl(getActivity()) + "tfnotifmsg");
            server.requestJSONObject(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void kirimBayar() {
        try {
            Date tanggal = new Date();
            JSONArray sendData = new JSONArray();
            for (int i = 0; i < payAdapter.getCount(); i++) {
                CollectionModel model = payAdapter.getItem(i);
                model.tanggal = Util.dateFormat.format(tanggal);
                model.CustId = customerModel.CustId;
                JSONObject js = model.toJson();
//                JSONObject js = new JSONObject(json);
                js.put("SalesID", Integer.parseInt(UserModel.getInstance(getActivity()).SalesId));
                js.put("CustomerId", Integer.parseInt(customerModel.CustId));
                sendData.put(js);
            }
            android.util.Log.e("sendData", "" + sendData.toString());
            jenis = 2;
            server.setUrl(Util.getServerUrl(getActivity()) + "collection");
            server.requestJSONObjectArray(sendData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refresh() {
        jenis = 1;
        server.setUrl(Util.getServerUrl(getActivity()) + "getoutletbalance/OutletId/" + customerModel.CustId);
        server.requestJSONObject(null);
    }

    private void tambah() {
        String action = ((Button) view.findViewById(R.id.btn_tambah)).getText().toString();
        if (action.equals("Cancel")) {
            Util.confirmDialog(getActivity(), "INFO", "Anda yakin ingin membatalkan?", new ConfirmListener() {

                @Override
                public void onDialogCompleted(boolean answer) {
                    if (answer) bersih();
                }
            });
        } else {
            viewPayCollection(new CollectionModel(commonModel.CommonID, "1"), 0);
        }
    }

    private void bersih() {
        payAdapter.clear();
        view.findViewById(R.id.panel_pin).setVisibility(View.GONE);
        ((Button) view.findViewById(R.id.btn_tambah)).setText("Add Payment");
        payAdapter.setEdit(true);
        NotifId = null;
        pinServer = 0;
        listNotification.clear();
        ((EditText) view.findViewById(R.id.pin)).setText(null);
        ((TextView) view.findViewById(R.id.total_pembayaran)).setText("Total : " + Util.numberFormat.format(0));
        view.findViewById(R.id.tbl_invoice).setVisibility(View.VISIBLE);
        view.findViewById(R.id.total_invoice).setVisibility(View.VISIBLE);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(getActivity(), error);
    }

    @Override
    public void messageServer(String respon) {
        android.util.Log.e("respon", "" + respon);
        try {
            if (jenis == 1) {
                JSONObject json = new JSONObject(respon);
                CreditLimit = json.getDouble("OutletCreditLimit");
                SisaCreditBalance = json.getDouble("OutletCreditUsed");
                ((TextView) view.findViewById(R.id.CreditLimit)).setText(Util.numberFormat.format(CreditLimit));
                ((TextView) view.findViewById(R.id.SisaCreditBalance)).setText(Util.numberFormat.format(SisaCreditBalance));
                ((TextView) view.findViewById(R.id.CreditBalance)).setText(Util.numberFormat.format(CreditLimit - SisaCreditBalance));
                JSONArray Notif = json.getJSONArray("Notif");
                listNotification.clear();
                for (int i = 0; i < Notif.length(); i++) {
                    JSONObject notifMsg = Notif.getJSONObject(i);
                    listNotification.add(new ComboBoxModel(notifMsg.getString("NotifId"), notifMsg.getString("NotifValue")));
                }

                LayoutInflater inflater = LayoutInflater.from(context);
                InvItem = json.getJSONArray("InvItem");
                TableLayout tbl_invoice = (TableLayout) view.findViewById(R.id.tbl_invoice);
                for (int i = (tbl_invoice.getChildCount() - 1); i > 0; i--) {
                    tbl_invoice.removeViewAt(i);
                }
                total_invoice = 0;
                for (int i = 0; i < InvItem.length(); i++) {
                    JSONObject Inv = InvItem.getJSONObject(i);
                    TableRow row = (TableRow) inflater.inflate(R.layout.row_collection_tirta, null);
                    ((TextView) row.findViewById(R.id.row_invoiceno)).setText(Inv.getString("InvNo"));
                    ((TextView) row.findViewById(R.id.row_dudate)).setText(Inv.getString("InvDu"));
                    ((TextView) row.findViewById(R.id.row_amount)).setText(Util.numberFormat.format(Inv.getDouble("InvAm")));
                    double sisa = Inv.getDouble("InvAm") - Inv.getDouble("InvRest");
                    ((TextView) row.findViewById(R.id.row_sisa)).setText(Util.numberFormat.format(sisa));
                    tbl_invoice.addView(row);
                    total_invoice += sisa;
                }
                ((TextView) view.findViewById(R.id.total_invoice)).setText("Total : " + Util.numberFormat.format(total_invoice));

            } else if (jenis == 2) {
                JSONObject json = new JSONObject(respon);
                if (json.getString("Result").equals("INSERT_OK")) {
                    Util.Toast(getActivity(), json.getString("Message"));
                    bersih();
                    ((KunjunganActivity) getActivity()).keInformasi();
                } else {
                    Util.showDialogInfo(getActivity(), json.getString("Message"));
                }
            } else if (jenis == 3) {
                JSONObject json = new JSONObject(respon);
                if (json.getString("Result").equals("INSERT_OK")) {
//					inputPin(json.getInt("ServerID"));
                    pinServer = json.getInt("ServerID");
                    ((EditText) view.findViewById(R.id.pin)).setText(null);
                    ((CheckBox) view.findViewById(R.id.ck_pin)).setChecked(false);

                    runcekTombol();
                } else {
                    Util.showDialogInfo(getActivity(), json.getString("Message"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    private void runcekTombol() {
        final Button button = (Button) view.findViewById(R.id.btn_send_pin);
        button.setEnabled(false);
        Thread t = new Thread() {
            public void run() {
                for (int i = 0; i < 60; i++) {
                    final int detik = i + 1;
                    button.post(new Runnable() {
                        @Override
                        public void run() {
                            button.setText(String.valueOf(detik));
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                button.post(new Runnable() {
                    @Override
                    public void run() {
                        button.setText("SEND PIN");
                        button.setEnabled(true);
                    }
                });
            }
        };
        t.start();
    }

    private void viewPayCollection(final CollectionModel model, final int jenis) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_collection_pay);

        ((Spinner) dialog.findViewById(R.id.data2)).setAdapter(new ComboBoxAdapter(getActivity(), ctrlConfig.listComboBox("PAYMENT_METHOD")));
        ((EditText) dialog.findViewById(R.id.data1)).setText(model.data1);
        int index = ((ComboBoxAdapter) ((Spinner) dialog.findViewById(R.id.data2)).getAdapter()).indexOf(new ComboBoxModel(model.data2));
        ((Spinner) dialog.findViewById(R.id.data2)).setSelection(index);
        ((EditText) dialog.findViewById(R.id.data3)).setText(model.data3);
        ((EditText) dialog.findViewById(R.id.data4)).setText(model.data4);
        ((EditText) dialog.findViewById(R.id.data5)).setText(model.data5);
        ((EditText) dialog.findViewById(R.id.data6)).setText(model.data6);

        dialog.findViewById(R.id.btn_tgl).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                popupTanggal(dialog);
            }
        });

        ((Spinner) dialog.findViewById(R.id.data2)).setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ComboBoxModel model = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.data2)).getSelectedItem();
                if (model != null) {
                    if (model.text.equals("GIRO")) {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.nomor_giro).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.data5).setVisibility(View.VISIBLE);
                        ((TextView) dialog.findViewById(R.id.tgl_lbl)).setText("Tanggal Jatuh Tempo");
                    } else if (model.text.equals("TRANSFER")) {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.nomor_giro).setVisibility(View.GONE);
                        dialog.findViewById(R.id.data5).setVisibility(View.GONE);
                        ((TextView) dialog.findViewById(R.id.tgl_lbl)).setText("Tanggal Transfer");
                    } else if (model.text.equals("CREDIT LINE")) {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.GONE);
                        dialog.findViewById(R.id.nomor_giro).setVisibility(View.GONE);
                        dialog.findViewById(R.id.data5).setVisibility(View.GONE);
                    } else {
                        dialog.findViewById(R.id.panel_non_cash).setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String validate = ((EditText) dialog.findViewById(R.id.data1)).getText().toString();
                if (validate.equals("") || validate.equals("0")) {
                    Util.showDialogInfo(getActivity(), "Today Payment tidak boleh kosong atau 0!");
                    return;
                }
                ComboBoxModel combo = ((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.data2)).getSelectedItem());
                if (combo.text.equals("CREDIT LINE")) {
                    if (jenis == 0) {
                        String snominal = ((EditText) dialog.findViewById(R.id.data1)).getText().toString();
                        double nominal = Double.parseDouble(snominal.equals("") ? "0" : snominal);
                        if (nominal > (CreditLimit - SisaCreditBalance)) {
                            Util.showDialogInfo(getActivity(), "Credit line melebihi credit yang tersedia!");
                            return;
                        }
                        if (!cekCreditLine()) {
                            Util.showDialogInfo(getActivity(), "Credit line sudah ada!");
                            return;
                        }
                    }
//					if(!isCreditLine(Double.parseDouble((validate.equals("") ? "0" : validate)))) {
//						Util.showDialogInfo(getActivity(), "Customer tidak bisa menggunakan credit line!");
//						return;
//					}
                    if (!isCreditLineOnLine(Double.parseDouble((validate.equals("") ? "0" : validate)))) {
                        Util.showDialogInfo(getActivity(), "Customer tidak bisa menggunakan credit line!");
                        return;
                    }
                }
                if (combo.text.equals("GIRO")) {
                    validate = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(getActivity(), "Tanggal jatuh tempo harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data5)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(getActivity(), "Nomor Giro harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(getActivity(), "Bank harus diisi!");
                        return;
                    }
                } else if (combo.text.equals("TRANSFER")) {
                    validate = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(getActivity(), "Tanggal Transfer harus diisi!");
                        return;
                    }
                    validate = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();
                    if (validate.equals("")) {
                        Util.showDialogInfo(getActivity(), "Bank harus diisi!");
                        return;
                    }
                }

                totalBayar();
                model.data1 = ((EditText) dialog.findViewById(R.id.data1)).getText().toString();
                String nominla = model.data1;
                nominla = nominla.equals("") ? "0" : nominla;
                total_pembayaran += Double.parseDouble(nominla);
                if (total_pembayaran > total_invoice) {
                    Util.showDialogInfo(getActivity(), "Total pembayaran lebih besar dari invoice!");
                    return;
                }

//				if(combo.text.equals("CREDIT LINE")) {
//					NotifId = ((ComboBoxModel)((Spinner)dialog.findViewById(R.id.data2)).getSelectedItem()).value;
//				}
//				if(combo.text.equals("GIRO")) {
//				}
                model.data2 = combo.value;
                model.data3 = ((EditText) dialog.findViewById(R.id.data3)).getText().toString();
                model.data4 = ((EditText) dialog.findViewById(R.id.data4)).getText().toString();
                model.data5 = ((EditText) dialog.findViewById(R.id.data5)).getText().toString();
                model.data6 = ((EditText) dialog.findViewById(R.id.data6)).getText().toString();

                model.payMethod = combo.text;
                if (jenis == 0) {
                    payAdapter.add(model);
                }
                totalBayar();
                ((TextView) view.findViewById(R.id.total_pembayaran)).setText("Total : " + Util.numberFormat.format(total_pembayaran));
                payAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void totalBayar() {
        total_pembayaran = 0;
        for (int i = 0; i < payAdapter.getCount(); i++) {
            CollectionModel model = payAdapter.getItem(i);
            try {
                String nominla = model.data1;
                nominla = nominla.equals("") ? "0" : nominla;
                total_pembayaran += Double.parseDouble(nominla);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void popupTanggal(final Dialog dialog) {
        try {
            final Calendar cal = Calendar.getInstance();
            final DatePickerDialog tglDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    ((EditText) dialog.findViewById(R.id.data4)).setText(Util.dateFormatBirthDate.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            tglDialog.show();
        } catch (Exception e) {
            Util.Toast(getActivity(), e.getMessage());
        }
    }

    private boolean isCreditLine(double creditLine) {
//		Util.showDialogInfo(getActivity(), "SisaCreditBalance::"+SisaCreditBalance);
        if (SisaCreditBalance > 0) {
            Cursor cursor = db.query("select count(*) jml from custcollect where custid='" + customerModel.CustId + "' and invday > 7");
            if (cursor.moveToNext()) {
//	    		Util.showDialogInfo(getActivity(), "invday::"+cursor.getInt(0));
                if (cursor.getInt(0) > 0) {
                    cursor.close();
                    db.close();
                    return false;
                }
            }
            cursor.close();
            cursor = db.query("select sum(invam-invrest) jml from custcollect where custid='" + customerModel.CustId + "'");
            if (cursor.moveToNext()) {
//	    		Util.showDialogInfo(getActivity(), "creditLine::"+cursor.getDouble(0));
                if (creditLine > cursor.getDouble(0)) {
                    cursor.close();
                    db.close();
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    private boolean isCreditLineOnLine(double creditLine) {
        try {
            if (InvItem != null) {
                if (SisaCreditBalance > 0) {
                    int day = 0;
                    double credit = 0;
                    for (int i = 0; i < InvItem.length(); i++) {
                        JSONObject Inv = InvItem.getJSONObject(i);
                        if (Inv.getInt("InvDay") > 7) {
                            day++;
                        }
                        credit += (Inv.getDouble("InvAm") - Inv.getDouble("InvRest"));
                    }
                    if (day > 0) {
                        return false;
                    }
                    if (creditLine > credit) {
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean cekCreditLine() {
        for (int i = 0; i < payAdapter.getCount(); i++) {
            CollectionModel model = payAdapter.getItem(i);
            if (model.payMethod.equals("CREDIT LINE")) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
        this.context = context;
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI = Realm.getDefaultInstance();
    }
}
