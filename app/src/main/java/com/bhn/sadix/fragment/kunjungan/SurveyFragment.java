package com.bhn.sadix.fragment.kunjungan;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.SurveyActivity;
import com.bhn.sadix.adapter.FormSurveyAdapter;
import com.bhn.sadix.database.CtrlFormSurvey;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.FormSurveyModel;

public class SurveyFragment extends Fragment implements OnItemClickListener {
    private View view = null;
    private Context context;
    private CustomerModel customerModel;
    private FormSurveyAdapter adapter;
    private CtrlFormSurvey ctrlFormSurvey;
    private KunjunganListener listener;

    //	public SurveyFragment(Context context) {
//		this.context = context;
//        ctrlFormSurvey = new CtrlFormSurvey(this.context);
//	}

    public static SurveyFragment newInstance() {
        Bundle args = new Bundle();
        SurveyFragment fragment = new SurveyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        ctrlFormSurvey = new CtrlFormSurvey(this.context);
        customerModel = listener.getCustomerModel();
    }

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerModel = listener.getCustomerModel();

        adapter = new FormSurveyAdapter(context, ctrlFormSurvey.list(customerModel));
        adapter.setNotifyOnChange(true);
        ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_list, container, false);
        view.findViewById(R.id.toolbar).setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//		((KunjunganActivity)context).isHome = false;
        FormSurveyModel formSurveyModel = adapter.getItem(position);
        Intent intent = new Intent(context, SurveyActivity.class);
        intent.putExtra("FormSurveyModel", formSurveyModel);
        intent.putExtra("CustomerModel", customerModel);
//        intent.putExtra("commonID", listener.getCommonModel().CommonID);
        startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
        this.context = context;
    }
}
