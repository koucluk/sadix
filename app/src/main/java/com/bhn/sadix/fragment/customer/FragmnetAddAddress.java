package com.bhn.sadix.fragment.customer;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.CustAddressAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;

public class FragmnetAddAddress extends BaseFragment {
    private CustAddressAdapter adapter;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;

    private ImageView profileImg;
    private CustAdddress custAdddress;
    private CustomerModel customerModel;
    private List<CustAdddress> list;

    private AddPhotoInterface event;

    public FragmnetAddAddress() {
        super(R.layout.fragment_cust_address, "Address");
    }

    public void addAddPhotoInterface(AddPhotoInterface event) {
        this.event = event;
    }

//	public void setCustomerModel(CustomerModel customerModel) {
//		this.customerModel = customerModel;
//	}

    public List<CustAdddress> getList() {
        return list;
    }

    public void setList(List<CustAdddress> list) {
        this.list = list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlConfig = new CtrlConfig(getActivity());
        adapter = new CustAddressAdapter(getActivity(), new ArrayList<CustAdddress>());
        adapter.setNotifyOnChange(true);
        ctrlAppModul = new CtrlAppModul(getActivity());
        if (list != null) {
            adapter.setList(list);
        }
    }

    @Override
    public void onCreateFragment(Bundle savedInstanceState) {
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                showCustAddress(adapter.getItem(position), false);
            }
        });
        ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
        view.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCustAddress();
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void addCustAddress() {
        CustAdddress custAdddress = new CustAdddress();
//		adapter.add(custAdddress);
        showCustAddress(custAdddress, true);
    }

    private void showCustAddress(final CustAdddress item, final boolean isAdd) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.from_add_cust_alamat);
        profileImg = (ImageView) dialog.findViewById(R.id.profileImg);
        if (item.bitmap != null) {
            profileImg.setImageBitmap(item.bitmap);
        }
        custAdddress = item;
        dialog.findViewById(R.id.profileImg).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (event == null) {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 100);
                } else {
                    event.addPhoto(item, (ImageView) dialog.findViewById(R.id.profileImg));
                }
            }
        });
        dialog.setTitle("ADD ADDRESS");
        ((EditText) dialog.findViewById(R.id.AddName)).setText(item.AddName);
        ((EditText) dialog.findViewById(R.id.AddAddres)).setText(item.AddAddres);
        ((EditText) dialog.findViewById(R.id.AddPhone)).setText(item.AddPhone);
        ((EditText) dialog.findViewById(R.id.AddFax)).setText(item.AddFax);
        ((EditText) dialog.findViewById(R.id.AddEmail)).setText(item.AddEmail);
        ((EditText) dialog.findViewById(R.id.AddRemark)).setText(item.AddRemark);
        ComboBoxAdapter adapter = new ComboBoxAdapter(getActivity(), ctrlConfig.listComboBox("ADDRESS_TYPE"));
        ((Spinner) dialog.findViewById(R.id.AddType)).setAdapter(adapter);
        int index = adapter.indexOf(new ComboBoxModel(String.valueOf(item.AddType)));
        if (index != -1) {
            ((Spinner) dialog.findViewById(R.id.AddType)).setSelection(index);
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_save)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                item.CustRandomID = customerModel.RandomID;
                item.AddName = ((EditText) dialog.findViewById(R.id.AddName)).getText().toString();
                item.AddAddres = ((EditText) dialog.findViewById(R.id.AddAddres)).getText().toString();
                item.AddPhone = ((EditText) dialog.findViewById(R.id.AddPhone)).getText().toString();
                item.AddFax = ((EditText) dialog.findViewById(R.id.AddFax)).getText().toString();
                item.AddEmail = ((EditText) dialog.findViewById(R.id.AddEmail)).getText().toString();
                item.AddRemark = ((EditText) dialog.findViewById(R.id.AddRemark)).getText().toString();
                item.AddType = Integer.parseInt(((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.AddType)).getSelectedItem()).value);
                if (isAdd) {
                    FragmnetAddAddress.this.adapter.add(item);
                } else {
                    FragmnetAddAddress.this.adapter.notifyDataSetChanged();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    custAdddress.bitmap = bitmap;
                    custAdddress.data = bao.toByteArray();
                    profileImg.setImageBitmap(bitmap);
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(getActivity(), e.getMessage());
                }
            }
        }
    }

    public CustAddressAdapter getAdapter() {
        return adapter;
    }

    public interface AddPhotoInterface {
        public void addPhoto(CustAdddress item, ImageView img);
    }

}
