package com.bhn.sadix.fragment.customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.CustomerListener;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

public abstract class BaseFragment extends Fragment implements ConnectionEvent {
    protected ConnectionServer server;
    protected View view;
    private int layout;
    private String title;

    protected KunjunganListener listener;
    CustomerListener customerListener;

    protected CustomerModel customerModel;
    protected CustomerTypeModel customerTypeModel;
    protected CommonModel commonModel;

    public BaseFragment(int layout, String titile) {
        this.layout = layout;
        this.title = titile;
    }

    public abstract void onCreateFragment(Bundle savedInstanceState);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(layout, container, false);

        if (view.findViewById(R.id.toolbar) != null) {
            view.findViewById(R.id.toolbar).setVisibility(View.GONE);
        }
        onCreateFragment(savedInstanceState);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof KunjunganActivity) {
            customerModel = listener.getCustomerModel();
            commonModel = listener.getCommonModel();
            customerTypeModel = listener.getCustomerType();
        } else {
            customerModel = customerListener.getCustomer();
        }
    }

    public String getTitle() {
        return title;
    }

    public CommonModel getCommonModel() {
        return commonModel;
    }

    public CustomerModel getCustomerModel() {
        return customerModel;
    }

    public CustomerTypeModel getCustomerTypeModel() {
        return customerTypeModel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        server = new ConnectionServer(getActivity());
        server.addConnectionEvent(this);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(getActivity(), error);
    }

    public void info(String info) {
        Util.showDialogInfo(getActivity(), info);
    }

    @Override
    public void messageServer(String respon) {
        try {
            messageJSON(new JSONObject(respon));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void messageJSON(JSONObject json) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof KunjunganActivity)
            listener = (KunjunganListener) context;
        else
            customerListener = (CustomerListener) context;
    }
}
