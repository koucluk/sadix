package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.AssetServiceAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.model.AssetServiceModel;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetServiceWidget.AssetServiceWidgetListener;
import com.bhn.sadix.widget.AssetType4Widget;
import com.bhn.sadix.widget.AssetType6Widget;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import io.realm.Realm;

public class AssetServiceFragment extends Fragment implements OnItemClickListener, /*ConnectionEvent,*/ AssetServiceWidgetListener {
    private View view = null;
    private Context context;
    private DbAsetHelper dbAsetHelper;
    private CommonModel commonModel;
    private CtrlAppModul ctrlAppModul;
    private AssetServiceAdapter adapter;
    //	private ConnectionServer server;
    private AssetType4Widget type4;
    private AssetType6Widget type6;
    private KunjunganListener listener;
    private Realm realmUI;

    //	public AssetServiceFragment(Context context, CommonModel commonModel) {
//		this.context = context;
//		this.commonModel = commonModel;
//		dbAsetHelper = new DbAsetHelper(this.context);
//		ctrlAppModul = new CtrlAppModul(this.context);
//		adapter = new AssetServiceAdapter(context, new ArrayList<AssetServiceModel>());
//		adapter.setNotifyOnChange(true);
////		server = new ConnectionServer(context);
////		server.addConnectionEvent(this);
//	}

    public static AssetServiceFragment newInstance() {
        Bundle args = new Bundle();

        AssetServiceFragment fragment = new AssetServiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        dbAsetHelper = new DbAsetHelper(this.context);
        ctrlAppModul = new CtrlAppModul(this.context);
        adapter = new AssetServiceAdapter(context, new ArrayList<AssetServiceModel>());
        adapter.setNotifyOnChange(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_asset_service, container, false);
        view.findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });
//        ((Button)view.findViewById(R.id.btn_send)).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				send();
//			}
//		});
        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();

        ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void loadAdapter() {

    }

    private void add() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_asset);
        dialog.setTitle("ADD ASSET");
        dialog.findViewById(R.id.btn_scanbarcode).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((KunjunganActivity) context).isHome = false;
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                try {
                    startActivityForResult(intent, 4);
                } catch (Exception e) {
                    dialog.dismiss();
                    Util.showDialogInfo(getActivity(), "QZing Scanner Not Found, Please Install First!");
                }
            }
        });
        dialog.findViewById(R.id.btn_input).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                inputAssetNumber();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                String AssetNumber = data.getStringExtra("SCAN_RESULT");
                searchAsset(AssetNumber);
            }
        } else if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    type4.setBitmap(bitmap, bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(context, e.getMessage());
                }
            }
        } else if (requestCode == 6) {
            String kode = data.getStringExtra("SCAN_RESULT");
//	        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            type6.setScanText(kode);
        }
        ((KunjunganActivity) context).isHome = true;
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void inputAssetNumber() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_input_asset);
        dialog.setTitle("INPUT ASSET NUMBER");
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String AssetNumber = ((EditText) dialog.findViewById(R.id.AssetNumber)).getText().toString();
                dialog.dismiss();
                searchAsset(AssetNumber);
            }
        });
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void searchAsset(String assetNumber) {
        try {
            int index = adapter.indexOf(new AssetServiceModel(assetNumber));
            if (index == -1) {
                JSONObject json = dbAsetHelper.get("select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + commonModel.CustomerId + " and ASETNO='" + assetNumber + "'");
                if (json != null) {
                    AssetServiceModel assetModel = new AssetServiceModel(context,
                            json.getString("ASETNAME"),
                            json.getString("ASETTYPE"),
                            json.getString("ASETNO"),
                            json.getString("ASETSID"),
                            this,
                            commonModel.CustomerId,
                            commonModel.CommonID
                    );
                    adapter.add(assetModel);
                    ((KunjunganActivity) context).isHome = false;
                    assetModel.show();
                } else {
                    Util.showDialogInfo(context, "Asset Tidak Ditemukan!");
                }
            } else {
                ((KunjunganActivity) context).isHome = false;
                adapter.getItem(index).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        adapter.getItem(position).show();
    }

    @Override
    public void photoType4(AssetType4Widget type4) {
        this.type4 = type4;
        ((KunjunganActivity) context).isHome = false;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 5);
    }

    //	@Override
//	public void error(String error) {
//		Util.showDialogError(context, error);
//	}
//	@Override
//	public void messageServer(String respon) {
//		try {
//			JSONObject json = new JSONObject(respon);
//			if(json.getString("Result").equals("INSERT_OK")) {
//				Util.okeDialog(context, "Info", json.getString("Message"), new OkListener() {
//					@Override
//					public void onDialogOk() {
//						adapter.clear();
//					}
//				});
//			} else {
//				Util.showDialogInfo(context, json.getString("Message"));
//			}
//		} catch (Exception e) {
//			error(e.getMessage());
//		}
//	}
    public AssetServiceAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void scanType6(AssetType6Widget type6) {
        this.type6 = type6;
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        startActivityForResult(intent, 6);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
    }
}
