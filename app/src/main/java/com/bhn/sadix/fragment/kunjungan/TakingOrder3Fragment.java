package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.Data.PrincipleOrderModel;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.PrinsipleAdapter;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.database.DbSkuHelper;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/*import com.bhn.sadix.model.DiscountPromoModel;
import com.bhn.sadix.model.DiscountResultModel;
import com.bhn.sadix.model.PrinsipleModel;*/

public class TakingOrder3Fragment extends Fragment {
    private View view = null;
    private DbMasterHelper dbMasterHelper;
    private DbSkuHelper dbSkuHelper;
    private PrinsipleAdapter adapter = null;

    private CustomerModel customerModel;
    //    private CommonModel commonModel;
    private com.bhn.sadix.Data.CommonModel commonModel;
    private CustomerTypeModel customerTypeModel;

    private PrincipleModel prinsipleModel;

    private CtrlTakingOrder ctrlTakingOrder;

    KunjunganListener listener;
    Realm realmUI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        dbMasterHelper = new DbMasterHelper(getActivity());
        dbSkuHelper = new DbSkuHelper(getActivity());
        ctrlTakingOrder = new CtrlTakingOrder(getActivity());
        realmUI = Realm.getDefaultInstance();

    }

    public PrinsipleAdapter getAdapter() {
        return adapter;
    }

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public void setCustomerTypeModel(CustomerTypeModel customerTypeModel) {
        this.customerTypeModel = customerTypeModel;
    }*/

//    public void setCommonModel(CommonModel commonModel) {
//        this.commonModel = commonModel;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_taking_order3, container, false);

        Log.d("On Create View TO3", "Called");
        commonModel = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

//        ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                pilihPrinsiple(position);
            }
        });
        if (customerTypeModel != null) {
            String title = customerTypeModel.DiskonType.equals("2") ? "(Rp)" : "(%)";
            ((TextView) view.findViewById(R.id.lbl_discount_bawah)).setText("Discount " + title);
        }
        ((EditText) view.findViewById(R.id.discount_bawah)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateAmount();
            }
        });

        customerModel = listener.getCustomerModel();
        customerTypeModel = listener.getCustomerType();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*if (commonModel == null)
            commonModel = listener.getCommonModel();*/
        if (customerModel == null)
            customerModel = listener.getCustomerModel();
        if (customerTypeModel == null)
            customerTypeModel = listener.getCustomerType();

        adapter = listener.getPrinsipleAdapter();
        prinsipleModel = listener.getPrinsipleModel();

        if (prinsipleModel != null)
            Log.d("Principle Model", "" + prinsipleModel.PrinsipleName);

        if (adapter == null) {
            Log.d("First Call - adapter n", "called");
            adapter = new PrinsipleAdapter(getActivity(), new ArrayList<PrincipleModel>());
            adapter.setNotifyOnChange(true);

            try {
                if (commonModel != null) {
                    List<JSONObject> list = dbMasterHelper.queryListJSON("select * from custprinsiple where CustID='" + customerModel.CustId + "'");
                    for (final JSONObject jsonObject : list) {
                        final Cursor cursor = dbSkuHelper.query("select * from prinsiple where PrinsipleId='" + jsonObject.getString("PrinsipleID") + "'");
                        if (cursor.moveToNext()) {
                            realmUI.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    try {
                                        Log.d("Cursor - 0", cursor.getString(0));
                                        Log.d("Cursor - 1", cursor.getString(1));

                                        PrincipleModel pModel = new PrincipleModel(cursor);

                                        pModel.CreditLimit = jsonObject.getDouble("CreditLimit");
                                        pModel.PrinsipleType = jsonObject.getInt("PrinsipleType");
                                        pModel.CreditTOP = jsonObject.getDouble("CreditTOP");
                                        pModel.CommonID = commonModel.CommonID;

                                        realm.copyToRealm(pModel);

                                        PrincipleOrderModel model = realmUI.where(PrincipleOrderModel.class)
                                                .equalTo("CommonID", commonModel.CommonID)
                                                .equalTo("PrinsipleId", Integer.parseInt(jsonObject.getString("PrinsipleID")))
                                                .findFirst();

                                        if (model != null) {
                                            Log.d("prinsiple g amount", model.G_AMOUNT + "");
                                            Log.d("prinsiple s amount", model.S_AMOUNT + "");
                                            Log.d("prinsiple d amount", model.D_VALUE + "");


                                            pModel.price = model.G_AMOUNT;
                                            pModel.total_price = model.S_AMOUNT;
                                            pModel.discount = model.D_VALUE;
                                            pModel.total_qty_k = model.total_k;
                                            pModel.total_qty_b = model.total_b;

                                            RealmResults<com.bhn.sadix.Data.TakingOrderModel> orderModels = realm.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                                    .equalTo("CommonID", commonModel.CommonID)
                                                    .equalTo("PrinsipleID", model.PrinsipleId)
                                                    .findAll();

                                            while (orderModels.listIterator().hasNext()) {
                                                pModel.list.add(orderModels.listIterator().next());
                                            }

//                                            ArrayList<TakingOrderModel> orderModels = (ArrayList<TakingOrderModel>) ctrlTakingOrder.getTakingOrderPrinsiple(commonModel.CommonID, model.RandomID);

                                            if (orderModels != null) {
                                                Log.d("orderModels Count", orderModels.size() + "");
                                            }
                                        }
                                        adapter.add(pModel);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                }

                if (customerModel != null && (adapter == null || adapter.getCount() == 0)) {
                    List<JSONObject> list = dbMasterHelper.queryListJSON("select * from custprinsiple where CustID='" + customerModel.CustId + "'");
                    for (final JSONObject jsonObject : list) {
                        final Cursor cursor = dbSkuHelper.query("select * from prinsiple where PrinsipleId='" + jsonObject.getString("PrinsipleID") + "'");
                        if (cursor.moveToNext()) {
                            final PrincipleModel model = new PrincipleModel(cursor);
                            realmUI.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    try {
                                        model.CommonID = commonModel.CommonID;
                                        model.CreditLimit = jsonObject.getDouble("CreditLimit");
                                        model.PrinsipleType = jsonObject.getInt("PrinsipleType");
                                        model.CreditTOP = jsonObject.getDouble("CreditTOP");
                                        realm.copyToRealm(model);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            adapter.add(model);
                        }
                        dbSkuHelper.close();
                    }
                }

                Log.d("adapter size", adapter.getCount() + "");
                listener.setPrincipleAdapter(adapter);
                ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
                calculateAmount();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//            if (prinsipleModel != null) {
            ((ListView) view.findViewById(R.id.list)).setAdapter(adapter);
//            adapter.notifyDataSetChanged();
            calculateAmount();
//            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("On Resume TO3", "Called");

        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
//		loadOrder();
//        calculateAmount();
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
//        calculateAmount();
    }

    private void calculateAmount() {
        int QTY_B = 0;
        int QTY_K = 0;
        double PRICE = 0;
        double DISCOUNT = 0;
        double S_AMOUNT = 0;
        for (int j = 0; j < adapter.getCount(); j++) {
            PrincipleModel prinsipleModel = adapter.getItem(j);
            QTY_B += prinsipleModel.total_qty_b;
            QTY_K += prinsipleModel.total_qty_k;
            PRICE += prinsipleModel.total_price;
        }
        String cek = ((EditText) view.findViewById(R.id.discount_bawah)).getText().toString();
        DISCOUNT = Double.parseDouble(cek.equals("") ? "0" : cek);
        if (customerTypeModel != null) {
            if (DISCOUNT > customerTypeModel.DiskonMax) {
                String max = "";
                if (customerTypeModel.DiskonType.equals("2")) {
                    max = "Rp." + customerTypeModel.DiskonMax;
                } else {
                    max = "" + customerTypeModel.DiskonMax + "%";
                }
                Util.showDialogInfo(getActivity(), "Nilai melebihi batas masimum " + max);
                ((EditText) view.findViewById(R.id.discount_bawah)).setText(null);
                return;
            } else {
                if (customerTypeModel.DiskonType.equals("2")) {
                    S_AMOUNT = PRICE - DISCOUNT;
                } else {
                    S_AMOUNT = PRICE - (PRICE * DISCOUNT / 100);
                }
            }
        }
        final int finalQTY_B = QTY_B;
        final int finalQTY_K = QTY_K;
        final double finalPRICE = PRICE;
        final double finalDISCOUNT = DISCOUNT;
        final double finalS_AMOUNT = S_AMOUNT;

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel.QTY_B = finalQTY_B;
                commonModel.QTY_K = finalQTY_K;
                commonModel.PRICE = finalPRICE;
                commonModel.DISCOUNT = finalDISCOUNT;
                commonModel.S_AMOUNT = finalS_AMOUNT;
            }
        });

        if (view != null) {
            ((TextView) view.findViewById(R.id.total_qty_bawah)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
            ((TextView) view.findViewById(R.id.price_bawah)).setText(Util.numberFormat.format(commonModel.PRICE));
            ((TextView) view.findViewById(R.id.total_price_bawah)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
        }
    }

    private void pilihPrinsiple(int position) {
        listener.handleTO3(adapter.getItem(position), position);
    }

    public boolean isOrder() {
        if (adapter != null && adapter.getCount() > 0) {
            for (int i = 0; i < adapter.getCount(); i++) {
                PrincipleModel model = adapter.getItem(i);
                if (model.total_price > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
