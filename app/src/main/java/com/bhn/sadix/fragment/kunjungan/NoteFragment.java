package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;

import io.realm.Realm;

public class NoteFragment extends Fragment {
    private View view = null;
    private String note = "";
    private CommonModel commonModel;
    private KunjunganListener listener;
    Realm realmUI;

    EditText etNote;

//    public void setNote(String note) {
//        this.note = note;
//    }

    public static NoteFragment newInstance(String note) {
        Bundle args = new Bundle();

        NoteFragment fragment = new NoteFragment();
        args.putString("Note", note);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            note = getArguments().getString("Note");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_note, container, false);
        etNote = (EditText) view.findViewById(R.id.note);

        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
//        /*if (note == null && note.equals(""))

        note = commonModel.Description;

        etNote.setText(note);
        etNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        commonModel.Description = editable.toString();
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI = Realm.getDefaultInstance();
        if (view != null) {
            ((EditText) view.findViewById(R.id.note)).setText(commonModel.Description);
        }
    }

    public String getNote() {
        if (view != null) {
            String sNote = ((EditText) view.findViewById(R.id.note)).getText().toString();
            if (sNote.equals("") == false) {
                note = sNote;
            }
        }
        return note;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        listener = (KunjunganListener) context;
    }
}
