package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.SearchSKUActivity;
import com.bhn.sadix.adapter.StokCustomerAdapter;
import com.bhn.sadix.database.CtrlCrcStock;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlStokOutlet;
import com.bhn.sadix.model.CrcStockModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.StockOutletModel;
import com.bhn.sadix.model.StokCustomerModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

//import com.bhn.sadix.model.CommonModel;

public class StokOutletFragment extends Fragment {
    private KunjunganListener listener;
    private View view = null;
    private StokCustomerAdapter adapter = null;
    private StokCustomerAdapter adapterStok = null;
    private ExpandableListView listAll;
    private ExpandableListView listStok;
    private Context context;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private CtrlSKU ctrlSKU;
    private CtrlStokOutlet ctrlStokOutlet;
    private CommonModel commonModel;
    private CustomerModel customerModel;
    private CtrlCrcStock ctrlCrcStock;
    private Realm realmUI;

    public StokCustomerAdapter getStokAdapter() {
        return adapter;
    }

    public static StokOutletFragment newInstance() {
        return new StokOutletFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        ctrlSKU = new CtrlSKU(context);
        ctrlStokOutlet = new CtrlStokOutlet(context);
        ctrlCrcStock = new CtrlCrcStock(context);
        realmUI = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_stok, container, false);
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_all_product) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_stok) {
                    mPager.setCurrentItem(1);
                }
            }
        });
        view.findViewById(R.id.tab_all_product).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        view.findViewById(R.id.tab_stok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) view.findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    loadAllProduk();
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_all_product);
                } else if (position == 1) {
                    loadStok(listStok);
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_stok);
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);

        view.findViewById(R.id.btn_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((KunjunganActivity) context).isHome = false;
                Intent intent = new Intent(context, SearchSKUActivity.class);
                intent.putExtra("CustGroupId", customerModel.CustGroupID);
                startActivityForResult(intent, 3);
            }
        });

        customerModel = listener.getCustomerModel();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*if (commonModel == null) {
            commonModel = listener.getCommonModel();
        }*/
        commonModel = realmUI.where(CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        if (customerModel == null) {
            customerModel = listener.getCustomerModel();
        }

        if (adapter == null) {
            ((KunjunganActivity) getActivity()).loadStokOutlet();
        }
    }

    public void loadAdapter() {

        adapter = listener.getStokAdapter();

        if (adapter == null) {
            ((KunjunganActivity) getActivity()).loadStokOutlet();
        } else {
            adapter.notifyDataSetChanged();

            pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);
//            loadAllProduk();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                SKUModel model = (SKUModel) data.getSerializableExtra("data");
                addOrderItemSearch(new StokCustomerModel(customerModel, model, 0, 0, UUID.randomUUID().toString()));
            }
        }
        ((KunjunganActivity) context).isHome = true;
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addOrderItemSearch(StokCustomerModel stokCustomerModel) {
        try {
            int index = adapter.getPosisionHeader(new GroupSKUModel(stokCustomerModel.skuModel.GroupId));
            List<StokCustomerModel> list = adapter.listOrderByGroup(adapter.getGroup(index));
            if (list != null) {
                int indexProduk = list.indexOf(stokCustomerModel);
                if (indexProduk == -1) {
                    StockOutletModel stockOutletModel = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID, stokCustomerModel.skuModel.SKUId);
                    if (stockOutletModel != null) {
                        stokCustomerModel.QTY_B = stockOutletModel.QTY_B;
                        stokCustomerModel.QTY_K = stockOutletModel.QTY_K;
                    }
                    adapter.addChild(adapter.getGroup(index), stokCustomerModel);
                    addJumlah(stokCustomerModel);
                } else {
                    addJumlah(list.get(indexProduk));
                }
            } else {
                StockOutletModel stockOutletModel = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID, stokCustomerModel.skuModel.SKUId);
                if (stockOutletModel != null) {
                    stokCustomerModel.QTY_B = stockOutletModel.QTY_B;
                    stokCustomerModel.QTY_K = stockOutletModel.QTY_K;
                }
                adapter.addChild(adapter.getGroup(index), stokCustomerModel);
                addJumlah(stokCustomerModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadAllProduk() {
        try {
            listAll.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadStok(ExpandableListView listStok) {
        try {
            adapterStok = new StokCustomerAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<StokCustomerModel>>());
            for (int i = 0; i < adapter.getGroupCount(); i++) {
                populateSKUByGroup(adapter.getGroup(i));
                GroupSKUModel groupSKUModel = adapter.getGroup(i);
                adapterStok.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                    final StokCustomerModel orderDetailModel = adapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterStok.addChild(groupSKUModel, orderDetailModel);

                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                                boolean needToSave = false;
                                StockCustomerModel stkModel = realm.where(StockCustomerModel.class)
                                        .equalTo("RandomID", orderDetailModel.RandomID)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .findFirst();

                                if (stkModel == null) {
                                    needToSave = true;
                                }

                                StockCustomerModel model = new StockCustomerModel();
                                model.CommonID = commonModel.CommonID;
                                model.CustId = customerModel.CustId;
                                model.QTY_B = orderDetailModel.QTY_B;
                                model.QTY_K = orderDetailModel.QTY_K;
                                model.RandomID = orderDetailModel.RandomID;
                                model.SKUId = orderDetailModel.skuModel.SKUId;
                                model.tanggal = "";

                                realm.copyToRealmOrUpdate(model);

                                if (needToSave)
                                    commonModel.stockCustomerModel.add(model);
                            }
                        });


                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterStok.removeHeader(groupSKUModel);
                }
            }
            adapterStok.notifyDataSetChanged();
            if (listStok != null)
                listStok.setAdapter(adapterStok);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    public boolean isStok() {
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                StokCustomerModel orderDetailModel = adapter.getChild(i, j);
                if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                    return true;
                }
            }
        }
        return false;
    }

//    public void setAdapter(StokCustomerAdapter adapter) {
//        this.adapter = adapter;
//    }

    private void addJumlah(final StokCustomerModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_stok);
        dialog.setTitle(model.skuModel.ProductName);

        CrcStockModel crcStockModel = ctrlCrcStock.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
        if (crcStockModel != null) {
            if (model.QTY_B == 0) {
                model.QTY_B = crcStockModel.QTY_L;
            }
            if (model.QTY_K == 0) {
                model.QTY_K = crcStockModel.QTY;
            }
        }

        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        if (model.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
        }
        if (model.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                model.QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                model.QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);

                loadStok(null);
                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void populateSKUByGroup(GroupSKUModel group) {
        try {
            List<StokCustomerModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    StokCustomerModel model = new StokCustomerModel(customerModel, skuModel, 0, 0, UUID.randomUUID().toString());
                    if (list == null || list.indexOf(model) == -1) {
//                        StockOutletModel stockOutletModel = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID, skuModel.SKUId);
                        com.bhn.sadix.Data.StockCustomerModel stockOutletModel = realmUI.where(com.bhn.sadix.Data.StockCustomerModel.class)
                                .equalTo("CommonID", commonModel.CommonID)
                                .equalTo("SKUId", skuModel.SKUId)
                                .findFirst();/*getStockOutletModel(commonModel.CommonID, skuModel.SKUId);*/

                        if (stockOutletModel != null) {
                            model.QTY_B = stockOutletModel.QTY_B;
                            model.QTY_K = stockOutletModel.QTY_K;
                            model.RandomID = stockOutletModel.RandomID;
                        }
                        adapter.addChild(group, model);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
        loadAdapter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);

            if (position == 0) {
                listAll = ((ExpandableListView) v.findViewById(R.id.list));
                listAll.setVisibility(View.VISIBLE);
                listAll.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        StokCustomerAdapter adapter = (StokCustomerAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                listAll.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        StokCustomerAdapter adapter = (StokCustomerAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(adapter.getGroup(groupPosition));
                        return false;
                    }
                });
                loadAllProduk();
            } else {
                listStok = ((ExpandableListView) v.findViewById(R.id.list));
                listStok.setVisibility(View.VISIBLE);
                listStok.setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        StokCustomerAdapter adapter = (StokCustomerAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                loadStok(listStok);
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
    }
}
