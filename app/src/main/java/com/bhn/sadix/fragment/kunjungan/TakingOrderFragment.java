package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.DiscountPromoModel;
import com.bhn.sadix.Data.DiscountResultModel;
import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.SearchSKUActivity;
import com.bhn.sadix.adapter.DiskonAdapter;
import com.bhn.sadix.adapter.DiskonPromoAdapter;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.SalesProgramAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCrcOrder;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSalesProgram;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbDiskonHelper;
import com.bhn.sadix.database.DbSkuHelper;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.CrcOrderModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.DiskonPromoModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

//import com.google.firebase.crash.FirebaseCrash;

//import com.bhn.sadix.model.CommonModel;

public class TakingOrderFragment extends Fragment implements DiskonAdapter.CheckedChangeListener {
    private View view = null;
    KunjunganListener listener;
    private KunjunganActivity activity;
    private OrderDetailAdapter adapterAll = null;
    private OrderDetailAdapter adapterOrder = null;
    private OrderDetailAdapter adapterFocus = null;
    private OrderDetailAdapter adapterPromo = null;
    private OrderDetailAdapter adapterSearch = null;
    private DiskonAdapter diskonAdapter = null;
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private CtrlSalesProgram ctrlSalesProgram;
    private Context context;
    private ConnectionServer server;
    private CustomerTypeModel customerTypeModel;
    private CommonModel commonModel;

    ProgressDialog progressDialog;

    private boolean isCanvas = false;
    private boolean isPrinciple = false;

    private CtrlAppModul ctrlAppModul;
    private CtrlSKU ctrlSKU;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlCrcOrder ctrlCrcOrder;
    private CtrlGroupSKU ctrlGroupSKU;
    private DbDiskonHelper dbDiskonHelper;
    private CustomerModel customerModel;

    private DiskonPromoAdapter diskonPromoAdapter;

    private View vFocus;
    private View vPromo;
    private View vAll;
    private View vOrder;
    private View vDiskonPromo;
    private View vHadiah;

    private View mainProduct;
    private View mainDiscPromo;
    private View mainOrder;

    private DbSkuHelper dbSku;

    //    private CommonModel commonModel;
    private Realm realmUI;

    private List<JSONObject> listPilih = new ArrayList<JSONObject>();

    private int MODEL_ORDER;

    public int getMODEL_ORDER() {
        return MODEL_ORDER;
    }

    public static TakingOrderFragment newInstance() {
        return new TakingOrderFragment();
    }

    public static TakingOrderFragment newInstance(boolean isCanvas, boolean isPrinciple, int modelOrder) {
        TakingOrderFragment takingOrderFragment = new TakingOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isCanvas", isCanvas);
        bundle.putBoolean("isPrinciple", isPrinciple);
        bundle.putInt("modelOrder", modelOrder);
        takingOrderFragment.setArguments(bundle);
        return takingOrderFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isCanvas = bundle.getBoolean("isCanvas");
            isPrinciple = bundle.getBoolean("isPrinciple");
            MODEL_ORDER = bundle.getInt("modelOrder");
        }

        if (realmUI == null)
            realmUI = Realm.getDefaultInstance();

        this.context = getActivity();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Tunggu Sebentar...");
        progressDialog.setCancelable(false);

        ctrlSalesProgram = new CtrlSalesProgram(this.context);
        ctrlAppModul = new CtrlAppModul(this.context);
        ctrlSKU = new CtrlSKU(this.context);
        ctrlTakingOrder = new CtrlTakingOrder(this.context);
        ctrlCrcOrder = new CtrlCrcOrder(this.context);
        dbDiskonHelper = new DbDiskonHelper(this.context);
        ctrlGroupSKU = new CtrlGroupSKU(this.context);
        dbSku = new DbSkuHelper(this.context);
        server = new ConnectionServer(this.context);
        adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());

//        commonModel = listener.getCommonModel();
        customerModel = listener.getCustomerModel();
        customerTypeModel = listener.getCustomerType();

        android.util.Log.e("kesini", "okeaa");

        if (adapterAll == null) {
            if (isPrinciple) {
                adapterAll = listener.getTakingPrincipleAdapter();
            } else {
                adapterAll = listener.getTakingAdapter();
            }
        }

        android.util.Log.e("kesini", "okeaa");
        if (isPrinciple) {
            android.util.Log.e("Is Principle", listener.getPrinsipleModel().PrinsipleId);

            PrincipleModel pModel = realmUI.where(PrincipleModel.class)
                    .equalTo("PrinsipleId", listener.getPrinsipleModel().PrinsipleId)
                    .findFirst();

            RealmList<com.bhn.sadix.Data.TakingOrderModel> ls = pModel.list; /*(ArrayList<TakingOrderModel>) getActivity().getIntent().getSerializableExtra("list");*/

            if (ls != null) {
                android.util.Log.e("List", "Not Null");
                StringBuilder sb = new StringBuilder();
                sb.append("(");
                for (int i = 0; i < ls.size(); i++) {
                    if (i > 0) sb.append(",");
                    sb.append("'").append(ls.get(i).SKUId).append("'");
                }
                sb.append(")");
                Log.d("SB", "" + sb);
                /*for (int i = 0; i < this.adapterAll.getGroupCount(); i++) {
                    if (ctrlGroupSKU.isLoadGroup(this.adapterAll.getGroup(i).GroupId, sb.toString())) {
//                        populateSKUByGroup(1, this.adapterAll.getGroup(i));
                    }
                }*/
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("On View Created TO", "Called");
        /*if (commonModel == null) {
            commonModel = listener.getCommonModel();
        }*/
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        customerModel = listener.getCustomerModel();
        customerTypeModel = listener.getCustomerType();

        if (isPrinciple) {
            realmUI.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    commonModel.QTY_B = listener.getPrinsipleModel().total_qty_b;/*getActivity().getIntent().getIntExtra("total_qty_b", 0);*/
                    commonModel.QTY_K = listener.getPrinsipleModel().total_qty_k; /*getActivity().getIntent().getIntExtra("total_qty_k", 0);*/
                    commonModel.PRICE = listener.getPrinsipleModel().price; /*getActivity().getIntent().getDoubleExtra("price", 0);*/
                    commonModel.DISCOUNT = listener.getPrinsipleModel().discount;/*getActivity().getIntent().getDoubleExtra("discount", 0);*/
                    commonModel.S_AMOUNT = listener.getPrinsipleModel().total_price;/*getActivity().getIntent().getDoubleExtra("total_price", 0);*/
                }
            });

            if (view != null) {
                ((EditText) view.findViewById(R.id.discount)).setText(Util.NUMBER_FORMAT.format(commonModel.DISCOUNT));
            }
        }

        if (adapterAll == null) {
            if (isPrinciple) {
                adapterAll = listener.getTakingPrincipleAdapter();
                Log.d("Principle", "Called");
            } else {
                if (getActivity() instanceof KunjunganActivity) {
                    ((KunjunganActivity) getActivity()).loadTakingOrder(1);
                } else {
                    loadAdapter(true);
                }
            }
        }

        if (isPrinciple) {
            loadPrinciple();

            initAdapter();
            pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);
        }

        ((EditText) view.findViewById(R.id.discount)).setText(commonModel.DISCOUNT + "");
    }

    public void loadAdapter(boolean first) {
        adapterAll = listener.getTakingAdapter();

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        if (adapterAll == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadTakingOrder(1);
            }
        } else {
            adapterAll.notifyDataSetChanged();

            if (first)
                initAdapter();

            pagerAdapter = new ViewPagerAdapter();
            mPager.setAdapter(pagerAdapter);

            try {
                if (view != null) {
                    ((TextView) view.findViewById(R.id.total_qty)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
                    ((TextView) view.findViewById(R.id.price)).setText(Util.numberFormat.format(commonModel.PRICE));
                    ((TextView) view.findViewById(R.id.total_price)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
                }
            } catch (Exception e) {
//                FirebaseCrash.log("Checked - null commonModel");
                e.printStackTrace();
            }
            calculateAmount();
            loadOrder();
        }
    }

    public void loadPrinciple() {
        android.util.Log.e("kesini", "oke");
        RealmList<com.bhn.sadix.Data.TakingOrderModel> ls = listener.getPrinsipleModel().list; /*(ArrayList<TakingOrderModel>) getActivity().getIntent().getSerializableExtra("list")*/
        if (ls != null) {
            android.util.Log.e("kesini", "oke");
            StringBuilder sb = new StringBuilder();
            sb.append("(");
            for (int i = 0; i < ls.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append("'").append(ls.get(i).SKUId).append("'");
            }
            sb.append(")");
            android.util.Log.e("sb", "" + sb);
            /*for (int i = 0; i < this.adapterAll.getGroupCount(); i++) {
                if (ctrlGroupSKU.isLoadGroup(this.adapterAll.getGroup(i).GroupId, sb.toString())) {
                    populateSKUByGroup(1, this.adapterAll.getGroup(i));
                }
            }*/
        }
    }

    public boolean isCanvas() {
        return isCanvas;
    }

    public void setCanvas(boolean isCanvas) {
        this.isCanvas = isCanvas;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_taking_order, container, false);

        /*this.context = getActivity();
        ctrlSalesProgram = new CtrlSalesProgram(this.context);
        ctrlAppModul = new CtrlAppModul(this.context);
        ctrlSKU = new CtrlSKU(this.context);
        ctrlTakingOrder = new CtrlTakingOrder(this.context);
        ctrlCrcOrder = new CtrlCrcOrder(this.context);
        dbDiskonHelper = new DbDiskonHelper(this.context);
        ctrlGroupSKU = new CtrlGroupSKU(this.context);
        dbSku = new DbSkuHelper(this.context);
        server = new ConnectionServer(this.context);
        adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());*/

        Log.d("On Create View TO", "Called");
        if (!ctrlAppModul.isModul("61")) {
//            view.findViewById(R.id.tab2).setVisibility(View.GONE);
            view.findViewById(R.id.tab_promo).setVisibility(View.GONE);
            ((RadioButton) view.findViewById(R.id.tab_all_product)).setText("Order");
        }
        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_product) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_promo) {
                    mPager.setCurrentItem(1);
                } else if (checkedId == R.id.tab_all_product) {
                    mPager.setCurrentItem(2);
                }
            }
        });

        mPager = (ViewPager) view.findViewById(R.id.pager_taking_order);
        mPager.setOffscreenPageLimit(3);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_product);
                } else if (position == 1) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_promo);
                } else if (position == 2) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_all_product);
                }
                super.onPageSelected(position);
            }
        };

        mPager.addOnPageChangeListener(ViewPagerListener);

        if (customerTypeModel != null) {
            String title = customerTypeModel.DiskonType.equals("2") ? "(Rp)" : "(%)";
            ((TextView) view.findViewById(R.id.lbl_discount)).setText("Discount " + title);
        }
        ((EditText) view.findViewById(R.id.discount)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateAmount();
            }
        });

        view.findViewById(R.id.btn_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof KunjunganActivity) {
                    ((KunjunganActivity) context).isHome = false;
                }
                Intent intent = new Intent(context, SearchSKUActivity.class);

                if (isPrinciple) {
                    intent.putExtra("PrinsipleModel", listener.getPrinsipleModel().PrinsipleId);
                }

                intent.putExtra("CustGroupId", customerModel.CustGroupID);
                startActivityForResult(intent, 3);
            }
        });

        return view;
    }

    private void loadDiskonPromo() {
        String sqlGroup = "";
        String sqlProduk = "";
        int Price = 0;
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel group = adapterOrder.getGroup(i);
            if (i == 0) {
                sqlGroup = "'" + group.GroupId + "'";
            } else {
                sqlGroup += ",'" + group.GroupId + "'";
            }
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
                if (i == 0 && j == 0) {
                    sqlProduk = "'" + model.skuModel.SKUId + "'";
                } else {
                    sqlProduk += ",'" + model.skuModel.SKUId + "'";
                }
            }
        }
        CommonModel cmModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
//        Log.d("Common Price", String.valueOf(cmModel.PRICE));
        try {
            if (!sqlGroup.equals("") && !sqlProduk.equals("")) {
                Log.wtf("Load Diskon", "Available");

                if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='" + customerModel.CustId + "' or CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "'") > 0) {

                    diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
                                    + "where DISCOUNT_PROMO_ID in ("
                                    + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                                    + ") "
                                    + "and DISCOUNT_PROMO_ID in ("
                                    + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='" + customerModel.CustId + "' or CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "' group by DISCOUNT_PROMO_ID "
                                    + ") "
                                    + "union "
                                    + "select * from DISCOUNT_PROMO "
                                    + "where DISCOUNT_PROMO_ID in ("
                                    + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                                    + ") "
                            /*+ "union "
                            + "SELECT a.* FROM DISCOUNT_PROMO a "
                            + "LEFT JOIN "
                            + "(SELECT * FROM DISCOUNT_TOTAL) b on a.DISCOUNT_PROMO_ID=b.DISCOUNT_PROMO_ID WHERE b.MIN_ORDER <= " + cmModel.PRICE + " "
                            + "union "
                            + "SELECT a.* FROM DISCOUNT_PROMO a "
                            + "LEFT JOIN "
                            + "(SELECT * FROM DISCOUNT_TOTAL) b on a.DISCOUNT_PROMO_ID=b.DISCOUNT_PROMO_ID WHERE b.MIN_ORDER <= " + cmModel.PRICE + " "
                            + "and DISCOUNT_PROMO_ID in ("
                            + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='" + customerModel.CustId + "' or CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "' group by DISCOUNT_PROMO_ID "
                            + ") "*/
                    ));
                } else {
                    diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
                            + "where DISCOUNT_PROMO_ID in ("
                            + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                            + ")"));
                }

                diskonAdapter.setListPilih(listPilih);
                diskonAdapter.setNotifyOnChange(true);
                diskonAdapter.setCheckedChangeListener(this);

                if (vDiskonPromo != null)
                    ((ListView) vDiskonPromo.findViewById(R.id.listdata)).setAdapter(diskonAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //	private void loadDiskonPromo(boolean isChecked, JSONObject json) {
//		String sqlGroup = "";
//		String sqlProduk = "";
//		for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
//			GroupSKUModel group = adapterOrder.getGroup(i);
//			if(i == 0) {
//				sqlGroup = "'" + group.GroupId + "'";
//			} else {
//				sqlGroup += ",'" + group.GroupId + "'";
//			}
//			for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
//				OrderDetailModel model = adapterOrder.getChild(i, j);
//				if(i == 0 && j == 0) {
//					sqlProduk = "'" + model.skuModel.SKUId + "'";
//				} else {
//					sqlProduk += ",'" + model.skuModel.SKUId + "'";
//				}
//			}
//		}
//		if(!sqlGroup.equals("") && !sqlProduk.equals("")) {
//			if(!isChecked) {
//				if(dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='"+customerModel.CustId+"' or CUSTOMER_TYPE_ID='"+customerTypeModel.CustomerTypeId+"'") > 0) {
//					diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
//							+ "where DISCOUNT_PROMO_ID in ("
//							+ "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in ("+sqlGroup+") or SKU_ID in ("+sqlProduk+") group by DISCOUNT_PROMO_ID"
//							+ ") "
//							+ "and DISCOUNT_PROMO_ID in ("
//							+ "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='"+customerModel.CustId+"' or CUSTOMER_TYPE_ID='"+customerTypeModel.CustomerTypeId+"' group by DISCOUNT_PROMO_ID "
//							+ ")"));
//				} else {
//					diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
//							+ "where DISCOUNT_PROMO_ID in ("
//							+ "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in ("+sqlGroup+") or SKU_ID in ("+sqlProduk+") group by DISCOUNT_PROMO_ID"
//							+ ")"));
//				}
//			} else {
//				try {
//					if(dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='"+customerModel.CustId+"' or CUSTOMER_TYPE_ID='"+customerTypeModel.CustomerTypeId+"'") > 0) {
//						diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
//						+ "where DISCOUNT_PROMO_ID not in (select DISCOUNT_PROMO_ID_NOACTIVE from DISCOUNT_PROMO_NOACTIVE where DISCOUNT_PROMO_ID='"+json.getString("DISCOUNT_PROMO_ID")+"') "
//						+ "and DISCOUNT_PROMO_ID in (select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in ("+sqlGroup+") or SKU_ID in ("+sqlProduk+") group by DISCOUNT_PROMO_ID) "
//						+ "and DISCOUNT_PROMO_ID in ("
//						+ "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='"+customerModel.CustId+"' or CUSTOMER_TYPE_ID='"+customerTypeModel.CustomerTypeId+"' group by DISCOUNT_PROMO_ID "
//						+ ") "), json);
//					} else {
//						diskonAdapter = new DiskonAdapter(this.context, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
//						+ "where DISCOUNT_PROMO_ID not in (select DISCOUNT_PROMO_ID_NOACTIVE from DISCOUNT_PROMO_NOACTIVE where DISCOUNT_PROMO_ID='"+json.getString("DISCOUNT_PROMO_ID")+"') "
//						+ "and DISCOUNT_PROMO_ID in (select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in ("+sqlGroup+") or SKU_ID in ("+sqlProduk+") group by DISCOUNT_PROMO_ID)"), json);
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//	    	diskonAdapter.setNotifyOnChange(true);
//	    	diskonAdapter.setCheckedChangeListener(this);
//    		((ListView)vDiskonPromo.findViewById(R.id.listdata)).setAdapter(diskonAdapter);
//		}
//	}
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                SKUModel model = (SKUModel) data.getSerializableExtra("data");
                addOrderItemSearch(new OrderDetailModel(model));
            }
        }

        if (getActivity() instanceof KunjunganActivity) {
            ((KunjunganActivity) context).isHome = true;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadOrder() {
        try {
            Log.d("Load Order", "Called");
            adapterOrder = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapterAll.getGroup(i);
                adapterOrder.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < adapterAll.getChildrenCount(i); j++) {
                    final OrderDetailModel orderDetailModel = adapterAll.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterOrder.addChild(groupSKUModel, orderDetailModel);

                        /*realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                            }
                        });
                        final CommonModel cmModel = realmUI
                                .where(CommonModel.class)
                                .equalTo("Done", 0)
                                .findFirst();*/

                        if (commonModel != null) {
                            realmUI.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    boolean needToAdd = false;
                                    if (!(realm.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                            .equalTo("CommonID", commonModel.CommonID)
                                            .equalTo("SKUId", orderDetailModel.skuModel.SKUId).findAll().size() > 0)) {
                                        needToAdd = true;
                                    }
                                    com.bhn.sadix.Data.TakingOrderModel tm = new com.bhn.sadix.Data.TakingOrderModel();

                                    tm.CustId = customerModel.CustId;
                                    tm.SKUId = orderDetailModel.skuModel.SKUId;
                                    tm.tanggal = "";
                                    tm.SalesProgramId = orderDetailModel.salesProgramModel != null ? orderDetailModel.salesProgramModel.SalesProgramId : "";
                                    tm.QTY_B = orderDetailModel.QTY_B;
                                    tm.QTY_K = orderDetailModel.QTY_K;
                                    tm.DISCOUNT1 = orderDetailModel.DISCOUNT1;
                                    tm.DISCOUNT2 = orderDetailModel.DISCOUNT2;
                                    tm.DISCOUNT3 = orderDetailModel.DISCOUNT3;
                                    tm.TOTAL_PRICE = orderDetailModel.TOTAL_PRICE;
                                    tm.PAYMENT_TERM = orderDetailModel.PAYMENT_TERM;
                                    tm.note = orderDetailModel.note;
                                    tm.PRICE_B = orderDetailModel.PRICE_B;
                                    tm.PRICE_K = orderDetailModel.PRICE_K;
                                    tm.LAMA_KREDIT = orderDetailModel.LAMA_CREDIT;
                                    tm.DiscountType = orderDetailModel.DiscountType;
                                    tm.CommonID = commonModel.CommonID;
                                    tm.RandomID = orderDetailModel.RandomID;
                                    if (isPrinciple) {
                                        tm.PrinsipleID = listener.getPrinsipleModel().PrinsipleId;
                                    }

                                    realm.copyToRealmOrUpdate(tm);

                                    if (needToAdd)
                                        commonModel.takingOrderModel.add(tm);
                                }
                            });
                        }

                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterOrder.removeHeader(groupSKUModel);
                }
            }
            if (vOrder != null) {
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                adapterOrder.notifyDataSetInvalidated();
            }

            loadDiskonPromo();
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        realmUI = Realm.getDefaultInstance();

        if (!isPrinciple)
            loadAdapter(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        realmUI.close();
//        calculateAmount();
    }

    public void calculateAmount() {
        Log.d("Calculate", "Called");
        int QTY_B = 0;
        int QTY_K = 0;
        double PRICE = 0;
        double DISCOUNT = 0;
        double S_AMOUNT = 0;

        if (adapterAll != null) {
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                for (int j = 0; j < adapterAll.getChildrenCount(i); j++) {
                    OrderDetailModel orderDetailModel = adapterAll.getChild(i, j);
                    QTY_B += orderDetailModel.QTY_B;
                    QTY_K += orderDetailModel.QTY_K;
                    PRICE += orderDetailModel.TOTAL_PRICE;
                }
            }

            if (view != null) {
                String cek = ((EditText) view.findViewById(R.id.discount)).getText().toString();
                DISCOUNT = Double.parseDouble(cek.equals("") ? "0" : cek);
            } else {
                DISCOUNT = commonModel.DISCOUNT;
            }

            if (customerTypeModel != null) {
                if (DISCOUNT > customerTypeModel.DiskonMax) {
                    String max = "";
                    if (customerTypeModel.DiskonType.equals("2")) {
                        max = "Rp." + customerTypeModel.DiskonMax;
                    } else {
                        max = "" + customerTypeModel.DiskonMax + "%";
                    }
                    Util.showDialogInfo(context, "Nilai melebihi batas masimum " + max);
//                    ((EditText) view.findViewById(R.id.discount)).setText(null);
                    return;
                } else {
                    if (customerTypeModel.DiskonType.equals("2")) {
                        S_AMOUNT = PRICE - DISCOUNT;
                    } else {
                        S_AMOUNT = PRICE - (PRICE * DISCOUNT / 100);
                    }
                }
            }
            final int finalQTY_B = QTY_B;
            final int finalQTY_K = QTY_K;
            final double finalPRICE = PRICE;
            final double finalDISCOUNT = DISCOUNT;
            final double finalS_AMOUNT = S_AMOUNT;

            try {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        commonModel.QTY_B = finalQTY_B;
                        commonModel.QTY_K = finalQTY_K;
                        commonModel.PRICE = finalPRICE;

                        Log.d("Common Price", "UPdated");

                        commonModel.DISCOUNT = finalDISCOUNT;
                        commonModel.S_AMOUNT = finalS_AMOUNT;
                    }
                });

                if (view != null) {
                    ((TextView) view.findViewById(R.id.total_qty)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
                    ((TextView) view.findViewById(R.id.price)).setText(Util.numberFormat.format(commonModel.PRICE));
                    ((TextView) view.findViewById(R.id.total_price)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
                }

                if (isPrinciple) {
                    if (commonModel.PRICE == 0 && listener.getPrinsipleModel().price > 0) {
                        realmUI.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                commonModel.QTY_B = listener.getPrinsipleModel().total_qty_b; /*getActivity().getIntent().getIntExtra("total_qty_b", 0);*/
                                commonModel.QTY_K = listener.getPrinsipleModel().total_qty_k; /*getActivity().getIntent().getIntExtra("total_qty_k", 0);*/
                                commonModel.PRICE = listener.getPrinsipleModel().price; /*getActivity().getIntent().getDoubleExtra("price", 0);*/
                                commonModel.DISCOUNT = listener.getPrinsipleModel().discount; /*getActivity().getIntent().getDoubleExtra("discount", 0);*/
                                commonModel.S_AMOUNT = listener.getPrinsipleModel().total_price; /*getActivity().getIntent().getDoubleExtra("total_price", 0);*/
                            }
                        });

                        if (view != null) {
                            ((TextView) view.findViewById(R.id.total_qty)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
                            ((TextView) view.findViewById(R.id.price)).setText(Util.numberFormat.format(commonModel.PRICE));
                            ((TextView) view.findViewById(R.id.total_price)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                /*realmUI = Realm.getDefaultInstance();
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        commonModel.QTY_B = finalQTY_B;
                        commonModel.QTY_K = finalQTY_K;
                        commonModel.PRICE = finalPRICE;

                        Log.d("Common Price", "UPdated");

                        commonModel.DISCOUNT = finalDISCOUNT;
                        commonModel.S_AMOUNT = finalS_AMOUNT;
                    }
                });*/
            }
        }
    }

    private void total(final View dialog, final OrderDetailModel model) {
        double total = totalValue(dialog, model);
        if (total < 0) {
            Util.showDialogInfo(context, "Diskon anda melebihi total harga!");
            ((EditText) dialog.findViewById(R.id.discount_1)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_2)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_3)).setText("0");
            ((TextView) dialog.findViewById(R.id.total_price)).setText("0");
        } else {
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(total));
        }
    }

    private double totalValue(final View dialog, final OrderDetailModel model) {
        try {
            boolean isDiskonBesar = ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked();
            String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
            int jml1 = Integer.parseInt(cek.equals("") ? "0" : cek);
            cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
            int jml2 = Integer.parseInt(cek.equals("") ? "0" : cek);

            double diskon = 0;

            //diskon 1
            cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
            double diskon1 = 0;
            if (model.skuModel.DISCOUNT_1_TYPE.equals("2")) {
                diskon1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon1 > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(context, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(context, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                } else {
                    diskon1 = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) * persen / 100;
                }
            }
            diskon = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) - diskon1;
            //diskon 2
            cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
            double diskon2 = 0;
            if (model.skuModel.DISCOUNT_2_TYPE.equals("2")) {
                diskon2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon2 > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(context, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(context, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                } else {
                    diskon2 = diskon * persen / 100;
                }
            }
            diskon = diskon - diskon2;
            //hitung total
            double total = 0;
//			if(model.skuModel.DISCOUNT_TYPE.equals("1")) {
            if (isDiskonBesar) {
//				double het_b = model.skuModel.HET_PRICE_L;
//				het_b -= diskon1;
//				het_b -= diskon2;
                total = (jml1 * diskon) + (jml2 * model.skuModel.HET_PRICE);
            } else {
//				double het_k = model.skuModel.HET_PRICE;
//				het_k -= diskon1;
//				het_k -= diskon2;
                double qty_k = jml1 * model.skuModel.CONVER;
                qty_k += jml2;
//				total = het_k*qty_k;
                total = diskon * qty_k;
            }
            //diskon 3
            cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
            double diskon3 = 0;
            if (model.skuModel.DISCOUNT_3_TYPE.equals("2")) {
                diskon3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon3 > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(context, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(context, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                } else {
                    diskon3 = total * persen / 100;
                }
            }
            total = total - diskon3;
            return total;
        } catch (Exception e) {
            return 0;
        }

    }

    private void addJumlah(final OrderDetailModel model) {
        final Dialog dl = new Dialog(getActivity());
        dl.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dl.setContentView(R.layout.dialog_taking_order);
        dl.setContentView(R.layout.dialog_new_taking_order);
        ((TextView) dl.findViewById(R.id.title)).setText(model.skuModel.ProductName);
        ((TextView) dl.findViewById(R.id.SKUCODE)).setText(model.skuModel.SKUCODE);

//		final List<View> lViews = new ArrayList<View>();
        final ViewPager mPager = (ViewPager) dl.findViewById(R.id.pager);
        mPager.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(View collection, final int position) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = null;
                if (position == 0) {
                    v = inflater.inflate(R.layout.dialog_taking_order, null);
                    showDataTakingOrder(v, model, dl);
                } else if (position == 1) {
                    v = inflater.inflate(R.layout.dialog_taking_order_image, null);
                    loadImage(model, v);
                }
//		    	lViews.add(v);
                ((ViewPager) collection).addView(v, 0);
                return v;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == ((View) object);
            }

            @Override
            public void destroyItem(View collection, int position, Object view) {
                ((ViewPager) collection).removeView((View) view);
            }

            @Override
            public Parcelable saveState() {
                return null;
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
//		final View dialog = lViews.get(1);
//		final View image = lViews.get(0);

        dl.findViewById(R.id.panel_produk).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        dl.findViewById(R.id.gambar).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });

        dl.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dl.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void loadImage(final OrderDetailModel model, final View image) {
        if (model.bitmap == null) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        String data = ConnectionServer.requestJSONObjectNonThread((JSONObject) null, Util.getServerUrl(context) + "getskupic/SKUid/" + model.skuModel.SKUId, true);
                        JSONObject json = new JSONObject(data);
                        if (json.has("SKUPicture")) {
                            byte[] img = Base64.decode(json.getString("SKUPicture"));
                            model.bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                            if (model.bitmap != null) {
                                image.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                        image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                        ((ImageViewTouch) image.findViewById(R.id.img)).setDisplayType(DisplayType.FIT_IF_BIGGER);
                                        ((ImageViewTouch) image.findViewById(R.id.img)).setImageBitmap(model.bitmap);
                                    }
                                });
                            } else {
                                image.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                        image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        } else {
                            image.post(new Runnable() {
                                @Override
                                public void run() {
                                    image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                    image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                                }
                            });
                        }
//                        ((TextView) image.findViewById(R.id.img_desc)).setText(json.getString("SKUDesc"));
//                        ((TextView) image.findViewById(R.id.img_desc)).setText("SKU DESC" + model.skuModel.SKUId);
                        if (json.has("SKUDesc")) {
                            Log.d("Desc", json.getString("SKUDesc"));
                            model.SkuDesc = json.getString("SKUDesc").equals("null") ? "-" : json.getString("SKUDesc");
                            ((TextView) image.findViewById(R.id.img_desc)).setText(json.getString("SKUDesc"));
                        } else {
                            Log.d("Desc", "Desc Not Available");
                            model.SkuDesc = "-";
                            image.findViewById(R.id.img_desc).setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        image.post(new Runnable() {
                            @Override
                            public void run() {
                                image.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                image.findViewById(R.id.img).setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            };
            t.start();
        } else {
            image.findViewById(R.id.progressBar).setVisibility(View.GONE);
            image.findViewById(R.id.img).setVisibility(View.VISIBLE);
            ((ImageViewTouch) image.findViewById(R.id.img)).setDisplayType(DisplayType.FIT_IF_BIGGER);
            ((ImageViewTouch) image.findViewById(R.id.img)).setImageBitmap(model.bitmap);
            ((TextView) image.findViewById(R.id.img_desc)).setText(model.SkuDesc);
        }
    }

    private void showDataTakingOrder(final View dialog, final OrderDetailModel model, final Dialog dl) {
        try {
            if (customerModel.CustTop == 0) {
                ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
                dialog.findViewById(R.id.credit).setEnabled(false);
                dialog.findViewById(R.id.cash).setEnabled(false);
            } else {
                ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
            }

            if (ctrlAppModul.isModul("34")) {
                CrcOrderModel crcOrderModel = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
                if (crcOrderModel != null) {
                    if (model.QTY_B == 0) {
//						model.QTY_B = crcOrderModel.QTY_L;
                        ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(crcOrderModel.QTY_L));
                    }
                    if (model.QTY_K == 0) {
//						model.QTY_K = crcOrderModel.QTY;
                        ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(crcOrderModel.QTY));
                    }
                }
            }

            int visibility = ctrlAppModul.isModul("53") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_1).setVisibility(visibility);
            visibility = ctrlAppModul.isModul("54") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_2).setVisibility(visibility);
            visibility = ctrlAppModul.isModul("55") ? View.VISIBLE : View.GONE;
            dialog.findViewById(R.id.row_discount_3).setVisibility(visibility);

            AppModul appModul = ctrlAppModul.get("57");
            if (appModul != null) {
                if (appModul.AppModulValue.equals("0")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul.AppModulValue.equals("1")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul.AppModulValue.equals("2")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                } else if (appModul.AppModulValue.equals("3")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                }
            }
            AppModul appModul68 = ctrlAppModul.get("68");
            if (appModul68 != null) {
                if (appModul68.AppModulValue.equals("0")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
                } else if (appModul68.AppModulValue.equals("1")) {
                    dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                    ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
                }
            }

            final KeyListener keyJml1 = ((EditText) dialog.findViewById(R.id.jml_1)).getKeyListener();
            final KeyListener keyJml2 = ((EditText) dialog.findViewById(R.id.jml_2)).getKeyListener();

            ((EditText) dialog.findViewById(R.id.jml_1)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (ctrlAppModul.isModul("73")) {
                        String cek = s.toString();
                        cek = cek.equals("") ? "0" : cek;
                        if (Integer.parseInt(cek) == 0) {
                            ((EditText) dialog.findViewById(R.id.jml_1)).setKeyListener(keyJml1);
                            ((EditText) dialog.findViewById(R.id.jml_2)).setKeyListener(keyJml2);
                        } else {
                            ((EditText) dialog.findViewById(R.id.jml_2)).setText("0");
                            ((EditText) dialog.findViewById(R.id.jml_2)).setKeyListener(null);
                        }
                    }
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.jml_2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (ctrlAppModul.isModul("73")) {
                        String cek = s.toString();
                        cek = cek.equals("") ? "0" : cek;
                        if (Integer.parseInt(cek) == 0) {
                            ((EditText) dialog.findViewById(R.id.jml_1)).setKeyListener(keyJml1);
                            ((EditText) dialog.findViewById(R.id.jml_2)).setKeyListener(keyJml2);
                        } else {
                            ((EditText) dialog.findViewById(R.id.jml_1)).setText("0");
                            ((EditText) dialog.findViewById(R.id.jml_1)).setKeyListener(null);
                        }
                    }
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_1)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((EditText) dialog.findViewById(R.id.discount_3)).addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    total(dialog, model);
                }
            });
            ((RadioGroup) dialog.findViewById(R.id.rb_diskon)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    total(dialog, model);
                }
            });
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(model.TOTAL_PRICE));
            ((RadioGroup) dialog.findViewById(R.id.payment_term)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.cash) {
                        dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
                        ((EditText) dialog.findViewById(R.id.lama_credit)).setText(null);
                    } else if (checkedId == R.id.credit) {
                        dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
                        ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
                    }
                }
            });
            if (customerModel.CustTop > 0) {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setKeyListener(null);
            }
            dialog.findViewById(R.id.stok_1).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    stok(model, dialog);
                }
            });
            dialog.findViewById(R.id.stok_2).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    stok(model, dialog);
                }
            });

            String title = model.skuModel.DISCOUNT_1_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_1)).setText("Discount1 " + title);
            title = model.skuModel.DISCOUNT_2_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_2)).setText("Discount2 " + title);
            title = model.skuModel.DISCOUNT_3_TYPE.equals("2") ? "(Rp)" : "(%)";
            ((TextView) dialog.findViewById(R.id.lbl_discount_3)).setText("Add Discount " + title);

            ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
            ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
            ((Spinner) dialog.findViewById(R.id.SalesProgram)).setAdapter(new SalesProgramAdapter(context, ctrlSalesProgram.listByGropID(model.skuModel.GroupId)));
            if (model.salesProgramModel != null) {
                int index = ((SalesProgramAdapter) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getAdapter()).indexOf(model.salesProgramModel);
                ((Spinner) dialog.findViewById(R.id.SalesProgram)).setSelection(index);
            }
            if (customerModel.CustTop > 0 && (model.PAYMENT_TERM == null || model.PAYMENT_TERM.equals("") || model.PAYMENT_TERM.equals("2"))) {
                ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
                dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
            } else if (model.PAYMENT_TERM != null && model.PAYMENT_TERM.equals("1")) {
                ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
                dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
            }
            ((EditText) dialog.findViewById(R.id.note)).setText(model.note);
            ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
            ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
            ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setText(model.skuModel.SBESAR);
            ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setText(model.skuModel.SKECIL);

            ((TextView) dialog.findViewById(R.id.price_1)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE_L));
            ((TextView) dialog.findViewById(R.id.price_2)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE));
            if (model.LAMA_CREDIT > 0) {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(model.LAMA_CREDIT));
            } else {
                ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
            }

            if (model.QTY_B > 0) {
                ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
            }
            if (model.QTY_K > 0) {
                ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
            }
            if (model.DISCOUNT1 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_1)).setText(String.valueOf(model.DISCOUNT1));
            }
            if (model.DISCOUNT2 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_2)).setText(String.valueOf(model.DISCOUNT2));
            }
            if (model.DISCOUNT3 > 0) {
                ((EditText) dialog.findViewById(R.id.discount_3)).setText(String.valueOf(model.DISCOUNT3));
            }
            dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dl.dismiss();
                }
            });
            dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                    int QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                    int QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);
                    if (isCanvas) {
                        if (QTY_B > model.skuModel.QBESAR) {
                            Util.showDialogInfo(context, "Stok " + model.skuModel.SBESAR + " tidak cukup!");
                            return;
                        }
                        if (QTY_K > model.skuModel.QKECIL) {
                            if (model.skuModel.QBESAR > 0) {
                                Util.confirmDialog(context, "Info", "Stok " + model.skuModel.SKECIL + " tidak cukup, Apakah ingin ambil dari stok " + model.skuModel.QBESAR + "?", new ConfirmListener() {
                                    @Override
                                    public void onDialogCompleted(boolean answer) {
                                        if (answer) {
                                            model.skuModel.QBESAR -= 1;
                                            model.skuModel.QKECIL += model.skuModel.CONVER;
                                            ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.skuModel.QBESAR));
                                            ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.skuModel.QKECIL));
                                        } else {
                                            return;
                                        }
                                    }
                                });
                            } else {
                                Util.showDialogInfo(context, "Stok " + model.skuModel.SKECIL + " tidak cukup dan stok " + model.skuModel.SBESAR + " tidak cukup!");
                            }
                            return;
                        }
                    }
                    model.QTY_B = QTY_B;
                    model.QTY_K = QTY_K;
                    model.salesProgramModel = (SalesProgramModel) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getSelectedItem();
                    cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
                    model.DISCOUNT1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
                    model.DISCOUNT2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
                    model.DISCOUNT3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                    model.note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();
                    if (((RadioButton) dialog.findViewById(R.id.cash)).isChecked()) {
                        model.PAYMENT_TERM = "1";
                    } else if (((RadioButton) dialog.findViewById(R.id.credit)).isChecked()) {
                        model.PAYMENT_TERM = "2";
                    }
                    cek = ((EditText) dialog.findViewById(R.id.lama_credit)).getText().toString();
                    model.LAMA_CREDIT = Integer.parseInt(cek.equals("") ? "0" : cek);
                    model.PRICE_B = model.skuModel.HET_PRICE_L;
                    model.PRICE_K = model.skuModel.HET_PRICE;
                    model.TOTAL_PRICE = totalValue(dialog, model);

                    //tambahan untuk tipe diskon
                    if (((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked()) {
                        model.DiscountType = "1";
                    } else if (((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).isChecked()) {
                        model.DiscountType = "0";
                    }

//                    calculateAmount();

                    adapterAll.notifyDataSetChanged();
                    if (adapterFocus != null) {
                        adapterFocus.notifyDataSetChanged();
                    }

                    if (adapterPromo != null)
                        adapterPromo.notifyDataSetChanged();

                    if (adapterSearch != null) {
                        adapterSearch.notifyDataSetChanged();
                    }
                    calculateAmount();
                    loadOrder();

                    dl.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stok(final OrderDetailModel model, final View dialog) {
        if (isCanvas) {
            ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.skuModel.QBESAR));
            ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.skuModel.QKECIL));
        } else {
            server.addConnectionEvent(new ConnectionEvent() {
                @Override
                public void messageServer(String respon) {
                    try {
                        JSONObject json = new JSONArray(respon).getJSONObject(0);
                        model.STOK_B = json.getInt("StockQty_L");
                        model.STOK_K = json.getInt("StockQty");
                        ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
                        ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
                    } catch (Exception e) {
                        e.printStackTrace();
                        error(e.getMessage());
                    }
                }

                @Override
                public void error(String error) {
                    Util.showDialogError(context, error);
                }
            });
            server.setUrl(Util.getServerUrl(context) + "getSKUstock/SalesId/" + UserModel.getInstance(context).SalesId + "/SKUId/" + model.skuModel.SKUId);
            server.requestJSONObject(null);
        }
    }

    public void initAdapter() {
        try {
            dbSku = new DbSkuHelper(getActivity());
            adapterFocus = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            adapterPromo = new OrderDetailAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapterAll.getGroup(i);
//                populateSKUByGroup(1, groupSKUModel, true);
                String sql = "select * from groupsku_custtype where groupsku_id='" + groupSKUModel.GroupId + "' "
                        + "and custtype_id='" + customerModel.CustomerTypeId + "'";

                Cursor cursor = dbSku.query(sql);
                while (cursor.moveToNext()) {
                    if (cursor.getInt(2) == 1) {
                        adapterFocus.addHeader(groupSKUModel);
//                        populateSKUByGroup(2, groupSKUModel);
                    } else if (cursor.getInt(2) == 2) {
                        adapterPromo.addHeader(groupSKUModel);
//                        populateSKUByGroup(3, groupSKUModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(activity, e.getMessage());
        }
    }

    private void populateSKUByGroup(int jenis, GroupSKUModel group, boolean showProgress) {
        try {
            List<OrderDetailModel> list = null;
//            List<OrderDetailModel> list = adapterAll.listOrderByGroup(group);

            if (jenis == 1) list = adapterAll.listOrderByGroup(group);
            else if (jenis == 2) list = adapterFocus.listOrderByGroup(group);
            else if (jenis == 3) list = adapterPromo.listOrderByGroup(group);
            {
                if (list == null || list.size() < group.jumlah) {
                    Log.wtf("List ODM", "masuk");
                    List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                    for (SKUModel skuModel : listSKU) {
                        OrderDetailModel orderDetailModel = new OrderDetailModel(skuModel);
                        Log.wtf("List SKU", "masuk");
                        if (list == null || list.indexOf(orderDetailModel) == -1) {
                            Log.wtf("List index", "masuk");
                            TakingOrderModel takingOrderModel = null;
                            com.bhn.sadix.Data.TakingOrderModel takingOrder = null;

                            if (isPrinciple) {
                                PrincipleModel pModel = realmUI.where(PrincipleModel.class)
                                        .equalTo("PrinsipleId", listener.getPrinsipleModel().PrinsipleId)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .findFirst();

                                RealmList<com.bhn.sadix.Data.TakingOrderModel> ls = pModel.list;
                                //                            RealmList<com.bhn.sadix.Data.TakingOrderModel> ls = listener.getPrinsipleModel().list; /*(ArrayList<TakingOrderModel>) getActivity().getIntent().getSerializableExtra("list");*/
                                if (ls != null) {
                                    takingOrder = ls.where().equalTo("SKUId", skuModel.SKUId).findFirst();
                                    /*int index = ls.indexOf(new com.bhn.sadix.Data.TakingOrderModel(skuModel.SKUId));
                                    if (index != -1) {*/
                                    /*takingOrder = ls.get(index);*/
                                    //                                }
                                }
                            }

                            if (takingOrder == null) {
                                takingOrder = realmUI.where(com.bhn.sadix.Data.TakingOrderModel.class)
                                        .equalTo("CommonID", commonModel.CommonID)
                                        .equalTo("SKUId", skuModel.SKUId)
                                        .findFirst();
                            }

                            if (takingOrder != null) {
                                orderDetailModel.DISCOUNT1 = takingOrder.DISCOUNT1;
                                orderDetailModel.DISCOUNT2 = takingOrder.DISCOUNT2;
                                orderDetailModel.DISCOUNT3 = takingOrder.DISCOUNT3;
                                orderDetailModel.note = takingOrder.note;
                                orderDetailModel.PAYMENT_TERM = takingOrder.PAYMENT_TERM;
                                orderDetailModel.QTY_B = takingOrder.QTY_B;
                                orderDetailModel.QTY_K = takingOrder.QTY_K;
                                String SalesProgramId = takingOrder.SalesProgramId;
                                if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                    orderDetailModel.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                                    if (orderDetailModel.salesProgramModel == null) {
                                        orderDetailModel.salesProgramModel = new SalesProgramModel();
                                    }
                                } else {
                                    orderDetailModel.salesProgramModel = new SalesProgramModel();
                                }
                                orderDetailModel.TOTAL_PRICE = takingOrder.TOTAL_PRICE;
                                orderDetailModel.LAMA_CREDIT = takingOrder.LAMA_KREDIT;
                                orderDetailModel.PRICE_B = takingOrder.PRICE_B;
                                orderDetailModel.PRICE_K = takingOrder.PRICE_K;
                                orderDetailModel.RandomID = takingOrder.RandomID;
                                orderDetailModel.DiscountType = takingOrder.DiscountType;
                            }

                            adapterAll.addChild(group, orderDetailModel);
                            if (jenis == 1) { //all
                                if (group.FokusId.equals("1")) {
                                    if (adapterFocus != null)
                                        adapterFocus.addChild(group, orderDetailModel);
                                }
                                if (group.PromoId.equals("1")) {
                                    if (adapterPromo != null)
                                        adapterPromo.addChild(group, orderDetailModel);
                                }
                            } else if (jenis == 2) { //focus
                                Log.wtf("jenis 2", "masuk");
                                adapterFocus.addChild(group, orderDetailModel);
                                adapterFocus.notifyDataSetChanged();
                                if (group.PromoId.equals("1")) {
                                    adapterPromo.addChild(group, orderDetailModel);
                                    adapterPromo.notifyDataSetChanged();
                                }
                            } else if (jenis == 3) { //promo
                                Log.wtf("jenis 3 ", "masuk");
                                adapterPromo.addChild(group, orderDetailModel);
                                adapterPromo.notifyDataSetChanged();
                                if (group.FokusId.equals("1")) {
                                    adapterFocus.addChild(group, orderDetailModel);
                                    adapterFocus.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }

        calculateAmount();
    }

    public CommonModel getCommonModel() {
        return commonModel;
    }

    public OrderDetailAdapter getOrderAdapter() {
        return adapterAll;
    }

    class ViewPagerAdapter extends PagerAdapter {
        //	    final int PAGE_COUNT = 6;
        @Override
        public Object instantiateItem(View collection, final int position) {
            /*LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);*/

            final View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_taking_order, null);
            SegmentedRadioGroup segmentedRadioGroup = (SegmentedRadioGroup) v.findViewById(R.id.second_tab);
            final ViewPager viewPager = (ViewPager) v.findViewById(R.id.pager_content_to);
            ContentAdapter contentAdapter;

            if (position == 0) {
                mainProduct = v;

                v.findViewById(R.id.tab_first).setVisibility(View.VISIBLE);
                ((RadioButton) v.findViewById(R.id.tab_first)).setText("Focus");
                v.findViewById(R.id.tab_second).setVisibility(View.VISIBLE);
                ((RadioButton) v.findViewById(R.id.tab_second)).setText("Promo");
                v.findViewById(R.id.tab_third).setVisibility(View.VISIBLE);
                ((RadioButton) v.findViewById(R.id.tab_third)).setText("All Product");

                contentAdapter = new ContentAdapter(1);
                viewPager.setAdapter(contentAdapter);

                ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        if (position == 0) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_first);
                            viewPager.setCurrentItem(0);
                        } else if (position == 1) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_second);
                            viewPager.setCurrentItem(1);
                        } else if (position == 2) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_third);
                            viewPager.setCurrentItem(2);
                        }
                        super.onPageSelected(position);
                    }
                };
                viewPager.addOnPageChangeListener(ViewPagerListener);

                segmentedRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                        switch (i) {
                            case R.id.tab_first:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.tab_second:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.tab_third:
                                viewPager.setCurrentItem(2);
                                break;
                        }
                    }
                });

            } else if (position == 1) {
                v.findViewById(R.id.tab_first).setVisibility(View.VISIBLE);
                if (!ctrlAppModul.isModul("61")) {
                    ((RadioButton) v.findViewById(R.id.tab_first)).setText("Order");
                } else {
                    ((RadioButton) v.findViewById(R.id.tab_first)).setText("Discount & Promo");
                }
                v.findViewById(R.id.tab_second).setVisibility(View.GONE);
                v.findViewById(R.id.tab_third).setVisibility(View.GONE);

                if (!ctrlAppModul.isModul("61")) {
                    contentAdapter = new ContentAdapter(3);
                } else {
                    contentAdapter = new ContentAdapter(2);
                }
                viewPager.setAdapter(contentAdapter);

                ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        if (position == 0) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_first);
                            viewPager.setCurrentItem(0);
                        }
                        super.onPageSelected(position);
                    }
                };
                viewPager.setOnPageChangeListener(ViewPagerListener);

                segmentedRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                        switch (i) {
                            case R.id.tab_first:
                                viewPager.setCurrentItem(0);
                                break;
                        }
                    }
                });

            } else if (position == 2) {
                v.findViewById(R.id.tab_first).setVisibility(View.VISIBLE);
                if (ctrlAppModul.isModul("61")) {
                    ((RadioButton) v.findViewById(R.id.tab_first)).setText("Order");
                    ((RadioButton) v.findViewById(R.id.tab_second)).setText("Bonus");
                    v.findViewById(R.id.tab_second).setVisibility(View.VISIBLE);
                } else {
                    ((RadioButton) v.findViewById(R.id.tab_first)).setText("Order");
                    v.findViewById(R.id.tab_second).setVisibility(View.GONE);
                }
                v.findViewById(R.id.tab_third).setVisibility(View.GONE);

                contentAdapter = new ContentAdapter(3);
                viewPager.setAdapter(contentAdapter);

                ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        if (position == 0) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_first);
                            viewPager.setCurrentItem(0);
                        } else if (position == 1) {
                            ((SegmentedRadioGroup) v.findViewById(R.id.second_tab)).check(R.id.tab_second);
                            viewPager.setCurrentItem(1);
                        }
                        super.onPageSelected(position);
                    }
                };
                viewPager.setOnPageChangeListener(ViewPagerListener);

                segmentedRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                        switch (i) {
                            case R.id.tab_first:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.tab_second:
                                viewPager.setCurrentItem(1);
                                break;
                        }
                    }
                });
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            super.finishUpdate(container);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return ctrlAppModul.isModul("61") ? 3 : 2;
        }
    }

    class ContentAdapter extends PagerAdapter {
        int type;

        public ContentAdapter(int type) {
            this.type = type;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View v = null;/*= LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);*/

            if (type == 1) {
                switch (position) {
                    case 0:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);

                        Log.d("Focus Called", "Called");
                        vFocus = v;
                        v.findViewById(R.id.list).setVisibility(View.VISIBLE);
                        ((ExpandableListView) v.findViewById(R.id.list)).setAdapter(adapterFocus);
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                addJumlah(adapter.getChild(groupPosition, childPosition));
                                return false;
                            }
                        });
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                Log.wtf("Focus Clicked", "YESS BOSS");
//                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                populateSKUByGroup(2, adapterFocus.getGroup(groupPosition), false);
                                return false;
                            }
                        });

                        /*for (int i = 0; i < adapterFocus.getGroupCount(); i++) {
                            populateSKUByGroup(2, adapterFocus.getGroup(i));
                        }*/
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v);
                        return v;
//                        break;
                    case 1:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                        Log.d("Promo Called", "Called");
                        vPromo = v;
                        v.findViewById(R.id.list).setVisibility(View.VISIBLE);
                        ((ExpandableListView) v.findViewById(R.id.list)).setAdapter(adapterPromo);
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                addJumlah(adapter.getChild(groupPosition, childPosition));
                                return false;
                            }
                        });
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                populateSKUByGroup(3, adapterPromo.getGroup(groupPosition), false);
                                return false;
                            }
                        });
                        /*for (int i = 0; i < adapterPromo.getGroupCount(); i++) {
                            populateSKUByGroup(3, adapterPromo.getGroup(i));
                        }*/
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v/*, 0*/);
                        return v;
//                        break;
                    case 2:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                        Log.d("All Called", "Called");
                        vAll = v;
                        v.findViewById(R.id.list).setVisibility(View.VISIBLE);
                        ((ExpandableListView) v.findViewById(R.id.list)).setAdapter(adapterAll);

                        Log.d("Adapter All Size", "" + adapterAll.getGroupCount());

                        ((ExpandableListView) v.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                addJumlah(adapter.getChild(groupPosition, childPosition));
                                return false;
                            }
                        });
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                populateSKUByGroup(1, adapter.getGroup(groupPosition), false);
                                return false;
                            }
                        });
                        /*for (int i = 0; i < adapterAll.getGroupCount(); i++) {
                            populateSKUByGroup(1, adapterAll.getGroup(i));
                        }*/
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v/*, 0*/);
                        return v;
//                        break;
                }
            } else if (type == 2) {
                v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                switch (position) {
                    case 0:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                        Log.d("Discount Called", "Called");
                        vDiskonPromo = v;
                        v.findViewById(R.id.listdata).setVisibility(View.VISIBLE);
//                        ((ListView) v.findViewById(R.id.listdata)).setAdapter(diskonAdapter);
                        loadDiskonPromo();
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v/*, 0*/);
                        return v;
//                        break;
                }
            } else if (type == 3) {
                v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                switch (position) {
                    case 0:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);
                        Log.d("Order Called", "Called - adapter size" + adapterOrder.getGroupCount());
                        vOrder = v;
                        v.findViewById(R.id.list).setVisibility(View.VISIBLE);
                        ((ExpandableListView) v.findViewById(R.id.list)).setAdapter(adapterOrder);
                        ((ExpandableListView) v.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                                addJumlah(adapter.getChild(groupPosition, childPosition));
                                return false;
                            }
                        });
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v/*, 0*/);
                        return v;
//                        break;
                    case 1:
                        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_produk_taking_order, null);

                        vHadiah = v;

                        Log.d("Hadiah Called", "Called");
                        v.findViewById(R.id.list).setVisibility(View.VISIBLE);
                        if (diskonPromoAdapter != null) {
                            ((ExpandableListView) v.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
                        }
                        /*if (container.getChildCount() > 0)
                            container.removeViewAt(0);*/
                        container.addView(v/*, 0*/);
                        return v;
//                        break;
                    default:
                        return v;
                }
            }

            return v;
/*
            if (container.getChildCount() > 0)
                container.removeViewAt(0);
            container.addView(v, 0);
            return v;*/
        }

        @Override
        public void finishUpdate(ViewGroup container) {

            super.finishUpdate(container);
        }

        @Override
        public int getCount() {
            switch (type) {
                case 1:
                    return 3;
                case 2:
                    return 1;
                case 3:
                    if (!ctrlAppModul.isModul("61")) {
                        return 1;
                    } else {
                        return 2;
                    }
                default:
                    return 0;
            }
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void addOrderItemSearch(OrderDetailModel model) {
        try {
            int index = adapterAll.getPosisionHeader(new GroupSKUModel(model.skuModel.GroupId));
            List<OrderDetailModel> list = adapterAll.listOrderByGroup(adapterAll.getGroup(index));

            if (list != null) {
                int indexProduk = list.indexOf(model);
                if (indexProduk == -1) {
                    TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, model.skuModel.SKUId);
                    if (takingOrderModel != null) {
                        model.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                        model.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                        model.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                        model.note = takingOrderModel.note;
                        model.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                        model.QTY_B = takingOrderModel.QTY_B;
                        model.QTY_K = takingOrderModel.QTY_K;
                        String SalesProgramId = takingOrderModel.SalesProgramId;
                        if (SalesProgramId != null && !SalesProgramId.equals("")) {
                            model.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                        }
                        model.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                        model.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                        model.PRICE_B = takingOrderModel.PRICE_B;
                        model.PRICE_K = takingOrderModel.PRICE_K;
                        model.RandomID = takingOrderModel.RandomID;
                    }
                    adapterAll.addChild(adapterAll.getGroup(index), model);
                    if (model.skuModel.FokusId.equals("1")) {
                        adapterFocus.addChild(adapterAll.getGroup(index), model);
                    }
                    if (model.skuModel.PromoId.equals("1")) {
                        adapterPromo.addChild(adapterAll.getGroup(index), model);
                    }
                    addJumlah(model);
                } else {
                    addJumlah(list.get(indexProduk));
                }
            } else {
                TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, model.skuModel.SKUId);
                if (takingOrderModel != null) {
                    model.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                    model.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                    model.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                    model.note = takingOrderModel.note;
                    model.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                    model.QTY_B = takingOrderModel.QTY_B;
                    model.QTY_K = takingOrderModel.QTY_K;
                    String SalesProgramId = takingOrderModel.SalesProgramId;
                    if (SalesProgramId != null && !SalesProgramId.equals("")) {
                        model.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                    }
                    model.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                    model.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                    model.PRICE_B = takingOrderModel.PRICE_B;
                    model.PRICE_K = takingOrderModel.PRICE_K;
                    model.RandomID = takingOrderModel.RandomID;
                }
                adapterAll.addChild(adapterAll.getGroup(index), model);
                if (model.skuModel.FokusId.equals("1")) {
                    adapterFocus.addChild(adapterAll.getGroup(index), model);
                }
                if (model.skuModel.PromoId.equals("1")) {
                    adapterPromo.addChild(adapterAll.getGroup(index), model);
                }
                addJumlah(model);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    @Override
    public void onCheckedChanged(DiskonAdapter adapter, JSONObject json, boolean isChecked) {
        try {
//			if(!isChecked) {
//				loadDiskonPromo(isChecked, null);
//				diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
//				if(vHadiah != null) {
//	    			((ExpandableListView)vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
//	    		}
//				return;
//			} else {
//				loadDiskonPromo(isChecked, json);
//			}

            if (!isChecked) {
                final JSONObject finalJson1 = json;
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        try {
                            DiscountPromoModel model = realm.where(DiscountPromoModel.class).equalTo("DiscountPromoID", finalJson1.getInt("DISCOUNT_PROMO_ID")).findFirst();
                            model.deleteFromRealm();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                removelistPilih(json);
            } else {
                final JSONObject finalJson = json;
                Log.d("Discount Promo", "Added");
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        DiscountPromoModel model = realm.createObject(DiscountPromoModel.class);
                        model.CommonID = commonModel.CommonID;
                        model.CustomerId = Integer.parseInt(customerModel.CustId);
                        try {
                            model.DiscountPromoID = finalJson.getInt("DISCOUNT_PROMO_ID");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                listPilih.add(json);
            }

//            diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
            diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<String>(), new HashMap<String, List<DiskonPromoModel>>());

            for (int i = 0; i < adapter.getCount(); i++) {
                json = adapter.getItem(i);
//                Log.d("List JSON")
                if (json.has("isChecked") && json.getBoolean("isChecked")) {
                    boolean isDiskon = false;
                    if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "'") > 0) {
                        if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' and CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "' ") > 0) {
                            if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' and CUSTOMER_ID='" + customerModel.CustId + "' ") > 0) {
                                isDiskon = true;
                            } else {
                                isDiskon = true;
                            }
                        }
                    } else {
                        isDiskon = true;
                    }
                    if (isDiskon) {
                        Log.wtf("Is Diskon", "True");
                        if (json.getInt("DISCOUNT_PROMO_TYPE") == 1) {
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_TOTAL where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "'");
                            checkDiscountTotal(list);
                        } else if (json.getInt("DISCOUNT_PROMO_TYPE") == 2) {
                            Log.wtf("Diskon type", "DUA");
                            Log.wtf("DISKON PROMO ID", json.getString("DISCOUNT_PROMO_ID"));
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_PROMO_ITEM where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' order by SORT_ORDER");
                            for (JSONObject jsonObject : list) {
                                if (jsonObject.getInt("SKU_ID") > 0) { //diskon sku
                                    cekProduk(jsonObject);
                                } else { //diskon group sku
                                    cekGroup(jsonObject);
                                }
                            }
                        } else if (json.getInt("DISCOUNT_PROMO_TYPE") == 3) {
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_PROMO_ITEM where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' order by SORT_ORDER");
                            for (JSONObject jsonObject : list) {
                                if (jsonObject.getInt("SKU_ID") > 0) { //diskon sku
                                    cekProdukType3(json, jsonObject);
                                } else { //diskon group sku
                                    cekGroupType3(json, jsonObject);
                                }
                            }
                        }
                    }
                }
            }
            if (vHadiah != null) {
                ((ExpandableListView) vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkDiscountTotal(List<JSONObject> list) {
        for (JSONObject object : list) {
//            if()
        }
    }

    private void removelistPilih(JSONObject json) {
        try {
            for (int i = 0; i < listPilih.size(); i++) {
                JSONObject data = listPilih.get(i);
                if (data.getInt("DISCOUNT_PROMO_ID") == json.getInt("DISCOUNT_PROMO_ID")) {
                    listPilih.remove(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cekGroupType3(JSONObject json, JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel model = adapterOrder.getGroup(i);
            int jml = adapterOrder.getTotalPerGoupSatuanBesar(model);
            if (jsonObject.getInt("GROUPPRODUCT_ID") == Integer.parseInt(model.GroupId) && jml >= json.getInt("MIN_QTY")) {
                JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                int jmlDiskon = jsonResult.getInt("QTY");
                if (jsonResult.getInt("ISMULTIPLY") == 1) {
                    jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                }

                String SKUNAME = jsonResult.getString("SKU_NAME");
                final DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                diskonPromoModel.QTY = jmlDiskon;
                diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
//				if(diskonPromoAdapter == null) {
//					diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
//				}
                int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                if (hIndex != -1) {
                    SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                } else {
                    diskonPromoAdapter.addHeader(SKUNAME);
                }
                diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);

                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
//                        if (realm.where(DiscountResultModel.class).equalTo("RandomID", diskonPromoModel.RandomID).findFirst() == null) {
                        DiscountResultModel model = realm.createObject(DiscountResultModel.class);
                        model.CustomerId = Integer.parseInt(customerModel.CustId);
                        model.DiscountResultID = diskonPromoModel.DiscountResultID;
                        model.DiscountResultValue = diskonPromoModel.QTY;
                        model.CommonID = commonModel.CommonID;
                        model.RandomID = diskonPromoModel.RandomID;
//                        }
                    }
                });

//				if(vHadiah != null) {
//	    			((ExpandableListView)vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
//	    		}
                break;
            }
        }
    }

    private void cekProdukType3(JSONObject json, JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
//				int jml = (model.QTY_B * (int)model.skuModel.CONVER) + model.QTY_K;
                int jml = model.QTY_B + (int) (model.QTY_K / model.skuModel.CONVER);
                if (jsonObject.getInt("SKU_ID") == Integer.parseInt(model.skuModel.SKUId) && jml >= json.getInt("MIN_QTY")) {
                    JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                    int jmlDiskon = jsonResult.getInt("QTY");
                    if (jsonResult.getInt("ISMULTIPLY") == 1) {
                        jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                    }

                    String SKUNAME = jsonResult.getString("SKU_NAME");
                    final DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                    diskonPromoModel.QTY = jmlDiskon;
                    diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                    diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                    GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
//					if(diskonPromoAdapter == null) {
//						diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
//					}
                    int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                    if (hIndex != -1) {
                        SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                    } else {
                        diskonPromoAdapter.addHeader(SKUNAME);
                    }
                    diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);
//					if(vHadiah != null) {
//		    			((ExpandableListView)vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
//		    		}

                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
//                            if (realm.where(DiscountResultModel.class).equalTo("RandomID", diskonPromoModel.RandomID).findFirst() == null) {
                            DiscountResultModel model = realm.createObject(DiscountResultModel.class);
                            model.CustomerId = Integer.parseInt(customerModel.CustId);
                            model.DiscountResultID = diskonPromoModel.DiscountResultID;
                            model.DiscountResultValue = diskonPromoModel.QTY;
                            model.CommonID = commonModel.CommonID;
                            model.RandomID = diskonPromoModel.RandomID;
//                            }
                        }
                    });
                    break;
                }
            }
        }
    }

    private void cekProduk(JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
//				int jml = (model.QTY_B * (int)model.skuModel.CONVER) + model.QTY_K;
                int jml = model.QTY_B + (int) (model.QTY_K / model.skuModel.CONVER);
                if (jsonObject.getInt("SKU_ID") == Integer.parseInt(model.skuModel.SKUId) &&
                        ((jsonObject.getString("CONDITION").equals(">") && jml > jsonObject.getInt("QTY")) ||
                                (jsonObject.getString("CONDITION").equals(">=") && jml >= jsonObject.getInt("QTY")))
                        ) {
                    JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                    int jmlDiskon = jsonResult.getInt("QTY");
                    if (jsonResult.getInt("ISMULTIPLY") == 1) {
                        jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                    }

                    String SKUNAME = jsonResult.getString("SKU_NAME");
                    final DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                    diskonPromoModel.QTY = jmlDiskon;
                    diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                    diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");

//                    GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
//					if(diskonPromoAdapter == null) {
//						diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
//					}
                    int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);

                    if (hIndex != -1) {
                        SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                    } else {
                        diskonPromoAdapter.addHeader(SKUNAME);
                    }

                    diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);

                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
//                            if (realm.where(DiscountResultModel.class).equalTo("RandomID", diskonPromoModel.RandomID).findFirst() == null) {
                            DiscountResultModel model = realm.createObject(DiscountResultModel.class);
                            model.CustomerId = Integer.parseInt(customerModel.CustId);
                            model.DiscountResultID = diskonPromoModel.DiscountResultID;
                            model.DiscountResultValue = diskonPromoModel.QTY;
                            model.CommonID = commonModel.CommonID;
                            model.RandomID = diskonPromoModel.RandomID;
//                            }
                        }
                    });

//					if(vHadiah != null) {
//		    			((ExpandableListView)vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
//		    		}
                    break;
                }
            }
        }
    }

    private void cekGroup(JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel model = adapterOrder.getGroup(i);
            int jml = adapterOrder.getTotalPerGoupSatuanBesar(model);
            if (jsonObject.getInt("GROUPPRODUCT_ID") == Integer.parseInt(model.GroupId) &&
                    ((jsonObject.getString("CONDITION").equals(">") && jml > jsonObject.getInt("QTY")) ||
                            (jsonObject.getString("CONDITION").equals(">=") && jml >= jsonObject.getInt("QTY")))
                    ) {
                JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                int jmlDiskon = jsonResult.getInt("QTY");
                if (jsonResult.getInt("ISMULTIPLY") == 1) {
                    jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                }

                String SKUNAME = jsonResult.getString("SKU_NAME");

                final DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                diskonPromoModel.QTY = jmlDiskon;
                diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));

//				if(diskonPromoAdapter == null) {
//					diskonPromoAdapter = new DiskonPromoAdapter(context, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
//				}
                Log.d("Group SKU", SKUNAME);
                int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                if (hIndex != -1) {
                    SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                } else {
                    diskonPromoAdapter.addHeader(SKUNAME);
                }
                diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);

                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
//                        if (realm.where(DiscountResultModel.class).equalTo("RandomID", diskonPromoModel.RandomID).findFirst() == null) {

                        DiscountResultModel model = realm.createObject(DiscountResultModel.class);
                        model.CustomerId = Integer.parseInt(customerModel.CustId);
                        model.DiscountResultID = diskonPromoModel.DiscountResultID;
                        model.DiscountResultValue = diskonPromoModel.QTY;
                        model.CommonID = commonModel.CommonID;
                        model.RandomID = diskonPromoModel.RandomID;
//                        }
                    }
                });

                if (vHadiah != null) {
                    ((ExpandableListView) vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
                }
                break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    public DiskonPromoAdapter getDiskonPromoAdapter() {
        return diskonPromoAdapter;
    }

    public DiskonAdapter getDiskonAdapter() {
        return diskonAdapter;
    }
//	public JSONObject getDiskonPromo() {
//		return (diskonAdapter == null ? null : diskonAdapter.getJsonPilih());
//	}
}
