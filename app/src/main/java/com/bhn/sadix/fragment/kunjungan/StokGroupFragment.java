package com.bhn.sadix.fragment.kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.StokGroupAdapter;
import com.bhn.sadix.adapter.ViewPagerAdapter;
import com.bhn.sadix.adapter.ViewPagerAdapter.PageItem;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.StockGroupModel;
import com.bhn.sadix.util.Util;
//import com.google.firebase.crash.FirebaseCrash;
import com.makeramen.segmented.SegmentedRadioGroup;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.Realm;

public class StokGroupFragment extends Fragment {
    private View view = null;
    private Context context;
    private ViewPager mPager;
    private StokGroupAdapter adapter;
    private StokGroupAdapter adapterStok;

    private CustomerModel customerModel;

    private ViewPagerAdapter pagerAdapter;

    private KunjunganListener listener;
    private Realm RealmUI;
    private CommonModel commonModel;

    //	public StokGroupFragment(Context context) {
//		this.context = context;
//		pagerAdapter = new ViewPagerAdapter(this.context);
//	}

    public static StokGroupFragment newInstance() {
        Bundle args = new Bundle();

        StokGroupFragment fragment = new StokGroupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        pagerAdapter = new ViewPagerAdapter(this.context);
    }

    /*public void setAdapter(StokGroupAdapter adapter) {
        this.adapter = adapter;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_stok_group, container, false);

        RealmUI = Realm.getDefaultInstance();

        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_all_product) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_stok) {
                    mPager.setCurrentItem(1);
                }
            }
        });
        view.findViewById(R.id.tab_all_product).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        view.findViewById(R.id.tab_stok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) view.findViewById(R.id.pager);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_all_product);
                    loadAllProduk();
                } else if (position == 1) {
                    ((SegmentedRadioGroup) view.findViewById(R.id.tab)).check(R.id.tab_stok);
                    loadStok();
                }
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = listener.getStokGroupAdapter();
        customerModel = listener.getCustomerModel();

        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokGroup();
            }
        } else {
            pagerAdapter.add(new AllStok());
            pagerAdapter.add(new Stok());
            mPager.setAdapter(pagerAdapter);
        }
    }

    public void loadAdapter() {
        RealmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = RealmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });
        
        adapter = listener.getStokGroupAdapter();

        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadStokGroup();
            }
        } else {
            pagerAdapter.add(new AllStok());
            pagerAdapter.add(new Stok());
            mPager.setAdapter(pagerAdapter);
        }
    }

    class AllStok implements ViewPagerAdapter.PageItem {
        private View view;

        @Override
        public int getLayout() {
            return R.layout.activity_list;
        }

        @Override
        public void onStart() {
            ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if (adapter != null) {
                        addJumlah(adapter.getItem(position));
                    }
                }
            });
            loadAllProduk();
        }

        @Override
        public void onPause() {
        }

        @Override
        public void setView(View view) {
            this.view = view;
        }

        @Override
        public View getView() {
            return view;
        }
    }

    class Stok implements ViewPagerAdapter.PageItem {
        private View view;

        @Override
        public int getLayout() {
            return R.layout.activity_list;
        }

        @Override
        public void onStart() {
            ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if (adapterStok != null) {
                        addJumlah(adapterStok.getItem(position));
                    }
                }
            });
        }

        @Override
        public void onPause() {
        }

        @Override
        public void setView(View view) {
            this.view = view;
        }

        @Override
        public View getView() {
            return view;
        }
    }

    private void loadAllProduk() {
        try {
            PageItem pageItem = pagerAdapter.get(0);
            ((ListView) pageItem.getView().findViewById(R.id.list)).setAdapter(adapter);
        } catch (Exception e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void loadStok() {
        try {
            adapterStok = new StokGroupAdapter(context, new ArrayList<StockGroupModel>());
            for (int i = 0; i < adapter.getCount(); i++) {
                StockGroupModel stockGroupModel = adapter.getItem(i);
                if (stockGroupModel.Quantity2 > 0 || stockGroupModel.Quantity > 0) {
                    adapterStok.add(stockGroupModel);
                }
            }
            PageItem pageItem = pagerAdapter.get(1);
            ((ListView) pageItem.getView().findViewById(R.id.list)).setAdapter(adapterStok);
        } catch (Exception e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
            Util.showDialogError(context, e.getMessage());
        }
    }

    private void addJumlah(final StockGroupModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_stok);
        dialog.setTitle(model.groupModel.GroupName);

        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText("Quantity 2");
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText("Quatity");
        if (model.Quantity2 > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.Quantity2));
        }
        if (model.Quantity > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.Quantity));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                model.Quantity2 = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                model.Quantity = Integer.parseInt(cek.equals("") ? "0" : cek);

                RealmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        com.bhn.sadix.Data.StockGroupModel stock = null;

                        if ((stock = realm.where(com.bhn.sadix.Data.StockGroupModel.class).equalTo("RandomID", model.RandomID).findFirst()) == null) {
                            stock = realm.createObject(com.bhn.sadix.Data.StockGroupModel.class, UUID.randomUUID().toString());
                        }

                        stock.CommonID = commonModel.CommonID;
                        stock.CustomerId = customerModel.CustId;
                        stock.GroupId = model.GroupId;
                        stock.Quantity = model.Quantity;
                        stock.Quantity2 = model.Quantity2;
                    }
                });

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                if (adapterStok != null) {
                    adapterStok.notifyDataSetChanged();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadAdapter();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
