package com.bhn.sadix.fragment.kunjungan;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.HistoryAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.HistoryDetailModel;
import com.bhn.sadix.model.HistoryModel;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HistoryPenjualanFragment extends Fragment implements ConnectionEvent {
    private View view = null;
    private ConnectionServer server;
    private CustomerModel customerModel;
    private HistoryAdapter adapter;
    private Context context;
    private KunjunganListener listener;

    //	public HistoryPenjualanFragment(Context context) {
//		this.context = context;
//		server = new ConnectionServer(this.context);
//		server.addConnectionEvent(this);
//		adapter = new HistoryAdapter(this.context, new ArrayList<HistoryModel>(), new HashMap<HistoryModel, List<HistoryDetailModel>>());
//	}

    public static HistoryPenjualanFragment newInstance() {
        HistoryPenjualanFragment fragment = new HistoryPenjualanFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        server = new ConnectionServer(this.context);
        server.addConnectionEvent(this);
        adapter = new HistoryAdapter(this.context, new ArrayList<HistoryModel>(), new HashMap<HistoryModel, List<HistoryDetailModel>>());
        customerModel = listener.getCustomerModel();
    }

    /*public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }*/

    private void load() {
        if (adapter.getGroupCount() == 0) {
            server.setUrl(Util.getServerUrl(context) + "gethistory/CustomerId/" + customerModel.CustId);
            server.requestJSONObject(null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_history, container, false);

        if (customerModel == null) {
            listener.getCustomerModel();
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (customerModel == null) {
            customerModel = listener.getCustomerModel();
        }

        load();

        if (adapter != null) {
            ((ExpandableListView) view.findViewById(R.id.list)).setAdapter(adapter);
        }
    }

    @Override
    public void error(String error) {
        if (error.startsWith("Koneksi gagal")) {
            Util.showDialogError(context, "Tidak bisa menampilkan data history, karena signal tidak ada");
        } else {
            Util.showDialogError(getActivity(), error);
        }
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONArray json = new JSONArray(respon);
            for (int i = 0; i < json.length(); i++) {
                JSONObject jsonObj = json.getJSONObject(i);
                JSONObject Transaction = jsonObj.getJSONObject("Transaction");
                HistoryModel historyModel = new HistoryModel(
                        Transaction.getString("TransactionId"),
                        Transaction.getString("TransactionTime"),
                        Transaction.getString("CustomerId"),
                        Transaction.getDouble("Total"),
                        Transaction.getDouble("Diskon"),
                        Transaction.getDouble("TypeDiskon"),
                        Transaction.getDouble("GrandTotal"),
                        Transaction.getString("Desc")
                );
                adapter.addHeader(historyModel);
                JSONArray Details = jsonObj.getJSONArray("Details");
                for (int j = 0; j < Details.length(); j++) {
                    JSONObject jsonDtl = Details.getJSONObject(j);
                    adapter.addChild(historyModel, new HistoryDetailModel(
                            jsonDtl.getString("TransactionId"),
                            jsonDtl.getString("TransactionIds"),
                            jsonDtl.getString("GroupName"),
                            jsonDtl.getString("Name"),
                            jsonDtl.getDouble("Price"),
                            jsonDtl.getInt("Qty"),
                            jsonDtl.getDouble("Price2"),
                            jsonDtl.getInt("Qty2"),
                            jsonDtl.getString("Satuan1"),
                            jsonDtl.getString("Satuan2"),
                            jsonDtl.getDouble("Discount1"),
                            jsonDtl.getDouble("Discount2"),
                            jsonDtl.getDouble("Discount3"),
                            jsonDtl.getDouble("Total"),
                            jsonDtl.getDouble("GrandTotal"),
                            jsonDtl.getDouble("T_Discount"),
                            jsonDtl.getString("SalesProgramDescription")
                    ));
                }
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
