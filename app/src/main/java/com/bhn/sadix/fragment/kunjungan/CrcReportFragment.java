package com.bhn.sadix.fragment.kunjungan;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.CrcReportAdapter;
import com.bhn.sadix.database.CtrlCrcOrder;
import com.bhn.sadix.database.CtrlCrcStock;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.CRCReportModel;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import java.util.List;

import io.realm.Realm;

public class CrcReportFragment extends Fragment {
    private View view = null;
    private CrcReportAdapter adapter;
    //    private CommonModel commonModel;
    private com.bhn.sadix.Data.CommonModel commonModel;
    private CustomerModel customerModel;
    private CtrlSKU ctrlSKU;
    private CtrlCrcOrder ctrlCrcOrder;
    private CtrlCrcStock ctrlCrcStock;
    KunjunganListener listener;
    private Realm realm;

    public static CrcReportFragment newInstance() {
        Bundle args = new Bundle();

        CrcReportFragment fragment = new CrcReportFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CrcReportFragment() {
    }

    public void set(CommonModel commonModel, CustomerModel customerModel) {
//        this.commonModel = commonModel;
        this.customerModel = customerModel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlSKU = new CtrlSKU(getActivity());
        ctrlCrcOrder = new CtrlCrcOrder(getActivity());
        ctrlCrcStock = new CtrlCrcStock(getActivity());
    }

    public void setAdapter(CrcReportAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_crc_report, container, false);
        realm = Realm.getDefaultInstance();

        customerModel = listener.getCustomerModel();
        commonModel = realm.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Done", 0).findFirst();

        /*if (adapter != null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadCrcReport();
            }
        } else {
            loadAdapter();
        }*/
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (adapter != null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadCrcReport();
            }
        } else {
            loadAdapter();
        }
    }

    public void loadAdapter() {
        adapter = listener.getCrcReportAdapter();
        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity) {
                ((KunjunganActivity) getActivity()).loadCrcReport();
            }
        } else {
            ((ExpandableListView) view.findViewById(R.id.list)).setAdapter(adapter);

            ((ExpandableListView) view.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    CrcReportAdapter adapter = (CrcReportAdapter) parent.getExpandableListAdapter();
                    populateSKUByGroup(adapter.getGroup(groupPosition));
                    return false;
                }
            });
        }
    }

    private void populateSKUByGroup(GroupSKUModel group) {
        try {
            List<CRCReportModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    CRCReportModel crcReportModel = new CRCReportModel(skuModel);
                    if (list == null || list.indexOf(crcReportModel) == -1) {
                        crcReportModel.ordes = ctrlCrcOrder.list(customerModel.CustId, skuModel.SKUId);
                        crcReportModel.stocks = ctrlCrcStock.list(customerModel.CustId, skuModel.SKUId);
                        adapter.addChild(group, crcReportModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getActivity(), e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }
        loadAdapter();
    }

    @Override
    public void onPause() {
        super.onPause();
        realm.close();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
