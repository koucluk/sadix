package com.bhn.sadix.fragment.kunjungan;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.CrcOrderAdapter;
import com.bhn.sadix.adapter.SalesProgramAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCrcOrder;
import com.bhn.sadix.database.CtrlCrcStock;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSalesProgram;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CrcOrderStokModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class CRCFragment extends Fragment implements CrcOrderAdapter.CrcOrderListener {
    private View view = null;
    private CrcOrderAdapter adapter;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    private CustomerModel customerModel;
    private CustomerTypeModel customerTypeModel;
    private CtrlSKU ctrlSKU;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlSalesProgram ctrlSalesProgram;
    private CtrlCrcOrder ctrlCrcOrder;
    private CtrlCrcStock ctrlCrcStock;
    private CtrlAppModul ctrlAppModul;
    private CtrlConfig ctrlConfig;
    private ConnectionServer server;
    private boolean isCanvas = false;
    private KunjunganListener listener;
    Realm realm;

    //	public CRCFragment(CommonModel commonModel, CustomerModel customerModel,
//			CustomerTypeModel customerTypeModel) {
//		this.commonModel = commonModel;
//		this.customerModel = customerModel;
//		this.customerTypeModel = customerTypeModel;
//	}
    /*public void setCommonModel(CommonModel commonModel) {
        this.commonModel = commonModel;
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    public void setCustomerTypeModel(CustomerTypeModel customerTypeModel) {
        this.customerTypeModel = customerTypeModel;
    }*/

    public static CRCFragment newInstance() {

        Bundle args = new Bundle();

        CRCFragment fragment = new CRCFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        server = new ConnectionServer(getActivity());
        ctrlSKU = new CtrlSKU(getActivity());
        ctrlSalesProgram = new CtrlSalesProgram(getActivity());
        ctrlTakingOrder = new CtrlTakingOrder(getActivity());
        ctrlCrcOrder = new CtrlCrcOrder(getActivity());
        ctrlCrcStock = new CtrlCrcStock(getActivity());
        ctrlAppModul = new CtrlAppModul(getActivity());
        ctrlConfig = new CtrlConfig(getActivity());
    }

    /*public void setAdapter(CrcOrderAdapter adapter) {
        this.adapter = adapter;
        this.adapter.addCrcOrderListener(this);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_crc, container, false);

        realm = Realm.getDefaultInstance();
        commonModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();

        if (adapter != null) {
            ((ExpandableListView) view.findViewById(R.id.list)).setAdapter(adapter);
        }
        ((ExpandableListView) view.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                CrcOrderAdapter adapter = (CrcOrderAdapter) parent.getExpandableListAdapter();
                populateSKUByGroup(adapter.getGroup(groupPosition));
                return false;
            }
        });
        if (customerTypeModel != null) {
            String title = customerTypeModel.DiskonType.equals("2") ? "(Rp)" : "(%)";
            ((TextView) view.findViewById(R.id.lbl_discount)).setText("Discount " + title);
        }
        ((EditText) view.findViewById(R.id.discount)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateAmount();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof KunjunganActivity)
            ((KunjunganActivity) getActivity()).loadCrc();
    }

    public void loadAdapter() {
        adapter = listener.getCrcOrderAdapter();

        if (adapter == null) {
            if (getActivity() instanceof KunjunganActivity)
                ((KunjunganActivity) getActivity()).loadCrc();
        } else {
            adapter.addCrcOrderListener(this);
            customerModel = listener.getCustomerModel();
            customerTypeModel = listener.getCustomerType();
            ((ExpandableListView) view.findViewById(R.id.list)).setAdapter(adapter);
            calculateAmount();
        }
    }

    private void populateSKUByGroup(GroupSKUModel group) {
        try {
            List<CrcOrderStokModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    CrcOrderStokModel crcOrderStokModel = new CrcOrderStokModel(skuModel);
                    crcOrderStokModel.lastOrder = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(skuModel.SKUId));
                    crcOrderStokModel.lastStock = ctrlCrcStock.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(skuModel.SKUId));

//					CrcOrderModel crcOrderModel = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(crcOrderStokModel.skuModel.SKUId));
//					if(crcOrderModel != null) {
//						crcOrderStokModel.QTY_B_REK = crcOrderModel.QTY_L;
//						crcOrderStokModel.QTY_K_REK = crcOrderModel.QTY;
//					}


                    int QTY_B = (crcOrderStokModel.lastOrder.QTY_L + crcOrderStokModel.lastStock.QTY_L) - crcOrderStokModel.stock.QTY_B;
                    QTY_B = (int) (QTY_B * 1.5);
                    int QTY_K = (crcOrderStokModel.lastOrder.QTY + crcOrderStokModel.lastStock.QTY) - crcOrderStokModel.stock.QTY_K;
                    QTY_K = (int) (QTY_K * 1.5);
                    if (QTY_B < 0) QTY_B = 0;
                    if (QTY_K < 0) QTY_K = 0;
                    crcOrderStokModel.QTY_B_REK = QTY_B;
                    crcOrderStokModel.QTY_K_REK = QTY_K;

                    if (list == null || list.indexOf(crcOrderStokModel) == -1) {
                        TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, skuModel.SKUId);
                        if (takingOrderModel != null) {
                            crcOrderStokModel.order.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                            crcOrderStokModel.order.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                            crcOrderStokModel.order.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                            crcOrderStokModel.order.note = takingOrderModel.note;
                            crcOrderStokModel.order.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                            crcOrderStokModel.order.QTY_B = takingOrderModel.QTY_B;
                            crcOrderStokModel.order.QTY_K = takingOrderModel.QTY_K;
                            String SalesProgramId = takingOrderModel.SalesProgramId;
                            if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                crcOrderStokModel.order.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                            }
                            crcOrderStokModel.order.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                            crcOrderStokModel.order.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                            crcOrderStokModel.order.PRICE_B = takingOrderModel.PRICE_B;
                            crcOrderStokModel.order.PRICE_K = takingOrderModel.PRICE_K;
                            crcOrderStokModel.order.RandomID = takingOrderModel.RandomID;
                        }
                        adapter.addChild(group, crcOrderStokModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getActivity(), e.getMessage());
        }
    }

    @Override
    public void order(final CrcOrderStokModel model) {
        if (model.stock.QTY_B == 0 && model.stock.QTY_K == 0 && model.stock.ReasonID.equals("")) {
//			Util.showDialogInfo(getActivity(), "Stok harus diisi terlebih dahulu!");
            Util.okeDialog(getActivity(), "INFO", "Stok harus diisi terlebih dahulu!", new OkListener() {

                @Override
                public void onDialogOk() {
                    addStock(model);
                }
            });
        } else {
            addOrder(model);
        }
    }

    @Override
    public void stock(CrcOrderStokModel model) {
        addStock(model);
    }

    private void addOrder(final CrcOrderStokModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_taking_order);
        ((TextView) dialog.findViewById(R.id.title)).setText(model.order.skuModel.ProductName);

        ComboBoxAdapter adapterReason = new ComboBoxAdapter(getActivity(), new ArrayList<ComboBoxModel>());
        adapterReason.add(new ComboBoxModel("", "---Reason---"));
        List<ComboBoxModel> list = ctrlConfig.listComboBox("REASON_ORDER_CRC");
        for (ComboBoxModel comboBoxModel : list) {
            adapterReason.add(comboBoxModel);
        }
        ((Spinner) dialog.findViewById(R.id.ReasonID)).setAdapter(adapterReason);
        int posisi = adapterReason.indexOf(new ComboBoxModel(model.order.ReasonID));
        if (posisi >= 0) {
            ((Spinner) dialog.findViewById(R.id.ReasonID)).setSelection(posisi);
        }

//		if(ctrlAppModul.isModul("34")) {
//			CrcOrderModel crcOrderModel = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
//			if(crcOrderModel != null) {
//				if(model.order.QTY_B == 0) {
//					model.order.QTY_B = crcOrderModel.QTY_L;
//				}
//				if(model.order.QTY_K == 0) {
//					model.order.QTY_K = crcOrderModel.QTY;
//				}
//			}
//		}

        int visibility = ctrlAppModul.isModul("53") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_1).setVisibility(visibility);
        visibility = ctrlAppModul.isModul("54") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_2).setVisibility(visibility);
        visibility = ctrlAppModul.isModul("55") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_3).setVisibility(visibility);

        AppModul appModul = ctrlAppModul.get("57");
        if (appModul != null) {
            if (appModul.AppModulValue.equals("0")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
            } else if (appModul.AppModulValue.equals("1")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
            } else if (appModul.AppModulValue.equals("2")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
            } else if (appModul.AppModulValue.equals("3")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
            }
        }
        AppModul appModul68 = ctrlAppModul.get("68");
        if (appModul68 != null) {
            if (appModul68.AppModulValue.equals("0")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
            } else if (appModul68.AppModulValue.equals("1")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
            }
        }

        ((EditText) dialog.findViewById(R.id.jml_1)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model.order);
//				int orderQTY_L = (model.lastOrder == null ? 0 : model.lastOrder.QTY_L);
//				int stockQTY_L = (model.lastStock == null ? 0 : model.lastStock.QTY_L);
//				double order = (orderQTY_L + stockQTY_L) - model.stock.QTY_B;
//				order = order*1.5;
//				if(order > model.order.QTY_B) {
//					dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
//					dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
//				}
                cekQtyOrder(dialog, model, false);
            }
        });
        ((EditText) dialog.findViewById(R.id.jml_2)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model.order);
//				int orderQTY = (model.lastOrder == null ? 0 : model.lastOrder.QTY);
//				int stockQTY = (model.lastStock == null ? 0 : model.lastStock.QTY);
//				double order = (orderQTY + stockQTY) - model.stock.QTY_K;
//				order = order*1.5;
//				if(order > model.order.QTY_K) {
//					dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
//					dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
//				}
                cekQtyOrder(dialog, model, false);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_1)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model.order);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_2)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model.order);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_3)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model.order);
            }
        });
        ((RadioGroup) dialog.findViewById(R.id.rb_diskon)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
//				if(((RadioButton)dialog.findViewById(R.id.rb_diskon_b)).isChecked()) {
//					model.DiscountType = "1";
//				} else if(((RadioButton)dialog.findViewById(R.id.rb_diskon_k)).isChecked()) {
//					model.DiscountType = "0";
//				}
                total(dialog, model.order);
            }
        });
        ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(model.order.TOTAL_PRICE));
        ((RadioGroup) dialog.findViewById(R.id.payment_term)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.cash) {
                    dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
                    ((EditText) dialog.findViewById(R.id.lama_credit)).setText(null);
                } else if (checkedId == R.id.credit) {
                    dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
                    ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
                }
            }
        });
        if (customerModel.CustTop > 0) {
            ((EditText) dialog.findViewById(R.id.lama_credit)).setKeyListener(null);
        }
        ((Button) dialog.findViewById(R.id.stok_1)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stok(model.order, dialog);
            }
        });
        ((Button) dialog.findViewById(R.id.stok_2)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stok(model.order, dialog);
            }
        });

        String title = model.skuModel.DISCOUNT_1_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_1)).setText("Discount1 " + title);
        title = model.skuModel.DISCOUNT_2_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_2)).setText("Discount2 " + title);
        title = model.skuModel.DISCOUNT_3_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_3)).setText("Add Discount " + title);

//		dialog.setTitle(model.skuModel.ProductName);
        ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.order.STOK_B));
        ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.order.STOK_K));
        ((Spinner) dialog.findViewById(R.id.SalesProgram)).setAdapter(new SalesProgramAdapter(getActivity(), ctrlSalesProgram.listByGropID(model.skuModel.GroupId)));
        if (model.order.salesProgramModel != null) {
            int index = ((SalesProgramAdapter) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getAdapter()).indexOf(model.order.salesProgramModel);
            ((Spinner) dialog.findViewById(R.id.SalesProgram)).setSelection(index);
        }
        if (model.order.PAYMENT_TERM == null || model.order.PAYMENT_TERM.equals("") || model.order.PAYMENT_TERM.equals("2")) {
            ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
            dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
        } else if (model.order.PAYMENT_TERM.equals("1")) {
            ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
            dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
        }
        ((EditText) dialog.findViewById(R.id.note)).setText(model.order.note);
        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setText(model.skuModel.SBESAR);
        ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setText(model.skuModel.SKECIL);

        ((TextView) dialog.findViewById(R.id.price_1)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE_L));
        ((TextView) dialog.findViewById(R.id.price_2)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE));
        if (model.order.LAMA_CREDIT > 0) {
            ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(model.order.LAMA_CREDIT));
        } else {
            ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(customerModel.CustTop));
        }

        if (model.order.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.order.QTY_B));
        } else {
            ((EditText) dialog.findViewById(R.id.jml_1)).setTextColor(Color.RED);
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B_REK));
        }
        if (model.order.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.order.QTY_K));
        } else {
            ((EditText) dialog.findViewById(R.id.jml_2)).setTextColor(Color.RED);
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K_REK));
        }
        if (model.order.DISCOUNT1 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_1)).setText(String.valueOf(model.order.DISCOUNT1));
        }
        if (model.order.DISCOUNT2 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_2)).setText(String.valueOf(model.order.DISCOUNT2));
        }
        if (model.order.DISCOUNT3 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_3)).setText(String.valueOf(model.order.DISCOUNT3));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ComboBoxModel reason = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.ReasonID)).getSelectedItem();
                if (cekQtyOrder(dialog, model, true) && reason.value.equals("")) {
                    Util.showDialogInfo(getActivity(), "Reason harus dipilih!");
                    ScrollView sv = (ScrollView) dialog.findViewById(R.id.scrl);
                    sv.scrollTo(0, sv.getBottom());
                    return;
                }
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                int QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                int QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);
                if (isCanvas) {
                    if (QTY_B > model.skuModel.QBESAR) {
                        Util.showDialogInfo(getActivity(), "Stok " + model.skuModel.SBESAR + " tidak cukup!");
                        return;
                    }
                    if (QTY_K > model.skuModel.QKECIL) {
                        if (model.skuModel.QBESAR > 0) {
                            Util.confirmDialog(getActivity(), "Info", "Stok " + model.skuModel.SKECIL + " tidak cukup, Apakah ingin ambil dari stok " + model.skuModel.QBESAR + "?", new ConfirmListener() {
                                @Override
                                public void onDialogCompleted(boolean answer) {
                                    if (answer) {
                                        model.skuModel.QBESAR -= 1;
                                        model.skuModel.QKECIL += model.skuModel.CONVER;
                                        ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.skuModel.QBESAR));
                                        ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.skuModel.QKECIL));
                                    } else {
                                        return;
                                    }
                                }
                            });
                        } else {
                            Util.showDialogInfo(getActivity(), "Stok " + model.skuModel.SKECIL + " tidak cukup dan stok " + model.skuModel.SBESAR + " tidak cukup!");
                        }
                        return;
                    }
                }
                model.order.QTY_B = QTY_B;
                model.order.QTY_K = QTY_K;
                model.QTY_B_REK = 0;
                model.QTY_K_REK = 0;
                model.order.salesProgramModel = (SalesProgramModel) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getSelectedItem();
                cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
                model.order.DISCOUNT1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
                model.order.DISCOUNT2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
                model.order.DISCOUNT3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                model.order.note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();
                if (((RadioButton) dialog.findViewById(R.id.cash)).isChecked()) {
                    model.order.PAYMENT_TERM = "1";
                } else if (((RadioButton) dialog.findViewById(R.id.credit)).isChecked()) {
                    model.order.PAYMENT_TERM = "2";
                }
                cek = ((EditText) dialog.findViewById(R.id.lama_credit)).getText().toString();
                model.order.LAMA_CREDIT = Integer.parseInt(cek.equals("") ? "0" : cek);
                model.order.PRICE_B = model.skuModel.HET_PRICE_L;
                model.order.PRICE_K = model.skuModel.HET_PRICE;
                model.order.TOTAL_PRICE = totalValue(dialog, model.order);
                if (reason.value.equals("")) {
                    model.ReasonOrder = "";
                    model.order.ReasonID = "";
                } else {
                    model.ReasonOrder = reason.text;
                    model.order.ReasonID = reason.value;
                }

                //tambahan untuk tipe diskon
                if (((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked()) {
                    model.order.DiscountType = "1";
                } else if (((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).isChecked()) {
                    model.order.DiscountType = "0";
                }

                calculateAmount();

                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean cekQtyOrder(Dialog dialog, CrcOrderStokModel model, boolean isreason) {
        String sstok = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
        int QTY_B = Integer.parseInt(sstok.equals("") ? "0" : sstok);
        sstok = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
        int QTY_K = Integer.parseInt(sstok.equals("") ? "0" : sstok);
        int QTY = (int) (QTY_B * model.skuModel.CONVER) + QTY_K;
        int order = (model.lastOrder.getQty() + model.lastStock.getQty()) - model.stock.getQty();
        order = (int) (order * 1.5);
        if (isreason) {
            if (order > QTY) {
                return true;
            } else {
                return false;
            }
        } else {
            if (order >= QTY) {
                dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
                dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
                return true;
            } else {
                dialog.findViewById(R.id.lblReasonID).setVisibility(View.GONE);
                dialog.findViewById(R.id.ReasonID).setVisibility(View.GONE);
                return false;
            }
        }
    }

    private void total(final Dialog dialog, final OrderDetailModel model) {
        double total = totalValue(dialog, model);
        if (total < 0) {
            Util.showDialogInfo(getActivity(), "Diskon anda melebihi total harga!");
            ((EditText) dialog.findViewById(R.id.discount_1)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_2)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_3)).setText("0");
            ((TextView) dialog.findViewById(R.id.total_price)).setText("0");
        } else {
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(total));
        }
    }

    private double totalValue(final Dialog dialog, final OrderDetailModel model) {
        try {
            boolean isDiskonBesar = ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked();
            String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
            int jml1 = Integer.parseInt(cek.equals("") ? "0" : cek);
            cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
            int jml2 = Integer.parseInt(cek.equals("") ? "0" : cek);

            double diskon = 0;

            //diskon 1
            cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
            double diskon1 = 0;
            if (model.skuModel.DISCOUNT_1_TYPE.equals("2")) {
                diskon1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon1 > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(getActivity(), "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(getActivity(), "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                } else {
                    diskon1 = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) * persen / 100;
                }
            }
            diskon = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) - diskon1;
            //diskon 2
            cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
            double diskon2 = 0;
            if (model.skuModel.DISCOUNT_2_TYPE.equals("2")) {
                diskon2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon2 > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(getActivity(), "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(getActivity(), "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                } else {
                    diskon2 = diskon * persen / 100;
                }
            }
            diskon = diskon - diskon2;
            //hitung total
            double total = 0;
//			if(model.skuModel.DISCOUNT_TYPE.equals("1")) {
            if (isDiskonBesar) {
//				double het_b = model.skuModel.HET_PRICE_L;
//				het_b -= diskon1;
//				het_b -= diskon2;
                total = (jml1 * diskon) + (jml2 * model.skuModel.HET_PRICE);
            } else {
//				double het_k = model.skuModel.HET_PRICE;
//				het_k -= diskon1;
//				het_k -= diskon2;
                double qty_k = jml1 * model.skuModel.CONVER;
                qty_k += jml2;
//				total = het_k*qty_k;
                total = diskon * qty_k;
            }
            //diskon 3
            cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
            double diskon3 = 0;
            if (model.skuModel.DISCOUNT_3_TYPE.equals("2")) {
                diskon3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon3 > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(getActivity(), "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(getActivity(), "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                } else {
                    diskon3 = total * persen / 100;
                }
            }
            total = total - diskon3;
            return total;
        } catch (Exception e) {
            return 0;
        }
    }

    private void stok(final OrderDetailModel model, final Dialog dialog) {
        if (isCanvas) {
            ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.skuModel.QBESAR));
            ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.skuModel.QKECIL));
        } else {
            server.addConnectionEvent(new ConnectionEvent() {
                @Override
                public void messageServer(String respon) {
                    try {
                        JSONObject json = new JSONArray(respon).getJSONObject(0);
                        model.STOK_B = json.getInt("StockQty_L");
                        model.STOK_K = json.getInt("StockQty");
                        ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
                        ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
                    } catch (Exception e) {
                        error(e.getMessage());
                    }
                }

                @Override
                public void error(String error) {
                    Util.showDialogError(getActivity(), error);
                }
            });
            server.setUrl(Util.getServerUrl(getActivity()) + "getSKUstock/SalesId/" + UserModel.getInstance(getActivity()).SalesId + "/SKUId/" + model.skuModel.SKUId);
            server.requestJSONObject(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }
        if (adapter != null)
            loadAdapter();
//        calculateAmount();
    }

    @Override
    public void onPause() {
        super.onPause();
        realm.close();
        calculateAmount();
    }

    private void calculateAmount() {
        int QTY_B = 0;
        int QTY_K = 0;
        double PRICE = 0;
        double DISCOUNT = 0;
        double S_AMOUNT = 0;
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                OrderDetailModel orderDetailModel = adapter.getChild(i, j).order;
                QTY_B += orderDetailModel.QTY_B;
                QTY_K += orderDetailModel.QTY_K;
                PRICE += orderDetailModel.TOTAL_PRICE;
            }
        }
        String cek = ((EditText) view.findViewById(R.id.discount)).getText().toString();
        DISCOUNT = Double.parseDouble(cek.equals("") ? "0" : cek);
        if (customerTypeModel != null) {
            if (DISCOUNT > customerTypeModel.DiskonMax) {
                String max = "";
                if (customerTypeModel.DiskonType.equals("2")) {
                    max = "Rp." + customerTypeModel.DiskonMax;
                } else {
                    max = "" + customerTypeModel.DiskonMax + "%";
                }
                Util.showDialogInfo(getActivity(), "Nilai melebihi batas masimum " + max);
                ((EditText) view.findViewById(R.id.discount)).setText(null);
                return;
            } else {
                if (customerTypeModel.DiskonType.equals("2")) {
                    S_AMOUNT = PRICE - DISCOUNT;
                } else {
                    S_AMOUNT = PRICE - (PRICE * DISCOUNT / 100);
                }
            }
        }
        final int finalQTY_B = QTY_B;
        final int finalQTY_K = QTY_K;
        final double finalPRICE = PRICE;
        final double finalDISCOUNT = DISCOUNT;
        final double finalS_AMOUNT = S_AMOUNT;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel.QTY_B = finalQTY_B;
                commonModel.QTY_K = finalQTY_K;
                commonModel.PRICE = finalPRICE;
                commonModel.DISCOUNT = finalDISCOUNT;
                commonModel.S_AMOUNT = finalS_AMOUNT;
            }
        });

        if (view != null) {
            ((TextView) view.findViewById(R.id.total_qty)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
            ((TextView) view.findViewById(R.id.price)).setText(Util.numberFormat.format(commonModel.PRICE));
            ((TextView) view.findViewById(R.id.total_price)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
        }
    }

    private void addStock(final CrcOrderStokModel model) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_stok);
        dialog.setTitle(model.skuModel.ProductName);

        ComboBoxAdapter adapterCombo = new ComboBoxAdapter(getActivity(), new ArrayList<ComboBoxModel>());
        adapterCombo.add(new ComboBoxModel("", "---Reason---"));
        List<ComboBoxModel> list = ctrlConfig.listComboBox("REASON_STOCK_CRC");
        for (ComboBoxModel comboBoxModel : list) {
            adapterCombo.add(comboBoxModel);
        }
        ((Spinner) dialog.findViewById(R.id.ReasonID)).setAdapter(adapterCombo);
        int index = adapterCombo.indexOf(new ComboBoxModel(model.stock.ReasonID));
        if (index >= 0) {
            ((Spinner) dialog.findViewById(R.id.ReasonID)).setSelection(index);
        }

//		CrcStockModel crcStockModel = ctrlCrcStock.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
//		if(crcStockModel != null) {
//			if(model.stock.QTY_B == 0) {
//				model.stock.QTY_B = crcStockModel.QTY_L;
//			}
//			if(model.stock.QTY_K == 0) {
//				model.stock.QTY_K = crcStockModel.QTY;
//			}
//		}
        if (model.stock.QTY_K > 0) {
            dialog.findViewById(R.id.lblReasonID).setVisibility(View.GONE);
            dialog.findViewById(R.id.ReasonID).setVisibility(View.GONE);
        } else {
            dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
            dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
        }
        if (model.stock.QTY_K > 0) {
            dialog.findViewById(R.id.lblReasonID).setVisibility(View.GONE);
            dialog.findViewById(R.id.ReasonID).setVisibility(View.GONE);
        } else {
            dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
            dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
        }

        ((EditText) dialog.findViewById(R.id.jml_1)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                cekReasonStok(dialog, model);
            }
        });
        ((EditText) dialog.findViewById(R.id.jml_2)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                cekReasonStok(dialog, model);
            }
        });

        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        if (model.stock.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.stock.QTY_B));
        }
        if (model.stock.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.stock.QTY_K));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ComboBoxModel boxModel = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.ReasonID)).getSelectedItem();
                if (cekReasonStok(dialog, model) && boxModel.value.equals("")) {
                    Util.showDialogInfo(getActivity(), "Reason harus dipilih!");
                    return;
                }
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                model.stock.QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                model.stock.QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);
                if (boxModel.value.equals("")) {
                    model.ReasonStock = "";
                    model.stock.ReasonID = "";
                } else {
                    model.ReasonStock = boxModel.text;
                    model.stock.ReasonID = boxModel.value;
                }

                int QTY_B = (model.lastOrder.QTY_L + model.lastStock.QTY_L) - model.stock.QTY_B;
                QTY_B = (int) (QTY_B * 1.5);
                int QTY_K = (model.lastOrder.QTY + model.lastStock.QTY) - model.stock.QTY_K;
                QTY_K = (int) (QTY_K * 1.5);
                if (QTY_B < 0) QTY_B = 0;
                if (QTY_K < 0) QTY_K = 0;
                model.QTY_B_REK = QTY_B;
                model.QTY_K_REK = QTY_K;

                adapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean cekReasonStok(Dialog dialog, CrcOrderStokModel model) {
        try {
            String sstok = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
            int QTY_B = Integer.parseInt(sstok.equals("") ? "0" : sstok);
            sstok = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
            int QTY_K = Integer.parseInt(sstok.equals("") ? "0" : sstok);
            int QTY = (int) (QTY_B * model.skuModel.CONVER) + QTY_K;
            if (QTY > 0 && (model.lastOrder.getQty() + model.lastStock.getQty()) > QTY) {
                dialog.findViewById(R.id.lblReasonID).setVisibility(View.GONE);
                dialog.findViewById(R.id.ReasonID).setVisibility(View.GONE);
            } else {
                dialog.findViewById(R.id.lblReasonID).setVisibility(View.VISIBLE);
                dialog.findViewById(R.id.ReasonID).setVisibility(View.VISIBLE);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }
}
