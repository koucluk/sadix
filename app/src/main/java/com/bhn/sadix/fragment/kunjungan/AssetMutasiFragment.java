package com.bhn.sadix.fragment.kunjungan;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.AssetMutasiAdapter;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.model.AssetMutasiModel;
import com.bhn.sadix.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

//import com.bhn.sadix.model.CommonModel;

public class AssetMutasiFragment extends Fragment implements OnItemClickListener/*, ConnectionEvent*/ {
    private View view = null;
    private Context context;
    private DbAsetHelper dbAsetHelper;
    private CommonModel commonModel;
    private RealmResults<com.bhn.sadix.Data.AssetMutasiModel> realmModels;
    private AssetMutasiAdapter mutasiAdapter;
    //	private ConnectionServer server;
    private int jenis;
    private KunjunganListener listener;
    private DatabaseHelper databaseHelper;
    private CtrlCommon ctrlCommon;
    private Realm realm;

    //	public AssetMutasiFragment(Context context, CommonModel commonModel) {
//		this.context = context;
//		this.commonModel = commonModel;
//		dbAsetHelper = new DbAsetHelper(this.context);
//		adapter = new AssetMutasiAdapter(context, new ArrayList<AssetMutasiModel>());
//		adapter.setNotifyOnChange(true);
////		server = new ConnectionServer(context);
////		server.addConnectionEvent(this);
//	}

    public static AssetMutasiFragment newInstance() {
        Bundle args = new Bundle();

        AssetMutasiFragment fragment = new AssetMutasiFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        dbAsetHelper = new DbAsetHelper(this.context);
        ctrlCommon = new CtrlCommon(getActivity());
        /*mutasiAdapter = new AssetMutasiAdapter(context, new ArrayList<AssetMutasiModel>());
        mutasiAdapter.setNotifyOnChange(true);*/

//        commonModel = listener.getCommonModel();
        databaseHelper = new DatabaseHelper(this.context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_kunjungan_asset_mutasi, container, false);
        view.findViewById(R.id.btn_penarikan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jenis = 2;
                add();
            }
        });
        view.findViewById(R.id.btn_pemasangan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jenis = 1;
                add();
            }
        });

        realm = Realm.getDefaultInstance();
//        ((Button)view.findViewById(R.id.btn_send)).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				send();
//			}
//		});

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        commonModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
//        mutasiAdapter = listener.getMutasiAdapter();

        loadAdapter();
    }

    private void loadAdapter() {
//        List<JSONObject> mutasiObject = databaseHelper.list("Newasset", commonModel.CommonID);
//        RealmResults<com.bhn.sadix.Data.AssetMutasiModel> mutasiObject;

        realmModels = realm
                .where(com.bhn.sadix.Data.AssetMutasiModel.class)
                .equalTo("CommonID", commonModel.CommonID)
                .findAll();

        /*realmModels.addChangeListener(new RealmChangeListener<RealmResults<com.bhn.sadix.Data.AssetMutasiModel>>() {
            @Override
            public void onChange(RealmResults<com.bhn.sadix.Data.AssetMutasiModel> element) {
                if (realmModels.isLoaded() && realmModels.isValid() && realmModels.size() > 0) {
                    updateAdapter();
                }
            }
        });*/

        /*RealmResults<com.bhn.sadix.Data.AssetMutasiModel> mutasiObject = realm
                .where(com.bhn.sadix.Data.AssetMutasiModel.class)
                .equalTo("CommonID", commonModel.CommonID)
                .findAll();*/

        mutasiAdapter = new AssetMutasiAdapter(this.context, new ArrayList<AssetMutasiModel>());
        mutasiAdapter.setNotifyOnChange(true);

        /*if (mutasiObject.size() == 0) {
            String sql = "select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + commonModel.CustomerId + " and ASETNO='" + object.getString("AsetID") + "'";
            dbAsetHelper.list()
            Integer.parseInt(CUSTID),
                    json.getString("AsetID"),
                    json.getInt("TypeID"),
                    json.getInt("MerkID"),
                    json.getString("AsetName"),
                    json.getString("NoSeri")
        }*/


        Log.wtf("Mutasi Size", realmModels.size() + "");

        for (com.bhn.sadix.Data.AssetMutasiModel object : realmModels) {
            String ASSETNAME = "test";
            ASSETNAME = object.AsetName;

            Log.e("ASET NAME", object.AsetName + " Name");

            String ASETID = object.AsetID;
//            Log.e("ASET NAME", object.AsetID);

//            if (!ASETID.equals(object.AsetID)) {
//                ASETID =
            int jenis = 1;
            AssetMutasiModel model = new AssetMutasiModel(getActivity(), ASSETNAME,
                    ASETID, ASETID,
                    jenis, commonModel.CustomerId,
                    new AssetMutasiModel.AssetMutasiEvent() {
                        @Override
                        public void add(AssetMutasiModel model) {
                            if (!model.isEdit) {
                                mutasiAdapter.add(model);
                            }
                        }

                        @Override
                        public void cancel(AssetMutasiModel model) {

                        }
                    }, object.NoSeri, object.Remark, commonModel.CommonID);

            mutasiAdapter.add(model);
//            }
        }

        String sql = "select * from Aset where CommonID='" + commonModel.CommonID + "' and tipe=3";

        RealmResults<AssetModel> mutasiPullObject = realm.where(AssetModel.class)
                .equalTo("CommonID", commonModel.CommonID)
                .equalTo("tipe", 3)
                .findAll();

//        List<JSONObject> mutasiPullObject = databaseHelper.list(sql);
        for (AssetModel object : mutasiPullObject) {
            JSONObject json = null;
            try {
                json = dbAsetHelper.get("select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + commonModel.CustomerId + " and ASETNO='" + object.AsetID + "'");

                if (json != null) {
                    AssetMutasiModel assetModel = new AssetMutasiModel(getActivity(),
                            json.getString("ASETNAME"),
                            json.getString("ASETSID"),
                            json.getString("ASETNO"),
                            2,
                            commonModel.CustomerId,
                            new AssetMutasiModel.AssetMutasiEvent() {
                                @Override
                                public void cancel(AssetMutasiModel model) {
                                }

                                @Override
                                public void add(AssetMutasiModel model) {
                                    if (!model.isEdit) {
                                        mutasiAdapter.add(model);
                                    }
                                }
                            }, "", "", commonModel.CommonID
                    );

                    mutasiAdapter.add(assetModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ((ListView) view.findViewById(R.id.list)).setAdapter(mutasiAdapter);

        ((ListView) view.findViewById(R.id.list)).setOnItemClickListener(this);

    }

    private void updateAdapter() {

    }

    //	private void send() {
//	}
    private void add() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_asset);
        dialog.setTitle("ADD ASSET");
        dialog.findViewById(R.id.btn_scanbarcode).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                ((KunjunganActivity) context).isHome = false;
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                try {
                    startActivityForResult(intent, 4);
                } catch (Exception e) {
                    dialog.dismiss();
                    Util.showDialogInfo(getActivity(), "ZXing Scanner Not Found, Please Install First!");
                }
            }
        });
        dialog.findViewById(R.id.btn_input).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                inputAssetNumber();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 4) {
            if (resultCode == Activity.RESULT_OK) {
                String AssetNumber = data.getStringExtra("SCAN_RESULT");
                searchAsset(AssetNumber);
            }
        }
        ((KunjunganActivity) context).isHome = true;
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void inputAssetNumber() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_input_asset);
        dialog.setTitle("INPUT ASSET NUMBER");
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String AssetNumber = ((EditText) dialog.findViewById(R.id.AssetNumber)).getText().toString();
                dialog.dismiss();
                searchAsset(AssetNumber);
            }
        });
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void searchAsset(String assetNumber) {
        try {
            int index = mutasiAdapter.indexOf(new AssetMutasiModel(assetNumber, jenis));
            if (index == -1) {
                if (jenis == 1) { //pemasangan
                    AssetMutasiModel assetModel = new AssetMutasiModel(getActivity(),
                            "",
                            assetNumber,
                            assetNumber,
                            jenis,
                            commonModel.CustomerId,
                            new AssetMutasiModel.AssetMutasiEvent() {
                                @Override
                                public void cancel(AssetMutasiModel model) {
                                }

                                @Override
                                public void add(AssetMutasiModel model) {
                                    if (!model.isEdit) {
                                        mutasiAdapter.add(model);
                                    }
                                }
                            }, "", "", commonModel.CommonID
                    );
                    assetModel.isEdit = false;
//					adapter.add(assetModel);
                    ((KunjunganActivity) context).isHome = true;
                    assetModel.show();
                } else if (jenis == 2) {//penarikan
                    JSONObject json = dbAsetHelper.get("select ASETNAME,ASETTYPE,ASETNO,ASETSID from ASSET_DATA where CUSTID=" + commonModel.CustomerId + " and ASETNO='" + assetNumber + "'");
                    if (json != null) {
                        AssetMutasiModel assetModel = new AssetMutasiModel(getActivity(),
                                json.getString("ASETNAME"),
                                json.getString("ASETSID"),
                                json.getString("ASETNO"),
                                jenis,
                                commonModel.CustomerId,
                                new AssetMutasiModel.AssetMutasiEvent() {
                                    @Override
                                    public void cancel(AssetMutasiModel model) {
                                    }

                                    @Override
                                    public void add(AssetMutasiModel model) {
                                        if (!model.isEdit) {
                                            mutasiAdapter.add(model);
                                        }
                                    }
                                }, "", "", commonModel.CommonID
                        );
//						adapter.add(assetModel);
//                        ((KunjunganActivity) context).isHome = false;
                        assetModel.show();
                    } else {
                        Util.showDialogInfo(context, "Asset Tidak Ditemukan!");
                    }
                }
            } else {
//                ((KunjunganActivity) context).isHome = false;
                mutasiAdapter.getItem(index).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AssetMutasiAdapter getAdapter() {
        return mutasiAdapter;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        AssetMutasiModel model = mutasiAdapter.getItem(position);
        model.isEdit = true;
        model.show();
    }
//	@Override
//	public void error(String error) {
//		Util.showDialogError(context, error);
//	}
//	@Override
//	public void messageServer(String respon) {
//		try {
//			JSONObject json = new JSONObject(respon);
//			if(json.getString("Result").equals("INSERT_OK")) {
//				Util.okeDialog(context, "Info", json.getString("Message"), new OkListener() {
//					@Override
//					public void onDialogOk() {
//						adapter.clear();
//					}
//				});
//			} else {
//				Util.showDialogInfo(context, json.getString("Message"));
//			}
//		} catch (Exception e) {
//			error(e.getMessage());
//		}
//	}


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (KunjunganListener) context;
    }

    @Override
    public void onPause() {
        super.onPause();

        realm.close();
        /*ctrlCommon.deleteAssetMutasiData(commonModel.CommonID);
        ctrlCommon.deleteAssetMutasiData(commonModel.CommonID, "3");

        for (int i = 0; i < mutasiAdapter.getCount(); i++) {
            try {
                AssetMutasiModel assetMutasiModel = mutasiAdapter.getItem(i);
                if (assetMutasiModel.getAssetMutasiWidget().getJenis() == 1) {//pemasangan
                    JSONObject json = assetMutasiModel.getAssetMutasiWidget().toJSON();
                    json.put("CommonID", commonModel.CommonID);

                    databaseHelper.save(json, "Newasset");
                } else if (assetMutasiModel.getAssetMutasiWidget().getJenis() == 2) {//penarikan
                    JSONObject json = assetMutasiModel.getAssetMutasiWidget().toJSON();
                    json.put("CommonID", commonModel.CommonID);
                    json.put("jenisWidget", "6");
                    json.put("tipe", "3");
                    databaseHelper.save(json, "Aset");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        if (realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }
        loadAdapter();
    }
}
