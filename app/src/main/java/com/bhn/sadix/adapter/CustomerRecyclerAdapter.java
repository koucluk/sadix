package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetMutasiModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by caksono on 13/04/17.
 */

public class CustomerRecyclerAdapter extends RecyclerView.Adapter<CustomerRecyclerAdapter.CustomerHolder> {

    private List<CustomerModel> mList;
    private Context context;
    private SparseArray<CustomerAdapter.ViewHolder> holders;
    RealmResults<CommonModel> All;
    private ClickItem clickItem;

    public CustomerRecyclerAdapter(Context context, List<CustomerModel> mList) {
        this.mList = mList;
        this.context = context;
        clickItem = (ClickItem) context;
    }

    @Override
    public CustomerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_customer, parent, false);
        return new CustomerHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomerHolder holder, int position) {
        CustomerModel menu = mList.get(position);

        holder.kd_pelanggan.setText(menu.CustomerAddress);
        holder.nm_pelanggan.setText(menu.CustomerName + " - " + menu.CustomerID);

        if (menu.isNew != null && menu.isNew.equals("1")) {
            holder.kd_pelanggan.setTextColor(Color.BLUE);
            holder.nm_pelanggan.setTextColor(Color.BLUE);
        } else {
            holder.kd_pelanggan.setTextColor(Color.BLACK);
            holder.nm_pelanggan.setTextColor(Color.BLACK);
        }

        holder.image.setImageResource(R.drawable.not_yet_visited);

        holder.holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickItem.clickedItem(holder.getAdapterPosition(), mList.get(holder.getAdapterPosition()));
            }
        });

        Realm realmO = Realm.getDefaultInstance();
//        final CommonModel[] commonModel = {null};
//        CommonModel commonModel = null;

        final RealmResults<CommonModel> all = realmO.where(CommonModel.class)
                .equalTo("StatusID", 0)
                .like("CustomerId", menu.CustId)
                .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                .equalTo("Done", 1).findAllAsync();

        RealmChangeListener listener = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                if (all != null && all.isValid() && all.isLoaded()) {
                    if (all.size() > 0) {
//                        holders.put(holder.getAdapterPosition());
                        updateView(all.last(), holder.getAdapterPosition(), holder);
                    }
                }
            }
        };

        all.addChangeListener(listener);

        Log.d("All Size", all.size() + "");

        realmO.close();
    }

    private void updateView(CommonModel commonModel, int adapterPosition, CustomerHolder holder) {
        CustomerModel menu = mList.get(adapterPosition);

        if (commonModel != null && commonModel.EndVisitTime != null) {
            boolean sesuai_jadwal = false;
            if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D1") && menu.D1.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D2") && menu.D2.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D3") && menu.D3.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D4") && menu.D4.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D5") && menu.D5.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D6") && menu.D6.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D7") && menu.D7.equals("true")) {
                sesuai_jadwal = true;
            }

            String CallST = Util.getStringPreference(context, "CallST");
            android.util.Log.e("Cust Adapter | CallST", "" + CallST);
            int order = 0;

            if (CallST.equals("18")) {
                order = commonModel.collectionModel.size();
            } else if (CallST.equals("21") || CallST.equals("22")) {
                order = commonModel.takingOrderModel.size();
//                order[0] = realmO.where(TakingOrderModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID).size();
            } else if (CallST.equals("43")) {
                order = commonModel.assetModel.where().equalTo("tipe", 5).findAll().size();
//                order[0] = realmO.where(AssetModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = db.list("Aset", commonModel.CommonID).size();
            } else if (CallST.equals("33") || CallST.equals("38")) {
                order = commonModel.assetModel.size();
//                order[0] = realmO.where(AssetModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = db.list("Aset", commonModel.CommonID).size();
            } else if (CallST.equals("37")) {
                order = commonModel.assetMutasiModel.size();
                Log.wtf("order 37", "check mutasi : " + order);
                if (order == 0) {
                    order = commonModel.assetModel.where().equalTo("tipe", 4).findAll().size();

                    if (order == 0) {
                        order = commonModel.assetModel.where().equalTo("tipe", 3).findAll().size();
                    }
                    Log.wtf("order 37", "check asset service : " + order);
                }
            }
                /*Realm realm1 = Realm.getDefaultInstance();
                order = realm1.where(AssetMutasiModel.class).equalTo("CommonID", commonModel.CommonID).findAll().size();

                realm1.close();*/
            Log.d("cust value", menu.CustId + "-" + commonModel.CommonID + "-" + commonModel.D + "-" + sesuai_jadwal + "-" + order);

            if (sesuai_jadwal && order > 0) {
                if (menu.isNOO.equals("1")) {
                    holder.image.setImageResource(R.drawable.extra_call);
                } else {
                    holder.image.setImageResource(R.drawable.effective_call);
                }
            } else if (!sesuai_jadwal && order > 0) {
                holder.image.setImageResource(R.drawable.extra_call);
            } else if (sesuai_jadwal && order == 0) {
                holder.image.setImageResource(R.drawable.call);
            } else if (!sesuai_jadwal && order == 0) {
                holder.image.setImageResource(R.drawable.invalid_visited);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class CustomerHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView kd_pelanggan;
        TextView nm_pelanggan;
        LinearLayout holder;

        public CustomerHolder(View itemView) {
            super(itemView);

            holder = (LinearLayout) itemView.findViewById(R.id.holder);
            kd_pelanggan = (TextView) itemView.findViewById(R.id.kd_pelanggan);
            nm_pelanggan = (TextView) itemView.findViewById(R.id.nm_pelanggan);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public interface ClickItem {
        void clickedItem(int position, CustomerModel model);
    }
}
