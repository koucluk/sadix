package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.ResumeOrderModel;
import com.bhn.sadix.model.ResumeOrderDetailModul;
import com.bhn.sadix.util.Util;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class ResumeOrderAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<ResumeOrderModel> _listDataHeader; 
    private HashMap<ResumeOrderModel, List<ResumeOrderDetailModul>> _listDataChild;
    private ComboBoxModel groupBy;
 
    public ResumeOrderAdapter(Context context, List<ResumeOrderModel> listDataHeader,
            HashMap<ResumeOrderModel, List<ResumeOrderDetailModul>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public ComboBoxModel getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(ComboBoxModel groupBy) {
		this.groupBy = groupBy;
	}

	public void addHeader(ResumeOrderModel brandModel) {
    	_listDataHeader.add(brandModel);
//    	notifyDataSetChanged();
    }
    
    public void addChild(ResumeOrderModel brandModel, ResumeOrderDetailModul productModel) {
    	List<ResumeOrderDetailModul> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<ResumeOrderDetailModul>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
//    	notifyDataSetChanged();
    }
    
    public void addChild(ResumeOrderModel brandModel, List<ResumeOrderDetailModul> list) {
		_listDataChild.put(brandModel, list);
//    	notifyDataSetChanged();
    }
 
    @Override
    public ResumeOrderDetailModul getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ResumeOrderDetailModul childText = (ResumeOrderDetailModul) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_resum_order, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.Name);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.QTY);
        txtListChild.setText(childText.CustomerName);
        if(groupBy != null && groupBy.value.equals("2")) {
        	txtListChildJml.setText(Util.numberFormat.format(childText.TOTAL_PRICE));
        } else {
        	txtListChildJml.setText(String.valueOf(childText.QTY_B)+","+String.valueOf(childText.QTY_K));
        }
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<ResumeOrderDetailModul> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public ResumeOrderModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ResumeOrderModel headerTitle = (ResumeOrderModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_resum_order, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.Name);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.QTY);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.ProductName);
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        if(groupBy != null && groupBy.value.equals("2")) {
        	lblListHeaderJml.setText(Util.numberFormat.format(headerTitle.TOTAL_PRICE));
        } else {
            lblListHeaderJml.setText(String.valueOf(headerTitle.QTY_B)+","+String.valueOf(headerTitle.QTY_K));
        }
        return convertView;
    }

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(ResumeOrderModel groupModel) {
    	_listDataHeader.remove(groupModel);
//    	notifyDataSetChanged();
	}
}