package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.HistoryModel;
import com.bhn.sadix.model.HistoryDetailModel;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class HistoryAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<HistoryModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<HistoryModel, List<HistoryDetailModel>> _listDataChild;
 
    public HistoryAdapter(Context context, List<HistoryModel> listDataHeader,
            HashMap<HistoryModel, List<HistoryDetailModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(HistoryModel historyModel) {
    	_listDataHeader.add(historyModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(HistoryModel historyModel, HistoryDetailModel historyDetailModel) {
    	List<HistoryDetailModel> list = _listDataChild.get(historyModel);
    	if(list == null) {
    		list = new ArrayList<HistoryDetailModel>();
    		_listDataChild.put(historyModel, list);
    	}
    	list.add(historyDetailModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(HistoryModel historyModel, List<HistoryDetailModel> list) {
		_listDataChild.put(historyModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public HistoryDetailModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final HistoryDetailModel childText = (HistoryDetailModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_history_detail, null);
        }
        TextView Name = (TextView) convertView.findViewById(R.id.Name);
        TextView GrandTotal = (TextView) convertView.findViewById(R.id.GrandTotal);
        TextView Satuan1 = (TextView) convertView.findViewById(R.id.Satuan1);
        TextView Satuan2 = (TextView) convertView.findViewById(R.id.Satuan2);
        TextView Price = (TextView) convertView.findViewById(R.id.Price);
        TextView Price2 = (TextView) convertView.findViewById(R.id.Price2);
        TextView Qty = (TextView) convertView.findViewById(R.id.Qty);
        TextView Qty2 = (TextView) convertView.findViewById(R.id.Qty2);
        TextView Discount1 = (TextView) convertView.findViewById(R.id.Discount1);
        TextView Discount2 = (TextView) convertView.findViewById(R.id.Discount2);
        TextView Discount3 = (TextView) convertView.findViewById(R.id.Discount3);
        TextView Total = (TextView) convertView.findViewById(R.id.Total);
//        TextView T_Discount = (TextView) convertView.findViewById(R.id.T_Discount);
        Name.setText(childText.Name);
        GrandTotal.setText(childText.getGrandTotalFormat());
        Satuan1.setText(childText.Satuan1);
        Satuan2.setText(childText.Satuan2);
        Price.setText(childText.getPriceFormat());
        Price2.setText(childText.getPrice2Format());
        Qty.setText(childText.getQtyFormat());
        Qty2.setText(childText.getQty2Format());
        Discount1.setText(childText.getDiscount1Format());
        Discount2.setText(childText.getDiscount2Format());
        Discount3.setText(childText.getDiscount3Format());
        Total.setText(childText.getTotalFormat());
//        T_Discount.setText(childText.getT_DiscountFormat());
        final View Detail = convertView.findViewById(R.id.Detail);
        convertView.findViewById(R.id.Header).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Detail.getVisibility() == View.GONE) {
					Detail.setVisibility(View.VISIBLE);
				} else {
					Detail.setVisibility(View.GONE);
				}
			}
		}) ;
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<HistoryDetailModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public HistoryModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final HistoryModel headerTitle = (HistoryModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_history_header, null);
        }
        TextView TransactionTime = (TextView) convertView.findViewById(R.id.TransactionTime);
        TextView GrandTotal = (TextView) convertView.findViewById(R.id.GrandTotal);
        TextView Total = (TextView) convertView.findViewById(R.id.Total);
        TextView Diskon = (TextView) convertView.findViewById(R.id.Diskon);
        TransactionTime.setTypeface(null, Typeface.BOLD);
        GrandTotal.setTypeface(null, Typeface.BOLD);
        TransactionTime.setText(headerTitle.getTransactionTimeFormat());
        GrandTotal.setText(headerTitle.getGrandTotalFormat());
        Total.setText(headerTitle.getTotalFormat());
        Diskon.setText(headerTitle.getDiskonFormat());
        return convertView;
    }

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(HistoryModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
}