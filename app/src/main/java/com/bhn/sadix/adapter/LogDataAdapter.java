package com.bhn.sadix.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.model.CollectorModel;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.LogDataDetailModel;
import com.bhn.sadix.model.ReturModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.StockCompetitorModel;
import com.bhn.sadix.model.StockOutletModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class LogDataAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<CommonModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<CommonModel, List<LogDataDetailModel>> _listDataChild;
    private CtrlCustomer ctrlCustomer;
    private CtrlSKU ctrlSKU;
    private CtrlSKUCom ctrlSKUCom;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;

    public LogDataAdapter(Context context, List<CommonModel> listDataHeader,
                          HashMap<CommonModel, List<LogDataDetailModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        ctrlCustomer = new CtrlCustomer(_context);
        ctrlSKU = new CtrlSKU(_context);
        ctrlConfig = new CtrlConfig(_context);
        ctrlSKUCom = new CtrlSKUCom(_context);
        ctrlAppModul = new CtrlAppModul(_context);
    }

    public void addHeader(CommonModel historyModel) {
        _listDataHeader.add(historyModel);
        notifyDataSetChanged();
    }

    public void addChild(CommonModel historyModel, LogDataDetailModel historyDetailModel) {
        List<LogDataDetailModel> list = _listDataChild.get(historyModel);
        if (list == null) {
            list = new ArrayList<LogDataDetailModel>();
            _listDataChild.put(historyModel, list);
        }
        list.add(historyDetailModel);
        notifyDataSetChanged();
    }

    public void addChild(CommonModel historyModel, List<LogDataDetailModel> list) {
        _listDataChild.put(historyModel, list);
        notifyDataSetChanged();
    }

    @Override
    public LogDataDetailModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LogDataDetailModel childText = (LogDataDetailModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = infalInflater.inflate(R.layout.item_log_data_detail, null);
        }
        TextView orders = (TextView) convertView.findViewById(R.id.orders);
        orders.setVisibility(ctrlAppModul.isModul("21") || ctrlAppModul.isModul("22") ? View.VISIBLE : View.GONE);
        final View detail_order = convertView.findViewById(R.id.detail_order);
        ((LinearLayout) detail_order).removeAllViews();
        for (TakingOrderModel takingOrderModel : childText.takingOrderModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(takingOrderModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(takingOrderModel.QTY_B) + "," + String.valueOf(takingOrderModel.QTY_K));
            ((LinearLayout) detail_order).addView(layout);
        }
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_order.getVisibility() == View.GONE) {
                    detail_order.setVisibility(View.VISIBLE);
                } else {
                    detail_order.setVisibility(View.GONE);
                }
            }
        });
        TextView return_sku = (TextView) convertView.findViewById(R.id.return_sku);
        return_sku.setVisibility(ctrlAppModul.isModul("24") ? View.VISIBLE : View.GONE);
        final View detail_return = convertView.findViewById(R.id.detail_return);
        ((LinearLayout) detail_return).removeAllViews();
        for (ReturModel returModel : childText.returModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(returModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(returModel.QTY_B) + "," + String.valueOf(returModel.QTY_K));
            ((LinearLayout) detail_return).addView(layout);
        }
        return_sku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_return.getVisibility() == View.GONE) {
                    detail_return.setVisibility(View.VISIBLE);
                } else {
                    detail_return.setVisibility(View.GONE);
                }
            }
        });
        TextView stok = (TextView) convertView.findViewById(R.id.stok);
        stok.setVisibility(ctrlAppModul.isModul("19") ? View.VISIBLE : View.GONE);
        final View detail_stok = convertView.findViewById(R.id.detail_stok);
        ((LinearLayout) detail_stok).removeAllViews();
        for (StockOutletModel stockOutletModel : childText.stockOutletModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(stockOutletModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(stockOutletModel.QTY_B) + "," + String.valueOf(stockOutletModel.QTY_K));
            ((LinearLayout) detail_stok).addView(layout);
        }
        stok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_stok.getVisibility() == View.GONE) {
                    detail_stok.setVisibility(View.VISIBLE);
                } else {
                    detail_stok.setVisibility(View.GONE);
                }
            }
        });
        TextView stok_kompetitor = (TextView) convertView.findViewById(R.id.stok_kompetitor);
        stok_kompetitor.setVisibility(ctrlAppModul.isModul("20") ? View.VISIBLE : View.GONE);
        final View detail_stok_kompetitor = convertView.findViewById(R.id.detail_stok_kompetitor);
        ((LinearLayout) detail_stok_kompetitor).removeAllViews();
        for (StockCompetitorModel stockCompetitorModel : childText.stockCompetitorModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUComModel model = ctrlSKUCom.get(stockCompetitorModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(stockCompetitorModel.QTY_B) + "," + String.valueOf(stockCompetitorModel.QTY_K));
            ((LinearLayout) detail_stok_kompetitor).addView(layout);
        }
        stok_kompetitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_stok_kompetitor.getVisibility() == View.GONE) {
                    detail_stok_kompetitor.setVisibility(View.VISIBLE);
                } else {
                    detail_stok_kompetitor.setVisibility(View.GONE);
                }
            }
        });
        TextView collection = (TextView) convertView.findViewById(R.id.collection);
        collection.setVisibility(ctrlAppModul.isModul("18") ? View.VISIBLE : View.GONE);
        final View detail_collection = convertView.findViewById(R.id.detail_collection);
        collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (childText.collectorModel != null) {
                    if (detail_collection.getVisibility() == View.GONE) {
                        detail_collection.setVisibility(View.VISIBLE);
                    } else {
                        detail_collection.setVisibility(View.GONE);
                    }
                }
            }
        });
        if (childText.collectorModel != null) {
//        	if(childText.collectorModel.status.equals("1")) { //pay
//        		((TextView)detail_collection.findViewById(R.id.lbl1)).setText("Today Pay :");
//        		((TextView)detail_collection.findViewById(R.id.data1)).setText(childText.collectorModel.data1);
//        		((TextView)detail_collection.findViewById(R.id.lbl2)).setText("Payment Method :");
//        		ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("PAYMENT_METHOD", childText.collectorModel.data2);
//        		((TextView)detail_collection.findViewById(R.id.data2)).setText(combo.text);
//        		((TextView)detail_collection.findViewById(R.id.lbl3)).setText("Note :");
//        		((TextView)detail_collection.findViewById(R.id.data3)).setText(childText.collectorModel.data3);
//        	} else if(childText.collectorModel.status.equals("2")) { //not pay
//        		((TextView)detail_collection.findViewById(R.id.lbl1)).setText("Not Payment Reason :");
//        		ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("NO_PAYMENT_REASON", childText.collectorModel.data1);
//        		((TextView)detail_collection.findViewById(R.id.data1)).setText(combo.text);
//        		((TextView)detail_collection.findViewById(R.id.lbl2)).setText("Promise Date :");
//        		((TextView)detail_collection.findViewById(R.id.data2)).setText(childText.collectorModel.data2);
//        		((TextView)detail_collection.findViewById(R.id.lbl3)).setText("Note :");
//        		((TextView)detail_collection.findViewById(R.id.data3)).setText(childText.collectorModel.data3);
//        	}
            ((LinearLayout) detail_collection).removeAllViews();
            for (CollectorModel model : childText.collectorModel) {
                LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log__data_collection, null);
                if (model.status.equals("1")) { //pay
                    ((TextView) layout.findViewById(R.id.lbl1)).setText("Today Pay :");
                    ((TextView) layout.findViewById(R.id.data1)).setText(model.data1);
                    ((TextView) layout.findViewById(R.id.lbl2)).setText("Payment Method :");
                    ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("PAYMENT_METHOD", model.data2);
                    ((TextView) layout.findViewById(R.id.data2)).setText(combo.text);
                    ((TextView) layout.findViewById(R.id.lbl3)).setText("Note :");
                    ((TextView) layout.findViewById(R.id.data3)).setText(model.data3);
                } else if (model.status.equals("2")) { //not pay
                    ((TextView) layout.findViewById(R.id.lbl1)).setText("Not Payment Reason :");
                    ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("NO_PAYMENT_REASON", model.data1);
                    ((TextView) layout.findViewById(R.id.data1)).setText(combo.text);
                    ((TextView) layout.findViewById(R.id.lbl2)).setText("Promise Date :");
                    ((TextView) layout.findViewById(R.id.data2)).setText(model.data2);
                    ((TextView) layout.findViewById(R.id.lbl3)).setText("Note :");
                    ((TextView) layout.findViewById(R.id.data3)).setText(model.data3);
                }
                ((LinearLayout) detail_collection).addView(layout);
            }
        }
        TextView price_monitoring = (TextView) convertView.findViewById(R.id.price_monitoring);
        price_monitoring.setVisibility(ctrlAppModul.isModul("42") ? View.VISIBLE : View.GONE);
        final View detail_price_monitoring = convertView.findViewById(R.id.detail_price_monitoring);
        price_monitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_price_monitoring.getVisibility() == View.GONE) {
                    detail_price_monitoring.setVisibility(View.VISIBLE);
                } else {
                    detail_price_monitoring.setVisibility(View.GONE);
                }
            }
        });
        ((LinearLayout) detail_price_monitoring).removeAllViews();
        for (JSONObject model : childText.listPriceMonitoring) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_price_monitoring, null);
            try {
                android.util.Log.e("model", model.toString());
                if (model.getInt("ProductID") == 1) {
                    layout.findViewById(R.id.LevelID).setVisibility(View.VISIBLE);
                    ((TextView) layout.findViewById(R.id.SKUId)).setText("Level : " + String.valueOf(model.getInt("LevelID")));
                    SKUModel sku = ctrlSKU.get(model.getString("SKUId"));
                    ((TextView) layout.findViewById(R.id.SKUId)).setText(sku.ProductName);
                } else {
                    layout.findViewById(R.id.LevelID).setVisibility(View.GONE);
                    SKUComModel sku = ctrlSKUCom.get(model.getString("SKUId"));
                    ((TextView) layout.findViewById(R.id.SKUId)).setText(sku.ProductName);
                }
                ((TextView) layout.findViewById(R.id.BuyPrice)).setText("Buy Price : " + Util.numberFormat.format(model.getInt("BuyPrice")));
                ((TextView) layout.findViewById(R.id.SellPrice)).setText("Sell Price : " + Util.numberFormat.format(model.getInt("SellPrice")));
                ((TextView) layout.findViewById(R.id.Qty)).setText("Qty: " + Util.NUMBER_FORMAT.format(model.getInt("Qty")));
                ((TextView) layout.findViewById(R.id.Note)).setText("Note : " + model.getString("Note"));
                ((LinearLayout) detail_price_monitoring).addView(layout);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<LogDataDetailModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public CommonModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final CommonModel headerTitle = (CommonModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_log_data, null);
        }
        TextView BeginVisitTime = (TextView) convertView.findViewById(R.id.BeginVisitTime);
        TextView CustName = (TextView) convertView.findViewById(R.id.CustName);

        if (headerTitle.Status != null && headerTitle.Status.equals("0")) {
            BeginVisitTime.setTextColor(_context.getResources().getColor(R.color.red));
            CustName.setTextColor(_context.getResources().getColor(R.color.red));
        } else {
            BeginVisitTime.setTextColor(_context.getResources().getColor(R.color.black));
            CustName.setTextColor(_context.getResources().getColor(R.color.black));
        }

        ImageButton btn_send = (ImageButton) convertView.findViewById(R.id.btn_send);
        BeginVisitTime.setTypeface(null, Typeface.BOLD);
        BeginVisitTime.setText(headerTitle.BeginVisitTime);
        CustomerModel customerModel = ctrlCustomer.get(headerTitle.CustomerId);
        if (customerModel != null) {
            CustName.setText(customerModel.CustomerName);
        }
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send(headerTitle);
            }
        });
        return convertView;
    }

    private void send(final CommonModel headerTitle) {
        if (headerTitle.Status != null && headerTitle.Status.equals("1")) {
            Util.confirmDialog(_context, "Info", "Data sudah terkirim, Apakah ingin mengirim lagi?", new ConfirmListener() {
                @Override
                public void onDialogCompleted(boolean answer) {
                    if (answer) {
                        new LoadData(_context, headerTitle).execute();
                    }
                }
            });
        } else {
            new LoadData(_context, headerTitle).execute();
        }
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(CommonModel commonModel) {
        _listDataHeader.remove(commonModel);
        notifyDataSetChanged();
    }

    public abstract void berhasilKirim();

    private class LoadData extends AsyncTask<Void, Void, String> {
        private CommonModel commonModel;
        private ProgressDialog progressDialog;

        public LoadData(Context context, CommonModel commonModel) {
            this.commonModel = commonModel;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            return Util.sendUlang(_context, commonModel.CommonID);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result == null) {
//				notifyDataSetChanged();
                berhasilKirim();
                Util.showDialogInfo(_context, "Data berhasil dikirim");
            } else {
//				Util.showDialogError(_context, result);
                Util.infoErrorKirimLog(_context, result);
            }
            super.onPostExecute(result);
        }
    }
}