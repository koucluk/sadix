package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.StokOnCarModel;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class StokOnCarAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<StokOnCarModel>> _listDataChild;
 
    public StokOnCarAdapter(Context context, List<GroupSKUModel> listDataHeader,
            HashMap<GroupSKUModel, List<StokOnCarModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(GroupSKUModel brandModel) {
    	addHeader(brandModel, true);
    }
    
    public void addHeader(GroupSKUModel brandModel, boolean isNotifyData) {
    	_listDataHeader.add(brandModel);
    	if(isNotifyData) {
    		notifyDataSetChanged();
    	}
    }
    
    public void addChild(GroupSKUModel brandModel, StokOnCarModel productModel) {
    	addChild(brandModel, productModel, true);
    }
    
    public void addChild(GroupSKUModel brandModel, StokOnCarModel productModel, boolean isNotifyData) {
    	List<StokOnCarModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<StokOnCarModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	if(isNotifyData) {
    		notifyDataSetChanged();
    	}
    }
    
    public void addChild(GroupSKUModel brandModel, List<StokOnCarModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public StokOnCarModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final StokOnCarModel childText = (StokOnCarModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListItemJml);
        txtListChild.setText(childText.skuModel.toString());
        txtListChildJml.setText(String.valueOf(childText.QTY_B)+","+String.valueOf(childText.QTY_K));
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<StokOnCarModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int[] hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml[0])+","+String.valueOf(hitungJml[1]));
        return convertView;
    }
 
    private int[] hitungJumlah(GroupSKUModel headerTitle) {
    	int[] jml = new int[2];
    	jml[0] = 0;
    	jml[1] = 0;
    	List<StokOnCarModel> list = _listDataChild.get(headerTitle);
    	if(list != null) {
	    	for (StokOnCarModel stokOnCarModel : list) {
				jml[0] += stokOnCarModel.QTY_B;
				jml[1] += stokOnCarModel.QTY_K;
			}
    	}
		return jml;
	}

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(GroupSKUModel groupSKUModel) {
		removeHeader(groupSKUModel, true);
	}

	public void removeHeader(GroupSKUModel groupSKUModel, boolean isNotifyData) {
    	_listDataHeader.remove(groupSKUModel);
    	if(isNotifyData) {
    		notifyDataSetChanged();
    	}
	}
    
    public void removeChild(GroupSKUModel brandModel) {
    	List<StokOnCarModel> list = _listDataChild.get(brandModel);
    	if(list != null) {
    		_listDataChild.put(brandModel, null);
    	}
    	notifyDataSetChanged();
    }
    
    public List<StokOnCarModel> listOrderByGroup(GroupSKUModel brandModel) {
    	return _listDataChild.get(brandModel);
    }
}