package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.bhn.sadix.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DiskonAdapter extends BaseAdapter {
    private List<JSONObject> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private CheckedChangeListener listener;
    private List<JSONObject> listPilih;

    //	private JSONObject jsonPilih;
    public DiskonAdapter(Context context, List<JSONObject> mList) {
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    //	public DiskonAdapter(Context context, List<JSONObject> mList, JSONObject jsonPilih) {
//		mInflater = LayoutInflater.from(context);
//		this.mList = mList;
//		this.jsonPilih = jsonPilih;
//	}
    public DiskonAdapter(Context context) {
        this(context, new ArrayList<JSONObject>());
    }

    //	public void setJsonPilih(JSONObject jsonPilih) {
//		this.jsonPilih = jsonPilih;
//	}
//	public JSONObject getJsonPilih() {
//		return jsonPilih;
//	}
    public void setList(List<JSONObject> mList) {
        this.mList = mList;
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void setListPilih(List<JSONObject> listPilih) {
        this.listPilih = listPilih;
    }

    public void setCheckedChangeListener(CheckedChangeListener listener) {
        this.listener = listener;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(JSONObject model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(JSONObject model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(JSONObject model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(JSONObject model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public JSONObject getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final JSONObject menu = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_list_diskon, null);
            holder = new ViewHolder();
            holder.item = (CheckBox) convertView.findViewById(R.id.item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {
            holder.item.setOnCheckedChangeListener(null);
            if (menu.has("isChecked") && menu.getBoolean("isChecked")) {
                holder.item.setChecked(true);
            } else {
                holder.item.setChecked(false);
            }
            if (cekDiskon(menu)) {
                try {
                    menu.put("isChecked", true);
                    holder.item.setChecked(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//			 if(jsonPilih != null) {
//				 android.util.Log.e("pilih", ""+jsonPilih.getInt("DISCOUNT_PROMO_ID") + " :: " +  menu.getInt("DISCOUNT_PROMO_ID"));
//				 holder.item.setChecked((jsonPilih.getInt("DISCOUNT_PROMO_ID") == menu.getInt("DISCOUNT_PROMO_ID")));
//			 }
            holder.item.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        menu.put("isChecked", isChecked);
                        if (listener != null) {
                            listener.onCheckedChanged(DiskonAdapter.this, menu, isChecked);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.item.setText(menu.getString("DISCOUNT_PROMO_NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private boolean cekDiskon(JSONObject json) {
        try {
            for (JSONObject jsonObject : listPilih) {
                if (jsonObject.getInt("DISCOUNT_PROMO_ID") == json.getInt("DISCOUNT_PROMO_ID")) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    static class ViewHolder {
        CheckBox item;
    }

    public interface CheckedChangeListener {
        public void onCheckedChanged(DiskonAdapter adapter, JSONObject json, boolean isChecked);
    }

}
