package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.ComboBoxModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ComboBoxAdapter extends BaseAdapter {
    private List<ComboBoxModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;

    public ComboBoxAdapter(Context context, List<ComboBoxModel> mList) {
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(ComboBoxModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(ComboBoxModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(ComboBoxModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return mList.size();
    }

    public ComboBoxModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ComboBoxModel menu = (ComboBoxModel) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_text, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText(menu.text);
        return convertView;
    }

    static class ViewHolder {
        TextView text;
    }

    public int indexOf(ComboBoxModel comboBoxModel) {
        return mList.indexOf(comboBoxModel);
    }

}
