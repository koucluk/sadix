package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.ReturDetailModel;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class ReturDetailAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<ReturDetailModel>> _listDataChild;
 
    public ReturDetailAdapter(Context context, List<GroupSKUModel> listDataHeader,
            HashMap<GroupSKUModel, List<ReturDetailModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(GroupSKUModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addHeaderNoNotifay(GroupSKUModel brandModel) {
    	_listDataHeader.add(brandModel);
    }
    
    public void addChild(GroupSKUModel brandModel, ReturDetailModel productModel) {
    	List<ReturDetailModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<ReturDetailModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChildNoNotifay(GroupSKUModel brandModel, ReturDetailModel productModel) {
    	List<ReturDetailModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<ReturDetailModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    }
    
    public void addChild(GroupSKUModel brandModel, List<ReturDetailModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
    
    public void addChildNoNotifay(GroupSKUModel brandModel, List<ReturDetailModel> list) {
		_listDataChild.put(brandModel, list);
    }
 
    @Override
    public ReturDetailModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ReturDetailModel childText = (ReturDetailModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListItemJml);
        txtListChild.setText(childText.skuModel.toString());
        txtListChildJml.setText(String.valueOf(childText.QTY_B)+","+String.valueOf(childText.QTY_K));
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<ReturDetailModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int[] hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml[0])+","+String.valueOf(hitungJml[1]));
        return convertView;
    }
 
    private int[] hitungJumlah(GroupSKUModel headerTitle) {
    	int[] jml = new int[2];
    	jml[0] = 0;
    	jml[1] = 0;
    	List<ReturDetailModel> list = _listDataChild.get(headerTitle);
    	if(list != null) {
	    	for (ReturDetailModel stokCustomerModel : list) {
				jml[0] += stokCustomerModel.QTY_B;
				jml[1] += stokCustomerModel.QTY_K;
			}
    	}
		return jml;
	}

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(GroupSKUModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
    
    public void removeChild(GroupSKUModel brandModel) {
    	List<ReturDetailModel> list = _listDataChild.get(brandModel);
    	if(list != null) {
    		_listDataChild.put(brandModel, null);
    	}
    	notifyDataSetChanged();
    }
    
    public List<ReturDetailModel> listOrderByGroup(GroupSKUModel brandModel) {
    	return _listDataChild.get(brandModel);
    }

	public int getPosisionHeader(GroupSKUModel groupSKUModel) {
    	return _listDataHeader.indexOf(groupSKUModel);
	}
}