package com.bhn.sadix.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.R;
import com.bhn.sadix.model.CustBrandingModel;

import java.util.List;

import io.realm.RealmResults;

public class CustBrandingAdapter extends BaseAdapter {
    //    private List<CustBrandingModel> mList;
    private RealmResults<CustomerBrandingModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;

    public CustBrandingAdapter(Context context, RealmResults<CustomerBrandingModel> mList) {
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(CustomerBrandingModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(CustomerBrandingModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(CustomerBrandingModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(CustBrandingModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public CustomerBrandingModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        CustomerBrandingModel menu = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_cust_photo, null);
            holder = new ViewHolder();
            holder.NmCust = (TextView) convertView.findViewById(R.id.NmCust);
            holder.NmBranding = (TextView) convertView.findViewById(R.id.NmPhoto);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Log.d("NmCust", menu.NmCust+"");
        holder.NmCust.setText(menu.NmCust);
        holder.NmBranding.setText(menu.NmBranding);
        return convertView;
    }

    static class ViewHolder {
        TextView NmCust;
        TextView NmBranding;
    }

}
