package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.Util;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class PriceListAdapter extends BaseExpandableListAdapter {

    private String Filter = "";
    private static Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<SKUModel>> _listDataChild;

    public PriceListAdapter(Context context, List<GroupSKUModel> listDataHeader,
                            HashMap<GroupSKUModel, List<SKUModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        if (listChildData != null) {
            this._listDataChild = listChildData;
        } else {
            this._listDataChild = new HashMap<>();
        }
    }

    public PriceListAdapter(Context context, List<GroupSKUModel> listDataHeader,
                            HashMap<GroupSKUModel, List<SKUModel>> listChildData, String filter) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        if (listChildData != null) {
            this._listDataChild = listChildData;
        } else {
            this._listDataChild = new HashMap<>();
        }

        Filter = filter;
    }

    public void addHeader(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }

    public void addHeaderNoNotifay(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
    }

    public void addChild(GroupSKUModel brandModel, SKUModel productModel) {
        List<SKUModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(GroupSKUModel brandModel, SKUModel productModel) {
        List<SKUModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
    }

    public void addChild(GroupSKUModel brandModel, List<SKUModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(GroupSKUModel brandModel, List<SKUModel> list) {
        _listDataChild.put(brandModel, list);
    }

    @Override
    public SKUModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final SKUModel skuModel = getChild(groupPosition, childPosition);
        Log.d("GroupID - SKUModel", skuModel.GroupId);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_price_list, null);
        }

        TextView txtSku = (TextView) convertView.findViewById(R.id.txt_sku_id);
        TextView txtSkuName = (TextView) convertView.findViewById(R.id.txt_sku_name);
        TextView txtIcon = (TextView) convertView.findViewById(R.id.icon_list);
        TextView txtValueB = (TextView) convertView.findViewById(R.id.value_big_price);
        TextView txtValueK = (TextView) convertView.findViewById(R.id.value_small_price);
        TextView txtLabelB = (TextView) convertView.findViewById(R.id.label_big_price);
        TextView txtLabelK = (TextView) convertView.findViewById(R.id.label_small_price);
        ImageView showPicture = (ImageView) convertView.findViewById(R.id.btn_show_picture);

        txtSku.setText(skuModel.SKUId);
        txtSkuName.setText(skuModel.ProductName);
        txtIcon.setText(String.valueOf(skuModel.ProductName.charAt(0)));

        String unitB = skuModel.SBESAR + " : ";
        String unitK = skuModel.SKECIL + " : ";

        txtLabelB.setText(unitB);
        txtLabelK.setText(unitK);
        txtValueB.setText(Util.numberFormat.format(skuModel.HET_PRICE_L));
        txtValueK.setText(Util.numberFormat.format(skuModel.HET_PRICE));

        showPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowDialog(skuModel);
            }
        });

        if (!Filter.equals("")) {
//            Filter = Filter.toUpperCase();


            /*String replacedSKU = skuModel.SKUId.replaceAll("(?i)" + Filter, "<font color='#00FF1E'>" + Filter + "</font>");
            String replacedName = skuModel.ProductName.replaceAll("(?i)" + Filter, "<font color='#00FF1E'>" + Filter + "</font>");*/

            /*txtSku.setText(Html.fromHtml(replacedSKU));
            txtSkuName.setText(Html.fromHtml(replacedName));*/
            txtSku.setText(highlight(Filter, skuModel.SKUId));
            txtSkuName.setText(highlight(Filter, skuModel.ProductName));
        }

        return convertView;
    }

    public static CharSequence highlight(String search, String originalText) {
        // ignore case and accents
        // the same thing should have been done for the search text
        String normalizedText = Normalizer.normalize(originalText, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").toLowerCase();

        int start = normalizedText.indexOf(search);
        if (start < 0) {
            // not found, nothing to to
            return originalText;
        } else {
            // highlight each appearance in the original text
            // while searching in normalized text
            Spannable highlighted = new SpannableString(originalText);
            while (start >= 0) {
                int spanStart = Math.min(start, originalText.length());
                int spanEnd = Math.min(start + search.length(), originalText.length());

                highlighted.setSpan(new BackgroundColorSpan(_context.getResources().getColor(R.color.highlighted_text)), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                highlighted.setSpan(new ForegroundColorSpan(_context.getResources().getColor(android.R.color.black)), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                start = normalizedText.indexOf(search, spanEnd);
            }

            return highlighted;
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<SKUModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = getGroup(groupPosition);
        Log.d("GroupID", headerTitle.GroupId);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_price_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView txtLabelPromo = (TextView) convertView.findViewById(R.id.label_promo);
        TextView txtLabelFokus = (TextView) convertView.findViewById(R.id.label_fokus);

        if (headerTitle.FokusId.equals("1")) {
            txtLabelFokus.setVisibility(View.VISIBLE);
        } else {
            txtLabelFokus.setVisibility(View.GONE);
        }

        if (headerTitle.PromoId.equals("1")) {
            txtLabelPromo.setVisibility(View.VISIBLE);
        } else {
            txtLabelPromo.setVisibility(View.GONE);
        }

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        return convertView;
    }

    /*private int[] hitungJumlah(GroupSKUModel headerTitle) {
        int[] jml = new int[2];
        jml[0] = 0;
        jml[1] = 0;
        List<SKUModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (SKUModel skuModel : list) {
                jml[0] += stokCustomerModel.QTY_B;
                jml[1] += stokCustomerModel.QTY_K;
            }
        }
        return jml;
    }*/

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(GroupSKUModel groupSKUModel) {
        _listDataHeader.remove(groupSKUModel);
        notifyDataSetChanged();
    }

    public void removeChild(GroupSKUModel brandModel) {
        List<SKUModel> list = _listDataChild.get(brandModel);
        if (list != null) {
            _listDataChild.put(brandModel, null);
        }
        notifyDataSetChanged();
    }

    public List<SKUModel> listOrderByGroup(GroupSKUModel brandModel) {
        return _listDataChild.get(brandModel);
    }

    public int getPosisionHeader(GroupSKUModel groupSKUModel) {
        return _listDataHeader.indexOf(groupSKUModel);
    }

    public abstract void onShowDialog(SKUModel skuModel);
}