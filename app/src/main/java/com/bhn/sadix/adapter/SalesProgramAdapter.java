package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.SalesProgramModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SalesProgramAdapter extends BaseAdapter {
	private List<SalesProgramModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public SalesProgramAdapter(Context context, List<SalesProgramModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(SalesProgramModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(SalesProgramModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(SalesProgramModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int indexOf(SalesProgramModel model) {
		return mList.indexOf(model);
    }
	public int getCount() {
		return mList.size();
	}

	public SalesProgramModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		SalesProgramModel menu = (SalesProgramModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_text, null);
			 holder = new ViewHolder();
			 holder.text = (TextView) convertView.findViewById(R.id.text);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.text.setText(menu.SalesProgramDescription);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView text;
	}

}
