package com.bhn.sadix.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.KunjunganListener;
import com.bhn.sadix.R;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.model.CustBrandingModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.ItemPhotoCustomerBrandingWidgetListener;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by caksono on 29/03/17.
 */

public class BrandingPendingAdapter extends RecyclerView.Adapter<BrandingPendingAdapter.ViewHolder> implements ConnectionEvent {

    List<CustBrandingModel> custBrandingModels;
    byte[] data;
    Context context;
    CustomerModel customerModel;
    private ConnectionServer server;
    CustBrandingModel selectedModel;
    private CtrlCustBranding ctrlCustBranding;
    ItemPhotoCustomerBrandingWidgetListener listener;
    private int Clicked = 0;
    private Dialog pendingDialog;
    KunjunganListener kunjunganListener;

    public BrandingPendingAdapter(List<CustBrandingModel> model, ItemPhotoCustomerBrandingWidgetListener listener, Context context, CustomerModel customerModel, Dialog pendingDialog
            , KunjunganListener kunjunganListener) {
        custBrandingModels = model;
        this.context = context;
        this.customerModel = customerModel;
        this.listener = listener;
        this.kunjunganListener = kunjunganListener;

        server = new ConnectionServer(context);
        server.addConnectionEvent(this);
        ctrlCustBranding = new CtrlCustBranding(context);
        this.pendingDialog = pendingDialog;
    }

    public void saveLokal(boolean send) {

//        ctrlCustBranding.delete(customerModel.CustId, selectedModel.BrandingID);

        CustBrandingModel brandingModel = new CustBrandingModel();
        brandingModel.BrandingID = selectedModel.BrandingID;
        brandingModel.CustomerID = customerModel.CustId;
        brandingModel.Description = selectedModel.Description;
        brandingModel.PhotoPic = customerModel.CustId + "_" + selectedModel.BrandingID + ".jpg";
        brandingModel.SalesID = UserModel.getInstance(context).SalesId;

        if (!send)
            brandingModel.done = 0;
        else
            brandingModel.done = 1;

        if (data != null) {
            Util.saveFileImg(data, brandingModel.PhotoPic);
            data = null;
        }
        if (context instanceof KunjunganActivity) {
            KunjunganActivity activity = (KunjunganActivity) context;
            brandingModel.GeoLong = activity.commonModel.GeoLong;
            brandingModel.GeoLat = activity.commonModel.GeoLat;
        }

        ctrlCustBranding.save(brandingModel);

        Util.Toast(context, "Data tersimpan dilokal");

        custBrandingModels.remove(selectedModel);
        notifyDataSetChanged();
    }

    public void sendData(CustBrandingModel custBrandingModel) {
        if (data == null) {
            Toast.makeText(context, "Data Null", Toast.LENGTH_SHORT).show();
            Util.showDialogInfo(context, "Photo belum diambil!");
            return;
        }
        try {
            JSONObject json = new JSONObject();
            json.put("SalesID", UserModel.getInstance(context).SalesId);
            json.put("CustomerID", customerModel.CustId);
            json.put("BrandingID", custBrandingModel.BrandingID);
            json.put("PhotoPic", Base64.encodeBytes(data));
            if (context instanceof KunjunganActivity) {
                KunjunganActivity activity = (KunjunganActivity) context;
                json.put("GeoLong", activity.commonModel.GeoLong);
                json.put("GeoLat", activity.commonModel.GeoLat);
            }
            json.put("Description", custBrandingModel.Description);
            server.setUrl(Util.getServerUrl(context) + "photobranding");
            server.requestJSONObjectNoTimeOut(json);
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.txtDesc.setText(custBrandingModels.get(position).Description);
        String type = custBrandingModels.get(position).BrandingID.equals("6") ? "Produk After" : "Produk Before";
        holder.txtType.setText(type);

        try {
            data = Util.readFile(Util.LOKASI_IMAGE + custBrandingModels.get(position).PhotoPic);
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            holder.imgThumb.setImageDrawable(new BitmapDrawable(bmp));
        } catch (IOException e) {
            e.printStackTrace();
        }

        holder.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                Clicked = position;
                selectedModel = custBrandingModels.get(position);
                sendData(selectedModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return custBrandingModels.size();
    }

    @Override
    public void error(String error) {
        saveLokal(true);
        checkSize();
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                Log.wtf("RESULT OK", "OK");
                data = null;
                custBrandingModels.remove(selectedModel);
                notifyDataSetChanged();
            } else {
                saveLokal(true);
            }
            Util.Toast(context, json.getString("Message"));
            checkSize();
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    private void checkSize() {
        Log.d("Check Size", "Called");

        if (custBrandingModels.size() == 0) {
            Log.d("Dialog Dismiss", "Dismissed");
            kunjunganListener.dismissAndUpdateDialog(pendingDialog);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgThumb;
        TextView txtDesc;
        ImageButton btnSend;
        TextView txtType;

        public ViewHolder(View itemView) {
            super(itemView);

            imgThumb = (ImageView) itemView.findViewById(R.id.img_pending_branding);
            txtDesc = (TextView) itemView.findViewById(R.id.txt_desc_pending_branding);
            txtType = (TextView) itemView.findViewById(R.id.txt_type_pending_branding);
            btnSend = (ImageButton) itemView.findViewById(R.id.btn_send_branding);
        }
    }
}
