package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CapasityModel;
import com.bhn.sadix.model.GroupSKUModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CapasityAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<CapasityModel>> _listDataChild;

    public CapasityAdapter(Context context, List<GroupSKUModel> listDataHeader,
                           HashMap<GroupSKUModel, List<CapasityModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    public void addHeader(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }

    public void addHeaderNoNotifay(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
    }

    public void addChild(GroupSKUModel brandModel, CapasityModel productModel) {
        List<CapasityModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<CapasityModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(GroupSKUModel brandModel, CapasityModel productModel) {
        List<CapasityModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<CapasityModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
    }

    public void addChild(GroupSKUModel brandModel, List<CapasityModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(GroupSKUModel brandModel, List<CapasityModel> list) {
        _listDataChild.put(brandModel, list);
    }

    @Override
    public CapasityModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CapasityModel childText = (CapasityModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_capasity_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText.skuModel.toString());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<CapasityModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_capasity_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(GroupSKUModel groupSKUModel) {
        _listDataHeader.remove(groupSKUModel);
        notifyDataSetChanged();
    }

    public void removeChild(GroupSKUModel brandModel) {
        List<CapasityModel> list = _listDataChild.get(brandModel);
        if (list != null) {
            _listDataChild.put(brandModel, null);
        }
        notifyDataSetChanged();
    }

    public List<CapasityModel> listOrderByGroup(GroupSKUModel brandModel) {
        return _listDataChild.get(brandModel);
    }

    public int getPosisionHeader(GroupSKUModel groupSKUModel) {
        return _listDataHeader.indexOf(groupSKUModel);
    }
}