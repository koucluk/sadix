package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.R;
import com.bhn.sadix.model.CollectorModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public abstract class CollectionPayAdapter extends BaseAdapter {
    //    private List<CollectorModel> mList;
    private List<CollectionModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private Context context;
    //	private CollectionPayListener listener;
    private boolean isEdit = true;
    Realm realm;

    /*public CollectionPayAdapter(Context context, List<CollectorModel> mList) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }*/

    public CollectionPayAdapter(Context context, List<CollectionModel> mList) {
        this.context = context;
        realm = Realm.getDefaultInstance();
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
        notifyDataSetChanged();
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(final CollectionModel model, final int position) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<CollectionModel> pay = realm.where(CollectionModel.class).equalTo("RandomID", model.RandomID).findAll();
                pay.deleteAllFromRealm();
            }
        });
        mList.remove(position);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
        /*if (mList.remove(model)) {

        }*/
    }

    /*public void insert(CollectorModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }*/

    public void add(final CollectionModel model) {
        mList.add(model);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(model);
            }
        });
//        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(CollectorModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

//    public CollectorModel getItem(int position) {
//        return mList.get(position);
//    }

    public CollectionModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
//        final CollectorModel menu = (CollectorModel) getItem(position);

        final CollectionModel menu = getItem(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_collection_pay, null);
            holder = new ViewHolder();
            holder.data1 = (TextView) convertView.findViewById(R.id.data1);
            holder.data2 = (TextView) convertView.findViewById(R.id.data2);
            holder.data3 = (TextView) convertView.findViewById(R.id.data3);
            holder.imageExpand = (ImageView) convertView.findViewById(R.id.imageExpand);
            holder.imageDelete = (ImageView) convertView.findViewById(R.id.imageDelete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (isEdit) {
            holder.imageExpand.setVisibility(View.VISIBLE);
            holder.imageDelete.setVisibility(View.VISIBLE);
        } else {
            holder.imageExpand.setVisibility(View.GONE);
            holder.imageDelete.setVisibility(View.GONE);
        }
        final TextView data3 = holder.data3;
        final ImageView imageExpand = holder.imageExpand;
        holder.imageExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data3.getVisibility() == View.GONE) {
                    imageExpand.setImageResource(android.R.drawable.arrow_up_float);
                    data3.setVisibility(View.VISIBLE);
                } else if (data3.getVisibility() == View.VISIBLE) {
                    imageExpand.setImageResource(android.R.drawable.arrow_down_float);
                    data3.setVisibility(View.GONE);
                }
//				if(listener != null) {
//					listener.expand();
//				}
            }
        });
        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.confirmDialog(context, "INFO", "Apakah data ingin dihapus?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) {
                            remove(menu, position);
                            updateRemove();
                        }
                    }
                });
            }
        });
        holder.data1.setText(menu.data1);
        holder.data2.setText(menu.payMethod);
        holder.data3.setText(menu.data3);
        return convertView;
    }

    public abstract void updateRemove();

    //	public void addCollectionPayListener(CollectionPayListener listener) {
//		this.listener = listener;
//	}
//	public interface CollectionPayListener {
//		public void expand();
//	}
    static class ViewHolder {
        TextView data1;
        TextView data2;
        TextView data3;
        ImageView imageExpand;
        ImageView imageDelete;
    }

}
