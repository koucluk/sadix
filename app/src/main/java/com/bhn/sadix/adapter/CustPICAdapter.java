package com.bhn.sadix.adapter;

import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

public class CustPICAdapter extends BaseAdapter {
	private List<CustPIC> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	private Context context;
	public CustPICAdapter(Context context, List<CustPIC> mList) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void setList(List<CustPIC> mList) {
		this.mList = mList;
		notifyDataSetChanged();
	}
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(CustPIC model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(CustPIC model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(CustPIC model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public CustPIC getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CustPIC menu = (CustPIC) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_pic_cust, null);
			 holder = new ViewHolder();
			 holder.PicName = (TextView) convertView.findViewById(R.id.PicName);
			 holder.image = (ImageView) convertView.findViewById(R.id.image);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.PicName.setText(menu.PicName);
		 holder.image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hapus(menu);
			}
		});
		 return convertView;
	}
	
	private void hapus(final CustPIC menu) {
		Util.confirmDialog(context, "Info", "Apakah data ingin dihapus?", new ConfirmListener() {
			@Override
			public void onDialogCompleted(boolean answer) {
				if(answer) {
					remove(menu);
				}
			}
		});
	}
	public JSONArray toJsonArray() {
		JSONArray array = new JSONArray();
		for (CustPIC custPIC : mList) {
			array.put(custPIC.toJson());
		}
		return array;
	}

	public List<CustPIC> getList() {
		return mList;
	}

	static class ViewHolder {
		TextView PicName;
		ImageView image;
	}

}
