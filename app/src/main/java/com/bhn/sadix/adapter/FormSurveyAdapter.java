package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.FormSurveyModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FormSurveyAdapter extends BaseAdapter {
	private List<FormSurveyModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public FormSurveyAdapter(Context context, List<FormSurveyModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(FormSurveyModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(FormSurveyModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(FormSurveyModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public FormSurveyModel getItem(int position) {
		return mList.get(position);
	}
	public int getPosition(FormSurveyModel menu) {
		return mList.indexOf(menu);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		FormSurveyModel menu = (FormSurveyModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_form_survey, null);
			 holder = new ViewHolder();
			 holder.FormName = (TextView) convertView.findViewById(R.id.FormName);
			 holder.FormRemark = (TextView) convertView.findViewById(R.id.FormRemark);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.FormName.setText(menu.FormName);
		 holder.FormRemark.setText(menu.FormRemark);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView FormName;
		TextView FormRemark;
	}

}
