package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.StokCompetitorModel;
 
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
 
public class StokCompetitorAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupSKUComModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUComModel, List<StokCompetitorModel>> _listDataChild;
 
    public StokCompetitorAdapter(Context context, List<GroupSKUComModel> listDataHeader,
            HashMap<GroupSKUComModel, List<StokCompetitorModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(GroupSKUComModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupSKUComModel brandModel, StokCompetitorModel productModel) {
    	List<StokCompetitorModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<StokCompetitorModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupSKUComModel brandModel, List<StokCompetitorModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public StokCompetitorModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final StokCompetitorModel childText = (StokCompetitorModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListItemJml);
        txtListChild.setText(childText.skuModel.toString());
        txtListChildJml.setText(String.valueOf(childText.QTY_B)+","+String.valueOf(childText.QTY_K));
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<StokCompetitorModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupSKUComModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUComModel headerTitle = (GroupSKUComModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int[] hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml[0])+","+String.valueOf(hitungJml[1]));
        return convertView;
    }
 
    private int[] hitungJumlah(GroupSKUComModel headerTitle) {
    	int[] jml = new int[2];
    	jml[0] = 0;
    	jml[1] = 0;
    	List<StokCompetitorModel> list = _listDataChild.get(headerTitle);
    	if(list != null) {
	    	for (StokCompetitorModel stokCustomerModel : list) {
				jml[0] += stokCustomerModel.QTY_B;
				jml[1] += stokCustomerModel.QTY_K;
			}
    	}
		return jml;
	}

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(GroupSKUComModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
}