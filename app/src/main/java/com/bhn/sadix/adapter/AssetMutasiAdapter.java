package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.model.AssetMutasiModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class AssetMutasiAdapter extends BaseAdapter {
    private List<AssetMutasiModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private Context context;
    private DbAsetHelper dbAsetHelper;
    Realm realm;

    public AssetMutasiAdapter(Context context, List<AssetMutasiModel> mList) {
        this.context = context;
        dbAsetHelper = new DbAsetHelper(context);
        mInflater = LayoutInflater.from(context);
        realm = Realm.getDefaultInstance();
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(AssetMutasiModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(AssetMutasiModel model, int index) {
        model.adapter = this;
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(AssetMutasiModel model) {
        model.adapter = this;
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(AssetMutasiModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public AssetMutasiModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final AssetMutasiModel menu = (AssetMutasiModel) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_asset, null);
            holder = new ViewHolder();
            holder.ASETNAME = (TextView) convertView.findViewById(R.id.ASETNAME);
            holder.ASETNO = (TextView) convertView.findViewById(R.id.ASETNO);
            holder.btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.confirmDialog(context, "INFO", "Anda yakin akan menghapus data?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) {
                            if (menu.getAssetMutasiWidget().getJenis() == 1) {
                                final RealmResults<com.bhn.sadix.Data.AssetMutasiModel> res =
                                        realm.where(com.bhn.sadix.Data.AssetMutasiModel.class).equalTo("AsetID", menu.ASETID)
                                                .equalTo("CommonID", menu.CommonID).findAll();
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        res.deleteAllFromRealm();
                                    }
                                });
                                dbAsetHelper.deleteAsset(menu.ASETID);
                            }
                            remove(menu);
                        }
                    }
                });
            }
        });
        holder.ASETNAME.setText(menu.ASETNAME);
        holder.ASETNO.setText(menu.ASETNO);
        if (menu.getAssetMutasiWidget().getJenis() == 1) {
            convertView.setBackgroundColor(Color.LTGRAY);
        } else {
            convertView.setBackgroundColor(Color.WHITE);
        }
        return convertView;
    }

    static class ViewHolder {
        TextView ASETNAME;
        TextView ASETNO;
        ImageView btndelete;
    }

}
