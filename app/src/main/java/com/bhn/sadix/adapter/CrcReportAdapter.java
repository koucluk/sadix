package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CRCReportModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.util.Util;
 
public class CrcReportAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<CRCReportModel>> _listDataChild;
 
    public CrcReportAdapter(Context context, List<GroupSKUModel> listDataHeader,
            HashMap<GroupSKUModel, List<CRCReportModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addHeader(GroupSKUModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupSKUModel brandModel, CRCReportModel productModel) {
    	List<CRCReportModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<CRCReportModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChildNoNotify(GroupSKUModel brandModel, CRCReportModel productModel) {
    	List<CRCReportModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<CRCReportModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    }
    
    public void addChild(GroupSKUModel brandModel, List<CRCReportModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public CRCReportModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CRCReportModel childText = (CRCReportModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_crc_report_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        
        TextView order_1 = (TextView) convertView.findViewById(R.id.order_1);
        TextView order_2 = (TextView) convertView.findViewById(R.id.order_2);
        TextView order_3 = (TextView) convertView.findViewById(R.id.order_3);
        
        TextView stock_1 = (TextView) convertView.findViewById(R.id.stock_1);
        TextView stock_2 = (TextView) convertView.findViewById(R.id.stock_2);
        TextView stock_3 = (TextView) convertView.findViewById(R.id.stock_3);
        
        TextView lbl_3 = (TextView) convertView.findViewById(R.id.lbl_3);
        TextView lbl_2 = (TextView) convertView.findViewById(R.id.lbl_2);
        TextView lbl_1 = (TextView) convertView.findViewById(R.id.lbl_1);
        
        txtListChild.setText(childText.skuModel.toString());
        if(childText.ordes != null && childText.ordes.size() > 0) {
        	if(childText.ordes.size() == 1) {
        		order_1.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY));
        	} else if(childText.ordes.size() == 2) {
        		order_1.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY));
        		order_2.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(1).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(1).QTY));
        	} else if(childText.ordes.size() == 3) {
        		order_1.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(0).QTY));
        		order_2.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(1).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(1).QTY));
        		order_3.setText(Util.NUMBER_FORMAT.format(childText.ordes.get(2).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.ordes.get(2).QTY));
        	}
        }
        if(childText.stocks != null && childText.stocks.size() > 0) {
        	if(childText.stocks.size() == 1) {
        		stock_1.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY));
        		lbl_1.setText(childText.stocks.get(0).POSTDATE);
        	} else if(childText.stocks.size() == 2) {
        		stock_1.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY));
        		stock_2.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(1).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(1).QTY));
        		lbl_1.setText(childText.stocks.get(0).POSTDATE);
        		lbl_2.setText(childText.stocks.get(1).POSTDATE);
        	} else if(childText.stocks.size() == 3) {
        		stock_1.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(0).QTY));
        		stock_2.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(1).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(1).QTY));
        		stock_3.setText(Util.NUMBER_FORMAT.format(childText.stocks.get(2).QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.stocks.get(2).QTY));
        		lbl_1.setText(childText.stocks.get(0).POSTDATE);
        		lbl_2.setText(childText.stocks.get(1).POSTDATE);
        		lbl_3.setText(childText.stocks.get(2).POSTDATE);
        	}
        }
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<CRCReportModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_crc_report_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        return convertView;
    }

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(GroupSKUModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
    
    public void removeChild(GroupSKUModel brandModel) {
    	List<CRCReportModel> list = _listDataChild.get(brandModel);
    	if(list != null) {
    		_listDataChild.put(brandModel, null);
    	}
    	notifyDataSetChanged();
    }
    
    public List<CRCReportModel> listOrderByGroup(GroupSKUModel brandModel) {
    	return _listDataChild.get(brandModel);
    }
    
    public int getPosisionHeader(GroupSKUModel model) {
    	return _listDataHeader.indexOf(model);
    }
}