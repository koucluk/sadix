package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.AssetModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import java.util.List;

public class AssetAdapter extends BaseAdapter {
    private List<AssetModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private Context context;

    public AssetAdapter(Context context, List<AssetModel> mList) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(AssetModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(AssetModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(AssetModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(AssetModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public AssetModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final AssetModel menu = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_asset, null);
            holder = new ViewHolder();
            holder.ASETNAME = (TextView) convertView.findViewById(R.id.ASETNAME);
            holder.ASETNO = (TextView) convertView.findViewById(R.id.ASETNO);
            holder.btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.confirmDialog(context, "INFO", "Anda yakin akan menghapus data?", new ConfirmListener() {
                    @Override
                    public void onDialogCompleted(boolean answer) {
                        if (answer) {
                            remove(menu);
                        }
                    }
                });
            }
        });
        holder.ASETNAME.setText(menu.ASETNAME);
        holder.ASETNO.setText(menu.ASETNO);
        return convertView;
    }

    static class ViewHolder {
        TextView ASETNAME;
        TextView ASETNO;
        ImageView btndelete;
    }

}
