package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.DiskonPromoModel;
import com.bhn.sadix.model.GroupSKUModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiskonPromoAdapter extends BaseExpandableListAdapter {

    private Context _context;
    //    private List<GroupSKUModel> _listDataHeader; // header titles
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
//    private HashMap<GroupSKUModel, List<DiskonPromoModel>> _listDataChild;
    private HashMap<String, List<DiskonPromoModel>> _listDataChild;

    /*public DiskonPromoAdapter(Context context, List<GroupSKUModel> listDataHeader,
                              HashMap<GroupSKUModel, List<DiskonPromoModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }*/

    public DiskonPromoAdapter(Context context, List<String> listDataHeader,
                              HashMap<String, List<DiskonPromoModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    /*public void addHeader(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }*/

    public void addHeader(String brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }

    /*public void addChild(GroupSKUModel brandModel, DiskonPromoModel productModel) {
        List<DiskonPromoModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<DiskonPromoModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
        notifyDataSetChanged();
    }*/

    public void addChild(String brandModel, DiskonPromoModel productModel) {
        List<DiskonPromoModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<DiskonPromoModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
        notifyDataSetChanged();
    }

    /*public void addChildNoNotify(GroupSKUModel brandModel, DiskonPromoModel productModel) {
        List<DiskonPromoModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<DiskonPromoModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
    }*/

    /*public void addChild(GroupSKUModel brandModel, List<DiskonPromoModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }*/

    public void addChild(String brandModel, List<DiskonPromoModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }

    @Override
    public DiskonPromoModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final DiskonPromoModel childText = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_diskon_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListItemJml);
        TextView lblListItemSatuan = (TextView) convertView.findViewById(R.id.lblListItemSatuan);
        txtListChild.setText(childText.skuModel.toString());
        txtListChildJml.setText(String.valueOf(childText.QTY));
        lblListItemSatuan.setText(childText.SATUAN);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<DiskonPromoModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    /*@Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }*/

    @Override
    public String getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /*@Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml));
        return convertView;
    }*/

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml));
        return convertView;
    }

    /*public int hitungJumlah(GroupSKUModel headerTitle) {
        int jml = 0;
        List<DiskonPromoModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (DiskonPromoModel model : list) {
                jml += model.QTY;
            }
        }
        return jml;
    }*/

    public int hitungJumlah(String headerTitle) {
        int jml = 0;
        List<DiskonPromoModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (DiskonPromoModel model : list) {
                jml += model.QTY;
            }
        }
        return jml;
    }

    /*public int getTotal() {
        int jml = 0;
        for (int i = 0; i < _listDataHeader.size(); i++) {
            GroupSKUModel headerTitle = _listDataHeader.get(i);
            List<DiskonPromoModel> list = _listDataChild.get(headerTitle);
            if (list != null) {
                for (DiskonPromoModel model : list) {
                    jml += model.QTY;
                }
            }
        }
        return jml;
    }*/

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(GroupSKUModel groupSKUModel) {
        _listDataHeader.remove(groupSKUModel);
        notifyDataSetChanged();
    }

    /*public void removeChild(GroupSKUModel brandModel) {
        List<DiskonPromoModel> list = _listDataChild.get(brandModel);
        if (list != null) {
            _listDataChild.put(brandModel, null);
        }
        notifyDataSetChanged();
    }*/

    public List<DiskonPromoModel> listOrderByGroup(GroupSKUModel brandModel) {
        return _listDataChild.get(brandModel);
    }

    /*public int getPosisionHeader(GroupSKUModel model) {
        return _listDataHeader.indexOf(model);
    }*/

    public int getPosisionHeader(String model) {
        return _listDataHeader.indexOf(model);
    }
}