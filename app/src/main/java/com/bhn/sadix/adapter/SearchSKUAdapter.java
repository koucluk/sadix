package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.SKUModel;

import java.util.List;

public class SearchSKUAdapter extends BaseAdapter {
    private List<SKUModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;

    public SearchSKUAdapter(Context context, List<SKUModel> mList) {
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public List<SKUModel> getmList() {
        return mList;
    }

    public void setmList(List<SKUModel> mList) {
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(SKUModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(SKUModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(SKUModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(SKUModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public SKUModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        SKUModel menu = (SKUModel) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_sku_search, null);
            holder = new ViewHolder();
            holder.ProductName = (TextView) convertView.findViewById(R.id.ProductName);
            holder.SKUCODE = (TextView) convertView.findViewById(R.id.SKUCODE);
            holder.txtIcon = (TextView) convertView.findViewById(R.id.txt_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ProductName.setText(menu.ProductName);
        holder.SKUCODE.setText(menu.SKUCODE);
        holder.txtIcon.setText(menu.ProductName.charAt(0) + "");
        return convertView;
    }

    static class ViewHolder {
        TextView ProductName;
        TextView SKUCODE;
        //		ImageView image;
        TextView txtIcon;
    }

}
