package com.bhn.sadix.adapter;

import java.util.List;

import org.json.JSONObject;

import com.bhn.sadix.R;
import com.bhn.sadix.model.ComboBoxModel;




import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ComboBoxJsonAdapter extends BaseAdapter {
	private List<JSONObject> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	private String text;
	public ComboBoxJsonAdapter(Context context, List<JSONObject> mList, String text) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
		this.text = text;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(JSONObject model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(JSONObject model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(JSONObject model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public JSONObject getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		JSONObject menu = (JSONObject) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_text, null);
			 holder = new ViewHolder();
			 holder.text = (TextView) convertView.findViewById(R.id.text);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 }
		 try {
			 holder.text.setText(menu.getString(text));
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		 return convertView;
	}
	
	static class ViewHolder {
		TextView text;
	}

	public int indexOf(ComboBoxModel comboBoxModel) {
		return mList.indexOf(comboBoxModel);
	}

}
