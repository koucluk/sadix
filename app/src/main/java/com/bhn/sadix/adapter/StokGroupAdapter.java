package com.bhn.sadix.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.StockGroupModel;

public class StokGroupAdapter extends BaseAdapter {
	private List<StockGroupModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public StokGroupAdapter(Context context, List<StockGroupModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(StockGroupModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(StockGroupModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(StockGroupModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public StockGroupModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final StockGroupModel menu = (StockGroupModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_stok_group, null);
			 holder = new ViewHolder();
			 holder.GroupName = (TextView) convertView.findViewById(R.id.GroupName);
			 holder.Stok = (TextView) convertView.findViewById(R.id.Stok);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.GroupName.setText(menu.groupModel.GroupName);
		 holder.Stok.setText(String.valueOf(menu.Quantity2)+","+String.valueOf(menu.Quantity));
		 
		 return convertView;
	}
	
	static class ViewHolder {
		TextView GroupName;
		TextView Stok;
	}

}
