package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.LogGpsModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LogGpsAdapter extends BaseAdapter {
	private List<LogGpsModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public LogGpsAdapter(Context context, List<LogGpsModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(LogGpsModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(LogGpsModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(LogGpsModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int indexOf(LogGpsModel model) {
		return mList.indexOf(model);
    }
	public int getCount() {
		return mList.size();
	}

	public LogGpsModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		LogGpsModel menu = (LogGpsModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_text, null);
			 holder = new ViewHolder();
			 holder.text = (TextView) convertView.findViewById(R.id.text);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.text.setText(menu.LogTime);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView text;
	}

}
