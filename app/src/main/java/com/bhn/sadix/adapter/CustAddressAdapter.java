package com.bhn.sadix.adapter;

import java.util.List;

import org.json.JSONArray;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

public class CustAddressAdapter extends BaseAdapter {
	private List<CustAdddress> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	private Context context;
	public CustAddressAdapter(Context context, List<CustAdddress> mList) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setList(List<CustAdddress> mList) {
		this.mList = mList;
		notifyDataSetChanged();
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(CustAdddress model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(CustAdddress model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(CustAdddress model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public CustAdddress getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CustAdddress menu = (CustAdddress) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_address_cust, null);
			 holder = new ViewHolder();
			 holder.AddName = (TextView) convertView.findViewById(R.id.AddName);
			 holder.image = (ImageView) convertView.findViewById(R.id.image);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.AddName.setText(menu.AddName);
		 holder.image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hapus(menu);
			}
		});
		 return convertView;
	}
	
	private void hapus(final CustAdddress menu) {
		Util.confirmDialog(context, "Info", "Apakah data ingin dihapus?", new ConfirmListener() {
			@Override
			public void onDialogCompleted(boolean answer) {
				if(answer) {
					remove(menu);
				}
			}
		});
	}
	
	public JSONArray toJsonArray(Location location) {
		JSONArray array = new JSONArray();
		for (CustAdddress custAdddress : mList) {
			array.put(custAdddress.toJson(location));
		}
		return array;
	}

	public List<CustAdddress> getList() {
		return mList;
	}

	static class ViewHolder {
		TextView AddName;
		ImageView image;
	}

}
