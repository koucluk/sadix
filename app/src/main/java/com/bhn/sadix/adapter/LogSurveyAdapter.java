package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CommonSurveyModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LogSurveyAdapter extends BaseAdapter {
	private List<CommonSurveyModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public LogSurveyAdapter(Context context, List<CommonSurveyModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(CommonSurveyModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(CommonSurveyModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(CommonSurveyModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int indexOf(CommonSurveyModel model) {
		return mList.indexOf(model);
    }
	public int getCount() {
		return mList.size();
	}

	public CommonSurveyModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		CommonSurveyModel menu = (CommonSurveyModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_cust_photo, null);
			 holder = new ViewHolder();
			 holder.NmCust = (TextView) convertView.findViewById(R.id.NmCust);
			 holder.NmPhoto = (TextView) convertView.findViewById(R.id.NmPhoto);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.NmCust.setText(menu.nmCustomer);
		 holder.NmPhoto.setText(menu.nmForm);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView NmCust;
		TextView NmPhoto;
	}

}
