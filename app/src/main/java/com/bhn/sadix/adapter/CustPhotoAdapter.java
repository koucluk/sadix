package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CustPhotoModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustPhotoAdapter extends BaseAdapter {
	private List<CustPhotoModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public CustPhotoAdapter(Context context, List<CustPhotoModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(CustPhotoModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(CustPhotoModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(CustPhotoModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int indexOf(CustPhotoModel model) {
		return mList.indexOf(model);
    }
	public int getCount() {
		return mList.size();
	}

	public CustPhotoModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		CustPhotoModel menu = (CustPhotoModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_cust_photo, null);
			 holder = new ViewHolder();
			 holder.NmCust = (TextView) convertView.findViewById(R.id.NmCust);
			 holder.NmPhoto = (TextView) convertView.findViewById(R.id.NmPhoto);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 holder.NmCust.setText(menu.NmCust);
		 holder.NmPhoto.setText(menu.NmPhoto);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView NmCust;
		TextView NmPhoto;
	}

}
