package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.MenuUtamaModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuHomeAdapter extends BaseAdapter {
	private List<MenuUtamaModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public MenuHomeAdapter(Context context, List<MenuUtamaModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(MenuUtamaModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(MenuUtamaModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(MenuUtamaModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public MenuUtamaModel getItem(int position) {
		return mList.get(position);
	}
	public int getPosition(MenuUtamaModel menu) {
		return mList.indexOf(menu);
	}

	public long getItemId(int position) {
		return position;
	}

	public boolean isEnabled(int position) {
//		return super.isEnabled(position);
		MenuUtamaModel menu = getItem(position);
		return (menu.id == 0 ? false : true);
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuUtamaModel menu = (MenuUtamaModel) getItem(position);
		 View view;
		 if(menu.id == 0) {
        	 view = getItemView(convertView, parent, menu);
         } else {
        	 view = getSectionView(convertView, parent, menu);
         }
		 return view;
	}
	private View getItemView( View convertView, ViewGroup parentView, MenuUtamaModel menu ) {
		convertView = mInflater.inflate( R.layout.group_menu, parentView, false);
		if (convertView != null) {
			TextView labelView = (TextView) convertView.findViewById( R.id.group);
			labelView.setText(menu.menu);
		}
		return convertView;
	}
	private View getSectionView(View convertView, ViewGroup parentView, MenuUtamaModel menu) {
		convertView = mInflater.inflate( R.layout.item_menu_utama, parentView, false);
		if (convertView != null) {
			TextView labelView = (TextView) convertView.findViewById( R.id.text);
			labelView.setText(menu.menu);
			ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
			if(menu.drawable != null) {
				imageView.setImageDrawable(menu.drawable);
			}
		}
		return convertView;
	}

}
