package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderDetailAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<OrderDetailModel>> _listDataChild;

    public OrderDetailAdapter(Context context, List<GroupSKUModel> listDataHeader,
                              HashMap<GroupSKUModel, List<OrderDetailModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    public void addHeader(GroupSKUModel brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }

    public int indexOfHeader(GroupSKUModel brandModel) {
        return _listDataHeader.indexOf(brandModel);
    }

    public void addChild(GroupSKUModel brandModel, OrderDetailModel productModel) {
        List<OrderDetailModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<OrderDetailModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
        notifyDataSetChanged();
    }

    public void addChildNoNotify(GroupSKUModel brandModel, OrderDetailModel productModel) {
        List<OrderDetailModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<OrderDetailModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
    }

    public void addChild(GroupSKUModel brandModel, List<OrderDetailModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }

    @Override
    public OrderDetailModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final OrderDetailModel childText = (OrderDetailModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_item, null);
        }
        if (childText.Promo == 1) {
            convertView.findViewById(R.id.lbl_promo).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.lbl_promo).setVisibility(View.GONE);
        }
        if (childText.NewProduct == 1) {
            convertView.findViewById(R.id.lbl_new_product).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.lbl_new_product).setVisibility(View.GONE);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListItemJml);
        TextView SKUCODE = (TextView) convertView.findViewById(R.id.SKUCODE);
        txtListChild.setText(childText.skuModel.toString());
        SKUCODE.setText(childText.skuModel.SKUCODE);
        txtListChildJml.setText(String.valueOf(childText.QTY_B) + "," + String.valueOf(childText.QTY_K));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<OrderDetailModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_stok_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView lblListHeaderJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        lblListHeaderJml.setTypeface(null, Typeface.BOLD);
        int[] hitungJml = hitungJumlah(headerTitle);
        lblListHeaderJml.setText(String.valueOf(hitungJml[0]) + "," + String.valueOf(hitungJml[1]));
        return convertView;
    }

    public int[] hitungJumlah(GroupSKUModel headerTitle) {
        int[] jml = new int[2];
        jml[0] = 0;
        jml[1] = 0;
        List<OrderDetailModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (OrderDetailModel model : list) {
                jml[0] += model.QTY_B;
                jml[1] += model.QTY_K;
            }
        }
        return jml;
    }

    public int getTotalPerGoupSatuanKecil(GroupSKUModel headerTitle) {
        int jml = 0;
        List<OrderDetailModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (OrderDetailModel model : list) {
                jml = (model.QTY_B * (int) model.skuModel.CONVER) + model.QTY_K;
            }
        }
        return jml;
    }

    public int getTotalPerGoupSatuanBesar(GroupSKUModel headerTitle) {
        int jml = 0;
        List<OrderDetailModel> list = _listDataChild.get(headerTitle);
        if (list != null) {
            for (OrderDetailModel model : list) {
                jml += model.QTY_B + (int) (model.QTY_K / model.skuModel.CONVER);
            }
        }
        return jml;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(GroupSKUModel groupSKUModel) {
        _listDataHeader.remove(groupSKUModel);
        notifyDataSetChanged();
    }

    public void removeChild(GroupSKUModel brandModel) {
        List<OrderDetailModel> list = _listDataChild.get(brandModel);
        if (list != null) {
            _listDataChild.put(brandModel, null);
        }
        notifyDataSetChanged();
    }

    public List<OrderDetailModel> listOrderByGroup(GroupSKUModel brandModel) {
        return _listDataChild.get(brandModel);
    }

    public int getPosisionHeader(GroupSKUModel model) {
        return _listDataHeader.indexOf(model);
    }

    public boolean isOrder() {
        if (_listDataHeader != null && _listDataHeader.size() > 0) {
            for (GroupSKUModel groupSKUModel : _listDataHeader) {
                List<OrderDetailModel> data = _listDataChild.get(groupSKUModel);
                if (data != null) {
                    for (OrderDetailModel orderDetailModel : data) {
                        if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}