package com.bhn.sadix.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

//import com.google.firebase.crash.FirebaseCrash;

public class SummeryVisitAdapter extends BaseAdapter {
    private List<Object[]> mList;
    private LayoutInflater mInflater;
    private CtrlCustomer ctrlCustomer;
    //    private CtrlCommon ctrlCommon;
    private CtrlTakingOrder ctrlTakingOrder;
    private Context context;
    private RealmResults<CommonModel> Result;

    public SummeryVisitAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        mList = new ArrayList<Object[]>();
//        ctrlCommon = new CtrlCommon(context);
        ctrlCustomer = new CtrlCustomer(context);
        ctrlTakingOrder = new CtrlTakingOrder(context);
    }

    public int getCount() {
        return mList.size();
    }

    public Object[] getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Object[] menu = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_summery_visit, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.jml = (TextView) convertView.findViewById(R.id.jml);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.image.setImageDrawable((Drawable) menu[0]);
        holder.text.setText((String) menu[1]);
        holder.jml.setText((String) menu[2]);
        return convertView;
    }

    public void destroy() {
        try {
            Result.removeAllChangeListeners();
        } catch (RealmException e) {
//            FirebaseCrash.log("Checked - " + e.getMessage());
            e.printStackTrace();
        }
    }

    static class ViewHolder {
        TextView text;
        ImageView image;
        TextView jml;
    }

    public void loadData(final int day) {
        Calendar cal = Calendar.getInstance();
        int cDay = cal.get(Calendar.DAY_OF_WEEK);

        int dayCall = day - cDay;
        cal.add(Calendar.DAY_OF_MONTH, dayCall);

        final Realm realmO = Realm.getDefaultInstance();

        RealmChangeListener callback = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                if (Result != null && Result.isValid() && Result.isLoaded()) {
                    updateAdapter(day, realmO);
                }
            }
        };

        Result = realmO
                .where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("StatusID", 0)
                .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()) + "*")
                .equalTo("Done", 1)
                .findAllAsync();

        Result.addChangeListener(callback);
//        new Load(/*context, day*/).execute(day);
    }

    private void updateAdapter(int day, Realm realmO) {
        int effective_call = 0;
        int extra_call = 0;
        int call = 0;
        int invalid_call = 0;
        int schedule = 0;

        schedule = ctrlCustomer.countPerhari(day);

        for (com.bhn.sadix.Data.CommonModel commonModel : Result) {
            if (commonModel != null && commonModel.EndVisitTime != null) {
                CustomerModel menu = ctrlCustomer.get(commonModel.CustomerId);
                if (menu != null) {
                    boolean sesuai_jadwal = false;
                    if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D1") && menu.D1.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D2") && menu.D2.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D3") && menu.D3.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D4") && menu.D4.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D5") && menu.D5.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D6") && menu.D6.equals("true")) {
                        sesuai_jadwal = true;
                    } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D7") && menu.D7.equals("true")) {
                        sesuai_jadwal = true;
                    }

                    String CallST = Util.getStringPreference(context, "CallST");
                    int order = 0;

                    if (CallST.equals("21") || CallST.equals("22")) {
                        order = commonModel.takingOrderModel.size();
                    } else if (CallST.equals("43")) {
                        order = commonModel.assetModel.size();
                    } else if (CallST.equals("33")) {
                        order = commonModel.assetModel.size();
                    } else if (CallST.equals("37")) {
                        order = commonModel.assetMutasiModel.size();
                        Log.wtf("order 37", "check mutasi : " + order);
                        if (order == 0) {
                            order = commonModel.assetModel.where().equalTo("tipe", 4).findAll().size();

                            if (order == 0) {
                                order = commonModel.assetModel.where().equalTo("tipe", 3).findAll().size();
                            }
                            Log.wtf("order 37", "check asset service : " + order);
                        }
                    }

                    if (sesuai_jadwal && order > 0) {
                        if (menu.isNOO.equals("1")) {
                            extra_call++;
                        } else {
                            effective_call++;
                        }
                    } else if (!sesuai_jadwal && order > 0) {
                        extra_call++;
                    } else if (sesuai_jadwal && order == 0) {
                        call++;
                    } else if (!sesuai_jadwal && order == 0) {
                        invalid_call++;
                    }
                }
            }
        }
        realmO.close();

        mList.clear();
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.schedule),
                "Schedule",
                String.valueOf(schedule)
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.effective_call),
                "Effective Call",
                String.valueOf(effective_call)
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.extra_call),
                "Extra Call",
                String.valueOf(extra_call)
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.call),
                "Call",
                String.valueOf(call)
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.invalid_visited),
                "Invalid Visit",
                String.valueOf(invalid_call)
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.not_yet_visited),
                "Not yet Visited",
                String.valueOf(schedule - (effective_call + call))
        });
        mList.add(new Object[]{
                context.getResources().getDrawable(R.drawable.noo),
                "NOO",
                String.valueOf(ctrlCustomer.getCountNOO(day))
        });
        notifyDataSetChanged();
    }

    class Load extends AsyncTask<Integer, Void, String> {
        //        private Context context;
        private ProgressDialog progressDialog;
        private int day;
        private int effective_call = 0;
        private int extra_call = 0;
        private int call = 0;
        private int invalid_call = 0;
        private int schedule = 0;

        /*public Load(Context context, int day) {
            this.context = context;

        }*/
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Load Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Integer... params) {
            try {
                day = params[0];

                Realm realmO = Realm.getDefaultInstance();
                schedule = ctrlCustomer.countPerhari(day);

                Calendar cal = Calendar.getInstance();
                int cDay = cal.get(Calendar.DAY_OF_WEEK);

                int dayCall = day - cDay;
                cal.add(Calendar.DAY_OF_MONTH, dayCall);

//                List<CommonModel> list = ctrlCommon.listPerhari(day);
                Log.d("DATE", new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));

                RealmResults<com.bhn.sadix.Data.CommonModel> list = realmO
                        .where(com.bhn.sadix.Data.CommonModel.class)
                        .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()) + "*")
                        .equalTo("Done", 1)
                        .findAll();

                Log.wtf("list sum", list.size() + "");

                for (com.bhn.sadix.Data.CommonModel commonModel : list) {
                    if (commonModel != null && commonModel.EndVisitTime != null) {
                        CustomerModel menu = ctrlCustomer.get(commonModel.CustomerId);
                        if (menu != null) {
                            boolean sesuai_jadwal = false;
                            if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D1") && menu.D1.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D2") && menu.D2.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D3") && menu.D3.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D4") && menu.D4.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D5") && menu.D5.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D6") && menu.D6.equals("true")) {
                                sesuai_jadwal = true;
                            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D7") && menu.D7.equals("true")) {
                                sesuai_jadwal = true;
                            }

                            int order = realmO.where(TakingOrderModel.class).equalTo("CommonID", commonModel.CommonID).findAll().size()/*.ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID).size()*/;


                            if (sesuai_jadwal && order > 0) {
                                if (menu.isNOO.equals("1")) {
                                    extra_call++;
                                } else {
                                    effective_call++;
                                }
                            } else if (!sesuai_jadwal && order > 0) {
                                extra_call++;
                            } else if (sesuai_jadwal && order == 0) {
                                call++;
                            } else if (!sesuai_jadwal && order == 0) {
                                invalid_call++;
                            }
                        }
                    }
                }
                realmO.close();
                return null;
            } catch (Exception e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String respon) {
            mList.clear();
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.schedule),
                    "Schedule",
                    String.valueOf(schedule)
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.effective_call),
                    "Effective Call",
                    String.valueOf(effective_call)
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.extra_call),
                    "Extra Call",
                    String.valueOf(extra_call)
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.call),
                    "Call",
                    String.valueOf(call)
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.invalid_visited),
                    "Invalid Visit",
                    String.valueOf(invalid_call)
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.not_yet_visited),
                    "Not yet Visited",
                    String.valueOf(schedule - (effective_call + call))
            });
            mList.add(new Object[]{
                    context.getResources().getDrawable(R.drawable.noo),
                    "NOO",
                    String.valueOf(ctrlCustomer.getCountNOO(day))
            });
            notifyDataSetChanged();
            progressDialog.dismiss();
            if (respon != null) {
                Util.showDialogError(context, respon);
            }
        }
    }
}
