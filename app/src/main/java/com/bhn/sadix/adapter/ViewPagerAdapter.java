package com.bhn.sadix.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    private List<PageItem> list;
    private Context context;

    public ViewPagerAdapter(Context context) {
        this.context = context;
        list = new ArrayList<PageItem>();
    }

    public ViewPagerAdapter(Context context, List<PageItem> list) {
        this.context = context;
        this.list = list;
    }

    public void add(PageItem item) {
        list.add(item);
        notifyDataSetChanged();
    }

    public PageItem get(int index) {
        return list.get(index);
    }

    @Override
    public Object instantiateItem(View collection, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        PageItem page = list.get(position);
        View v = inflater.inflate(page.getLayout(), null);
        page.setView(v);
        page.onStart();
        ((ViewPager) collection).addView(v, 0);
        return v;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        PageItem page = list.get(position);
        page.onPause();
        ((ViewPager) collection).removeView((View) view);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public interface PageItem {
        public int getLayout();

        public void onStart();

        public void onPause();

        public void setView(View view);

        public View getView();
    }

}
