package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class CustomerAdapter extends BaseAdapter {
    private List<CustomerModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    //    private CtrlCommon ctrlCommon;
    private CtrlTakingOrder ctrlTakingOrder;
    private DatabaseHelper db;
    private Context context;
    private SparseArray<ViewHolder> holders;
//    RealmResults<CommonModel> all;
//    Realm realmUI;

    public CustomerAdapter(Context context, List<CustomerModel> mList/*, Realm realm*/) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
//        ctrlCommon = new CtrlCommon(context);
        ctrlTakingOrder = new CtrlTakingOrder(context);
        holders = new SparseArray<>();
//        realmUI = Realm.getDefaultInstance();
//        realmUI = realm;
        db = new DatabaseHelper(context);
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(CustomerModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(CustomerModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(CustomerModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return mList.size();
    }

    public CustomerModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final CustomerModel menu = getItem(position);
//        final CommonModel commonModel = ctrlCommon.getByCustomerAndDate(menu.CustId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        /*if (all.size() > 0)
            commonModel = all.last();*/

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_customer, null);
            holder = new ViewHolder();
            holder.kd_pelanggan = (TextView) convertView.findViewById(R.id.kd_pelanggan);
            holder.nm_pelanggan = (TextView) convertView.findViewById(R.id.nm_pelanggan);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.kd_pelanggan.setText(menu.CustomerAddress);
        holder.nm_pelanggan.setText(menu.CustomerName + " - " + menu.CustomerID);

        if (menu.isNew != null && menu.isNew.equals("1")) {
            holder.kd_pelanggan.setTextColor(Color.BLUE);
            holder.nm_pelanggan.setTextColor(Color.BLUE);
        } else {
            holder.kd_pelanggan.setTextColor(Color.BLACK);
            holder.nm_pelanggan.setTextColor(Color.BLACK);
        }

//		 if(menu.isNOO != null && menu.isNOO.equals("1")) {
//			 holder.image.setImageResource(R.drawable.logo_sadix);
//		 } else {
        holder.image.setImageResource(R.drawable.not_yet_visited);

        Realm realmO = Realm.getDefaultInstance();
//        final CommonModel[] commonModel = {null};
//        CommonModel commonModel = null;

        final ViewHolder finalHolder = holder;

        final RealmResults<CommonModel> all = realmO.where(CommonModel.class)
                .like("CustomerId", menu.CustId)
                .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                .equalTo("Done", 1).findAllAsync();

        RealmChangeListener listener = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                if (all != null && all.isValid() && all.isLoaded()) {
                    if (all.size() > 0) {
                        holders.put(position, finalHolder);
                        updateView(all.last(), position, menu);
                    }
                }
            }
        };

        all.addChangeListener(listener);

        Log.d("All Size", all.size() + "");

        /*realmO.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

            }
        });*/

        realmO.close();

        return convertView;
    }

    private void updateView(CommonModel commonModel, /*ViewHolder holder*/ int position, CustomerModel menu) {
        if (commonModel != null && commonModel.EndVisitTime != null) {
            boolean sesuai_jadwal = false;
            if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D1") && menu.D1.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D2") && menu.D2.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D3") && menu.D3.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D4") && menu.D4.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D5") && menu.D5.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D6") && menu.D6.equals("true")) {
                sesuai_jadwal = true;
            } else if (commonModel.D != null && commonModel.D.equalsIgnoreCase("D7") && menu.D7.equals("true")) {
                sesuai_jadwal = true;
            }

            ViewHolder holder = holders.get(position);

            String CallST = Util.getStringPreference(context, "CallST");
            android.util.Log.e("CallST", "" + CallST);
            int order = 0;

            if (CallST.equals("21") || CallST.equals("22")) {
                order = commonModel.takingOrderModel.size();
//                order[0] = realmO.where(TakingOrderModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID).size();
            } else if (CallST.equals("43")) {
                order = commonModel.assetModel.size();
//                order[0] = realmO.where(AssetModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = db.list("Aset", commonModel.CommonID).size();
            } else if (CallST.equals("33")) {
                order = commonModel.assetModel.size();
//                order[0] = realmO.where(AssetModel.class).equalTo("CommonID", commonModel[0].CommonID).findAll().size();
//                order = db.list("Aset", commonModel.CommonID).size();
            } else if (CallST.equals("37")) {
                order = commonModel.assetMutasiModel.size();
                Log.wtf("order 37", "check mutasi : " + order);
                if (order == 0) {
                    order = commonModel.assetModel.where().equalTo("tipe", 4).findAll().size();
                    Log.wtf("order 37", "check asset service : " + order);
                }
            }

            Log.d("cust value", menu.CustId + "-" + commonModel.CommonID + "-" + commonModel.D + "-" + sesuai_jadwal + "-" + order);

            if (sesuai_jadwal && order > 0) {
                if (menu.isNOO.equals("1")) {
                    holder.image.setImageResource(R.drawable.extra_call);
                } else {
                    holder.image.setImageResource(R.drawable.effective_call);
                }
            } else if (!sesuai_jadwal && order > 0) {
                holder.image.setImageResource(R.drawable.extra_call);
            } else if (sesuai_jadwal && order == 0) {
                holder.image.setImageResource(R.drawable.call);
            } else if (!sesuai_jadwal && order == 0) {
                holder.image.setImageResource(R.drawable.invalid_visited);
            }
        }
    }

    static class ViewHolder {
        ImageView image;
        TextView kd_pelanggan;
        TextView nm_pelanggan;
    }

}
