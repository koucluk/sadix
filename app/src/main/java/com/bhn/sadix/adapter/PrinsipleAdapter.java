package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.R;
import com.bhn.sadix.model.PrinsipleModel;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;

import io.realm.Realm;

public class PrinsipleAdapter extends BaseAdapter {
    private ArrayList<PrincipleModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private Context context;

    public PrinsipleAdapter(Context context, ArrayList<PrincipleModel> mList) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    /*public void remove(PrinsipleModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }*/

    /*public void insert(PrinsipleModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }*/

    public void add(PrincipleModel model) {
        Realm.getDefaultInstance().copyToRealmOrUpdate(model);
        if (mList == null) {
            mList = new ArrayList<>();
        }
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    /*public void set(PrinsipleModel model, int index) {
        mList.set(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }*/

    public int indexOf(PrinsipleModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        if (mList != null) return mList.size();
        else return 0;
    }

    public PrincipleModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final PrincipleModel menu = (PrincipleModel) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_prinsiple_taking_order3, null);
            holder = new ViewHolder();
            holder.PrinsipleName = (TextView) convertView.findViewById(R.id.PrinsipleName);
            holder.total_qty = (TextView) convertView.findViewById(R.id.total_qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.discount = (TextView) convertView.findViewById(R.id.discount);
            holder.total_price = (TextView) convertView.findViewById(R.id.total_price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.PrinsipleName.setText(menu.PrinsipleName);
        holder.total_qty.setText(Util.numberFormat.format(menu.total_qty_b) + "," + Util.numberFormat.format(menu.total_qty_k));
        holder.price.setText(Util.numberFormat.format(menu.price));
        holder.discount.setText(Util.numberFormat.format(menu.discount));
        holder.total_price.setText(Util.numberFormat.format(menu.total_price));
//		 menu.total_qty_b = 0;
//		 menu.total_qty_k = 0;
//		 menu.price = 0;
//		 if(menu.list != null && menu.list.size() > 0) {
//			 for (TakingOrderModel order : menu.list) {
//				
//			}
//		 }
        return convertView;
    }

    static class ViewHolder {
        TextView PrinsipleName;
        TextView total_qty;
        TextView price;
        TextView discount;
        TextView total_price;
    }

}
