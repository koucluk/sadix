package com.bhn.sadix.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.LogDataDetail;
import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.Data.StockCompetitorModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

//import com.bhn.sadix.model.CommonModel;

public abstract class LogDataRealmAdapter extends BaseExpandableListAdapter {

    private Context _context;
    //    private List<CommonModel> _listDataHeader; // header titles
    private List<CommonModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<CommonModel, List<LogDataDetail>> _listDataChild;
    private CtrlCustomer ctrlCustomer;
    private CtrlSKU ctrlSKU;
    private CtrlSKUCom ctrlSKUCom;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;
    Realm realmUI;

    public LogDataRealmAdapter(Context context, List<CommonModel> listDataHeader,
                               HashMap<CommonModel, List<LogDataDetail>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        ctrlCustomer = new CtrlCustomer(_context);
        ctrlSKU = new CtrlSKU(_context);
        ctrlConfig = new CtrlConfig(_context);
        ctrlSKUCom = new CtrlSKUCom(_context);
        ctrlAppModul = new CtrlAppModul(_context);
        realmUI = Realm.getDefaultInstance();
    }

    public void addHeader(CommonModel historyModel) {
        _listDataHeader.add(historyModel);
        notifyDataSetChanged();
    }

    public void addHeader(String CommonID) {
        CommonModel model = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();

        CommonModel historyModel = realmUI.copyFromRealm(model);
        _listDataHeader.add(historyModel);

        notifyDataSetChanged();
    }

    public void addChild(CommonModel historyModel, LogDataDetail historyDetailModel) {
        List<LogDataDetail> list = _listDataChild.get(historyModel);
        if (list == null) {
            list = new ArrayList<>();
            _listDataChild.put(historyModel, list);
        }
        list.add(historyDetailModel);
        notifyDataSetChanged();
    }

    public void addChild(String CommonID/*, LogDataDetail historyDetailModel*/) {
        CommonModel model = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();

        RealmList<TakingOrderModel> listOrder = model.takingOrderModel;

        Log.d("listOrder on Log", listOrder.size() + "");

        RealmList<StockCustomerModel> listStockOutlet = model.stockCustomerModel;
        Log.d("listStock on Log", listStockOutlet.size() + "");
        RealmList<com.bhn.sadix.Data.StockCompetitorModel> listStockCompetitor = model.stockCompetitorModel;
        Log.d("listStockC on Log", listStockCompetitor.size() + "");
        RealmList<ReturnModel> listRetur = model.returnModel;
        Log.d("listRetur on Log", listRetur.size() + "");
        RealmList<CollectionModel> collectorModel = model.collectionModel;
        Log.d("listCollect on Log", collectorModel.size() + "");
        RealmList<PriceMonitoringModel> priceMonitoring = model.priceMonitoringModel;
        Log.d("priceMonitoring on Log", priceMonitoring.size() + "");

        LogDataDetail historyDetailModel = new LogDataDetail(listStockOutlet, listStockCompetitor, listOrder, listRetur, collectorModel, priceMonitoring);

        CommonModel historyModel = realmUI.copyFromRealm(model);

        List<LogDataDetail> list = _listDataChild.get(historyModel);
        if (list == null) {
            list = new ArrayList<>();
            list.add(historyDetailModel);
            _listDataChild.put(historyModel, list);
        } else {
            list.add(historyDetailModel);
            _listDataChild.put(historyModel, list);
        }
        notifyDataSetChanged();
    }

    public void addChild(CommonModel historyModel, List<LogDataDetail> list) {
        _listDataChild.put(historyModel, list);
        notifyDataSetChanged();
    }

    @Override
    public LogDataDetail getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LogDataDetail childText = getChild(groupPosition, childPosition);
        Log.wtf("getChildView", "Called");
        if (convertView == null) {
            convertView = infalInflater.inflate(R.layout.item_log_data_detail, null);
        }
        TextView orders = (TextView) convertView.findViewById(R.id.orders);
        orders.setVisibility(ctrlAppModul.isModul("21") || ctrlAppModul.isModul("22") ? View.VISIBLE : View.GONE);
        final View detail_order = convertView.findViewById(R.id.detail_order);
        ((LinearLayout) detail_order).removeAllViews();
        for (TakingOrderModel takingOrderModel : childText.takingOrderModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(takingOrderModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(takingOrderModel.QTY_B) + "," + String.valueOf(takingOrderModel.QTY_K));
            ((LinearLayout) detail_order).addView(layout);
        }
        orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_order.getVisibility() == View.GONE) {
                    detail_order.setVisibility(View.VISIBLE);
                } else {
                    detail_order.setVisibility(View.GONE);
                }
            }
        });
        TextView return_sku = (TextView) convertView.findViewById(R.id.return_sku);
        return_sku.setVisibility(ctrlAppModul.isModul("24") ? View.VISIBLE : View.GONE);
        final View detail_return = convertView.findViewById(R.id.detail_return);
        ((LinearLayout) detail_return).removeAllViews();
        for (ReturnModel returModel : childText.returModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(returModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(returModel.QTY_B) + "," + String.valueOf(returModel.QTY_K));
            ((LinearLayout) detail_return).addView(layout);
        }
        return_sku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_return.getVisibility() == View.GONE) {
                    detail_return.setVisibility(View.VISIBLE);
                } else {
                    detail_return.setVisibility(View.GONE);
                }
            }
        });
        TextView stok = (TextView) convertView.findViewById(R.id.stok);
        stok.setVisibility(ctrlAppModul.isModul("19") ? View.VISIBLE : View.GONE);
        final View detail_stok = convertView.findViewById(R.id.detail_stok);
        ((LinearLayout) detail_stok).removeAllViews();
        for (StockCustomerModel stockOutletModel : childText.stockOutletModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUModel model = ctrlSKU.get(stockOutletModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(stockOutletModel.QTY_B) + "," + String.valueOf(stockOutletModel.QTY_K));
            ((LinearLayout) detail_stok).addView(layout);
        }
        stok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_stok.getVisibility() == View.GONE) {
                    detail_stok.setVisibility(View.VISIBLE);
                } else {
                    detail_stok.setVisibility(View.GONE);
                }
            }
        });
        TextView stok_kompetitor = (TextView) convertView.findViewById(R.id.stok_kompetitor);
        stok_kompetitor.setVisibility(ctrlAppModul.isModul("20") ? View.VISIBLE : View.GONE);
        final View detail_stok_kompetitor = convertView.findViewById(R.id.detail_stok_kompetitor);
        ((LinearLayout) detail_stok_kompetitor).removeAllViews();
        for (StockCompetitorModel stockCompetitorModel : childText.stockCompetitorModel) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_detail_item, null);
            TextView lblListHeader = (TextView) layout.findViewById(R.id.lblListHeader);
            TextView lblListHeaderJml = (TextView) layout.findViewById(R.id.lblListHeaderJml);
            SKUComModel model = ctrlSKUCom.get(stockCompetitorModel.SKUId);
            if (model != null) {
                lblListHeader.setText(model.ProductName);
            }
            lblListHeaderJml.setText(String.valueOf(stockCompetitorModel.QTY_B) + "," + String.valueOf(stockCompetitorModel.QTY_K));
            ((LinearLayout) detail_stok_kompetitor).addView(layout);
        }
        stok_kompetitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_stok_kompetitor.getVisibility() == View.GONE) {
                    detail_stok_kompetitor.setVisibility(View.VISIBLE);
                } else {
                    detail_stok_kompetitor.setVisibility(View.GONE);
                }
            }
        });
        TextView collection = (TextView) convertView.findViewById(R.id.collection);
        collection.setVisibility(ctrlAppModul.isModul("18") ? View.VISIBLE : View.GONE);
        final View detail_collection = convertView.findViewById(R.id.detail_collection);
        collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (childText.collectorModel != null) {
                    if (detail_collection.getVisibility() == View.GONE) {
                        detail_collection.setVisibility(View.VISIBLE);
                    } else {
                        detail_collection.setVisibility(View.GONE);
                    }
                }
            }
        });
        if (childText.collectorModel != null) {
//        	if(childText.collectorModel.status.equals("1")) { //pay
//        		((TextView)detail_collection.findViewById(R.id.lbl1)).setText("Today Pay :");
//        		((TextView)detail_collection.findViewById(R.id.data1)).setText(childText.collectorModel.data1);
//        		((TextView)detail_collection.findViewById(R.id.lbl2)).setText("Payment Method :");
//        		ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("PAYMENT_METHOD", childText.collectorModel.data2);
//        		((TextView)detail_collection.findViewById(R.id.data2)).setText(combo.text);
//        		((TextView)detail_collection.findViewById(R.id.lbl3)).setText("Note :");
//        		((TextView)detail_collection.findViewById(R.id.data3)).setText(childText.collectorModel.data3);
//        	} else if(childText.collectorModel.status.equals("2")) { //not pay
//        		((TextView)detail_collection.findViewById(R.id.lbl1)).setText("Not Payment Reason :");
//        		ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("NO_PAYMENT_REASON", childText.collectorModel.data1);
//        		((TextView)detail_collection.findViewById(R.id.data1)).setText(combo.text);
//        		((TextView)detail_collection.findViewById(R.id.lbl2)).setText("Promise Date :");
//        		((TextView)detail_collection.findViewById(R.id.data2)).setText(childText.collectorModel.data2);
//        		((TextView)detail_collection.findViewById(R.id.lbl3)).setText("Note :");
//        		((TextView)detail_collection.findViewById(R.id.data3)).setText(childText.collectorModel.data3);
//        	}
            ((LinearLayout) detail_collection).removeAllViews();
            for (CollectionModel model : childText.collectorModel) {
                LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log__data_collection, null);
                if (model.status.equals("1")) { //pay
                    ((TextView) layout.findViewById(R.id.lbl1)).setText("Today Pay :");
                    ((TextView) layout.findViewById(R.id.data1)).setText(model.data1);
                    ((TextView) layout.findViewById(R.id.lbl2)).setText("Payment Method :");
                    ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("PAYMENT_METHOD", model.data2);
                    ((TextView) layout.findViewById(R.id.data2)).setText(combo.text);
                    ((TextView) layout.findViewById(R.id.lbl3)).setText("Note :");
                    ((TextView) layout.findViewById(R.id.data3)).setText(model.data3);
                } else if (model.status.equals("2")) { //not pay
                    ((TextView) layout.findViewById(R.id.lbl1)).setText("Not Payment Reason :");
                    ComboBoxModel combo = ctrlConfig.getByConfigClassAndConfigId("NO_PAYMENT_REASON", model.data1);
                    ((TextView) layout.findViewById(R.id.data1)).setText(combo.text);
                    ((TextView) layout.findViewById(R.id.lbl2)).setText("Promise Date :");
                    ((TextView) layout.findViewById(R.id.data2)).setText(model.data2);
                    ((TextView) layout.findViewById(R.id.lbl3)).setText("Note :");
                    ((TextView) layout.findViewById(R.id.data3)).setText(model.data3);
                }
                ((LinearLayout) detail_collection).addView(layout);
            }
        }
        TextView price_monitoring = (TextView) convertView.findViewById(R.id.price_monitoring);
        price_monitoring.setVisibility(ctrlAppModul.isModul("42") ? View.VISIBLE : View.GONE);
        final View detail_price_monitoring = convertView.findViewById(R.id.detail_price_monitoring);
        price_monitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detail_price_monitoring.getVisibility() == View.GONE) {
                    detail_price_monitoring.setVisibility(View.VISIBLE);
                } else {
                    detail_price_monitoring.setVisibility(View.GONE);
                }
            }
        });
        ((LinearLayout) detail_price_monitoring).removeAllViews();
        for (PriceMonitoringModel model : childText.listPriceMonitoring) {
            LinearLayout layout = (LinearLayout) infalInflater.inflate(R.layout.item_log_price_monitoring, null);
            try {
                android.util.Log.e("model", model.toString());
                if (model.ProductID == 1) {
                    layout.findViewById(R.id.LevelID).setVisibility(View.VISIBLE);
                    ((TextView) layout.findViewById(R.id.SKUId)).setText("Level : " + String.valueOf(model.LevelID));
                    SKUModel sku = ctrlSKU.get(model.SKUId);
                    ((TextView) layout.findViewById(R.id.SKUId)).setText(sku.ProductName);
                } else {
                    layout.findViewById(R.id.LevelID).setVisibility(View.GONE);
                    SKUComModel sku = ctrlSKUCom.get(model.SKUId);
                    ((TextView) layout.findViewById(R.id.SKUId)).setText(sku.ProductName);
                }
                ((TextView) layout.findViewById(R.id.BuyPrice)).setText("Buy Price : " + Util.numberFormat.format(model.BuyPrice));
                ((TextView) layout.findViewById(R.id.SellPrice)).setText("Sell Price : " + Util.numberFormat.format(model.SellPrice));
                ((TextView) layout.findViewById(R.id.Qty)).setText("Qty: " + Util.NUMBER_FORMAT.format(model.Qty));
                ((TextView) layout.findViewById(R.id.Note)).setText("Note : " + model.Note);
                ((LinearLayout) detail_price_monitoring).addView(layout);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<LogDataDetail> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public CommonModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final CommonModel headerTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_log_data, null);
        }
        TextView BeginVisitTime = (TextView) convertView.findViewById(R.id.BeginVisitTime);
        TextView CustName = (TextView) convertView.findViewById(R.id.CustName);

        if (headerTitle.Status != null && headerTitle.Status.equals("0")) {
            BeginVisitTime.setTextColor(_context.getResources().getColor(R.color.red));
            CustName.setTextColor(_context.getResources().getColor(R.color.red));
        } else {
            BeginVisitTime.setTextColor(_context.getResources().getColor(R.color.black));
            CustName.setTextColor(_context.getResources().getColor(R.color.black));
        }

        ImageButton btn_send = (ImageButton) convertView.findViewById(R.id.btn_send);
        BeginVisitTime.setTypeface(null, Typeface.BOLD);
        BeginVisitTime.setText(headerTitle.BeginVisitTime);

        final CustomerModel customerModel = ctrlCustomer.get(headerTitle.CustomerId);

        if (customerModel != null) {
            if (customerModel.isEdit.equals("1")) {
                CustName.setText(customerModel.CustomerName + "*");
            } else {
                CustName.setText(customerModel.CustomerName);
            }
        }
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!customerModel.isEdit.equals("1")) {
                    send(headerTitle);
                } else {
                    Toast.makeText(_context, "Data Customer belum terkirim!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return convertView;
    }

    private void send(final CommonModel headerTitle) {
        if (headerTitle.Status != null && headerTitle.Status.equals("1")) {
            Util.confirmDialog(_context, "Info", "Data sudah terkirim, Apakah ingin mengirim lagi?", new ConfirmListener() {
                @Override
                public void onDialogCompleted(boolean answer) {
                    if (answer) {
                        new LoadData(/*_context, headerTitle*/).execute(headerTitle.CommonID);
                    }
                }
            });
        } else {
            new LoadData(/*_context, headerTitle*/).execute(headerTitle.CommonID);
        }
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(CommonModel commonModel) {
        _listDataHeader.remove(commonModel);
        notifyDataSetChanged();
    }

    public abstract void berhasilKirim();

    private class LoadData extends AsyncTask<String, Void, String> {
        private CommonModel commonModel;
        private ProgressDialog progressDialog;

        /*public LoadData(Context context, CommonModel commonModel) {
            this.commonModel = commonModel;
        }*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(_context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Log.d("Param 0", params[0]);
            return Util.sendUlang(_context, params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result == null) {
                berhasilKirim();
                Util.showDialogInfo(_context, "Data berhasil dikirim");
            } else {
//				Util.showDialogError(_context, result);
                Util.infoErrorKirimLog(_context, result);
            }
            super.onPostExecute(result);
        }
    }
}