package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.MenuUtamaModel;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuUtamaAdapter extends BaseAdapter {
	private List<MenuUtamaModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	public MenuUtamaAdapter(Context context, List<MenuUtamaModel> mList) {
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(MenuUtamaModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(MenuUtamaModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(MenuUtamaModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int getCount() {
		return mList.size();
	}

	public MenuUtamaModel getItem(int position) {
		return mList.get(position);
	}
	public int getPosition(MenuUtamaModel menu) {
		return mList.indexOf(menu);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		MenuUtamaModel menu = (MenuUtamaModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_menu_utama, null);
			 holder = new ViewHolder();
			 holder.text = (TextView) convertView.findViewById(R.id.text);
			 holder.image = (ImageView) convertView.findViewById(R.id.image);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 } 
		 if(menu.isCaption) {
			 holder.text.setText(menu.caption);
		 } else {
			 holder.text.setText(menu.menu);
		 }
		 if(menu.color != -1) {
			 holder.text.setTextColor(menu.color);
		 }
		 if(menu.drawable != null) {
			 holder.image.setVisibility(View.VISIBLE);
			 holder.image.setImageDrawable(menu.drawable);
		 } else {
			 holder.image.setVisibility(View.GONE);
		 }
		 return convertView;
	}
	
	static class ViewHolder {
		TextView text;
		ImageView image;
	}

}
