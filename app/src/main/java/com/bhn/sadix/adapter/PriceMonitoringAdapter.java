package com.bhn.sadix.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

//import com.bhn.sadix.model.PriceMonitoringModel;

public class PriceMonitoringAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<Object> _listDataHeader; // header titles
    // child data in format of header title, child title
//    private HashMap<Object, List<PriceMonitoringModel>> _listDataChild;
    private HashMap<Object, List<PriceMonitoringModel>> _listDataChild;
    Realm realm;

    public PriceMonitoringAdapter(Context context, List<Object> listDataHeader,
                                  HashMap<Object, List<PriceMonitoringModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        realm = Realm.getDefaultInstance();
    }

    public void addHeader(Object brandModel) {
        _listDataHeader.add(brandModel);
        notifyDataSetChanged();
    }

    public void addHeaderNoNotifay(Object brandModel) {
        _listDataHeader.add(brandModel);
    }

    public void addChild(Object brandModel, PriceMonitoringModel productModel) {
        realm = Realm.getDefaultInstance();
        PriceMonitoringModel model = realm.copyFromRealm(productModel);
        List<PriceMonitoringModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<>();
            _listDataChild.put(brandModel, list);
        }
        list.add(model);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(Object brandModel, PriceMonitoringModel productModel) {
        List<PriceMonitoringModel> list = _listDataChild.get(brandModel);
        if (list == null) {
            list = new ArrayList<PriceMonitoringModel>();
            _listDataChild.put(brandModel, list);
        }
        list.add(productModel);
    }

    public void addChild(Object brandModel, List<PriceMonitoringModel> list) {
        _listDataChild.put(brandModel, list);
        notifyDataSetChanged();
    }

    public void addChildNoNotifay(Object brandModel, List<PriceMonitoringModel> list) {
        _listDataChild.put(brandModel, list);
    }

    @Override
    public PriceMonitoringModel getChild(int groupPosition, int childPosititon) {
        /*Realm realm = Realm.getDefaultInstance();
        String SkuID = this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon).SKUId;
        PriceMonitoringModel pm = realm.where(PriceMonitoringModel.class).equalTo("SKUId", SkuID).findFirst();
        PriceMonitoringModel fPm = realm.copyFromRealm(pm);*/
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
//        return fPm /*realm.copyFromRealm(this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon))*/;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final PriceMonitoringModel childText = getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_capasity_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        if (childText.ProductID == 1) {
            txtListChild.setText(childText.ProductName);
        } else if (childText.ProductID == 0) {
            txtListChild.setText(childText.ProductName);
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<PriceMonitoringModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Object headerTitle = (Object) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_capasity_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void removeHeader(Object groupSKUModel) {
        _listDataHeader.remove(groupSKUModel);
        notifyDataSetChanged();
    }

    public void removeChild(Object brandModel) {
        List<PriceMonitoringModel> list = _listDataChild.get(brandModel);
        if (list != null) {
            _listDataChild.put(brandModel, null);
        }
        notifyDataSetChanged();
    }

    public List<PriceMonitoringModel> listOrderByGroup(Object brandModel) {
        return _listDataChild.get(brandModel);
    }

    public int getPosisionHeader(Object groupSKUModel) {
        return _listDataHeader.indexOf(groupSKUModel);
    }
}