package com.bhn.sadix.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.CrcOrderStokModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.util.Util;
 
public class CrcOrderAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<GroupSKUModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<GroupSKUModel, List<CrcOrderStokModel>> _listDataChild;
    private CrcOrderListener listener;
 
    public CrcOrderAdapter(Context context, List<GroupSKUModel> listDataHeader,
            HashMap<GroupSKUModel, List<CrcOrderStokModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }
    
    public void addCrcOrderListener(CrcOrderListener listener) {
    	this.listener = listener;
    }
    
    public void addHeader(GroupSKUModel brandModel) {
    	_listDataHeader.add(brandModel);
    	notifyDataSetChanged();
    }
    
    public void addChild(GroupSKUModel brandModel, CrcOrderStokModel productModel) {
    	List<CrcOrderStokModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<CrcOrderStokModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    	notifyDataSetChanged();
    }
    
    public void addChildNoNotify(GroupSKUModel brandModel, CrcOrderStokModel productModel) {
    	List<CrcOrderStokModel> list = _listDataChild.get(brandModel);
    	if(list == null) {
    		list = new ArrayList<CrcOrderStokModel>();
    		_listDataChild.put(brandModel, list);
    	}
    	list.add(productModel);
    }
    
    public void addChild(GroupSKUModel brandModel, List<CrcOrderStokModel> list) {
		_listDataChild.put(brandModel, list);
    	notifyDataSetChanged();
    }
 
    @Override
    public CrcOrderStokModel getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CrcOrderStokModel childText = (CrcOrderStokModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_crc_item, null);
        }
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView order_last = (TextView) convertView.findViewById(R.id.order_last);
        TextView stock_last = (TextView) convertView.findViewById(R.id.stock_last);
        TextView order_current = (TextView) convertView.findViewById(R.id.order_current);
        TextView stock_current = (TextView) convertView.findViewById(R.id.stock_current);
        TextView lbl_last = (TextView) convertView.findViewById(R.id.lbl_last);

        TextView ReasonOrder = (TextView) convertView.findViewById(R.id.ReasonOrder);
        TextView ReasonStock = (TextView) convertView.findViewById(R.id.ReasonStock);
        
        ReasonOrder.setText(childText.ReasonOrder);
        ReasonStock.setText(childText.ReasonStock);
        
        txtListChild.setText(childText.skuModel.toString());
        if(childText.lastOrder != null) {
        	order_last.setText(Util.NUMBER_FORMAT.format(childText.lastOrder.QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.lastOrder.QTY));
        	lbl_last.setText(childText.lastOrder.POSTDATE);
        }
        if(childText.lastStock != null) {
        	stock_last.setText(Util.NUMBER_FORMAT.format(childText.lastStock.QTY_L) + "," + Util.NUMBER_FORMAT.format(childText.lastStock.QTY));
        }
        if(childText.order != null) {
        	if(childText.order.QTY_B > 0 || childText.order.QTY_K > 0) {
        		order_current.setTextColor(Color.BLACK);
        		order_current.setText(Util.NUMBER_FORMAT.format(childText.order.QTY_B) + "," + Util.NUMBER_FORMAT.format(childText.order.QTY_K));
        	} else {
        		order_current.setText(Util.NUMBER_FORMAT.format(childText.QTY_B_REK) + "," + Util.NUMBER_FORMAT.format(childText.QTY_K_REK));
        		order_current.setTextColor(Color.RED);
        	}
        }
        if(childText.stock != null) {
        	stock_current.setText(Util.NUMBER_FORMAT.format(childText.stock.QTY_B) + "," + Util.NUMBER_FORMAT.format(childText.stock.QTY_K));
        }
        order_current.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(listener != null) {
					listener.order(childText);
				}
			}
		});
        stock_current.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(listener != null) {
					listener.stock(childText);
				}
			}
		});
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
    	List<CrcOrderStokModel> data = this._listDataChild.get(this._listDataHeader.get(groupPosition));
        return ((data != null && data.size() > 0) ? data.size() : 0);
    }
 
    @Override
    public GroupSKUModel getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupSKUModel headerTitle = (GroupSKUModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_crc_group, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        TextView txtListChildJml = (TextView) convertView.findViewById(R.id.lblListHeaderJml);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.toString());
        txtListChildJml.setTypeface(null, Typeface.BOLD);
        int[] hitungJml = hitungJumlah(headerTitle);
        txtListChildJml.setText(String.valueOf(hitungJml[0])+","+String.valueOf(hitungJml[1]));
        return convertView;
    }
 
    public int[] hitungJumlah(GroupSKUModel headerTitle) {
    	int[] jml = new int[2];
    	jml[0] = 0;
    	jml[1] = 0;
    	List<CrcOrderStokModel> list = _listDataChild.get(headerTitle);
    	if(list != null) {
	    	for (CrcOrderStokModel model : list) {
				jml[0] += model.QTY_B_REK;
				jml[1] += model.QTY_K_REK;
			}
    	}
		return jml;
	}
 
//    public int[] hitungJumlah(GroupSKUModel headerTitle) {
//    	int[] jml = new int[2];
//    	jml[0] = 0;
//    	jml[1] = 0;
//    	List<CrcOrderStokModel> list = _listDataChild.get(headerTitle);
//    	if(list != null) {
//	    	for (CrcOrderStokModel model : list) {
//				jml[0] += model.QTY_B;
//				jml[1] += model.QTY_K;
//			}
//    	}
//		return jml;
//	}
// 
//    public int getTotalPerGoupSatuanKecil(GroupSKUModel headerTitle) {
//    	int jml = 0;
//    	List<CrcOrderStokModel> list = _listDataChild.get(headerTitle);
//    	if(list != null) {
//	    	for (CrcOrderStokModel model : list) {
//	    		jml = (model.QTY_B * (int)model.skuModel.CONVER) + model.QTY_K;
//			}
//    	}
//		return jml;
//	}
//    public int getTotalPerGoupSatuanBesar(GroupSKUModel headerTitle) {
//    	int jml = 0;
//    	List<CrcOrderStokModel> list = _listDataChild.get(headerTitle);
//    	if(list != null) {
//	    	for (CrcOrderStokModel model : list) {
//	    		jml += model.QTY_B + (int)(model.QTY_K/model.skuModel.CONVER);
//			}
//    	}
//		return jml;
//	}

	@Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

	public void removeHeader(GroupSKUModel groupSKUModel) {
    	_listDataHeader.remove(groupSKUModel);
    	notifyDataSetChanged();
	}
    
    public void removeChild(GroupSKUModel brandModel) {
    	List<CrcOrderStokModel> list = _listDataChild.get(brandModel);
    	if(list != null) {
    		_listDataChild.put(brandModel, null);
    	}
    	notifyDataSetChanged();
    }
    
    public List<CrcOrderStokModel> listOrderByGroup(GroupSKUModel brandModel) {
    	return _listDataChild.get(brandModel);
    }
    
    public int getPosisionHeader(GroupSKUModel model) {
    	return _listDataHeader.indexOf(model);
    }
    public interface CrcOrderListener {
    	public void order(CrcOrderStokModel model);
    	public void stock(CrcOrderStokModel model);
    }
}