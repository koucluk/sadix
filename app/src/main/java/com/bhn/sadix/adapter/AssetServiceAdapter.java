package com.bhn.sadix.adapter;

import java.util.List;

import com.bhn.sadix.R;
import com.bhn.sadix.model.AssetServiceModel;



import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AssetServiceAdapter extends BaseAdapter {
	private List<AssetServiceModel> mList;
	private LayoutInflater mInflater;
	private boolean mNotifyOnChange;
	private Context context;
	public AssetServiceAdapter(Context context, List<AssetServiceModel> mList) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.mList = mList;
	}
	public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }
	public void clear() {
		mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void remove(AssetServiceModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }
	public void insert(AssetServiceModel model, int index) {
		mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public void add(AssetServiceModel model) {
		mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }
	public int indexOf(AssetServiceModel model) {
		return mList.indexOf(model);
    }
	public int getCount() {
		return mList.size();
	}

	public AssetServiceModel getItem(int position) {
		return mList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		final AssetServiceModel menu = (AssetServiceModel) getItem(position);
		if (convertView == null) {
			 convertView = mInflater.inflate(R.layout.item_asset, null);
			 holder = new ViewHolder();
			 holder.ASETNAME = (TextView) convertView.findViewById(R.id.ASETNAME);
			 holder.ASETNO = (TextView) convertView.findViewById(R.id.ASETNO);
			 holder.btndelete = (ImageView) convertView.findViewById(R.id.btndelete);
			 convertView.setTag(holder);
		 } else {
			 holder = (ViewHolder) convertView.getTag();
		 }
		 holder.btndelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.confirmDialog(context, "INFO", "Anda yakin akan menghapus data?", new ConfirmListener() {
					@Override
					public void onDialogCompleted(boolean answer) {
						if(answer) {
							remove(menu);
						}
					}
				});
			}
		 });
		 holder.ASETNAME.setText(menu.ASETNAME);
		 holder.ASETNO.setText(menu.ASETNO);
		 return convertView;
	}
	
	static class ViewHolder {
		TextView ASETNAME;
		TextView ASETNO;
		ImageView btndelete;
	}

}
