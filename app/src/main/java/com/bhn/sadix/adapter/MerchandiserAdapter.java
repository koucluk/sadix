package com.bhn.sadix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.MerchandiserModel;

import java.util.List;

public class MerchandiserAdapter extends BaseAdapter {
    private List<MerchandiserModel> mList;
    private LayoutInflater mInflater;
    private boolean mNotifyOnChange;
    private Context context;

    public MerchandiserAdapter(Context context, List<MerchandiserModel> mList) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void clear() {
        mList.clear();
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void remove(MerchandiserModel model) {
        if (mList.remove(model)) {
            if (mNotifyOnChange) {
                notifyDataSetChanged();
            }
        }
    }

    public void insert(MerchandiserModel model, int index) {
        mList.add(index, model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public void add(MerchandiserModel model) {
        mList.add(model);
        if (mNotifyOnChange) {
            notifyDataSetChanged();
        }
    }

    public int indexOf(MerchandiserModel model) {
        return mList.indexOf(model);
    }

    public int getCount() {
        return mList.size();
    }

    public MerchandiserModel getItem(int position) {
        return mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final MerchandiserModel menu = (MerchandiserModel) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_merchandiser, null);
            holder = new ViewHolder();
            holder.NAME = (TextView) convertView.findViewById(R.id.NAME);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.NAME.setText(menu.NAME);
        return convertView;
    }

    static class ViewHolder {
        TextView NAME;
    }

}
