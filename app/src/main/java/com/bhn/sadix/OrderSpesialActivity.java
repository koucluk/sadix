package com.bhn.sadix;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.adapter.DiskonAdapter;
import com.bhn.sadix.adapter.DiskonPromoAdapter;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.SalesProgramAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCrcOrder;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSalesProgram;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbDiskonHelper;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CrcOrderModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.DiskonPromoModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.SalesProgramModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

public class OrderSpesialActivity extends AppCompatActivity implements DiskonAdapter.CheckedChangeListener {
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;

    private OrderDetailAdapter adapter = null;
    private OrderDetailAdapter adapterOrder = null;
    private OrderDetailAdapter adapterFocus = null;
    private OrderDetailAdapter adapterPromo = null;
    private OrderDetailAdapter adapterSearch = null;
    private DiskonAdapter diskonAdapter = null;
    private DiskonPromoAdapter diskonPromoAdapter;

    private CtrlAppModul ctrlAppModul;
    private CtrlCustomerType ctrlCustomerType;
    private CtrlSalesProgram ctrlSalesProgram;
    private CtrlSKU ctrlSKU;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlCrcOrder ctrlCrcOrder;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlConfig ctrlConfig;
    private DbDiskonHelper dbDiskonHelper;

    Realm realmUI;
    CommonModel commonModel;

    //    private CommonModel commonModel = new CommonModel();
    private CustomerTypeModel customerTypeModel;
    private CustomerModel customerModel;

    private ConnectionServer server;

    private View vFocus;
    private View vPromo;
    private View vAll;
    private View vOrder;
    private View vDiskonPromo;
    private View vHadiah;

    private ProgressDialog progressDialog;

    private List<JSONObject> listPilih = new ArrayList<JSONObject>();

    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            commonModel.GeoLat = String.format("%1$s", location.getLatitude());
            commonModel.GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_spesial);
        realmUI = Realm.getDefaultInstance();
//        commonModel = rea
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }

        ctrlSalesProgram = new CtrlSalesProgram(this);
        ctrlSKU = new CtrlSKU(this);
        ctrlTakingOrder = new CtrlTakingOrder(this);
        ctrlCrcOrder = new CtrlCrcOrder(this);
        dbDiskonHelper = new DbDiskonHelper(this);
        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlConfig = new CtrlConfig(this);
        server = new ConnectionServer(this);
        adapterOrder = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);

        ctrlAppModul = new CtrlAppModul(this);
        ctrlCustomerType = new CtrlCustomerType(this);
        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = ctrlCustomerType.get(customerModel.CustomerTypeId);
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + customerModel.CustomerCrLimit + " | Sisa Credit Limit : " + customerModel.CustomerCrBalance);
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }

        CommonModel cmModel = (CommonModel) getIntent().getSerializableExtra("CommonModel");
        if (cmModel == null) {
            commonModel.SC = UserModel.getInstance(this).CompanyID;
            commonModel.D = "D" + String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
            commonModel.BeginVisitTime = Util.dateFormat.format(new Date());
            commonModel.SalesId = UserModel.getInstance(this).SalesId;
            commonModel.CustomerId = customerModel.CustId;
            commonModel.Visit = 1;
            commonModel.RVisitID = 0;
            commonModel.StatusID = 1;
        } else {
            commonModel = cmModel;
        }

        if (!ctrlAppModul.isModul("61")) {
            findViewById(R.id.tab2).setVisibility(View.GONE);
        }
        ((SegmentedRadioGroup) findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_focus) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_promo) {
                    mPager.setCurrentItem(1);
                } else if (checkedId == R.id.tab_all_product) {
                    mPager.setCurrentItem(2);
                } else if (checkedId == R.id.tab_order) {
                    mPager.setCurrentItem(3);
                }
            }
        });
        ((SegmentedRadioGroup) findViewById(R.id.tab2)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_diskon_promo) {
                    mPager.setCurrentItem(4);
                } else if (checkedId == R.id.tab_hadiah) {
                    mPager.setCurrentItem(5);
                }
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager_taking_order);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_focus);
                } else if (position == 1) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_promo);
                } else if (position == 2) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_all_product);
                } else if (position == 3) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_order);
                } else if (position == 4) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).check(R.id.tab_diskon_promo);
                    loadDiskonPromo();
                } else if (position == 5) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).clearCheck();
                    ((SegmentedRadioGroup) findViewById(R.id.tab2)).check(R.id.tab_hadiah);
                }
                super.onPageSelected(position);
            }
        };
        mPager.setOnPageChangeListener(ViewPagerListener);
        if (customerTypeModel != null) {
            String title = customerTypeModel.DiskonType.equals("2") ? "(Rp)" : "(%)";
            ((TextView) findViewById(R.id.lbl_discount)).setText("Discount " + title);
        }
        ((EditText) findViewById(R.id.discount)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateAmount();
            }
        });
        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);

        ((Button) findViewById(R.id.btn_search)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderSpesialActivity.this, SearchSKUActivity.class);
                intent.putExtra("CustGroupId", customerModel.CustGroupID);
                startActivityForResult(intent, 3);
            }
        });
        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });
        populateTakingOrder();
    }

    private void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    private void populateTakingOrder() {
        adapter = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
        adapterFocus = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
        adapterPromo = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
        List<GroupSKUModel> list = ctrlGroupSKU.list();
        for (GroupSKUModel groupSKUModel : list) {
            adapter.addHeader(groupSKUModel);
            if (groupSKUModel.FokusId.equals("1")) {
                adapterFocus.addHeader(groupSKUModel);
            }
            if (groupSKUModel.PromoId.equals("1")) {
                adapterPromo.addHeader(groupSKUModel);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Check out")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Check out");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
            chekOutOrder();
        }
        return super.onOptionsItemSelected(item);
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan kunjungan?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    finish();
                }
            }
        });
    }

    private void chekOutOrder() {
        Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    if (commonModel.PRICE == 0) {
                        Log.d("CommonPrice", "Nol");
                        noOrder();
                    } else {
                        Log.d("CommonPrice", "Exists");
                        if (ctrlAppModul.isModul("35")) {
                            pinOrder();
                        } else {
                            kirimOrder();
                        }
                    }
                }
            }
        });
    }

    private void kirimOrder() {
        commonModel.ROrderID = "0";
        progressDialog.setMessage("Check Out Data...");
        progressDialog.show();
        new Checkout().execute();
    }

    private void pinOrder() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pin_order);
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = ((EditText) dialog.findViewById(R.id.pin)).getText().toString();
                if (pin.equals(customerModel.PinID)) {
                    dialog.dismiss();
                    kirimOrder();
                } else {
                    Util.showDialogInfo(OrderSpesialActivity.this, "PIN salah!");
                }
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void noOrder() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("Check-out Failed!");
//        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_NO_SALES")));
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        /*((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ComboBoxModel coModel = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem();
                commonModel.ROrderID = coModel.value;
                progressDialog.setMessage("Check Out Data...");
                progressDialog.show();
                new Checkout().execute();
            }
        });*/
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void loadOrder() {
        try {
            adapterOrder = new OrderDetailAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
            for (int i = 0; i < adapter.getGroupCount(); i++) {
                GroupSKUModel groupSKUModel = adapter.getGroup(i);
                adapterOrder.addHeader(groupSKUModel);
                boolean flag = false;
                for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                    OrderDetailModel orderDetailModel = adapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        adapterOrder.addChild(groupSKUModel, orderDetailModel);
                        flag = true;
                    }
                }
                if (flag == false) {
                    adapterOrder.removeHeader(groupSKUModel);
                }
            }
            if (vOrder != null) {
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                adapterOrder.notifyDataSetInvalidated();
            }
        } catch (Exception e) {
            Util.showDialogError(this, e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                SKUModel model = (SKUModel) data.getSerializableExtra("data");
                addOrderItemSearch(new OrderDetailModel(model));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addOrderItemSearch(OrderDetailModel model) {
        try {
            int index = adapter.getPosisionHeader(new GroupSKUModel(model.skuModel.GroupId));
            List<OrderDetailModel> list = adapter.listOrderByGroup(adapter.getGroup(index));
            if (list != null) {
                int indexProduk = list.indexOf(model);
                if (indexProduk == -1) {
                    TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, model.skuModel.SKUId);
                    if (takingOrderModel != null) {
                        model.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                        model.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                        model.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                        model.note = takingOrderModel.note;
                        model.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                        model.QTY_B = takingOrderModel.QTY_B;
                        model.QTY_K = takingOrderModel.QTY_K;
                        String SalesProgramId = takingOrderModel.SalesProgramId;
                        if (SalesProgramId != null && !SalesProgramId.equals("")) {
                            model.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                        }
                        model.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                        model.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                        model.PRICE_B = takingOrderModel.PRICE_B;
                        model.PRICE_K = takingOrderModel.PRICE_K;
                    }
                    adapter.addChild(adapter.getGroup(index), model);
                    if (model.skuModel.FokusId.equals("1")) {
                        adapterFocus.addChild(adapter.getGroup(index), model);
                    }
                    if (model.skuModel.PromoId.equals("1")) {
                        adapterPromo.addChild(adapter.getGroup(index), model);
                    }
                    addJumlah(model);
                } else {
                    addJumlah(list.get(indexProduk));
                }
            } else {
                TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, model.skuModel.SKUId);
                if (takingOrderModel != null) {
                    model.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                    model.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                    model.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                    model.note = takingOrderModel.note;
                    model.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                    model.QTY_B = takingOrderModel.QTY_B;
                    model.QTY_K = takingOrderModel.QTY_K;
                    String SalesProgramId = takingOrderModel.SalesProgramId;
                    if (SalesProgramId != null && !SalesProgramId.equals("")) {
                        model.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                    }
                    model.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                    model.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                    model.PRICE_B = takingOrderModel.PRICE_B;
                    model.PRICE_K = takingOrderModel.PRICE_K;
                }
                adapter.addChild(adapter.getGroup(index), model);
                if (model.skuModel.FokusId.equals("1")) {
                    adapterFocus.addChild(adapter.getGroup(index), model);
                }
                if (model.skuModel.PromoId.equals("1")) {
                    adapterPromo.addChild(adapter.getGroup(index), model);
                }
                addJumlah(model);
            }
        } catch (Exception e) {
            Util.showDialogError(this, e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadOrder();
        calculateAmount();
    }

    @Override
    public void onPause() {
        super.onPause();
        calculateAmount();
    }

    private void calculateAmount() {
        int QTY_B = 0;
        int QTY_K = 0;
        double PRICE = 0;
        double DISCOUNT = 0;
        double S_AMOUNT = 0;
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                OrderDetailModel orderDetailModel = adapter.getChild(i, j);
                QTY_B += orderDetailModel.QTY_B;
                QTY_K += orderDetailModel.QTY_K;
                PRICE += orderDetailModel.TOTAL_PRICE;
            }
        }
        String cek = ((EditText) findViewById(R.id.discount)).getText().toString();
        DISCOUNT = Double.parseDouble(cek.equals("") ? "0" : cek);
        if (customerTypeModel != null) {
            if (DISCOUNT > customerTypeModel.DiskonMax) {
                String max = "";
                if (customerTypeModel.DiskonType.equals("2")) {
                    max = "Rp." + customerTypeModel.DiskonMax;
                } else {
                    max = "" + customerTypeModel.DiskonMax + "%";
                }
                Util.showDialogInfo(this, "Nilai melebihi batas masimum " + max);
                ((EditText) findViewById(R.id.discount)).setText(null);
                return;
            } else {
                if (customerTypeModel.DiskonType.equals("2")) {
                    S_AMOUNT = PRICE - DISCOUNT;
                } else {
                    S_AMOUNT = PRICE - (PRICE * DISCOUNT / 100);
                }
            }
        }
        commonModel.QTY_B = QTY_B;
        commonModel.QTY_K = QTY_K;
        commonModel.PRICE = PRICE;
        commonModel.DISCOUNT = DISCOUNT;
        commonModel.S_AMOUNT = S_AMOUNT;

        ((TextView) findViewById(R.id.total_qty)).setText(commonModel.QTY_B + "," + commonModel.QTY_K);
        ((TextView) findViewById(R.id.price)).setText(Util.numberFormat.format(commonModel.PRICE));
        ((TextView) findViewById(R.id.total_price)).setText(Util.numberFormat.format(commonModel.S_AMOUNT));
    }

    private void loadDiskonPromo() {
        String sqlGroup = "";
        String sqlProduk = "";
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel group = adapterOrder.getGroup(i);
            if (i == 0) {
                sqlGroup = "'" + group.GroupId + "'";
            } else {
                sqlGroup += ",'" + group.GroupId + "'";
            }
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
                if (i == 0 && j == 0) {
                    sqlProduk = "'" + model.skuModel.SKUId + "'";
                } else {
                    sqlProduk += ",'" + model.skuModel.SKUId + "'";
                }
            }
        }
        if (!sqlGroup.equals("") && !sqlProduk.equals("")) {
            if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='" + customerModel.CustId + "' or CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "'") > 0) {
                diskonAdapter = new DiskonAdapter(this, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
                        + "where DISCOUNT_PROMO_ID in ("
                        + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                        + ") "
                        + "and DISCOUNT_PROMO_ID in ("
                        + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_CUSTOMER where CUSTOMER_ID='" + customerModel.CustId + "' or CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "' group by DISCOUNT_PROMO_ID "
                        + ") "
                        + "union "
                        + "select * from DISCOUNT_PROMO "
                        + "where DISCOUNT_PROMO_ID in ("
                        + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                        + ")"));
            } else {
                diskonAdapter = new DiskonAdapter(this, dbDiskonHelper.list("select * from DISCOUNT_PROMO "
                        + "where DISCOUNT_PROMO_ID in ("
                        + "select DISCOUNT_PROMO_ID from DISCOUNT_PROMO_ITEM where GROUPPRODUCT_ID in (" + sqlGroup + ") or SKU_ID in (" + sqlProduk + ") group by DISCOUNT_PROMO_ID"
                        + ")"));
            }
            diskonAdapter.setListPilih(listPilih);
            diskonAdapter.setNotifyOnChange(true);
            diskonAdapter.setCheckedChangeListener(this);
            ((ListView) vDiskonPromo.findViewById(R.id.listdata)).setAdapter(diskonAdapter);
        }
    }

    @Override
    public void onCheckedChanged(DiskonAdapter adapter, JSONObject json, boolean isChecked) {
        try {
            if (!isChecked) {
                removelistPilih(json);
            } else {
                listPilih.add(json);
            }
//            diskonPromoAdapter = new DiskonPromoAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<DiskonPromoModel>>());
            diskonPromoAdapter = new DiskonPromoAdapter(this, new ArrayList<String>(), new HashMap<String, List<DiskonPromoModel>>());
            for (int i = 0; i < adapter.getCount(); i++) {
                json = adapter.getItem(i);
                if (json.has("isChecked") && json.getBoolean("isChecked")) {
                    boolean isDiskon = false;
                    if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "'") > 0) {
                        if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' and CUSTOMER_TYPE_ID='" + customerModel.CustomerTypeId + "' ") > 0) {
                            if (dbDiskonHelper.count("from DISCOUNT_PROMO_CUSTOMER where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' and CUSTOMER_ID='" + customerModel.CustId + "' ") > 0) {
                                isDiskon = true;
                            } else {
                                isDiskon = true;
                            }
                        }
                    } else {
                        isDiskon = true;
                    }
                    if (isDiskon) {
                        if (json.getInt("DISCOUNT_PROMO_TYPE") == 1) {
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_TOTAL where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "'");
                        } else if (json.getInt("DISCOUNT_PROMO_TYPE") == 2) {
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_PROMO_ITEM where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' order by SORT_ORDER");
                            for (JSONObject jsonObject : list) {
                                if (jsonObject.getInt("SKU_ID") > 0) { //diskon sku
                                    cekProduk(jsonObject);
                                } else { //diskon group sku
                                    cekGroup(jsonObject);
                                }
                            }
                        } else if (json.getInt("DISCOUNT_PROMO_TYPE") == 3) {
                            List<JSONObject> list = dbDiskonHelper.list("select * from DISCOUNT_PROMO_ITEM where DISCOUNT_PROMO_ID='" + json.getString("DISCOUNT_PROMO_ID") + "' order by SORT_ORDER");
                            for (JSONObject jsonObject : list) {
                                if (jsonObject.getInt("SKU_ID") > 0) { //diskon sku
                                    cekProdukType3(json, jsonObject);
                                } else { //diskon group sku
                                    cekGroupType3(json, jsonObject);
                                }
                            }
                        }
                    }
                }
            }
            if (vHadiah != null) {
                ((ExpandableListView) vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removelistPilih(JSONObject json) {
        try {
            for (int i = 0; i < listPilih.size(); i++) {
                JSONObject data = listPilih.get(i);
                if (data.getInt("DISCOUNT_PROMO_ID") == json.getInt("DISCOUNT_PROMO_ID")) {
                    listPilih.remove(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cekProduk(JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
                int jml = model.QTY_B + (int) (model.QTY_K / model.skuModel.CONVER);
                if (jsonObject.getInt("SKU_ID") == Integer.parseInt(model.skuModel.SKUId) &&
                        ((jsonObject.getString("CONDITION").equals(">") && jml > jsonObject.getInt("QTY")) ||
                                (jsonObject.getString("CONDITION").equals(">=") && jml >= jsonObject.getInt("QTY")))
                        ) {
                    JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                    int jmlDiskon = jsonResult.getInt("QTY");
                    if (jsonResult.getInt("ISMULTIPLY") == 1) {
                        jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                    }

                    String SKUNAME = jsonResult.getString("SKU_NAME");
                    DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                    diskonPromoModel.QTY = jmlDiskon;
                    diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                    diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                    GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
                    int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                    if (hIndex != -1) {
                        SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                    } else {
                        diskonPromoAdapter.addHeader(SKUNAME);
                    }
                    diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);
                    break;
                }
            }
        }
    }

    private void cekGroup(JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel model = adapterOrder.getGroup(i);
            int jml = adapterOrder.getTotalPerGoupSatuanBesar(model);
            if (jsonObject.getInt("GROUPPRODUCT_ID") == Integer.parseInt(model.GroupId) &&
                    ((jsonObject.getString("CONDITION").equals(">") && jml > jsonObject.getInt("QTY")) ||
                            (jsonObject.getString("CONDITION").equals(">=") && jml >= jsonObject.getInt("QTY")))
                    ) {
                JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                int jmlDiskon = jsonResult.getInt("QTY");
                if (jsonResult.getInt("ISMULTIPLY") == 1) {
                    jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                }

                String SKUNAME = jsonResult.getString("SKU_NAME");
                DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                diskonPromoModel.QTY = jmlDiskon;
                diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
                int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                if (hIndex != -1) {
                    SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                } else {
                    diskonPromoAdapter.addHeader(SKUNAME);
                }
                diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);
                break;
            }
        }
    }

    private void cekProdukType3(JSONObject json, JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            for (int j = 0; j < adapterOrder.getChildrenCount(i); j++) {
                OrderDetailModel model = adapterOrder.getChild(i, j);
                int jml = model.QTY_B + (int) (model.QTY_K / model.skuModel.CONVER);
                if (jsonObject.getInt("SKU_ID") == Integer.parseInt(model.skuModel.SKUId) && jml >= json.getInt("MIN_QTY")) {
                    JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                    int jmlDiskon = jsonResult.getInt("QTY");
                    if (jsonResult.getInt("ISMULTIPLY") == 1) {
                        jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                    }

                    String SKUNAME = jsonResult.getString("SKU_NAME");
                    DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                    diskonPromoModel.QTY = jmlDiskon;
                    diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                    diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                    GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
                    int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                    if (hIndex != -1) {
                        SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                    } else {
                        diskonPromoAdapter.addHeader(SKUNAME);
                    }
                    diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);
                    break;
                }
            }
        }
    }

    private void cekGroupType3(JSONObject json, JSONObject jsonObject) throws Exception {
        for (int i = 0; i < adapterOrder.getGroupCount(); i++) {
            GroupSKUModel model = adapterOrder.getGroup(i);
            int jml = adapterOrder.getTotalPerGoupSatuanBesar(model);
            if (jsonObject.getInt("GROUPPRODUCT_ID") == Integer.parseInt(model.GroupId) && jml >= json.getInt("MIN_QTY")) {
                JSONObject jsonResult = dbDiskonHelper.get("select * from DISCOUNT_PROMO_RESULT where DISCOUNT_PROMO_ID='" + jsonObject.getString("DISCOUNT_PROMO_ID") + "'");
                int jmlDiskon = jsonResult.getInt("QTY");
                if (jsonResult.getInt("ISMULTIPLY") == 1) {
                    jmlDiskon = jmlDiskon * (jml / jsonResult.getInt("MULTIPLY_QTY"));
                }

                String SKUNAME = jsonResult.getString("SKU_NAME");
                DiskonPromoModel diskonPromoModel = new DiskonPromoModel(SKUNAME);
                diskonPromoModel.QTY = jmlDiskon;
                diskonPromoModel.DiscountResultID = jsonResult.getInt("DISCOUNT_PROMO_RESULT_ID");
                diskonPromoModel.SATUAN = jsonResult.getString("SKU_SATUAN");
//                GroupSKUModel groupSKUModel = ctrlGroupSKU.getBySKUId(jsonResult.getString("SKU_ID"));
                int hIndex = diskonPromoAdapter.getPosisionHeader(SKUNAME);
                if (hIndex != -1) {
                    SKUNAME = diskonPromoAdapter.getGroup(hIndex);
                } else {
                    diskonPromoAdapter.addHeader(SKUNAME);
                }
                diskonPromoAdapter.addChild(SKUNAME, diskonPromoModel);
                break;
            }
        }
    }

    private void addJumlah(final OrderDetailModel model) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_taking_order);
        ((TextView) dialog.findViewById(R.id.title)).setText(model.skuModel.ProductName);

        if (ctrlAppModul.isModul("34")) {
            CrcOrderModel crcOrderModel = ctrlCrcOrder.get(Integer.parseInt(customerModel.CustId), Integer.parseInt(model.skuModel.SKUId));
            if (crcOrderModel != null) {
                if (model.QTY_B == 0) {
                    model.QTY_B = crcOrderModel.QTY_L;
                }
                if (model.QTY_K == 0) {
                    model.QTY_K = crcOrderModel.QTY;
                }
            }
        }

        int visibility = ctrlAppModul.isModul("53") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_1).setVisibility(visibility);
        visibility = ctrlAppModul.isModul("54") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_2).setVisibility(visibility);
        visibility = ctrlAppModul.isModul("55") ? View.VISIBLE : View.GONE;
        dialog.findViewById(R.id.row_discount_3).setVisibility(visibility);

        AppModul appModul = ctrlAppModul.get("57");
        if (appModul != null) {
            if (appModul.AppModulValue.equals("0")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
            } else if (appModul.AppModulValue.equals("1")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setChecked(true);
            } else if (appModul.AppModulValue.equals("2")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.VISIBLE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
            } else if (appModul.AppModulValue.equals("3")) {
                dialog.findViewById(R.id.group_diskon).setVisibility(View.GONE);
                ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setChecked(true);
            }
        }

        ((EditText) dialog.findViewById(R.id.jml_1)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model);
            }
        });
        ((EditText) dialog.findViewById(R.id.jml_2)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_1)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_2)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model);
            }
        });
        ((EditText) dialog.findViewById(R.id.discount_3)).addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                total(dialog, model);
            }
        });
        ((RadioGroup) dialog.findViewById(R.id.rb_diskon)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                total(dialog, model);
            }
        });
        ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(model.TOTAL_PRICE));
        ((RadioGroup) dialog.findViewById(R.id.payment_term)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.cash) {
                    dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
                    ((EditText) dialog.findViewById(R.id.lama_credit)).setText(null);
                } else if (checkedId == R.id.credit) {
                    dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
                }
            }
        });
        ((Button) dialog.findViewById(R.id.stok_1)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stok(model, dialog);
            }
        });
        ((Button) dialog.findViewById(R.id.stok_2)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                stok(model, dialog);
            }
        });

        String title = model.skuModel.DISCOUNT_1_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_1)).setText("Discount1 " + title);
        title = model.skuModel.DISCOUNT_2_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_2)).setText("Discount2 " + title);
        title = model.skuModel.DISCOUNT_3_TYPE.equals("2") ? "(Rp)" : "(%)";
        ((TextView) dialog.findViewById(R.id.lbl_discount_3)).setText("Add Discount " + title);

        ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
        ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
        ((Spinner) dialog.findViewById(R.id.SalesProgram)).setAdapter(new SalesProgramAdapter(this, ctrlSalesProgram.listByGropID(model.skuModel.GroupId)));
        if (model.salesProgramModel != null) {
            int index = ((SalesProgramAdapter) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getAdapter()).indexOf(model.salesProgramModel);
            ((Spinner) dialog.findViewById(R.id.SalesProgram)).setSelection(index);
        }
        if (model.PAYMENT_TERM == null || model.PAYMENT_TERM.equals("") || model.PAYMENT_TERM.equals("2")) {
            ((RadioButton) dialog.findViewById(R.id.credit)).setChecked(true);
            dialog.findViewById(R.id.panel_credit).setVisibility(View.VISIBLE);
        } else if (model.PAYMENT_TERM.equals("1")) {
            ((RadioButton) dialog.findViewById(R.id.cash)).setChecked(true);
            dialog.findViewById(R.id.panel_credit).setVisibility(View.GONE);
        }
        ((EditText) dialog.findViewById(R.id.note)).setText(model.note);
        ((TextView) dialog.findViewById(R.id.lbl_jml_1)).setText(model.skuModel.SBESAR);
        ((TextView) dialog.findViewById(R.id.lbl_jml_2)).setText(model.skuModel.SKECIL);
        ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).setText(model.skuModel.SBESAR);
        ((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).setText(model.skuModel.SKECIL);

        ((TextView) dialog.findViewById(R.id.price_1)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE_L));
        ((TextView) dialog.findViewById(R.id.price_2)).setText(Util.numberFormat.format(model.skuModel.HET_PRICE));
        ((EditText) dialog.findViewById(R.id.lama_credit)).setText(String.valueOf(model.LAMA_CREDIT));

        if (model.QTY_B > 0) {
            ((EditText) dialog.findViewById(R.id.jml_1)).setText(String.valueOf(model.QTY_B));
        }
        if (model.QTY_K > 0) {
            ((EditText) dialog.findViewById(R.id.jml_2)).setText(String.valueOf(model.QTY_K));
        }
        if (model.DISCOUNT1 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_1)).setText(String.valueOf(model.DISCOUNT1));
        }
        if (model.DISCOUNT2 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_2)).setText(String.valueOf(model.DISCOUNT2));
        }
        if (model.DISCOUNT3 > 0) {
            ((EditText) dialog.findViewById(R.id.discount_3)).setText(String.valueOf(model.DISCOUNT3));
        }
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
                int QTY_B = Integer.parseInt(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
                int QTY_K = Integer.parseInt(cek.equals("") ? "0" : cek);
                model.QTY_B = QTY_B;
                model.QTY_K = QTY_K;
                model.salesProgramModel = (SalesProgramModel) ((Spinner) dialog.findViewById(R.id.SalesProgram)).getSelectedItem();
                cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
                model.DISCOUNT1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
                model.DISCOUNT2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
                model.DISCOUNT3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                model.note = ((EditText) dialog.findViewById(R.id.note)).getText().toString();
                if (((RadioButton) dialog.findViewById(R.id.cash)).isChecked()) {
                    model.PAYMENT_TERM = "1";
                } else if (((RadioButton) dialog.findViewById(R.id.credit)).isChecked()) {
                    model.PAYMENT_TERM = "2";
                }
                cek = ((EditText) dialog.findViewById(R.id.lama_credit)).getText().toString();
                model.LAMA_CREDIT = Integer.parseInt(cek.equals("") ? "0" : cek);
                model.PRICE_B = model.skuModel.HET_PRICE_L;
                model.PRICE_K = model.skuModel.HET_PRICE;
                model.TOTAL_PRICE = totalValue(dialog, model);

                //tambahan untuk tipe diskon
                if (((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked()) {
                    model.DiscountType = "1";
                } else if (((RadioButton) dialog.findViewById(R.id.rb_diskon_k)).isChecked()) {
                    model.DiscountType = "0";
                }

                calculateAmount();

                adapter.notifyDataSetChanged();
                adapterFocus.notifyDataSetChanged();
                adapterPromo.notifyDataSetChanged();
                if (adapterSearch != null) {
                    adapterSearch.notifyDataSetChanged();
                }
                loadOrder();
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void total(final Dialog dialog, final OrderDetailModel model) {
        double total = totalValue(dialog, model);
        if (total < 0) {
            Util.showDialogInfo(this, "Diskon anda melebihi total harga!");
            ((EditText) dialog.findViewById(R.id.discount_1)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_2)).setText("0");
            ((EditText) dialog.findViewById(R.id.discount_3)).setText("0");
            ((TextView) dialog.findViewById(R.id.total_price)).setText("0");
        } else {
            ((TextView) dialog.findViewById(R.id.total_price)).setText(Util.numberFormat.format(total));
        }
    }

    private double totalValue(final Dialog dialog, final OrderDetailModel model) {
        try {
            boolean isDiskonBesar = ((RadioButton) dialog.findViewById(R.id.rb_diskon_b)).isChecked();
            String cek = ((EditText) dialog.findViewById(R.id.jml_1)).getText().toString();
            int jml1 = Integer.parseInt(cek.equals("") ? "0" : cek);
            cek = ((EditText) dialog.findViewById(R.id.jml_2)).getText().toString();
            int jml2 = Integer.parseInt(cek.equals("") ? "0" : cek);

            double diskon = 0;

            //diskon 1
            cek = ((EditText) dialog.findViewById(R.id.discount_1)).getText().toString();
            double diskon1 = 0;
            if (model.skuModel.DISCOUNT_1_TYPE.equals("2")) {
                diskon1 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon1 > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(this, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_1_MAX) {
                    Util.Toast(this, "Diskon1 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_1)).setText(null);
                    diskon1 = 0;
                } else {
                    diskon1 = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) * persen / 100;
                }
            }
            diskon = (isDiskonBesar ? model.skuModel.HET_PRICE_L : model.skuModel.HET_PRICE) - diskon1;
            //diskon 2
            cek = ((EditText) dialog.findViewById(R.id.discount_2)).getText().toString();
            double diskon2 = 0;
            if (model.skuModel.DISCOUNT_2_TYPE.equals("2")) {
                diskon2 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon2 > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(this, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_2_MAX) {
                    Util.Toast(this, "Diskon2 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_2)).setText(null);
                    diskon2 = 0;
                } else {
                    diskon2 = diskon * persen / 100;
                }
            }
            diskon = diskon - diskon2;
            //hitung total
            double total = 0;
//			if(model.skuModel.DISCOUNT_TYPE.equals("1")) {
            if (isDiskonBesar) {
//				double het_b = model.skuModel.HET_PRICE_L;
//				het_b -= diskon1;
//				het_b -= diskon2;
                total = (jml1 * diskon) + (jml2 * model.skuModel.HET_PRICE);
            } else {
//				double het_k = model.skuModel.HET_PRICE;
//				het_k -= diskon1;
//				het_k -= diskon2;
                double qty_k = jml1 * model.skuModel.CONVER;
                qty_k += jml2;
//				total = het_k*qty_k;
                total = diskon * qty_k;
            }
            //diskon 3
            cek = ((EditText) dialog.findViewById(R.id.discount_3)).getText().toString();
            double diskon3 = 0;
            if (model.skuModel.DISCOUNT_3_TYPE.equals("2")) {
                diskon3 = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (diskon3 > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(this, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                }
            } else {
                double persen = Double.parseDouble(cek.equals("") ? "0" : cek);
                if (persen > model.skuModel.DISCOUNT_3_MAX) {
                    Util.Toast(this, "Diskon3 melebih");
                    ((EditText) dialog.findViewById(R.id.discount_3)).setText(null);
                    diskon3 = 0;
                } else {
                    diskon3 = total * persen / 100;
                }
            }
            total = total - diskon3;
            return total;
        } catch (Exception e) {
            return 0;
        }

    }

    private void stok(final OrderDetailModel model, final Dialog dialog) {
        server.addConnectionEvent(new ConnectionEvent() {
            @Override
            public void messageServer(String respon) {
                try {
                    JSONObject json = new JSONArray(respon).getJSONObject(0);
                    model.STOK_B = json.getInt("StockQty_L");
                    model.STOK_K = json.getInt("StockQty");
                    ((Button) dialog.findViewById(R.id.stok_1)).setText(Util.numberFormat.format(model.STOK_B));
                    ((Button) dialog.findViewById(R.id.stok_2)).setText(Util.numberFormat.format(model.STOK_K));
                } catch (Exception e) {
                    error(e.getMessage());
                }
            }

            @Override
            public void error(String error) {
                Util.showDialogError(OrderSpesialActivity.this, error);
            }
        });
        server.setUrl(Util.getServerUrl(this) + "getSKUstock/SalesId/" + UserModel.getInstance(this).SalesId + "/SKUId/" + model.skuModel.SKUId);
        server.requestJSONObject(null);
    }

    class ViewPagerAdapter extends PagerAdapter {
        @Override
        public Object instantiateItem(View collection, final int position) {
            LayoutInflater inflater = (LayoutInflater) OrderSpesialActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_produk_taking_order, null);
            if (position == 0) {
                vFocus = v;
                vFocus.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vFocus.findViewById(R.id.list)).setAdapter(adapterFocus);
                ((ExpandableListView) vFocus.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                ((ExpandableListView) vFocus.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(2, adapter.getGroup(groupPosition));
                        return false;
                    }
                });
            } else if (position == 1) {
                vPromo = v;
                vPromo.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vPromo.findViewById(R.id.list)).setAdapter(adapterPromo);
                ((ExpandableListView) vPromo.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                ((ExpandableListView) vPromo.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(3, adapter.getGroup(groupPosition));
                        return false;
                    }
                });
            } else if (position == 2) {
                vAll = v;
                vAll.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vAll.findViewById(R.id.list)).setAdapter(adapter);
                ((ExpandableListView) vAll.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
                ((ExpandableListView) vAll.findViewById(R.id.list)).setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        populateSKUByGroup(1, adapter.getGroup(groupPosition));
                        return false;
                    }
                });
            } else if (position == 3) {
                vOrder = v;
                vOrder.findViewById(R.id.list).setVisibility(View.VISIBLE);
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setAdapter(adapterOrder);
                ((ExpandableListView) vOrder.findViewById(R.id.list)).setOnChildClickListener(new OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                        OrderDetailAdapter adapter = (OrderDetailAdapter) parent.getExpandableListAdapter();
                        addJumlah(adapter.getChild(groupPosition, childPosition));
                        return false;
                    }
                });
            } else if (position == 4) {
                vDiskonPromo = v;
                vDiskonPromo.findViewById(R.id.listdata).setVisibility(View.VISIBLE);
            } else if (position == 5) {
                vHadiah = v;
                vHadiah.findViewById(R.id.list).setVisibility(View.VISIBLE);
                if (diskonPromoAdapter != null) {
                    ((ExpandableListView) vHadiah.findViewById(R.id.list)).setAdapter(diskonPromoAdapter);
                }
            }

            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return ctrlAppModul.isModul("61") ? 6 : 4;
        }
    }

    private void populateSKUByGroup(int jenis, GroupSKUModel group) {
        try {
            List<OrderDetailModel> list = adapter.listOrderByGroup(group);
            if (list == null || list.size() < group.jumlah) {
                List<SKUModel> listSKU = ctrlSKU.listByGroup(group, customerModel.CustGroupID);
                for (SKUModel skuModel : listSKU) {
                    OrderDetailModel orderDetailModel = new OrderDetailModel(skuModel);
                    if (list == null || list.indexOf(orderDetailModel) == -1) {
                        TakingOrderModel takingOrderModel = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID, skuModel.SKUId);
                        if (takingOrderModel != null) {
                            orderDetailModel.DISCOUNT1 = takingOrderModel.DISCOUNT1;
                            orderDetailModel.DISCOUNT2 = takingOrderModel.DISCOUNT2;
                            orderDetailModel.DISCOUNT3 = takingOrderModel.DISCOUNT3;
                            orderDetailModel.note = takingOrderModel.note;
                            orderDetailModel.PAYMENT_TERM = takingOrderModel.PAYMENT_TERM;
                            orderDetailModel.QTY_B = takingOrderModel.QTY_B;
                            orderDetailModel.QTY_K = takingOrderModel.QTY_K;
                            String SalesProgramId = takingOrderModel.SalesProgramId;
                            if (SalesProgramId != null && !SalesProgramId.equals("")) {
                                orderDetailModel.salesProgramModel = ctrlSalesProgram.get(SalesProgramId);
                            }
                            orderDetailModel.TOTAL_PRICE = takingOrderModel.TOTAL_PRICE;
                            orderDetailModel.LAMA_CREDIT = takingOrderModel.LAMA_KREDIT;
                            orderDetailModel.PRICE_B = takingOrderModel.PRICE_B;
                            orderDetailModel.PRICE_K = takingOrderModel.PRICE_K;
                        }
                        adapter.addChild(group, orderDetailModel);
                        if (jenis == 1) { //all
                            if (group.FokusId.equals("1")) {
                                adapterFocus.addChild(group, orderDetailModel);
                            }
                            if (group.PromoId.equals("1")) {
                                adapterPromo.addChild(group, orderDetailModel);
                            }
                        } else if (jenis == 2) { //focus
                            adapterFocus.addChild(group, orderDetailModel);
                            if (group.PromoId.equals("1")) {
                                adapterPromo.addChild(group, orderDetailModel);
                            }
                        } else if (jenis == 3) { //promo
                            adapterPromo.addChild(group, orderDetailModel);
                            if (group.FokusId.equals("1")) {
                                adapterFocus.addChild(group, orderDetailModel);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Util.showDialogError(this, e.getMessage());
        }
    }

    class Checkout extends AsyncTask<Void, Void, String> {
        //		private String resultAsset = null;
        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            String tanggal = Util.dateFormat.format(new Date());
            try {
                commonModel.EndVisitTime = tanggal;
                commonModel.GeoLat = location != null ? String.format("%1$s", location.getLatitude()) : "0";
                commonModel.GeoLong = location != null ? String.format("%1$s", location.getLongitude()) : "0";
                commonModel.LogTime = Util.dateFormat.format(location != null ? new Date(location.getTime()) : new Date());
                commonModel.InvNO = "";
                commonModel.Description = "";//noteFragment.getNote();

                JSONObject Common = new JSONObject();
                Common.put("SC", commonModel.SC);
                Common.put("GeoLat", Double.parseDouble(commonModel.GeoLat.equals("") ? "0" : commonModel.GeoLat));
                Common.put("CustomerId", commonModel.CustomerId);
                Common.put("BeginVisitTime", commonModel.BeginVisitTime);
                Common.put("GeoLong", Double.parseDouble(commonModel.GeoLong.equals("") ? "0" : commonModel.GeoLong));
                Common.put("LogTime", commonModel.LogTime);
                Common.put("EndVisitTime", commonModel.EndVisitTime);
                Common.put("SalesId", Integer.parseInt(commonModel.SalesId));
                Common.put("S_AMOUNT", commonModel.PRICE);
                Common.put("Description", commonModel.Description);
                Common.put("D_VALUE", commonModel.DISCOUNT);
                Common.put("G_AMOUNT", commonModel.S_AMOUNT);
                Common.put("Visit", commonModel.Visit);
                Common.put("RVisitID", commonModel.RVisitID);
                Common.put("InvNO", commonModel.InvNO);
                Common.put("StatusID", commonModel.StatusID);

                JSONArray Stock = new JSONArray();
                JSONArray StockCompetitor = new JSONArray();
                JSONArray Order = new JSONArray();
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    if (adapter.listOrderByGroup(adapter.getGroup(i)) != null) {
                        for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                            OrderDetailModel orderDetailModel = adapter.getChild(i, j);
                            if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                                JSONObject json = new JSONObject();
                                json.put("CustomerId", Integer.parseInt(customerModel.CustId));
                                json.put("SKUId", Integer.parseInt(orderDetailModel.skuModel.SKUId));
                                json.put("Amount", orderDetailModel.QTY_K);
                                json.put("Amount2", orderDetailModel.QTY_B);
                                //							json.put("DiscountType", orderDetailModel.skuModel.DISCOUNT_TYPE);
                                json.put("DiscountType", orderDetailModel.DiscountType);
                                json.put("Discount1", orderDetailModel.DISCOUNT1);
                                json.put("Discount2", orderDetailModel.DISCOUNT2);
                                json.put("Discount3", orderDetailModel.DISCOUNT3);
                                double T_Discount = ((orderDetailModel.QTY_K * orderDetailModel.skuModel.HET_PRICE) + (orderDetailModel.QTY_B * orderDetailModel.skuModel.HET_PRICE_L)) - orderDetailModel.TOTAL_PRICE;
                                //							json.put("T_Discount", orderDetailModel.DISCOUNT1+orderDetailModel.DISCOUNT2+orderDetailModel.DISCOUNT3);
                                json.put("T_Discount", T_Discount);
                                json.put("Total", orderDetailModel.TOTAL_PRICE);
                                //							json.put("SalesProgramId", Integer.parseInt(UserModel.getInstance(KunjunganActivity.this).SalesId));
                                json.put("SalesProgramId", Integer.parseInt(orderDetailModel.salesProgramModel.SalesProgramId));
                                json.put("ItmDescription", orderDetailModel.note);
                                json.put("TopType", orderDetailModel.PAYMENT_TERM);
                                json.put("TopDays", orderDetailModel.LAMA_CREDIT);
                                json.put("TransactionTime", tanggal);
                                Order.put(json);
                                //							isOrder = true;
                            }
                        }
                    }
                }

//				if(isOrder) {
//					commonModel.ROrderID = "0";
//				} else {
//					commonModel.ROrderID = "3";
//				}
                Common.put("ROrderID", commonModel.ROrderID);

                JSONArray Return = new JSONArray();
                JSONArray Collection = new JSONArray();
                JSONArray GroupStock = new JSONArray();
                JSONArray DiscountPromo = new JSONArray();
                JSONArray DiscountResult = new JSONArray();

                JSONObject jsonSend = new JSONObject();
                jsonSend.put("Common", Common);
                jsonSend.put("Stock", Stock);
                jsonSend.put("Order", Order);
                jsonSend.put("Return", Return);
                jsonSend.put("Collection", Collection);
                jsonSend.put("Competitor", StockCompetitor);
                jsonSend.put("GroupStock", GroupStock);
                jsonSend.put("DiscountPromo", DiscountPromo);
                jsonSend.put("DiscountResult", DiscountResult);

                JSONArray Asset = new JSONArray();
                JSONArray Newasset = new JSONArray();
                jsonSend.put("Asset", Asset);
                jsonSend.put("Newasset", Newasset);

                android.util.Log.e("jsonSend", jsonSend.toString());

//				saveLokal(tanggal);

                server.setUrl(Util.getServerUrl(OrderSpesialActivity.this) + "endvisit");
                result = server.requestJSONObjectNonThread(jsonSend);
                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        commonModel.Status = "1";
//						ctrlCommon.update(commonModel);
                        result = jsonRespon.getString("Message");
                    } else {
                        result = null;
                    }
                }
            } catch (Exception e) {
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.Toast(OrderSpesialActivity.this, result);
                finish();
            } /*else {
                Util.okeDialog(KunjunganActivity.this, "Info", "Koneksi gagal, data akan tersimpan diinternal memory", new OkListener() {
					@Override
					public void onDialogOk() {
						if(takingOrderFragment.isCanvas()) {
							Util.confirmDialog(KunjunganActivity.this, "Info", "Nomor Faktur " + commonModel.InvNO + ", Apakah Faktur ingin dicetak?", new ConfirmListener() {
								@Override
								public void onDialogCompleted(boolean answer) {
									if(answer) {
										isHome = false;
									    bluePrint.open();
										printFaktur();
									} else {
										finish();
									}
								}
							});
						} else {
							isHome = false;
							finish();
						}
					}
				});
			}*/
            super.onPostExecute(result);
        }
    }
}
