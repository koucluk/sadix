package com.bhn.sadix;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bhn.sadix.adapter.CustomerRecyclerAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

public class AllCustomerActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, /*OnItemClickListener*/ CustomerRecyclerAdapter.ClickItem {
    //    private CustomerAdapter adapter;
    private CustomerRecyclerAdapter adapter;
    private CtrlCustomer ctrlCustomer;
    private CtrlCustomerType ctrlCustomerType;
    private ProgressDialog progressDialog;
    private String filter = null;
    private CtrlAppModul ctrlAppModul;
    //    private CtrlCommon ctrlCommon;
    private int jenis;
    private SharedPreferences sharedPreferences;
    private Realm realmUI;

    private RecyclerView list;
    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_customer);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ctrlAppModul = new CtrlAppModul(this);
        ctrlCustomer = new CtrlCustomer(this);
        ctrlCustomerType = new CtrlCustomerType(this);

        realmUI = Realm.getDefaultInstance();

        com.bhn.sadix.Data.CommonModel model = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();
                    /*CmId = getSharedPreferences("SADIX", MODE_PRIVATE).getString("custId", "0");
                    Log.d("Ini last CID ", CmId);
                    model = ctrlCommon.getCommonModel(CmId);*/
        if (model != null) {
            startActivity(new Intent(this, MenuUtamaActivity.class));
            Log.d("Common ID", model.CommonID);
            Intent intent = new Intent(this, KunjunganActivity.class);
            CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
            Log.d("Ini last CID ", customerModel.CustId);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
            intent.putExtra("isLoad", true);
            startActivity(intent);

            finish();
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MenuUtamaActivity.isActive) {
                    finish();
                } else {
                    Intent intent = new Intent(AllCustomerActivity.this, MenuUtamaActivity.class);
                    startActivity(intent);
                }
            }
        });

        sharedPreferences = getSharedPreferences("SADIX", MODE_PRIVATE);
        progressDialog = new ProgressDialog(this)/* {
            @Override
			public boolean onKeyDown(int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
					dismiss();
				}
				return super.onKeyDown(keyCode, event);
			}
			
		}*/;
        jenis = getIntent().getIntExtra("jenis", 0);

        Log.d("Jenis", jenis + "");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");

//        ctrlCommon = new CtrlCommon(this);
        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            jenis = savedInstanceState.getInt("jenis");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        new LoadData().execute();
    }

    @Override
    public void clickedItem(int position, CustomerModel model) {
//        CustomerModel model = adapter.getItem(position);
//        Toast.makeText(this, "" + model.CustomerName, Toast.LENGTH_SHORT).show();

        Intent intent = null;
        sharedPreferences.edit().putInt("jenis", jenis);
        if (jenis == 1) {
            intent = new Intent(this, DetailCustomerActivity.class);
            if (ctrlAppModul.isModul("56")) {
                com.bhn.sadix.Data.CommonModel commonModel = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                        .equalTo("CustomerId", model.CustId)
                        .equalTo("StatusID", 0)
                        .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                        .equalTo("Done", 1).findFirst();

                if (commonModel != null) {
                    //				Util.showDialogInfo(this, "Customer " + model.CustomerName + " sudah dikunjungi!");
                    //				return;
                    intent.putExtra("MSG", "Customer " + model.CustomerName + " sudah dikunjungi!");
                }
            }
            //		Intent intent = new Intent(this, DetailCustomerActivity.class);
        } else if (jenis == 2) {
            intent = new Intent(this, ServiceAssetActivity.class);
        } else if (jenis == 3) {
//			intent = new Intent(this, OrderSpesialActivity.class);
            intent = new Intent(this, NewOrderSpesialActivity.class);
        }

        if (intent != null) {
            intent.putExtra("CustomerModel", model);
            intent.putExtra("jenis", jenis);
            Util.putPreference(this, "taking_from", Util.ALL_CUSTOMER);
            startActivity(intent);
            finish();
        }
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        /*public LoadData() {
            progressDialog.show();
        }*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                List<CustomerModel> model;
                if (filter != null && filter.equals("") == false) {
                    model = ctrlCustomer.listByName(filter);
                    adapter = new CustomerRecyclerAdapter(AllCustomerActivity.this, model);
                } else {
                    model = ctrlCustomer.list();
                    adapter = new CustomerRecyclerAdapter(AllCustomerActivity.this, model);
                }
//                adapter.setNotifyOnChange(true);
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return "" + e.getLocalizedMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result == null) {
//                ((ListView) findViewById(R.id.list)).setAdapter(adapter);
                list.setAdapter(adapter);
            } else {
                Util.okeDialog(AllCustomerActivity.this, "INFO", "Data yang didownload tidak lengkap, harap login kembali!", new OkListener() {

                    @Override
                    public void onDialogOk() {
                        finish();
                    }
                });
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("jenis", jenis);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        searchView.setOnQueryTextListener(this);
        MenuItem item = menu.add(1, 1, 1, "Search")
                .setIcon(android.R.drawable.ic_menu_search);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            item.setActionView(searchView)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            finish();
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filter = query;
        new LoadData().execute();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (filter != null && newText.equals("")) {
            filter = null;
            new LoadData().execute();
        }
        return false;
    }

    /*@Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CustomerModel model = adapter.getItem(position);
        Intent intent = null;
        sharedPreferences.edit().putInt("jenis", jenis);
        if (jenis == 1) {
            intent = new Intent(this, DetailCustomerActivity.class);
            if (ctrlAppModul.isModul("56")) {

                com.bhn.sadix.Data.CommonModel commonModel = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                        .equalTo("CustomerId", model.CustId)
                        .like("BeginVisitTime", new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "*")
                        .equalTo("Done", 1).findFirst();

//                commonModel = ctrlCommon.getByCustomerAndDate(model.CustId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                if (commonModel != null) {
                    //				Util.showDialogInfo(this, "Customer " + model.CustomerName + " sudah dikunjungi!");
                    //				return;
                    intent.putExtra("MSG", "Customer " + model.CustomerName + " sudah dikunjungi!");
                }
            }
            //		Intent intent = new Intent(this, DetailCustomerActivity.class);
        } else if (jenis == 2) {
            intent = new Intent(this, ServiceAssetActivity.class);
        } else if (jenis == 3) {
//			intent = new Intent(this, OrderSpesialActivity.class);
            intent = new Intent(this, NewOrderSpesialActivity.class);
        }
        if (intent != null) {
            intent.putExtra("CustomerModel", model);
            intent.putExtra("jenis", jenis);
            startActivity(intent);
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realmUI.close();
    }
}
