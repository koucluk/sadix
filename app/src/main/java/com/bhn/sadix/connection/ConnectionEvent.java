package com.bhn.sadix.connection;

public interface ConnectionEvent {
	public void error(String error);
	public void messageServer(String respon);
}
