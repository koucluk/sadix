package com.bhn.sadix.connection;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ConnectionServer {
    private ConnectionEvent event;
    private Context context;
    private ProgressDialog progressDialog;
    public final static String POST = "POST";
    public final static String GET = "GET";
    private final int ERROR = 1;
    private final int SUCCES = 2;
    private String url;
    private HashMap<String, String> param = new HashMap<String, String>();
    private String method = GET;
    private String error;
    private String respon;
    private boolean progres = true;
    private boolean isCancel = false;
    private boolean lajutLoding = false;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SUCCES:
                    if (event != null) event.messageServer(respon);
                    break;
                case ERROR:
                    if (event != null && isCancel == false) event.error(error);
                    if (isCancel) {
                        event.error(error);
                    }
                    break;
            }
            if (!lajutLoding) {
                progressDialog.dismiss();
            } else {
                lajutLoding = false;
            }
        }
    };

    public void lanjutLoading() {
        lajutLoding = true;
    }

    private static final int HTTP_TIMEOUT = 60 * 1000; // milliseconds
    private DefaultHttpClient client;
    private HttpGet get;
    private HttpPost post;

    public ConnectionServer(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(this.context) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                    if (client != null) {
                        isCancel = true;
                        error = "Cancel Connection";
                        handler.sendEmptyMessage(ERROR);
                        if (method.equals(GET)) {
                            get.abort();
                        } else if (method.equals(POST)) {
                            post.abort();
                        }
                        client.getConnectionManager().shutdown();
                    }
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
    }

    public ConnectionServer() {
    }

    private static DefaultHttpClient getHttpClient() {
        DefaultHttpClient mHttpClient = new DefaultHttpClient();
        ClientConnectionManager mgr = mHttpClient.getConnectionManager();
        final HttpParams params = mHttpClient.getParams();

        mHttpClient = new DefaultHttpClient(
                new ThreadSafeClientConnManager(params,
                        mgr.getSchemeRegistry()), params);

        HttpConnectionParams.setConnectionTimeout(params, HTTP_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, HTTP_TIMEOUT);
        ConnManagerParams.setTimeout(params, HTTP_TIMEOUT);
        return mHttpClient;
    }

    public void setProgressDialog(boolean cancel) {
        progressDialog.setCancelable(cancel);
    }

    public Context getContext() {
        return context;
    }

    public void addConnectionEvent(ConnectionEvent event) {
        this.event = event;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void addParam(String key, String value) {
        param.put(key, value);
    }

    public void clearParam() {
        param.clear();
    }

    public void conncetion(boolean progres) {
        this.progres = progres;
        conncetion();
    }

    public ConnectionEvent getEvent() {
        return event;
    }

    public String connectionNonThread() {
        respon = null;
        client = ConnectionServer.getHttpClient();//new DefaultHttpClient();
        StringBuffer buffer = new StringBuffer();
        List<NameValuePair> paramPost = new ArrayList<NameValuePair>();
        Iterator<String> keys = param.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            buffer.append(key + "=" + param.get(key));
            paramPost.add(new BasicNameValuePair(key, param.get(key)));
        }
        try {
            StringBuffer bufferRespon = new StringBuffer();
            HttpResponse getResponse = null;
            if (method.equals(GET)) {
                get = new HttpGet(url + (buffer.toString().equals("") ? "" : "?" + buffer.toString()));
                getResponse = client.execute(get);
            } else if (method.equals(POST)) {
                post = new HttpPost(url);
                UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramPost);
                post.setEntity(formEntity);
                getResponse = client.execute(post);
            }
            final int statusCode = getResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                error = "Error Code " + statusCode;
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(getResponse.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                respon = bufferRespon.toString();
            }
        } catch (Exception e) {
        }
        return respon;
    }

    public static Drawable getDrawableFromStream(String address) {
        try {
            URL url = new URL(address);
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            return d;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void uploadImage(List<Bitmap> bitmaps, List<String> paramName) {
        List<byte[]> list = new ArrayList<byte[]>();
        for (Bitmap bitmap : bitmaps) {
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
            list.add(bao.toByteArray());
        }
        uploadByteData(list, paramName);
    }

    public void uploadByteData(final List<byte[]> data, final List<String> paramName) {
        if (progres) progressDialog.show();
        Thread th = new Thread() {
            public void run() {
                try {
                    StringBuffer bufferRespon = new StringBuffer();
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    for (int i = 0; i < data.size(); i++) {
                        byte[] ds = data.get(i);
                        nameValuePairs.add(new BasicNameValuePair(paramName.get(i), Base64.encodeBytes(ds)));
                    }
                    HttpClient httpclient = ConnectionServer.getHttpClient();//new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(url);
                    UrlEncodedFormEntity obj = new UrlEncodedFormEntity(nameValuePairs);
                    obj.setChunked(true);
                    httppost.setEntity(obj);
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) {
                        bufferRespon.append(line);
                    }
                    respon = bufferRespon.toString();
                    handler.sendEmptyMessage(SUCCES);
                } catch (Exception e) {
                    error = "Error Connection";
                    handler.sendEmptyMessage(ERROR);
                }
            }
        };
        th.start();
    }

    public void conncetion() {
        isCancel = false;
        if (progres) progressDialog.show();
        Thread th = new Thread() {
            public void run() {
                client = ConnectionServer.getHttpClient();//new DefaultHttpClient();
                StringBuffer buffer = new StringBuffer();
                List<NameValuePair> paramPost = new ArrayList<NameValuePair>();
                Iterator<String> keys = param.keySet().iterator();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    buffer.append(key + "=" + param.get(key));
                    paramPost.add(new BasicNameValuePair(key, param.get(key)));
                }
                try {
                    StringBuffer bufferRespon = new StringBuffer();
                    HttpResponse getResponse = null;
                    if (method.equals(GET)) {
                        get = new HttpGet(url + (buffer.toString().equals("") ? "" : "?" + buffer.toString()));
                        getResponse = client.execute(get);
                    } else if (method.equals(POST)) {
                        post = new HttpPost(url);
                        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramPost);
                        post.setEntity(formEntity);
                        getResponse = client.execute(post);
                    }
                    final int statusCode = getResponse.getStatusLine().getStatusCode();
                    if (statusCode != HttpStatus.SC_OK) {
                        if (!isCancel) {
                            error = "Error Code " + statusCode;
                            handler.sendEmptyMessage(ERROR);
                        }
                    } else {
                        BufferedReader rd = new BufferedReader(new InputStreamReader(getResponse.getEntity().getContent()));
                        String line = "";
                        while ((line = rd.readLine()) != null) {
                            bufferRespon.append(line);
                        }
                        respon = bufferRespon.toString();
                        handler.sendEmptyMessage(SUCCES);
                    }
                } catch (Exception e) {
                    if (!isCancel) {
                        error = "Error Connection";
                        handler.sendEmptyMessage(ERROR);
                    }
                }
            }
        };
        th.start();
    }

    public String requestJSONObjectNonThread(JSONObject json) {
        client = ConnectionServer.getHttpClient();//new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                if (!isCancel) {
                    error = "Error Code " + statusCode;
                    return null;
                }
            } else if (statusCode == 500) {
                error = "Data gagal terkirim, data akan tersimpan diinternal memory";
                return null;
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                respon = bufferRespon.toString();
            }
            return respon;

        } catch (Exception e) {
            Log.e("requestJSONObjectNonThread", ">>" + e.getMessage());
//			error = e.getMessage();
            error = "Koneksi gagal, data akan tersimpan diinternal memory";
            return null;
        }
    }

    public String requestJSONObjectNonThreadArray(JSONArray json) {
        client = ConnectionServer.getHttpClient();//new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                if (!isCancel) {
                    error = "Error Code " + statusCode;
                    return null;
                }
            } else if (statusCode == 500) {
                error = "Data gagal terkirim, data akan tersimpan diinternal memory";
                return null;
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                respon = bufferRespon.toString();
            }
            return respon;

        } catch (Exception e) {
            Log.e("requestJSONObjectNonThread", ">>" + e.getMessage());
//			error = e.getMessage();
            error = "Koneksi gagal, data akan tersimpan diinternal memory";
            return null;
        }
    }

    public String requestJSONObjectNonThread(JSONArray json) {
        client = ConnectionServer.getHttpClient();//new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                if (!isCancel) {
                    error = "Error Code " + statusCode;
                    return null;
                }
            } else if (statusCode == 500) {
                error = "Data gagal terkirim, data akan tersimpan diinternal memory";
                return null;
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                respon = bufferRespon.toString();
            }
            return respon;

        } catch (Exception e) {
            Log.e("requestJSONObjectNonThread", ">>" + e.getMessage());
//			error = e.getMessage();
            error = "Koneksi gagal, data akan tersimpan diinternal memory";
            return null;
        }
    }

    public void requestJSONObject(final JSONObject json) {
        isCancel = false;
        if (progres) progressDialog.show();
        Thread th = new Thread() {
            public void run() {
                respon = requestJSONObjectNonThread(json);
                if (respon == null) {
                    handler.sendEmptyMessage(ERROR);
                } else {
                    handler.sendEmptyMessage(SUCCES);
                }
            }
        };
        th.start();
    }

    public void requestJSONObjectArray(final JSONArray json) {
        isCancel = false;
        if (progres) progressDialog.show();
        Thread th = new Thread() {
            public void run() {
                respon = requestJSONObjectNonThread(json);
                if (respon == null) {
                    handler.sendEmptyMessage(ERROR);
                } else {
                    handler.sendEmptyMessage(SUCCES);
                }
            }
        };
        th.start();
    }

    public static String requestJSONObjectNonThread(JSONObject json, String url, boolean isTimeOut) {
        DefaultHttpClient client = null;
        if (isTimeOut) client = getHttpClient();
        else client = new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            HttpGet get = null;
            HttpPost post = null;
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                android.util.Log.e("error", bufferRespon.toString());
                return "Error Code:" + statusCode;
            } else if (statusCode == 500) {
                JSONObject jsonError = new JSONObject();
                try {
                    jsonError.put("Result", "GAGAL");
                    jsonError.put("Message", "Data gagal terkirim, data akan tersimpan diinternal memory");
                } catch (Exception e2) {
                }
                return jsonError.toString();
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                return bufferRespon.toString();
            }

        } catch (Exception e) {
            JSONObject jsonError = new JSONObject();
            try {
                jsonError.put("Result", "GAGAL");
                jsonError.put("Message", "Koneksi gagal, data akan tersimpan diinternal memory");
            } catch (Exception e2) {
            }
            return jsonError.toString();
        }
    }

    public static String requestJSONObjectNonThread(JSONArray json, String url, boolean isTimeOut) {
        DefaultHttpClient client = null;
        if (isTimeOut) client = getHttpClient();
        else client = new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            HttpGet get = null;
            HttpPost post = null;
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                return "Error Code:" + statusCode;
            } else if (statusCode == 500) {
                JSONObject jsonError = new JSONObject();
                try {
                    jsonError.put("Result", "GAGAL");
                    jsonError.put("Message", "Data gagal terkirim, data akan tersimpan diinternal memory");
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return jsonError.toString();
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                return bufferRespon.toString();
            }

        } catch (Exception e) {
            JSONObject jsonError = new JSONObject();
            try {
                jsonError.put("Result", "GAGAL");
                jsonError.put("Message", "Koneksi gagal, data akan tersimpan diinternal memory");
            } catch (Exception e2) {
            }
            return jsonError.toString();
        }
    }

    public void requestJSONObjectNoTimeOut(final JSONObject json) {
        isCancel = false;
        if (progres) progressDialog.show();
        Thread th = new Thread() {
            public void run() {
                respon = requestJSONObjectNonThreadNoTimeOut(json);
                if (respon == null) {
                    handler.sendEmptyMessage(ERROR);
                } else {
                    handler.sendEmptyMessage(SUCCES);
                }
            }
        };
        th.start();
    }

    public String requestJSONObjectNonThreadNoTimeOut(JSONObject json) {
        client = new DefaultHttpClient();
        try {
            HttpResponse response = null;
            StringBuffer bufferRespon = new StringBuffer();
            if (json == null) {
                get = new HttpGet(url);
                response = client.execute(get);
            } else {
                post = new HttpPost(url);
                StringEntity se = new StringEntity(json.toString());
                post.setEntity(se);
                post.setHeader("Accept", "application/json, text/javascript, text/html, */*;q=0.01");
                post.setHeader("Accept-Charset", "utf-8, ISO-8859-1;q=0.7, *;q=0.3");
                post.setHeader("Accept-Encoding", "gzip,deflate,sdch");
                post.setHeader("Content-Type", "application/json; charset=utf-8");
                response = client.execute(post);
            }
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                if (!isCancel) {
                    error = "Error Code " + statusCode;
                    return null;
                }
            } else if (statusCode == 500) {
                error = "Data gagal terkirim, data akan tersimpan diinternal memory";
                return null;
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = rd.readLine()) != null) {
                    bufferRespon.append(line);
                }
                respon = bufferRespon.toString();
            }
            return respon;

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("requestJSONObjectNonThread", e.getMessage());
//			error = e.getMessage();
            error = "Koneksi gagal, data akan tersimpan diinternal memory";
            return null;
        }
    }
}
