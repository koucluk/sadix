package com.bhn.sadix;

import java.util.List;

import com.bhn.sadix.database.CtrlInformasi;
import com.bhn.sadix.model.InformasiModel;
import com.bhn.sadix.widget.InformationWidget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

public class InformationActivity extends AppCompatActivity {
    private CtrlInformasi ctrlInformasi;
    Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_kunjungan_informasi);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctrlInformasi = new CtrlInformasi(this);
        List<InformasiModel> list = ctrlInformasi.list();
        for (InformasiModel informasiModel : list) {
            ((LinearLayout) findViewById(R.id.container)).addView(new InformationWidget(this, informasiModel));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            finish();
        }
        return true;
    }

}
