package com.bhn.sadix;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.adapter.SummeryVisitAdapter;
import com.bhn.sadix.model.MenuUtamaModel;

import java.util.ArrayList;
import java.util.Calendar;

public class SummeryVisitActivity extends AppCompatActivity implements OnItemClickListener {

    private SummeryVisitAdapter adapter;
    private MenuUtamaAdapter adapterMenu;
    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence sDrawerTitle;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summery_visit);

        /*getSupportActionBar().setHomeButtonEnabled(true);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new SummeryVisitAdapter(this);
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        adapterMenu = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapterMenu.setNotifyOnChange(true);
        adapterMenu.add(new MenuUtamaModel(1, "Sunday", getResources().getColor(R.color.black), true, "Minggu"));
        adapterMenu.add(new MenuUtamaModel(2, "Monday", getResources().getColor(R.color.black), true, "Senin"));
        adapterMenu.add(new MenuUtamaModel(3, "Tuesday", getResources().getColor(R.color.black), true, "Selasa"));
        adapterMenu.add(new MenuUtamaModel(4, "Wednesday", getResources().getColor(R.color.black), true, "Rabu"));
        adapterMenu.add(new MenuUtamaModel(5, "Thursday", getResources().getColor(R.color.black), true, "Kamis"));
        adapterMenu.add(new MenuUtamaModel(6, "Friday", getResources().getColor(R.color.black), true, "Jumat"));
        adapterMenu.add(new MenuUtamaModel(7, "Saturday", getResources().getColor(R.color.black), true, "Sabtu"));
        adapterMenu.add(new MenuUtamaModel(8, "Close", getResources().getColor(R.color.red), true, "Close"));
        mDrawerList.setAdapter(adapterMenu);
        mDrawerList.setOnItemClickListener(this);


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

        Calendar cal = Calendar.getInstance();
        int hari = cal.get(Calendar.DAY_OF_WEEK);
        MenuUtamaModel menu = adapterMenu.getItem(adapterMenu.getPosition(new MenuUtamaModel(hari)));
        mDrawerTitle = getTitle();
        setTitle("Summary Visit (" + (menu != null ? menu.caption : "") + ")");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapterMenu.getItem(position);
        if (model.id == 8) {
            finish();
        } else {
            Log.d("Model Caption", model.caption);
            getSupportActionBar().setTitle("Summary Visit (" + model.caption + ")");
//			setTitle("Summary Visit ("+model.caption+")");
            mDrawerList.setItemChecked(position, true);
            mDrawerLayout.closeDrawer(mDrawerList);
//	        mDrawerTitle = getTitle();
            adapter.loadData(model.id);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Calendar cal = Calendar.getInstance();
        int hari = cal.get(Calendar.DAY_OF_WEEK);
        adapter.loadData(hari);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == android.R.id.home) {
//        	finish();
//        }
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adapter != null) {
            adapter.destroy();
        }
    }
}
