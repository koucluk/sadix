package com.bhn.sadix;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlSurvey;
import com.bhn.sadix.model.CommonSurveyModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.FormSurveyModel;
import com.bhn.sadix.model.HasilSurveyModel;
import com.bhn.sadix.model.SurveyModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.survey.SurveyWidget;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import io.realm.Realm;

public class SurveyActivity extends AppCompatActivity implements ConnectionEvent {
    private CtrlSurvey ctrlSurvey;
    //    private LocationManager locationManager;
    private Toolbar toolbar;
    private Location location;
    private String GeoLat = "0";
    private String GeoLong = "0";
    private String BeginSurveyTime;
    private String EndSurveyTime;
    private FormSurveyModel formSurveyModel;
    private CustomerModel customerModel;
    private boolean isDone = false;
    Realm realm;
    CommonModel commonModel;

    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            GeoLat = String.format("%1$s", location.getLatitude());
            GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private ConnectionServer server;
    private Messenger mService;
    final Messenger mMessenger = new Messenger(new LocationHandler());
    private String commonId;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
            try {
                Message msg = Message.obtain(null, SadixService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };
    private boolean isBound = false;

    class LocationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case SadixService.MSG_SEND_LOC:
                    Bundle bundle = msg.getData();
                    location = new Location("");
                    location.setLatitude(bundle.getDouble("lat"));
                    location.setLongitude(bundle.getDouble("long"));
                    location.setTime(bundle.getLong("time"));

                    Log.d("Loc Survey - lat", bundle.getDouble("lat") + "");
                    Log.d("Loc Survey - long", bundle.getDouble("long") + "");
                    Log.d("Loc Survey - time", bundle.getLong("time") + "");

                    GeoLat = String.format("%1$s", location.getLatitude());
                    GeoLong = String.format("%1$s", location.getLongitude());
                    break;
            }
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        realm = Realm.getDefaultInstance();
        commonModel = realm.where(CommonModel.class).equalTo("Done", 0).findFirst();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        CheckIfServiceIsRunning();

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctrlSurvey = new CtrlSurvey(this);
        /*try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }*/
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        BeginSurveyTime = Util.dateFormat.format(new Date());
        commonId = commonModel.CommonID;
        formSurveyModel = (FormSurveyModel) getIntent().getSerializableExtra("FormSurveyModel");
        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        load();
    }

    private void CheckIfServiceIsRunning() {
        if (SadixService.isRunning()) {
            bindService(new Intent(this, SadixService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            isBound = true;
        }
    }

    private void load() {
//        List<HasilSurveyModel> hasilSurveyModels = ctrlSurvey.listHasilSurvey(commonId);
        if (formSurveyModel != null) {
            setTitle(formSurveyModel.FormName);
            List<SurveyModel> list = ctrlSurvey.list(formSurveyModel);
            ((LinearLayout) findViewById(R.id.panel)).removeAllViews();
            for (SurveyModel surveyModel : list) {
                ((LinearLayout) findViewById(R.id.panel)).addView(new SurveyWidget(this, surveyModel, commonId));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Save").setIcon(android.R.drawable.ic_menu_save)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Save").setIcon(android.R.drawable.ic_menu_save);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Util.confirmDialog(this, "Batal Survey", "Ingin membatalkan survey?", new ConfirmListener() {
                @Override
                public void onDialogCompleted(boolean answer) {
                    if (answer) {
                        ctrlSurvey.deleteCommonSurvey(commonId);
                        finish();
                    }
                }
            });
        } else if (item.getItemId() == 1) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        try {
            EndSurveyTime = Util.dateFormat.format(new Date());
            server.setUrl(Util.getServerUrl(this) + "endsurvey");
            JSONObject json = new JSONObject();
            JSONObject Common = new JSONObject();
            Common.put("CustomerId", Integer.parseInt(customerModel.CustId));
            Common.put("SalesId", Integer.parseInt(UserModel.getInstance(this).SalesId));
            Common.put("GeoLat", Double.parseDouble(GeoLat));
            Common.put("GeoLong", Double.parseDouble(GeoLong));
            Common.put("BeginSurveyTime", BeginSurveyTime);
            Common.put("EndSurveyTime", EndSurveyTime);
            Common.put("FormId", formSurveyModel.FormId);
            JSONArray Survey = new JSONArray();
            for (int i = 0; i < ((LinearLayout) findViewById(R.id.panel)).getChildCount(); i++) {
                View view = ((LinearLayout) findViewById(R.id.panel)).getChildAt(i);
                if (view instanceof SurveyWidget) {
                    SurveyWidget surveyWidget = (SurveyWidget) view;
                    JSONArray array = surveyWidget.getListID();
                    for (int j = 0; j < array.length(); j++) {
                        Survey.put(array.getJSONObject(j));
                    }
                }
            }
            json.put("Common", Common);
            json.put("Survey", Survey);
//			error(json.toString());
            server.requestJSONObject(json);
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void error(String error) {
        isDone = true;
        saveLokal(true);
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                isDone = true;
                Util.Toast(this, json.getString("Message"));
                ctrlSurvey.deleteCommonSurvey(commonId);
                finish();
            } else {
                error(json.getString("Message"));
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isDone)
            saveLokal(false);
    }

    private void saveLokal(boolean send) {
//        String commonId = "S" + Util.FORMAT_ID.format(new Date());

        ctrlSurvey.deleteCommonSurvey(commonId);

        CommonSurveyModel commonSurveyModel = new CommonSurveyModel(
                commonId,
                customerModel.CustId,
                UserModel.getInstance(this).SalesId,
                GeoLat,
                GeoLong,
                BeginSurveyTime,
                EndSurveyTime,
                formSurveyModel.FormId
        );

        if (!send) {
            commonSurveyModel.done = 0;
        } else {
            commonSurveyModel.done = 1;
        }

        ctrlSurvey.saveCommonSurvey(commonSurveyModel);

        for (int i = 0; i < ((LinearLayout) findViewById(R.id.panel)).getChildCount(); i++) {
            Log.wtf("Saving survey", "index " + i);
            View view = ((LinearLayout) findViewById(R.id.panel)).getChildAt(i);
            if (view instanceof SurveyWidget) {
                SurveyWidget surveyWidget = (SurveyWidget) view;
                if (surveyWidget.surveyModel.SurveyType.equals("0")) {

                    HasilSurveyModel hasilSurveyModel = new HasilSurveyModel(
                            commonId,
                            surveyWidget.surveyModel.FormId,
                            surveyWidget.surveyModel.SurveyId,
                            surveyWidget.ListID,
                            ((EditText) surveyWidget.findViewById(R.id.panelText)).getText().toString()

                    );

                    Log.d("Save Survey 0", ((EditText) surveyWidget.findViewById(R.id.panelText)).getText().toString());
                    ctrlSurvey.saveHasilSurvey(hasilSurveyModel);
                } else if (surveyWidget.surveyModel.SurveyType.equals("3")) {
                    HasilSurveyModel hasilSurveyModel = new HasilSurveyModel(
                            commonId,
                            surveyWidget.surveyModel.FormId,
                            surveyWidget.surveyModel.SurveyId,
                            surveyWidget.ListID,
                            ((EditText) surveyWidget.findViewById(R.id.panelNumber)).getText().toString()
                    );
                    Log.d("Save Survey 3", ((EditText) surveyWidget.findViewById(R.id.panelText)).getText().toString());
                    ctrlSurvey.saveHasilSurvey(hasilSurveyModel);
                } else if (surveyWidget.surveyModel.SurveyType.equals("2")) {
                    View viewDetail = ((LinearLayout) surveyWidget.findViewById(R.id.panel)).getChildAt(0);
                    if (viewDetail instanceof RadioGroup) {
                        RadioGroup radioGroup = (RadioGroup) viewDetail;
                        HasilSurveyModel hasilSurveyModel = new HasilSurveyModel(
                                commonId,
                                surveyWidget.surveyModel.FormId,
                                surveyWidget.surveyModel.SurveyId,
                                String.valueOf(radioGroup.getCheckedRadioButtonId() == -1 ? 0 : radioGroup.getCheckedRadioButtonId()),
                                ""
                        );
                        Log.d("Save Survey 2", String.valueOf(radioGroup.getCheckedRadioButtonId() == -1 ? 0 : radioGroup.getCheckedRadioButtonId()));
                        ctrlSurvey.saveHasilSurvey(hasilSurveyModel);
                    }
                } else if (surveyWidget.surveyModel.SurveyType.equals("1")) {
                    for (int j = 0; j < ((LinearLayout) surveyWidget.findViewById(R.id.panel)).getChildCount(); j++) {
                        View viewDetail = ((LinearLayout) surveyWidget.findViewById(R.id.panel)).getChildAt(j);
                        if (viewDetail instanceof CheckBox) {
                            CheckBox checkBox = (CheckBox) viewDetail;
                            if (checkBox.isChecked()) {
                                HasilSurveyModel hasilSurveyModel = new HasilSurveyModel(
                                        commonId,
                                        surveyWidget.surveyModel.FormId,
                                        surveyWidget.surveyModel.SurveyId,
                                        String.valueOf(checkBox.getId()),
                                        ""
                                );
                                Log.d("Save Survey 1", String.valueOf(checkBox.getId()));
                                ctrlSurvey.saveHasilSurvey(hasilSurveyModel);
                            }
                        }
                    }
                }
            }
        }
        Util.Toast(this, "Data tersimpan dilokal");
        finish();
    }

    @Override
    protected void onDestroy() {
        if (isBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, SadixService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            unbindService(serviceConnection);
            isBound = false;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Util.confirmDialog(this, "Batal Survey", "Data survey belum terkirim, Ingin membatalkan survey?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    finish();
                }
            }
        });
    }
}
