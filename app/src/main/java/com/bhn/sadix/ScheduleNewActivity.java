package com.bhn.sadix;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.bhn.sadix.adapter.CustomerAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.util.Util;
import com.makeramen.segmented.SegmentedRadioGroup;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class ScheduleNewActivity extends AppCompatActivity implements OnItemClickListener {
    private ViewPager mPager;
    private ViewPagerAdapter pagerAdapter;
    private ProgressDialog progressDialog;
    private CustomerAdapter adapterScheduler;
    private CustomerAdapter adapterAll;
    private View vScheduler;
    private View vAll;

    public DrawerLayout mDrawerLayout;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private MenuUtamaAdapter adapter;
    private CtrlCustomer ctrlCustomer;
    private String day = "";
    private String filter = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        ctrlCustomer = new CtrlCustomer(this);

        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new MenuUtamaModel(1, "Sunday", getResources().getColor(R.color.black), true, "Minggu"));
        adapter.add(new MenuUtamaModel(2, "Monday", getResources().getColor(R.color.black), true, "Senin"));
        adapter.add(new MenuUtamaModel(3, "Tuesday", getResources().getColor(R.color.black), true, "Selasa"));
        adapter.add(new MenuUtamaModel(4, "Wednesday", getResources().getColor(R.color.black), true, "Rabu"));
        adapter.add(new MenuUtamaModel(5, "Thursday", getResources().getColor(R.color.black), true, "Kamis"));
        adapter.add(new MenuUtamaModel(6, "Friday", getResources().getColor(R.color.black), true, "Jum'at"));
        adapter.add(new MenuUtamaModel(7, "Saturday", getResources().getColor(R.color.black), true, "Sabtu"));
        adapter.add(new MenuUtamaModel(8, "Close", getResources().getColor(R.color.red), true, "Close"));
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(this);
        int index = adapter.getPosition(new MenuUtamaModel(new SimpleDateFormat("EEEE").format(new Date())));
        if (index != -1) {
            MenuUtamaModel model = adapter.getItem(index);
            if (model != null) {
                day = "D" + model.id;
                ((RadioButton) findViewById(R.id.tab_day)).setText(model.caption);
            }
        }

        mTitle = mDrawerTitle = getTitle();

        ((SegmentedRadioGroup) findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_day) {
                    mPager.setCurrentItem(0);
                } else if (checkedId == R.id.tab_all_cust) {
                    mPager.setCurrentItem(1);
                }
            }
        });
        ((RadioButton) findViewById(R.id.tab_day)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        ((RadioButton) findViewById(R.id.tab_all_cust)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(1);
            }
        });
        mPager = (ViewPager) findViewById(R.id.pager);
//        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
//            @Override
//            public void onPageSelected(int position) {
//                super.onPageSelected(position);
//                if(position == 0) {
//					loadScheduler();
//                	((SegmentedRadioGroup)findViewById(R.id.tab)).check(R.id.tab_day);
//                } else if(position == 1) {
//                	loadScheduler();
//                	((SegmentedRadioGroup)findViewById(R.id.tab)).check(R.id.tab_all_cust);
//                }
//            }
//        };
//        mPager.setOnPageChangeListener(ViewPagerListener);
        pagerAdapter = new ViewPagerAdapter();
        mPager.setAdapter(pagerAdapter);

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
        if (model.id == 8) {
            finish();
        } else {
            day = "D" + model.id;
            ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_day);
            ((RadioButton) findViewById(R.id.tab_day)).setText(model.caption);
        }
    }

    private void loadScheduler() {
        progressDialog.show();
        new LoadDataScheduler().execute();
    }

    private void loadAll() {
        progressDialog.show();
        new LoadDataAll().execute();
    }

    private class LoadDataAll extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                adapterAll = new CustomerAdapter(getApplicationContext(), ctrlCustomer.list());
            } catch (Exception e) {
                Log.e("Exception1", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (vAll != null) {
                ((ListView) vAll.findViewById(R.id.list)).setAdapter(adapterAll);
                adapterAll.notifyDataSetChanged();
            }
            super.onPostExecute(result);
        }
    }

    private class LoadDataScheduler extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                adapterScheduler = new CustomerAdapter(getApplicationContext(), ctrlCustomer.listByDay(day));
            } catch (Exception e) {
                Log.e("Exception2", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (vScheduler != null) {
                ((ListView) vScheduler.findViewById(R.id.list)).setAdapter(adapterScheduler);
                adapterScheduler.notifyDataSetChanged();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends PagerAdapter {
        final int PAGE_COUNT = 2;

        @Override
        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) ScheduleNewActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_schedule_item, null);
            if (position == 0) {
                vScheduler = v;
                loadScheduler();
            } else if (position == 1) {
                vAll = v;
                loadAll();
            }
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }

}
