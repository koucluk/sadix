package com.bhn.sadix;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bhn.sadix.adapter.HistoryAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.HistoryDetailModel;
import com.bhn.sadix.model.HistoryModel;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderHistoryActivity extends AppCompatActivity implements ConnectionEvent {
    private CustomerModel customerModel;
    private CustomerTypeModel customerTypeModel;
    private ConnectionServer server;
    private HistoryAdapter adapter;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        adapter = new HistoryAdapter(this, new ArrayList<HistoryModel>(), new HashMap<HistoryModel, List<HistoryDetailModel>>());
        ((ExpandableListView) findViewById(R.id.list)).setAdapter(adapter);
        load();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void load() {
        if (adapter.getGroupCount() == 0) {
            server.setUrl(Util.getServerUrl(this) + "gethistory/CustomerId/" + customerModel.CustId);
            server.requestJSONObject(null);
        }
    }

    @Override
    public void error(String error) {
        if (error.startsWith("Koneksi gagal")) {
            Util.showDialogError(this, "Tidak bisa menampilkan data history, karena signal tidak ada");
        } else {
            Util.showDialogError(this, error);
        }
//		Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONArray json = new JSONArray(respon);
            for (int i = 0; i < json.length(); i++) {
                JSONObject jsonObj = json.getJSONObject(i);
                JSONObject Transaction = jsonObj.getJSONObject("Transaction");
                HistoryModel historyModel = new HistoryModel(
                        Transaction.getString("TransactionId"),
                        Transaction.getString("TransactionTime"),
                        Transaction.getString("CustomerId"),
                        Transaction.getDouble("Total"),
                        Transaction.getDouble("Diskon"),
                        Transaction.getDouble("TypeDiskon"),
                        Transaction.getDouble("GrandTotal"),
                        Transaction.getString("Desc")
                );
                adapter.addHeader(historyModel);
                JSONArray Details = jsonObj.getJSONArray("Details");
                for (int j = 0; j < Details.length(); j++) {
                    JSONObject jsonDtl = Details.getJSONObject(j);
                    adapter.addChild(historyModel, new HistoryDetailModel(
                            jsonDtl.getString("TransactionId"),
                            jsonDtl.getString("TransactionIds"),
                            jsonDtl.getString("GroupName"),
                            jsonDtl.getString("Name"),
                            jsonDtl.getDouble("Price"),
                            jsonDtl.getInt("Qty"),
                            jsonDtl.getDouble("Price2"),
                            jsonDtl.getInt("Qty2"),
                            jsonDtl.getString("Satuan1"),
                            jsonDtl.getString("Satuan2"),
                            jsonDtl.getDouble("Discount1"),
                            jsonDtl.getDouble("Discount2"),
                            jsonDtl.getDouble("Discount3"),
                            jsonDtl.getDouble("Total"),
                            jsonDtl.getDouble("GrandTotal"),
                            jsonDtl.getDouble("T_Discount"),
                            jsonDtl.getString("SalesProgramDescription")
                    ));
                }
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

}
