package com.bhn.sadix;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.VisitTypeCalendarDate;
import com.bhn.sadix.widget.VisitTypeCalendarTime;
import com.bhn.sadix.widget.VisitTypeFreeText;
import com.bhn.sadix.widget.VisitTypeMultipleChoice;
import com.bhn.sadix.widget.VisitTypeNumber;
import com.bhn.sadix.widget.VisitTypePhoto;
import com.bhn.sadix.widget.VisitTypeSIngleChoice;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

//import com.bhn.sadix.model.CommonModel;

public class VisitActivity extends AppCompatActivity implements VisitTypePhoto.VisitTypePhotoListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private CustomerTypeModel customerTypeModel;
    private LinearLayout panel;
    private DbMasterHelper db;
    //    private CommonModel commonModel;
    private CommonModel commonModel;
    private Realm realmUI;
    private CustomerModel customerModel;
    private VisitTypePhoto visitTypePhoto;
    private CtrlAppModul ctrlAppModul;
    private Location location;
    private Messenger mService;
    GoogleApiClient mGoogleApiClient;
    final Messenger mMessenger = new Messenger(new LocationHandler());

    private Toolbar toolbar;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
            try {
                Message msg = Message.obtain(null, SadixService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    private boolean isBound = false;
    private boolean isConnected = false;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isConnected = true;
        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        CheckIfServiceIsRunning();

        initView();
    }

    private void initView() {
        CheckIfServiceIsRunning();

        panel = (LinearLayout) findViewById(R.id.panel);
        try {
            List<JSONObject> list = db.queryListJSON("select * from visit_note where CustType='" + customerTypeModel.CustomerTypeId + "'");

            Date beginVisit = Util.dateFormat.parse(commonModel.BeginVisitTime);
            Date endVisit = Util.dateFormat.parse(Util.dateFormat.format(new Date()));

            Log.d("BEGIN VISIT", commonModel.BeginVisitTime);
            Log.d("END VISIT", Util.dateFormat.format(new Date()));

            //get minutes threshold
            long difference = endVisit.getTime() - beginVisit.getTime();
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int minDifference = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

            Log.d("DIFFERENCE", minDifference + "");

            float distance = getDistanceVisit(location.getLatitude(), location.getLongitude(), Double.parseDouble(customerModel.GeoLat), Double.parseDouble(customerModel.GeoLong));

            Log.d("DISTANCE", distance + "");

            boolean anyView = false;

            if (list.size() > 0) {
                for (JSONObject jsonObject : list) {
                    Gson gson = new Gson();
                    VisitNote visitNote = gson.fromJson(jsonObject.toString(), VisitNote.class);

                    boolean GeoStatus = false;
                    boolean DurStatus = false;

                    if (visitNote.getGeoID() == 0) {
                        switch (visitNote.getGeoOpr()) {
                            case Util.GREATER_THAN:
                                if (distance > visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (distance >= visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (distance < visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                        }

                        switch (visitNote.getDurOpr()) {
                            case Util.GREATER_THAN:
                                if (minDifference > visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (minDifference >= visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (minDifference < visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                        }

                        if (GeoStatus || DurStatus) {
                            //showPanel
                            addView(visitNote.getVisitType(), visitNote, commonModel.CommonID);
                            anyView = true;
                        }
                    } else if (visitNote.getGeoID() == 2 && visitNote.getDurID() == 2) {
                        switch (visitNote.getGeoOpr()) {
                            case Util.GREATER_THAN:
                                if (distance > visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (distance >= visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (distance < visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                        }

                        switch (visitNote.getDurOpr()) {
                            case Util.GREATER_THAN:
                                if (minDifference > visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (minDifference >= visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (minDifference < visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                        }

                        if (GeoStatus && DurStatus) {
                            //showPanel
                            addView(visitNote.getVisitType(), visitNote, commonModel.CommonID);
                            anyView = true;
                        }
                    } else if (visitNote.getGeoID() == 2) {
                        switch (visitNote.getGeoOpr()) {
                            case Util.GREATER_THAN:
                                if (distance > visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (distance >= visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (distance < visitNote.getGeoValue()) {
                                    GeoStatus = true;
                                }
                                break;
                        }
                        if (GeoStatus) {
                            //showPanel
                            addView(visitNote.getVisitType(), visitNote, commonModel.CommonID);
                            anyView = true;
                        }
                    } else if (visitNote.getDurID() == 2) {
                        switch (visitNote.getDurOpr()) {
                            case Util.GREATER_THAN:
                                if (minDifference > visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.GREATER_THAN_EQUALS:
                                if (minDifference >= visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                            case Util.LESS_THAN:
                                if (minDifference < visitNote.getDurValue()) {
                                    DurStatus = true;
                                }
                                break;
                        }
                        if (DurStatus) {
                            //showPanel
                            addView(visitNote.getVisitType(), visitNote, commonModel.CommonID);
                            anyView = true;
                        }
                    }
                }

                if (!anyView) {
                    save();
                }
            } else {
                /*Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();*/
                save();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google Connect Failed", Toast.LENGTH_SHORT).show();
    }

    class LocationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SadixService.MSG_SEND_LOC:
                    Bundle bundle = msg.getData();

                    location = new Location("");
                    location.setLatitude(bundle.getDouble("lat"));
                    location.setLongitude(bundle.getDouble("long"));
                    location.setTime(bundle.getLong("time"));

                    Log.d("First Loc", "lat : " + bundle.getDouble("lat") + " long: " + bundle.getDouble("long"));
                    break;
            }
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        db = new DbMasterHelper(this);
        ctrlAppModul = new CtrlAppModul(this);
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 1).findFirst();
        customerModel = (CustomerModel) getIntent().getSerializableExtra("customerModel");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    private float getDistanceVisit(double latA, double longA, double latB, double longB) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(latB - latA);
        double dLng = Math.toRadians(longB - longA);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(latA)) * Math.cos(Math.toRadians(latB)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    private void CheckIfServiceIsRunning() {
        if (SadixService.isRunning()) {
            Log.d("Service binding", "Binded");
            bindService(new Intent(this, SadixService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            isBound = true;
        }
    }

    private void addView(int visitType, VisitNote visitNote, String commonID) {
        switch (visitType) {
            case 0:
                panel.addView(new VisitTypeFreeText(this, visitNote, commonID));
                break;
            case 1:
                panel.addView(new VisitTypeMultipleChoice(this, visitNote, commonID));
                break;
            case 2:
                panel.addView(new VisitTypeSIngleChoice(this, visitNote, commonID));
                break;
            case 3:
                panel.addView(new VisitTypeNumber(this, visitNote, commonID));
                break;
            case 4:
                panel.addView(new VisitTypeCalendarDate(this, visitNote, commonID));
                break;
            case 5:
                panel.addView(new VisitTypeCalendarTime(this, visitNote, commonID));
                break;
            case 6:
                panel.addView(new VisitTypePhoto(this, visitNote, commonID, this));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Save")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Save");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            setResult(RESULT_CANCELED);
            finish();
        } else if (item.getItemId() == 2) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < panel.getChildCount(); i++) {
                View view = panel.getChildAt(i);
                if (view instanceof VisitTypeFreeText) {
                    VisitTypeFreeText visitType = (VisitTypeFreeText) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                } else if (view instanceof VisitTypeMultipleChoice) {
                    VisitTypeMultipleChoice visitType = (VisitTypeMultipleChoice) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    List<JSONObject> values = visitType.getValue();
                    for (JSONObject jsonObject : values) {
                        array.put(jsonObject);
                    }
                } else if (view instanceof VisitTypeSIngleChoice) {
                    VisitTypeSIngleChoice visitType = (VisitTypeSIngleChoice) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                } else if (view instanceof VisitTypeNumber) {
                    VisitTypeNumber visitType = (VisitTypeNumber) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                } else if (view instanceof VisitTypeCalendarDate) {
                    VisitTypeCalendarDate visitType = (VisitTypeCalendarDate) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                } else if (view instanceof VisitTypeCalendarTime) {
                    VisitTypeCalendarTime visitType = (VisitTypeCalendarTime) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                } else if (view instanceof VisitTypePhoto) {
                    VisitTypePhoto visitType = (VisitTypePhoto) view;
                    if (!visitType.isValid()) {
                        return;
                    }
                    array.put(visitType.getValue());
                }
            }
            Intent intent = new Intent();
            intent.putExtra("data", array.toString());
            setResult(RESULT_OK, intent);
        } catch (Exception e) {
            e.printStackTrace();
            setResult(RESULT_CANCELED);
        }
        finish();
    }

    @Override
    public void visitPhoto(VisitTypePhoto photo) {
        visitTypePhoto = photo;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 5);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    visitTypePhoto.setData(bao.toByteArray(), bitmap);
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(this, e.getMessage());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        if (isBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, SadixService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            unbindService(serviceConnection);
            isBound = false;
        }
        super.onDestroy();
    }
}
