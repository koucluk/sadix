package com.bhn.sadix;

import java.util.ArrayList;

import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class VisitCustomerActivity extends AppCompatActivity implements OnItemClickListener {
    private MenuUtamaAdapter adapter;
    private CustomerModel customerModel;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;
    private CustomerTypeModel customerTypeModel;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_customer);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }

        ctrlAppModul = new CtrlAppModul(this);
        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        if (ctrlAppModul.isModul("14"))
            adapter.add(new MenuUtamaModel(1, "Scan QR Code", getResources().getDrawable(R.drawable.scan_qr_code)));
        if (ctrlAppModul.isModul("15"))
            adapter.add(new MenuUtamaModel(2, "By Pass QR Code", getResources().getDrawable(R.drawable.by_pass_scan_qr)));
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);

        ctrlConfig = new CtrlConfig(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        if (model.id == 2) {
            byPassQRCodeDialog();
        } else if (model.id == 1) {
            scanBarcode();
        }
    }

    private void scanBarcode() {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            Util.showDialogInfo(this, "ZXing Scanner Not Found, Please Install First!");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String kode = intent.getStringExtra("SCAN_RESULT");
//		        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                String[] pecahKode = kode.split("[|]");
                if (pecahKode.length == 3) {
                    if (pecahKode[0].equalsIgnoreCase(UserModel.getInstance(this).CompanyID) && pecahKode[2].equalsIgnoreCase(customerModel.CustId)) {
                        Intent kunjungan = new Intent(this, KunjunganActivity.class);
                        kunjungan.putExtra("CustomerModel", customerModel);
                        kunjungan.putExtra("CustomerTypeModel", customerTypeModel);
                        startActivity(kunjungan);
                        finish();
                    } else {
                        Util.Toast(this, "Barcode tidak sama");
                    }
                }
            }
        }
    }

    private void byPassQRCodeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("By Pass Reason");
        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_BC_NOSCAN")));
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                Intent intent = new Intent(VisitCustomerActivity.this, KunjunganActivity.class);
                intent.putExtra("ComboBoxModel", (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem());
                intent.putExtra("CustomerTypeModel", customerTypeModel);
                intent.putExtra("CustomerModel", customerModel);
                startActivity(intent);
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

}
