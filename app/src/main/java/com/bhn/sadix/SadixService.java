package com.bhn.sadix;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlInformasi;
import com.bhn.sadix.database.CtrlLogGps;
import com.bhn.sadix.model.InformasiModel;
import com.bhn.sadix.model.LogGpsModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmResults;

public class SadixService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private ConnectionServer server;
    private Timer timer;
    private Timer timerAutoLogout;

    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_SEND_LOC = 5;
    static final int MSG_SERVICE_FAILED = 6;
//    static final int MSG_SET_INT_VALUE = 3;
//    static final int MSG_SET_STRING_VALUE = 4;

    private int countPending;

    private static boolean isRunning;

    ArrayList<Messenger> mClient = new ArrayList<>();
    Messenger messenger = new Messenger(new LocationHandler());

    //Location
    private static GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private boolean mIsInResolution;

    class LocationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClient.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClient.remove(msg.replyTo);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private TimerTask autoLogout = new TimerTask() {
        @Override
        public void run() {
            try {
                String time = Util.FORMAT_LOGOUT.format(new Date());
                String DAY_OF_LOGOUT = Util.getStringPreference(SadixService.this, "DAY_OF_LOGOUT");
                if (!time.equals(DAY_OF_LOGOUT)) {
                    Log.d("Logout", "OKE");
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.where(CommonModel.class).equalTo("Done", 0).findAll().deleteAllFromRealm();
                        }
                    });

                    Util.putPreference(SadixService.this, "isLogin", false);
                    stopService(new Intent(SadixService.this, SadixService.class));

                    realm.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    //    private LocationManager locationManager;
    private Location location;
    /*private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location loc) {
            location = loc;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
//        	if(ctrlAppModul.isModul("50") == false) {
//        		Util.Toast(SadixService.this, "Mengingatkan GPS tidak ditampilkan, namun tracking tetap jalan dengan sumber data A-GPS");
//        	}
        }
    };*/

    private CtrlLogGps ctrlLogGps;
    private CtrlInformasi ctrlInformation;

    private NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 100;
    private Notification notification;
    private boolean isForeground = false;

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        ctrlLogGps = new CtrlLogGps(this);
        ctrlInformation = new CtrlInformasi(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

//        mGoogleApiClient.connect();
//		ctrlAppModul = new CtrlAppModul(this);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        try {
//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
//        } catch (Exception e) {
//            Util.Toast(this, e.getMessage());
//        }
        server = new ConnectionServer(this);
        timer = new Timer("SadixServiceTimer");
//		timer.schedule(updateTask, 1000L, 1000L);

        timerAutoLogout = new Timer("SadixServiceAutoLogout");
        timerAutoLogout.schedule(autoLogout, 1000L, 1000L);
    }

    private Notification createNotification(boolean isSound) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, LogMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_sadix)
                .setContentTitle("Sadix")
                .setContentText("Sending order")
                .setProgress(100, 10, true)

//                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
//        android.util.Log.e("isSound", "isSound");
        if (isSound) {
//            android.util.Log.e("pake suara", "pake suara");
            notificationBuilder.setSound(defaultSoundUri);
        }

        Notification notification = notificationBuilder.build();

        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        return notification;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mGoogleApiClient.connect();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        closeNotification(true);
        try {
            mGoogleApiClient.disconnect();
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }

        timer.cancel();
        timer = null;
        timerAutoLogout.cancel();
        timerAutoLogout = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    private void closeNotification(boolean failed) {
        if (notification != null && isForeground) {
            isForeground = false;
            stopForeground(true);

            String message = failed ? "Sending Failed" : "Sending Completed";

            Intent intent = new Intent(this, LogMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo_sadix)
                    .setTicker(message)
                    .setContentIntent(pendingIntent)
                    .setContentTitle("Sadix");

            if (!failed)
                notificationBuilder.setContentText(message);
            else
                notificationBuilder.setContentText(message);
//                    .setProgress(0, 0, true)

//                .setSound(defaultSoundUri)
            Notification notif = notificationBuilder.build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;

            mNotificationManager.notify(NOTIFICATION_ID, notif);
//            notification = null;
        }
    }

    public static GoogleApiClient getGoogleClient() {
        return mGoogleApiClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        final PendingResult<LocationSettingsResult> locationResult = requestLocation();

        locationResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates states = locationSettingsResult.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                        if (location != null) {

                        }

                        startLocationUpdate();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        /*try {
//                            status.startResolutionForResult(, Util.REQUEST_CHECK_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }*/
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        int time = Integer.parseInt(UserModel.getInstance(SadixService.this).Interval);

//        timer.cancel();
        timer.schedule(getUpdateTask(), 1000L, time * 60 * 1000L);
    }

    public TimerTask getUpdateTask() {

        TimerTask updateTask = new TimerTask() {
            @Override
            public void run() {
                CtrlCustomer ctrlCustomer = new CtrlCustomer(SadixService.this);
                CtrlCommon ctrlCommon = new CtrlCommon(SadixService.this);

                Realm realm = Realm.getDefaultInstance();
//			int pending = ctrlCustomer.pendingData();
//			pending += ctrlCommon.pendingData();

                final int[] pendingCommon = new int[1];

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<CommonModel> cm = realm.where(CommonModel.class)
                                .equalTo("Done", 1)
                                .equalTo("Status", "0")
//                    .isNotNull("EndVisitTime")
                                .findAll();

                        pendingCommon[0] = cm.size();
                    }
                });

                Log.wtf("pending common", pendingCommon[0] + "");

                int pending = 0;
                try {
                    pending = ctrlCustomer.pendingData();
                    pending += pendingCommon[0];
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (pending > 0) {
                    if (countPending > 5) {
                        closeNotification(true);
                        countPending = 0;
                    } else {
                        countPending += 1;
                        Log.d("Count Pending", countPending + "");
                        if (isForeground) {
                            notification = createNotification(false);
                            mNotificationManager.notify(NOTIFICATION_ID, notification);
                        } else {
                            isForeground = true;
                            notification = createNotification(true);
                            stopForeground(false);
                            mNotificationManager.notify(NOTIFICATION_ID, notification);
                        }
                        Log.d("Close Notif", "Not Called");
                    }
                } else {
                    Log.d("Close Notif", "Called");
                    closeNotification(false);
                }

            /*Location loc = null;
            if (location == null && locationManager != null) {
                loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }*/

                LogGpsModel logGpsModel = new LogGpsModel(
                        UserModel.getInstance(SadixService.this).SalesId,
                        Util.getIMEI(SadixService.this),
//				Util.dateFormat.format(location == null ? (loc != null ? new Date(loc.getTime()) : new Date()) : new Date(location.getTime())),
                        Util.dateFormat.format(new Date()),
                        UserModel.getInstance(SadixService.this).CompanyID,
                        location == null ? 0 : location.getLongitude(),
                        location == null ? 0 : location.getLatitude(),
                        1
                );
                try {
                    int jam = Integer.parseInt(new SimpleDateFormat("kk").format(new Date()));
                    int WHStart = Integer.parseInt(UserModel.getInstance(SadixService.this).WHStart);
                    int WHStop = Integer.parseInt(UserModel.getInstance(SadixService.this).WHStop);
                    if (jam >= WHStart && jam <= WHStop) {
                        if (location != null) {
                            JSONObject json = new JSONObject();
                            json.put("SalesID", Integer.parseInt(logGpsModel.SalesID));
                            json.put("IMEI", logGpsModel.IMEI);
                            json.put("LogTime", logGpsModel.LogTime);
                            json.put("SC", logGpsModel.SC);
                            json.put("GeoLong", logGpsModel.GeoLong);
                            json.put("GeoLat", logGpsModel.GeoLat);
                            json.put("statusLog", logGpsModel.statusLog);
                            server.setUrl(Util.getServerUrl(SadixService.this) + "salesmanjourney");
                            String respon = server.requestJSONObjectNonThread(json);
//						android.util.Log.e("respon", ""+respon);
                            JSONObject jsonRespon = new JSONObject(respon);
                            if (jsonRespon.getString("Result").equals("INSERT_OK")) {
                                if (jsonRespon.has("MsgID") && jsonRespon.getInt("MsgID") > 0) {
                                    InformasiModel informasiModel = new InformasiModel(
                                            jsonRespon.getString("Header"),
                                            jsonRespon.getString("Message"),
                                            Util.dateFormat.format(new Date())
                                    );
                                    ctrlInformation.save(informasiModel);
                                }
                            } else {
                                ctrlLogGps.save(logGpsModel);
                            }
                        }
                    }
//				android.util.Log.e("aa", "mulai");
                    Util.sendUlangCustomer(SadixService.this, null);
                    Util.sendUlang(SadixService.this, null);
                    Util.sendUlangCustBranding(SadixService.this, null);
                    Util.sendUlangCustPhoto(SadixService.this, null);
                    Util.sendUlangLogSurvey(SadixService.this, null);
                    Util.sendUlangSalesTarget(SadixService.this);
                    Util.sendUlangSalesNote(SadixService.this);
                    List<LogGpsModel> list = ctrlLogGps.list();
                    for (LogGpsModel log : list) {
                        Util.sendUlangLogGPS(SadixService.this, log);
                    }

                    int finalpendingCommon = realm.where(CommonModel.class)
                            .equalTo("Done", 1)
                            .equalTo("Status", "0")
//                        .isNotNull("EndVisitTime")
                            .findAll().size();

                    Log.wtf("final pending common", finalpendingCommon + "");

                    if (finalpendingCommon == 0) {
                        closeNotification(false);
                    }
//				android.util.Log.e("aa", "selesai");
                } catch (Exception e) {
                    e.printStackTrace();
                    ctrlLogGps.save(logGpsModel);
                }

                realm.close();
            }
        };

        return updateTask;
    }

    private void startLocationUpdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    private PendingResult<LocationSettingsResult> requestLocation() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(20000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        return result;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Toast.makeText(this, "Google Service not connected\n" + result.toString(), Toast.LENGTH_SHORT).show();
        Log.i("google client", "GoogleApiClient connection failed: " + result.toString());
        /*if (!result.hasResolution()) {
            // Show a localized error dialog.
            GooglePlayServicesUtil.getErrorDialog(
                    result.getErrorCode(), this, 0, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            retryConnecting();
                        }
                    }).show();
            return;
        }
        // If there is an existing resolution error being displayed or a resolution
        // activity has started before, do nothing and wait for resolution
        // progress to be completed.
        if (mIsInResolution) {
            return;
        }
        mIsInResolution = true;
        try {
            result.startResolutionForResult(this, 1);
        } catch (IntentSender.SendIntentException e) {
            Log.e("resolution exception", "Exception while starting resolution activity", e);
            retryConnecting();
        }*/
    }

    private void retryConnecting() {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;

        for (int i = 0; i < mClient.size(); i++) {
            try {
                Bundle bundle = new Bundle();
                bundle.putDouble("long", location.getLongitude());
                bundle.putDouble("lat", location.getLatitude());
                bundle.putLong("time", location.getTime());
                Message msg = Message.obtain(null, MSG_SEND_LOC);
                msg.setData(bundle);

                mClient.get(i).send(msg);
            } catch (RemoteException e) {
                mClient.remove(i);
                e.printStackTrace();
            }
        }
    }
}





/*
package com.bhn.sadix;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlInformasi;
import com.bhn.sadix.database.CtrlLogGps;
import com.bhn.sadix.model.InformasiModel;
import com.bhn.sadix.model.LogGpsModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

public class SadixService extends Service {
	private ConnectionServer server;
	private Timer timer;
	private Timer timerAutoLogout;
	private TimerTask updateTask = new TimerTask() {
		@Override
		public void run() {
			CtrlCustomer ctrlCustomer = new CtrlCustomer(SadixService.this);
			CtrlCommon ctrlCommon = new CtrlCommon(SadixService.this);
//			int pending = ctrlCustomer.pendingData();
//			pending += ctrlCommon.pendingData();
			int pending = 0;
			try {
				pending = ctrlCustomer.pendingData();
				pending += ctrlCommon.pendingData();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(pending > 0) {
	            if (isForeground) {
	                notification = createNotification(false);
	                mNotificationManager.notify(NOTIFICATION_ID, notification);
	            } else {
	                isForeground = true;
	                notification = createNotification(true);
	                stopForeground(false);
	                mNotificationManager.notify(NOTIFICATION_ID, notification);
	            }
			} else {
                closeNotification();
			}
            
			Location loc = null;
			if(location == null && locationManager != null) {
				loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}
			LogGpsModel logGpsModel = new LogGpsModel(
				UserModel.getInstance(SadixService.this).SalesId, 
				Util.getIMEI(SadixService.this), 
//				Util.dateFormat.format(location == null ? (loc != null ? new Date(loc.getTime()) : new Date()) : new Date(location.getTime())), 
				Util.dateFormat.format(new Date()), 
				UserModel.getInstance(SadixService.this).CompanyID, 
				location == null ? (loc != null ? loc.getLongitude() : 0) : location.getLongitude(), 
				location == null ? (loc != null ? loc.getLatitude() : 0) : location.getLatitude(), 
				1
			);
			try {
				int jam = Integer.parseInt(new SimpleDateFormat("kk").format(new Date()));
				int WHStart = Integer.parseInt(UserModel.getInstance(SadixService.this).WHStart);
				int WHStop = Integer.parseInt(UserModel.getInstance(SadixService.this).WHStop);
				if(jam >= WHStart && jam <= WHStop) {
					if(location != null) {
						JSONObject json = new JSONObject();
						json.put("SalesID", Integer.parseInt(logGpsModel.SalesID));
						json.put("IMEI", logGpsModel.IMEI);
						json.put("LogTime", logGpsModel.LogTime);
						json.put("SC", logGpsModel.SC);
						json.put("GeoLong", logGpsModel.GeoLong);
						json.put("GeoLat", logGpsModel.GeoLat);
						json.put("statusLog", logGpsModel.statusLog);
				        server.setUrl(Util.getServerUrl(SadixService.this)+"salesmanjourney");
						String respon = server.requestJSONObjectNonThread(json);
//						android.util.Log.e("respon", ""+respon);
						JSONObject jsonRespon = new JSONObject(respon);
						if(jsonRespon.getString("Result").equals("INSERT_OK")) {
							if(jsonRespon.has("MsgID") && jsonRespon.getInt("MsgID") > 0) {
								InformasiModel informasiModel = new InformasiModel(
									jsonRespon.getString("Header"), 
									jsonRespon.getString("Message"), 
									Util.dateFormat.format(new Date())
								);
								ctrlInformation.save(informasiModel);
							}
						} else {
							ctrlLogGps.save(logGpsModel);
						}
					}
				}
//				android.util.Log.e("aa", "mulai");
				Util.sendUlangCustomer(SadixService.this, null);
				Util.sendUlang(SadixService.this, null);
				Util.sendUlangCustBranding(SadixService.this, null);
				Util.sendUlangCustPhoto(SadixService.this, null);
				Util.sendUlangLogSurvey(SadixService.this, null);
				Util.sendUlangSalesTarget(SadixService.this);
				Util.sendUlangSalesNote(SadixService.this);
				List<LogGpsModel> list = ctrlLogGps.list();
				for (LogGpsModel log : list) {
					Util.sendUlangLogGPS(SadixService.this, log);
				}
//				android.util.Log.e("aa", "selesai");
			} catch (Exception e) {
				e.printStackTrace();
				ctrlLogGps.save(logGpsModel);
			}
		}
	};
	private TimerTask autoLogout = new TimerTask() {
		@Override
		public void run() {
			try {
				String time = Util.FORMAT_LOGOUT.format(new Date());
				String DAY_OF_LOGOUT = Util.getStringPreference(SadixService.this, "DAY_OF_LOGOUT");
				if(!time.equals(DAY_OF_LOGOUT)) {
					Util.putPreference(SadixService.this, "isLogin", false);
					stopService(new Intent(SadixService.this, SadixService.class));
				}
			} catch (Exception e) {
			}
		}
	};

	private LocationManager locationManager;
	private Location location;
    private LocationListener locationListener = new LocationListener() {
        @Override
    	public void onLocationChanged(Location loc) {
        	location = loc;
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onProviderDisabled(String provider) {
//        	if(ctrlAppModul.isModul("50") == false) {
//        		Util.Toast(SadixService.this, "Mengingatkan GPS tidak ditampilkan, namun tracking tetap jalan dengan sumber data A-GPS");
//        	}
        }
    };

    private CtrlLogGps ctrlLogGps;
    private CtrlInformasi ctrlInformation;
//    private CtrlAppModul ctrlAppModul;

    private NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 100;
    private Notification notification;
    private boolean isForeground = false;
    
	@Override
	public void onCreate() {
		super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		ctrlLogGps = new CtrlLogGps(this);
		ctrlInformation = new CtrlInformasi(this);
//		ctrlAppModul = new CtrlAppModul(this);
	    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    try {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		} catch (Exception e) {
			Util.Toast(this, e.getMessage());
		}
		server = new ConnectionServer(this);
		timer = new Timer("SadixServiceTimer");
		int time = Integer.parseInt(UserModel.getInstance(SadixService.this).Interval);
		timer.schedule(updateTask, 1000L, time*60*1000L);
//		timer.schedule(updateTask, 1000L, 1000L);
		
		timerAutoLogout = new Timer("SadixServiceAutoLogout");
		timerAutoLogout.schedule(autoLogout, 1000L, 1000L);
	}

    private Notification createNotification(boolean isSound) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, LogMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_sadix)
                .setContentTitle("Sadix")
                .setContentText("Sending order")
//                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
//        android.util.Log.e("isSound", "isSound");
        if(isSound) {
//            android.util.Log.e("pake suara", "pake suara");
            notificationBuilder.setSound(defaultSoundUri);
        }
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        return notification;
    }
	@Override
	public void onDestroy() {
		super.onDestroy();
        closeNotification();
		try {
			locationManager.removeUpdates(locationListener);
		} catch (Exception e) {
			Util.Toast(this, e.getMessage());
		}
		
		timer.cancel();
		timer = null;
		timerAutoLogout.cancel();
		timerAutoLogout = null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

    private void closeNotification() {
        if(notification != null && isForeground) {
            isForeground = false;
            stopForeground(true);
            mNotificationManager.cancel(NOTIFICATION_ID);
            notification = null;
        }
    }

}
*/
