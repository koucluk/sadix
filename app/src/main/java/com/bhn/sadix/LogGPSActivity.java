package com.bhn.sadix;

import java.util.List;

import org.json.JSONArray;

import com.bhn.sadix.adapter.LogGpsAdapter;
import com.bhn.sadix.database.CtrlLogGps;
import com.bhn.sadix.model.LogGpsModel;
import com.bhn.sadix.util.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LogGPSActivity extends AppCompatActivity implements OnItemClickListener {
    private CtrlLogGps ctrlLogGps;
    private LogGpsAdapter adapter;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctrlLogGps = new CtrlLogGps(this);
        adapter = new LogGpsAdapter(this, ctrlLogGps.list());
        adapter.setNotifyOnChange(true);
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Export")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Send All")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Export");
            menu.add(0, 2, 2, "Send All");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            new ExportData(this).execute();
        } else if (item.getItemId() == 2) {
            new SendData(this).execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        LogGpsModel model = adapter.getItem(position);
        new LoadData(this, model).execute();
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        private LogGpsModel model;
        private ProgressDialog progressDialog;
        private Context context;

        public LoadData(Context context, LogGpsModel model) {
            this.model = model;
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            return Util.sendUlangLogGPS(context, model);
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new LogGpsAdapter(context, ctrlLogGps.list());
            adapter.setNotifyOnChange(true);
            ((ListView) findViewById(R.id.list)).setAdapter(adapter);
            progressDialog.dismiss();
            if (result == null) {
                Util.showDialogInfo(context, "Data berhasil dikirim");
            } else {
                Util.showDialogError(context, result);
            }
            super.onPostExecute(result);
        }
    }

    private class ExportData extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        private Context context;

        public ExportData(Context context) {
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Export Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                List<LogGpsModel> list = ctrlLogGps.list();
                JSONArray array = new JSONArray();
                for (LogGpsModel logGpsModel : list) {
                    array.put(Util.convertJSONLogGPS(context, logGpsModel));
                }
                Util.saveFile(array.toString().getBytes(), "ExpoprtLogGPS.txt");
            } catch (Exception e) {
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogError(LogGPSActivity.this, result);
            } else {
                Util.showDialogInfo(LogGPSActivity.this, "Export data berhasil!");
            }
            super.onPostExecute(result);
        }
    }

    private class SendData extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        private Context context;

        public SendData(Context context) {
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Send All Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            for (int i = 0; i < adapter.getCount(); i++) {
                LogGpsModel model = adapter.getItem(i);
                Util.sendUlangLogGPS(context, model);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            adapter = new LogGpsAdapter(context, ctrlLogGps.list());
            adapter.setNotifyOnChange(true);
            ((ListView) findViewById(R.id.list)).setAdapter(adapter);
            progressDialog.dismiss();
            if (result == null) {
                Util.showDialogInfo(context, "Data berhasil dikirim");
            } else {
                Util.showDialogError(context, result);
            }
            super.onPostExecute(result);
        }
    }

}
