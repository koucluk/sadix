package com.bhn.sadix;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.ResumeOrderAdapter;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.ResumeOrderDetailModul;
import com.bhn.sadix.model.ResumeOrderModel;
import com.bhn.sadix.model.SKUModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ResumeOrderActivity extends AppCompatActivity {
    private CtrlTakingOrder ctrlTakingOrder;
    private ProgressDialog progressDialog;
    private ResumeOrderAdapter adapter;

    ResumeOrderActivity activity;
    private Toolbar toolbar;
    private CtrlSKU ctrlSKU;
    private CtrlCustomer ctrlCustomer;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_order);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        activity = this;

        ctrlSKU = new CtrlSKU(this);
        ctrlCustomer = new CtrlCustomer(this);
        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ctrlTakingOrder = new CtrlTakingOrder(this);
        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        ComboBoxAdapter adapter = new ComboBoxAdapter(this, new ArrayList<ComboBoxModel>());
        adapter.setNotifyOnChange(true);
        adapter.add(new ComboBoxModel("1", "By Quantity"));
        adapter.add(new ComboBoxModel("2", "By Amount"));
        ((Spinner) findViewById(R.id.orderBy)).setAdapter(adapter);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
        new LoadData().execute();
        ((Spinner) findViewById(R.id.orderBy)).setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                new LoadData().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Realm realm = Realm.getDefaultInstance();

                        adapter = new ResumeOrderAdapter(ResumeOrderActivity.this, new ArrayList<ResumeOrderModel>(), new HashMap<ResumeOrderModel, List<ResumeOrderDetailModul>>());
                        adapter.setGroupBy((ComboBoxModel) ((Spinner) findViewById(R.id.orderBy)).getSelectedItem());
//                        List<ResumeOrderModel> list = ctrlTakingOrder.listResumeOrder(null);

                        RealmResults<TakingOrderModel> modelDisticnt = realm.where(TakingOrderModel.class).distinct("SKUId");
                        Log.d("model distinct Size", modelDisticnt.size() + "");
                        List<ResumeOrderModel> list = new ArrayList<>();

                        for (TakingOrderModel mod : modelDisticnt) {
                            ResumeOrderModel resumeOrderModel = new ResumeOrderModel();

                            RealmResults<TakingOrderModel> model = realm.where(TakingOrderModel.class).equalTo("SKUId", mod.SKUId).findAll();

                            int SUM_QTYB = model.sum("QTY_B").intValue();
                            int SUM_QTYK = model.sum("QTY_K").intValue();
                            int SUM_PRICE = model.sum("TOTAL_PRICE").intValue();

                            resumeOrderModel.SKUId = mod.SKUId;
                            resumeOrderModel.QTY_B = SUM_QTYB;
                            resumeOrderModel.QTY_K = SUM_QTYK;
                            resumeOrderModel.TOTAL_PRICE = SUM_PRICE;
                            SKUModel skuModel = ctrlSKU.get(mod.SKUId);
                            resumeOrderModel.ProductName = skuModel.ProductName;

                            list.add(resumeOrderModel);
                        }
                        Log.d("Header Size", list.size() + "");

                        for (ResumeOrderModel resumeOrderModel : list) {
                            /*adapter.addHeader(resumeOrderModel);
                            List<ResumeOrderDetailModul> listDetail = ctrlTakingOrder.listResumeOrderDetail(null, resumeOrderModel.SKUId);
                            adapter.addChild(resumeOrderModel, listDetail);*/
                            adapter.addHeader(resumeOrderModel);

//                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K " +
//                            "from retur where SKUId='" + SKUId + "' " +
//                            "group by CustId"

                            RealmResults<TakingOrderModel> detailDisticnt = realm.where(TakingOrderModel.class).equalTo("SKUId", resumeOrderModel.SKUId).distinct("CustId");

                            List<ResumeOrderDetailModul> listDetail = new ArrayList<>();

                            for (TakingOrderModel mod : detailDisticnt) {
                                ResumeOrderDetailModul resumeOrderDetailModul = new ResumeOrderDetailModul();

                                RealmResults<TakingOrderModel> model = realm.where(TakingOrderModel.class).equalTo("SKUId", mod.SKUId).equalTo("CustId", mod.CustId).findAll();

                                int SUM_QTYB = model.sum("QTY_B").intValue();
                                int SUM_QTYK = model.sum("QTY_K").intValue();
                                int SUM_PRICE = model.sum("TOTAL_PRICE").intValue();

                                resumeOrderDetailModul.QTY_B = SUM_QTYB;
                                resumeOrderDetailModul.QTY_K = SUM_QTYK;
                                resumeOrderDetailModul.TOTAL_PRICE = SUM_PRICE;
                                CustomerModel skuModel = ctrlCustomer.get(mod.CustId);
                                resumeOrderDetailModul.CustomerName = skuModel.CustomerName;

                                listDetail.add(resumeOrderDetailModul);
                            }
                            Log.d("Child Size", listDetail.size() + "");

//                    List<ResumeOrderDetailModul> listDetail = ctrlRetur.listResumeOrderDetail(null, resumeOrderModel.SKUId);
                            adapter.addChild(resumeOrderModel, listDetail);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            /*try {
                adapter = new ResumeOrderAdapter(ResumeOrderActivity.this, new ArrayList<ResumeOrderModel>(), new HashMap<ResumeOrderModel, List<ResumeOrderDetailModul>>());
                adapter.setGroupBy((ComboBoxModel) ((Spinner) findViewById(R.id.orderBy)).getSelectedItem());
                List<ResumeOrderModel> list = ctrlTakingOrder.listResumeOrder(null);
                for (ResumeOrderModel resumeOrderModel : list) {
                    adapter.addHeader(resumeOrderModel);
                    List<ResumeOrderDetailModul> listDetail = ctrlTakingOrder.listResumeOrderDetail(null, resumeOrderModel.SKUId);
                    adapter.addChild(resumeOrderModel, listDetail);
                }
            } catch (Exception e) {
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (adapter != null) {
                ((ExpandableListView) findViewById(R.id.list)).setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            progressDialog.dismiss();
            super.onPostExecute(result);
        }
    }

}
