package com.bhn.sadix;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.adapter.TargetAdapter;
import com.bhn.sadix.adapter.TargetComAdapter;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlGroupSKUCom;
import com.bhn.sadix.database.CtrlSKUCom;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.fragment.customer.FragmentAdapter;
import com.bhn.sadix.fragment.kunjungan.TargetComFragment;
import com.bhn.sadix.fragment.kunjungan.TargetFragment;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.GroupSKUComModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.SKUComModel;
import com.bhn.sadix.model.TargetComModel;
import com.bhn.sadix.model.TargetModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.CustomerListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SalesTargetActivity extends AppCompatActivity implements CustomerListener {
    private CustomerModel customerModel;
    private CustomerTypeModel customerTypeModel;
    private FragmentAdapter adapter;
    private ViewPager pager;
    private TargetFragment targetFragment;
    private TargetComFragment targetComFragment;
    private TargetAdapter targetAdapter;
    private TargetComAdapter targetComAdapter;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlGroupSKUCom ctrlGroupSKUCom;
    private CtrlSKUCom ctrlSKUCom;
    private DbMasterHelper ctrlDb;
    private DatabaseHelper db;
    private ProgressDialog progressDialog;
    private ConnectionServer server;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_target);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        server = new ConnectionServer(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Tunggu sebentar...");

        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlGroupSKUCom = new CtrlGroupSKUCom(this);
        ctrlSKUCom = new CtrlSKUCom(this);
        ctrlDb = new DbMasterHelper(this);

        targetFragment = new TargetFragment();
        targetComFragment = new TargetComFragment();

        adapter = new FragmentAdapter(getSupportFragmentManager(), new ArrayList<BaseFragment>());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }
        };
        pager.setOnPageChangeListener(ViewPagerListener);

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
            targetFragment.setCustomerModel(customerModel);
        }

        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });
        new LoadData().execute();
    }

    protected void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
            new SendTarget().execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public CustomerModel getCustomer() {
        return customerModel;
    }

    @Override
    public CustomerTypeModel getCustomerTypeModel() {
        return customerTypeModel;
    }

    private class SendTarget extends AsyncTask<Void, Void, String> {
        private JSONArray arraySave = new JSONArray();

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                for (int i = 0; i < arraySave.length(); i++) {
                    JSONObject json = arraySave.getJSONObject(i);
                    android.util.Log.e("arraySave", "" + json.toString());
                    if (ctrlDb.isProducttarget(json)) {
                        ctrlDb.updateProducttarget(json);
                    } else {
                        ctrlDb.save(json, "producttarget");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            progressDialog.dismiss();
            if (result != null) {
                Util.okeDialog(SalesTargetActivity.this, "Info", result, new OkListener() {
                    @Override
                    public void onDialogOk() {
                        finish();
                    }
                });
            } else {
                Util.okeDialog(SalesTargetActivity.this, "Info", "Koneksi gagal, data akan tersimpan diinternal memory", new OkListener() {
                    @Override
                    public void onDialogOk() {
                        finish();
                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                JSONArray array = new JSONArray();
                for (int i = 0; i < targetAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < targetAdapter.getChildrenCount(i); j++) {
                        TargetModel targetModel = targetFragment.getAdapter().getChild(i, j);
                        for (TargetModel.Detail detail : targetModel.details) {
                            if (detail.Price > 0 || detail.Qty > 0) {
                                array.put(detail.toJsonSalesTarget());
                                arraySave.put(detail.toSaveSalesTarget());
                            }
                        }
                    }
                }
                for (int i = 0; i < targetComAdapter.getGroupCount(); i++) {
                    for (int j = 0; j < targetComAdapter.getChildrenCount(i); j++) {
                        TargetComModel targetModel = targetComFragment.getAdapter().getChild(i, j);
                        for (TargetComModel.Detail detail : targetModel.details) {
                            if (detail.Price > 0 || detail.Qty > 0) {
                                array.put(detail.toJsonSalesTarget());
                                arraySave.put(detail.toSaveSalesTarget());
                            }
                        }
                    }
                }
                android.util.Log.e("data", "" + array.toString());
                server.setUrl(Util.getServerUrl(SalesTargetActivity.this) + "targetdata");
                String result = server.requestJSONObjectNonThread(array);
                android.util.Log.e("result", "" + result);

                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        result = jsonRespon.getString("Message");
                    } else {
                        result = null;
                        for (int i = 0; i < array.length(); i++) {
                            db.save(array.getJSONObject(i), "Salestarget");
                        }
                    }
                } else {
                    result = null;
                    for (int i = 0; i < array.length(); i++) {
                        db.save(array.getJSONObject(i), "Salestarget");
                    }
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    private void populateTakingOrder() {
        targetAdapter = new TargetAdapter(this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<TargetModel>>());
        List<GroupSKUModel> list = ctrlGroupSKU.list();
        for (GroupSKUModel groupSKUModel : list) {
            targetAdapter.addHeader(groupSKUModel);
        }
        targetComAdapter = new TargetComAdapter(this, new ArrayList<GroupSKUComModel>(), new HashMap<GroupSKUComModel, List<TargetComModel>>());
        List<GroupSKUComModel> listCom = ctrlGroupSKUCom.list();
        for (GroupSKUComModel groupSKUComModel : listCom) {
            targetComAdapter.addHeader(groupSKUComModel);
            List<SKUComModel> listSKU = ctrlSKUCom.listByGroup(groupSKUComModel);
            for (SKUComModel skuComModel : listSKU) {
                Cursor cursor = ctrlDb.query("select * from productperiod order by year,month");
                targetComAdapter.addChild(groupSKUComModel, new TargetComModel(skuComModel, cursor, ctrlDb, customerModel, Util.getStringPreference(this, "SalesId")));
                cursor.close();
                ctrlDb.close();
            }
        }
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                populateTakingOrder();
            } catch (Exception e) {
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            targetFragment.setAdapter(targetAdapter);
            targetComFragment.setAdapter(targetComAdapter);
            adapter.add(targetFragment);
            adapter.add(targetComFragment);
            progressDialog.dismiss();
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Save")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Save");
        }
        return true;
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan salestarget?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        close();
    }

}
