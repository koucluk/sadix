package com.bhn.sadix.util;

import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;

public interface PieChartListener {
	public void onClick(GraphicalView mChartView, CategorySeries mSeries, DefaultRenderer mRenderer);
}
