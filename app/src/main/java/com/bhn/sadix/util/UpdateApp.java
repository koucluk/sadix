package com.bhn.sadix.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

public class UpdateApp extends AsyncTask<String,Void,Void> {
	private Context context;
	private ProgressDialog progressDialog;
	private File outputFile;
	public UpdateApp(Context contextf) {
		context = contextf;
	    progressDialog = new ProgressDialog(contextf);
	    progressDialog.setCancelable(false);
		progressDialog.setMessage("Update Aplikasi...");
		progressDialog.show();
	}
	@Override
	protected Void doInBackground(String... arg0) {
      try {
            URL url = new URL(arg0[0]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.connect();
            String PATH = Environment.getExternalStorageDirectory() + "/Sadix/";
            File file = new File(PATH);
            file.mkdirs();
            outputFile = new File(file, "Sadix.apk");
            if(outputFile.exists()){
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile, true);
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();
        } catch (Exception e) {
        }
	    return null;
	}
	@Override
	protected void onPostExecute(Void respon) {
		progressDialog.dismiss();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(outputFile), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity)context).finish();
	}
}
