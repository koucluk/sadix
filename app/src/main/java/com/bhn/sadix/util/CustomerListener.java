package com.bhn.sadix.util;

import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;

/**
 * Created by caksono on 15/03/17.
 */

public interface CustomerListener {
    CustomerModel getCustomer();

    CustomerTypeModel getCustomerTypeModel();
}
