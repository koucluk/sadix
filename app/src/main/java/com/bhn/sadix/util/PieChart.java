package com.bhn.sadix.util;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

public class PieChart {
	private GraphicalView mChartView;
	private CategorySeries mSeries;
	private DefaultRenderer mRenderer;
	private PieChartListener  listener;
	public PieChart(Context context, String title, PieChartListener  list) {
		this.listener = list;
		mSeries = new CategorySeries(title);
		mRenderer = new DefaultRenderer();
		mRenderer.setLabelsTextSize(15);
		mRenderer.setLegendTextSize(15);
	    mRenderer.setZoomButtonsVisible(true);
	    mRenderer.setStartAngle(180);
	    mRenderer.setDisplayValues(true);
	    mChartView = ChartFactory.getPieChartView(context, mSeries, mRenderer);
	    mRenderer.setClickEnabled(true);
	    if(listener != null) {
			mChartView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SeriesSelection seriesSelection = mChartView.getCurrentSeriesAndPoint();
					if (seriesSelection == null) {
					} else {
						for (int i = 0; i < mSeries.getItemCount(); i++) {
							mRenderer.getSeriesRendererAt(i).setHighlighted(i == seriesSelection.getPointIndex());
						}
						mChartView.repaint();
						if(listener != null) {
							listener.onClick(mChartView, mSeries, mRenderer);
						}
					}
				}
			});
	    }
		mRenderer.setLabelsColor(Color.BLACK);
	}
	public void addItem(String category, double value, int color) {
		mSeries.add(category, value);
        SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
        renderer.setColor(color);
        mRenderer.addSeriesRenderer(renderer);
	}
	public void repaint() {
		mChartView.repaint();
	}
	public GraphicalView getView() {
		return mChartView;
	}
}
