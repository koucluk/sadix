package com.bhn.sadix.util;

public interface ConfirmListener {
	public void onDialogCompleted(boolean answer);
}
