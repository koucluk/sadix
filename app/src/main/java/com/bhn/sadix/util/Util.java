package com.bhn.sadix.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.AssetMutasiModel;
import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.Data.DiscountResultModel;
import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.Data.PrincipleOrderModel;
import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.Data.StockCompetitorModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.Data.StockGroupModel;
import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.SadixService;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlLogGps;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.database.CtrlSurvey;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CommonSurveyModel;
import com.bhn.sadix.model.CustPhotoModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.HasilSurveyModel;
import com.bhn.sadix.model.LogGpsModel;
import com.bhn.sadix.model.SKUModel;
import com.bhn.sadix.model.UserModel;
import com.printer.ZQPrinter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class Util {
    public final static String LOKASI_IMAGE = Environment.getExternalStorageDirectory() + "/Sadix/img/";
    public final static String LOKASI = Environment.getExternalStorageDirectory() + "/Sadix/";
    //	public final static String SERVER_URL = "http://sdx.x-locate.com/SadixApi2/";
    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    //	public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss");
    public static final SimpleDateFormat FORMAT_ID = new SimpleDateFormat("ddMMyyyyHHmmss");
    public static final DecimalFormat numberFormat = new DecimalFormat("#,##0");
    public static final DecimalFormat NUMBER_FORMAT = new DecimalFormat("##0");
    public final static SimpleDateFormat dateFormatString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat FORMAT_LOGOUT = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat dateFormatBirthDate = new SimpleDateFormat("dd/MM/yyyy");
    public final static SimpleDateFormat FormatStringDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    public final static SimpleDateFormat DATE_FORMAT_YYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat TIME_FORMAT_HHMM = new SimpleDateFormat("HH:mm");
    //	public static final int Width = 160;//59
//	public static final int Height = 120;//60
    public static final String DBSKU = "DbSku.db";
    public static final String DBMASTER = "DbMaster.db";
    public static final String DBASSET = "DbAset.db";
    public static final String DBDISKON = "DbDiskon.db";
    public static final String GRAPH = "GRAPH";
    public static final String GRAPH_TIME = "GRAPH_TIME";
    public static final int REQUEST_CHECK_LOCATION = 0x123;

    public static final int GREATER_THAN = 2;
    public static final int GREATER_THAN_EQUALS = 0;
    public static final int LESS_THAN = 1;

    public static final int ALL_CUSTOMER = 1;
    public static final int SCHEDULE = 2;
    private static com.bhn.sadix.database.CtrlDiscountPromo ctrlDiscountPromo;
    private static CtrlCustomer ctrlCustomer;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static String getServerUrl(Context context) {
        String PathSvr = Util.getStringPreference(context, "PathSvr");
        Log.d("PathSvr", PathSvr + "");
        if (PathSvr != null && !PathSvr.trim().equals("")) {
            return PathSvr;
        } else {
            return "http://sdx.x-locate.com/SadixApi2/";
//			return "http://103.236.190.106/SadixApi2/";
        }
    }

    public static void showDialog(Context context, String title, String message, int iconId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.setIcon(iconId);
        builder.show();
    }

    public static void confirmDialog(Context context, final String title, final String message, final ConfirmListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                if (listener != null) {
                    dialog.dismiss();
                    listener.onDialogCompleted(true);
                }
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                if (listener != null) {
                    listener.onDialogCompleted(false);
                }
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static void okeDialog(Context context, final String title, final String message, final OkListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                if (listener != null) {
                    listener.onDialogOk();
                }
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static void updateDialog(Context context, final OkListener listener) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle("INFO");
        dialog.setMessage("Untuk bisa terus bisa menggunakan aplikasi Sadix, Anda harus update ke versi terbaru");
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                if (listener != null) {
                    listener.onDialogOk();
                }
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static void infoErrorKirimLog(final Context context, final String infoError) {
        AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.setTitle("INFO");
        dialog.setMessage("Pengiriman Gagal, Data akan disimpan di Local");
        dialog.setCancelable(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                dialog.dismiss();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Detail", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonId) {
                dialog.dismiss();
                Util.showDialogError(context, infoError);
            }
        });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
    }

    public static void showDialogInfo(Context context, String message) {
        showDialog(context, "Info", message, android.R.drawable.ic_dialog_info);
    }

    public static void showDialogError(Context context, String message) {
        showDialog(context, "Error", message, android.R.drawable.ic_dialog_alert);
    }

    public static String getStringPreference(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        String session_id = sharedPrefs.getString(key, null);
        return session_id;
    }

    public static int getIntegerPreference(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        int data = sharedPrefs.getInt(key, 0);
        return data;
    }

    public static boolean getBooleanPreference(Context context, String key) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean session_id = sharedPrefs.getBoolean(key, false);
        return session_id;
    }

    public static void putPreference(Context context, String key, String value) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void putPreference(Context context, String key, boolean value) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void putPreference(Context context, String key, int value) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static String getSessionId(Context context) {
        return getStringPreference(context, "session_id");
    }

    public static boolean isOnline(Context context) {
        return getBooleanPreference(context, "config_online");
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
//    	return "358916050593834";
    }

    public static String getNetworkOperatorName(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkOperatorName();
//    	return "358916050593834";
    }

    public static String getAPN(Context context) {
        try {//path to APN table
            final Uri APN_TABLE_URI = Uri.parse("content://telephony/carriers");

            //path to preffered APNs
            final Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");

            //receiving cursor to preffered APN table
            Cursor c = context.getContentResolver().query(PREFERRED_APN_URI, null, null, null, null);

            //moving the cursor to beggining of the table
            c.moveToFirst();

            //now the cursor points to the first preffered APN and we can get some
            //information about it
            //for example first preffered APN id
            int index = c.getColumnIndex("_id");    //getting index of required column
            Short id = c.getShort(index);           //getting APN's id from

            //we can get APN name by the same way
            index = c.getColumnIndex("name");
            String name = c.getString(index);
            return name;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public static Bitmap drawTextToBitmap(Context gContext, int gResId, String gText) {
        Resources resources = gContext.getResources();
        Bitmap bitmap = BitmapFactory.decodeResource(resources, gResId);
        return drawTextToBitmap(gContext, bitmap, gText);
    }

    public static Bitmap drawTextToBitmap(Context gContext, Bitmap bitmap, String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(61, 61, 61));
        // text size in pixels
        paint.setTextSize((int) (14 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = (bitmap.getHeight() + bounds.height()) / 2;

        canvas.drawText(gText, x * scale, y * scale, paint);

        return bitmap;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void gantiBahasa(Context context, String locale) {
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(locale);
        res.updateConfiguration(conf, dm);
    }

    public static byte[] readFile(String file) throws IOException {
        return readFile(new File(file));
    }

    public static byte[] readFile(File file) throws IOException {
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength) {
                throw new IOException("File size >= 2 GB");
            }
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    public static void saveFileImg(byte[] data, String nmFile) {
        try {
            FileOutputStream outStream = new FileOutputStream(Util.LOKASI_IMAGE + nmFile);
            outStream.write(data);
            outStream.flush();
            outStream.close();
            outStream = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFile(byte[] data, String nmFile) {
        try {
            FileOutputStream outStream = new FileOutputStream(Util.LOKASI + nmFile);
            outStream.write(data);
            outStream.flush();
            outStream.close();
            outStream = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Toast(Context context, String message) {
        android.widget.Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT).show();
    }

    public static double emptyStringToNumber(String data) {
        double angka = 0;
        if (data != null && data.equals("") == false) {
            angka = Double.parseDouble(data);
        }
        return angka;
    }

    public static boolean isRunService(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SadixService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void printCembayaran(Context context, ZQPrinter PrinterService, CommonModel commonModel, CustomerModel customerModel, PrintListener listener) {
        try {
            CtrlSKU ctrlSKU = new CtrlSKU(context);
            CustomerTypeModel customerTypeModel = new CtrlCustomerType(context).get(customerModel.CustomerTypeId);
//			CtrlAppModul ctrlAppModul = new CtrlAppModul(context);
//			CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
//			CustomerModel customerModel = ctrlCustomer.get(commonModel.CustomerId);
            PrinterService.PrintText(UserModel.getInstance(context).CompanyName, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText(UserModel.getInstance(context).CompanyName, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(2);
            PrinterService.PrintText(customerModel.CustomerName, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText(customerModel.CustomerAddress, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText("--------------------------------", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(2);
            PrinterService.PrintText("InvNO: " + commonModel.InvNO, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText(UserModel.getInstance(context).FullName, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText("Date: " + commonModel.BeginVisitTime, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText("--------------------------------", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            CtrlTakingOrder ctrlTakingOrder = new CtrlTakingOrder(context);
            Cursor cursor = ctrlTakingOrder.listOrder(commonModel.CommonID);
            while (cursor.moveToNext()) {
                SKUModel model = ctrlSKU.get(cursor.getString(0));
                PrinterService.PrintText(model.ProductName, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
                PrinterService.LineFeed(1);
                PrinterService.PrintText("" + cursor.getInt(1) + model.SBESAR, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                PrinterService.PrintText("\t" + cursor.getInt(2) + model.SKECIL, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                PrinterService.PrintText("\t" + Util.numberFormat.format(cursor.getDouble(3)), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                PrinterService.LineFeed(1);
                double diskon = cursor.getDouble(4);
                if (diskon > 0) {
                    if (model.DISCOUNT_1_TYPE.equals("2")) {
                        PrinterService.PrintText("Dis:" + Util.numberFormat.format(diskon), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    } else {
                        PrinterService.PrintText("Dis:" + diskon + "%", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    }
                    PrinterService.LineFeed(1);
                }
                diskon = cursor.getDouble(5);
                if (diskon > 0) {
                    if (model.DISCOUNT_2_TYPE.equals("2")) {
                        PrinterService.PrintText("Dis:" + Util.numberFormat.format(diskon), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    } else {
                        PrinterService.PrintText("Dis:" + diskon + "%", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    }
                    PrinterService.LineFeed(1);
                }
                diskon = cursor.getDouble(6);
                if (diskon > 0) {
                    if (model.DISCOUNT_3_TYPE.equals("2")) {
                        PrinterService.PrintText("Dis:" + Util.numberFormat.format(diskon), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    } else {
                        PrinterService.PrintText("Dis:" + diskon + "%", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
                    }
                    PrinterService.LineFeed(1);
                }
            }
            PrinterService.PrintText("--------------------------------", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            cursor.close();
            ctrlTakingOrder.close();
            PrinterService.LineFeed(1);
            PrinterService.PrintText("" + commonModel.QTY_B + "+", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            PrinterService.PrintText("\t" + commonModel.QTY_K, ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            PrinterService.PrintText("\t" + Util.numberFormat.format(commonModel.PRICE), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            PrinterService.LineFeed(1);
            if (customerTypeModel.DiskonType.equals("2")) {
                PrinterService.PrintText("\t\t-" + Util.numberFormat.format(commonModel.DISCOUNT), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            } else {
                PrinterService.PrintText("\t" + commonModel.DISCOUNT + "%", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            }
            PrinterService.LineFeed(1);
            PrinterService.PrintText("\t\t" + Util.numberFormat.format(commonModel.S_AMOUNT), ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, 0);
            PrinterService.LineFeed(1);
            PrinterService.PrintText("--------------------------------", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(1);
            PrinterService.PrintText("Power By sadix.net", ZQPrinter.ALIGNMENT_LEFT, ZQPrinter.FT_DEFAULT, ZQPrinter.TS_0WIDTH | ZQPrinter.TS_0HEIGHT);
            PrinterService.LineFeed(5);
            if (listener != null) {
                listener.printFinish();
            }
        } catch (Exception e) {
            if (listener != null) {
                listener.errorPrint(e.getMessage());
            }
        }
    }

    /*public static String sendUlang(Context context, String CommonID) {
        Realm realm = Realm.getDefaultInstance();
        try {
            CtrlCommon ctrlCommon = new CtrlCommon(context);
            CommonModel commonModel = null;
            if (CommonID == null) {
                commonModel = ctrlCommon.getPendingCust();
            } else {
                commonModel = ctrlCommon.getCommonModel(CommonID);
            }
            if (commonModel != null) {
                JSONObject jsonSend = Util.convertJSONEndVisit(context, commonModel.CommonID, realm);
                android.util.Log.e("result", "jsonSend::" + jsonSend.toString());
                if (jsonSend != null) {

                    Log.wtf("URL Server", Util.getServerUrl(context));

                    String result = ConnectionServer.requestJSONObjectNonThread(jsonSend, Util.getServerUrl(context) + "endvisit", true);
                    android.util.Log.e("result", "result::" + result);
                    if (result != null) {
                        JSONObject jsonRespon = new JSONObject(result);
                        if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                            commonModel.Status = "1";
                            ctrlCommon.update(commonModel);
                            return null;
                        } else {
                            return jsonRespon.getString("Message");
                        }
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }*/

    public static String sendUlang(Context context, String CommonID) {
        Realm realmO = Realm.getDefaultInstance();

        ctrlCustomer = new CtrlCustomer(context);

        Log.d("Send Ulang Sadix", "Called");
        try {
//            CtrlCommon ctrlCommon = new CtrlCommon(context);
            com.bhn.sadix.Data.CommonModel commonModel = null;
            if (CommonID == null) {
                commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Status", String.valueOf(0)).equalTo("Done", 1).findFirst();
            } else {
                commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("CommonID", CommonID).equalTo("Done", 1).findFirst();
            }

            if (commonModel != null) {
                CustomerModel customerModel = ctrlCustomer.get(commonModel.CustomerId);
                if (!customerModel.isEdit.equals("1")) {
                    JSONObject jsonSend = Util.convertJSONEndVisit(context, commonModel.CommonID, realmO);
                    android.util.Log.e("result", "jsonSend::" + jsonSend.toString());
                    if (jsonSend != null) {
                        Log.wtf("URL Server", Util.getServerUrl(context));

                        String result = ConnectionServer.requestJSONObjectNonThread(jsonSend, Util.getServerUrl(context) + "endvisit", true);

                        android.util.Log.e("result", "result::" + result);
                        if (result != null) {
                            JSONObject jsonRespon = new JSONObject(result);
                            if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
//                            final com.bhn.sadix.Data.CommonModel finalCommonModel = commonModel;
                                final com.bhn.sadix.Data.CommonModel finalCommonModel1 = commonModel;
                                realmO.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        com.bhn.sadix.Data.CommonModel model = realm.where(com.bhn.sadix.Data.CommonModel.class)
                                                .equalTo("CommonID", finalCommonModel1.CommonID).findFirst();
                                        model.Status = "1";
                                    }
                                });

//                            realmO.close();
//                            ctrlCommon.update(commonModel);
                                return null;
                            } else {
                                return jsonRespon.getString("Message");
                            }
                        }
                    }
                }
                customerModel = null;
            }
            ctrlCustomer = null;
            realmO.close();
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public static String sendUlangCustomer(Context context, CustomerModel customerModel) {
        try {
            CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
            CtrlCommon ctrlCommon = new CtrlCommon(context);
            CtrlCustPhoto ctrlCustPhoto = new CtrlCustPhoto(context);
            if (customerModel == null) {
                customerModel = ctrlCustomer.getGagalKirimEdit();
            }
            if (customerModel != null) {
                JSONObject json = Util.convertJSONCustomer(context, customerModel);
//				json.put("CustomerId", Integer.parseInt(customerModel.CustId));
//				json.put("CustomerName", customerModel.CustomerName);
//				json.put("CustomerAddress", customerModel.CustomerAddress);
//				json.put("CustomerEmail", customerModel.CustomerEmail);
//				json.put("CustomerPhone", customerModel.CustomerPhone);
//				json.put("CustomerFax", customerModel.CustomerFax);
//				json.put("CustomerNpwp", customerModel.CustomerNPWP);
//				json.put("OwnerName", customerModel.OwnerName);
//				json.put("OwnerAddress", customerModel.OwnerAddress);
//				json.put("OwnerEmail", customerModel.OwnerEmail);
//				json.put("OwnerPhone", customerModel.OwnerPhone);
//				json.put("CustomerTypeId", Integer.parseInt(customerModel.CustomerTypeId));
//				json.put("GeoLat", Double.parseDouble(customerModel.GeoLat.equals("") ? "0" : customerModel.GeoLat));
//				json.put("GeoLong", Double.parseDouble(customerModel.GeoLong.equals("") ? "0" : customerModel.GeoLong));
//				json.put("StatusId", customerModel.isNew.equals("1") ? 1 : 2);
//				json.put("SalesId", Integer.parseInt(UserModel.getInstance(context).SalesId));
//				json.put("Picture", "xxx.jpg");

                String result = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "postcustdata", true);
                android.util.Log.e("result", "" + result);
                if (result != null) {
                    final JSONObject jsonRespon = new JSONObject(result);
                    android.util.Log.e("customerModel.isNew", "" + customerModel.isNew);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        if (customerModel.isNew != null && customerModel.isNew.equals("1")) {
                            if (jsonRespon.has("ServerID") && jsonRespon.getInt("ServerID") > 0) {
                                customerModel.CustId = jsonRespon.getString("ServerID");
                                customerModel.isNew = "0";
                                ctrlCommon.updateCustomerIdByCustomerRandomId(customerModel);
                                ctrlCustPhoto.updateCustomerIdByCustomerRandomId(customerModel);
                                android.util.Log.e("customerModel.isNew", "" + customerModel.isNew);

                                Realm realm = Realm.getDefaultInstance();

                                final CustomerModel finalCustomerModel = customerModel;

                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        com.bhn.sadix.Data.CommonModel commonModel = realm.where(com.bhn.sadix.Data.CommonModel.class)
                                                .equalTo("CustomerId", finalCustomerModel.RandomID).findFirst();

                                        try {
                                            if (commonModel != null) {
                                                commonModel.CustomerId = jsonRespon.getString("ServerID");
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                        customerModel.isEdit = "0";
                        ctrlCustomer.update(customerModel);
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            return "Error::" + e.getMessage();
        }
    }

    public static String sendUlangCustPhoto(Context context, CustPhotoModel custPhotoModel) {
        try {
            CtrlCustPhoto ctrlCustPhoto = new CtrlCustPhoto(context);
            if (custPhotoModel == null) {
                custPhotoModel = ctrlCustPhoto.getPending();
            }
            if (custPhotoModel != null) {
                JSONObject json = Util.convertJSONCustPhoto(context, custPhotoModel);
//				json.put("SalesID", UserModel.getInstance(context).SalesId);
//				json.put("CustomerID", custPhotoModel.CustomerID);
//				json.put("PhotoTypeID", custPhotoModel.PhotoTypeID);
//				json.put("PhotoPic", Base64.encodeBytes(Util.readFile(LOKASI_IMAGE+custPhotoModel.PhotoPic)));
                String result = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "photodata", true);
                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        ctrlCustPhoto.delete(custPhotoModel.CustomerID, custPhotoModel.PhotoTypeID);
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            Log.e("sendUlangCustPhoto", e.getMessage());
            return e.getMessage();
        }
    }

    public static String sendUlangCustBranding(Context context, CustomerBrandingModel custBrandingModel) {
        try {
            Realm realmO = Realm.getDefaultInstance();


            CtrlCustBranding ctrlCustBranding = new CtrlCustBranding(context);
            if (custBrandingModel == null) {
                custBrandingModel = realmO.where(CustomerBrandingModel.class).equalTo("Done", 1).findFirst();
                if (custBrandingModel != null) {
                    custBrandingModel = realmO.copyFromRealm(custBrandingModel);
                }
            }

            if (custBrandingModel != null) {
                final String commonID = custBrandingModel.CommonID;
                JSONObject json = Util.convertJSONCustBranding(context, custBrandingModel);
//				json.put("SalesID", custBrandingModel.SalesID);
//				json.put("CustomerID", custBrandingModel.CustomerID);
//				json.put("BrandingID", custBrandingModel.BrandingID);
//				json.put("PhotoPic", Base64.encodeBytes(Util.readFile(LOKASI_IMAGE+custBrandingModel.PhotoPic)));
//				json.put("GeoLong", custBrandingModel.GeoLong);
//				json.put("GeoLat", custBrandingModel.GeoLat);
//				json.put("Description", custBrandingModel.Description);
                String result = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "photobranding", true);
                Log.d("Ctrl Branding", "Send Ulang");
                Log.d("Result branding", result);
                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        Log.d("Ctrl Branding", "Deleted");
                        realmO.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                CustomerBrandingModel model = realm.where(CustomerBrandingModel.class).equalTo("CommonID", commonID).findFirst();
                                model.deleteFromRealm();
                            }
                        });
                        realmO.close();
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("sendUlangCustBranding", e.getMessage());
            return e.getMessage();
        }
    }

    public static String sendUlangLogGPS(Context context, LogGpsModel model) {
        try {
            CtrlLogGps ctrlLogGps = new CtrlLogGps(context);
            if (model == null) {
                model = ctrlLogGps.get();
            }
            if (model != null) {
                JSONObject json = Util.convertJSONLogGPS(context, model);
//				json.put("SalesID", Integer.parseInt(model.SalesID));
//				json.put("IMEI", model.IMEI);
//				json.put("LogTime", model.LogTime);
//				json.put("SC", model.SC);
//				json.put("GeoLong", model.GeoLong);
//				json.put("GeoLat", model.GeoLat);
//				json.put("statusLog", model.statusLog);
                String respon = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "salesmanjourney", true);
                if (respon != null) {
                    JSONObject jsonRespon = new JSONObject(respon);
                    if (jsonRespon.getString("Result").equals("INSERT_OK")) {
                        ctrlLogGps.delete(model.LogTime);
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            Log.e("sendUlangLogGPS", e.getMessage());
            return e.getMessage();
        }
    }

    public static String sendUlangLogSurvey(Context context, CommonSurveyModel model) {
        try {
            CtrlSurvey ctrlSurvey = new CtrlSurvey(context);
            if (model == null) {
                model = ctrlSurvey.getCommonSurvey();
            }
            if (model != null) {
                JSONObject json = Util.convertJSONLogSurvey(context, model);
//				JSONObject Common = new JSONObject();
//				Common.put("CustomerId", Integer.parseInt(model.CustomerId));
//				Common.put("SalesId", Integer.parseInt(model.SalesId));
//				Common.put("GeoLat", Double.parseDouble(model.GeoLat));
//				Common.put("GeoLong", Double.parseDouble(model.GeoLong));
//				Common.put("BeginSurveyTime", model.BeginSurveyTime);
//				Common.put("EndSurveyTime", model.EndSurveyTime);
//				Common.put("FormId", model.FormId);
//				JSONArray Survey = new JSONArray();
//				List<HasilSurveyModel> list = ctrlSurvey.listHasilSurvey(model.CommonId);
//				for (HasilSurveyModel hasilSurveyModel : list) {
//					JSONObject jsonSurvey = new JSONObject();
//					jsonSurvey.put("FormId", Integer.parseInt(hasilSurveyModel.FormId));
//					jsonSurvey.put("SurveyId", Integer.parseInt(hasilSurveyModel.SurveyId));
//					jsonSurvey.put("ListID", Integer.parseInt(hasilSurveyModel.ListID));
//					jsonSurvey.put("ListValue", hasilSurveyModel.ListValue);
//					Survey.put(jsonSurvey);
//				}
//				json.put("Common", Common);
//				json.put("Survey", Survey);
                String respon = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "endsurvey", true);
                if (respon != null) {
                    JSONObject jsonRespon = new JSONObject(respon);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        ctrlSurvey.deleteCommonSurvey(model.CommonId);
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("sendUlangLogSurvey", e.getMessage());
            return e.getMessage();
        }
    }

    public static String sendUlangSalesNote(Context context) {
        try {
            DatabaseHelper db = new DatabaseHelper(context);
            JSONObject data = db.getAwalData("SalesNote");
            if (data != null) {
                String respon = ConnectionServer.requestJSONObjectNonThread(data, Util.getServerUrl(context) + "salesnote", true);
                if (respon != null) {
                    JSONObject jsonRespon = new JSONObject(respon);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        db.deleteByID("SalesNote", "ID", data.getString("ID"));
                        return null;
                    } else {
                        return jsonRespon.getString("Message");
                    }
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


    /*public static JSONObject convertJSONEndVisit(Context context, String CommonID) {
        try {
            CtrlCommon ctrlCommon = new CtrlCommon(context);
            CtrlSKU ctrlSKU = new CtrlSKU(context);
            CtrlRetur ctrlRetur = new CtrlRetur(context);
            CtrlStokOutlet ctrlStokOutlet = new CtrlStokOutlet(context);
            CtrlTakingOrder ctrlTakingOrder = new CtrlTakingOrder(context);
            CtrlStokCompetitor ctrlStokCompetitor = new CtrlStokCompetitor(context);
            CtrlCollector ctrlCollector = new CtrlCollector(context);
            CtrlStockGroup ctrlStockGroup = new CtrlStockGroup(context);
            CtrlDiscountPromo ctrlDiscountPromo = new CtrlDiscountPromo(context);
            CtrlDiscountResult ctrlDiscountResult = new CtrlDiscountResult(context);
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
//			CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
            CommonModel commonModel = null;
            if (CommonID == null) {
                commonModel = ctrlCommon.getPendingCust();
            } else {
                commonModel = ctrlCommon.getCommonModel(CommonID);
            }
            if (commonModel != null) {
                List<ReturModel> listRetur = ctrlRetur.getReturModel(commonModel.CommonID);
                List<StockOutletModel> listStok = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID);
                List<TakingOrderModel> liatOrder = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID);
                List<PrinsipleOrderModel> liatPrinsiple = ctrlTakingOrder.getPrinsipleOrderModel(commonModel.CommonID);
                List<StockCompetitorModel> listStokCompetitor = ctrlStokCompetitor.getStockCompetitorModel(commonModel.CommonID);

                if (commonModel.EndVisitTime == null || commonModel.EndVisitTime.equals("")) {
                    commonModel.EndVisitTime = Util.dateFormat.format(new Date());
                }

                JSONObject Common = new JSONObject();
                Common.put("SC", commonModel.SC);
                Common.put("GeoLat", Double.parseDouble(commonModel.GeoLat.equals("") ? "0" : commonModel.GeoLat));
                Common.put("CustomerId", commonModel.CustomerId);
                Common.put("BeginVisitTime", commonModel.BeginVisitTime);
                Common.put("GeoLong", Double.parseDouble(commonModel.GeoLong.equals("") ? "0" : commonModel.GeoLong));
                Common.put("LogTime", commonModel.LogTime);
                Common.put("EndVisitTime", commonModel.EndVisitTime);
                Common.put("SalesId", Integer.parseInt(commonModel.SalesId));
                Common.put("S_AMOUNT", commonModel.PRICE);
                Common.put("Description", commonModel.Description);
                Common.put("D_VALUE", commonModel.DISCOUNT);
                Common.put("G_AMOUNT", commonModel.S_AMOUNT);
                Common.put("Visit", commonModel.Visit);
                Common.put("RVisitID", commonModel.RVisitID);
                Common.put("InvNO", commonModel.InvNO);
                Common.put("ROrderID", commonModel.ROrderID);
                Common.put("StatusID", commonModel.StatusID);
                //penambahan RandomID
                Common.put("RandomID", commonModel.CommonID);
                Common.put("CustomerRID", commonModel.CustomerRID);
//				CustomerModel customerModel = ctrlCustomer.get(commonModel.CustomerId);
//				if(customerModel != null) {
//					Common.put("CustomerRID", customerModel.RandomID);
//				}

                JSONArray Stock = new JSONArray();
                for (StockOutletModel stockOutletModel : listStok) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(stockOutletModel.SKUId));
                    json.put("SKUName", stockOutletModel.SKUId);
                    json.put("Quantity", stockOutletModel.QTY_K);
                    json.put("Quantity2", stockOutletModel.QTY_B);
                    json.put("CustomerId", Integer.parseInt(stockOutletModel.CustId));
                    if (stockOutletModel.tanggal != null && !stockOutletModel.tanggal.equals("")) {
                        json.put("TransactionTime", stockOutletModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    //penambahan
                    json.put("RandomID", stockOutletModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    Stock.put(json);
                }
                JSONArray StockCompetitor = new JSONArray();
                for (StockCompetitorModel stockCompetitorModel : listStokCompetitor) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(stockCompetitorModel.SKUId));
                    json.put("SKUName", stockCompetitorModel.SKUId);
                    json.put("Quantity", stockCompetitorModel.QTY_K);
                    json.put("Quantity2", stockCompetitorModel.QTY_B);
                    json.put("CustomerId", Integer.parseInt(stockCompetitorModel.CustId));
//					json.put("TransactionTime", stockCompetitorModel.tanggal);
                    if (stockCompetitorModel.tanggal != null && !stockCompetitorModel.tanggal.equals("")) {
                        json.put("TransactionTime", stockCompetitorModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    json.put("Price", stockCompetitorModel.PRICE);
                    json.put("Price2", stockCompetitorModel.PRICE2);
                    json.put("Note", stockCompetitorModel.Note);
                    //penambahan
                    json.put("RandomID", stockCompetitorModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    StockCompetitor.put(json);
                }
                JSONArray Order = new JSONArray();
                for (TakingOrderModel takingOrderModel : liatOrder) {
                    SKUModel skuModel = ctrlSKU.get(takingOrderModel.SKUId);
                    JSONObject json = new JSONObject();
                    json.put("CustomerId", Integer.parseInt(takingOrderModel.CustId));
                    json.put("SKUId", Integer.parseInt(takingOrderModel.SKUId));
                    json.put("Amount", takingOrderModel.QTY_K);
                    json.put("Amount2", takingOrderModel.QTY_B);
                    json.put("DiscountType", takingOrderModel.DiscountType);
                    json.put("Discount1", takingOrderModel.DISCOUNT1);
                    json.put("Discount2", takingOrderModel.DISCOUNT2);
                    json.put("Discount3", takingOrderModel.DISCOUNT3);
                    double T_Discount = ((takingOrderModel.QTY_K * skuModel.HET_PRICE) + (takingOrderModel.QTY_B * skuModel.HET_PRICE_L)) - takingOrderModel.TOTAL_PRICE;
//					json.put("T_Discount", takingOrderModel.DISCOUNT1+takingOrderModel.DISCOUNT2+takingOrderModel.DISCOUNT3);
                    json.put("T_Discount", T_Discount);
                    json.put("Total", takingOrderModel.TOTAL_PRICE);
                    json.put("SalesProgramId", Integer.parseInt(
                            (takingOrderModel.SalesProgramId == null || takingOrderModel.SalesProgramId.equals("null")) ? "0" : takingOrderModel.SalesProgramId
                    ));
                    json.put("ItmDescription", takingOrderModel.note);
                    json.put("TopType", takingOrderModel.PAYMENT_TERM);
                    json.put("TopDays", takingOrderModel.LAMA_KREDIT);
//					json.put("TransactionTime", takingOrderModel.tanggal);
                    if (takingOrderModel.tanggal != null && !takingOrderModel.tanggal.equals("")) {
                        json.put("TransactionTime", takingOrderModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    //penambahan
                    json.put("RandomID", takingOrderModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    json.put("Price", takingOrderModel.PRICE_K);
                    json.put("PrinsipleID", takingOrderModel.PrinsipleID);
                    Order.put(json);
                }
                JSONArray Return = new JSONArray();
                for (ReturModel returModel : listRetur) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(returModel.SKUId));
                    json.put("SKUName", returModel.SKUId);
                    json.put("Quantity1", returModel.QTY_K);
                    json.put("Quantity2", returModel.QTY_B);
                    json.put("T_Return", Integer.parseInt(returModel.tipe != null ? returModel.tipe : "0"));
                    json.put("CustomerId", Integer.parseInt(returModel.CustId));
//					json.put("TransactionTime", returModel.tanggal);
                    if (returModel.tanggal != null && !returModel.tanggal.equals("")) {
                        json.put("TransactionTime", returModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    json.put("Description", returModel.note);
                    //penambahan
                    json.put("RandomID", returModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    Return.put(json);
                }
                JSONArray Collection = new JSONArray();
                List<CollectorModel> collectorModel = ctrlCollector.get(commonModel.CommonID);
                for (CollectorModel model : collectorModel) {
                    if ((model.tanggal != null && !model.tanggal.equals(""))) {
                        model.tanggal = commonModel.EndVisitTime;
                    }
                    Collection.put(model.toJson());
                }
//				if(collectorModel != null) {
//					JSONObject json = new JSONObject();
//					json.put("StatusId", collectorModel.status);
//					json.put("Data1", collectorModel.data1);
//					json.put("Data2", collectorModel.data2);
//					json.put("Data3", collectorModel.data3);
////					json.put("TransactionTime", collectorModel.tanggal);
//					if(collectorModel.tanggal != null && !collectorModel.tanggal.equals("")) {
//						json.put("TransactionTime", collectorModel.tanggal);
//					} else {
//						json.put("TransactionTime", commonModel.EndVisitTime);
//					}
//					Collection.put(json);
//				}
                JSONArray GroupStock = new JSONArray();
                List<StockGroupModel> listStockGroup = ctrlStockGroup.getStockGroupModel(commonModel.CommonID);
                for (StockGroupModel stockGroupModel : listStockGroup) {
                    GroupStock.put(stockGroupModel.toJSON());
                }
                JSONArray DiscountPromo = new JSONArray();
                List<DiscountPromoModel> listDiscountPromo = ctrlDiscountPromo.getDiscountPromoModel(commonModel.CommonID);
                for (DiscountPromoModel discountPromoModel : listDiscountPromo) {
                    DiscountPromo.put(discountPromoModel.toJSON());
                }
                JSONArray DiscountResult = new JSONArray();
                List<DiscountResultModel> listDiscountResult = ctrlDiscountResult.getDiscountResultModel(commonModel.CommonID);
                for (DiscountResultModel discountResultModel : listDiscountResult) {
                    DiscountResult.put(discountResultModel.toJSON());
                }

                JSONArray Asset = new JSONArray();
                List<JSONObject> listAset = databaseHelper.list("Aset", commonModel.CommonID);
                for (JSONObject jsonObject : listAset) {
                    if (jsonObject.getInt("jenisWidget") == 4) {
                        File file = new File(LOKASI_IMAGE + jsonObject.getString("namaFile"));
                        if (file.exists()) {
                            jsonObject.put("Value", Base64.encodeBytes(Util.readFile(file)));
                        } else {
                            jsonObject.put("Value", "");
                        }
                    }
                    Asset.put(jsonObject);
                }
                JSONArray Newasset = new JSONArray();
                listAset = databaseHelper.list("Newasset", commonModel.CommonID);
                for (JSONObject jsonObject : listAset) {
                    Newasset.put(jsonObject);
                }
                JSONArray Target = new JSONArray();
                listAset = databaseHelper.list("TargetProduk", commonModel.CommonID);
                for (JSONObject jsonObject : listAset) {
                    Target.put(jsonObject);
                }
                JSONArray PriceMon = new JSONArray();
                listAset = databaseHelper.list("PriceMonitoring", commonModel.CommonID);
                for (JSONObject jsonObject : listAset) {
                    PriceMon.put(jsonObject);
                }

                JSONArray Prinsiple = new JSONArray();
                for (PrinsipleOrderModel prinsipleOrderModel : liatPrinsiple) {
                    Prinsiple.put(prinsipleOrderModel.toJSON());
                }

                JSONArray VisitNote = new JSONArray();
                List<JSONObject> lstVisitNot = databaseHelper.list("VisitNote", commonModel.CommonID);
                for (JSONObject jvisitNot : lstVisitNot) {
                    VisitNote.put(jvisitNot);
                }

                JSONObject jsonSend = new JSONObject();
                jsonSend.put("Common", Common);
                jsonSend.put("Stock", Stock);
                jsonSend.put("Order", Order);
                jsonSend.put("Return", Return);
                jsonSend.put("Collection", Collection);
                jsonSend.put("Competitor", StockCompetitor);
                jsonSend.put("GroupStock", GroupStock);
                jsonSend.put("DiscountPromo", DiscountPromo);
                jsonSend.put("DiscountResult", DiscountResult);
                jsonSend.put("Asset", Asset);
                jsonSend.put("Newasset", Newasset);
                jsonSend.put("Target", Target);
                jsonSend.put("PriceMon", PriceMon);
                jsonSend.put("Prinsiple", Prinsiple);
                jsonSend.put("VisitNote", VisitNote);

                return jsonSend;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    public static boolean convertCSVEndVisit(Context context, String CommonID, Realm realm) {
        Realm realm0 = realm;
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        CtrlSKU ctrlSKU = new CtrlSKU(context);

        String SEPARATOR = ";";

        try {
            com.bhn.sadix.Data.CommonModel commonModel = null;
            if (CommonID == null) {
                commonModel = realm0.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Status", 0).findFirst();
            } else {
                commonModel = realm0.where(com.bhn.sadix.Data.CommonModel.class).equalTo("CommonID", CommonID).findFirst();
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(Util.LOKASI + "ExportEndVisit.csv", true));

            if (commonModel != null) {
                final com.bhn.sadix.Data.CommonModel finalCommonModel = commonModel;
                realm0.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (finalCommonModel.EndVisitTime == null || finalCommonModel.EndVisitTime.equals("")) {
                            finalCommonModel.EndVisitTime = Util.dateFormat.format(new Date());
                        }
                    }
                });

                JSONObject Common = new JSONObject(/*gson.toJson(commonModel)*/);

                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Common");
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.CommonID);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.SC);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(Double.parseDouble(commonModel.GeoLat.equals("") ? "0" : commonModel.GeoLat));
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(Double.parseDouble(commonModel.GeoLong.equals("") ? "0" : commonModel.GeoLong));
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.CustomerId);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.BeginVisitTime);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.LogTime);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.EndVisitTime);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(Integer.parseInt(commonModel.SalesId));
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.PRICE);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.DISCOUNT);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.Description);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.S_AMOUNT);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.Visit);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.RVisitID);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.InvNO);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.ROrderID);
                stringBuffer.append(SEPARATOR);
                stringBuffer.append(commonModel.StatusID);

                writer.write(stringBuffer.toString());
                writer.newLine();

                RealmList<StockCustomerModel> listStok = commonModel.stockCustomerModel;
                for (StockCustomerModel stockOutletModel : listStok) {
                    SKUModel skuModel = ctrlSKU.get(stockOutletModel.SKUId);

                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Stock Outlet");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(commonModel.CommonID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockOutletModel.RandomID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(Integer.parseInt(stockOutletModel.SKUId));
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(skuModel.ProductName);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockOutletModel.QTY_K);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockOutletModel.QTY_B);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(Integer.parseInt(stockOutletModel.CustId));
                    stringBuffer.append(SEPARATOR);
                    if (stockOutletModel.tanggal != null && !stockOutletModel.tanggal.equals("")) {
                        stringBuffer.append(stockOutletModel.tanggal);
                    } else {
                        stringBuffer.append(commonModel.EndVisitTime);
                    }

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmList<StockCompetitorModel> listStokCompetitor = commonModel.stockCompetitorModel;
                JSONArray StockCompetitor = new JSONArray();
                for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModel : listStokCompetitor) {
                    SKUModel skuModel = ctrlSKU.get(stockCompetitorModel.SKUId);

                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Stock Competitor");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.RandomID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(commonModel.CommonID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(Integer.parseInt(stockCompetitorModel.SKUId));
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(skuModel.ProductName);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.QTY_K);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.QTY_B);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.CustId);

                    if (stockCompetitorModel.tanggal != null && !stockCompetitorModel.tanggal.equals("")) {
                        stringBuffer.append(stockCompetitorModel.tanggal);
                    } else {
                        stringBuffer.append(commonModel.EndVisitTime);
                    }

                    stringBuffer.append(SEPARATOR);

                    stringBuffer.append(stockCompetitorModel.PRICE);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.PRICE2);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.Note);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockCompetitorModel.CustId);

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmList<TakingOrderModel> listOrder = commonModel.takingOrderModel;
                JSONArray Order = new JSONArray();
                for (com.bhn.sadix.Data.TakingOrderModel takingOrderModel : listOrder) {
                    SKUModel skuModel = ctrlSKU.get(takingOrderModel.SKUId);

                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Taking Order");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.RandomID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(commonModel.CommonID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.CustId);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.QTY_K);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.QTY_B);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.DiscountType);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.DISCOUNT1);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.DISCOUNT2);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.DISCOUNT3);
                    stringBuffer.append(SEPARATOR);

                    double T_Discount = ((takingOrderModel.QTY_K * skuModel.HET_PRICE) + (takingOrderModel.QTY_B * skuModel.HET_PRICE_L)) - takingOrderModel.TOTAL_PRICE;
                    stringBuffer.append(T_Discount);
                    stringBuffer.append(SEPARATOR);

                    stringBuffer.append(takingOrderModel.TOTAL_PRICE);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(Integer.parseInt(
                            (takingOrderModel.SalesProgramId == null || takingOrderModel.SalesProgramId.equals("null")) || takingOrderModel.SalesProgramId.equals("") ? "0" : takingOrderModel.SalesProgramId
                    ));
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.note);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.PAYMENT_TERM);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.LAMA_KREDIT);
                    stringBuffer.append(SEPARATOR);
                    if (takingOrderModel.tanggal != null && !takingOrderModel.tanggal.equals("")) {
                        stringBuffer.append(takingOrderModel.tanggal);
                    } else {
                        stringBuffer.append(commonModel.EndVisitTime);
                    }
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.PRICE_K);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(takingOrderModel.PrinsipleID);

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmList<ReturnModel> listRetur = commonModel.returnModel;
                for (ReturnModel returModel : listRetur) {
                    SKUModel skuModel = ctrlSKU.get(returModel.SKUId);

                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Return");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(returModel.RandomID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(commonModel.CommonID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(returModel.SKUId);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(skuModel.ProductName);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(returModel.QTY_K);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(returModel.QTY_B);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(Integer.parseInt(returModel.tipe != null ? returModel.tipe : "0"));
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(returModel.CustId);
                    stringBuffer.append(SEPARATOR);

                    if (returModel.tanggal != null && !returModel.tanggal.equals("")) {
                        stringBuffer.append(returModel.tanggal);
                    } else {
                        stringBuffer.append(commonModel.EndVisitTime);
                    }
                    stringBuffer.append(SEPARATOR);

                    stringBuffer.append(returModel.note);

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

//                JSONArray Collection = new JSONArray();
                RealmList<CollectionModel> collectorModel = commonModel.collectionModel;
                for (final CollectionModel model : collectorModel) {
                    final com.bhn.sadix.Data.CommonModel finalCommonModel1 = commonModel;
                    realm0.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            if ((model.tanggal != null && !model.tanggal.equals(""))) {
                                model.tanggal = finalCommonModel1.EndVisitTime;
                            }
                        }
                    });

                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Collection");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(model.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmResults<StockGroupModel> listStockGroup = realm.where(StockGroupModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (StockGroupModel stockGroupModel : listStockGroup) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Stock Group");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(stockGroupModel.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmResults<DiscountResultModel> listDiscountResult;
                listDiscountResult = realm.where(DiscountResultModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (DiscountResultModel discountResultModel : listDiscountResult) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Discount Result");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(discountResultModel.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmResults<com.bhn.sadix.Data.DiscountPromoModel> discountPromoModel = realm.where(com.bhn.sadix.Data.DiscountPromoModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (com.bhn.sadix.Data.DiscountPromoModel model : discountPromoModel) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Discount Promo");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(model.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

//                JSONArray Asset = new JSONArray();
//                List<JSONObject> listAset = databaseHelper.list("Aset", commonModel.CommonID);
                RealmList<AssetModel> listAset = commonModel.assetModel;
//                RealmResults<AssetModel> listAset = realmO.where(AssetModel.class).equalTo("CommonID", CommonID).findAll();
                for (AssetModel asset : listAset) {
//                    JSONObject jsonObject = new JSONObject();
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Asset");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.Name);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.NameFile);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.TypeMnt);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.Posisi);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.tipe);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.Idx);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.GroupAset);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.RandomID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.JenisWidget);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.CommonID);
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(asset.AsetID);
                    stringBuffer.append(SEPARATOR);

                    if (asset.TypeMnt == 4) {
                        File file = new File(LOKASI_IMAGE + asset.NameFile);
                        if (file.exists()) {
                            stringBuffer.append(Base64.encodeBytes(Util.readFile(file)));
                        } else {
                            stringBuffer.append("");
                        }
                    } else {
                        stringBuffer.append(asset.Value);
                    }
                }

                List<JSONObject> listAsett;

                RealmList<AssetMutasiModel> assetMutasi = commonModel.assetMutasiModel;
                for (AssetMutasiModel mutasi : assetMutasi) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Mutasi Asset");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(mutasi.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

//                TODO: RE-CHECK THIS OUT
                /*JSONArray Target = new JSONArray();
                listAsett = databaseHelper.list("TargetProduk", commonModel.CommonID);
                for (JSONObject jsonObject : listAsett) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Target Product");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append()
                    Target.put(jsonObject);
                }*/

                RealmResults<PriceMonitoringModel> priceMonitoringModel = realm0.where(PriceMonitoringModel.class).equalTo("CommonID", CommonID).findAll();
                JSONArray PriceMon = new JSONArray();
//                listAsett = databaseHelper.list("PriceMonitoring", commonModel.CommonID);

                for (PriceMonitoringModel jsonObject : priceMonitoringModel) {
//                    JSONObject object = new JSONObject();
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Price Monitoring");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(jsonObject.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                RealmResults<PrincipleOrderModel> listPrinsiple = realm0.where(PrincipleOrderModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (PrincipleOrderModel prinsipleOrderModel : listPrinsiple) {
                    stringBuffer = new StringBuffer();
                    stringBuffer.append("Principle Order");
                    stringBuffer.append(SEPARATOR);
                    stringBuffer.append(prinsipleOrderModel.toCSV());

                    writer.write(stringBuffer.toString());
                    writer.newLine();
                }

                /*JSONArray VisitNote = new JSONArray();
                List<JSONObject> lstVisitNot = databaseHelper.list("VisitNote", commonModel.CommonID);
                for (JSONObject jvisitNot : lstVisitNot) {
                    VisitNote.put(jvisitNot);
                }*/

               /* realm.close();
                JSONObject jsonSend = new JSONObject();
                jsonSend.put("Common", Common);
                jsonSend.put("Stock", Stock);
                jsonSend.put("Order", Order);
                jsonSend.put("Return", Return);
                jsonSend.put("Collection", Collection);
                jsonSend.put("Competitor", StockCompetitor);
                jsonSend.put("GroupStock", GroupStock);
                jsonSend.put("DiscountPromo", DiscountPromo);
                jsonSend.put("DiscountResult", DiscountResult);
                jsonSend.put("Asset", Asset);
                jsonSend.put("Newasset", Newasset);
                jsonSend.put("Target", Target);
                jsonSend.put("PriceMon", PriceMon);
                jsonSend.put("Prinsiple", Prinsiple);
                jsonSend.put("VisitNote", VisitNote);*/

                writer.flush();
                writer.close();

                return true;
            } else {
                return false;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static JSONObject convertJSONEndVisit(Context context, String CommonID, Realm realm) {
        Realm realmO = realm;
        try {
//            CtrlCommon ctrlCommon = new CtrlCommon(context);
            /*CtrlRetur ctrlRetur = new CtrlRetur(context);
            CtrlStokOutlet ctrlStokOutlet = new CtrlStokOutlet(context);
            CtrlTakingOrder ctrlTakingOrder = new CtrlTakingOrder(context);
            CtrlStokCompetitor ctrlStokCompetitor = new CtrlStokCompetitor(context);
            CtrlCollector ctrlCollector = new CtrlCollector(context);
            CtrlStockGroup ctrlStockGroup = new CtrlStockGroup(context);
            CtrlDiscountPromo ctrlDiscountPromo = new CtrlDiscountPromo(context);
            CtrlDiscountResult ctrlDiscountResult = new CtrlDiscountResult(context);*/
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            CtrlSKU ctrlSKU = new CtrlSKU(context);
//			CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
//            CommonModel commonModel = null;
            com.bhn.sadix.Data.CommonModel commonModel = null;
            if (CommonID == null) {
                commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Status", 0).findFirst();
            } else {
                commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("CommonID", CommonID).findFirst();
            }

            if (commonModel != null) {
                final com.bhn.sadix.Data.CommonModel finalCommonModel = commonModel;
                realmO.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (finalCommonModel.EndVisitTime == null || finalCommonModel.EndVisitTime.equals("")) {
                            finalCommonModel.EndVisitTime = Util.dateFormat.format(new Date());
                        }
                    }
                });


                JSONObject Common = new JSONObject(/*gson.toJson(commonModel)*/);
                Common.put("SC", commonModel.SC);
                Common.put("GeoLat", Double.parseDouble(commonModel.GeoLat.equals("") ? "0" : commonModel.GeoLat));
                Common.put("CustomerId", commonModel.CustomerId);
                Common.put("BeginVisitTime", commonModel.BeginVisitTime);
                Common.put("GeoLong", Double.parseDouble(commonModel.GeoLong.equals("") ? "0" : commonModel.GeoLong));
                Common.put("LogTime", commonModel.LogTime);
                Common.put("EndVisitTime", commonModel.EndVisitTime);
                Common.put("SalesId", Integer.parseInt(commonModel.SalesId));
                Common.put("S_AMOUNT", commonModel.PRICE);
                Common.put("Description", commonModel.Description);
                Common.put("D_VALUE", commonModel.DISCOUNT);
                Common.put("G_AMOUNT", commonModel.S_AMOUNT);
                Common.put("Visit", commonModel.Visit);
                Common.put("RVisitID", commonModel.RVisitID);
                Common.put("InvNO", commonModel.InvNO);
                Common.put("ROrderID", commonModel.ROrderID);
                Common.put("StatusID", commonModel.StatusID);
                //penambahan RandomID
                Common.put("RandomID", commonModel.CommonID);
                Common.put("CustomerRID", commonModel.CustomerRID);
//				CustomerModel customerModel = ctrlCustomer.get(commonModel.CustomerId);
//				if(customerModel != null) {
//					Common.put("CustomerRID", customerModel.RandomID);
//				}

//                RealmResults<StockCustomerModel> listStok = realmO.where(StockCustomerModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                RealmList<StockCustomerModel> listStok = commonModel.stockCustomerModel;
                JSONArray Stock = new JSONArray();
                for (StockCustomerModel stockOutletModel : listStok) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(stockOutletModel.SKUId));
                    json.put("SKUName", stockOutletModel.SKUId);
                    json.put("Quantity", stockOutletModel.QTY_K);
                    json.put("Quantity2", stockOutletModel.QTY_B);
                    json.put("CustomerId", Integer.parseInt(stockOutletModel.CustId));
                    if (stockOutletModel.tanggal != null && !stockOutletModel.tanggal.equals("")) {
                        json.put("TransactionTime", stockOutletModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    //penambahan
                    json.put("RandomID", stockOutletModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    Stock.put(json);
                }

//                RealmResults<com.bhn.sadix.Data.StockCompetitorModel> listStokCompetitor = realmO.where(com.bhn.sadix.Data.StockCompetitorModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                RealmList<StockCompetitorModel> listStokCompetitor = commonModel.stockCompetitorModel;
                JSONArray StockCompetitor = new JSONArray();
                for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModel : listStokCompetitor) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(stockCompetitorModel.SKUId));
                    json.put("SKUName", stockCompetitorModel.SKUId);
                    json.put("Quantity", stockCompetitorModel.QTY_K);
                    json.put("Quantity2", stockCompetitorModel.QTY_B);
                    json.put("CustomerId", Integer.parseInt(stockCompetitorModel.CustId));
//					json.put("TransactionTime", stockCompetitorModel.tanggal);
                    if (stockCompetitorModel.tanggal != null && !stockCompetitorModel.tanggal.equals("")) {
                        json.put("TransactionTime", stockCompetitorModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    json.put("Price", stockCompetitorModel.PRICE);
                    json.put("Price2", stockCompetitorModel.PRICE2);
                    json.put("Note", stockCompetitorModel.Note);
                    //penambahan
                    json.put("RandomID", stockCompetitorModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    StockCompetitor.put(json);
                }

                RealmList<TakingOrderModel> listOrder = commonModel.takingOrderModel;
//                RealmResults<com.bhn.sadix.Data.TakingOrderModel> listOrder = realmO.where(com.bhn.sadix.Data.TakingOrderModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                JSONArray Order = new JSONArray();
                for (com.bhn.sadix.Data.TakingOrderModel takingOrderModel : listOrder) {
                    SKUModel skuModel = ctrlSKU.get(takingOrderModel.SKUId);
                    JSONObject json = new JSONObject();
                    json.put("CustomerId", Integer.parseInt(takingOrderModel.CustId));
                    json.put("SKUId", Integer.parseInt(takingOrderModel.SKUId));
                    json.put("Amount", takingOrderModel.QTY_K);
                    json.put("Amount2", takingOrderModel.QTY_B);
                    json.put("DiscountType", takingOrderModel.DiscountType);
                    json.put("Discount1", takingOrderModel.DISCOUNT1);
                    json.put("Discount2", takingOrderModel.DISCOUNT2);
                    json.put("Discount3", takingOrderModel.DISCOUNT3);
                    double T_Discount = ((takingOrderModel.QTY_K * skuModel.HET_PRICE) + (takingOrderModel.QTY_B * skuModel.HET_PRICE_L)) - takingOrderModel.TOTAL_PRICE;
//					json.put("T_Discount", takingOrderModel.DISCOUNT1+takingOrderModel.DISCOUNT2+takingOrderModel.DISCOUNT3);
                    json.put("T_Discount", T_Discount);
                    json.put("Total", takingOrderModel.TOTAL_PRICE);
                    json.put("SalesProgramId", Integer.parseInt(
                            (takingOrderModel.SalesProgramId == null || takingOrderModel.SalesProgramId.equals("null")) || takingOrderModel.SalesProgramId.equals("") ? "0" : takingOrderModel.SalesProgramId
                    ));
                    json.put("ItmDescription", takingOrderModel.note);
                    json.put("TopType", takingOrderModel.PAYMENT_TERM);
                    json.put("TopDays", takingOrderModel.LAMA_KREDIT);
//					json.put("TransactionTime", takingOrderModel.tanggal);
                    if (takingOrderModel.tanggal != null && !takingOrderModel.tanggal.equals("")) {
                        json.put("TransactionTime", takingOrderModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    //penambahan
                    json.put("RandomID", takingOrderModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    json.put("Price", takingOrderModel.PRICE_K);
                    json.put("PrinsipleID", takingOrderModel.PrinsipleID);
                    Order.put(json);
                }

                RealmList<ReturnModel> listRetur = commonModel.returnModel;
//                RealmResults<ReturnModel> listRetur = realmO.where(ReturnModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                JSONArray Return = new JSONArray();
                for (ReturnModel returModel : listRetur) {
                    JSONObject json = new JSONObject();
                    json.put("SKUId", Integer.parseInt(returModel.SKUId));
                    json.put("SKUName", returModel.SKUId);
                    json.put("Quantity1", returModel.QTY_K);
                    json.put("Quantity2", returModel.QTY_B);
                    json.put("T_Return", Integer.parseInt(returModel.tipe != null ? returModel.tipe : "0"));
                    json.put("CustomerId", Integer.parseInt(returModel.CustId));
//					json.put("TransactionTime", returModel.tanggal);
                    if (returModel.tanggal != null && !returModel.tanggal.equals("")) {
                        json.put("TransactionTime", returModel.tanggal);
                    } else {
                        json.put("TransactionTime", commonModel.EndVisitTime);
                    }
                    json.put("Description", returModel.note);
                    //penambahan
                    json.put("RandomID", returModel.RandomID);
                    json.put("CommonID", commonModel.CommonID);
                    Return.put(json);
                }

                JSONArray Collection = new JSONArray();
                RealmList<CollectionModel> collectorModel = commonModel.collectionModel;
//                RealmResults<CollectionModel> collectorModel = realmO.where(CollectionModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (final CollectionModel model : collectorModel) {
                    final com.bhn.sadix.Data.CommonModel finalCommonModel1 = commonModel;
                    realmO.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            if ((model.tanggal != null && !model.tanggal.equals(""))) {
                                model.tanggal = finalCommonModel1.EndVisitTime;
                            }
                        }
                    });
                    Collection.put(model.toJson());
                    Log.d("Collection JSON", model.toJson().toString());
                }
//				if(collectorModel != null) {
//					JSONObject json = new JSONObject();
//					json.put("StatusId", collectorModel.status);
//					json.put("Data1", collectorModel.data1);
//					json.put("Data2", collectorModel.data2);
//					json.put("Data3", collectorModel.data3);
////					json.put("TransactionTime", collectorModel.tanggal);
//					if(collectorModel.tanggal != null && !collectorModel.tanggal.equals("")) {
//						json.put("TransactionTime", collectorModel.tanggal);
//					} else {
//						json.put("TransactionTime", commonModel.EndVisitTime);
//					}
//					Collection.put(json);
//				}

                JSONArray GroupStock = new JSONArray();
                RealmResults<StockGroupModel> listStockGroup = realm.where(StockGroupModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (StockGroupModel stockGroupModel : listStockGroup) {
                    GroupStock.put(stockGroupModel.toJSON());
                }

                JSONArray DiscountResult = new JSONArray();
                RealmResults<DiscountResultModel> listDiscountResult;
                listDiscountResult = realm.where(DiscountResultModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (DiscountResultModel discountResultModel : listDiscountResult) {
                    DiscountResult.put(discountResultModel.toJSON());
                }

                JSONArray DiscountPromo = new JSONArray();
                RealmResults<com.bhn.sadix.Data.DiscountPromoModel> discountPromoModel = realm.where(com.bhn.sadix.Data.DiscountPromoModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                for (com.bhn.sadix.Data.DiscountPromoModel model : discountPromoModel) {
                    DiscountPromo.put(model.toJSON());
                }

                JSONArray Asset = new JSONArray();
//                List<JSONObject> listAset = databaseHelper.list("Aset", commonModel.CommonID);
                RealmList<AssetModel> listAset = commonModel.assetModel;
//                RealmResults<AssetModel> listAset = realmO.where(AssetModel.class).equalTo("CommonID", CommonID).findAll();
                for (AssetModel asset : listAset) {
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("Name", asset.Name);
                    jsonObject.put("namaFile", asset.NameFile);
                    jsonObject.put("TypeMnt", asset.TypeMnt);
                    jsonObject.put("Posisi", asset.Posisi);
                    jsonObject.put("tipe", asset.tipe);
                    jsonObject.put("Idx", asset.Idx);
                    jsonObject.put("GroupAset", asset.GroupAset);
                    jsonObject.put("RandomID", asset.RandomID);
                    jsonObject.put("jenisWidget", asset.JenisWidget);
                    jsonObject.put("CommonID", asset.CommonID);
                    jsonObject.put("AsetID", asset.AsetID);

                    if (asset.TypeMnt == 4) {
                        File file = new File(LOKASI_IMAGE + asset.NameFile);
                        if (file.exists()) {
                            jsonObject.put("Value", Base64.encodeBytes(Util.readFile(file)));
                        } else {
                            jsonObject.put("Value", "");
                        }
                    } else {
                        jsonObject.put("Value", asset.Value);
                    }

                    Asset.put(jsonObject);
                }

                List<JSONObject> listAsett;

                RealmList<AssetMutasiModel> assetMutasi = commonModel.assetMutasiModel;
                JSONArray Newasset = new JSONArray();
                for (AssetMutasiModel mutasi : assetMutasi) {
                    JSONObject object = new JSONObject();
                    
                    object.put("AsetName", mutasi.AsetName);
                    object.put("AsetID", mutasi.AsetID);
                    object.put("MerkID", mutasi.MerkID);
                    object.put("TypeID", mutasi.TypeID);
                    object.put("Remark", mutasi.Remark);
                    object.put("NoSeri", mutasi.NoSeri);
                    object.put("CommonID", mutasi.CommonID);
                    object.put("RandomID", mutasi.RandomID);

                    Newasset.put(object);
                }

                JSONArray Target = new JSONArray();
                listAsett = databaseHelper.list("TargetProduk", commonModel.CommonID);
                for (JSONObject jsonObject : listAsett) {
                    Target.put(jsonObject);
                }

                RealmResults<PriceMonitoringModel> priceMonitoringModel = realmO.where(PriceMonitoringModel.class).equalTo("CommonID", CommonID).findAll();
                JSONArray PriceMon = new JSONArray();
//                listAsett = databaseHelper.list("PriceMonitoring", commonModel.CommonID);

                for (PriceMonitoringModel jsonObject : priceMonitoringModel) {
//                    JSONObject object = new JSONObject();
                    PriceMon.put(jsonObject.toJson());
                }

                RealmResults<PrincipleOrderModel> listPrinsiple = realmO.where(PrincipleOrderModel.class).equalTo("CommonID", commonModel.CommonID).findAll();
                JSONArray Prinsiple = new JSONArray();
                for (PrincipleOrderModel prinsipleOrderModel : listPrinsiple) {
                    Prinsiple.put(prinsipleOrderModel.toJSON());
                }

                JSONArray VisitNote = new JSONArray();
                List<JSONObject> lstVisitNot = databaseHelper.list("VisitNote", commonModel.CommonID);
                for (JSONObject jvisitNot : lstVisitNot) {
                    VisitNote.put(jvisitNot);
                }

//                realm.close();
                JSONObject jsonSend = new JSONObject();
                jsonSend.put("Common", Common);
                jsonSend.put("Stock", Stock);
                jsonSend.put("Order", Order);
                jsonSend.put("Return", Return);
                jsonSend.put("Collection", Collection);
                jsonSend.put("Competitor", StockCompetitor);
                jsonSend.put("GroupStock", GroupStock);
                jsonSend.put("DiscountPromo", DiscountPromo);
                jsonSend.put("DiscountResult", DiscountResult);
                jsonSend.put("Asset", Asset);
                jsonSend.put("Newasset", Newasset);
                jsonSend.put("Target", Target);
                jsonSend.put("PriceMon", PriceMon);
                jsonSend.put("Prinsiple", Prinsiple);
                jsonSend.put("VisitNote", VisitNote);

                return jsonSend;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject convertJSONCustomer(Context context, CustomerModel customerModel) {
        try {
            CtrlCustomer ctrlCustomer = new CtrlCustomer(context);
            if (customerModel == null) {
                customerModel = ctrlCustomer.getGagalKirimEdit();
            }
            if (customerModel != null) {
                JSONObject json = new JSONObject();
                json.put("CustomerId", Integer.parseInt(customerModel.CustId));
                json.put("CustomerName", customerModel.CustomerName);
                json.put("CustomerAddress", customerModel.CustomerAddress);
                json.put("CustomerEmail", customerModel.CustomerEmail);
                json.put("CustomerPhone", customerModel.CustomerPhone);
                json.put("CustomerFax", customerModel.CustomerFax);
                json.put("CustomerNpwp", customerModel.CustomerNPWP);
                json.put("OwnerName", customerModel.OwnerName);
                json.put("OwnerAddress", customerModel.OwnerAddress);
                json.put("OwnerEmail", customerModel.OwnerEmail);
                json.put("OwnerPhone", customerModel.OwnerPhone);
                json.put("OwnerBirthPlace", customerModel.OwnerBirthPlace);
                json.put("OwnerBirthDate", customerModel.OwnerBirthDate);
                json.put("OwnerIdNo", customerModel.OwnerIdNo);
                json.put("CustomerTypeId", Integer.parseInt(customerModel.CustomerTypeId));
                json.put("GeoLat", Double.parseDouble(customerModel.GeoLat.equals("") ? "0" : customerModel.GeoLat));
                json.put("GeoLong", Double.parseDouble(customerModel.GeoLong.equals("") ? "0" : customerModel.GeoLong));
                json.put("StatusId", customerModel.isNew.equals("1") ? 1 : 2);
                json.put("SalesId", Integer.parseInt(UserModel.getInstance(context).SalesId));
                json.put("Picture", "xxx.jpg");

                json.put("OwnerReligion", customerModel.OwnerReligion);
                json.put("OwnerHobby", customerModel.OwnerHobby);
                json.put("Description", customerModel.Description);
                json.put("CustomerLevel", customerModel.CustomerLevel);
                json.put("CustomerUpLineID", customerModel.CustomerUpLineID);
                json.put("CustomerLegal", customerModel.CustomerLegal);
                json.put("RandomID", customerModel.RandomID);

                return json;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static JSONObject convertJSONCustPhoto(Context context, CustPhotoModel custPhotoModel) {
        try {
            CtrlCustPhoto ctrlCustPhoto = new CtrlCustPhoto(context);
            if (custPhotoModel == null) {
                custPhotoModel = ctrlCustPhoto.getPending();
            }
            if (custPhotoModel != null) {
                JSONObject json = new JSONObject();
                json.put("SalesID", UserModel.getInstance(context).SalesId);
                json.put("CustomerID", custPhotoModel.CustomerID);
                json.put("PhotoTypeID", custPhotoModel.PhotoTypeID);
                json.put("PhotoPic", Base64.encodeBytes(Util.readFile(LOKASI_IMAGE + custPhotoModel.PhotoPic)));
                json.put("RandomID", custPhotoModel.RandomID);

                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject convertJSONCustBranding(Context context, CustomerBrandingModel custBrandingModel) {
        try {
            CtrlCustBranding ctrlCustBranding = new CtrlCustBranding(context);
            /*if (custBrandingModel == null) {
                custBrandingModel = ctrlCustBranding.getPending();
            }*/

            if (custBrandingModel != null) {
                JSONObject json = new JSONObject();
                json.put("SalesID", custBrandingModel.SalesID);
                json.put("CustomerID", custBrandingModel.CustomerID);
                json.put("BrandingID", custBrandingModel.BrandingID);
                json.put("PhotoPic", Base64.encodeBytes(Util.readFile(LOKASI_IMAGE + custBrandingModel.PhotoPic)));
                json.put("GeoLong", custBrandingModel.GeoLong);
                json.put("GeoLat", custBrandingModel.GeoLat);
                json.put("Description", custBrandingModel.Description);

                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject convertJSONLogGPS(Context context, LogGpsModel model) {
        try {
            CtrlLogGps ctrlLogGps = new CtrlLogGps(context);
            if (model == null) {
                model = ctrlLogGps.get();
            }
            if (model != null) {
                JSONObject json = new JSONObject();
                json.put("SalesID", Integer.parseInt(model.SalesID));
                json.put("IMEI", model.IMEI);
                json.put("LogTime", model.LogTime);
                json.put("SC", model.SC);
                json.put("GeoLong", model.GeoLong);
                json.put("GeoLat", model.GeoLat);
                json.put("statusLog", model.statusLog);

                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject convertJSONLogSurvey(Context context, CommonSurveyModel model) {
        try {
            CtrlSurvey ctrlSurvey = new CtrlSurvey(context);
            if (model == null) {
                model = ctrlSurvey.getCommonSurvey();
            }
            if (model != null) {
                JSONObject json = new JSONObject();
                JSONObject Common = new JSONObject();
                Common.put("CustomerId", Integer.parseInt(model.CustomerId));
                Common.put("SalesId", Integer.parseInt(model.SalesId));
                Common.put("GeoLat", Double.parseDouble(model.GeoLat));
                Common.put("GeoLong", Double.parseDouble(model.GeoLong));
                Common.put("BeginSurveyTime", model.BeginSurveyTime);
                Common.put("EndSurveyTime", model.EndSurveyTime);
                Common.put("FormId", model.FormId);
                JSONArray Survey = new JSONArray();
                List<HasilSurveyModel> list = ctrlSurvey.listHasilSurvey(model.CommonId);
                for (HasilSurveyModel hasilSurveyModel : list) {
                    JSONObject jsonSurvey = new JSONObject();
                    jsonSurvey.put("FormId", Integer.parseInt(hasilSurveyModel.FormId));
                    jsonSurvey.put("SurveyId", Integer.parseInt(hasilSurveyModel.SurveyId));
                    jsonSurvey.put("ListID", Integer.parseInt(hasilSurveyModel.ListID));
                    jsonSurvey.put("ListValue", hasilSurveyModel.ListValue);
                    Survey.put(jsonSurvey);
                }
                json.put("Common", Common);
                json.put("Survey", Survey);

                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void printLog(String log) {
        android.util.Log.e("printLog", "" + log);
    }

    public interface PrintListener {
        public void printFinish();

        public void errorPrint(String message);

    }

    public static void existAndCreateTable(String table, SQLiteDatabase db, String sqlCreate) {
        try {
            Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE name=?", new String[]{table});
            if (!cursor.moveToNext()) {
                db.execSQL(sqlCreate);
            }
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void existAndCreateKolom(String table, String kolom, SQLiteDatabase db, String tipeData) {
        try {
            Cursor cursor = db.rawQuery("PRAGMA table_info( " + table + " )", null);
            boolean flag = false;
            while (cursor.moveToNext()) {
                if (cursor.getString(1).toLowerCase().equals(kolom.trim().toLowerCase())) {
                    flag = true;
                    break;
                }
            }
            cursor.close();
            if (!flag) {
                db.execSQL("ALTER TABLE " + table + " ADD COLUMN " + kolom + " " + tipeData);
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String sendUlangSalesTarget(Context context) {
        try {
            DatabaseHelper db = new DatabaseHelper(context);
            JSONArray json = db.getJSONArray("select * from Salestarget");
            if (json.length() == 0) {
                return null;
            }
            String result = ConnectionServer.requestJSONObjectNonThread(json, Util.getServerUrl(context) + "targetdata", true);
            android.util.Log.e("result", "" + result);
            if (result != null) {
                JSONObject jsonRespon = new JSONObject(result);
                if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                    SQLiteDatabase dts = db.getWritableDatabase();
                    dts.execSQL("delete from Salestarget");
                    dts.close();
                    return null;
                } else {
                    return jsonRespon.getString("Message");
                }
            }
            return "gagal terkirim, data akan tersimpan dilokal!";
        } catch (Exception e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
    }

    public static Drawable getDrawableFromStream(String address) {
        try {
            URL url = new URL(address);
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            return d;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void updateListViewHeight(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            return;
        }
        //get listview height
        int totalHeight = 0;
        int adapterCount = myListAdapter.getCount();
        for (int size = 0; size < adapterCount; size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
        myListView.setLayoutParams(params);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
