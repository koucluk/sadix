package com.bhn.sadix.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.printer.ZQPrinter;

import java.util.Set;

public class BluePrint {
    private Context context;
    public ZQPrinter PrinterService = null;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mmDevice;

    public BluePrint(Context context) {
        this.context = context;
    }

    private void informasi(String informasi) {
        Toast.makeText(context, informasi, Toast.LENGTH_SHORT).show();
    }

    private void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                informasi("No bluetooth adapter available");
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                context.startActivity(enableBluetooth);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals("AB-321M")) {
                        mmDevice = device;
                        informasi("Printer Device Found");
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            informasi("Printer Device Not Found");
        } catch (Exception e) {
            informasi("Printer Device Not Found");
        }
    }

    private void openBT() {
        try {
            PrinterService = new ZQPrinter();
            int nRet = PrinterService.Connect(mmDevice.getAddress());
            if (nRet == 0) {
                informasi("Printer Opened");
            } else {
                informasi("Printer Not Opened");
            }
        } catch (NullPointerException e) {
            informasi("Printer Not Opened");
        } catch (Exception e) {
            informasi("Printer Not Opened");
        }
    }

    public void open() {
        findBT();
        openBT();
    }

    public void close() {
        try {
            if (PrinterService != null) {
                PrinterService.Disconnect();
                PrinterService = null;
            }
            informasi("Printer Closed");
        } catch (NullPointerException e) {
            informasi("Printer Note Closed");
        } catch (Exception e) {
            informasi("Printer Note Closed");
        }
    }

}
