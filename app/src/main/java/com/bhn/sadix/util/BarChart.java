package com.bhn.sadix.util;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Color;

public class BarChart {
	private GraphicalView mChartView;
	private XYMultipleSeriesDataset dataset;
	private XYSeriesRenderer renderer;
	private XYSeries series;
	private XYMultipleSeriesRenderer multiRenderer;
	public BarChart(Context context, String title, String titleX, String titleY, String tSeries) {
		renderer = new XYSeriesRenderer();
//		renderer.setColor(Color.rgb(130, 130, 230));
		renderer.setFillPoints(true);
		renderer.setLineWidth(2);
		renderer.setDisplayChartValues(true);
		
		multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setBarSpacing(0.1);
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle(title);
        multiRenderer.setXTitle(titleX);
        multiRenderer.setYTitle(titleY);
        multiRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
        multiRenderer.setZoomButtonsVisible(true);
	    dataset = new XYMultipleSeriesDataset();
	    series = new XYSeries(tSeries);
		multiRenderer.addSeriesRenderer(renderer);
		dataset.addSeries(series);
	    mChartView = ChartFactory.getBarChartView(context, dataset, multiRenderer, Type.DEFAULT);
	}
	public void addItem(String category, int index, double value) {
		series.add(index, value);
		multiRenderer.addXTextLabel(index, category);
	}
	public void repaint() {
		mChartView.repaint();
	}
	public GraphicalView getView() {
		return mChartView;
	}

}
