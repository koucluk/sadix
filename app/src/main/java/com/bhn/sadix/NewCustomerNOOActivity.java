package com.bhn.sadix;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.fragment.customer.BaseFragment;
import com.bhn.sadix.fragment.customer.FragmentAdapter;
import com.bhn.sadix.fragment.customer.FragmentCustomer;
import com.bhn.sadix.fragment.customer.FragmnetAddAddress;
import com.bhn.sadix.fragment.customer.FragmnetAddPIC;
import com.bhn.sadix.model.CustAdddress;
import com.bhn.sadix.model.CustPIC;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.CustomerListener;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class NewCustomerNOOActivity extends AppCompatActivity implements CustomerListener {
    private FragmentAdapter adapter;
    private FragmentCustomer fragmentCustomer;
    private FragmnetAddAddress fragmnetAddAddress;
    private FragmnetAddPIC fragmnetAddPIC;
    private ViewPager pager;
    private LocationManager locationManager;
    public Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            fragmentCustomer.setLcation(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    
    private CustomerModel customerModel = new CustomerModel();
    private CtrlAppModul ctrlAppModul;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctrlAppModul = new CtrlAppModul(this);
        setContentView(R.layout.activity_new_customer_noo);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        fragmentCustomer = new FragmentCustomer();
        fragmentCustomer.setCustomerModel(customerModel);
        fragmnetAddAddress = new FragmnetAddAddress();
//        fragmnetAddAddress.setCustomerModel(customerModel);
        fragmnetAddPIC = new FragmnetAddPIC();
        fragmnetAddPIC.setCustomerModel(customerModel);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new FragmentAdapter(getSupportFragmentManager(), new ArrayList<BaseFragment>());
        adapter.add(fragmentCustomer);
        if (ctrlAppModul.isModul("67")) adapter.add(fragmnetAddAddress);
        if (ctrlAppModul.isModul("66")) adapter.add(fragmnetAddPIC);
        pager.setAdapter(adapter);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public JSONArray getAddress() {
        if (fragmnetAddAddress.getAdapter() != null) {
            return fragmnetAddAddress.getAdapter().toJsonArray(location);
        } else {
            return new JSONArray();
        }
    }

    public List<CustAdddress> getAddressByList() {
        if (fragmnetAddAddress.getAdapter() != null) {
            return fragmnetAddAddress.getAdapter().getList();
        } else {
            return new ArrayList<CustAdddress>(0);
        }
    }

    public JSONArray getPic() {
        if (fragmnetAddPIC.getAdapter() != null) {
            return fragmnetAddPIC.getAdapter().toJsonArray();
        } else {
            return new JSONArray();
        }
    }

    public List<CustPIC> getPicByList() {
        if (fragmnetAddPIC.getAdapter() != null) {
            return fragmnetAddPIC.getAdapter().getList();
        } else {
            return new ArrayList<CustPIC>(0);
        }
    }

    @Override
    public CustomerModel getCustomer() {
        return customerModel;
    }

    @Override
    public CustomerTypeModel getCustomerTypeModel() {
        return null;
    }
}
