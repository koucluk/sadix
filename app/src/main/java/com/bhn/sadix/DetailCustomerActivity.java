package com.bhn.sadix;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.MenuUtamaAdapter;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.MenuUtamaModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;

public class DetailCustomerActivity extends AppCompatActivity implements OnItemClickListener {
    public CommonModel commonModel = new CommonModel();
    private MenuUtamaAdapter adapter;
    private CustomerModel customerModel;
    private CtrlConfig ctrlConfig;
    private CtrlCustomerType ctrlCustomerType;
    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            commonModel.GeoLat = String.format("%1$s", location.getLatitude());
            commonModel.GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private ProgressDialog progressDialog;
    private ConnectionServer server;
    private CtrlAppModul ctrlAppModul;

    private CustomerTypeModel customerTypeModel;

    private Toolbar toolbar;
    private CtrlCustomer ctrlCustomer;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_customer);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ctrlConfig = new CtrlConfig(this);
        ctrlCustomerType = new CtrlCustomerType(this);

        ctrlCustomer = new CtrlCustomer(this);

        Realm realmUI = Realm.getDefaultInstance();

        com.bhn.sadix.Data.CommonModel model = realmUI.where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        if (model != null) {
            startActivity(new Intent(this, MenuUtamaActivity.class));
            Log.d("Common ID", model.CommonID);
            Intent intent = new Intent(this, KunjunganActivity.class);
            CustomerModel customerModel = ctrlCustomer.get(model.CustomerId);
            Log.d("Ini last CID ", customerModel.CustId);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", ctrlCustomerType.get(customerModel.CustomerTypeId));
            intent.putExtra("isLoad", true);
            startActivity(intent);

            finish();
        }

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = ctrlCustomerType.get(customerModel.CustomerTypeId);
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }


        ctrlAppModul = new CtrlAppModul(this);
        adapter = new MenuUtamaAdapter(this, new ArrayList<MenuUtamaModel>());
        adapter.setNotifyOnChange(true);
        if (ctrlAppModul.isModul("9"))
            adapter.add(new MenuUtamaModel(1, "Check In/Visit", getResources().getDrawable(R.drawable.visit)));
        if (ctrlAppModul.isModul("10"))
            adapter.add(new MenuUtamaModel(2, "Not Visit", getResources().getDrawable(R.drawable.not_visit)));
        if (ctrlAppModul.isModul("11"))
            adapter.add(new MenuUtamaModel(3, "Order History", getResources().getDrawable(R.drawable.order_history)));
        if (ctrlAppModul.isModul("12"))
            adapter.add(new MenuUtamaModel(4, "Location", getResources().getDrawable(R.drawable.location)));
        if (ctrlAppModul.isModul("13"))
            adapter.add(new MenuUtamaModel(5, "Show Photo", getResources().getDrawable(R.drawable.show_photo)));
        if (ctrlAppModul.isModul("41"))
            adapter.add(new MenuUtamaModel(6, "Market Share", getResources().getDrawable(R.drawable.show_photo)));
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);

        commonModel.SC = UserModel.getInstance(this).CompanyID;
        commonModel.BeginVisitTime = Util.dateFormat.format(new Date());
        commonModel.SalesId = UserModel.getInstance(this).SalesId;
        commonModel.CustomerId = customerModel.CustId;
        commonModel.D = new SimpleDateFormat("EEEE").format(new Date());
        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        server = new ConnectionServer(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (item.getItemId() == android.R.id.home) {
            finish();
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        MenuUtamaModel model = adapter.getItem(position);
        if (model.id == 2) {
            if (getIntent().getStringExtra("MSG") == null) {
                notVisitDialog();
            } else {
                Util.showDialogInfo(this, getIntent().getStringExtra("MSG"));
            }
        } else if (model.id == 1) {
            if (getIntent().getStringExtra("MSG") == null) {
                showDialogVisit();
                /*Intent intent = new Intent(this, VisitCustomerActivity.class);
                intent.putExtra("CustomerModel", customerModel);
                intent.putExtra("CustomerTypeModel", customerTypeModel);
                startActivity(intent);*/
//                finish();
            } else {
                Util.showDialogInfo(this, getIntent().getStringExtra("MSG"));
            }
        } else if (model.id == 4) {
            if (location != null) {
//				Intent intent = new Intent(this, CustomerLocationActivity.class);
//				intent.putExtra("CustomerModel", customerModel);
//				startActivity(intent);

                String uri = "http://maps.google.com/maps?saddr=" + String.format("%1$s", location.getLatitude()) +
                        "," + String.format("%1$s", location.getLongitude()) + "&daddr=" + customerModel.GeoLat +
                        "," + customerModel.GeoLong + "&mode=driving";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
//                finish();
            } else {
                Util.showDialogInfo(this, "Lokasi anda belum terdeteksi GPS!");
            }
        } else if (model.id == 3) {
            Intent intent = new Intent(this, OrderHistoryActivity.class);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", customerTypeModel);
            startActivity(intent);
//            finish();
        } else if (model.id == 5) {
            Intent intent = new Intent(this, ShowPhotoActivity.class);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", customerTypeModel);
            startActivity(intent);
//            finish();
        } else if (model.id == 6) {
            Intent intent = new Intent(this, SalesTargetActivity.class);
            intent.putExtra("CustomerModel", customerModel);
            intent.putExtra("CustomerTypeModel", customerTypeModel);
            startActivity(intent);
//            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String kode = intent.getStringExtra("SCAN_RESULT");
//		        String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                String[] pecahKode = kode.split("[|]");
                if (pecahKode.length == 3) {
                    if (pecahKode[0].equalsIgnoreCase(UserModel.getInstance(this).CompanyID) && pecahKode[2].equalsIgnoreCase(customerModel.CustId)) {
                        Intent kunjungan = new Intent(this, KunjunganActivity.class);
                        kunjungan.putExtra("CustomerModel", customerModel);
                        kunjungan.putExtra("CustomerTypeModel", customerTypeModel);
                        startActivity(kunjungan);
                        finish();
                    } else {
                        Util.Toast(this, "Barcode tidak sama");
                    }
                }
            }
        }
    }

    private void scanBarcode() {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            Util.showDialogInfo(this, "ZXing Scanner Not Found, Please Install First!");
        }
    }

    private void showDialogVisit() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_visit_customer);
        dialog.setTitle("Visiting Customer");

        final int[] statusMode = {0};

        final Spinner spinnerNoScan = ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode));
        spinnerNoScan.setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_BC_NOSCAN")));

        RadioGroup rgScan = (RadioGroup) dialog.findViewById(R.id.rg_scan);

        if (ctrlAppModul.isModul("14"))
            dialog.findViewById(R.id.rb_scan).setVisibility(View.VISIBLE);
        else
            dialog.findViewById(R.id.rb_scan).setVisibility(View.GONE);

        if (ctrlAppModul.isModul("15"))
            dialog.findViewById(R.id.rb_bypass).setVisibility(View.VISIBLE);
        else
            dialog.findViewById(R.id.rb_bypass).setVisibility(View.GONE);

        rgScan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                if (i == R.id.rb_bypass) {
                    statusMode[0] = 0;
                    spinnerNoScan.setVisibility(View.VISIBLE);
                } else if (i == R.id.rb_scan) {
                    statusMode[0] = 1;
                    spinnerNoScan.setVisibility(View.GONE);
                }
            }
        });

        if (dialog.findViewById(R.id.rb_scan).isShown())
            rgScan.check(R.id.rb_scan);
        else
            rgScan.check(R.id.rb_bypass);

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (statusMode[0] == 0) {
                    Intent intent = new Intent(DetailCustomerActivity.this, KunjunganActivity.class);
                    intent.putExtra("ComboBoxModel", (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem());
                    intent.putExtra("CustomerTypeModel", customerTypeModel);
                    intent.putExtra("CustomerModel", customerModel);
                    startActivity(intent);
                    dialog.dismiss();
                    finish();
                } else {
                    scanBarcode();
                }
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void notVisitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_not_visit_reason);
        dialog.setTitle("Not Visit Reason");
        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_NO_VISIT")));
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//				finish();
                sendNotVisit((ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem());
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void sendNotVisit(final ComboBoxModel comboBoxModel) {
        new Checkout(comboBoxModel).execute();
    }

    class Checkout extends AsyncTask<Void, Void, String> {
        private ComboBoxModel comboBoxModel;

        public Checkout(ComboBoxModel comboBoxModel) {
            this.comboBoxModel = comboBoxModel;
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            String tanggal = Util.dateFormat.format(new Date());
            try {
                commonModel.EndVisitTime = tanggal;
                commonModel.BeginVisitTime = tanggal;
                commonModel.GeoLat = location != null ? String.format("%1$s", location.getLatitude()) : "0";
                commonModel.GeoLong = location != null ? String.format("%1$s", location.getLongitude()) : "0";
                commonModel.LogTime = Util.dateFormat.format(location != null ? new Date(location.getTime()) : new Date());

                JSONObject Common = new JSONObject();
                Common.put("SC", commonModel.SC);
                Common.put("GeoLat", Double.parseDouble(commonModel.GeoLat.equals("") ? "0" : commonModel.GeoLat));
                Common.put("CustomerId", commonModel.CustomerId);
                Common.put("BeginVisitTime", commonModel.BeginVisitTime);
                Common.put("GeoLong", Double.parseDouble(commonModel.GeoLong.equals("") ? "0" : commonModel.GeoLong));
                Common.put("LogTime", commonModel.LogTime);
                Common.put("EndVisitTime", commonModel.EndVisitTime);
                Common.put("SalesId", Integer.parseInt(commonModel.SalesId));
                Common.put("S_AMOUNT", 0);
                Common.put("D_VALUE", 0);
                Common.put("G_AMOUNT", 0);
                Common.put("Visit", 0);
                Common.put("RVisitID", Integer.parseInt(comboBoxModel.value));
                Common.put("InvNO", "");
                Common.put("Description", "");

                JSONArray Stock = new JSONArray();
                JSONArray StockCompetitor = new JSONArray();
                JSONArray Order = new JSONArray();
                JSONArray Return = new JSONArray();
                JSONArray Collection = new JSONArray();

                JSONObject jsonSend = new JSONObject();
                jsonSend.put("Common", Common);
                jsonSend.put("Stock", Stock);
                jsonSend.put("Order", Order);
                jsonSend.put("Return", Return);
                jsonSend.put("Collection", Collection);
                jsonSend.put("StockCompetitor", StockCompetitor);

                server.setUrl(Util.getServerUrl(DetailCustomerActivity.this) + "endvisit");
                result = server.requestJSONObjectNonThread(jsonSend);
                if (result != null) {
                    JSONObject jsonRespon = new JSONObject(result);
                    if (jsonRespon.has("Result") && jsonRespon.getString("Result").equals("INSERT_OK")) {
                        result = jsonRespon.getString("Message");
                    } else {
                        result = jsonRespon.getString("Message");
                    }
                }
            } catch (Exception e) {
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            finish();
            super.onPostExecute(result);
        }
    }

}
