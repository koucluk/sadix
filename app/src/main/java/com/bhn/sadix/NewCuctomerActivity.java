package com.bhn.sadix;


import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.ScalingUtilities;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.ItemPhotoCustomerListener;
import com.bhn.sadix.widget.ItemPhotoCustomerWidget;

public class NewCuctomerActivity extends AppCompatActivity implements ConnectionEvent, ItemPhotoCustomerListener {
    private ComboBoxAdapter adapter;
    private CtrlCustomerType ctrlCustomerType;
    private CtrlCustomer ctrlCustomer;
    private ConnectionServer server;

    NewCuctomerActivity activity;

    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            ((TextView) findViewById(R.id.Lokasi)).setText("Longitude / Latitude : " + String.format("%1$s", location.getLongitude()) +
                    " / " + String.format("%1$s", location.getLatitude()));
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
//        	if(ctrlAppModul.isModul("50") == false) {
//        		Util.Toast(NewCuctomerActivity.this, "Mengingatkan GPS tidak ditampilkan, namun tracking tetap jalan dengan sumber data A-GPS");
//        	}
        }
    };
    private CustomerModel customerModel = new CustomerModel();
    private CtrlAppModul ctrlAppModul;

    private ItemPhotoCustomerWidget widget;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
        activity = this;
        setContentView(R.layout.fragment_kunjungan_edit_customer);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ImageButton) findViewById(R.id.btn_add_photo)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addPhoto();
            }
        });

        ((ImageButton) findViewById(R.id.btn_save)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        ((ImageButton) findViewById(R.id.btn_tgl)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupBirthDate();
            }
        });

//        ((ScrollView) findViewById(R.id.scroll)).requestFocus(ScrollView.FOCUS_DOWN);

        ctrlCustomerType = new CtrlCustomerType(this);
        ctrlCustomer = new CtrlCustomer(this);
        ctrlAppModul = new CtrlAppModul(this);
        adapter = new ComboBoxAdapter(this, ctrlCustomerType.listComboBox());
        adapter.setNotifyOnChange(true);
        ((Spinner) findViewById(R.id.CustomerTypeId)).setAdapter(adapter);

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }

        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ctrlAppModul.isModul("50")) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == false) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        }
    }

    private void popupBirthDate() {
        try {
//			String tglLahir = ((EditText)findViewById(R.id.OwnerBirthDate)).getText().toString();
            final Calendar cal = Calendar.getInstance();
//			if(tglLahir.trim().equals("")) {
//				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
//			} else {
//				String[] arr = tglLahir.split("-");
//				cal.set(Integer.parseInt(arr[0]), Integer.parseInt(arr[1])-1, Integer.parseInt(arr[2]));
//			}
            final DatePickerDialog tglDialog = new DatePickerDialog(this, new OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    ((EditText) findViewById(R.id.OwnerBirthDate)).setText(Util.dateFormatBirthDate.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            tglDialog.show();
        } catch (Exception e) {
            Util.Toast(this, e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        if (location == null) {
            Util.showDialogInfo(this, "Lokasi tidak ditemukan!");
            return;
        }
        if (isValid() == false) {
            return;
        }
        LinearLayout panel_photo = ((LinearLayout) findViewById(R.id.panel_photo));
        if (panel_photo.getChildCount() == 0) {
            Util.showDialogInfo(this, "Photo wajib diisi!");
            return;
        }
        for (int i = 0; i < panel_photo.getChildCount(); i++) {
            View view = panel_photo.getChildAt(i);
            if (view instanceof ItemPhotoCustomerWidget) {
                ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                if (item.isAmbilPhoto() == false) {
                    Util.showDialogInfo(this, "Photo belum diambil!");
                    return;
                }
            }
        }
        try {
            customerModel.CustId = "0";
            customerModel.CustomerAddress = ((EditText) findViewById(R.id.CustomerAddress)).getText().toString();
            customerModel.CustomerCrBalance = 0;
            customerModel.CustomerCrLimit = 0;
            customerModel.CustomerEmail = ((EditText) findViewById(R.id.CustomerEmail)).getText().toString();
            customerModel.CustomerFax = ((EditText) findViewById(R.id.CustomerFax)).getText().toString();
            customerModel.CustomerName = ((EditText) findViewById(R.id.CustomerName)).getText().toString();
            customerModel.CustomerNPWP = ((EditText) findViewById(R.id.CustomerNPWP)).getText().toString();
            customerModel.CustomerPhone = ((EditText) findViewById(R.id.CustomerPhone)).getText().toString();
            customerModel.CustomerTypeId = ((ComboBoxModel) ((Spinner) findViewById(R.id.CustomerTypeId)).getSelectedItem()).value;
            customerModel.GeoLat = location == null ? "" : String.format("%1$s", location.getLatitude());
            customerModel.GeoLong = location == null ? "" : String.format("%1$s", location.getLongitude());
            customerModel.isNew = "1";
            customerModel.isEdit = "1";
            customerModel.isNOO = "1";
            customerModel.OwnerAddress = ((EditText) findViewById(R.id.OwnerAddress)).getText().toString();
            customerModel.OwnerEmail = ((EditText) findViewById(R.id.OwnerEmail)).getText().toString();
            customerModel.OwnerName = ((EditText) findViewById(R.id.OwnerName)).getText().toString();
            customerModel.OwnerPhone = ((EditText) findViewById(R.id.OwnerPhone)).getText().toString();
            customerModel.OwnerBirthPlace = ((EditText) findViewById(R.id.OwnerBirthPlace)).getText().toString();
            customerModel.OwnerBirthDate = ((EditText) findViewById(R.id.OwnerBirthDate)).getText().toString();
            customerModel.OwnerIdNo = ((EditText) findViewById(R.id.OwnerIdNo)).getText().toString();
            customerModel.D1 = "false";
            customerModel.D2 = "false";
            customerModel.D3 = "false";
            customerModel.D4 = "false";
            customerModel.D5 = "false";
            customerModel.D6 = "false";
            customerModel.D7 = "false";

            JSONObject json = new JSONObject();
            json.put("CustomerId", Integer.parseInt(customerModel.CustId));
            json.put("CustomerName", customerModel.CustomerName);
            json.put("CustomerAddress", customerModel.CustomerAddress);
            json.put("CustomerEmail", customerModel.CustomerEmail);
            json.put("CustomerPhone", customerModel.CustomerPhone);
            json.put("CustomerFax", customerModel.CustomerFax);
            json.put("CustomerNpwp", customerModel.CustomerNPWP);
            json.put("OwnerName", customerModel.OwnerName);
            json.put("OwnerAddress", customerModel.OwnerAddress);
            json.put("OwnerEmail", customerModel.OwnerEmail);
            json.put("OwnerPhone", customerModel.OwnerPhone);
            json.put("OwnerBirthPlace", customerModel.OwnerBirthPlace);
            json.put("OwnerBirthDate", customerModel.OwnerBirthDate);
            json.put("OwnerIdNo", customerModel.OwnerIdNo);
            json.put("CustomerTypeId", Integer.parseInt(customerModel.CustomerTypeId));
            json.put("GeoLat", Double.parseDouble(customerModel.GeoLat.equals("") ? "0" : customerModel.GeoLat));
            json.put("GeoLong", Double.parseDouble(customerModel.GeoLong.equals("") ? "0" : customerModel.GeoLong));
            json.put("StatusId", 1);
            json.put("SalesId", Integer.parseInt(UserModel.getInstance(this).SalesId));
            json.put("Picture", "xxx.jpg");

            server.setUrl(Util.getServerUrl(this) + "postcustdata");
            server.requestJSONObject(json);
        } catch (Exception e) {
//			error(e.getMessage());
            Util.showDialogInfo(this, e.getMessage());
        }
    }

    private void addPhoto() {
        ItemPhotoCustomerWidget widget = new ItemPhotoCustomerWidget(this);
        widget.addItemPhotoCustomerListener(this);
        widget.setCustomerModel(customerModel);
        ((LinearLayout) findViewById(R.id.panel_photo)).addView(widget);

        widget.takePicture();
    }

    @Override
    public void error(String error) {
        saveLokal();
//		Util.showDialogError(this, error);
        Util.okeDialog(this, "Info", error, new OkListener() {
            @Override
            public void onDialogOk() {
                finish();
            }
        });
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                if (json.has("ServerID") && json.getInt("ServerID") > 0) {
                    customerModel.CustId = json.getString("ServerID");
                    customerModel.isNew = "0";
                }
                customerModel.isEdit = "0";
                ctrlCustomer.save(customerModel);
                Util.Toast(this, json.getString("Message"));
//				finish();
                new KirimGambar(this, false).execute();
            } else {
                Util.showDialogInfo(this, json.getString("Message"));
            }
        } catch (Exception e) {
//			error(e.getMessage());
            Util.showDialogInfo(this, e.getMessage());
        }
    }

    private void saveLokal() {
        ctrlCustomer.save(customerModel);
        LinearLayout layout = (LinearLayout) findViewById(R.id.panel_photo);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View view = layout.getChildAt(i);
            if (view instanceof ItemPhotoCustomerWidget) {
                ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                item.saveKeLokal();
            }
        }
        Util.Toast(this, "Data disimpan dilokal");
    }

    @Override
    public void takePicture(ItemPhotoCustomerWidget widget) {
        this.widget = widget;
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    @Override
    public void send(ItemPhotoCustomerWidget widget) {
        ((LinearLayout) findViewById(R.id.panel_photo)).removeView(widget);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
//		super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
//					bitmap = Bitmap.createScaledBitmap(bitmap, Util.Width, Util.Height, false);
//					bitmap = ScalingUtilities.createScaledBitmap(bitmap, Util.Width, Util.Height, ScalingUtilities.ScalingLogic.FIT);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap, ctrlAppModul.getInt("59"), ctrlAppModul.getInt("60"), ScalingUtilities.ScalingLogic.FIT);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    widget.setImage(bitmap);
                    widget.setData(bao.toByteArray());
                    bao.flush();
                    bao.close();
                    bao = null;
                } catch (Exception e) {
                    Util.showDialogError(this, e.getMessage());
                }
            }
        }
    }

    @Override
    public void delete(final ItemPhotoCustomerWidget widget) {
        Util.confirmDialog(this, "Info", "Apakah photo ingin dihapus?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    ((LinearLayout) findViewById(R.id.panel_photo)).removeView(widget);
                }
            }
        });
    }

    class KirimGambar extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;
        private boolean isSave;

        public KirimGambar(Context context, boolean isSave) {
            this.isSave = isSave;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Kirim gambar...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LinearLayout layout = (LinearLayout) findViewById(R.id.panel_photo);
                    for (int i = 0; i < layout.getChildCount(); i++) {
                        View view = layout.getChildAt(i);
                        if (view instanceof ItemPhotoCustomerWidget) {
                            ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                            if (isSave) {
                                item.saveKeLokal();
                            } else {
                                String respon = ConnectionServer.requestJSONObjectNonThread(item.getJsonSendData(), Util.getServerUrl(NewCuctomerActivity.this) + "photodata", false);
                                try {
                                    JSONObject json = new JSONObject(respon);
                                    if ((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
                                        item.saveKeLokal();
                                    }
                                } catch (Exception e) {
                                }
                            }
                        }
                    }
                }
            });
            /*LinearLayout layout = (LinearLayout) findViewById(R.id.panel_photo);
            for (int i = 0; i < layout.getChildCount(); i++) {
                View view = layout.getChildAt(i);
                if (view instanceof ItemPhotoCustomerWidget) {
                    ItemPhotoCustomerWidget item = (ItemPhotoCustomerWidget) view;
                    if (isSave) {
                        item.saveKeLokal();
                    } else {
                        String respon = ConnectionServer.requestJSONObjectNonThread(item.getJsonSendData(), Util.getServerUrl(NewCuctomerActivity.this) + "photodata", false);
                        try {
                            JSONObject json = new JSONObject(respon);
                            if ((json.has("Result") && json.getString("Result").equals("INSERT_OK")) == false) {
                                item.saveKeLokal();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(Void respon) {
            progressDialog.dismiss();
            finish();
        }
    }

    private boolean isValid() {
        if (location == null) {
            Util.showDialogInfo(this, "Lokasi tidak ditemukan!");
            return false;
        }
        String valid = ((EditText) findViewById(R.id.CustomerName)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Customer Name harus diisi!");
            return false;
        }
        valid = ((EditText) findViewById(R.id.CustomerAddress)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Customer Address harus diisi!");
            return false;
        }
        valid = ((EditText) findViewById(R.id.CustomerPhone)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Customer Phone harus diisi!");
            return false;
        }
        valid = ((EditText) findViewById(R.id.OwnerName)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Owner Name harus diisi!");
            return false;
        }
        valid = ((EditText) findViewById(R.id.OwnerAddress)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Owner Address harus diisi!");
            return false;
        }
        valid = ((EditText) findViewById(R.id.OwnerPhone)).getText().toString();
        if (valid.trim().equals("")) {
            Util.showDialogInfo(this, "Owner Phone harus diisi!");
            return false;
        }
        return true;
    }

}
