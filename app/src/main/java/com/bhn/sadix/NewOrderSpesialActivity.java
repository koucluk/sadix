package com.bhn.sadix;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.adapter.AssetAdapter;
import com.bhn.sadix.adapter.AssetMutasiAdapter;
import com.bhn.sadix.adapter.AssetServiceAdapter;
import com.bhn.sadix.adapter.CollectionPayAdapter;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.CrcOrderAdapter;
import com.bhn.sadix.adapter.CrcReportAdapter;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.PriceMonitoringAdapter;
import com.bhn.sadix.adapter.PrinsipleAdapter;
import com.bhn.sadix.adapter.ReturDetailAdapter;
import com.bhn.sadix.adapter.StokCompetitorAdapter;
import com.bhn.sadix.adapter.StokCustomerAdapter;
import com.bhn.sadix.adapter.StokGroupAdapter;
import com.bhn.sadix.adapter.StokOnCarAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlDiscountPromo;
import com.bhn.sadix.database.CtrlDiscountResult;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.kunjungan.TakingOrderFragment;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class NewOrderSpesialActivity extends AppCompatActivity implements KunjunganListener {

    //    private CommonModel commonModel = new CommonModel();
    com.bhn.sadix.Data.CommonModel commonModel;
    private CustomerTypeModel customerTypeModel;
    private CustomerModel customerModel;

    private CtrlCustomerType ctrlCustomerType;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlCommon ctrlCommon;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;
    private DbMasterHelper dbmaster;
    private Location location;
    private Messenger mService;
    private Realm realmUI;
    final Messenger mMessenger = new Messenger(new LocationHandler());

    private boolean isBound = false;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = new Messenger(iBinder);
            try {
                Message msg = Message.obtain(null, SadixService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    @Override
    public CommonModel getCommonModel() {
        return null;
    }

    @Override
    public CustomerModel getCustomerModel() {
        return customerModel;
    }

    @Override
    public CustomerTypeModel getCustomerType() {
        return customerTypeModel;
    }

    @Override
    public OrderDetailAdapter getTakingAdapter() {
        return takingOrderAdapter;
    }

    @Override
    public OrderDetailAdapter getTakingPrincipleAdapter() {
        return null;
    }

    @Override
    public ReturDetailAdapter getReturAdapter() {
        return null;
    }

    @Override
    public StokCustomerAdapter getStokAdapter() {
        return null;
    }

    @Override
    public StokCompetitorAdapter getStokCompetitor() {
        return null;
    }

    @Override
    public StokGroupAdapter getStokGroupAdapter() {
        return null;
    }

    @Override
    public CollectionPayAdapter getCollectionAdapter() {
        return null;
    }

    @Override
    public PriceMonitoringAdapter getPriceMonitoringAdapter() {
        return null;
    }

    @Override
    public PriceMonitoringAdapter getPriceCompetitorAdapter() {
        return null;
    }

    @Override
    public PrincipleModel getPrinsipleModel() {
        return null;
    }

    @Override
    public StokOnCarAdapter getStokOnCarAdapter() {
        return null;
    }

    @Override
    public CrcReportAdapter getCrcReportAdapter() {
        return null;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void updateMenuFunction() {

    }

    @Override
    public void restoreMenuFunction() {

    }

    @Override
    public PrinsipleAdapter getPrinsipleAdapter() {
        return null;
    }

    @Override
    public void handleTO3(PrincipleModel model, int pos) {

    }
/*
    @Override
    public void handleTO3(PrinsipleModel model, int pos) {

    }*/

    @Override
    public void setPrincipleAdapter(PrinsipleAdapter adapter) {

    }

    @Override
    public CrcOrderAdapter getCrcOrderAdapter() {
        return null;
    }

    @Override
    public AssetAdapter getAssetAdapter() {
        return null;
    }

    @Override
    public AssetMutasiAdapter getMutasiAdapter() {
        return null;
    }

    @Override
    public AssetServiceAdapter getAssetServiceAdapter() {
        return null;
    }

    @Override
    public void dismissAndUpdateDialog(Dialog dialog) {

    }

    class LocationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SadixService.MSG_SEND_LOC:
                    Bundle bundle = msg.getData();

                    location = new Location("");
                    location.setLatitude(bundle.getDouble("lat"));
                    location.setLongitude(bundle.getDouble("long"));
                    location.setTime(bundle.getLong("time"));

                    Log.d("Loc NewOrder - lat", bundle.getDouble("lat") + "");
                    Log.d("Loc NewOrder - long", bundle.getDouble("long") + "");
                    Log.d("Loc NewOrder - time", bundle.getLong("time") + "");
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            commonModel.GeoLat = String.format("%1$s", location.getLatitude());
                            commonModel.GeoLong = String.format("%1$s", location.getLongitude());
                        }
                    });
                    break;
            }
        }
    }
    //    private LocationManager locationManager;
    /*private LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location loc) {
            location = loc;
            commonModel.GeoLat = String.format("%1$s", location.getLatitude());
            commonModel.GeoLong = String.format("%1$s", location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };*/


    private ProgressDialog progressDialog;
    private TakingOrderFragment fragment;
    private OrderDetailAdapter takingOrderAdapter;
    private CtrlDiscountPromo ctrlDiscountPromo;
    private CtrlDiscountResult ctrlDiscountResult;

    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_spesial);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                close();
            }
        });

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CheckIfServiceIsRunning();

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);

        ctrlCustomerType = new CtrlCustomerType(this);
        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlTakingOrder = new CtrlTakingOrder(this);
        ctrlCommon = new CtrlCommon(this);
        ctrlConfig = new CtrlConfig(this);
        ctrlAppModul = new CtrlAppModul(this);
        dbmaster = new DbMasterHelper(this);
        ctrlDiscountPromo = new CtrlDiscountPromo(this);
        ctrlDiscountResult = new CtrlDiscountResult(this);

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = ctrlCustomerType.get(customerModel.CustomerTypeId);
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + customerModel.CustomerCrLimit + " | Sisa Credit Limit : " + customerModel.CustomerCrBalance);
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }

        realmUI = Realm.getDefaultInstance();
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Log.d("Creating Common Model", "true");
                String CommonID = UUID.randomUUID().toString();

                commonModel = realmUI.createObject(com.bhn.sadix.Data.CommonModel.class, CommonID);

//                commonModel.CommonID = UUID.randomUUID().toString();
                commonModel.SC = UserModel.getInstance(NewOrderSpesialActivity.this).CompanyID;
                commonModel.D = "D" + String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
                commonModel.BeginVisitTime = Util.dateFormat.format(new Date());
                commonModel.SalesId = UserModel.getInstance(NewOrderSpesialActivity.this).SalesId;
                commonModel.CustomerId = customerModel.CustId;
                commonModel.Visit = 1;
                commonModel.RVisitID = 0;
                commonModel.StatusID = 1;
                commonModel.Done = 0;
            }
        });

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel = realmUI.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Done", 0).findFirst();
            }
        });

        /*CommonModel cmModel = (CommonModel) getIntent().getSerializableExtra("CommonModel");
        if (cmModel == null) {
            commonModel.SC = UserModel.getInstance(this).CompanyID;
            commonModel.D = "D" + String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
            commonModel.BeginVisitTime = Util.dateFormat.format(new Date());
            commonModel.SalesId = UserModel.getInstance(this).SalesId;
            commonModel.CustomerId = customerModel.CustId;
            commonModel.Visit = 1;
            commonModel.RVisitID = 0;
            commonModel.StatusID = 1;
        } else {
            commonModel = cmModel;
        }*/

        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });

        fragment = TakingOrderFragment.newInstance(false, false, 0);//new TakingOrderFragment(this, customerTypeModel, customerModel);
        /*fragment.setCustomerModel(customerModel);
        fragment.setCustomerTypeModel(customerTypeModel);*/
//        fragment.setCommonModel(commonModel);
//        fragment.setCanvas(false);

        new LoadData().execute();
    }

    private void CheckIfServiceIsRunning() {
        if (SadixService.isRunning()) {
            bindService(new Intent(this, SadixService.class), serviceConnection, Context.BIND_AUTO_CREATE);
            isBound = true;
        }
    }

    @Override
    public void onBackPressed() {
        close();
    }

    private void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Check out")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Check out");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
            chekOutOrder();
        }
        return super.onOptionsItemSelected(item);
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan kunjungan?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Done", 0).findFirst().deleteFromRealm();
                        }
                    });
                    finish();
                }
            }
        });
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                takingOrderAdapter = new OrderDetailAdapter(NewOrderSpesialActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
                List<GroupSKUModel> list = ctrlGroupSKU.list();
                for (GroupSKUModel groupSKUModel : list) {
                    takingOrderAdapter.addHeader(groupSKUModel);
                }
            } catch (Exception e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
//            fragment.setAdapter(takingOrderAdapter);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content_frame, fragment)
                    .addToBackStack(null)
                    .commit();

            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogInfo(NewOrderSpesialActivity.this, result);
            }
            super.onPostExecute(result);
        }
    }

    private class CheckoutBackground extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            final String tanggal = Util.dateFormat.format(new Date());
            try {
                Realm realmO = Realm.getDefaultInstance();
                realmO.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        com.bhn.sadix.Data.CommonModel cmModel = realm.where(com.bhn.sadix.Data.CommonModel.class)
                                .equalTo("Done", 0).findFirst();

                        cmModel.EndVisitTime = tanggal;
                        cmModel.GeoLat = location != null ? String.format("%1$s", location.getLatitude()) : "0";
                        cmModel.GeoLong = location != null ? String.format("%1$s", location.getLongitude()) : "0";
                        cmModel.LogTime = Util.dateFormat.format(location != null ? new Date(location.getTime()) : new Date());
                        cmModel.InvNO = "";
                        cmModel.EndVisitTime = tanggal;
                        cmModel.Done = 1;
                        cmModel.Status = "0";
                    }
                });
//				commonModel.Description = "";
//                saveLokal(tanggal);
                realmO.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Util.Toast(NewOrderSpesialActivity.this, "Data berhasil disimpan!");
            finish();
            super.onPostExecute(result);
        }
    }

    private void saveLokal(String tanggal) {
//		commonModel.Description = "";
        /*ctrlCommon.save(commonModel);
        for (int i = 0; i < takingOrderAdapter.getGroupCount(); i++) {
            if (takingOrderAdapter.listOrderByGroup(takingOrderAdapter.getGroup(i)) != null) {
                for (int j = 0; j < takingOrderAdapter.getChildrenCount(i); j++) {
                    OrderDetailModel orderDetailModel = takingOrderAdapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        ctrlTakingOrder.save(new TakingOrderModel(
                                customerModel.CustId,
                                orderDetailModel.skuModel.SKUId,
                                tanggal,
                                orderDetailModel.salesProgramModel.SalesProgramId,
                                orderDetailModel.QTY_B,
                                orderDetailModel.QTY_K,
                                orderDetailModel.DISCOUNT1,
                                orderDetailModel.DISCOUNT2,
                                orderDetailModel.DISCOUNT3,
                                orderDetailModel.TOTAL_PRICE,
                                orderDetailModel.PAYMENT_TERM,
                                orderDetailModel.note,
                                orderDetailModel.PRICE_B,
                                orderDetailModel.PRICE_K,
                                orderDetailModel.LAMA_CREDIT,
                                //						orderDetailModel.skuModel.DISCOUNT_TYPE,
                                orderDetailModel.DiscountType,
                                commonModel.CommonID,
                                orderDetailModel.RandomID
                        ));
                    }
                }
            }
        }*/
        /*try {
            if (fragment.getDiskonAdapter() != null) {
                for (int i = 0; i < fragment.getDiskonAdapter().getCount(); i++) {
                    JSONObject promo = fragment.getDiskonAdapter().getItem(i);
                    if (promo.has("isChecked") && promo.getBoolean("isChecked")) {
                        ctrlDiscountPromo.save(new DiscountPromoModel(
                                promo.getInt("DISCOUNT_PROMO_ID"),
                                Integer.parseInt(customerModel.CustId),
                                commonModel.CommonID
                        ));
                    }
                }
            }
            if (fragment.getDiskonPromoAdapter() != null) {
                for (int i = 0; i < fragment.getDiskonPromoAdapter().getGroupCount(); i++) {
                    for (int j = 0; j < fragment.getDiskonPromoAdapter().getChildrenCount(i); j++) {
                        DiskonPromoModel model = fragment.getDiskonPromoAdapter().getChild(i, j);
                        ctrlDiscountResult.save(new DiscountResultModel(
                                Integer.parseInt(customerModel.CustId),
                                model.DiscountResultID,
                                model.QTY,
                                commonModel.CommonID,
                                model.RandomID
                        ));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void chekOutOrder() {
        final String CallST = Util.getStringPreference(this, "CallST");
        Log.d("CALL ST", CallST);
        Util.confirmDialog(this, "Info", "Apakah anda ingin Check Out Data?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    boolean noOrder = false;
                    if (CallST.equals("21") || CallST.equals("22")) {//Taking Order dan Canvass
                        noOrder = commonModel.PRICE == 0;
                    }

                    Log.d("No Order", noOrder + "");
                    if (noOrder) {
                        noOrder();
                    } else {
                        if (ctrlAppModul.isModul("35")) {
                            pinOrder();
                        } else {
//							kirimOrder();
                            if (ctrlAppModul.isModul("74")) {
                                if (takingOrderAdapter.isOrder()) {
                                    popupAlamat();
                                } else {
                                    kirimOrder();
                                }
                            } else {
                                kirimOrder();
                            }
                        }
                    }
                }
            }
        });
    }

    private void popupAlamat() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Delivery Address");
        dialog.setContentView(R.layout.popup_alamat);
        ((Spinner) dialog.findViewById(R.id.deliveryAddress)).setAdapter(new ComboBoxAdapter(this, dbmaster.listCombo("select AddID,AddAddres from custaddres where CustID='" + customerModel.CustId + "'")));
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final ComboBoxModel model = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.deliveryAddress)).getSelectedItem();
                if (model != null) {
                    realmUI.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            commonModel.Description = model.text;
                        }
                    });

                    kirimOrder();
                } else {
                    Util.showDialogInfo(NewOrderSpesialActivity.this, "Alamat pengiriman tidak ada!");
                }
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void noOrder() {
        Log.d("No Order", "Called");
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_no_order);
        dialog.setTitle("Check-out Failed!");
//        ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("REASON_NO_SALES")));
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        /*((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ComboBoxModel coModel = (ComboBoxModel) ((Spinner) dialog.findViewById(R.id.alasan_by_pass_barcode)).getSelectedItem();
                commonModel.ROrderID = coModel.value;
                progressDialog.setMessage("Check Out Data...");
                progressDialog.show();
                new Checkout().execute();
            }
        });*/
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void pinOrder() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pin_order);
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String pin = ((EditText) dialog.findViewById(R.id.pin)).getText().toString();
                if (pin.equals(customerModel.PinID)) {
                    dialog.dismiss();
                    kirimOrder();
                } else {
                    Util.showDialogInfo(NewOrderSpesialActivity.this, "PIN salah!");
                }
            }
        });
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void kirimOrder() {
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                commonModel.ROrderID = "0";
            }
        });
        progressDialog.setMessage("Check Out Data...");
        progressDialog.show();
//    	new Checkout().execute();
        new CheckoutBackground().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        realmUI.close();
    }

    @Override
    protected void onDestroy() {
        realmUI.close();
        if (isBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, SadixService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            unbindService(serviceConnection);
            isBound = false;
        }
        super.onDestroy();
    }
}
