package com.bhn.sadix;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.Spinner;

import com.bhn.sadix.Data.CollectionModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.LogDataDetail;
import com.bhn.sadix.Data.PriceMonitoringModel;
import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.Data.StockCompetitorModel;
import com.bhn.sadix.Data.StockCustomerModel;
import com.bhn.sadix.Data.TakingOrderModel;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.adapter.LogDataRealmAdapter;
import com.bhn.sadix.database.CtrlCollector;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlRetur;
import com.bhn.sadix.database.CtrlStokCompetitor;
import com.bhn.sadix.database.CtrlStokOutlet;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class LogDataActivity extends AppCompatActivity {
    private ComboBoxAdapter adapterJenis;
    private CtrlCommon ctrlCommon;
    private ProgressDialog progressDialog;
    private LogDataRealmAdapter adapter;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlStokOutlet ctrlStokOutlet;
    private CtrlStokCompetitor ctrlStokCompetitor;
    private CtrlRetur ctrlRetur;
    private CtrlCollector ctrlCollector;
    private DatabaseHelper db;
    private Realm realmUI;
    private RealmResults<CommonModel> Result;

    //    Realm realm;
    private Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_data);

        realmUI = Realm.getDefaultInstance();
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapterJenis = new ComboBoxAdapter(this, new ArrayList<ComboBoxModel>());
        adapterJenis.add(new ComboBoxModel("-", "Semua"));
        adapterJenis.add(new ComboBoxModel("0", "Gagal Kirim"));
        adapterJenis.add(new ComboBoxModel("1", "Terkirim"));
        ((Spinner) findViewById(R.id.jenis)).setAdapter(adapterJenis);

        ((Spinner) findViewById(R.id.jenis)).setSelection(1);

        ((Spinner) findViewById(R.id.jenis)).setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Log.d("Selection Changed", "Position : " + position);
                loadData(adapterJenis.getItem(position).value);
//                new LoadData().execute(adapterJenis.getItem(position).value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        ctrlCommon = new CtrlCommon(this);
        ctrlTakingOrder = new CtrlTakingOrder(this);
        ctrlStokOutlet = new CtrlStokOutlet(this);
        ctrlStokCompetitor = new CtrlStokCompetitor(this);
        ctrlRetur = new CtrlRetur(this);
        ctrlCollector = new CtrlCollector(this);
        db = new DatabaseHelper(this);

        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
    }

    private void loadData(final String value) {

        RealmChangeListener callback = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                Log.d("On Change", "Called");
                if (Result != null && Result.isValid() && Result.isLoaded()) {
                    updateAdapter();
                }
//                adapter.notifyDataSetChanged();
            }
        };

        if (value.equals("-")) {
            Log.d("Semua", "Called");
            Result = realmUI.where(CommonModel.class)
                    .equalTo("Done", 1)
                    .findAllAsync();
        } else if (value.equals("0")) {
            Log.d("Gagal", "Called");
            Result = realmUI.where(CommonModel.class)
                    .equalTo("Done", 1)
                    .equalTo("Status", value)
                    .findAllAsync();
        } else if (value.equals("1")) {
            Log.d("Terkirim", "Called");
            Result = realmUI.where(CommonModel.class)
                    .equalTo("Done", 1)
                    .equalTo("Status", value)
                    .findAllAsync();
        }
        /*Result = realmUI.where(CommonModel.class)
                .equalTo("Done", 1)
                .equalTo("Status", value)
                .findAllAsync();*/

        Result.addChangeListener(callback);

        /*if (Result.isLoaded()) {
            if (!value.equals("")) {

                Log.wtf("Log data", Result.size() + "");


            }
        }*/
    }

    private void exportDatatoJSON() {
        android.util.Log.e("list", "" + Result.size());
        JSONArray array = new JSONArray();
        for (com.bhn.sadix.Data.CommonModel commonModel : Result) {
            android.util.Log.e("commonModel", "" + commonModel.CommonID);

            JSONObject object = Util.convertJSONEndVisit(LogDataActivity.this, commonModel.CommonID, realmUI);

            if (object != null) {
                array.put(object);
            }
//                        array.put(Util.convertJSONEndVisit(LogDataActivity.this, commonModel.CommonID));
        }
        Util.saveFile(array.toString().getBytes(), "ExportEndVisit.txt");
        Util.showDialogInfo(LogDataActivity.this, "Export data berhasil!");
    }


    private void updateAdapter() {
        adapter = new LogDataRealmAdapter(LogDataActivity.this, new ArrayList<CommonModel>(), new HashMap<CommonModel, List<LogDataDetail>>()) {
            @Override
            public void berhasilKirim() {
                updateAdapter();
            }
        };

        for (com.bhn.sadix.Data.CommonModel cmModel : Result) {
                        /*realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                            }
                        });*/
            CommonModel cm = realmUI.copyFromRealm(cmModel);

            adapter.addHeader(cm);

            List<TakingOrderModel> listOrder = cm.takingOrderModel/*.where().findAll()*/;

            Log.d("listOrder on Log", listOrder.size() + "");

            List<StockCustomerModel> listStockOutlet = cm.stockCustomerModel;
//                        Log.d("listStock on Log", listStockOutlet.size() + "");
            List<StockCompetitorModel> listStockCompetitor = cm.stockCompetitorModel;
//                        Log.d("listStockC on Log", listStockCompetitor.size() + "");
            List<ReturnModel> listRetur = cm.returnModel;
//                        Log.d("listRetur on Log", listRetur.size() + "");
            List<CollectionModel> collectorModel = cm.collectionModel;
//                        Log.d("listCollect on Log", collectorModel.size() + "");
            List<PriceMonitoringModel> priceMonitoring = cm.priceMonitoringModel;
//                        Log.d("priceMonitoring on Log", priceMonitoring.size() + "");

            adapter.addChild(cm, new LogDataDetail(listStockOutlet, listStockCompetitor, listOrder, listRetur, collectorModel, priceMonitoring));

        }

        if (adapter != null) {
            ((ExpandableListView) findViewById(R.id.list)).setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Export")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Export");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
//            exportData();
            showDialogExport();
//            new ExportData().execute(((ComboBoxModel) ((Spinner) findViewById(R.id.jenis)).getSelectedItem()).value);
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDialogExport() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LogDataActivity.this);
        builder.setTitle("Ekspor Data ke");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(LogDataActivity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("JSON");
        arrayAdapter.add("CSV");

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    exportDatatoJSON();
                } else {
                    exportDatatoCSV();
                }
            }
        });

        builder.show();
    }

    private void exportDatatoCSV() {
        android.util.Log.e("list", "" + Result.size());
        boolean result = false;

        File outputFile = new File(Util.LOKASI + "ExportEndVisit.csv");
        if (outputFile.exists()) {
            Log.d("FILE IS EXISTS", "true");
            outputFile.delete();
        }

        for (com.bhn.sadix.Data.CommonModel commonModel : Result) {
            android.util.Log.e("commonModel", "" + commonModel.CommonID);

            result = Util.convertCSVEndVisit(LogDataActivity.this, commonModel.CommonID, realmUI);
        }

        if (result) {
            Util.showDialogInfo(LogDataActivity.this, "Export data berhasil!");
        } else {
            Util.showDialogError(LogDataActivity.this, "Export data gagal");
        }
    }

    private class ExportData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Realm realmO = Realm.getDefaultInstance();
            String value = params[0];
            try {
                if (!value.equals("")) {

                    RealmResults<com.bhn.sadix.Data.CommonModel> list = null;

                    if (value.equals("-")) {
                        list = realmO.where(com.bhn.sadix.Data.CommonModel.class).notEqualTo("CustomerId", "").equalTo("Done", 1).findAll();
                    } else if (value.equals("0")) {
                        list = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Status", "0").equalTo("Done", 1).findAll();
                    } else if (value.equals("1")) {
                        list = realmO.where(com.bhn.sadix.Data.CommonModel.class).equalTo("Status", "1").equalTo("Done", 1).findAll();
                    }
                    android.util.Log.e("list", "" + list.size());
                    JSONArray array = new JSONArray();
                    for (com.bhn.sadix.Data.CommonModel commonModel : list) {
                        android.util.Log.e("commonModel", "" + commonModel.CommonID);

                        JSONObject object = Util.convertJSONEndVisit(LogDataActivity.this, commonModel.CommonID, realmO);

                        if (object != null) {
                            array.put(object);
                        }
//                        array.put(Util.convertJSONEndVisit(LogDataActivity.this, commonModel.CommonID));
                    }
                    realmO.close();
                    Util.saveFile(array.toString().getBytes(), "ExportEndVisit.txt");
                }
            } catch (Exception e) {
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogError(LogDataActivity.this, result);
            } else {
                Util.showDialogInfo(LogDataActivity.this, "Export data berhasil!");
            }
            super.onPostExecute(result);
        }
    }

    private class LoadData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String value = params[0];

                Log.d("value", params[0]);

                adapter = null;
                /*adapter = new LogDataRealmAdapter(LogDataActivity.this, new ArrayList<CommonModel>(), new HashMap<CommonModel, List<LogDataDetail>>()) {
                    @Override
                    public void berhasilKirim() {
                        new LoadData().execute(value);
                    }
                };*/

                if (!value.equals("")) {
                    /*List<CommonModel> list = null;
                    if (model.value.equals("-")) {
                        list = ctrlCommon.list();
                    } else {
                        list = ctrlCommon.listByStatus(model.value);
                    }*/

                    Realm realmO = Realm.getDefaultInstance();

                    RealmResults<com.bhn.sadix.Data.CommonModel> commonModel = null;

                    if (value.equals("-")) {
                        Log.d("Semua", "Called");
                        commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class)
                                .equalTo("Done", 1)
                                .findAll();
                    } else if (value.equals("0")) {
                        Log.d("Gagal", "Called");
                        commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class)
                                .like("Status", "0")
                                .equalTo("Done", 1)
                                .findAll();
                    } else if (value.equals("1")) {
                        Log.d("Terkirim", "Called");
                        commonModel = realmO.where(com.bhn.sadix.Data.CommonModel.class)
                                .like("Status", "1")
                                .equalTo("Done", 1)
                                .findAll();
                    }

                    /*for (com.bhn.sadix.Data.CommonModel cmModel : commonModel) {
                        adapter.addHeader(cmModel);
                        List<TakingOrderModel> listOrder = ctrlTakingOrder.getTakingOrderModel(commonModel.CommonID);
                        List<StockOutletModel> listStockOutlet = ctrlStokOutlet.getStockOutletModel(commonModel.CommonID);
                        List<StockCompetitorModel> listStockCompetitor = ctrlStokCompetitor.getStockCompetitorModel(commonModel.CommonID);
                        List<ReturModel> listRetur = ctrlRetur.getReturModel(commonModel.CommonID);
                        List<CollectorModel> collectorModel = ctrlCollector.get(commonModel.CommonID);
                        List<JSONObject> priceMonitoring = db.list("PriceMonitoring", commonModel.CommonID);
                        Log.d("Price Monitoring on Log", priceMonitoring.size() + "");

                        adapter.addChild(commonModel, new LogDataDetailModel(listStockOutlet, listStockCompetitor, listOrder, listRetur, collectorModel, priceMonitoring));
                    }*/
                    Log.wtf("Log data", commonModel.size() + "");

                    for (com.bhn.sadix.Data.CommonModel cmModel : commonModel) {
                        /*realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                            }
                        });*/
                        CommonModel cm = realmO.copyFromRealm(cmModel);

                        adapter.addHeader(cm);

                        List<TakingOrderModel> listOrder = cm.takingOrderModel/*.where().findAll()*/;

                        Log.d("listOrder on Log", listOrder.size() + "");

                        List<StockCustomerModel> listStockOutlet = cm.stockCustomerModel;
//                        Log.d("listStock on Log", listStockOutlet.size() + "");
                        List<StockCompetitorModel> listStockCompetitor = cm.stockCompetitorModel;
//                        Log.d("listStockC on Log", listStockCompetitor.size() + "");
                        List<ReturnModel> listRetur = cm.returnModel;
//                        Log.d("listRetur on Log", listRetur.size() + "");
                        List<CollectionModel> collectorModel = cm.collectionModel;
//                        Log.d("listCollect on Log", collectorModel.size() + "");
                        List<PriceMonitoringModel> priceMonitoring = cm.priceMonitoringModel;
//                        Log.d("priceMonitoring on Log", priceMonitoring.size() + "");

                        adapter.addChild(cm, new LogDataDetail(listStockOutlet, listStockCompetitor, listOrder, listRetur, collectorModel, priceMonitoring));
                    }

                    realmO.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
//                Log.e("LoadData", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (adapter != null) {
                ((ExpandableListView) findViewById(R.id.list)).setAdapter(adapter);
            }
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realmUI.close();
        Result.removeAllChangeListeners();
    }
}
