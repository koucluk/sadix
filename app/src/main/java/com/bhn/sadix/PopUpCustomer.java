package com.bhn.sadix;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bhn.sadix.adapter.CustomerAdapter;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.model.CustomerModel;

public class PopUpCustomer extends Dialog {
	private ListView list;
	private CustomerAdapter adapter;
	private CustomerListener listener;
	private LayoutInflater inflater;
	private ProgressBar progress;
	private CtrlCustomer ctrlCustomer;
	private int CustomerLevel;

	public PopUpCustomer(Context context, CustomerListener listener) {
		super(context);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.listener = listener;
		setCancelable(false);
	    ctrlCustomer = new CtrlCustomer(context);
	}
	public void setCustomerLevel(int customerLevel) {
		CustomerLevel = customerLevel-1;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
			dismiss();
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	public void show() {
		new Load().execute();
		super.show();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.popup_seach, null);
		list = (ListView) view.findViewById(R.id.list);
		progress = (ProgressBar) view.findViewById(R.id.progress);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(listener != null) {
					listener.selectCustomer(adapter.getItem(position));
				}
				dismiss();
			}
		});
		list.setVisibility(View.GONE);
		progress.setVisibility(View.GONE);
		setContentView(view);
		setTitle("Search Customer");
		list.setAdapter(adapter);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
	}
	class Load extends AsyncTask<Void, Void, Void> {
		Load() {
			if(progress != null) {
				progress.setVisibility(View.VISIBLE);
				list.setVisibility(View.GONE);
			}
		}
		@Override
		protected Void doInBackground(Void... voids) {
			adapter = new CustomerAdapter(getContext(), ctrlCustomer.listCustomerLevel(CustomerLevel));
			return null;
		}
		@Override
		protected void onPostExecute(Void respon) {
			super.onPostExecute(respon);
			list.setAdapter(adapter);
			progress.setVisibility(View.GONE);
			list.setVisibility(View.VISIBLE);
		}
	}
	public interface CustomerListener {
		public void selectCustomer(CustomerModel cust);
	}

}
