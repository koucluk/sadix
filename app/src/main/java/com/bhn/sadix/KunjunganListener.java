package com.bhn.sadix;

import android.app.Dialog;

import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.adapter.AssetAdapter;
import com.bhn.sadix.adapter.AssetMutasiAdapter;
import com.bhn.sadix.adapter.AssetServiceAdapter;
import com.bhn.sadix.adapter.CollectionPayAdapter;
import com.bhn.sadix.adapter.CrcOrderAdapter;
import com.bhn.sadix.adapter.CrcReportAdapter;
import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.adapter.PriceMonitoringAdapter;
import com.bhn.sadix.adapter.PrinsipleAdapter;
import com.bhn.sadix.adapter.ReturDetailAdapter;
import com.bhn.sadix.adapter.StokCompetitorAdapter;
import com.bhn.sadix.adapter.StokCustomerAdapter;
import com.bhn.sadix.adapter.StokGroupAdapter;
import com.bhn.sadix.adapter.StokOnCarAdapter;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;

public interface KunjunganListener {
    CommonModel getCommonModel();

    CustomerModel getCustomerModel();

    CustomerTypeModel getCustomerType();

    OrderDetailAdapter getTakingAdapter();

    OrderDetailAdapter getTakingPrincipleAdapter();

    ReturDetailAdapter getReturAdapter();

    StokCustomerAdapter getStokAdapter();

    StokCompetitorAdapter getStokCompetitor();

    StokGroupAdapter getStokGroupAdapter();

    CollectionPayAdapter getCollectionAdapter();

    PriceMonitoringAdapter getPriceMonitoringAdapter();

    PriceMonitoringAdapter getPriceCompetitorAdapter();

    //    PrinsipleModel getPrinsipleModel();
    PrincipleModel getPrinsipleModel();

    StokOnCarAdapter getStokOnCarAdapter();

    CrcReportAdapter getCrcReportAdapter();

    String getNote();

    void updateMenuFunction();

    void restoreMenuFunction();

    PrinsipleAdapter getPrinsipleAdapter();

    //    void handleTO3(PrinsipleModel model, int pos);
    void handleTO3(PrincipleModel model, int pos);

    void setPrincipleAdapter(PrinsipleAdapter adapter);

    CrcOrderAdapter getCrcOrderAdapter();

    AssetAdapter getAssetAdapter();

    AssetMutasiAdapter getMutasiAdapter();

    AssetServiceAdapter getAssetServiceAdapter();

    void dismissAndUpdateDialog(Dialog dialog);
}
