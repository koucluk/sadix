package com.bhn.sadix.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;

public class AssetType1Widget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private JSONObject json;
    private String ASETID;
    private String ASETNO;
    private String value = "1";
    private int tipe;

    private DatabaseHelper db;
    private String CommonID;
    private String RandomID;
    private Realm realmUI;
    private AssetModel model = null;

    public AssetType1Widget(Context context, JSONObject json, String ASETID, String ASETNO, String CommonID, int type) {
        super(context);
        RandomID = UUID.randomUUID().toString();
        tipe = type;
        this.context = context;
        this.json = json;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        db = new DatabaseHelper(this.context);
        this.CommonID = CommonID;
        realmUI = Realm.getDefaultInstance();
        init();
    }

    private void init() {
        try {
            if ((model = realmUI.where(AssetModel.class)
                    .equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX"))
                    .findFirst()) == null) {

                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel commonModel = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();
                        model = realm.createObject(AssetModel.class, RandomID);
                        commonModel.assetModel.add(model);
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    model.AsetID = ASETNO;
                    model.CommonID = CommonID;
                    model.Idx = json.getInt("IDX");
                    model.Name = json.getString("NAME");
                    model.GroupAset = json.getString("GROUP_ASET");
                    model.TypeMnt = json.getInt("TYPE_MNT");
                    model.Posisi = json.getInt("POSISI");
                    model.JenisWidget = 1;
                    model.tipe = tipe;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                model = realm.copyToRealmOrUpdate(model);
            }
        });

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_type_asset1, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.text)).setText(json.getString("NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        final AssetModel finalModel = model;

        ((RadioGroup) view.findViewById(R.id.radioGroupYesNo)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup rg, final int id) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (id == R.id.rd_yes) {
                            finalModel.Value = "1";
                        } else {
                            finalModel.Value = "0";
                        }
                    }
                });
            }
        });
        try {

//            JSONObject data = db.getMerchandiser("Aset", CommonID, json.getString("IDX"));
            AssetModel asset = realmUI.where(AssetModel.class).equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX"))
                    .findFirst();

            if (asset != null) {
                if (Integer.parseInt(asset.Value) == 1) {
                    ((RadioButton) view.findViewById(R.id.rd_yes)).setChecked(true);
                } else if (Integer.parseInt(asset.Value) == 0) {
                    ((RadioButton) view.findViewById(R.id.rd_no)).setChecked(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.setLayoutParams(param_view);

        addView(view);
    }

    /*public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
//			rjson.put("AsetID", ASETID);
            rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("Value", value);
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }*/

}
