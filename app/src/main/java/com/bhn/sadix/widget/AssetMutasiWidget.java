package com.bhn.sadix.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.AssetMutasiModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.model.ComboBoxModel;

import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

import io.realm.Realm;

public class AssetMutasiWidget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private String ASETNAME;
    private String ASETID;
    private String ASETTYPE;
    private String NoSeri;
    private String Keterangan;
    private int JENIS;
    private DbAsetHelper dbAsetHelper;
    private String RandomID;
    private String CommonID;
    private AssetMutasiModel model;
    private AssetModel assetModel;

    Realm realm;

    public AssetMutasiWidget(Context context, String ASETID, String ASETNAME, String ASETTYPE, int JENIS) {
        super(context);
        this.context = context;
        this.ASETID = ASETID;
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.JENIS = JENIS;
        RandomID = UUID.randomUUID().toString();
        dbAsetHelper = new DbAsetHelper(context);
        realm = Realm.getDefaultInstance();
        init();
    }

    public AssetMutasiWidget(Context context, String ASETID, String ASETNAME, String ASETTYPE, int JENIS, String NoSeri, String Keterangan, String CommonId) {
        super(context);
        this.context = context;
        this.ASETID = ASETID;
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.JENIS = JENIS;
        this.NoSeri = NoSeri;
        this.Keterangan = Keterangan;
        this.CommonID = CommonId;
        RandomID = UUID.randomUUID().toString();
        dbAsetHelper = new DbAsetHelper(context);
        realm = Realm.getDefaultInstance();
        init();
    }

    private void init() {
        final CommonModel commonModel = realm.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_asset_mutasi_widget, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.ASETNAME)).setText(ASETNAME);
            if (JENIS == 1) { //pemasangan
                if ((model = realm.where(AssetMutasiModel.class)
                        .equalTo("CommonID", CommonID)
                        .equalTo("AsetID", ASETID)
                        .findFirst()) == null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            model = realm.createObject(AssetMutasiModel.class, RandomID);
                            model.CommonID = CommonID;
                            model.Remark = "";
                            commonModel.assetMutasiModel.add(model);
                        }
                    });
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        model.AsetID = ASETID;
                    }
                });

                List<ComboBoxModel> merkData = dbAsetHelper.listCombo("select IDX,NAME from ASSET_CONFIG where TYPE='1'");
                List<ComboBoxModel> typeData = dbAsetHelper.listCombo("select IDX,NAME from ASSET_CONFIG where TYPE='0'");

                final ComboBoxAdapter merkAdapter = new ComboBoxAdapter(context, merkData);
                final ComboBoxAdapter typeAdapter = new ComboBoxAdapter(context, typeData);

                ((Spinner) view.findViewById(R.id.Merk)).setAdapter(merkAdapter);

                ((Spinner) view.findViewById(R.id.Type)).setAdapter(typeAdapter);

                if (model.MerkID != 0) {
                    int index = merkData.indexOf(new ComboBoxModel(String.valueOf(model.MerkID)));
                    ((Spinner) view.findViewById(R.id.Merk)).setSelection(index);
                }

                if (model.TypeID != 0) {
                    int index = typeData.indexOf(new ComboBoxModel(String.valueOf(model.TypeID)));
                    ((Spinner) view.findViewById(R.id.Type)).setSelection(index);
                }

                ((EditText) view.findViewById(R.id.IDAsset)).setText(ASETID);
                ((EditText) view.findViewById(R.id.AsetName)).setText(ASETNAME);
                ((EditText) view.findViewById(R.id.AsetName)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(final Editable editable) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.AsetName = editable.toString();
                            }
                        });
                    }
                });

                ((Spinner) view.findViewById(R.id.Merk)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.MerkID = Integer.parseInt(merkAdapter.getItem(i).value);
                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.MerkID = Integer.parseInt(merkAdapter.getItem(0).value);
                            }
                        });
                    }
                });

                ((Spinner) view.findViewById(R.id.Type)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.TypeID = Integer.parseInt(typeAdapter.getItem(i).value);
                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.TypeID = Integer.parseInt(typeAdapter.getItem(0).value);
                            }
                        });
                    }
                });

                if (NoSeri != null)
                    ((EditText) view.findViewById(R.id.NoSeri)).setText(NoSeri);

                ((EditText) view.findViewById(R.id.NoSeri)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(final Editable editable) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.NoSeri = editable.toString();
                            }
                        });
                    }
                });

                if (Keterangan != null)
                    ((EditText) view.findViewById(R.id.Keterangan)).setText(Keterangan);

                ((EditText) view.findViewById(R.id.Keterangan)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(final Editable editable) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                model.Remark = editable.toString();
                            }
                        });
                    }
                });

                view.findViewById(R.id.item_layout_penarikan).setVisibility(View.GONE);
                view.findViewById(R.id.item_layout_pemasangan).setVisibility(View.VISIBLE);
            } else if (JENIS == 2) { //penarikan
                if ((assetModel = realm.where(AssetModel.class)
                        .equalTo("CommonID", CommonID)
                        .equalTo("AsetID", ASETID)
                        .equalTo("tipe", 3)
                        .findFirst()) == null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            assetModel = realm.createObject(AssetModel.class, RandomID);
                            assetModel.CommonID = CommonID;
                            assetModel.Value = "";
                            assetModel.tipe = 3;
                            commonModel.assetModel.add(assetModel);
                        }
                    });
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        assetModel.AsetID = ASETID;
                    }
                });

                final ComboBoxAdapter configAdapter = new ComboBoxAdapter(context, dbAsetHelper.listComboPenarikan("select * from ASSET_CONFIG where TYPE='2'"));

                ((Spinner) view.findViewById(R.id.jenis_penarikan)).setAdapter(configAdapter);
                ((Spinner) view.findViewById(R.id.jenis_penarikan)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetModel.Idx = Integer.parseInt(configAdapter.getItem(i).value);
                                assetModel.Name = configAdapter.getItem(i).text;
                                assetModel.GroupAset = configAdapter.getItem(i).tmp1;
                                assetModel.TypeMnt = Integer.parseInt(configAdapter.getItem(i).tmp1);
                                assetModel.Posisi = Integer.parseInt(configAdapter.getItem(i).tmp1);
                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetModel.Idx = Integer.parseInt(configAdapter.getItem(0).value);
                                assetModel.Name = configAdapter.getItem(0).text;
                                assetModel.GroupAset = configAdapter.getItem(0).tmp1;
                                assetModel.TypeMnt = Integer.parseInt(configAdapter.getItem(0).tmp1);
                                assetModel.Posisi = Integer.parseInt(configAdapter.getItem(0).tmp1);
                            }
                        });

                    }
                });
                view.findViewById(R.id.item_layout_penarikan).setVisibility(View.VISIBLE);
                view.findViewById(R.id.item_layout_pemasangan).setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e("Exception_AssetView", "" + e.getMessage());
        }
        view.setLayoutParams(param_view);
        addView(view);
    }

    public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
            rjson.put("RandomID", RandomID);
            if (JENIS == 1) { //pemasangan
                rjson.put("AsetName", ((EditText) findViewById(R.id.AsetName)).getText().toString());
                rjson.put("AsetID", ASETID);
                ComboBoxModel model = (ComboBoxModel) ((Spinner) findViewById(R.id.Merk)).getSelectedItem();
                if (model != null) {
                    rjson.put("MerkID", Integer.parseInt(model.value));
                }
                model = (ComboBoxModel) ((Spinner) findViewById(R.id.Type)).getSelectedItem();
                if (model != null) {
                    rjson.put("TypeID", Integer.parseInt(model.value));
                }
                rjson.put("Remark", ((EditText) findViewById(R.id.Keterangan)).getText().toString());
                rjson.put("NoSeri", ((EditText) findViewById(R.id.NoSeri)).getText().toString());
            } else if (JENIS == 2) { //penarikan
                ComboBoxModel model = (ComboBoxModel) ((Spinner) findViewById(R.id.jenis_penarikan)).getSelectedItem();
                rjson.put("AsetID", ASETID);
                rjson.put("Value", "");
                if (model != null) {
                    rjson.put("Idx", model.value);
                    rjson.put("Name", model.text);
                    rjson.put("GroupAset", model.tmp1);
                    rjson.put("TypeMnt", model.tmp1);
                    rjson.put("Posisi", model.tmp1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }

    public int getJenis() {
        return JENIS;
    }
}
