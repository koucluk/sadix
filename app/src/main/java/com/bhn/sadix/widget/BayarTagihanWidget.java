package com.bhn.sadix.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.InformasiModel;

public class BayarTagihanWidget extends LinearLayout {
	private LayoutInflater mInflater;
	private Context context;
	private ComboBoxModel model;
	private double amount = 0;
	private CtrlConfig ctrlConfig;

	public BayarTagihanWidget(Context context) {
		super(context);
		this.context = context;
		ctrlConfig = new CtrlConfig(this.context);
		init();
	}
	public BayarTagihanWidget(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	public BayarTagihanWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	private void init() {
		ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		);
		setLayoutParams(param);
		
		mInflater = LayoutInflater.from(context);
		LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.bayar_tagihan, null);
		ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		);
		((Spinner)view.findViewById(R.id.jenis_bayar)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("PAYMENT_METHOD")));
		view.setLayoutParams(param_view);
		addView(view);
//		viewInfo();
	}
//	private void viewInfo() {
//		if(model != null) {
//			((TextView)findViewById(R.id.Header)).setText(informasiModel.Header);
//			((TextView)findViewById(R.id.DateValidity)).setText("Berlaku : " + informasiModel.DateValidity);
//			((TextView)findViewById(R.id.Content)).setText(informasiModel.Content.replaceAll("#13", "\n"));
//		}
//	}
}
