package com.bhn.sadix.widget;

public interface ItemPhotoCustomerBrandingWidgetListener {
    public void takePicture(ItemPhotoCustomerBrandingWidget widget);

    public void send(ItemPhotoCustomerBrandingWidget widget);

    public void delete(ItemPhotoCustomerBrandingWidget widget);
}
