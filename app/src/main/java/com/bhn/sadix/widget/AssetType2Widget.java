package com.bhn.sadix.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;

public class AssetType2Widget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private JSONObject json;
    private String ASETID;
    private EditText number;
    private String ASETNO;
    private DatabaseHelper db;
    private String CommonID;
    private String RandomID;
    private AssetModel model;
    private int tipe;

    Realm realmUI;

    public AssetType2Widget(Context context, JSONObject json, String ASETID, String ASETNO, String CommonID, int type) {
        super(context);
        RandomID = UUID.randomUUID().toString();
        this.context = context;
        this.json = json;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        tipe = type;
        realmUI = Realm.getDefaultInstance();
        db = new DatabaseHelper(this.context);
        init();
    }

    private void init() {
        try {
            if ((model = realmUI.where(AssetModel.class)
                    .equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX"))
                    .findFirst()) == null) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel commonModel = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();
                        model = realm.createObject(AssetModel.class, RandomID);
                        commonModel.assetModel.add(model);
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    model.AsetID = ASETNO;
                    model.CommonID = CommonID;
                    model.Idx = json.getInt("IDX");
                    model.Name = json.getString("NAME");
                    model.GroupAset = json.getString("GROUP_ASET");
                    model.TypeMnt = json.getInt("TYPE_MNT");
                    model.Posisi = json.getInt("POSISI");
                    model.JenisWidget = 2;
                    model.tipe = tipe;
//                    model.RandomID = RandomID;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                model = realm.copyToRealmOrUpdate(model);
            }
        });

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_type_asset2, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        number = (EditText) view.findViewById(R.id.number);

        final AssetModel finalModel = model;

        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        finalModel.Value = editable.toString();
                    }
                });
            }
        });

        try {
            ((TextView) view.findViewById(R.id.text)).setText(json.getString("NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            /*JSONObject data = db.getMerchandiser("Aset", CommonID, json.getString("IDX"));*/
            AssetModel asset = realmUI.where(AssetModel.class).equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX")).findFirst();
            if (asset != null) {
                ((EditText) view.findViewById(R.id.number)).setText(asset.Value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.setLayoutParams(param_view);
        addView(view);
    }

    /*public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
//			rjson.put("AsetID", ASETID);
            rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("Value", number.getText().toString());
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }*/

}
