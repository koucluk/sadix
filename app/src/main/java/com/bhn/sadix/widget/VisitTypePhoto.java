package com.bhn.sadix.widget;

import java.util.UUID;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.Util;

public class VisitTypePhoto extends LinearLayout {
    private VisitNote visitNote;
    private JSONObject visitItem;
    private DbMasterHelper db;
    private TextView label;
    private ImageView image;
    private ImageButton btn;
    private String RandomID;
    private String CommonID;
    private VisitTypePhotoListener listener;
    private byte[] data;

    public VisitTypePhoto(Context context, VisitNote visitNote, String CommonID, VisitTypePhotoListener listener) {
        super(context);
        this.visitNote = visitNote;
        db = new DbMasterHelper(context);
        RandomID = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        this.listener = listener;
        try {
            visitItem = db.queryJSON("select * from visit_item where VisitID='" + visitNote.getVisitID() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.visit_type_photo, null);
        label = (TextView) view.findViewById(R.id.label);
        image = (ImageView) view.findViewById(R.id.image);
        btn = (ImageButton) view.findViewById(R.id.btn);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                photo();
            }
        });

        try {
            label.setText(visitNote.getVisitName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    private void photo() {
        if (listener != null) {
            listener.visitPhoto(this);
        }
    }

    public void setData(byte data[], Bitmap bitmap) {
        this.data = data;
        image.setImageBitmap(bitmap);
    }

    public JSONObject getValue() {
        JSONObject json = new JSONObject();
        try {
            json.put("VisitId", visitNote.getVisitID());
            json.put("ItemId", visitItem.getString("ItemID"));
            if (data != null) {
                json.put("ItemValue", Base64.encodeBytes(data));
            } else {
                json.put("ItemValue", "");
            }
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public interface VisitTypePhotoListener {
        public void visitPhoto(VisitTypePhoto photo);
    }

    public boolean isValid() {
        try {
            int ReqField = visitNote.getReqField();
            if (ReqField == 1) {
                if (data != null) {
                    return true;
                } else {
                    Util.showDialogInfo(getContext(), visitNote.getVisitName() + " photo belum diambil!");
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getContext(), "" + e.getMessage());
            return false;
        }
    }

}
