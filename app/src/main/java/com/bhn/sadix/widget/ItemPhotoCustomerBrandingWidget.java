package com.bhn.sadix.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.KunjunganActivity;
import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustBrandingModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

import java.io.IOException;

import io.realm.Realm;

public class ItemPhotoCustomerBrandingWidget extends LinearLayout implements ConnectionEvent {
    private LayoutInflater mInflater;
    private Context context;
    private ImageButton btn_photo;
    private ImageButton btn_send;
    private ImageButton btn_hapus;
    private Spinner photo_type;
    private TextView image;
    private ItemPhotoCustomerBrandingWidgetListener listener;
    private byte[] data;
    private ConnectionServer server;
    private CustomerModel customerModel;
    private CtrlConfig ctrlConfig;
    private EditText Description;
    private CtrlCustBranding ctrlCustBranding;
    private String BrandingID;
    private String ID;
    public boolean isDone = false;
    public CustomerBrandingModel brandingModel;

    public ItemPhotoCustomerBrandingWidget(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ItemPhotoCustomerBrandingWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public ItemPhotoCustomerBrandingWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void addItemPhotoCustomerBrandingWidgetListener(ItemPhotoCustomerBrandingWidgetListener listener) {
        this.listener = listener;
    }

    public void setImage(Bitmap bitmap) {
        image.setBackgroundDrawable(new BitmapDrawable(bitmap));
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    /*public void setCommonModel(CommonModel commonModel) {
        this.commonModel = commonModel;
    }*/

    public void setBrandingModel(CustBrandingModel model) {

        int index = 0;
        for (ComboBoxModel Cmodel : ctrlConfig.listComboBox("BRANDING_CATEGORY")) {
            if (Cmodel.value.equals(model.BrandingID)) {
                break;
            }
            index += 1;
        }


        photo_type.setSelection(index);
        Description.setText(model.Description);


        try {
            data = Util.readFile(Util.LOKASI_IMAGE + model.PhotoPic);
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            image.setBackgroundDrawable(new BitmapDrawable(bmp));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setBrandingModel(CustomerBrandingModel model) {

        brandingModel = model;

        int index = 0;
        for (ComboBoxModel Cmodel : ctrlConfig.listComboBox("BRANDING_CATEGORY")) {
            if (Cmodel.value.equals(model.BrandingID)) {
                break;
            }
            index += 1;
        }

        photo_type.setSelection(index);
        Description.setText(model.Description);


        try {
            data = Util.readFile(Util.LOKASI_IMAGE + model.PhotoPic);
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            image.setBackgroundDrawable(new BitmapDrawable(bmp));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void init() {
        ctrlCustBranding = new CtrlCustBranding(context);
        ctrlConfig = new CtrlConfig(context);
        server = new ConnectionServer(context);
        server.addConnectionEvent(this);
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_photo_branding_customer, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);
        btn_photo = (ImageButton) view.findViewById(R.id.btn_photo);
        btn_send = (ImageButton) view.findViewById(R.id.btn_send);
        btn_hapus = (ImageButton) view.findViewById(R.id.btn_hapus);
        photo_type = (Spinner) view.findViewById(R.id.photo_type);
        photo_type.setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("BRANDING_CATEGORY")));
        image = (TextView) view.findViewById(R.id.image);
        Description = (EditText) view.findViewById(R.id.Description);
        btn_photo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.takePicture(ItemPhotoCustomerBrandingWidget.this);
                }
            }
        });
        btn_hapus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.delete(ItemPhotoCustomerBrandingWidget.this);
                }
            }
        });
        btn_send.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        });
        addView(view);
    }

    public void sendData() {

        Realm realm = Realm.getDefaultInstance();

        final com.bhn.sadix.Data.CommonModel commonModel = realm
                .where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        realm.close();
        if (data == null) {
            Util.showDialogInfo(context, "Photo belum diambil!");
            return;
        }
        try {
            JSONObject json = new JSONObject();
            json.put("SalesID", UserModel.getInstance(context).SalesId);
            json.put("CustomerID", customerModel.CustId);
            json.put("BrandingID", ((ComboBoxModel) photo_type.getSelectedItem()).value);
            json.put("PhotoPic", Base64.encodeBytes(data));
            Log.d("common geolat", commonModel.GeoLat);
            json.put("GeoLong", commonModel.GeoLong);
            json.put("GeoLat", commonModel.GeoLat);

            /*if (context instanceof KunjunganActivity) {
//                KunjunganActivity activity = (KunjunganActivity) context;
                json.put("GeoLong", commonModel.GeoLong);
                json.put("GeoLat", commonModel.GeoLat);
            }*/

            json.put("Description", Description.getText().toString());
            Log.wtf("json branding", json.toString());
            Util.saveFile(json.toString().getBytes(), "ExpoprtEndBranding.txt");
            server.setUrl(Util.getServerUrl(context) + "photobranding");
            server.requestJSONObjectNoTimeOut(json);
//            saveLokal(true, 0);
            isDone = true;
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    @Override
    public void error(String error) {
        isDone = true;
        saveLokal(true, 0);
//		Util.showDialogError(context, error);
        Util.Toast(context, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                data = null;
                if (listener != null) {
                    listener.send(this);
                }
            } else {
                isDone = true;
                saveLokal(true, 0);
            }
            Util.Toast(context, json.getString("Message"));
        } catch (Exception e) {
            e.printStackTrace();
            error(e.getMessage());
        }
    }

    public void saveLokal(final boolean send, final int position) {
//        ctrlCustBranding.delete(customerModel.CustId, ((ComboBoxModel) photo_type.getSelectedItem()).value, commonModel.CommonID, false);
        Realm realmUI = Realm.getDefaultInstance();
        final com.bhn.sadix.Data.CommonModel commonModel = realmUI
                .where(com.bhn.sadix.Data.CommonModel.class)
                .equalTo("Done", 0)
                .findFirst();

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (brandingModel == null)
                    brandingModel = new CustomerBrandingModel();


                brandingModel.CommonID = commonModel.CommonID;
                brandingModel.BrandingID = ((ComboBoxModel) photo_type.getSelectedItem()).value;
                brandingModel.CustomerID = customerModel.CustId;
                brandingModel.Description = Description.getText().toString();
                brandingModel.PhotoPic = customerModel.CustId + "_" + ((ComboBoxModel) photo_type.getSelectedItem()).value + ".jpg";
                brandingModel.SalesID = UserModel.getInstance(context).SalesId;
                brandingModel.NmBranding = ctrlConfig.getByConfigClassAndConfigId("BRANDING_CATEGORY", brandingModel.BrandingID).text;
                brandingModel.NmCust = customerModel.CustomerName;

                if (send) {
                    brandingModel.Done = 1;
                } else {
                    brandingModel.Done = 0;
                }

                if (data != null) {
                    Util.saveFileImg(data, brandingModel.PhotoPic);
                    data = null;
                }
                if (context instanceof KunjunganActivity) {
                    KunjunganActivity activity = (KunjunganActivity) context;
                    brandingModel.GeoLong = commonModel.GeoLong;
                    brandingModel.GeoLat = commonModel.GeoLat;
                }

                realm.copyToRealmOrUpdate(brandingModel);
            }
        });

        realmUI.close();

        Util.Toast(context, "Data tersimpan dilokal");
        if (send)
            if (listener != null) {
                listener.send(this);
            }
    }

    public String getBrandingID() {
        return ((ComboBoxModel) photo_type.getSelectedItem()).value;
    }
}
