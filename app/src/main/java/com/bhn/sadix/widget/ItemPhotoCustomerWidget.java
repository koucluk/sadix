package com.bhn.sadix.widget;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

import org.json.JSONObject;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.model.AppModul;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustPhotoModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.Util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class ItemPhotoCustomerWidget extends LinearLayout implements ConnectionEvent {
    private LayoutInflater mInflater;
    private Context context;
    private ImageButton btn_photo;
    private ImageButton btn_send;
    private ImageButton btn_hapus;
    private Spinner photo_type;
    //	private TextView image;
    private ImageViewTouch image;
    private CtrlConfig ctrlConfig;
    private ItemPhotoCustomerListener listener;
    private byte[] data;
    private ConnectionServer server;
    private CustomerModel customerModel;
    private CtrlCustPhoto ctrlCustPhoto;
    private CtrlAppModul ctrlAppModul;

    public ItemPhotoCustomerWidget(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ItemPhotoCustomerWidget(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    public ItemPhotoCustomerWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void addItemPhotoCustomerListener(ItemPhotoCustomerListener listener) {
        this.listener = listener;
    }

    public void setImage(Bitmap bitmap) {
//		image.setBackgroundDrawable(new BitmapDrawable(bitmap));
        image.setImageBitmap(bitmap);
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public void setCustomerModel(CustomerModel customerModel) {
        this.customerModel = customerModel;
    }

    private void init() {
        ctrlConfig = new CtrlConfig(context);
        ctrlCustPhoto = new CtrlCustPhoto(context);
        ctrlAppModul = new CtrlAppModul(context);
        server = new ConnectionServer(context);
        server.addConnectionEvent(this);
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_photo_customer, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);
        btn_photo = (ImageButton) view.findViewById(R.id.btn_photo);
        btn_send = (ImageButton) view.findViewById(R.id.btn_send);
        btn_hapus = (ImageButton) view.findViewById(R.id.btn_hapus);
        photo_type = (Spinner) view.findViewById(R.id.photo_type);
        photo_type.setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("PHOTO_TYPE")));
//		image = (TextView) view.findViewById(R.id.image);
        image = (ImageViewTouch) view.findViewById(R.id.image);
        image.setDisplayType(DisplayType.FIT_IF_BIGGER);
        btn_photo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });
        btn_send.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        });
        btn_hapus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.delete(ItemPhotoCustomerWidget.this);
                }
            }
        });
        AppModul modul = ctrlAppModul.get("51");
        if (modul != null) {
            if (modul.AppModulValue.equals("2")) {
                btn_send.setVisibility(View.VISIBLE);
            }
        }
        addView(view);
    }

    public void takePicture() {
        if (listener != null) {
            listener.takePicture(this);
        }
    }

    public boolean isAmbilPhoto() {
        return (data != null);
    }

    public JSONObject getJsonSendData() {
        try {
            JSONObject json = new JSONObject();
            json.put("SalesID", UserModel.getInstance(context).SalesId);
            json.put("CustomerID", customerModel.CustId);
            json.put("PhotoTypeID", ((ComboBoxModel) photo_type.getSelectedItem()).value);
            json.put("PhotoPic", Base64.encodeBytes(data));
            json.put("PhotoCat", 0);
            json.put("RandomID", customerModel.RandomID);
            return json;
        } catch (Exception e) {
            return null;
        }
    }

    public void sendData() {
        try {
            JSONObject json = getJsonSendData();
            server.setUrl(Util.getServerUrl(context) + "photodata");
            server.requestJSONObjectNoTimeOut(json);
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    @Override
    public void error(String error) {
        saveLokal();
        Util.Toast(context, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.has("Result") && json.getString("Result").equals("INSERT_OK")) {
                data = null;
                if (listener != null) {
                    listener.send(this);
                }
            } else {
                saveLokal();
            }
            Util.Toast(context, json.getString("Message"));
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

    private void saveLokal() {
        String nmFile = customerModel.CustId + "_" + ((ComboBoxModel) photo_type.getSelectedItem()).value + ".jpg";
        if (data != null) {
            Util.saveFileImg(data, nmFile);
            data = null;
        }
        ctrlCustPhoto.save(new CustPhotoModel(
                UserModel.getInstance(context).SalesId,
                customerModel.CustId,
                ((ComboBoxModel) photo_type.getSelectedItem()).value,
                nmFile,
                "0",
                customerModel.RandomID
        ));
        Util.Toast(context, "Data tersimpan dilokal");
        if (listener != null) {
            listener.send(this);
        }
    }

    public void saveKeLokal() {
        String nmFile = customerModel.CustId + "_" + ((ComboBoxModel) photo_type.getSelectedItem()).value + ".jpg";
        if (data != null) {
            Util.saveFileImg(data, nmFile);
            data = null;
        }
        ctrlCustPhoto.save(new CustPhotoModel(
                UserModel.getInstance(context).SalesId,
                customerModel.CustId,
                ((ComboBoxModel) photo_type.getSelectedItem()).value,
                nmFile,
                "0",
                customerModel.RandomID
        ));
    }
}
