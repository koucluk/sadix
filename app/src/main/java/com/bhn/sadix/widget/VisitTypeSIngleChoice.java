package com.bhn.sadix.widget;

import java.util.List;
import java.util.UUID;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxJsonAdapter;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.Util;

public class VisitTypeSIngleChoice extends LinearLayout {
    private VisitNote visitNote;
    private DbMasterHelper db;
    private TextView label;
    private Spinner spiner;
    private String RandomID;
    private String CommonID;

    public VisitTypeSIngleChoice(Context context, VisitNote visitNote, String CommonID) {
        super(context);
        this.visitNote = visitNote;
        db = new DbMasterHelper(context);
        RandomID = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.visit_type_single_choise, null);
        label = (TextView) view.findViewById(R.id.label);
        spiner = (Spinner) view.findViewById(R.id.spiner);
        try {
            List<JSONObject> items = db.queryListJSON("select * from visit_item where VisitID='" + visitNote.getVisitID() + "'");
            ComboBoxJsonAdapter adapter = new ComboBoxJsonAdapter(getContext(), items, "ItemValue");
            spiner.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);

        try {
            label.setText(visitNote.getVisitName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    public JSONObject getValue() {
        JSONObject json = new JSONObject();
        try {
            JSONObject pilih = (JSONObject) spiner.getSelectedItem();
            json.put("VisitId", visitNote.getVisitID());
            if (pilih != null) {
                json.put("ItemId", pilih.getString("ItemID"));
                json.put("ItemValue", pilih.getString("ItemValue"));
            }
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public boolean isValid() {
        try {
            int ReqField = visitNote.getReqField();
            if (ReqField == 1) {
                JSONObject pilih = (JSONObject) spiner.getSelectedItem();
                if (pilih != null) {
                    return true;
                } else {
                    Util.showDialogInfo(getContext(), visitNote.getVisitName() + " belum dipilih!");
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getContext(), "" + e.getMessage());
            return false;
        }
    }

}
