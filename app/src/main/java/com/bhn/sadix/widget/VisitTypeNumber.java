package com.bhn.sadix.widget;

import java.util.UUID;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.Util;

public class VisitTypeNumber extends LinearLayout {
    private VisitNote visitNote;
    private JSONObject visitItem;
    private DbMasterHelper db;
    private TextView label;
    private EditText text;
    private String RandomID;
    private String CommonID;

    public VisitTypeNumber(Context context, VisitNote visitNote, String CommonID) {
        super(context);
        this.visitNote = visitNote;
        db = new DbMasterHelper(context);
        RandomID = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        try {
            visitItem = db.queryJSON("select * from visit_item where VisitID='" + visitNote.getVisitID() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.visit_type_number, null);
        label = (TextView) view.findViewById(R.id.label);
        text = (EditText) view.findViewById(R.id.text);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);

        try {
            label.setText(visitNote.getVisitName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    public JSONObject getValue() {
        JSONObject json = new JSONObject();
        try {
            json.put("VisitId", visitNote.getVisitID());
            json.put("ItemId", visitItem.getString("ItemID"));
            json.put("ItemValue", text.getText().toString());
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public boolean isValid() {
        try {
            int ReqField = visitNote.getReqField();
            if (ReqField == 1) {
                String value = text.getText().toString();
                if (!"".equals(value)) {
                    return true;
                } else {
                    Util.showDialogInfo(getContext(), visitNote.getVisitName() + " belum diisi!");
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getContext(), "" + e.getMessage());
            return false;
        }
    }

}
