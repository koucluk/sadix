package com.bhn.sadix.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.model.InformasiModel;

public class InformationWidget extends LinearLayout {
	private LayoutInflater mInflater;
	private Context context;
	private InformasiModel informasiModel;

	public InformationWidget(Context context) {
		super(context);
		this.context = context;
		init();
	}
	public InformationWidget(Context context, InformasiModel informasiModel) {
		super(context);
		this.context = context;
		this.informasiModel = informasiModel;
		init();
	}
	public InformationWidget(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	public InformationWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	private void init() {
		ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		);
		setLayoutParams(param);
		
		mInflater = LayoutInflater.from(context);
		LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_fragment_kunjungan_informasi, null);
		ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		);
		view.setLayoutParams(param_view);
		addView(view);
		viewInfo();
	}
	public void setInformasiModel(InformasiModel informasiModel) {
		this.informasiModel = informasiModel;
		viewInfo();
	}
	private void viewInfo() {
		if(informasiModel != null) {
			((TextView)findViewById(R.id.Header)).setText(informasiModel.Header);
			((TextView)findViewById(R.id.DateValidity)).setText("Berlaku : " + informasiModel.DateValidity);
			((TextView)findViewById(R.id.DateValidity)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.Content)).setText(informasiModel.Content.replaceAll("#13", "\n"));
		}
	}
}
