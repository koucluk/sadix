package com.bhn.sadix.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbAsetHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AssetServiceWidget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private DbAsetHelper dbAsetHelper;
    private String ASETNAME;
    private String ASETTYPE;
    private String ASETID;
    private AssetServiceWidgetListener listener;
    private List<AssetDetailWidget> listWidget;
    private String custId;
    private String ASETNO;

    private String CommonID;

    public AssetServiceWidget(Context context, String ASETNAME, String ASETTYPE, String ASETID, AssetServiceWidgetListener listener, String custId, String ASETNO, String CommonID) {
        super(context);
        this.context = context;
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.ASETID = ASETID;
        this.listener = listener;
        this.custId = custId;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        dbAsetHelper = new DbAsetHelper(this.context);
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_asset_service_widget, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.ASETNAME)).setText(ASETNAME);
            if (listWidget == null) {
                listWidget = new ArrayList<>();
                String sql = "select GROUP_ASET,TYPE_ASET from ASSET_MAINTENANCE where TYPE_ASET=" + ASETTYPE + " and POSISI=3 group by GROUP_ASET,TYPE_ASET";
                List<JSONObject> list = dbAsetHelper.list(sql);
                for (JSONObject jsonObject : list) {
                    AssetDetailWidget assetDtl = new AssetDetailWidget(context, jsonObject, "3", ASETID, custId, ASETNO, CommonID, 4) {
                        @Override
                        public void photoType4(AssetType4Widget type4) {
                            if (listener != null) {
                                listener.photoType4(type4);
                            }
                        }

                        @Override
                        public void scanType6(AssetType6Widget type6) {
                            if (listener != null) {
                                listener.scanType6(type6);
                            }
                        }
                    };
                    ((LinearLayout) view.findViewById(R.id.item_layout)).addView(assetDtl);
                    listWidget.add(assetDtl);
                }
            } else {
                for (AssetDetailWidget assetDtl : listWidget) {
                    ((LinearLayout) view.findViewById(R.id.item_layout)).addView(assetDtl);
                }
            }
        } catch (Exception e) {
            android.util.Log.e("Exception_AssetView", "" + e.getMessage());
        }
        view.setLayoutParams(param_view);
        addView(view);
    }

    public List<AssetDetailWidget> getListWidget() {
        return listWidget;
    }

    public interface AssetServiceWidgetListener {
        public void photoType4(AssetType4Widget type4);

        public void scanType6(AssetType6Widget type6);
    }
}
