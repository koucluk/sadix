package com.bhn.sadix.widget;

public interface ItemPhotoCustomerListener {
	public void takePicture(ItemPhotoCustomerWidget widget);
	public void send(ItemPhotoCustomerWidget widget);
	public void delete(ItemPhotoCustomerWidget widget);
}
