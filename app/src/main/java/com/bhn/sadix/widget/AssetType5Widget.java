package com.bhn.sadix.widget;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;

public class AssetType5Widget extends RadioButton {
    private JSONObject json;
    private String ASETID;
    private String ASETNO;
    private DatabaseHelper db;
    private String CommonID;
    private String RandomID;
    private AssetModel assetModel;
    private int tipe;
    private int Index;
    Realm realmUI;

    public AssetType5Widget(int index, Context context, JSONObject json, String ASETID, String ASETNO, String CommonID, int tipe) {
        super(context);
        Index = index;
        RandomID = UUID.randomUUID().toString();
        this.json = json;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        this.tipe = tipe;
        db = new DatabaseHelper(context);
        realmUI = Realm.getDefaultInstance();
        init();
    }

    public AssetType5Widget(Context context, JSONObject json, String ASETID, String ASETNO, String CommonID, int tipe) {
        super(context);
        RandomID = UUID.randomUUID().toString();
        this.json = json;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        this.tipe = tipe;
        db = new DatabaseHelper(context);
        realmUI = Realm.getDefaultInstance();
        init();
    }

    private void init() {
        try {
            if ((assetModel = realmUI.where(AssetModel.class)
                    .equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX"))
                    .findFirst()) == null) {

                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel commonModel = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();
                        assetModel = realm.createObject(AssetModel.class, RandomID);
                        commonModel.assetModel.add(assetModel);
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    assetModel.CommonID = CommonID;
                    assetModel.Idx = json.getInt("IDX");
                    assetModel.Name = json.getString("NAME");
                    assetModel.GroupAset = json.getString("GROUP_ASET");
                    assetModel.TypeMnt = json.getInt("TYPE_MNT");
                    assetModel.Posisi = json.getInt("POSISI");
                    assetModel.AsetID = ASETNO;
                    assetModel.JenisWidget = 5;
                    assetModel.tipe = tipe;
//                    assetModel.RandomID = RandomID;
//            model.NameFile = fileName + ".jpg";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assetModel = realm.copyToRealmOrUpdate(assetModel);
            }
        });

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);
        try {
            setText(json.getString("NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            /*AssetModel asset;
            asset = realmUI.where(AssetModel.class).equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX")).findFirst();*/
            /*if (asset != null) {
                if (asset.Value.equals("1")) {
                    setChecked(true);
                } else if (asset.Value.equals("0")) {
                    setChecked(false);
                }
            }*/
//            setC
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCheck(final boolean check) {
        Log.d("Checked", "Changed");
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (check) {
                    Log.d("Checked", "Changed true");
                    assetModel.Value = "1";
//                    setCheck(true);
                } else {
                    Log.d("Checked", "Changed false");
                    assetModel.Value = "0";
//                    setCheck(true);
                }
            }
        });
    }

    public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
//			rjson.put("AsetID", ASETID);
            rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("Value", isChecked() ? "1" : "0");
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }
}
