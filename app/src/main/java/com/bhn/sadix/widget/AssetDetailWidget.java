package com.bhn.sadix.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.DbAsetHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public abstract class AssetDetailWidget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private JSONObject json;
    private DbAsetHelper dbAsetHelper;
    private String POSISI;
    private String ASETID;
    private String custId;
    private String ASETNO;
    private int type;
    private String CommonID;

    public AssetDetailWidget(Context context, JSONObject json, String POSISI, String ASETID, String custId, String ASETNO, String CommonID, int type) {
        super(context);
        this.context = context;
        this.json = json;
        dbAsetHelper = new DbAsetHelper(this.context);
        this.POSISI = POSISI;
        this.ASETID = ASETID;
        this.custId = custId;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        this.type = type;
        init();
    }

    public int getPosisi() {
        return Integer.parseInt(POSISI);
    }

    @SuppressLint("LongLogTag")
    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_fragment_kunjungan_asset_detail, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.GROUP_ASET)).setText(json.getString("GROUP_ASET"));
            String sql = "select * from ASSET_MAINTENANCE where TYPE_ASET='" + json.getString("TYPE_ASET") + "' and POSISI=" + POSISI + " and GROUP_ASET='" + json.getString("GROUP_ASET") + "' and TYPE_MNT<>'5'";
            List<JSONObject> list = dbAsetHelper.list(sql);
            if (list.size() > 0) {
                view.findViewById(R.id.layout_detail).setVisibility(View.VISIBLE);
                for (JSONObject jsonObject : list) {
                    if (jsonObject.getString("TYPE_MNT").equals("1")) {
                        ((LinearLayout) view.findViewById(R.id.layout_detail)).addView(new AssetType1Widget(context, jsonObject, ASETID, ASETNO, CommonID, type));
                    } else if (jsonObject.getString("TYPE_MNT").equals("2")) {
                        ((LinearLayout) view.findViewById(R.id.layout_detail)).addView(new AssetType2Widget(context, jsonObject, ASETID, ASETNO, CommonID, type));
                    } else if (jsonObject.getString("TYPE_MNT").equals("3")) {
                        ((LinearLayout) view.findViewById(R.id.layout_detail)).addView(new AssetType3Widget(context, jsonObject, ASETID, ASETNO, CommonID, type));
                    } else if (jsonObject.getString("TYPE_MNT").equals("4")) {
                        ((LinearLayout) view.findViewById(R.id.layout_detail)).addView(new AssetType4Widget(context, jsonObject, ASETID, custId, ASETNO, CommonID, type) {
                            @Override
                            public void photo(AssetType4Widget type4) {
                                photoType4(type4);
                            }
                        });
                    } else if (jsonObject.getString("TYPE_MNT").equals("6")) {
                        ((LinearLayout) view.findViewById(R.id.layout_detail)).addView(new AssetType6Widget(context, jsonObject, ASETID, ASETNO, CommonID, type) {
                            @Override
                            public void scan(AssetType6Widget type6) {
                                scanType6(type6);
                            }
                        });
                    }
                }
            } else {
                sql = "select * from ASSET_MAINTENANCE where TYPE_ASET='" + json.getString("TYPE_ASET") + "' and POSISI=" + POSISI + " and GROUP_ASET='" + json.getString("GROUP_ASET") + "' and TYPE_MNT='5'";
                list = dbAsetHelper.list(sql);
                ArrayList<RadioButton> buttons = new ArrayList<>();

                if (list.size() > 0) {

                    final RadioGroup group = (RadioGroup) view.findViewById(R.id.radioGroup);
                    group.setVisibility(View.VISIBLE);

                    for (JSONObject obj : list) {
                        final AssetType5Widget button = new AssetType5Widget(context, obj, ASETID, ASETNO, CommonID, type);

                        button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    button.setCheck(true);
                                } else {
                                    button.setCheck(false);
                                }
                            }
                        });
                        /*((RadioGroup) view.findViewById(R.id.radioGroup))*/
                        group.addView(button);
                    }

                    Realm realm = Realm.getDefaultInstance();

                    int index = 0;
                    for (JSONObject obj : list) {
                        AssetModel asset;
                        asset = realm.where(AssetModel.class).equalTo("CommonID", CommonID)
                                .equalTo("Idx", obj.getInt("IDX")).findFirst();
                        try {
                            if (asset != null) {
                                if (asset.Value != null) {
                                    if (asset.Value.equals("1")) {
                                        group.check(group.getChildAt(index).getId());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        index += 1;
                    }

                    realm.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e("Exception_AssetDetailWidget", "" + e.getMessage());
        }

        view.setLayoutParams(param_view);
        addView(view);
    }

    public boolean isPhoto() {
        boolean isPhoto = true;
        LinearLayout linear = ((LinearLayout) findViewById(R.id.layout_detail));
        for (int i = 0; i < linear.getChildCount(); i++) {
            if (linear.getChildAt(i) instanceof AssetType4Widget) {
                AssetType4Widget widget = (AssetType4Widget) linear.getChildAt(i);
                if (!widget.isPhoto()) {
                    isPhoto = false;
                    break;
                }
            }
        }
        return isPhoto;
    }

    public boolean isType5() {
        return (findViewById(R.id.radioGroup).getVisibility() == View.VISIBLE);
    }

    public abstract void photoType4(AssetType4Widget type4);

    public abstract void scanType6(AssetType6Widget type6);
}
