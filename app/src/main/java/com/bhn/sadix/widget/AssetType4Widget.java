package com.bhn.sadix.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;

public abstract class AssetType4Widget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private JSONObject json;
    private String ASETID;
    private ImageView image;
    private byte[] data;
    private String custId;
    private String ASETNO;
    private int tipe;
    private String fileName;
    private DatabaseHelper db;
    private String CommonID;
    private String RandomID;
    private AssetModel assetModel;
    Realm realmUI;

    public AssetType4Widget(Context context, JSONObject json, String ASETID, String custId, String ASETNO, String CommonID, int tipe) {
        super(context);
        RandomID = UUID.randomUUID().toString();
        this.context = context;
        this.json = json;
        this.ASETID = ASETID;
        this.custId = custId;
        this.ASETNO = ASETNO;
        this.tipe = tipe;
        fileName = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        db = new DatabaseHelper(this.context);
        realmUI = Realm.getDefaultInstance();
        init();
    }

    private void init() {
        try {
            if ((assetModel = realmUI.where(AssetModel.class)
                    .equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX"))
                    .findFirst()) == null) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel commonModel = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();
                        assetModel = realm.createObject(AssetModel.class, RandomID);
                        commonModel.assetModel.add(assetModel);
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    assetModel.AsetID = ASETNO;
                    assetModel.CommonID = CommonID;
                    assetModel.Idx = json.getInt("IDX");
                    assetModel.Name = json.getString("NAME");
                    assetModel.GroupAset = json.getString("GROUP_ASET");
                    assetModel.TypeMnt = json.getInt("TYPE_MNT");
                    assetModel.Posisi = json.getInt("POSISI");
                    assetModel.JenisWidget = 4;
                    assetModel.tipe = tipe;
//                    assetModel.RandomID = RandomID;
//            model.NameFile = fileName + ".jpg";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assetModel = realm.copyToRealmOrUpdate(assetModel);
            }
        });

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_type_asset4, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.text)).setText(json.getString("NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.setLayoutParams(param_view);
        image = (ImageView) view.findViewById(R.id.image);
        view.findViewById(R.id.photo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                photo(AssetType4Widget.this);
            }
        });
        try {
//            JSONObject dt = db.getMerchandiser("Aset", CommonID, json.getString("IDX"));
            AssetModel asset = realmUI.where(AssetModel.class).equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX")).findFirst();
            if (asset != null) {
                data = Util.readFile(Util.LOKASI_IMAGE + asset.NameFile);
                if (data != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    if (bitmap != null) {
                        image.setImageBitmap(bitmap);
                        image.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        realmUI.close();
        addView(view);
    }

    public abstract void photo(AssetType4Widget type4);

    public void setBitmap(Bitmap bitmap, byte[] data) {
        this.data = data;
        if (bitmap != null) {
            image.setImageBitmap(bitmap);
            image.setVisibility(View.VISIBLE);
        }
        saveFile();
    }

    /*public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
//			android.util.Log.e("fileName", ""+Util.LOKASI_IMAGE+fileName+".jpg");
            if (data == null) {
                data = Util.readFile(Util.LOKASI_IMAGE + fileName + ".jpg");
            }
            android.util.Log.e("data", "" + data.length);
//			rjson.put("AsetID", ASETID);
            rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("Value", Base64.encodeBytes(data));
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
//			android.util.Log.e("rjson", ""+rjson.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }*/

    /*public JSONObject toSaveJSON() {
        JSONObject rjson = new JSONObject();
        try {
//			rjson.put("AsetID", ASETID);
            rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("namaFile", fileName + ".jpg");
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }*/

    public void saveFile() {
        final String nmFile = fileName + ".jpg";
        /*if (realmUI.isClosed()) {
            realmUI = Realm.getDefaultInstance();
        }*/

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                assetModel.NameFile = nmFile;
                assetModel.Value = Base64.encodeBytes(data);
            }
        });

//        realmUI.close();
        if (data != null) {
            Util.saveFileImg(data, nmFile);
            data = null;
        }
    }

    public boolean isPhoto() {
        if (data == null) {
            try {
                data = Util.readFile(Util.LOKASI_IMAGE + fileName + ".jpg");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (data != null);
    }

}
