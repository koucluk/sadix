package com.bhn.sadix.widget;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.Util;

public class VisitTypeCalendarDate extends LinearLayout {
    private VisitNote visitNote;
    private JSONObject visitItem;
    private DbMasterHelper db;
    private TextView label;
    private EditText date;
    private ImageButton btn;
    private String RandomID;
    private String CommonID;

    public VisitTypeCalendarDate(Context context, VisitNote visitNote, String CommonID) {
        super(context);
        this.visitNote = visitNote;
        db = new DbMasterHelper(context);
        RandomID = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        try {
            visitItem = db.queryJSON("select * from visit_item where VisitID='" + visitNote.getVisitID() + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.visit_type_calendar_date, null);
        label = (TextView) view.findViewById(R.id.label);
        date = (EditText) view.findViewById(R.id.date);
        btn = (ImageButton) view.findViewById(R.id.btn);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tanggal();
            }
        });

        try {
            label.setText(visitNote.getVisitName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    private void tanggal() {
        try {
            final Calendar cal = Calendar.getInstance();
            if (!this.date.getText().toString().equals("YYYY-MM-DD")) {
                Date date = Util.DATE_FORMAT_YYYMMDD.parse(this.date.getText().toString());
                cal.setTime(date);
            }
            final DatePickerDialog tglDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    Calendar calData = Calendar.getInstance();
                    calData.set(year, month, day);
                    date.setText(Util.DATE_FORMAT_YYYMMDD.format(calData.getTime()));
                }
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            tglDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject getValue() {
        JSONObject json = new JSONObject();
        try {
            json.put("VisitId", visitNote.getVisitID());
            json.put("ItemId", visitItem.getString("ItemID"));
            json.put("ItemValue", date.getText().toString());
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public boolean isValid() {
        try {
            int ReqField = visitNote.getReqField();
            if (ReqField == 1) {
                String value = date.getText().toString();
                if (!"YYYY-MM-DD".equals(value)) {
                    return true;
                } else {
                    Util.showDialogInfo(getContext(), visitNote.getVisitName() + " belum diisi!");
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getContext(), "" + e.getMessage());
            return false;
        }
    }

}
