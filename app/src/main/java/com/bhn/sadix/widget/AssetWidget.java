package com.bhn.sadix.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ViewPagerAdapter;
import com.bhn.sadix.adapter.ViewPagerAdapter.PageItem;
import com.bhn.sadix.database.DbAsetHelper;
import com.makeramen.segmented.SegmentedRadioGroup;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AssetWidget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private ViewPagerAdapter adapter;
    private DbAsetHelper dbAsetHelper;
    private String ASETNAME;
    private String ASETTYPE;
    private String ASETID;
    private AssetWidgetListener listener;
    private String custId;
    private String ASETNO;
    private int type;

    private String CommonID;

    public AssetWidget(Context context, String ASETNAME, String ASETTYPE, String ASETID, AssetWidgetListener listener, String custId, String ASETNO, String CommonID, int type) {
        super(context);
        this.type = type;
        this.context = context;
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.ASETID = ASETID;
        this.listener = listener;
        this.custId = custId;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        dbAsetHelper = new DbAsetHelper(this.context);
        init();
    }

    public void setStatus(String status) {
        View view = findViewById(R.id.tab_status);
        if (view != null && view instanceof RadioButton) {
            RadioButton rb = (RadioButton) view;
            rb.setText(status);
        }
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_fragment_kunjungan_asset, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.ASETNAME)).setText(ASETNAME);
        } catch (Exception e) {
        }

        ((SegmentedRadioGroup) view.findViewById(R.id.tab)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.tab_befor) {
                    ((ViewPager) findViewById(R.id.pager)).setCurrentItem(0);
                } else if (checkedId == R.id.tab_after) {
                    ((ViewPager) findViewById(R.id.pager)).setCurrentItem(1);
                } else if (checkedId == R.id.tab_status) {
                    ((ViewPager) findViewById(R.id.pager)).setCurrentItem(2);
                }
            }
        });
        view.findViewById(R.id.tab_befor).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewPager) findViewById(R.id.pager)).setCurrentItem(0);
            }
        });
        view.findViewById(R.id.tab_after).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewPager) findViewById(R.id.pager)).setCurrentItem(1);
            }
        });
        view.findViewById(R.id.tab_status).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewPager) findViewById(R.id.pager)).setCurrentItem(2);
            }
        });

        List<PageItem> list = new ArrayList<ViewPagerAdapter.PageItem>();
        list.add(new AssetView("0"));
        list.add(new AssetView("1"));
        list.add(new AssetView("2"));
        adapter = new ViewPagerAdapter(context, list);

        ((ViewPager) view.findViewById(R.id.pager)).setAdapter(adapter);
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == 0) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_befor);
                } else if (position == 1) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_after);
                } else if (position == 2) {
                    ((SegmentedRadioGroup) findViewById(R.id.tab)).check(R.id.tab_status);
                }
            }
        };
        ((ViewPager) view.findViewById(R.id.pager)).setOnPageChangeListener(ViewPagerListener);
        view.setLayoutParams(param_view);
        addView(view);
    }

    public ViewPagerAdapter getAdapter() {
        return adapter;
    }

    public class AssetView implements ViewPagerAdapter.PageItem {
        private View view;
        private String POSISI;
        private List<AssetDetailWidget> listWidget;

        public AssetView(String pOSISI) {
            POSISI = pOSISI;
        }

        @Override
        public int getLayout() {
            return R.layout.item_asset_widget;
        }

        @Override
        public void onStart() {
            try {
                if (listWidget == null) {
                    listWidget = new ArrayList<AssetDetailWidget>();
                    String sql = "select GROUP_ASET,TYPE_ASET from ASSET_MAINTENANCE where TYPE_ASET=" + ASETTYPE + " and POSISI=" + POSISI + " group by GROUP_ASET,TYPE_ASET";
                    List<JSONObject> list = dbAsetHelper.list(sql);
                    for (JSONObject jsonObject : list) {
                        AssetDetailWidget assetDtl = new AssetDetailWidget(context, jsonObject, POSISI, ASETID, custId, ASETNO, CommonID, type) {
                            @Override
                            public void photoType4(AssetType4Widget type4) {
                                if (listener != null) {
                                    listener.photoType4(type4);
                                }
                            }

                            @Override
                            public void scanType6(AssetType6Widget type6) {
                                if (listener != null) {
                                    listener.scanType6(type6);
                                }
                            }
                        };
                        ((LinearLayout) view.findViewById(R.id.item_layout)).addView(assetDtl);
                        listWidget.add(assetDtl);
                    }
                } else {
                    for (AssetDetailWidget assetDtl : listWidget) {
                        ((LinearLayout) view.findViewById(R.id.item_layout)).addView(assetDtl);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                android.util.Log.e("Exception_AssetView", "" + e.getMessage());
            }
        }

        @Override
        public void onPause() {
            ((LinearLayout) view.findViewById(R.id.item_layout)).removeAllViews();
        }

        @Override
        public void setView(View view) {
            this.view = view;
        }

        @Override
        public View getView() {
            return view;
        }

        public List<AssetDetailWidget> getListWidget() {
            return listWidget;
        }
    }

    public interface AssetWidgetListener {
        public void photoType4(AssetType4Widget type4);

        public void scanType6(AssetType6Widget type6);
    }
}
