package com.bhn.sadix.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.R;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.model.VisitNote;
import com.bhn.sadix.util.Util;

public class VisitTypeMultipleChoice extends LinearLayout {
    private VisitNote visitNote;
    private DbMasterHelper db;
    private TextView label;
    private LinearLayout multiple;
    private String RandomID;
    private String CommonID;
    private List<JSONObject> items;

    public VisitTypeMultipleChoice(Context context, VisitNote visitNote, String CommonID) {
        super(context);
        this.visitNote = visitNote;
        db = new DbMasterHelper(context);
        RandomID = UUID.randomUUID().toString();
        this.CommonID = CommonID;
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        LayoutInflater mInflater = LayoutInflater.from(getContext());
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.visit_type_multiple_choise, null);
        label = (TextView) view.findViewById(R.id.label);
        multiple = (LinearLayout) view.findViewById(R.id.multiple);
        try {
            multiple.removeAllViews();
            items = db.queryListJSON("select * from visit_item where VisitID='" + visitNote.getVisitID() + "'");
            for (JSONObject json : items) {
                CheckBox checkBox = new CheckBox(getContext());
                checkBox.setText(json.getString("ItemValue"));
                checkBox.setId(json.getInt("ItemID"));
                multiple.addView(checkBox);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);

        try {
            label.setText(visitNote.getVisitName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    public List<JSONObject> getValue() {
        List<JSONObject> list = new ArrayList<JSONObject>();
        try {
            for (int i = 0; i < multiple.getChildCount(); i++) {
                View view = multiple.getChildAt(i);
                if (view instanceof CheckBox) {
                    CheckBox ck = (CheckBox) view;
                    if (ck.isChecked()) {
                        JSONObject json = new JSONObject();
                        json.put("VisitId", visitNote.getVisitID());
                        json.put("ItemId", ck.getId());
                        json.put("ItemValue", ck.getText().toString());
                        json.put("CommonID", CommonID);
                        json.put("RandomID", RandomID);
                        list.add(json);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean isValid() {
        try {
            int ReqField = visitNote.getReqField();
            if (ReqField == 1) {
                boolean diPilih = false;
                for (int i = 0; i < multiple.getChildCount(); i++) {
                    View view = multiple.getChildAt(i);
                    if (view instanceof CheckBox) {
                        CheckBox ck = (CheckBox) view;
                        if (ck.isChecked()) {
                            diPilih = true;
                        }
                    }
                }
                if (diPilih) {
                    return true;
                } else {
                    Util.showDialogInfo(getContext(), visitNote.getVisitName() + " harus pilih salah satu!");
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showDialogError(getContext(), "" + e.getMessage());
            return false;
        }
    }

}
