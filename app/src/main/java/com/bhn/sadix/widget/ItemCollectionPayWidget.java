package com.bhn.sadix.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.CollectorModel;
import com.bhn.sadix.model.ComboBoxModel;

public class ItemCollectionPayWidget extends LinearLayout {
	private LayoutInflater mInflater;
	private Context context;
	private CollectorModel model;
	private CtrlConfig ctrlConfig;

	public ItemCollectionPayWidget(Context context, CollectorModel model) {
		super(context);
		this.context = context;
		this.model = model;
		ctrlConfig = new CtrlConfig(context);
		init();
	}
	private void init() {
		ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.WRAP_CONTENT
		);
		setLayoutParams(param);
		
		mInflater = LayoutInflater.from(context);
		LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.fragment_collection_pay, null);
		ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
			ViewGroup.LayoutParams.MATCH_PARENT,
			ViewGroup.LayoutParams.WRAP_CONTENT
		);
		view.setLayoutParams(param_view);
		
		((Spinner)view.findViewById(R.id.data2)).setAdapter(new ComboBoxAdapter(context, ctrlConfig.listComboBox("PAYMENT_METHOD")));
		((EditText)view.findViewById(R.id.data1)).setText(model.data1);
		int index = ((ComboBoxAdapter)((Spinner)view.findViewById(R.id.data2)).getAdapter()).indexOf(new ComboBoxModel(model.data2));
		((Spinner)view.findViewById(R.id.data2)).setSelection(index);
		((EditText)view.findViewById(R.id.data3)).setText(model.data3);
		
		addView(view);
	}
	public void setData() {
		model.data1 = ((EditText)findViewById(R.id.data1)).getText().toString();
		model.data2 = ((ComboBoxModel)((Spinner)findViewById(R.id.data2)).getSelectedItem()).value;
		model.data3 = ((EditText)findViewById(R.id.data3)).getText().toString();
	}
	public CollectorModel getModel() {
		return model;
	}
}
