package com.bhn.sadix.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhn.sadix.Data.AssetModel;
import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.realm.Realm;

public abstract class AssetType6Widget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    private JSONObject json;
    private String ASETID;
    private String ASETNO;
    private DatabaseHelper db;
    private String CommonID;
    private String RandomID;
    private Realm realmUI;
    private AssetModel assetModel;
    private int tipe;

    public AssetType6Widget(Context context, JSONObject json, String ASETID, String ASETNO, String CommonID, int tipe) {
        super(context);
        RandomID = UUID.randomUUID().toString();
        this.context = context;
        this.json = json;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        this.CommonID = CommonID;
        this.tipe = tipe;
        realmUI = Realm.getDefaultInstance();
        db = new DatabaseHelper(this.context);
        init();
    }

    private void init() {
        assetModel = null;
        try {
            if ((assetModel = realmUI.where(AssetModel.class)
                    .equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getString("IDX"))
                    .findFirst()) == null) {
                realmUI.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        CommonModel commonModel = realmUI.where(CommonModel.class).equalTo("CommonID", CommonID).findFirst();
                        assetModel = realm.createObject(AssetModel.class, RandomID);
                        commonModel.assetModel.add(assetModel);
                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    assetModel.AsetID = ASETNO;
                    assetModel.CommonID = CommonID;
                    assetModel.Idx = json.getInt("IDX");
                    assetModel.Name = json.getString("NAME");
                    assetModel.GroupAset = json.getString("GROUP_ASET");
                    assetModel.TypeMnt = json.getInt("TYPE_MNT");
                    assetModel.Posisi = json.getInt("POSISI");
                    assetModel.JenisWidget = 6;
                    assetModel.tipe = tipe;
//                    assetModel.RandomID = RandomID;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                assetModel = realm.copyToRealmOrUpdate(assetModel);
            }
        });

        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.item_type_asset6, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        try {
            ((TextView) view.findViewById(R.id.text)).setText(json.getString("NAME"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.setLayoutParams(param_view);
        ((Button) view.findViewById(R.id.btn_scan)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scan(AssetType6Widget.this);
            }
        });
        try {
            AssetModel asset = realmUI.where(AssetModel.class).equalTo("CommonID", CommonID)
                    .equalTo("Idx", json.getInt("IDX")).findFirst();
            if (asset != null) {
                ((EditText) view.findViewById(R.id.barcode_text)).setText(asset.Value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        addView(view);
    }

    public abstract void scan(AssetType6Widget type6);

    /*public JSONObject toJSON() {
        JSONObject rjson = new JSONObject();
        try {
            rjson.put("AsetID", ASETID);
//			rjson.put("AsetID", ASETNO);
            rjson.put("Idx", json.getInt("IDX"));
            rjson.put("Name", json.getString("NAME"));
            rjson.put("GroupAset", json.getString("GROUP_ASET"));
            rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
            rjson.put("Posisi", json.getInt("POSISI"));
            rjson.put("CommonID", CommonID);
            rjson.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rjson;
    }*/

    //	public JSONObject toSaveJSON() {
//		JSONObject rjson = new JSONObject();
//		try {
////			rjson.put("AsetID", ASETID);
//			rjson.put("AsetID", ASETNO);
//			rjson.put("Idx", json.getInt("IDX"));
//			rjson.put("Name", json.getString("NAME"));
//			rjson.put("GroupAset", json.getString("GROUP_ASET"));
//			rjson.put("TypeMnt", json.getInt("TYPE_MNT"));
//			rjson.put("Posisi", json.getInt("POSISI"));
//			rjson.put("CommonID", CommonID);
//			rjson.put("RandomID", RandomID);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return rjson;
//	}
    public void setScanText(String code) {
        ((EditText) findViewById(R.id.barcode_text)).setText(code);
        realmUI.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                assetModel.Value = ((EditText) findViewById(R.id.barcode_text)).getText().toString();
            }
        });

    }
}
