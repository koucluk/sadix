package com.bhn.sadix.widget;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

/**
 * Created by map05 on 3/2/2017.
 */

public class XValueFormatter implements IAxisValueFormatter {

    private final List<String> labels;

    public XValueFormatter(List<String> label) {
        labels = label;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int val = (int) (value / 10f) - 1;

        return val >= 0 ? labels.get(val) : "";
    }
}
