package com.bhn.sadix;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import com.bhn.sadix.Data.ReturnModel;
import com.bhn.sadix.adapter.ResumeOrderAdapter;
import com.bhn.sadix.database.CtrlCustomer;
import com.bhn.sadix.database.CtrlRetur;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.ResumeOrderDetailModul;
import com.bhn.sadix.model.ResumeOrderModel;
import com.bhn.sadix.model.SKUModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class SummeryReturnActivity extends AppCompatActivity {
    private CtrlRetur ctrlRetur;
    private ProgressDialog progressDialog;
    private ResumeOrderAdapter adapter;
    private Realm realm;
    private Toolbar toolbar;
    private CtrlSKU ctrlSKU;
    private CtrlCustomer ctrlCustomer;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_order);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        ctrlRetur = new CtrlRetur(this);
        ctrlSKU = new CtrlSKU(this);
        ctrlCustomer = new CtrlCustomer(this);

        realm = Realm.getDefaultInstance();

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        findViewById(R.id.orderBy).setVisibility(View.GONE);
        ctrlRetur = new CtrlRetur(this);
        progressDialog = new ProgressDialog(this) {
            @Override
            public boolean onKeyDown(int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
                    dismiss();
                }
                return super.onKeyDown(keyCode, event);
            }

        };
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
        new LoadData().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                Realm realm = Realm.getDefaultInstance();
                adapter = new ResumeOrderAdapter(SummeryReturnActivity.this, new ArrayList<ResumeOrderModel>(), new HashMap<ResumeOrderModel, List<ResumeOrderDetailModul>>());

//                "select SKUId,SKUId ProductName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K, " +
//                        "from retur " +
//                        "where tanggal like '" + tanggal + "%' " +
//                        "group by SKUId"
//                RealmResults<ReturnModel> model = realm.where(ReturnModel.class).findAll().sort("SKUId");
                RealmResults<ReturnModel> modelDisticnt = realm.where(ReturnModel.class).distinct("SKUId");
                Log.d("model distinct Size", modelDisticnt.size() + "");
                List<ResumeOrderModel> list = new ArrayList<>();

                for (ReturnModel mod : modelDisticnt) {
                    ResumeOrderModel resumeOrderModel = new ResumeOrderModel();

                    RealmResults<ReturnModel> model = realm.where(ReturnModel.class).equalTo("SKUId", mod.SKUId).findAll();

                    int SUM_QTYB = model.sum("QTY_B").intValue();
                    int SUM_QTYK = model.sum("QTY_K").intValue();

                    resumeOrderModel.SKUId = mod.SKUId;
                    resumeOrderModel.QTY_B = SUM_QTYB;
                    resumeOrderModel.QTY_K = SUM_QTYK;
                    SKUModel skuModel = ctrlSKU.get(mod.SKUId);
                    resumeOrderModel.ProductName = skuModel.ProductName;

                    list.add(resumeOrderModel);
                }
                Log.d("Header Size", list.size() + "");
//                List<ResumeOrderModel> list = ctrlRetur.listResumeOrder(null);
                for (ResumeOrderModel resumeOrderModel : list) {
                    adapter.addHeader(resumeOrderModel);

//                    "select CustId,CustId CustomerName,sum(QTY_B) QTY_B,sum(QTY_K) QTY_K " +
//                            "from retur where SKUId='" + SKUId + "' " +
//                            "group by CustId"

                    RealmResults<ReturnModel> detailDisticnt = realm.where(ReturnModel.class).equalTo("SKUId", resumeOrderModel.SKUId).distinct("CustId");

                    List<ResumeOrderDetailModul> listDetail = new ArrayList<>();

                    for (ReturnModel mod : detailDisticnt) {
                        ResumeOrderDetailModul resumeOrderDetailModul = new ResumeOrderDetailModul();

                        RealmResults<ReturnModel> model = realm.where(ReturnModel.class).equalTo("SKUId", mod.SKUId).equalTo("CustId", mod.CustId).findAll();

                        int SUM_QTYB = model.sum("QTY_B").intValue();
                        int SUM_QTYK = model.sum("QTY_K").intValue();

                        resumeOrderDetailModul.QTY_B = SUM_QTYB;
                        resumeOrderDetailModul.QTY_K = SUM_QTYK;
                        CustomerModel skuModel = ctrlCustomer.get(mod.CustId);
                        resumeOrderDetailModul.CustomerName = skuModel.CustomerName;

                        listDetail.add(resumeOrderDetailModul);
                    }
                    Log.d("Child Size", listDetail.size() + "");

//                    List<ResumeOrderDetailModul> listDetail = ctrlRetur.listResumeOrderDetail(null, resumeOrderModel.SKUId);
                    adapter.addChild(resumeOrderModel, listDetail);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (adapter != null) {
                ((ExpandableListView) findViewById(R.id.list)).setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            progressDialog.dismiss();
            super.onPostExecute(result);
        }
    }

}
