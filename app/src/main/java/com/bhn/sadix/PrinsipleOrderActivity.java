package com.bhn.sadix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhn.sadix.adapter.OrderDetailAdapter;
import com.bhn.sadix.database.CtrlAppModul;
import com.bhn.sadix.database.CtrlCommon;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.CtrlCustomerType;
import com.bhn.sadix.database.CtrlGroupSKU;
import com.bhn.sadix.database.CtrlTakingOrder;
import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.fragment.kunjungan.TakingOrderFragment;
import com.bhn.sadix.model.CommonModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.model.DiscountPromoModel;
import com.bhn.sadix.model.DiscountResultModel;
import com.bhn.sadix.model.DiskonPromoModel;
import com.bhn.sadix.model.GroupSKUModel;
import com.bhn.sadix.model.OrderDetailModel;
import com.bhn.sadix.model.PrinsipleModel;
import com.bhn.sadix.model.TakingOrderModel;
import com.bhn.sadix.util.ConfirmListener;
import com.bhn.sadix.util.Util;

public class PrinsipleOrderActivity extends AppCompatActivity {
    private CustomerTypeModel customerTypeModel;
    private CustomerModel customerModel;
    private PrinsipleModel prinsipleModel;
    private CommonModel commonModel = new CommonModel();

    private CtrlCustomerType ctrlCustomerType;
    private CtrlGroupSKU ctrlGroupSKU;
    private CtrlCommon ctrlCommon;
    private CtrlTakingOrder ctrlTakingOrder;
    private CtrlConfig ctrlConfig;
    private CtrlAppModul ctrlAppModul;
    private DbMasterHelper dbmaster;

    private TakingOrderFragment fragment;
    private OrderDetailAdapter takingOrderAdapter;
    private ProgressDialog progressDialog;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_spesial);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ctrlCustomerType = new CtrlCustomerType(this);
        ctrlGroupSKU = new CtrlGroupSKU(this);
        ctrlTakingOrder = new CtrlTakingOrder(this);
        ctrlCommon = new CtrlCommon(this);
        ctrlConfig = new CtrlConfig(this);
        ctrlAppModul = new CtrlAppModul(this);
        dbmaster = new DbMasterHelper(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        prinsipleModel = (PrinsipleModel) getIntent().getSerializableExtra("PrinsipleModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + prinsipleModel.CreditLimit + " | Sisa Credit Limit : " + prinsipleModel.CreditTOP);
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }
        findViewById(R.id.expandInfo).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                expandInfo();
            }
        });

        fragment = new TakingOrderFragment();//new TakingOrderFragment(this, customerTypeModel, customerModel);
        /*fragment.setCustomerModel(customerModel);
        fragment.setCustomerTypeModel(customerTypeModel);*/
//        fragment.setCommonModel(commonModel);
//        fragment.setCanvas(false);

        new LoadData().execute();
    }

    public PrinsipleModel getPrinsipleModel() {
        return prinsipleModel;
    }

    @Override
    public void onBackPressed() {
        close();
    }

    private void expandInfo() {
        if (findViewById(R.id.expandInfoData).getVisibility() == View.GONE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_up_float);
            findViewById(R.id.expandInfoData).setVisibility(View.VISIBLE);
        } else if (findViewById(R.id.expandInfoData).getVisibility() == View.VISIBLE) {
            ((ImageView) findViewById(R.id.imageExpand)).setImageResource(android.R.drawable.arrow_down_float);
            findViewById(R.id.expandInfoData).setVisibility(View.GONE);
        }
    }

    private void close() {
        Util.confirmDialog(this, "Info", "Apakah anda akan membatalkan order?", new ConfirmListener() {
            @Override
            public void onDialogCompleted(boolean answer) {
                if (answer) {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Save")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Save");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            close();
        } else if (item.getItemId() == 2) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        ArrayList<TakingOrderModel> list = new ArrayList<TakingOrderModel>();
        for (int i = 0; i < takingOrderAdapter.getGroupCount(); i++) {
            if (takingOrderAdapter.listOrderByGroup(takingOrderAdapter.getGroup(i)) != null) {
                for (int j = 0; j < takingOrderAdapter.getChildrenCount(i); j++) {
                    OrderDetailModel orderDetailModel = takingOrderAdapter.getChild(i, j);
                    if (orderDetailModel.QTY_B > 0 || orderDetailModel.QTY_K > 0) {
                        list.add(new TakingOrderModel(
                                customerModel.CustId,
                                orderDetailModel.skuModel.SKUId,
                                "",
                                orderDetailModel.salesProgramModel.SalesProgramId,
                                orderDetailModel.QTY_B,
                                orderDetailModel.QTY_K,
                                orderDetailModel.DISCOUNT1,
                                orderDetailModel.DISCOUNT2,
                                orderDetailModel.DISCOUNT3,
                                orderDetailModel.TOTAL_PRICE,
                                orderDetailModel.PAYMENT_TERM,
                                orderDetailModel.note,
                                orderDetailModel.PRICE_B,
                                orderDetailModel.PRICE_K,
                                orderDetailModel.LAMA_CREDIT,
                                orderDetailModel.DiscountType,
                                "",
                                orderDetailModel.RandomID,
//							prinsipleModel.PrinsipleId
                                prinsipleModel.RandomID
                        ));
                    }
                }
            }
        }
        Intent intent = new Intent();
        try {
            ArrayList<DiscountPromoModel> listPromo = new ArrayList<DiscountPromoModel>();
            if (fragment.getDiskonAdapter() != null) {
                for (int i = 0; i < fragment.getDiskonAdapter().getCount(); i++) {
                    JSONObject promo = fragment.getDiskonAdapter().getItem(i);
                    if (promo.has("isChecked") && promo.getBoolean("isChecked")) {
                        listPromo.add(new DiscountPromoModel(
                                promo.getInt("DISCOUNT_PROMO_ID"),
                                Integer.parseInt(customerModel.CustId),
                                commonModel.CommonID
                        ));
                    }
                }
            }
            ArrayList<DiscountResultModel> listPromoResult = new ArrayList<DiscountResultModel>();
            if (fragment.getDiskonPromoAdapter() != null) {
                for (int i = 0; i < fragment.getDiskonPromoAdapter().getGroupCount(); i++) {
                    for (int j = 0; j < fragment.getDiskonPromoAdapter().getChildrenCount(i); j++) {
                        DiskonPromoModel model = fragment.getDiskonPromoAdapter().getChild(i, j);
                        listPromoResult.add(new DiscountResultModel(
                                Integer.parseInt(customerModel.CustId),
                                model.DiscountResultID,
                                model.QTY,
                                commonModel.CommonID,
                                model.RandomID
                        ));
                    }
                }
            }
            intent.putExtra("listPromo", listPromo);
            intent.putExtra("listPromoResult", listPromoResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
        intent.putExtra("list", list);
        intent.putExtra("CommonModel", commonModel);
        setResult(RESULT_OK, intent);
        finish();
    }

    private class LoadData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = null;
            try {
                takingOrderAdapter = new OrderDetailAdapter(PrinsipleOrderActivity.this, new ArrayList<GroupSKUModel>(), new HashMap<GroupSKUModel, List<OrderDetailModel>>());
                /*List<GroupSKUModel> list = ctrlGroupSKU.listByPrinsiple(prinsipleModel);
                for (GroupSKUModel groupSKUModel : list) {
                    takingOrderAdapter.addHeader(groupSKUModel);
                }*/
            } catch (Exception e) {
                e.printStackTrace();
                result = e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
//            fragment.setAdapter(takingOrderAdapter);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.main_content_frame, fragment)
                    .addToBackStack(null)
                    .commit();

            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogInfo(PrinsipleOrderActivity.this, result);
            }
            super.onPostExecute(result);
        }
    }

}
