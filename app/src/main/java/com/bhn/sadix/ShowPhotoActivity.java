package com.bhn.sadix;

import org.json.JSONObject;

import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.CustomerModel;
import com.bhn.sadix.model.CustomerTypeModel;
import com.bhn.sadix.util.Util;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class ShowPhotoActivity extends AppCompatActivity implements ConnectionEvent {
    private CtrlConfig ctrlConfig;
    private CustomerModel customerModel;
    private CustomerTypeModel customerTypeModel;
    private ConnectionServer server;

    Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_photo);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        ctrlConfig = new CtrlConfig(this);
        customerModel = (CustomerModel) getIntent().getSerializableExtra("CustomerModel");
        customerTypeModel = (CustomerTypeModel) getIntent().getSerializableExtra("CustomerTypeModel");
        if (customerModel != null) {
            ((TextView) findViewById(R.id.info1)).setText(customerModel.CustomerName + " - " + customerModel.CustomerID);
            ((TextView) findViewById(R.id.info2)).setText(customerModel.CustomerAddress);
            ((TextView) findViewById(R.id.info3)).setText("Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrLimit) + " | Sisa Credit Limit : " + Util.numberFormat.format(customerModel.CustomerCrBalance));
            ((TextView) findViewById(R.id.info4)).setText("Owner Name : " + customerModel.OwnerName);
            ((TextView) findViewById(R.id.info5)).setText("Phone : " + customerModel.CustomerPhone + " | Owner Phone : " + customerModel.OwnerPhone);
            ((TextView) findViewById(R.id.info6)).setText("Customer Type : " + (customerTypeModel == null ? customerModel.CustomerTypeId : customerTypeModel.Description));
            ((TextView) findViewById(R.id.info7)).setText("Customer Group : " + customerModel.CustGroupName);
        }
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        ((Spinner) findViewById(R.id.jenis_photo)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("PHOTO_TYPE")));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        ComboBoxModel comboBoxModel = (ComboBoxModel) ((Spinner) findViewById(R.id.jenis_photo)).getSelectedItem();
        server.setUrl(Util.getServerUrl(this) + "getphoto/CustomerId/" + customerModel.CustId + "/PhotoTypeId/" + comboBoxModel.value);
        server.requestJSONObject(null);
    }

    @Override
    public void error(String error) {
        Util.showDialogError(this, error);
    }

    @Override
    public void messageServer(String respon) {
        try {
            ((ImageViewTouch) findViewById(R.id.image)).setImageBitmap(null);
            JSONObject json = new JSONObject(respon);
            if (json.has("Photo")) {
                JSONObject obj = json.getJSONObject("Photo");
                if (obj.getInt("StatusID") == 1) {
                    String PhotoData = obj.getString("PhotoData");
                    byte[] data = Base64.decode(PhotoData);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    if (bitmap != null) {
                        ((ImageViewTouch) findViewById(R.id.image)).setImageBitmap(bitmap);
                    } else {
                        error("Image error!");
                    }
                } else {
                    error("Data photo tidak ada!");
                }
            } else {
                error("Data photo tidak ada!");
            }
        } catch (Exception e) {
            error(e.getMessage());
        }
    }

}
