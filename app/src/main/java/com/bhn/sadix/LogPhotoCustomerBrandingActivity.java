package com.bhn.sadix;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bhn.sadix.Data.CustomerBrandingModel;
import com.bhn.sadix.adapter.CustBrandingAdapter;
import com.bhn.sadix.database.CtrlCustBranding;
import com.bhn.sadix.util.Util;

import org.json.JSONArray;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class LogPhotoCustomerBrandingActivity extends AppCompatActivity implements OnItemClickListener {
    private CustBrandingAdapter adapter;
    private CtrlCustBranding ctrlCustBranding;

    Realm realm;
    private Toolbar toolbar;
    RealmResults<CustomerBrandingModel> Result;

    ProgressDialog progressDialog;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        realm = Realm.getDefaultInstance();
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Data...");
//        progressDialog.show();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctrlCustBranding = new CtrlCustBranding(this);

        RealmChangeListener callback = new RealmChangeListener() {
            @Override
            public void onChange(Object element) {
                if (Result != null && Result.isValid() && Result.isLoaded()) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    adapter = new CustBrandingAdapter(LogPhotoCustomerBrandingActivity.this, Result);
                    Log.d("Adapter branding", "Size " + adapter.getCount());
                    adapter.setNotifyOnChange(true);
                    ((ListView) findViewById(R.id.list)).setAdapter(adapter);
                    ((ListView) findViewById(R.id.list)).setOnItemClickListener(LogPhotoCustomerBrandingActivity.this);
                }
            }
        };

        Result = realm.where(CustomerBrandingModel.class)
                .equalTo("Done", 1)
                .findAllAsync();

        Result.addChangeListener(callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Export")
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Export");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            exportData();
//            new ExportData(this).execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        CustomerBrandingModel model = adapter.getItem(position);
        progressDialog.show();
        String res = Util.sendUlangCustBranding(this, model);
        if (res == null) {
            Util.showDialogInfo(this, "Data berhasil dikirim");
        } else {
            Util.showDialogError(this, res);
        }
        progressDialog.dismiss();
//        new LoadData(this, model).execute();
    }

    public void exportData() {
        progressDialog.show();
        JSONArray array = new JSONArray();
        for (CustomerBrandingModel custBrandingModel : Result) {
            array.put(Util.convertJSONCustBranding(this, custBrandingModel));
        }
        Util.saveFile(array.toString().getBytes(), "ExpoprtPhotoBranding.txt");
    }

    /*private class LoadData extends AsyncTask<Void, Void, String> {
        private CustomerBrandingModel model;
        private ProgressDialog progressDialog;
        private Context context;

        public LoadData(Context context, CustomerBrandingModel model) {
            this.model = model;
            this.context = context;
            Log.d("Selected...", "CustomerID : " + model.CustomerID + " BrandingID : " + model.BrandingID + " CommonID : " + model.CommonID);
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            return Util.sendUlangCustBranding(context, model);
        }

        @Override
        protected void onPostExecute(String result) {

            Realm realm = Realm.getDefaultInstance();

            List<CustomerBrandingModel> customerBrandingModel = realm.copyFromRealm(
                    realm.where(CustomerBrandingModel.class)
                            .equalTo("Done", 1)
                            .findAll()
            );

            adapter = new CustBrandingAdapter(context, customerBrandingModel);
            adapter.setNotifyOnChange(true);
            ((ListView) findViewById(R.id.list)).setAdapter(adapter);
            progressDialog.dismiss();
            if (result == null) {
                Util.showDialogInfo(context, "Data berhasil dikirim");
            } else {
                Util.showDialogError(context, result);
            }
            super.onPostExecute(result);
        }
    }*/

    private class ExportData extends AsyncTask<Void, Void, String> {
        private ProgressDialog progressDialog;
        private Context context;

        public ExportData(Context context) {
            this.context = context;
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Export Data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                List<CustomerBrandingModel> list = realm.where(CustomerBrandingModel.class).equalTo("Done", 1).findAll();
                JSONArray array = new JSONArray();
                for (CustomerBrandingModel custBrandingModel : list) {
                    array.put(Util.convertJSONCustBranding(context, custBrandingModel));
                }
                Util.saveFile(array.toString().getBytes(), "ExpoprtPhotoBranding.txt");
            } catch (Exception e) {
                return e.getMessage();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if (result != null) {
                Util.showDialogError(LogPhotoCustomerBrandingActivity.this, result);
            } else {
                Util.showDialogInfo(LogPhotoCustomerBrandingActivity.this, "Export data berhasil!");
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (realm.isClosed()) {
            realm = Realm.getDefaultInstance();
        }
    }
}
