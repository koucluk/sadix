package com.bhn.sadix;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bhn.sadix.Data.CommonModel;
import com.bhn.sadix.Data.PrincipleModel;
import com.bhn.sadix.adapter.SearchSKUAdapter;
import com.bhn.sadix.database.CtrlSKU;
import com.bhn.sadix.model.SKUModel;

import java.util.ArrayList;

import io.realm.Realm;

public class SearchSKUActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, OnItemClickListener {
    private CtrlSKU ctrlSKU;
    private SearchSKUAdapter adapter;
    private String CustGroupId;

    Toolbar toolbar;
    CommonModel commonModel;
    Realm realmUI;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        realmUI = Realm.getDefaultInstance();
        commonModel = realmUI.where(CommonModel.class).equalTo("Done", 0).findFirst();
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        CustGroupId = getIntent().getStringExtra("CustGroupId");
        ctrlSKU = new CtrlSKU(this);
        adapter = new SearchSKUAdapter(this, new ArrayList<SKUModel>());
        ((ListView) findViewById(R.id.list)).setAdapter(adapter);
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        realmUI = Realm.getDefaultInstance();
//        PrinsipleModel prinsipleModel = (PrinsipleModel) getIntent().getInt("PrinsipleModel");

        int ID;
        if (getIntent().getStringExtra("PrinsipleModel") == null || getIntent().getStringExtra("PrinsipleModel").equals(""))
            ID = 0;
        else
            ID = Integer.parseInt(getIntent().getStringExtra("PrinsipleModel"));

        Log.d("ID", ID + "");
        if (ID == 0) {
            adapter.setmList(ctrlSKU.searchSKU(null, CustGroupId));
        } else {
            String pID = String.valueOf(ID);

            PrincipleModel principleModel = realmUI
                    .where(PrincipleModel.class)
                    .equalTo("PrinsipleId", pID)
                    .equalTo("CommonID", commonModel.CommonID)
                    .findFirst();

            adapter.setmList(ctrlSKU.searchSKUByPrinsipel(null, CustGroupId, principleModel));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        searchView.setQueryHint("Search");
        searchView.setOnQueryTextListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(1, 1, 1, "Search")
                    .setIcon(android.R.drawable.ic_menu_search)
                    .setActionView(searchView)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        } else {
            menu.add(1, 1, 1, "Search");
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
//		adapter.setmList(ctrlSKU.searchSKU(query, CustGroupId));
        int ID = getIntent().getIntExtra("PrinsipleModel", 0);
        if (ID == 0) {
            adapter.setmList(ctrlSKU.searchSKU(query, CustGroupId));
        } else {
            PrincipleModel principleModel = realmUI.where(PrincipleModel.class).equalTo("PrinsipleId", ID).findFirst();
            adapter.setmList(ctrlSKU.searchSKUByPrinsipel(query, CustGroupId, principleModel));
        }
        adapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.trim().equals("")) {
            adapter.clear();
            int ID = getIntent().getIntExtra("PrinsipleModel", 0);
            if (ID == 0) {
                adapter.setmList(ctrlSKU.searchSKU(null, CustGroupId));
            } else {
                PrincipleModel principleModel = realmUI.where(PrincipleModel.class).equalTo("PrinsipleId", ID).findFirst();
                adapter.setmList(ctrlSKU.searchSKUByPrinsipel(null, CustGroupId, principleModel));
            }
//			adapter.setmList(ctrlSKU.searchSKU(null, CustGroupId));
            adapter.notifyDataSetChanged();
            adapter.notifyDataSetChanged();
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        SKUModel model = adapter.getItem(position);
        Intent intent = new Intent();
        intent.putExtra("data", model);
        setResult(RESULT_OK, intent);
        finish();
    }

}
