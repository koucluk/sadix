package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.UUID;

public class CommonAssetModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4485328793947301834L;
    public double GeoLat;
    public String BeginAsetTime;
    public int CustomerId;
    public String EndAsetTime;
    public double GeoLong;
    public int SalesId;
    public String CommonID;

    public CommonAssetModel() {
        CommonID = UUID.randomUUID().toString();
    }

    public CommonAssetModel(int customerId, int salesId) {
        this();
        CustomerId = customerId;
        SalesId = salesId;
    }

}
