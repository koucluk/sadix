package com.bhn.sadix.model;

import android.database.Cursor;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class PrinsipleModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8895232966073315150L;
    public String PrinsipleId;
    public String PrinsipleName;

    public String RandomID;

    public int total_qty_b = 0;
    public int total_qty_k = 0;
    public double price = 0;
    public double discount = 0;
    public double total_price = 0;

    public int PrinsipleType;
    public double CreditLimit;
    public double CreditTOP;

    public ArrayList<TakingOrderModel> list;
    public ArrayList<DiscountPromoModel> listPromo;
    public ArrayList<DiscountResultModel> listPromoResult;

    public PrinsipleModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public PrinsipleModel(String prinsipleId, String prinsipleName) {
        this();
        PrinsipleId = prinsipleId;
        PrinsipleName = prinsipleName;
    }

    public PrinsipleModel(Cursor c) {
        this();
        PrinsipleId = c.getString(0);
        PrinsipleName = c.getString(1);
    }

    public PrinsipleOrderModel toPrinsipleOrderModel(String commonID) {
        Log.d("Prinsiple qty b inside", total_qty_b + "");
        Log.d("Prinsiple qty k inside", total_qty_k + "");
        return new PrinsipleOrderModel(Integer.parseInt(PrinsipleId), total_qty_b, total_qty_k, price, discount, total_price, commonID, RandomID);
    }
}
