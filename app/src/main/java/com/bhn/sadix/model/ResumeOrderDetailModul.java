package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class ResumeOrderDetailModul implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8199427985397629615L;
    public String CustId;
    public String CustomerName;
    public int QTY_B;
    public int QTY_K;
    public double TOTAL_PRICE;

    public ResumeOrderDetailModul() {
    }

    public ResumeOrderDetailModul(Cursor cursor) {
        CustId = cursor.getString(0);
        CustomerName = cursor.getString(1);
        QTY_B = cursor.getInt(2);
        QTY_K = cursor.getInt(3);
        try {
            TOTAL_PRICE = cursor.getDouble(4);
        } catch (Exception e) {
            TOTAL_PRICE = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ResumeOrderDetailModul) {
            ResumeOrderDetailModul model = (ResumeOrderDetailModul) o;
            return (model.CustId.equals(CustId));
        }
        return false;
    }

}
