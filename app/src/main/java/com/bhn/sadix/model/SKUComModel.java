package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class SKUComModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 824547603594238920L;
	public String SKUId;
	public String ProductName;
	public String GroupId;
	public String SBESAR;
	public String SKECIL;
	public GroupSKUComModel groupSKUComModel;
	public SKUComModel() {
	}
	public SKUComModel(String sKUId) {
		SKUId = sKUId;
	}
	public SKUComModel(String sKUId, String productName, String groupId,
			String sBESAR, String sKECIL) {
		SKUId = sKUId;
		ProductName = productName;
		GroupId = groupId;
		SBESAR = sBESAR;
		SKECIL = sKECIL;
	}
	public SKUComModel(Cursor cursor) {
		SKUId = cursor.getString(0);
		ProductName = cursor.getString(1);
		GroupId = cursor.getString(2);
		SBESAR = cursor.getString(3);
		SKECIL = cursor.getString(4);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof SKUComModel) {
			SKUComModel model = (SKUComModel) o;
			if(model.SKUId != null) {
				return model.SKUId.equals(SKUId);
			}
		}
		return false;
	}
	@Override
	public String toString() {
		return ProductName;
	}

}
