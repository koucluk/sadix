package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class CrcOrderModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2586918370417513338L;
	public int CustID;
	public int SkuID;
	public int QTY = 0;
	public int QTY_L = 0;
	public int CONVER = 0;
	public String POSTDATE;
	public CrcOrderModel() {
	}
//	public CrcOrderModel(int custID, int skuID, int qTY, int qTY_L,
//			int cONVER) {
//		super();
//		CustID = custID;
//		SkuID = skuID;
//		QTY = qTY;
//		QTY_L = qTY_L;
//		CONVER = cONVER;
//	}
	public CrcOrderModel(Cursor cursor) {
		CustID = cursor.getInt(0);
		SkuID = cursor.getInt(1);
		QTY = cursor.getInt(2);
		QTY_L = cursor.getInt(3);
		CONVER = cursor.getInt(4);
		POSTDATE = cursor.getString(5);
	}
	public int getQty() {
		return ((QTY_L*CONVER)+QTY);
	}

}
