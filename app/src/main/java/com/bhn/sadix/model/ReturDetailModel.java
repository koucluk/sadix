package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.UUID;

public class ReturDetailModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7430765347190843167L;
    public SKUModel skuModel;
    public int QTY_B;
    public int QTY_K;
    public String note;
    public ComboBoxModel tipe;
    public String RandomID;

    public ReturDetailModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public ReturDetailModel(SKUModel skuModel) {
        this();
        this.skuModel = skuModel;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ReturDetailModel) {
            ReturDetailModel model = (ReturDetailModel) o;
            return model.skuModel.SKUId.equals(skuModel.SKUId);
        }
        return false;
    }

}
