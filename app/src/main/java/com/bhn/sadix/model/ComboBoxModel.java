package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class ComboBoxModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 533081905349063708L;
    public String value;
    public String text;
    public String tmp1;

    public ComboBoxModel() {
    }

    public ComboBoxModel(String value) {
        this.value = value;
    }

    public ComboBoxModel(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public ComboBoxModel(Cursor cursor) {
        value = cursor.getString(0);
        text = cursor.getString(1);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ComboBoxModel) {
            ComboBoxModel model = (ComboBoxModel) o;
            return (model.value.equals(value));
        }
        return false;
    }

}
