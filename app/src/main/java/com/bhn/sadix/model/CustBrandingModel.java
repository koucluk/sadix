package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;
import java.util.UUID;

public class CustBrandingModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2196211561087718833L;
    public String SalesID;
    public String CustomerID;
    public String BrandingID;
    public String PhotoPic;
    public String GeoLong;
    public String GeoLat;
    public String Description;
    public String NmCust;
    public String NmBranding;
    public String CommonID;
    public String RandomID;

    public int done;

    public CustBrandingModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public CustBrandingModel(String salesID, String customerID,
                             String brandingID, String photoPic, String geoLong, String geoLat,
                             String description) {
        this();
        SalesID = salesID;
        CustomerID = customerID;
        BrandingID = brandingID;
        PhotoPic = photoPic;
        GeoLong = geoLong;
        GeoLat = geoLat;
        Description = description;
    }

    public CustBrandingModel(Cursor cursor) {
        SalesID = cursor.getString(0);
        CustomerID = cursor.getString(1);
        BrandingID = cursor.getString(2);
        PhotoPic = cursor.getString(3);
        GeoLong = cursor.getString(4);
        GeoLat = cursor.getString(5);
        Description = cursor.getString(6);
        CommonID = cursor.getString(8);
    }

}
