package com.bhn.sadix.model;

import android.content.Context;

import com.bhn.sadix.util.Util;

public class UserModel {
    public String SalesId;
    public String UserName;
    public String PathUrl;
    public String Password;
    public String Interval;
    public String FullName;
    public String AppVersion;
    public String IMEI;
    public String SalesType;
    public String CompanyName;
    public String CompanyID;
    public String WHStart;
    public String WHStop;
    public boolean isLogin;
    public boolean isSavePassword;
    public String FormatInv;
    public int LastInv;
    private static UserModel model = new UserModel();

    private UserModel() {
    }

    public static UserModel getInstance(Context context) {
//		if(model == null) {
//			model = new UserModel();
        model.Password = Util.getStringPreference(context, "Password");
        model.AppVersion = Util.getStringPreference(context, "AppVersion");
        model.SalesId = Util.getStringPreference(context, "SalesId");
        model.UserName = Util.getStringPreference(context, "UserName");
        model.PathUrl = Util.getStringPreference(context, "PathUrl");
        model.Interval = Util.getStringPreference(context, "Interval");
        model.FullName = Util.getStringPreference(context, "FullName");
        model.IMEI = Util.getStringPreference(context, "IMEI");
        model.SalesType = Util.getStringPreference(context, "SalesType");
        model.CompanyName = Util.getStringPreference(context, "CompanyName");
        model.CompanyID = Util.getStringPreference(context, "CompanyID");
        model.WHStart = Util.getStringPreference(context, "WHStart");
        model.WHStop = Util.getStringPreference(context, "WHStop");
        model.FormatInv = Util.getStringPreference(context, "FormatInv");
        model.LastInv = Util.getIntegerPreference(context, "LastInv");
//		}
        model.isLogin = Util.getBooleanPreference(context, "isLogin");
        model.isSavePassword = Util.getBooleanPreference(context, "isSavePassword");
        return model;
    }
}
