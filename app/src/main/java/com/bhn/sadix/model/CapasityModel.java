package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONObject;

public class CapasityModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8089644187007601959L;
	public SKUModel skuModel;
	public List<Detail> details;
	public CapasityModel() {
		details = new ArrayList<CapasityModel.Detail>();
		Calendar calendar = Calendar.getInstance();
		details.add(new Detail(1, calendar.get(calendar.YEAR)));
		details.add(new Detail(2, calendar.get(calendar.YEAR)));
		details.add(new Detail(3, calendar.get(calendar.YEAR)));
		details.add(new Detail(4, calendar.get(calendar.YEAR)));
		details.add(new Detail(5, calendar.get(calendar.YEAR)));
		details.add(new Detail(6, calendar.get(calendar.YEAR)));
		details.add(new Detail(7, calendar.get(calendar.YEAR)));
		details.add(new Detail(8, calendar.get(calendar.YEAR)));
		details.add(new Detail(9, calendar.get(calendar.YEAR)));
		details.add(new Detail(10, calendar.get(calendar.YEAR)));
		details.add(new Detail(11, calendar.get(calendar.YEAR)));
		details.add(new Detail(12, calendar.get(calendar.YEAR)));
	}
	public CapasityModel(SKUModel skuModel) {
		this();
		this.skuModel = skuModel;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof CapasityModel) {
			CapasityModel model = (CapasityModel) o;
			return model.skuModel.SKUId.equals(skuModel.SKUId);
		}
		return false;
	}
	public List<JSONObject> listJson() {
		List<JSONObject> list = new ArrayList<JSONObject>();
		for (Detail detail : details) {
			list.add(detail.toJson());
		}
		return list;
	}
	
	public class Detail {
	    public int Bln;
	    public int Thn;
	    public long Qty;
	    public long Price;
		public Detail(int bln, int thn) {
			Bln = bln;
			Thn = thn;
		}
		public JSONObject toJson() {
			JSONObject json = new JSONObject();
			try {
				json.put("SKUId", skuModel.SKUId);
				json.put("Bln", Bln);
				json.put("Thn", Thn);
				json.put("Qty", Qty);
				json.put("Price", Price);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return json;
		}
	}

}
