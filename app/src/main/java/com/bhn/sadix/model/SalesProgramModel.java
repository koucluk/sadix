package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class SalesProgramModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7715146068182663150L;
	public String SalesProgramId;
	public String SalesProgramDescription;
	public String GroupId;
	public SalesProgramModel() {
	}
	public SalesProgramModel(String salesProgramId,
			String salesProgramDescription, String groupId) {
		SalesProgramId = salesProgramId;
		SalesProgramDescription = salesProgramDescription;
		GroupId = groupId;
	}
	public SalesProgramModel(Cursor cursor) {
		SalesProgramId = cursor.getString(0);
		SalesProgramDescription = cursor.getString(1);
		GroupId = cursor.getString(2);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof SalesProgramModel) {
			SalesProgramModel model = (SalesProgramModel) o;
			return (model.SalesProgramId.equals(SalesProgramId));
		}
		return false;
	}

}
