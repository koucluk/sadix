package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class ReturModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1616246959953519094L;
    public String CustId;
    public String SKUId;
    public String tanggal;
    public int QTY_B;
    public int QTY_K;
    public String note;
    public String tipe;
    public String CommonID;
    public String RandomID;

    public ReturModel() {
    }

    public ReturModel(String custId, String sKUId, String tanggal, int qTY_B,
                      int qTY_K, String note, String tipe, String CommonID, String RandomID) {
        CustId = custId;
        SKUId = sKUId;
        this.tanggal = tanggal;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        this.note = note;
        this.tipe = tipe;
        this.CommonID = CommonID;
        this.RandomID = RandomID;
    }

    public ReturModel(Cursor cursor) {
        CustId = cursor.getString(0);
        SKUId = cursor.getString(1);
        tanggal = cursor.getString(2);
        QTY_B = cursor.getInt(3);
        QTY_K = cursor.getInt(4);
        note = cursor.getString(5);
        tipe = cursor.getString(6);
        CommonID = cursor.getString(7);
        RandomID = cursor.getString(8);
    }

}
