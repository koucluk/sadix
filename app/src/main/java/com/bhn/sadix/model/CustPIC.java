package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.UUID;

import org.json.JSONObject;

import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.util.Util;

import android.database.Cursor;
import android.graphics.Bitmap;

public class CustPIC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3464051529323266395L;
	public int CustID;
	public int PicID;
	public String PicName;
	public int PicType;
	public String PicIdNo;
	public String PicAddres;
	public String PicPhone;
	public String PicFax;
	public String PicEmail;
	public String PicRemark;
	public String PicBirthPlace;
	public String PicBirthDate;
	public String PicReligion;
	public String PicHobby;
	public String StatusId;
	public String RandomID;
	public String CustRandomID;
    
    public Bitmap bitmap;
    public byte[] data;
	public CustPIC() {
		RandomID = UUID.randomUUID().toString();
		StatusId = "1";
	}
	public CustPIC(int custID, int picID,String picName, int picType, String picIdNo,
			String picAddres, String picPhone, String picFax, String picEmail,
			String picRemark, String picBirthPlace, String picBirthDate,
			String picReligion, String picHobby) {
		this();
		CustID = custID;
		PicID = picID;
		PicName = picName;
		PicType = picType;
		PicIdNo = picIdNo;
		PicAddres = picAddres;
		PicPhone = picPhone;
		PicFax = picFax;
		PicEmail = picEmail;
		PicRemark = picRemark;
		PicBirthPlace = picBirthPlace;
		PicBirthDate = picBirthDate;
		PicReligion = picReligion;
		PicHobby = picHobby;
	}
	public CustPIC(Cursor cursor) {
		CustID = cursor.getInt(0);
		PicID = cursor.getInt(1);
		PicName = cursor.getString(2);
		PicType = cursor.getInt(3);
		PicIdNo = cursor.getString(4);
		PicAddres = cursor.getString(5);
		PicPhone = cursor.getString(6);
		PicFax = cursor.getString(7);
		PicEmail = cursor.getString(8);
		PicRemark = cursor.getString(9);
		PicBirthPlace = cursor.getString(10);
		PicBirthDate = cursor.getString(11);
		PicReligion = cursor.getString(12);
		PicHobby = cursor.getString(13);
		RandomID = cursor.getString(14);
		CustRandomID = cursor.getString(15);
	}
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("StatusId", StatusId);
			json.put("PicID", PicID);
			json.put("PicName", PicName);
			json.put("PicType", PicType);
			json.put("PicIdNo", PicIdNo);
			json.put("PicAddres", PicAddres);
			json.put("PicPhone", PicPhone);
			json.put("PicFax", PicFax);
			json.put("PicEmail", PicEmail);
			json.put("PicRemark", PicRemark);
			json.put("PicBirthPlace", PicBirthPlace);
			json.put("PicBirthDate", PicBirthDate);
			json.put("PicReligion", PicReligion);
			json.put("PicHobby", PicHobby);
			json.put("RandomID", RandomID);
			json.put("CustRandomID", CustRandomID);
		} catch (Exception e) {
		}
		return json;
	}
	public JSONObject getJsonSendImage(String SalesId) {
		try {
			JSONObject json = new JSONObject();
			json.put("SalesID", SalesId);
			json.put("CustomerID", CustID);
			json.put("PhotoTypeID", 0);
			json.put("PhotoPic", Base64.encodeBytes(data));
			json.put("PhotoCat", 2);
			json.put("RandomID", RandomID);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
	public void saveImage(CtrlCustPhoto ctrlCustPhoto, String SalesId) {
		String nmFile = UUID.randomUUID().toString()+".jpg";
		if(data != null) {
			Util.saveFileImg(data, nmFile);
			data = null;
		}
		ctrlCustPhoto.save(new CustPhotoModel(
			SalesId, 
			String.valueOf(CustID), 
			"0", 
			nmFile,
			"2",
			RandomID
		));
	}

}
