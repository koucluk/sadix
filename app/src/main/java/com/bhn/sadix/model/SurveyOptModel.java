package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class SurveyOptModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9045510279816249399L;
    public String SurveyId;
    public String ListID;
    public String ListValue;

    public SurveyOptModel(String surveyId, String listID, String listValue) {
        super();
        SurveyId = surveyId;
        ListID = listID;
        ListValue = listValue;
    }

    public SurveyOptModel(Cursor cursor) {
        SurveyId = cursor.getString(0);
        ListID = cursor.getString(1);
        ListValue = cursor.getString(2);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SurveyOptModel) {
            SurveyOptModel model = (SurveyOptModel) o;
            return (model.SurveyId.equals(SurveyId) && model.ListID.equals(ListID));
        }
        return false;
    }

}
