package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class CommonSurveyModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5190851560908520745L;
    public String CommonId;
    public String CustomerId;
    public String SalesId;
    public String GeoLat;
    public String GeoLong;
    public String BeginSurveyTime;
    public String EndSurveyTime;
    public String FormId;
    public String nmCustomer;
    public String nmForm;
    public int done;

    public CommonSurveyModel() {
    }

    public CommonSurveyModel(String commonId, String customerId,
                             String salesId, String geoLat, String geoLong,
                             String beginSurveyTime, String endSurveyTime, String formId) {
        CommonId = commonId;
        CustomerId = customerId;
        SalesId = salesId;
        GeoLat = geoLat;
        GeoLong = geoLong;
        BeginSurveyTime = beginSurveyTime;
        EndSurveyTime = endSurveyTime;
        FormId = formId;
    }

    public CommonSurveyModel(Cursor cursor) {
        CommonId = cursor.getString(0);
        CustomerId = cursor.getString(1);
        SalesId = cursor.getString(2);
        GeoLat = cursor.getString(3);
        GeoLong = cursor.getString(4);
        BeginSurveyTime = cursor.getString(5);
        EndSurveyTime = cursor.getString(6);
        FormId = cursor.getString(7);
    }

}
