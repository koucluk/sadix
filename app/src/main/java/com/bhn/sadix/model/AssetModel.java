package com.bhn.sadix.model;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bhn.sadix.R;
import com.bhn.sadix.widget.AssetWidget;
import com.bhn.sadix.widget.AssetWidget.AssetWidgetListener;

public class AssetModel {
    public String ASETNAME;
    public String ASETTYPE;
    public String ASETNO;
    public String ASETID;
    private AssetWidget assetWidget;
    private Dialog dialog;
    private String CommonID;
    private int type;

    public AssetModel(String aSETNO) {
        ASETNO = aSETNO;
    }

    public AssetModel(final Activity context, String ASETNAME, String ASETTYPE, String ASETNO, String ASETID, AssetWidgetListener listener, String custId, String CommonID, int type) {
        this.type = type;
        this.CommonID = CommonID;
        assetWidget = new AssetWidget(context, ASETNAME, ASETTYPE, ASETID, listener, custId, ASETNO, this.CommonID, type);
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.ASETNO = ASETNO;
        this.ASETID = ASETID;

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(assetWidget);
                ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    public void show() {
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public AssetWidget getAssetWidget() {
        return assetWidget;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AssetModel) {
            AssetModel model = (AssetModel) o;
            return (ASETNO.equals(model.ASETNO));
        }
        return false;
    }
}
