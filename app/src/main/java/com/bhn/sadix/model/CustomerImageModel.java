package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class CustomerImageModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2677618357169948163L;
	public String CustId;
	public String type;
	public String namaFile;
	public CustomerImageModel() {
		super();
	}
	public CustomerImageModel(String CustId, String type, String namaFile) {
		super();
		this.CustId = CustId;
		this.type = type;
		this.namaFile = namaFile;
	}
	public CustomerImageModel(Cursor cursor) {
		CustId = cursor.getString(0);
		this.type =  cursor.getString(1);
		this.namaFile =  cursor.getString(2);
	}

}
