package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class StockCompetitorModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6597256125304070638L;
    public String CustId;
    public String tanggal;
    public String SKUId;
    public int QTY_B;
    public int QTY_K;
    public String CommonID;
    public double PRICE;
    public double PRICE2;
    public String Note;
    public String RandomID;

    public StockCompetitorModel() {
    }

    public StockCompetitorModel(String custId, String tanggal, String sKUId,
                                int qTY_B, int qTY_K, String CommonID, double PRICE, double PRICE2, String Note, String RandomID) {
        CustId = custId;
        this.tanggal = tanggal;
        SKUId = sKUId;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        this.CommonID = CommonID;
        this.PRICE = PRICE;
        this.PRICE2 = PRICE2;
        this.Note = Note;
        this.RandomID = RandomID;
    }

    public StockCompetitorModel(Cursor cursor) {
        CustId = cursor.getString(0);
        tanggal = cursor.getString(1);
        SKUId = cursor.getString(2);
        QTY_B = cursor.getInt(3);
        QTY_K = cursor.getInt(4);
        CommonID = cursor.getString(5);
        PRICE = cursor.getDouble(6);
        PRICE2 = cursor.getDouble(7);
        Note = cursor.getString(8);
        RandomID = cursor.getString(9);
    }

}
