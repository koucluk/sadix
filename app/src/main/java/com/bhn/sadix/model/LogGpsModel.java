package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class LogGpsModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6938487244052750722L;
	public String SalesID;
	public String IMEI;
	public String LogTime;
	public String SC;
	public double GeoLong;
	public double GeoLat;
	public int statusLog;
	public LogGpsModel(String salesID, String iMEI, String logTime, String sC,
			double geoLong, double geoLat, int statusLog) {
		super();
		SalesID = salesID;
		IMEI = iMEI;
		LogTime = logTime;
		SC = sC;
		GeoLong = geoLong;
		GeoLat = geoLat;
		this.statusLog = statusLog;
	}
	public LogGpsModel(Cursor cursor) {
		SalesID = cursor.getString(0);
		IMEI = cursor.getString(1);
		LogTime = cursor.getString(2);
		SC = cursor.getString(3);
		GeoLong = cursor.getDouble(4);
		GeoLat = cursor.getDouble(5);
		statusLog = cursor.getInt(6);
	}

}
