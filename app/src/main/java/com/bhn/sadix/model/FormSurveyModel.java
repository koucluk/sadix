package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class FormSurveyModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7136381894863934174L;
    public String FormId;
    public String FormName;
    public String FormRemark;

    public FormSurveyModel(String formId, String formName, String formRemark) {
        super();
        FormId = formId;
        FormName = formName;
        FormRemark = formRemark;
    }

    public FormSurveyModel(Cursor cursor) {
        FormId = cursor.getString(0);
        FormName = cursor.getString(1);
        FormRemark = cursor.getString(2);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FormSurveyModel) {
            FormSurveyModel model = (FormSurveyModel) o;
            return model.FormId.equals(FormId);
        }
        return false;
    }

}
