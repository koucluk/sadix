package com.bhn.sadix.model;

/**
 * Created by caksono on 06/03/17.
 */

public class VisitNote {
    int VisitID;
    String VisitName;
    int CustType;
    int VisitType;
    int GeoID;
    int GeoValue;
    int GeoOpr;
    int DurID;
    int DurValue;
    int DurOpr;
    int ReqField;

    public VisitNote(int visitID, String visitName, int custType, int visitType, int geoID, int geoValue, int geoOpr, int durID, int durValue, int durOpr, int reqField) {
        VisitID = visitID;
        VisitName = visitName;
        CustType = custType;
        VisitType = visitType;
        GeoID = geoID;
        GeoValue = geoValue;
        GeoOpr = geoOpr;
        DurID = durID;
        DurValue = durValue;
        DurOpr = durOpr;
        ReqField = reqField;
    }

    public int getReqField() {
        return ReqField;
    }

    public void setReqField(int reqField) {
        ReqField = reqField;
    }

    public int getVisitID() {
        return VisitID;
    }

    public void setVisitID(int visitID) {
        VisitID = visitID;
    }

    public String getVisitName() {
        return VisitName;
    }

    public void setVisitName(String visitName) {
        VisitName = visitName;
    }

    public int getCustType() {
        return CustType;
    }

    public void setCustType(int custType) {
        CustType = custType;
    }

    public int getVisitType() {
        return VisitType;
    }

    public void setVisitType(int visitType) {
        VisitType = visitType;
    }

    public int getGeoID() {
        return GeoID;
    }

    public void setGeoID(int geoID) {
        GeoID = geoID;
    }

    public int getGeoValue() {
        return GeoValue;
    }

    public void setGeoValue(int geoValue) {
        GeoValue = geoValue;
    }

    public int getGeoOpr() {
        return GeoOpr;
    }

    public void setGeoOpr(int geoOpr) {
        GeoOpr = geoOpr;
    }

    public int getDurID() {
        return DurID;
    }

    public void setDurID(int durID) {
        DurID = durID;
    }

    public int getDurValue() {
        return DurValue;
    }

    public void setDurValue(int durValue) {
        DurValue = durValue;
    }

    public int getDurOpr() {
        return DurOpr;
    }

    public void setDurOpr(int durOpr) {
        DurOpr = durOpr;
    }
}
