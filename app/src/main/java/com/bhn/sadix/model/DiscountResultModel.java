package com.bhn.sadix.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;

public class DiscountResultModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3097017013182255551L;
    public int CustomerId;
    public int DiscountResultID;
    public int DiscountResultValue;
    public String CommonID;
    public String RandomID;

    public DiscountResultModel() {
    }

    public DiscountResultModel(int customerId, int discountResultID,
                               int discountResultValue, String commonID, String RandomID) {
        CustomerId = customerId;
        DiscountResultID = discountResultID;
        DiscountResultValue = discountResultValue;
        CommonID = commonID;
        this.RandomID = RandomID;
    }

    public DiscountResultModel(Cursor cursor) {
        CustomerId = cursor.getInt(0);
        DiscountResultID = cursor.getInt(1);
        DiscountResultValue = cursor.getInt(2);
        CommonID = cursor.getString(3);
        RandomID = cursor.getString(4);
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("CustomerId", CustomerId);
            json.put("DiscountResultID", DiscountResultID);
            json.put("DiscountResultValue", DiscountResultValue);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}
