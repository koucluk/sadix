package com.bhn.sadix.model;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bhn.sadix.R;
import com.bhn.sadix.widget.AssetServiceWidget;
import com.bhn.sadix.widget.AssetServiceWidget.AssetServiceWidgetListener;

public class AssetServiceModel {
    public String ASETNAME;
    public String ASETTYPE;
    public String ASETNO;
    public String ASETID;
    private AssetServiceWidget assetWidgetService;
    private Dialog dialog;
    private String CommonID;

    public AssetServiceModel(String aSETNO) {
        ASETNO = aSETNO;
    }

    public AssetServiceModel(Context context, String ASETNAME, String ASETTYPE, String ASETNO, String ASETID, AssetServiceWidgetListener listener, String custId, String CommonID) {
        this.CommonID = CommonID;
        assetWidgetService = new AssetServiceWidget(context, ASETNAME, ASETTYPE, ASETID, listener, custId, ASETNO, this.CommonID);
        dialog = new Dialog(context);
        this.ASETNAME = ASETNAME;
        this.ASETTYPE = ASETTYPE;
        this.ASETNO = ASETNO;
        this.ASETID = ASETID;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(assetWidgetService);
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void show() {
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public AssetServiceWidget getAssetWidgetService() {
        return assetWidgetService;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AssetServiceModel) {
            AssetServiceModel model = (AssetServiceModel) o;
            return (ASETNO.equals(model.ASETNO));
        }
        return false;
    }
}
