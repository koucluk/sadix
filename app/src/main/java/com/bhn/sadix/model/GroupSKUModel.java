package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class GroupSKUModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8987069223778646970L;
    public String GroupId;
    public String GroupName;
    public String PromoId;
    public String FokusId;
    public int jumlah;

    public GroupSKUModel() {
    }

    public GroupSKUModel(String groupId) {
        GroupId = groupId;
    }

    public GroupSKUModel(String groupId, String groupName, String promoId, String fokusId) {
        GroupId = groupId;
        GroupName = groupName;
        PromoId = promoId;
        FokusId = fokusId;
    }

    public GroupSKUModel(String groupId, String groupName, String promoId, String fokusId, int jumlah) {
        GroupId = groupId;
        GroupName = groupName;
        PromoId = promoId;
        FokusId = fokusId;
        this.jumlah = jumlah;
    }

    public GroupSKUModel(Cursor cursor) {
        GroupId = cursor.getString(0);
        GroupName = cursor.getString(1);
        PromoId = cursor.getString(2);
        FokusId = cursor.getString(3);
        jumlah = cursor.getInt(4);
    }

    public GroupSKUModel copy() {
        return new GroupSKUModel(GroupId, GroupName, PromoId, FokusId);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof GroupSKUModel) {
            GroupSKUModel model = (GroupSKUModel) o;
            if (model.GroupId != null) {
                return model.GroupId.equals(GroupId);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return GroupName;
    }

    public GroupSKUModel newGroupSKUModel() {
        return new GroupSKUModel(GroupId, GroupName, PromoId, FokusId, jumlah);
    }

}
