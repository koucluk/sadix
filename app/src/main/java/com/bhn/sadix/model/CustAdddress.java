package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.UUID;

import org.json.JSONObject;

import com.bhn.sadix.connection.Base64;
import com.bhn.sadix.database.CtrlCustPhoto;
import com.bhn.sadix.util.Util;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;

public class CustAdddress implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1185698834676548907L;
	public int CustID;
	public int AddID;
	public String AddName;
    public int AddType;
    public String AddAddres;
    public String AddPhone;
    public String AddFax;
    public String AddEmail;
    public String AddRemark;
    public String GeoLong;
    public String GeoLat;
    public int StatusId;
    public String RandomID;
    public String CustRandomID;
    
    public Bitmap bitmap;
    public byte[] data;
	public CustAdddress() {
		RandomID = UUID.randomUUID().toString();
		GeoLong = "0";
		GeoLat = "0";
		StatusId = 0;
	}
	public CustAdddress(int custID, int addID, String addName, int addType, String addAddres,
			String addPhone, String addFax, String addEmail, String addRemark) {
		this();
		CustID = custID;
		AddID = addID;
		AddName = addName;
		AddType = addType;
		AddAddres = addAddres;
		AddPhone = addPhone;
		AddFax = addFax;
		AddEmail = addEmail;
		AddRemark = addRemark;
	}
	public CustAdddress(Cursor cursor) {
		CustID = cursor.getInt(0);
		AddID = cursor.getInt(1);
		AddName = cursor.getString(2);
		AddType = cursor.getInt(3);
		AddAddres = cursor.getString(4);
		AddPhone = cursor.getString(5);
		AddFax = cursor.getString(6);
		AddEmail = cursor.getString(7);
		AddRemark = cursor.getString(8);
		GeoLong = cursor.getString(9);
		GeoLat = cursor.getString(10);
		RandomID = cursor.getString(11);
		CustRandomID = cursor.getString(12);
	}
	public JSONObject toJson(Location location) {
		JSONObject json = new JSONObject();
		try {
			if(location != null) {
				GeoLat = String.format("%1$s", location.getLatitude());//location.getLatitude();
				GeoLong = String.format("%1$s", location.getLongitude());//location.getLongitude();
			}
			json.put("AddID", AddID);
//			json.put("GeoLong", GeoLong);
//			json.put("GeoLat", GeoLat);
			json.put("GeoLat", Double.parseDouble(GeoLat.equals("") ? "0" : GeoLat));
			json.put("GeoLong", Double.parseDouble(GeoLong.equals("") ? "0" : GeoLong));
			json.put("StatusId", StatusId);
			json.put("AddName", AddName);
			json.put("AddType", AddType);
			json.put("AddAddres", AddAddres);
			json.put("AddPhone", AddPhone);
			json.put("AddFax", AddFax);
			json.put("AddEmail", AddEmail);
			json.put("AddRemark", AddRemark);
			json.put("RandomID", RandomID);
			json.put("CustRandomID", CustRandomID);
		} catch (Exception e) {
		}
		return json;
	}
	public JSONObject getJsonSendImage(String SalesId) {
		try {
			JSONObject json = new JSONObject();
			json.put("SalesID", SalesId);
			json.put("CustomerID", CustID);
			json.put("PhotoTypeID", 0);
			json.put("PhotoPic", Base64.encodeBytes(data));
			json.put("PhotoCat", 1);
			json.put("RandomID", RandomID);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
	public void saveImage(CtrlCustPhoto ctrlCustPhoto, String SalesId) {
		String nmFile = UUID.randomUUID().toString()+".jpg";
		if(data != null) {
			Util.saveFileImg(data, nmFile);
			data = null;
		}
		ctrlCustPhoto.save(new CustPhotoModel(
			SalesId, 
			String.valueOf(CustID), 
			"0", 
			nmFile,
			"1",
			RandomID
		));
	}

}
