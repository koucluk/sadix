package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class HasilSurveyModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5500878183900117464L;
    public String CommonId;
    public String FormId;
    public String SurveyId;
    public String ListID;
    public String ListValue;

    public HasilSurveyModel() {
    }

    public HasilSurveyModel(String commonId, String formId, String surveyId,
                            String listID, String listValue) {
        CommonId = commonId;
        FormId = formId;
        SurveyId = surveyId;
        ListID = listID;
        ListValue = listValue;
    }

    public HasilSurveyModel(Cursor cursor) {
        CommonId = cursor.getString(0);
        FormId = cursor.getString(1);
        SurveyId = cursor.getString(2);
        ListID = cursor.getString(3);
        ListValue = cursor.getString(4);
    }

    /*@Override
    public boolean equals(Object obj) {
        if (obj instanceof FormSurveyModel) {
            FormSurveyModel model = (FormSurveyModel) obj;
            return model.FormId.equals(FormId);
        }
        return false;
    }*/
}
