package com.bhn.sadix.model;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.UUID;

public class OrderDetailModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3766157116453314872L;
    public SKUModel skuModel;
    public int QTY_B;
    public int QTY_K;
    public double DISCOUNT1;
    public double DISCOUNT2;
    public double DISCOUNT3;
    public double TOTAL_PRICE;
    public String PAYMENT_TERM;
    public SalesProgramModel salesProgramModel;
    public String note;
    public int LAMA_CREDIT;
    public double PRICE_B;
    public double PRICE_K;
    public int STOK_B;
    public int STOK_K;
    public String DiscountType;
    public String RandomID;

    public String ReasonID;

    public Bitmap bitmap;
    public String SkuDesc;

    public int Promo;
    public int NewProduct;
    public String KeteranganPromo;

    public OrderDetailModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public OrderDetailModel(SKUModel skuModel) {
        this();
        this.skuModel = skuModel;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof OrderDetailModel) {
            OrderDetailModel model = (OrderDetailModel) o;
            return model.skuModel.SKUId.equals(skuModel.SKUId);
        }
        return false;
    }

    public int getQty() {
        return (int) ((QTY_B * skuModel.CONVER) + QTY_K);
    }

}
