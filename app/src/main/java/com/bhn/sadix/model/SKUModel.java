package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class SKUModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5524374282724884168L;
	public String SKUId;
	public String GroupId;
	public String ProductName;
	public double HET_PRICE;
	public String SBESAR;
	public String SKECIL;
	public double CONVER;
	public double HET_PRICE_L;
	public String DISCOUNT_1_TYPE;
	public double DISCOUNT_1_MAX;
	public String DISCOUNT_2_TYPE;
	public double DISCOUNT_2_MAX;
	public String DISCOUNT_3_TYPE;
	public double DISCOUNT_3_MAX;
	public String DISCOUNT_TYPE;
	public int QBESAR;
	public int QKECIL;
	public int QOBESAR;
	public int QOKECIL;
	public String SKUCODE;
	
	public GroupSKUModel groupSKUModel;
	
	public String PromoId;
	public String FokusId;
	public SKUModel() {
	}
	public SKUModel(String sKUId, String groupId, String productName,
			double hET_PRICE, String sBESAR, String sKECIL, double cONVER,
			double hET_PRICE_L, String dISCOUNT_1_TYPE, double dISCOUNT_1_MAX,
			String dISCOUNT_2_TYPE, double dISCOUNT_2_MAX,
			String dISCOUNT_3_TYPE, double dISCOUNT_3_MAX, String dISCOUNT_TYPE,
			int qBESAR, int qKECIL) {
		SKUId = sKUId;
		GroupId = groupId;
		ProductName = productName;
		HET_PRICE = hET_PRICE;
		SBESAR = sBESAR;
		SKECIL = sKECIL;
		CONVER = cONVER;
		HET_PRICE_L = hET_PRICE_L;
		DISCOUNT_1_TYPE = dISCOUNT_1_TYPE;
		DISCOUNT_1_MAX = dISCOUNT_1_MAX;
		DISCOUNT_2_TYPE = dISCOUNT_2_TYPE;
		DISCOUNT_2_MAX = dISCOUNT_2_MAX;
		DISCOUNT_3_TYPE = dISCOUNT_3_TYPE;
		DISCOUNT_3_MAX = dISCOUNT_3_MAX;
		DISCOUNT_TYPE = dISCOUNT_TYPE;
		QBESAR = qBESAR;
		QKECIL = qKECIL;
		QOBESAR = 0;
		QOKECIL = 0;
	}
	public SKUModel(Cursor cursor) {
		SKUId = cursor.getString(0);
		GroupId = cursor.getString(1);
		ProductName = cursor.getString(2);
		HET_PRICE = cursor.getDouble(3);
		SBESAR = cursor.getString(4);
		SKECIL = cursor.getString(5);
		CONVER = cursor.getDouble(6);
		HET_PRICE_L = cursor.getDouble(7);
		DISCOUNT_1_TYPE = cursor.getString(8);
		DISCOUNT_1_MAX = cursor.getDouble(9);
		DISCOUNT_2_TYPE = cursor.getString(10);
		DISCOUNT_2_MAX = cursor.getDouble(11);
		DISCOUNT_3_TYPE = cursor.getString(12);
		DISCOUNT_3_MAX = cursor.getDouble(13);
		DISCOUNT_TYPE = cursor.getString(14);
		QBESAR = cursor.getInt(15);
		QKECIL = cursor.getInt(16);
		QOBESAR = cursor.getInt(17);
		QOKECIL = cursor.getInt(18);
		SKUCODE = cursor.getString(19);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof SKUModel) {
			SKUModel model = (SKUModel) o;
			if(model.SKUId != null) {
				return model.SKUId.equals(SKUId);
			}
		}
		return false;
	}
	@Override
	public String toString() {
		return ProductName;
	}

}
