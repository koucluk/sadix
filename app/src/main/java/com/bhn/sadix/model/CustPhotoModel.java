package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class CustPhotoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 505959500640272576L;
	public String SalesID;
	public String CustomerID;
	public String PhotoTypeID;
	public String PhotoPic;
	public String NmCust;
	public String NmPhoto;
	public String PhotoCat;
	public String RandomID;
	public CustPhotoModel() {
	}
	public CustPhotoModel(String salesID, String customerID,
			String photoTypeID, String photoPic, String photoCat, String randomID) {
		SalesID = salesID;
		CustomerID = customerID;
		PhotoTypeID = photoTypeID;
		PhotoPic = photoPic;
		PhotoCat = photoCat;
		RandomID = randomID;
	}
	public CustPhotoModel(Cursor cursor) {
		SalesID = cursor.getString(0);
		CustomerID = cursor.getString(1);
		PhotoTypeID = cursor.getString(2);
		PhotoPic = cursor.getString(3);
		PhotoCat = cursor.getString(4);
		RandomID = cursor.getString(5);
	}

}
