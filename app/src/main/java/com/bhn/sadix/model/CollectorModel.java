package com.bhn.sadix.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

public class CollectorModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8407527005720301344L;
    public String CustId;
    public String status;
    public String data1;
    public String data2;
    public String data3;
    public String tanggal;
    public String CommonID;
    public String data4;
    public String data5;
    public String data6;

    public String RandomID;

    //kaga ada di db
    public String payMethod;

    public CollectorModel() {
        RandomID = UUID.randomUUID().toString();
        this.CustId = "";
        this.status = "";
        this.data1 = "";
        this.data2 = "";
        this.data3 = "";
        this.CommonID = "";
        this.data4 = "";
        this.data5 = "";
        this.data6 = "";
    }

    public CollectorModel(String CommonID, String status) {
        this();
        this.CommonID = CommonID;
        this.status = status;
    }

    public CollectorModel(String custId, String status, String data1,
                          String data2, String data3, String CommonID,
                          String data4, String data5, String data6, String RandomID) {
        this();
        CustId = custId;
        this.status = status;
        this.data1 = data1;
        this.data2 = data2;
        this.data3 = data3;
        this.CommonID = CommonID;
        this.data4 = data4;
        this.data5 = data5;
        this.data6 = data6;
        this.RandomID = RandomID;
    }

    public CollectorModel(Cursor cursor) {
        CustId = cursor.getString(0);
        status = cursor.getString(1);
        data1 = cursor.getString(2);
        data2 = cursor.getString(3);
        data3 = cursor.getString(4);
        tanggal = cursor.getString(5);
        CommonID = cursor.getString(6);
        data4 = cursor.getString(7);
        data5 = cursor.getString(8);
        data6 = cursor.getString(9);
        RandomID = cursor.getString(10);
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("StatusId", status);
            json.put("Data1", data1);
            json.put("Data2", data2);
            json.put("Data3", data3);
            json.put("Data4", data4);
            json.put("Data5", data5);
            json.put("Data6", data6);
            json.put("TransactionTime", tanggal);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}
