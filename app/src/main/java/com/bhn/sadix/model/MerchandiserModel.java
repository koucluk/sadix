package com.bhn.sadix.model;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bhn.sadix.R;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetDetailWidget;
import com.bhn.sadix.widget.AssetWidget;
import com.bhn.sadix.widget.AssetWidget.AssetView;
import com.bhn.sadix.widget.AssetWidget.AssetWidgetListener;

public class MerchandiserModel {
    public String IDX;
    public String NAME;
    public String TYPE;
    private AssetWidget assetWidget;
    private Dialog dialog;

    private String CommonID;
    public boolean isEntry = false;
    private Context con;

    public MerchandiserModel(final Activity context, AssetWidgetListener listener, Cursor cursor, String CommonID) {
        this.con = context;
        IDX = cursor.getString(0);
        NAME = cursor.getString(1);
        TYPE = cursor.getString(2);
        this.CommonID = CommonID;
        assetWidget = new AssetWidget(context, NAME, IDX, "0", listener, "", "", this.CommonID, 5);
        assetWidget.setStatus("Asset");

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(assetWidget);
                dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            boolean isphoto = true;
                            for (int i = 0; i < assetWidget.getAdapter().getCount(); i++) {
                                AssetView asset = (AssetView) assetWidget.getAdapter().get(i);
                                for (AssetDetailWidget detail : asset.getListWidget()) {
                                    if (!detail.isPhoto()) {
                                        isphoto = false;
                                        break;
                                    }
                                }
                                if (!isphoto) {
                                    break;
                                }
                            }
                            if (isphoto) {
                                isEntry = true;
                                dialog.dismiss();
                            } else {
                                Util.showDialogInfo(con, "Ada photo yang belum lengkap!");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    public void show() {
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public AssetWidget getAssetWidget() {
        return assetWidget;
    }
}
