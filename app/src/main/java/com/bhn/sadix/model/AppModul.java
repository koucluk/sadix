package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class AppModul implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3603923798688820699L;
	public String AppModulId;
	public String AppModulValue;
	public AppModul(String appModulId, String appModulValue) {
		AppModulId = appModulId;
		AppModulValue = appModulValue;
	}
	public AppModul(Cursor cursor) {
		AppModulId = cursor.getString(0);
		AppModulValue = cursor.getString(1);
	}

}
