package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class TakingOrderModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5325268679869994823L;
    public String CustId;
    public String SKUId;
    public String tanggal;
    public String SalesProgramId;
    public int QTY_B;
    public int QTY_K;
    public double DISCOUNT1;
    public double DISCOUNT2;
    public double DISCOUNT3;
    public double TOTAL_PRICE;
    public String PAYMENT_TERM;
    public String note;

    public double PRICE_B;
    public double PRICE_K;
    public int LAMA_KREDIT;
    public String DiscountType;
    public String CommonID;
    public String RandomID;

    public String PrinsipleID = "";

    public TakingOrderModel() {
    }

    public TakingOrderModel(String custId, String sKUId, String tanggal,
                            String salesProgramId, int qTY_B, int qTY_K, double dISCOUNT1,
                            double dISCOUNT2, double dISCOUNT3, double tOTAL_PRICE,
                            String pAYMENT_TERM, String note, double pRICE_B, double pRICE_K,
                            int lAMA_KREDIT, String discountType, String CommonID, String RandomID) {
        CustId = custId;
        SKUId = sKUId;
        this.tanggal = tanggal;
        SalesProgramId = salesProgramId;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        DISCOUNT1 = dISCOUNT1;
        DISCOUNT2 = dISCOUNT2;
        DISCOUNT3 = dISCOUNT3;
        TOTAL_PRICE = tOTAL_PRICE;
        PAYMENT_TERM = pAYMENT_TERM;
        this.note = note;
        PRICE_B = pRICE_B;
        PRICE_K = pRICE_K;
        LAMA_KREDIT = lAMA_KREDIT;
        DiscountType = discountType;
        this.CommonID = CommonID;
        this.RandomID = RandomID;
    }

    public TakingOrderModel(String custId, String sKUId, String tanggal,
                            String salesProgramId, int qTY_B, int qTY_K, double dISCOUNT1,
                            double dISCOUNT2, double dISCOUNT3, double tOTAL_PRICE,
                            String pAYMENT_TERM, String note, double pRICE_B, double pRICE_K,
                            int lAMA_KREDIT, String discountType, String CommonID, String RandomID, String PrinsipleID) {
        CustId = custId;
        SKUId = sKUId;
        this.tanggal = tanggal;
        SalesProgramId = salesProgramId;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        DISCOUNT1 = dISCOUNT1;
        DISCOUNT2 = dISCOUNT2;
        DISCOUNT3 = dISCOUNT3;
        TOTAL_PRICE = tOTAL_PRICE;
        PAYMENT_TERM = pAYMENT_TERM;
        this.note = note;
        PRICE_B = pRICE_B;
        PRICE_K = pRICE_K;
        LAMA_KREDIT = lAMA_KREDIT;
        DiscountType = discountType;
        this.CommonID = CommonID;
        this.RandomID = RandomID;
        this.PrinsipleID = PrinsipleID;
    }

    public TakingOrderModel(Cursor cursor) {
        CustId = cursor.getString(0);
        SKUId = cursor.getString(1);
        this.tanggal = cursor.getString(2);
        SalesProgramId = cursor.getString(3);
        QTY_B = cursor.getInt(4);
        QTY_K = cursor.getInt(5);
        DISCOUNT1 = cursor.getDouble(6);
        DISCOUNT2 = cursor.getDouble(7);
        DISCOUNT3 = cursor.getDouble(8);
        TOTAL_PRICE = cursor.getDouble(9);
        PAYMENT_TERM = cursor.getString(10);
        this.note = cursor.getString(11);
        PRICE_B = cursor.getDouble(12);
        PRICE_K = cursor.getDouble(13);
        LAMA_KREDIT = cursor.getInt(14);
        this.DiscountType = cursor.getString(15);
        this.CommonID = cursor.getString(16);
        this.RandomID = cursor.getString(17);
        PrinsipleID = cursor.getString(18);
    }

    public TakingOrderModel(String sKUId) {
        SKUId = sKUId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TakingOrderModel) {
            TakingOrderModel model = (TakingOrderModel) o;
            return (model.SKUId.equals(SKUId));
        }
        return false;
    }

}
