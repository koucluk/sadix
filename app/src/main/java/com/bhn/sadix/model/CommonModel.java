package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;
import java.util.UUID;

public class CommonModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7447906681417559373L;
    public String SC;
    public String CustomerId;
    public String BeginVisitTime;
    public String EndVisitTime;
    public String LogTime;
    public String SalesId;
    public double S_AMOUNT;
    public String Description = "";
    public String GeoLat;
    public String GeoLong;

    public int QTY_B;
    public int QTY_K;
    public double PRICE;
    public double DISCOUNT;
    public int Visit;
    public int RVisitID;
    public String InvNO;
    public String Status;
    public String CommonID;
    public String D;
    public String ROrderID;
    public int StatusID;
    public String CustomerRID;
    public int Done;

    public CommonModel() {
//		CommonID = Util.FORMAT_ID.format(new Date());
        CommonID = UUID.randomUUID().toString();
        Status = "0";
    }

    public CommonModel(String customerId) {
        CustomerId = customerId;
//		CommonID = Util.FORMAT_ID.format(new Date());
        CommonID = UUID.randomUUID().toString();
        Status = "0";
    }

    public CommonModel(String sC, String customerId, String beginVisitTime,
                       String endVisitTime, String logTime, String salesId,
                       double s_AMOUNT, String description, String geoLat, String geoLong,
                       int qTY_B, int qTY_K, double pRICE, double dISCOUNT, String rOrderID, int statusID) {
//		CommonID = Util.FORMAT_ID.format(new Date());
        CommonID = UUID.randomUUID().toString();
        SC = sC;
        CustomerId = customerId;
        BeginVisitTime = beginVisitTime;
        EndVisitTime = endVisitTime;
        LogTime = logTime;
        SalesId = salesId;
        S_AMOUNT = s_AMOUNT;
        Description = description;
        GeoLat = geoLat;
        GeoLong = geoLong;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        PRICE = pRICE;
        DISCOUNT = dISCOUNT;
        Status = "0";
        ROrderID = rOrderID;
        StatusID = statusID;
    }

    public CommonModel(Cursor cursor) {
        SC = cursor.getString(0);
        CustomerId = cursor.getString(1);
        BeginVisitTime = cursor.getString(2);
        EndVisitTime = cursor.getString(3);
        LogTime = cursor.getString(4);
        SalesId = cursor.getString(5);
        S_AMOUNT = cursor.getDouble(6);
        Description = cursor.getString(7);
        GeoLat = cursor.getString(8);
        GeoLong = cursor.getString(9);
        QTY_B = cursor.getInt(10);
        QTY_K = cursor.getInt(11);
        PRICE = cursor.getDouble(12);
        DISCOUNT = cursor.getDouble(13);
        Visit = Integer.parseInt(cursor.getString(14));
        RVisitID = Integer.parseInt(cursor.getString(15));
        InvNO = cursor.getString(16);
        Status = cursor.getString(17);
        CommonID = cursor.getString(18);
        D = cursor.getString(19);
        ROrderID = cursor.getString(20);
        StatusID = cursor.getInt(21);
        CustomerRID = cursor.getString(22);
        Done = cursor.getInt(23);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CommonModel) {
            CommonModel model = (CommonModel) o;
            return model.CommonID.equals(CommonID);
        }
        return false;
    }

}
