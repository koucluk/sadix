package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class GroupSKUComModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8213629262263699531L;
	public String GroupId;
	public String GroupName;
	public GroupSKUComModel() {
	}
	public GroupSKUComModel(String groupId) {
		GroupId = groupId;
	}
	public GroupSKUComModel(String groupId, String groupName) {
		GroupId = groupId;
		GroupName = groupName;
	}
	public GroupSKUComModel(Cursor cursor) {
		GroupId = cursor.getString(0);
		GroupName = cursor.getString(1);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof GroupSKUComModel) {
			GroupSKUComModel model = (GroupSKUComModel) o;
			if(model.GroupId != null) {
				return model.GroupId.equals(GroupId);
			}
		}
		return false;
	}
	@Override
	public String toString() {
		return GroupName;
	}

}
