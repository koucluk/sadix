package com.bhn.sadix.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

public class PriceMonitoringModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -191465849703379448L;
    public SKUModel skuModel;
    public SKUComModel skuComModel;

    public long Qty;
    public long BuyPrice;
    public long SellPrice;
    public int LevelID;
    public String Note;
    private String RandomID;

    public PriceMonitoringModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public PriceMonitoringModel(SKUModel skuModel) {
        this();
        this.skuModel = skuModel;
    }

    public PriceMonitoringModel(SKUComModel skuComModel) {
        this();
        this.skuComModel = skuComModel;
    }

    //	@Override
//	public boolean equals(Object o) {
//		if(o instanceof PriceMonitoringModel) {
//			PriceMonitoringModel model = (PriceMonitoringModel) o;
//			return model.skuModel.SKUId.equals(skuModel.SKUId);
//		}
//		return false;
//	}
    public JSONObject toJson(String CommonID) {
        JSONObject json = new JSONObject();
        try {
            if (skuModel != null) {
                json.put("SKUId", skuModel.SKUId);
                json.put("ProductID", 1);
            } else if (skuComModel != null) {
                json.put("SKUId", skuComModel.SKUId);
                json.put("ProductID", 0);
            }
            json.put("Qty", Qty);
            json.put("BuyPrice", BuyPrice);
            json.put("SellPrice", SellPrice);
            json.put("LevelID", LevelID);
            json.put("Note", Note);
            json.put("RandomID", RandomID);
            json.put("CommonID", CommonID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public PriceMonitoringModel fromJson(JSONObject object) {
        PriceMonitoringModel model = new PriceMonitoringModel();
        try {
            model.Qty = object.getInt("Qty");
            model.BuyPrice = object.getLong("BuyPrice");
            model.SellPrice = object.getLong("SellPrice");
            model.LevelID = object.getInt("LevelID");
            model.Note = object.getString("Note");
            model.RandomID = object.getString("RandomID");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*if (skuModel != null) {
            json.put("SKUId", skuModel.SKUId);
            json.put("ProductID", 1);
        } else if (skuComModel != null) {
            json.put("SKUId", skuComModel.SKUId);
            json.put("ProductID", 0);
        }

        json.put("Qty", Qty);
        json.put("BuyPrice", BuyPrice);
        json.put("SellPrice", SellPrice);
        json.put("LevelID", LevelID);
        json.put("Note", Note);
        json.put("RandomID", RandomID);
        json.put("CommonID", CommonID);*/
        return model;
    }

}
