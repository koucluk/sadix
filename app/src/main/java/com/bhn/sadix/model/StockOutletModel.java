package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class StockOutletModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8505629326615903042L;
    public String CustId;
    public String tanggal;
    public String SKUId;
    public int QTY_B;
    public int QTY_K;
    public String CommonID;
    public String RandomID;

    public StockOutletModel() {
    }

    public StockOutletModel(String custId, String tanggal, String sKUId,
                            int qTY_B, int qTY_K, String commonID, String RandomID) {
        CustId = custId;
        this.tanggal = tanggal;
        SKUId = sKUId;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        CommonID = commonID;
        this.RandomID = RandomID;
    }

    public StockOutletModel(Cursor cursor) {
        CustId = cursor.getString(0);
        tanggal = cursor.getString(1);
        SKUId = cursor.getString(2);
        QTY_B = cursor.getInt(3);
        QTY_K = cursor.getInt(4);
        CommonID = cursor.getString(5);
        RandomID = cursor.getString(6);
    }

}
