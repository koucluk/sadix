package com.bhn.sadix.model;

import java.io.Serializable;

public class StokCompetitorModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -3789216100511240449L;
    public CustomerModel customerModel;
    public SKUComModel skuModel;
    public int QTY_B;
    public int QTY_K;
    public double PRICE;
    public double PRICE2;
    public String Note;
    public String RandomID;

    public StokCompetitorModel() {
    }

    public StokCompetitorModel(CustomerModel customerModel, SKUComModel skuModel,
                               int qTY_B, int qTY_K, double PRICE, double PRICE2, String Note, String RandomID) {
        this.customerModel = customerModel;
        this.skuModel = skuModel;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        this.PRICE = PRICE;
        this.PRICE2 = PRICE2;
        this.Note = Note;
        this.RandomID = RandomID;
    }

}
