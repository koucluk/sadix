package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class SurveyModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7859564204616930751L;
    public String FormId;
    public String SurveyId;
    public String SurveyName;
    public String SurveyType;

    public SurveyModel(String formId, String surveyId, String surveyName,
                       String surveyType) {
        super();
        FormId = formId;
        SurveyId = surveyId;
        SurveyName = surveyName;
        SurveyType = surveyType;
    }

    public SurveyModel(Cursor cursor) {
        FormId = cursor.getString(0);
        SurveyId = cursor.getString(1);
        SurveyName = cursor.getString(2);
        SurveyType = cursor.getString(3);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SurveyModel) {
            SurveyModel model = (SurveyModel) o;
            return (model.FormId.equals(FormId) && model.SurveyId.equals(SurveyId));
        }
        return false;
    }

}
