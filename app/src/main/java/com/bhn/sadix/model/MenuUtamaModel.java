package com.bhn.sadix.model;

import java.io.Serializable;

import android.graphics.drawable.Drawable;

public class MenuUtamaModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2252936934450052575L;
    public int id;
    public String menu;
    public Drawable drawable;
    public int color = -1;
    public boolean isCaption = false;
    public String caption;

    public MenuUtamaModel(int id) {
        this.id = id;
    }

    public MenuUtamaModel(String menu) {
        super();
        this.menu = menu;
    }

    public MenuUtamaModel(int id, String menu, Drawable drawable) {
        this.id = id;
        this.menu = menu;
        this.drawable = drawable;
    }

    public MenuUtamaModel(int id, String menu, int color, boolean isCaption,
                          String caption) {
        super();
        this.id = id;
        this.menu = menu;
        this.color = color;
        this.isCaption = isCaption;
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MenuUtamaModel) {
            MenuUtamaModel model = (MenuUtamaModel) o;
//			return (model.menu.equalsIgnoreCase(menu));
            return model.id == id;
        }
        return false;
    }

}
