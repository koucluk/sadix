package com.bhn.sadix.model;

import android.database.Cursor;

import com.bhn.sadix.database.DbMasterHelper;
import com.bhn.sadix.util.Util;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TargetModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -590257162577933868L;
    public SKUModel skuModel;
    public List<Detail> details;
    private int CustID;
    private int SalesID;

    //	public TargetModel() {
//		details = new ArrayList<TargetModel.Detail>();
//		Calendar calendar = Calendar.getInstance();
//		details.add(new Detail(1, calendar.get(calendar.YEAR)));
//		details.add(new Detail(2, calendar.get(calendar.YEAR)));
//		details.add(new Detail(3, calendar.get(calendar.YEAR)));
//		details.add(new Detail(4, calendar.get(calendar.YEAR)));
//		details.add(new Detail(5, calendar.get(calendar.YEAR)));
//		details.add(new Detail(6, calendar.get(calendar.YEAR)));
//		details.add(new Detail(7, calendar.get(calendar.YEAR)));
//		details.add(new Detail(8, calendar.get(calendar.YEAR)));
//		details.add(new Detail(9, calendar.get(calendar.YEAR)));
//		details.add(new Detail(10, calendar.get(calendar.YEAR)));
//		details.add(new Detail(11, calendar.get(calendar.YEAR)));
//		details.add(new Detail(12, calendar.get(calendar.YEAR)));
//	}
//	public TargetModel(SKUModel skuModel) {
//		this();
//		this.skuModel = skuModel;
//	}
    public TargetModel(SKUModel skuModel, Cursor cursor, DbMasterHelper ctrlDb, CustomerModel customerModel, String SalesID) {
        this.skuModel = skuModel;
        CustID = Integer.parseInt(customerModel.CustId);
        this.SalesID = Integer.parseInt(SalesID);
        details = new ArrayList<TargetModel.Detail>();
        while (cursor.moveToNext()) {
            Detail detail = new Detail(cursor.getInt(1), cursor.getInt(0), cursor.getInt(2));
            String sql = "select Qty,Het from producttarget where "
                    + "CustID='" + customerModel.CustId + "' "
                    + "and SkuID='" + skuModel.SKUId + "' "
                    + "and Month='" + detail.Bln + "' "
                    + "and Year='" + detail.Thn + "' "
                    + "and St_Target='0'";
//			android.util.Log.e("sql", sql);
            Cursor cr = ctrlDb.query(sql);
            if (cr.moveToNext()) {
                detail.Qty = cr.getLong(0);
                detail.Price = cr.getLong(1);
            }
            cr.close();
            details.add(detail);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TargetModel) {
            TargetModel model = (TargetModel) o;
            return model.skuModel.SKUId.equals(skuModel.SKUId);
        }
        return false;
    }

    public List<JSONObject> listJson(String CommonID) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (Detail detail : details) {
            list.add(detail.toJson(CommonID));
        }
        return list;
    }

    public class Detail {
        public int Bln;
        public int Thn;
        public long Qty;
        public long Price;
        public String RandomID;
        public int Lock;
        public String LogTime;
        //	    private String[] blnarr = {"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"};
        private String[] blnarr = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        public Detail(int bln, int thn) {
            Bln = bln;
            Thn = thn;
            RandomID = UUID.randomUUID().toString();
            LogTime = Util.dateFormat.format(new Date());
        }

        public Detail(int bln, int thn, int lock) {
            this(bln, thn);
            Lock = lock;
        }

        public JSONObject toJson(String CommonID) {
            JSONObject json = new JSONObject();
            try {
                json.put("SKUId", skuModel.SKUId);
                json.put("Bln", Bln);
                json.put("Thn", Thn);
                json.put("Qty", Qty);
                json.put("Price", Price);
                json.put("RandomID", RandomID);
                json.put("CommonID", CommonID);
                json.put("StTarget", "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return json;
        }

        public JSONObject toJsonSalesTarget() {
            JSONObject json = new JSONObject();
            try {
                json.put("SKUId", skuModel.SKUId);
                json.put("Bln", Bln);
                json.put("Thn", Thn);
                json.put("Qty", Qty);
                json.put("Price", Price);
                json.put("StTarget", 0);
                json.put("LogTime", LogTime);
                json.put("CustID", CustID);
                json.put("SalesID", SalesID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return json;
        }

        public JSONObject toSaveSalesTarget() {
            JSONObject json = new JSONObject();
            try {
                json.put("SkuID", skuModel.SKUId);
                json.put("Month", Bln);
                json.put("Year", Thn);
                json.put("Qty", Qty);
                json.put("St_Target", 0);
                json.put("Lock", 0);
                json.put("CustID", CustID);
                json.put("Het", Price);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return json;
        }

        public String getBulan() {
            return blnarr[Bln - 1] + " " + Thn;
        }
    }

}
