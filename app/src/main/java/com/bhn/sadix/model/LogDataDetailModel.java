package com.bhn.sadix.model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class LogDataDetailModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3898454328205758177L;
    public List<StockOutletModel> stockOutletModel;
    public List<StockCompetitorModel> stockCompetitorModel;
    public List<TakingOrderModel> takingOrderModel;
    public List<ReturModel> returModel;
    public List<CollectorModel> collectorModel;
    public List<JSONObject> listPriceMonitoring;

    public LogDataDetailModel(List<StockOutletModel> stockOutletModel,
                              List<StockCompetitorModel> stockCompetitorModel,
                              List<TakingOrderModel> takingOrderModel,
                              List<ReturModel> returModel, List<CollectorModel> collectorModel, List<JSONObject> listPriceMonitoring) {
        super();
        this.stockOutletModel = stockOutletModel;
        this.stockCompetitorModel = stockCompetitorModel;
        this.takingOrderModel = takingOrderModel;
        this.returModel = returModel;
        this.collectorModel = collectorModel;
        this.listPriceMonitoring = listPriceMonitoring;
    }

}
