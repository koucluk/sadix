package com.bhn.sadix.model;

import java.io.Serializable;

public class CrcOrderStokModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3500333141859531328L;
	public SKUModel skuModel;
	public OrderDetailModel order;
	public StokCustomerModel stock;
	public CrcOrderModel lastOrder;
	public CrcStockModel lastStock;
	public String ReasonOrder = "";
	public String ReasonStock = "";
	public int QTY_B_REK = 0;
	public int QTY_K_REK = 0;
	public CrcOrderStokModel(SKUModel skuModel) {
		this.skuModel = skuModel;
		order = new OrderDetailModel(skuModel);
		stock = new StokCustomerModel(skuModel);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof OrderDetailModel) {
			OrderDetailModel model = (OrderDetailModel) o;
			return model.skuModel.SKUId.equals(skuModel.SKUId);
		}
		return false;
	}

}
