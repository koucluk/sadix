package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class InformasiModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7214725689444058130L;
	public String Header;
	public String Content;
	public String DateValidity;
	public InformasiModel() {
	}
	public InformasiModel(String header, String content, String dateValidity) {
		Header = header;
		Content = content;
		DateValidity = dateValidity;
	}
	public InformasiModel(Cursor cursor) {
		Header = cursor.getString(0);
		Content = cursor.getString(1);
		DateValidity = cursor.getString(2);
	}
}
