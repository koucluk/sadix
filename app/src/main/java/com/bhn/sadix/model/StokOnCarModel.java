package com.bhn.sadix.model;

import java.io.Serializable;

public class StokOnCarModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3234035668411343797L;
    public SKUModel skuModel;
    public int QTY_B;
    public int QTY_K;

    public StokOnCarModel() {
    }

    public StokOnCarModel(SKUModel skuModel, int qTY_B, int qTY_K) {
        this.skuModel = skuModel;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
    }

}
