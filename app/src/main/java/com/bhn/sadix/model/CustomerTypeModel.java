package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class CustomerTypeModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1475411135237505835L;
	public String CustomerTypeId;
	public String Description;
	public String SC;
	public String DiskonType;
	public double DiskonMax;
	public CustomerTypeModel() {
	}
	public CustomerTypeModel(String customerTypeId, String description,
			String sC, String diskonType, double diskonMax) {
		CustomerTypeId = customerTypeId;
		Description = description;
		SC = sC;
		DiskonType = diskonType;
		DiskonMax = diskonMax;
	}
	public CustomerTypeModel(Cursor cursor) {
		CustomerTypeId = cursor.getString(0);
		Description = cursor.getString(1);
		SC = cursor.getString(2);
		DiskonType = cursor.getString(3);
		DiskonMax = cursor.getDouble(4);
	}
}
