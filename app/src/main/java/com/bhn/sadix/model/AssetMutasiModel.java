package com.bhn.sadix.model;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bhn.sadix.R;
import com.bhn.sadix.adapter.AssetMutasiAdapter;
import com.bhn.sadix.database.DbAsetHelper;
import com.bhn.sadix.util.Util;
import com.bhn.sadix.widget.AssetMutasiWidget;

import org.json.JSONObject;

public class AssetMutasiModel {
    public String ASETNAME;
    public String ASETID;
    public String ASETNO;
    public AssetMutasiAdapter adapter;
    private AssetMutasiWidget assetMutasiWidget;
    private Dialog dialog;
    private DbAsetHelper dbAsetHelper;
    private String CUSTID = null;
    private Context context;
    private Activity activity;
    private AssetMutasiEvent event;
    private int jenis;
    public boolean isEdit = false;
    public String CommonID;

    public AssetMutasiModel(String aSETNO, int jenis) {
        ASETNO = aSETNO;
        this.jenis = jenis;
    }

    public AssetMutasiModel(Context context, String ASETNAME, String ASETID, String ASETNO, int JENIS, String cUSTID, AssetMutasiEvent evt, String NoSeri, String Ket, String CommonID) {
        this.context = context;
        activity = (Activity) context;
        this.event = evt;
        jenis = JENIS;
        this.CommonID = CommonID;
        assetMutasiWidget = new AssetMutasiWidget(context, ASETID, ASETNAME, ASETNO, JENIS, NoSeri, Ket, CommonID);
        dialog = new Dialog(context);
        dbAsetHelper = new DbAsetHelper(context);
        this.ASETNAME = ASETNAME;
        this.ASETID = ASETID;
        this.ASETNO = ASETNO;
        this.CUSTID = cUSTID;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(assetMutasiWidget);
                dialog.findViewById(R.id.btn_cancel).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (event != null) {
                            event.cancel(AssetMutasiModel.this);
                        }
                    }
                });
                dialog.findViewById(R.id.btn_ok).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (assetMutasiWidget.getJenis() == 1) {
                            JSONObject json = assetMutasiWidget.toJSON();
                            try {
                                if (json.getString("NoSeri").equals("")) {
                                    Util.showDialogInfo(AssetMutasiModel.this.context, "isi nomor seri terlebih dahulu!");
                                    return;
                                }
                                AssetMutasiModel.this.ASETNAME = json.getString("AsetName");
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }
                                dbAsetHelper.saveAssetData(
                                        Integer.parseInt(CUSTID),
                                        json.getString("AsetID"),
                                        json.getInt("TypeID"),
                                        json.getInt("MerkID"),
                                        json.getString("AsetName"),
                                        json.getString("NoSeri")
                                );
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (event != null) {
                            event.add(AssetMutasiModel.this);
                        }
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    public AssetMutasiWidget getAssetMutasiWidget() {
        return assetMutasiWidget;
    }

    public void show() {
        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AssetMutasiModel) {
            AssetMutasiModel model = (AssetMutasiModel) o;
            return (ASETNO.equals(model.ASETNO));
        }
        return false;
    }

    public interface AssetMutasiEvent {
        void add(AssetMutasiModel model);

        void cancel(AssetMutasiModel model);
    }
}
