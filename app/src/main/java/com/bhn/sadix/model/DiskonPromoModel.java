package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.UUID;

public class DiskonPromoModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3766157116453314872L;
    //    public SKUModel skuModel;
    public String skuModel;
    public int DiscountResultID;
    public int QTY;
    public String SATUAN;
    public String RandomID;

    public DiskonPromoModel() {
        RandomID = UUID.randomUUID().toString();
    }

    /*public DiskonPromoModel(SKUModel skuModel) {
        this();
        this.skuModel = skuModel;
    }*/

    public DiskonPromoModel(String name) {
        skuModel = name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DiskonPromoModel) {
            DiskonPromoModel model = (DiskonPromoModel) o;
            return model.skuModel.equals(skuModel);
        }
        return false;
    }

}
