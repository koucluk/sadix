package com.bhn.sadix.model;

import java.util.List;

/**
 * Created by map05 on 3/2/2017.
 */

public class Chart {
    String Title;
    int TypeChart;
    List<DataChart> Data;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getTypeChat() {
        return TypeChart;
    }

    public void setTypeChat(int typeChart) {
        TypeChart = typeChart;
    }

    public List<DataChart> getData() {
        return Data;
    }

    public void setData(List<DataChart> data) {
        Data = data;
    }
}
