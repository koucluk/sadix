package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.Date;

import com.bhn.sadix.util.Util;

public class HistoryModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4065992368450811076L;
	public String TransactionId;
	public String TransactionTime;
	public String CustomerId;
	public double Total;
	public double Diskon;
	public double TypeDiskon;
	public double GrandTotal;
	public String Desc;
	public HistoryModel(String transactionId, String transactionTime,
			String customerId, double total, double diskon, double typeDiskon,
			double grandTotal, String desc) {
		super();
		TransactionId = transactionId;
		TransactionTime = transactionTime;
		CustomerId = customerId;
		Total = total;
		Diskon = diskon;
		TypeDiskon = typeDiskon;
		GrandTotal = grandTotal;
		Desc = desc;
	}
	public Date getTransactionTime() {
		try {
			return Util.dateFormat.parse(TransactionTime);
		} catch (Exception e) {
			return null;
		}
	}
	public String getTransactionTimeFormat() {
		Date date = getTransactionTime();
		if(date != null) {
			return Util.dateFormatString.format(date);
		} else {
			return "";
		}
	}
	public String getTotalFormat() {
		return Util.numberFormat.format(Total);
	}
	public String getDiskonFormat() {
		return Util.numberFormat.format(Diskon);
	}
	public String getGrandTotalFormat() {
		return Util.numberFormat.format(GrandTotal);
	}

}
