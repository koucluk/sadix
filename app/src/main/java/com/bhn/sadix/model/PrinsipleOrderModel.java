package com.bhn.sadix.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;

public class PrinsipleOrderModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4489062315274332063L;
    public int PrinsipleId;
    public double G_AMOUNT;
    public double D_VALUE;
    public double S_AMOUNT;
    public int total_k;
    public int total_b;
    public String CommonID;
    public String RandomID;

    public PrinsipleOrderModel() {
    }

    public PrinsipleOrderModel(int prinsipleId, int total_B, int total_K, double g_AMOUNT,
                               double d_VALUE, double s_AMOUNT, String commonID, String randomID) {
        PrinsipleId = prinsipleId;
        G_AMOUNT = g_AMOUNT;
        D_VALUE = d_VALUE;
        S_AMOUNT = s_AMOUNT;
        CommonID = commonID;
        RandomID = randomID;
        total_k = total_K;
        total_b = total_B;
    }

    public PrinsipleOrderModel(Cursor cursor) {
        PrinsipleId = cursor.getInt(0);
        G_AMOUNT = cursor.getDouble(1);
        D_VALUE = cursor.getDouble(2);
        S_AMOUNT = cursor.getDouble(3);
        CommonID = cursor.getString(4);
        RandomID = cursor.getString(5);
        total_k = cursor.getInt(6);
        total_b = cursor.getInt(7);
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("PrinsipleId", PrinsipleId);
            json.put("G_AMOUNT", G_AMOUNT);
            json.put("D_VALUE", D_VALUE);
            json.put("S_AMOUNT", S_AMOUNT);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}
