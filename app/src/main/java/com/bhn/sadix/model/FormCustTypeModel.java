package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class FormCustTypeModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5714174195907834668L;
	public String FormId;
	public String Id;
	public FormCustTypeModel(String formId, String id) {
		super();
		FormId = formId;
		Id = id;
	}
	public FormCustTypeModel(Cursor cursor) {
		FormId = cursor.getString(0);
		Id = cursor.getString(1);
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof FormCustTypeModel) {
			FormCustTypeModel model = (FormCustTypeModel) o; 
			return (model.FormId.equals(FormId) && model.Id.equals(Id));
		}
		return false;
	}

}
