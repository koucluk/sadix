package com.bhn.sadix.model;

/**
 * Created by map05 on 3/2/2017.
 */

public class DataChart {
    String Color;
    String Label;
    int Value;

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }
}
