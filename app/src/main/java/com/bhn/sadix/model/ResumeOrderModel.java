package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;

public class ResumeOrderModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1443385033056558196L;
    public String SKUId;
    public String ProductName;
    public int QTY_B;
    public int QTY_K;
    public double TOTAL_PRICE;

    public ResumeOrderModel() {
    }

    public ResumeOrderModel(Cursor cursor) {
        SKUId = cursor.getString(0);
        ProductName = cursor.getString(1);
        QTY_B = cursor.getInt(2);
        QTY_K = cursor.getInt(3);
        try {
            TOTAL_PRICE = cursor.getDouble(4);
        } catch (Exception e) {
            TOTAL_PRICE = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ResumeOrderModel) {
            ResumeOrderModel model = (ResumeOrderModel) o;
            return (model.SKUId.equals(SKUId));
        }
        return false;
    }

}
