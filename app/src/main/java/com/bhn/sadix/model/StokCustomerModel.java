package com.bhn.sadix.model;

import java.io.Serializable;

public class StokCustomerModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4465049110104693007L;
    public CustomerModel customerModel;
    public SKUModel skuModel;
    public int QTY_B;
    public int QTY_K;
    public String RandomID = "";
    public String ReasonID = "";

    public StokCustomerModel() {
    }

    public StokCustomerModel(SKUModel skuModel) {
        this.skuModel = skuModel;
    }

    public StokCustomerModel(CustomerModel customerModel, SKUModel skuModel,
                             int qTY_B, int qTY_K, String RandomID) {
        this.customerModel = customerModel;
        this.skuModel = skuModel;
        QTY_B = qTY_B;
        QTY_K = qTY_K;
        this.RandomID = RandomID;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof StokCustomerModel) {
            StokCustomerModel model = (StokCustomerModel) o;
            return model.skuModel.SKUId.equals(skuModel.SKUId);
        }
        return false;
    }

    public int getQty() {
        return (int) ((QTY_B * skuModel.CONVER) + QTY_K);
    }

}
