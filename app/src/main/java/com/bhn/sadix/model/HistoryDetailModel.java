package com.bhn.sadix.model;

import java.io.Serializable;

import com.bhn.sadix.util.Util;

public class HistoryDetailModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4600845743813519195L;
	public String TransactionId;
	public String TransactionIds;
	public String GroupName;
	public String Name;
	public double Price;
	public int Qty;
	public double Price2;
	public int Qty2;
	public String Satuan1;
	public String Satuan2;
	public double Discount1;
	public double Discount2;
	public double Discount3;
	public double Total;
	public double GrandTotal;
	public double T_Discount;
	public String SalesProgramDescription;
	public HistoryDetailModel(String transactionId, String transactionIds,
			String groupName, String name, double price, int qty,
			double price2, int qty2, String satuan1, String satuan2,
			double discount1, double discount2, double discount3, double total,
			double grandTotal, double t_Discount, String salesProgramDescription) {
		TransactionId = transactionId;
		TransactionIds = transactionIds;
		GroupName = groupName;
		Name = name;
		Price = price;
		Qty = qty;
		Price2 = price2;
		Qty2 = qty2;
		Satuan1 = satuan1;
		Satuan2 = satuan2;
		Discount1 = discount1;
		Discount2 = discount2;
		Discount3 = discount3;
		Total = total;
		GrandTotal = grandTotal;
		T_Discount = t_Discount;
		SalesProgramDescription = salesProgramDescription;
	}
	public String getGrandTotalFormat() {
		return Util.numberFormat.format(GrandTotal);
	}
	public String getPriceFormat() {
		return Util.numberFormat.format(Price);
	}
	public String getPrice2Format() {
		return Util.numberFormat.format(Price2);
	}
	public String getTotalFormat() {
		return Util.numberFormat.format(Total);
	}
	public String getQtyFormat() {
		return Util.numberFormat.format(Qty);
	}
	public String getQty2Format() {
		return Util.numberFormat.format(Qty2);
	}
	public String getDiscount1Format() {
		return Util.numberFormat.format(Discount1);
	}
	public String getDiscount2Format() {
		return Util.numberFormat.format(Discount2);
	}
	public String getDiscount3Format() {
		return Util.numberFormat.format(Discount3);
	}
	public String getT_DiscountFormat() {
		return Util.numberFormat.format(T_Discount);
	}

}
