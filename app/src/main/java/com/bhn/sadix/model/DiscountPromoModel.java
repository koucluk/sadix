package com.bhn.sadix.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

public class DiscountPromoModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3097017013182255551L;
    public int DiscountPromoID;
    public int CustomerId;
    public String CommonID;
    public String RandomID;

    public DiscountPromoModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public DiscountPromoModel(int discountPromoID, int customerId, String commonID/*, String RandomID*/) {
        this();
        DiscountPromoID = discountPromoID;
        CustomerId = customerId;
        CommonID = commonID;
//		this.RandomID = RandomID;
    }

    public DiscountPromoModel(Cursor cursor) {
        DiscountPromoID = cursor.getInt(0);
        CustomerId = cursor.getInt(1);
        CommonID = cursor.getString(2);
        RandomID = cursor.getString(3);
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("DiscountPromoID", DiscountPromoID);
            json.put("CustomerId", CustomerId);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
        }
        return json;
    }

}
