package com.bhn.sadix.model;

import java.io.Serializable;

import android.database.Cursor;

public class ConfigModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9118205409756393816L;
	public String ConfigId;
	public String ConfigName;
	public String ConfigClass;
	public ConfigModel() {
	}
	public ConfigModel(String configId, String configName, String configClass) {
		ConfigId = configId;
		ConfigName = configName;
		ConfigClass = configClass;
	}
	public ConfigModel(Cursor cursor) {
		ConfigId = cursor.getString(0);
		ConfigName = cursor.getString(1);
		ConfigClass = cursor.getString(2);
	}

}
