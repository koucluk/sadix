package com.bhn.sadix.model;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class CRCReportModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8472813369461944470L;
	public SKUModel skuModel;
	public String RandomID;
	public List<CrcOrderModel> ordes;
	public List<CrcStockModel> stocks;
	public CRCReportModel() {
		RandomID = UUID.randomUUID().toString();
	}
	public CRCReportModel(SKUModel skuModel) {
		this();
		this.skuModel = skuModel;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof OrderDetailModel) {
			OrderDetailModel model = (OrderDetailModel) o;
			return model.skuModel.SKUId.equals(skuModel.SKUId);
		}
		return false;
	}

}
