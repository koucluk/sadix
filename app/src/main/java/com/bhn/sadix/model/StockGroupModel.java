package com.bhn.sadix.model;

import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

public class StockGroupModel implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1149498577739589633L;
    public GroupSKUModel groupModel;
    public String GroupId;
    public String CustomerId;
    public int Quantity;
    public int Quantity2;
    public String CommonID;
    public String RandomID;

    public StockGroupModel() {
        RandomID = UUID.randomUUID().toString();
    }

    public StockGroupModel(GroupSKUModel groupModel, String CustomerId, String CommonID) {
        this();
        this.groupModel = groupModel;
        GroupId = groupModel.GroupId;
        this.CustomerId = CustomerId;
        this.CommonID = CommonID;
    }

    //	public StockGroupModel(String groupId, String customerId, int quantity,
//			int quantity2, String commonID) {
//		GroupId = groupId;
//		CustomerId = customerId;
//		Quantity = quantity;
//		Quantity2 = quantity2;
//		CommonID = commonID;
//	}
    public StockGroupModel(Cursor cursor) {
        GroupId = cursor.getString(0);
        CustomerId = cursor.getString(1);
        Quantity = cursor.getInt(2);
        Quantity2 = cursor.getInt(3);
        CommonID = cursor.getString(4);
        RandomID = cursor.getString(5);
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("GroupId", Integer.parseInt(GroupId));
            json.put("CustomerId", Integer.parseInt(CustomerId));
            json.put("Quantity", Quantity);
            json.put("Quantity2", Quantity2);
            json.put("CommonID", CommonID);
            json.put("RandomID", RandomID);
        } catch (Exception e) {
        }
        return json;
    }

}
