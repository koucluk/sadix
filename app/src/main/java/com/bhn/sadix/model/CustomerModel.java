package com.bhn.sadix.model;

import android.database.Cursor;

import java.io.Serializable;
import java.util.UUID;

public class CustomerModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6962058247668840038L;
    public String CustId;
    public String CustomerName;
    public String CustomerAddress;
    public String CustomerEmail;
    public String CustomerPhone;
    public String CustomerFax;
    public String CustomerNPWP;
    public String OwnerName;
    public String OwnerAddress;
    public String OwnerEmail;
    public String OwnerPhone;
    public String CustomerTypeId;
    public String GeoLong;
    public String GeoLat;

    public String D1;
    public String D2;
    public String D3;
    public String D4;
    public String D5;
    public String D6;
    public String D7;
    //database versi 2
    public String isNOO;
    //dataase versi 3
    public double CustomerCrLimit;
    public double CustomerCrBalance;
    public String isNew;
    public String isEdit;
    public String OwnerBirthPlace;
    public String OwnerBirthDate;
    public String OwnerIdNo;
    //database versi 4
    public String CustomerID;
    public String PinID;
    public String CustGroupID;
    public String CustGroupName;
    //----------------------------
    public String OwnerReligion;
    public String OwnerHobby;
    public String Description;
    public int CustomerLevel;
    public int CustomerUpLineID;
    public int CustomerLegal;
    public String RandomID;

    public int CustTop;

    public String CustomerNpwpName;
    public String CustomerNpwpAddress;

    public CustomerModel() {
        isEdit = "1";
        isNOO = "1";
        RandomID = UUID.randomUUID().toString();
    }

    //	public CustomerModel(String custId) {
//		super();
//		CustId = custId;
//		isEdit = "0";
//		isNOO = "0";
//	}
//	public CustomerModel(String custId, String customerName,
//			String customerAddress, String customerEmail, String customerPhone,
//			String customerFax, String customerNPWP, String ownerName,
//			String ownerAddress, String ownerEmail, String ownerPhone,
//			String customerTypeId, String geoLong, String geoLat, String d1,
//			String d2, String d3, String d4, String d5, String d6, String d7,
//			double customerCrLimit, double customerCrBalance, String isNew, String ownerBirthPlace,
//			String ownerBirthDate, String ownerIdNo, String customerID) {
//		super();
//		CustId = custId;
//		CustomerName = customerName;
//		CustomerAddress = customerAddress;
//		CustomerEmail = customerEmail;
//		CustomerPhone = customerPhone;
//		CustomerFax = customerFax;
//		CustomerNPWP = customerNPWP;
//		OwnerName = ownerName;
//		OwnerAddress = ownerAddress;
//		OwnerEmail = ownerEmail;
//		OwnerPhone = ownerPhone;
//		CustomerTypeId = customerTypeId;
//		GeoLong = geoLong;
//		GeoLat = geoLat;
//		D1 = d1;
//		D2 = d2;
//		D3 = d3;
//		D4 = d4;
//		D5 = d5;
//		D6 = d6;
//		D7 = d7;
//		CustomerCrLimit = customerCrLimit;
//		CustomerCrBalance = customerCrBalance;
//		this.isNew = isNew;
//		isEdit = "0";
//		isNOO = "0";
//		OwnerBirthPlace = ownerBirthPlace;
//		OwnerBirthDate = ownerBirthDate;
//		OwnerIdNo = ownerIdNo;
//		CustomerID = customerID;
//	}
    public CustomerModel(Cursor cursor) {
        CustId = cursor.getString(0);
        CustomerName = cursor.getString(1);
        CustomerAddress = cursor.getString(2);
        CustomerEmail = cursor.getString(3);
        CustomerPhone = cursor.getString(4);
        CustomerFax = cursor.getString(5);
        CustomerNPWP = cursor.getString(6);
        OwnerName = cursor.getString(7);
        OwnerAddress = cursor.getString(8);
        OwnerEmail = cursor.getString(9);
        OwnerPhone = cursor.getString(10);
        CustomerTypeId = cursor.getString(11);
        GeoLong = cursor.getString(12);
        GeoLat = cursor.getString(13);
        D1 = cursor.getString(14);
        D2 = cursor.getString(15);
        D3 = cursor.getString(16);
        D4 = cursor.getString(17);
        D5 = cursor.getString(18);
        D6 = cursor.getString(19);
        D7 = cursor.getString(20);
        CustomerCrLimit = cursor.getDouble(21);
        CustomerCrBalance = cursor.getDouble(22);
        isNew = cursor.getString(23);
        isEdit = cursor.getString(24);
        isNOO = cursor.getString(25);
        OwnerBirthPlace = cursor.getString(26);
        OwnerBirthDate = cursor.getString(27);
        OwnerIdNo = cursor.getString(28);
        CustomerID = cursor.getString(29);
        PinID = cursor.getString(30);
        CustGroupID = cursor.getString(31);
        CustGroupName = cursor.getString(32);

        OwnerReligion = cursor.getString(33);
        OwnerHobby = cursor.getString(34);
        CustomerLevel = cursor.getInt(35);
        CustomerUpLineID = cursor.getInt(36);
        CustomerLegal = cursor.getInt(37);
        Description = cursor.getString(38);
        RandomID = cursor.getString(39);

        CustTop = cursor.getInt(40);
        CustomerNpwpName = cursor.getString(41);
        CustomerNpwpAddress = cursor.getString(42);
    }

}
