package com.bhn.sadix.survey;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bhn.sadix.Data.HasilSurveyModel;
import com.bhn.sadix.R;
import com.bhn.sadix.database.CtrlSurvey;
import com.bhn.sadix.database.CtrlSurveyOpt;
import com.bhn.sadix.model.SurveyModel;
import com.bhn.sadix.model.SurveyOptModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.realm.Realm;

public class SurveyWidget extends LinearLayout {
    private LayoutInflater mInflater;
    private Context context;
    public SurveyModel surveyModel;
    private CtrlSurveyOpt ctrlSurveyOpt;
    private CtrlSurvey ctrlSurvey;
    public String ListID;
    public String CommonID;
    Realm realm;

    public SurveyWidget(Context context, SurveyModel surveyModel) {
        super(context);
        this.context = context;
        this.surveyModel = surveyModel;
        ctrlSurveyOpt = new CtrlSurveyOpt(context);
        ctrlSurvey = new CtrlSurvey(context);
        init();
    }

    public SurveyWidget(Context context, SurveyModel surveyModel, String CommonID) {
        super(context);
        this.context = context;
        this.surveyModel = surveyModel;
        ctrlSurveyOpt = new CtrlSurveyOpt(context);
        ctrlSurvey = new CtrlSurvey(context);
        this.CommonID = CommonID;
        realm = Realm.getDefaultInstance();
        init();
    }

    private void init() {
        ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(param);

        mInflater = LayoutInflater.from(context);
        LinearLayout view = (LinearLayout) mInflater.inflate(R.layout.widget_survey, null);
        ViewGroup.LayoutParams param_view = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        view.setLayoutParams(param_view);
        ((TextView) view.findViewById(R.id.text)).setText(surveyModel.SurveyName);
        List<SurveyOptModel> list = ctrlSurveyOpt.listBySurveyId(surveyModel.SurveyId);
        if (surveyModel.SurveyType.equals("0")) {
            view.findViewById(R.id.panelText).setVisibility(View.VISIBLE);
            ListID = list.get(0).ListID;

            List<List<String>> hasil = ctrlSurvey.getHasilSurvey(CommonID, surveyModel.FormId, surveyModel.SurveyId);

            if (hasil != null || hasil.size() > 0) {
                for (List<String> value : hasil) {
                    ((EditText) view.findViewById(R.id.panelText)).setText(value.get(1));
                }
            }

            /*((EditText) view.findViewById(R.id.panelText)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(final Editable editable) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            HasilSurveyModel model;
                            if ((model = realm.where(HasilSurveyModel.class)
                                    .equalTo("FormId", surveyModel.FormId)
                                    .equalTo("SurveyId", surveyModel.SurveyId)
                                    .equalTo("CommonId", CommonID).findFirst()) == null)
                                model = realm.createObject(HasilSurveyModel.class);

                            model.CommonId = CommonID;
                            model.FormId = surveyModel.FormId;
                            model.ListID = ListID;
                            model.SurveyId = surveyModel.SurveyId;
                            model.ListValue = editable.toString();
                        }
                    });
                }
            });*/

        } else if (surveyModel.SurveyType.equals("3")) {
            view.findViewById(R.id.panelNumber).setVisibility(View.VISIBLE);
            ListID = list.get(0).ListID;

            List<List<String>> hasil = ctrlSurvey.getHasilSurvey(CommonID, surveyModel.FormId, surveyModel.SurveyId);

            if (hasil != null || hasil.size() > 0) {
                for (List<String> value : hasil) {
                    ((EditText) view.findViewById(R.id.panelNumber)).setText(value.get(1));
                }

                /*((EditText) view.findViewById(R.id.panelNumber)).addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        HasilSurveyModel model;
                        if ((model = realm.where(HasilSurveyModel.class)
                                .equalTo("FormId", surveyModel.FormId)
                                .equalTo("SurveyId", surveyModel.SurveyId)
                                .equalTo("CommonId", CommonID).findFirst()) == null)
                            model = realm.createObject(HasilSurveyModel.class);

                        model.CommonId = CommonID;
                        model.FormId = surveyModel.FormId;
                        model.ListID = ListID;
                        model.SurveyId = surveyModel.SurveyId;
                        model.ListValue = editable.toString();
                    }
                });*/
            }

        } else {
            view.findViewById(R.id.panel).setVisibility(View.VISIBLE);
            ((LinearLayout) view.findViewById(R.id.panel)).removeAllViews();
            RadioGroup radioGroup = new RadioGroup(context);

            List<List<String>> hasil = ctrlSurvey.getHasilSurvey(CommonID, surveyModel.FormId, surveyModel.SurveyId);

            for (SurveyOptModel surveyOptModel : list) {
                if (surveyModel.SurveyType.equals("1")) {
                    CheckBox checkBox = new CheckBox(context);
                    checkBox.setId(Integer.parseInt(surveyOptModel.ListID));
                    checkBox.setText(surveyOptModel.ListValue);
                    if (hasil != null || hasil.size() > 0) {
                        for (List<String> checked : hasil) {
                            Log.wtf("Hasil Checkbox", checked.get(0));
                            Log.wtf("ListID", surveyOptModel.ListID);

                            if (checked.get(0).equalsIgnoreCase(surveyOptModel.ListID)) {
                                checkBox.setChecked(true);
                                break;
                            }
                        }
                    }
                    ((LinearLayout) view.findViewById(R.id.panel)).addView(checkBox);
                } else if (surveyModel.SurveyType.equals("2")) {
                    RadioButton radioButton = new RadioButton(context);
                    radioButton.setId(Integer.parseInt(surveyOptModel.ListID));
                    radioButton.setText(surveyOptModel.ListValue);
                    if (hasil != null || hasil.size() > 0) {
                        for (List<String> checked : hasil) {

                            Log.wtf("Hasil radio", checked.get(0));
                            Log.wtf("ListID", surveyOptModel.ListID);

                            if (checked.get(0).equalsIgnoreCase(surveyOptModel.ListID)) {
                                radioButton.setChecked(true);
                                break;
                            }
                        }
                    }
                    radioGroup.addView(radioButton);
                }
            }
            if (surveyModel.SurveyType.equals("2")) {
                ((LinearLayout) view.findViewById(R.id.panel)).addView(radioGroup);
            }
        }
        addView(view);
    }

    public JSONArray getListID() {
        JSONArray array = new JSONArray();
        try {
            if (surveyModel.SurveyType.equals("0")) {
                JSONObject jsonSurvey = new JSONObject();
                jsonSurvey.put("FormId", Integer.parseInt(surveyModel.FormId));
                jsonSurvey.put("SurveyId", Integer.parseInt(surveyModel.SurveyId));
                jsonSurvey.put("ListID", Integer.parseInt(ListID));
                jsonSurvey.put("ListValue", ((EditText) findViewById(R.id.panelText)).getText().toString());
                array.put(jsonSurvey);
            } else if (surveyModel.SurveyType.equals("3")) {
                JSONObject jsonSurvey = new JSONObject();
                jsonSurvey.put("FormId", Integer.parseInt(surveyModel.FormId));
                jsonSurvey.put("SurveyId", Integer.parseInt(surveyModel.SurveyId));
                jsonSurvey.put("ListID", Integer.parseInt(ListID));
                jsonSurvey.put("ListValue", ((EditText) findViewById(R.id.panelNumber)).getText().toString());
                array.put(jsonSurvey);
            } else if (surveyModel.SurveyType.equals("2")) {
                View view = ((LinearLayout) findViewById(R.id.panel)).getChildAt(0);
                if (view instanceof RadioGroup) {
                    RadioGroup radioGroup = (RadioGroup) view;
                    JSONObject jsonSurvey = new JSONObject();
                    jsonSurvey.put("FormId", Integer.parseInt(surveyModel.FormId));
                    jsonSurvey.put("SurveyId", Integer.parseInt(surveyModel.SurveyId));
                    jsonSurvey.put("ListID", radioGroup.getCheckedRadioButtonId() == -1 ? 0 : radioGroup.getCheckedRadioButtonId());
                    jsonSurvey.put("ListValue", "");
                    array.put(jsonSurvey);
                }
            } else if (surveyModel.SurveyType.equals("1")) {
                for (int i = 0; i < ((LinearLayout) findViewById(R.id.panel)).getChildCount(); i++) {
                    View view = ((LinearLayout) findViewById(R.id.panel)).getChildAt(i);
                    if (view instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) view;
                        if (checkBox.isChecked()) {
                            JSONObject jsonSurvey = new JSONObject();
                            jsonSurvey.put("FormId", Integer.parseInt(surveyModel.FormId));
                            jsonSurvey.put("SurveyId", Integer.parseInt(surveyModel.SurveyId));
                            jsonSurvey.put("ListID", checkBox.getId());
                            jsonSurvey.put("ListValue", "");
                            array.put(jsonSurvey);
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return array;
    }

}
