package com.bhn.sadix;

import java.util.Date;
import java.util.UUID;

import org.json.JSONObject;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.bhn.sadix.adapter.ComboBoxAdapter;
import com.bhn.sadix.connection.ConnectionEvent;
import com.bhn.sadix.connection.ConnectionServer;
import com.bhn.sadix.database.CtrlConfig;
import com.bhn.sadix.database.DatabaseHelper;
import com.bhn.sadix.model.ComboBoxModel;
import com.bhn.sadix.model.UserModel;
import com.bhn.sadix.util.OkListener;
import com.bhn.sadix.util.Util;

public class SalesNoteActivity extends AppCompatActivity implements ConnectionEvent {
    private CtrlConfig ctrlConfig;
    private ConnectionServer server;
    private JSONObject data;
    private DatabaseHelper db;

    Toolbar toolbar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_note);

        /*getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ctrlConfig = new CtrlConfig(this);
        db = new DatabaseHelper(this);
        server = new ConnectionServer(this);
        server.addConnectionEvent(this);
        ((Spinner) findViewById(R.id.NoteID)).setAdapter(new ComboBoxAdapter(this, ctrlConfig.listComboBox("NOTE_ID")));
//		findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				finish();
//			}
//		});
//		findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				save();
//			}
//		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            menu.add(0, 1, 1, "Cancel")
                    .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            menu.add(0, 2, 2, "Save")
                    .setIcon(android.R.drawable.ic_menu_save)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        } else {
            menu.add(0, 1, 1, "Cancel");
            menu.add(0, 2, 2, "Save");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == 1) {
            finish();
        } else if (item.getItemId() == 2) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void save() {
        try {
            ComboBoxModel model = (ComboBoxModel) ((Spinner) findViewById(R.id.NoteID)).getSelectedItem();
            String Description = ((EditText) findViewById(R.id.Description)).getText().toString();
            if (model == null) {
                Util.showDialogInfo(this, "Note id harus dipilih!");
                return;
            } else if (Description == null || Description.trim().equals("")) {
                Util.showDialogInfo(this, "Note harus diisi!");
                return;
            }
            server.setUrl(Util.getServerUrl(this) + "salesnote");
            data = new JSONObject();
            data.put("SalesID", Integer.parseInt(UserModel.getInstance(this).SalesId));
            data.put("NoteID", Integer.parseInt(model.value));
            data.put("Description", Description);
            data.put("LogDate", Util.dateFormat.format(new Date()));
            data.put("ID", UUID.randomUUID().toString());
            server.requestJSONObject(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void error(String error) {
        Util.okeDialog(this, "Info", "Koneksi gagal, data akan tersimpan diinternal memory", new OkListener() {
            @Override
            public void onDialogOk() {
                db.save(data, "SalesNote");
                finish();
            }
        });
    }

    @Override
    public void messageServer(String respon) {
        try {
            JSONObject json = new JSONObject(respon);
            if (json.getString("Result").equals("INSERT_OK")) {
                Util.Toast(this, json.getString("Message"));
                finish();
            } else {
                Util.okeDialog(this, "Info", "Data gagal tersimpan, data akan tersimpan diinternal memorycc", new OkListener() {
                    @Override
                    public void onDialogOk() {
                        db.save(data, "SalesNote");
                        finish();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
