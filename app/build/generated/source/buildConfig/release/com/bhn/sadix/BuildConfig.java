/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.bhn.sadix;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.bhn.sadix";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 31611;
  public static final String VERSION_NAME = "3.16.11";
}
