package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DiscountResultModelRealmProxy extends com.bhn.sadix.Data.DiscountResultModel
    implements RealmObjectProxy, DiscountResultModelRealmProxyInterface {

    static final class DiscountResultModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CustomerIdIndex;
        public long DiscountResultIDIndex;
        public long DiscountResultValueIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        DiscountResultModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(5);
            this.CustomerIdIndex = getValidColumnIndex(path, table, "DiscountResultModel", "CustomerId");
            indicesMap.put("CustomerId", this.CustomerIdIndex);
            this.DiscountResultIDIndex = getValidColumnIndex(path, table, "DiscountResultModel", "DiscountResultID");
            indicesMap.put("DiscountResultID", this.DiscountResultIDIndex);
            this.DiscountResultValueIndex = getValidColumnIndex(path, table, "DiscountResultModel", "DiscountResultValue");
            indicesMap.put("DiscountResultValue", this.DiscountResultValueIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "DiscountResultModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "DiscountResultModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final DiscountResultModelColumnInfo otherInfo = (DiscountResultModelColumnInfo) other;
            this.CustomerIdIndex = otherInfo.CustomerIdIndex;
            this.DiscountResultIDIndex = otherInfo.DiscountResultIDIndex;
            this.DiscountResultValueIndex = otherInfo.DiscountResultValueIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final DiscountResultModelColumnInfo clone() {
            return (DiscountResultModelColumnInfo) super.clone();
        }

    }
    private DiscountResultModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.DiscountResultModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CustomerId");
        fieldNames.add("DiscountResultID");
        fieldNames.add("DiscountResultValue");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    DiscountResultModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (DiscountResultModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.DiscountResultModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public int realmGet$CustomerId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.CustomerIdIndex);
    }

    public void realmSet$CustomerId(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.CustomerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.CustomerIdIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$DiscountResultID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.DiscountResultIDIndex);
    }

    public void realmSet$DiscountResultID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.DiscountResultIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.DiscountResultIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$DiscountResultValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.DiscountResultValueIndex);
    }

    public void realmSet$DiscountResultValue(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.DiscountResultValueIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.DiscountResultValueIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.RandomIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.RandomIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.RandomIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.RandomIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("DiscountResultModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("DiscountResultModel");
            realmObjectSchema.add(new Property("CustomerId", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DiscountResultID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DiscountResultValue", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("DiscountResultModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_DiscountResultModel")) {
            Table table = sharedRealm.getTable("class_DiscountResultModel");
            table.addColumn(RealmFieldType.INTEGER, "CustomerId", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "DiscountResultID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "DiscountResultValue", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_DiscountResultModel");
    }

    public static DiscountResultModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_DiscountResultModel")) {
            Table table = sharedRealm.getTable("class_DiscountResultModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 5) {
                if (columnCount < 5) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 5 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 5 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 5 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final DiscountResultModelColumnInfo columnInfo = new DiscountResultModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("CustomerId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerId") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'CustomerId' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.CustomerIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerId' does support null values in the existing Realm file. Use corresponding boxed type for field 'CustomerId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DiscountResultID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DiscountResultID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DiscountResultID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'DiscountResultID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DiscountResultIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DiscountResultID' does support null values in the existing Realm file. Use corresponding boxed type for field 'DiscountResultID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DiscountResultValue")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DiscountResultValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DiscountResultValue") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'DiscountResultValue' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DiscountResultValueIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DiscountResultValue' does support null values in the existing Realm file. Use corresponding boxed type for field 'DiscountResultValue' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'RandomID' is required. Either set @Required to field 'RandomID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'DiscountResultModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_DiscountResultModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.DiscountResultModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.DiscountResultModel obj = realm.createObjectInternal(com.bhn.sadix.Data.DiscountResultModel.class, true, excludeFields);
        if (json.has("CustomerId")) {
            if (json.isNull("CustomerId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'CustomerId' to null.");
            } else {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$CustomerId((int) json.getInt("CustomerId"));
            }
        }
        if (json.has("DiscountResultID")) {
            if (json.isNull("DiscountResultID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountResultID' to null.");
            } else {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$DiscountResultID((int) json.getInt("DiscountResultID"));
            }
        }
        if (json.has("DiscountResultValue")) {
            if (json.isNull("DiscountResultValue")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountResultValue' to null.");
            } else {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$DiscountResultValue((int) json.getInt("DiscountResultValue"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("RandomID")) {
            if (json.isNull("RandomID")) {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$RandomID(null);
            } else {
                ((DiscountResultModelRealmProxyInterface) obj).realmSet$RandomID((String) json.getString("RandomID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.DiscountResultModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.DiscountResultModel obj = new com.bhn.sadix.Data.DiscountResultModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CustomerId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'CustomerId' to null.");
                } else {
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$CustomerId((int) reader.nextInt());
                }
            } else if (name.equals("DiscountResultID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountResultID' to null.");
                } else {
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$DiscountResultID((int) reader.nextInt());
                }
            } else if (name.equals("DiscountResultValue")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountResultValue' to null.");
                } else {
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$DiscountResultValue((int) reader.nextInt());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((DiscountResultModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.DiscountResultModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.DiscountResultModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.DiscountResultModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.DiscountResultModel copy(Realm realm, com.bhn.sadix.Data.DiscountResultModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.DiscountResultModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.DiscountResultModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.DiscountResultModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((DiscountResultModelRealmProxyInterface) realmObject).realmSet$CustomerId(((DiscountResultModelRealmProxyInterface) newObject).realmGet$CustomerId());
            ((DiscountResultModelRealmProxyInterface) realmObject).realmSet$DiscountResultID(((DiscountResultModelRealmProxyInterface) newObject).realmGet$DiscountResultID());
            ((DiscountResultModelRealmProxyInterface) realmObject).realmSet$DiscountResultValue(((DiscountResultModelRealmProxyInterface) newObject).realmGet$DiscountResultValue());
            ((DiscountResultModelRealmProxyInterface) realmObject).realmSet$CommonID(((DiscountResultModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((DiscountResultModelRealmProxyInterface) realmObject).realmSet$RandomID(((DiscountResultModelRealmProxyInterface) newObject).realmGet$RandomID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.DiscountResultModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountResultModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountResultModelColumnInfo columnInfo = (DiscountResultModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountResultModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$CustomerId(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultIDIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultValueIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultValue(), false);
        String realmGet$CommonID = ((DiscountResultModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$RandomID = ((DiscountResultModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountResultModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountResultModelColumnInfo columnInfo = (DiscountResultModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountResultModel.class);
        com.bhn.sadix.Data.DiscountResultModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.DiscountResultModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$CustomerId(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultIDIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultValueIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultValue(), false);
                String realmGet$CommonID = ((DiscountResultModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$RandomID = ((DiscountResultModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.DiscountResultModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountResultModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountResultModelColumnInfo columnInfo = (DiscountResultModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountResultModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$CustomerId(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultIDIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultValueIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultValue(), false);
        String realmGet$CommonID = ((DiscountResultModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$RandomID = ((DiscountResultModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountResultModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountResultModelColumnInfo columnInfo = (DiscountResultModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountResultModel.class);
        com.bhn.sadix.Data.DiscountResultModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.DiscountResultModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$CustomerId(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultIDIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountResultValueIndex, rowIndex, ((DiscountResultModelRealmProxyInterface)object).realmGet$DiscountResultValue(), false);
                String realmGet$CommonID = ((DiscountResultModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$RandomID = ((DiscountResultModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.DiscountResultModel createDetachedCopy(com.bhn.sadix.Data.DiscountResultModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.DiscountResultModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.DiscountResultModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.DiscountResultModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.DiscountResultModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((DiscountResultModelRealmProxyInterface) unmanagedObject).realmSet$CustomerId(((DiscountResultModelRealmProxyInterface) realmObject).realmGet$CustomerId());
        ((DiscountResultModelRealmProxyInterface) unmanagedObject).realmSet$DiscountResultID(((DiscountResultModelRealmProxyInterface) realmObject).realmGet$DiscountResultID());
        ((DiscountResultModelRealmProxyInterface) unmanagedObject).realmSet$DiscountResultValue(((DiscountResultModelRealmProxyInterface) realmObject).realmGet$DiscountResultValue());
        ((DiscountResultModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((DiscountResultModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((DiscountResultModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((DiscountResultModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("DiscountResultModel = [");
        stringBuilder.append("{CustomerId:");
        stringBuilder.append(realmGet$CustomerId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DiscountResultID:");
        stringBuilder.append(realmGet$DiscountResultID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DiscountResultValue:");
        stringBuilder.append(realmGet$DiscountResultValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscountResultModelRealmProxy aDiscountResultModel = (DiscountResultModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aDiscountResultModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aDiscountResultModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aDiscountResultModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
