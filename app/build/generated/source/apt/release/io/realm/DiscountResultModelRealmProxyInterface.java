package io.realm;


public interface DiscountResultModelRealmProxyInterface {
    public int realmGet$CustomerId();
    public void realmSet$CustomerId(int value);
    public int realmGet$DiscountResultID();
    public void realmSet$DiscountResultID(int value);
    public int realmGet$DiscountResultValue();
    public void realmSet$DiscountResultValue(int value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
