package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HasilSurveyModelRealmProxy extends com.bhn.sadix.Data.HasilSurveyModel
    implements RealmObjectProxy, HasilSurveyModelRealmProxyInterface {

    static final class HasilSurveyModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long FormIdIndex;
        public long CommonIdIndex;
        public long ListIDIndex;
        public long ListValueIndex;
        public long SurveyIdIndex;

        HasilSurveyModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(5);
            this.FormIdIndex = getValidColumnIndex(path, table, "HasilSurveyModel", "FormId");
            indicesMap.put("FormId", this.FormIdIndex);
            this.CommonIdIndex = getValidColumnIndex(path, table, "HasilSurveyModel", "CommonId");
            indicesMap.put("CommonId", this.CommonIdIndex);
            this.ListIDIndex = getValidColumnIndex(path, table, "HasilSurveyModel", "ListID");
            indicesMap.put("ListID", this.ListIDIndex);
            this.ListValueIndex = getValidColumnIndex(path, table, "HasilSurveyModel", "ListValue");
            indicesMap.put("ListValue", this.ListValueIndex);
            this.SurveyIdIndex = getValidColumnIndex(path, table, "HasilSurveyModel", "SurveyId");
            indicesMap.put("SurveyId", this.SurveyIdIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final HasilSurveyModelColumnInfo otherInfo = (HasilSurveyModelColumnInfo) other;
            this.FormIdIndex = otherInfo.FormIdIndex;
            this.CommonIdIndex = otherInfo.CommonIdIndex;
            this.ListIDIndex = otherInfo.ListIDIndex;
            this.ListValueIndex = otherInfo.ListValueIndex;
            this.SurveyIdIndex = otherInfo.SurveyIdIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final HasilSurveyModelColumnInfo clone() {
            return (HasilSurveyModelColumnInfo) super.clone();
        }

    }
    private HasilSurveyModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.HasilSurveyModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("FormId");
        fieldNames.add("CommonId");
        fieldNames.add("ListID");
        fieldNames.add("ListValue");
        fieldNames.add("SurveyId");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    HasilSurveyModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (HasilSurveyModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.HasilSurveyModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$FormId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.FormIdIndex);
    }

    public void realmSet$FormId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.FormIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.FormIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.FormIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.FormIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIdIndex);
    }

    public void realmSet$CommonId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ListID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ListIDIndex);
    }

    public void realmSet$ListID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ListIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ListIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ListIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ListIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ListValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ListValueIndex);
    }

    public void realmSet$ListValue(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ListValueIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ListValueIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ListValueIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ListValueIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SurveyId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SurveyIdIndex);
    }

    public void realmSet$SurveyId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SurveyIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SurveyIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SurveyIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SurveyIdIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("HasilSurveyModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("HasilSurveyModel");
            realmObjectSchema.add(new Property("FormId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ListID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ListValue", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SurveyId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("HasilSurveyModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_HasilSurveyModel")) {
            Table table = sharedRealm.getTable("class_HasilSurveyModel");
            table.addColumn(RealmFieldType.STRING, "FormId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ListID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ListValue", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SurveyId", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_HasilSurveyModel");
    }

    public static HasilSurveyModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_HasilSurveyModel")) {
            Table table = sharedRealm.getTable("class_HasilSurveyModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 5) {
                if (columnCount < 5) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 5 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 5 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 5 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final HasilSurveyModelColumnInfo columnInfo = new HasilSurveyModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("FormId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'FormId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("FormId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'FormId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.FormIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'FormId' is required. Either set @Required to field 'FormId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonId' is required. Either set @Required to field 'CommonId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ListID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ListID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ListID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ListID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ListIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ListID' is required. Either set @Required to field 'ListID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ListValue")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ListValue' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ListValue") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ListValue' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ListValueIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ListValue' is required. Either set @Required to field 'ListValue' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SurveyId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SurveyId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SurveyId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SurveyId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SurveyIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SurveyId' is required. Either set @Required to field 'SurveyId' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'HasilSurveyModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_HasilSurveyModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.HasilSurveyModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.HasilSurveyModel obj = realm.createObjectInternal(com.bhn.sadix.Data.HasilSurveyModel.class, true, excludeFields);
        if (json.has("FormId")) {
            if (json.isNull("FormId")) {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$FormId(null);
            } else {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$FormId((String) json.getString("FormId"));
            }
        }
        if (json.has("CommonId")) {
            if (json.isNull("CommonId")) {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$CommonId(null);
            } else {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$CommonId((String) json.getString("CommonId"));
            }
        }
        if (json.has("ListID")) {
            if (json.isNull("ListID")) {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListID(null);
            } else {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListID((String) json.getString("ListID"));
            }
        }
        if (json.has("ListValue")) {
            if (json.isNull("ListValue")) {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListValue(null);
            } else {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListValue((String) json.getString("ListValue"));
            }
        }
        if (json.has("SurveyId")) {
            if (json.isNull("SurveyId")) {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$SurveyId(null);
            } else {
                ((HasilSurveyModelRealmProxyInterface) obj).realmSet$SurveyId((String) json.getString("SurveyId"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.HasilSurveyModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.HasilSurveyModel obj = new com.bhn.sadix.Data.HasilSurveyModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("FormId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$FormId(null);
                } else {
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$FormId((String) reader.nextString());
                }
            } else if (name.equals("CommonId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$CommonId(null);
                } else {
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$CommonId((String) reader.nextString());
                }
            } else if (name.equals("ListID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListID(null);
                } else {
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListID((String) reader.nextString());
                }
            } else if (name.equals("ListValue")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListValue(null);
                } else {
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$ListValue((String) reader.nextString());
                }
            } else if (name.equals("SurveyId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$SurveyId(null);
                } else {
                    ((HasilSurveyModelRealmProxyInterface) obj).realmSet$SurveyId((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.HasilSurveyModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.HasilSurveyModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.HasilSurveyModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.HasilSurveyModel copy(Realm realm, com.bhn.sadix.Data.HasilSurveyModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.HasilSurveyModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.HasilSurveyModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.HasilSurveyModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((HasilSurveyModelRealmProxyInterface) realmObject).realmSet$FormId(((HasilSurveyModelRealmProxyInterface) newObject).realmGet$FormId());
            ((HasilSurveyModelRealmProxyInterface) realmObject).realmSet$CommonId(((HasilSurveyModelRealmProxyInterface) newObject).realmGet$CommonId());
            ((HasilSurveyModelRealmProxyInterface) realmObject).realmSet$ListID(((HasilSurveyModelRealmProxyInterface) newObject).realmGet$ListID());
            ((HasilSurveyModelRealmProxyInterface) realmObject).realmSet$ListValue(((HasilSurveyModelRealmProxyInterface) newObject).realmGet$ListValue());
            ((HasilSurveyModelRealmProxyInterface) realmObject).realmSet$SurveyId(((HasilSurveyModelRealmProxyInterface) newObject).realmGet$SurveyId());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.HasilSurveyModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.HasilSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        HasilSurveyModelColumnInfo columnInfo = (HasilSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.HasilSurveyModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$FormId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$FormId();
        if (realmGet$FormId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
        }
        String realmGet$CommonId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$CommonId();
        if (realmGet$CommonId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
        }
        String realmGet$ListID = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListID();
        if (realmGet$ListID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ListIDIndex, rowIndex, realmGet$ListID, false);
        }
        String realmGet$ListValue = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListValue();
        if (realmGet$ListValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ListValueIndex, rowIndex, realmGet$ListValue, false);
        }
        String realmGet$SurveyId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$SurveyId();
        if (realmGet$SurveyId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, realmGet$SurveyId, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.HasilSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        HasilSurveyModelColumnInfo columnInfo = (HasilSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.HasilSurveyModel.class);
        com.bhn.sadix.Data.HasilSurveyModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.HasilSurveyModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$FormId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$FormId();
                if (realmGet$FormId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
                }
                String realmGet$CommonId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$CommonId();
                if (realmGet$CommonId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
                }
                String realmGet$ListID = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListID();
                if (realmGet$ListID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ListIDIndex, rowIndex, realmGet$ListID, false);
                }
                String realmGet$ListValue = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListValue();
                if (realmGet$ListValue != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ListValueIndex, rowIndex, realmGet$ListValue, false);
                }
                String realmGet$SurveyId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$SurveyId();
                if (realmGet$SurveyId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, realmGet$SurveyId, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.HasilSurveyModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.HasilSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        HasilSurveyModelColumnInfo columnInfo = (HasilSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.HasilSurveyModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$FormId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$FormId();
        if (realmGet$FormId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.FormIdIndex, rowIndex, false);
        }
        String realmGet$CommonId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$CommonId();
        if (realmGet$CommonId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, false);
        }
        String realmGet$ListID = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListID();
        if (realmGet$ListID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ListIDIndex, rowIndex, realmGet$ListID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ListIDIndex, rowIndex, false);
        }
        String realmGet$ListValue = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListValue();
        if (realmGet$ListValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ListValueIndex, rowIndex, realmGet$ListValue, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ListValueIndex, rowIndex, false);
        }
        String realmGet$SurveyId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$SurveyId();
        if (realmGet$SurveyId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, realmGet$SurveyId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.HasilSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        HasilSurveyModelColumnInfo columnInfo = (HasilSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.HasilSurveyModel.class);
        com.bhn.sadix.Data.HasilSurveyModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.HasilSurveyModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$FormId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$FormId();
                if (realmGet$FormId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.FormIdIndex, rowIndex, false);
                }
                String realmGet$CommonId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$CommonId();
                if (realmGet$CommonId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, false);
                }
                String realmGet$ListID = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListID();
                if (realmGet$ListID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ListIDIndex, rowIndex, realmGet$ListID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ListIDIndex, rowIndex, false);
                }
                String realmGet$ListValue = ((HasilSurveyModelRealmProxyInterface)object).realmGet$ListValue();
                if (realmGet$ListValue != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ListValueIndex, rowIndex, realmGet$ListValue, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ListValueIndex, rowIndex, false);
                }
                String realmGet$SurveyId = ((HasilSurveyModelRealmProxyInterface)object).realmGet$SurveyId();
                if (realmGet$SurveyId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, realmGet$SurveyId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SurveyIdIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.HasilSurveyModel createDetachedCopy(com.bhn.sadix.Data.HasilSurveyModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.HasilSurveyModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.HasilSurveyModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.HasilSurveyModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.HasilSurveyModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((HasilSurveyModelRealmProxyInterface) unmanagedObject).realmSet$FormId(((HasilSurveyModelRealmProxyInterface) realmObject).realmGet$FormId());
        ((HasilSurveyModelRealmProxyInterface) unmanagedObject).realmSet$CommonId(((HasilSurveyModelRealmProxyInterface) realmObject).realmGet$CommonId());
        ((HasilSurveyModelRealmProxyInterface) unmanagedObject).realmSet$ListID(((HasilSurveyModelRealmProxyInterface) realmObject).realmGet$ListID());
        ((HasilSurveyModelRealmProxyInterface) unmanagedObject).realmSet$ListValue(((HasilSurveyModelRealmProxyInterface) realmObject).realmGet$ListValue());
        ((HasilSurveyModelRealmProxyInterface) unmanagedObject).realmSet$SurveyId(((HasilSurveyModelRealmProxyInterface) realmObject).realmGet$SurveyId());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("HasilSurveyModel = [");
        stringBuilder.append("{FormId:");
        stringBuilder.append(realmGet$FormId() != null ? realmGet$FormId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonId:");
        stringBuilder.append(realmGet$CommonId() != null ? realmGet$CommonId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ListID:");
        stringBuilder.append(realmGet$ListID() != null ? realmGet$ListID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ListValue:");
        stringBuilder.append(realmGet$ListValue() != null ? realmGet$ListValue() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SurveyId:");
        stringBuilder.append(realmGet$SurveyId() != null ? realmGet$SurveyId() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HasilSurveyModelRealmProxy aHasilSurveyModel = (HasilSurveyModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aHasilSurveyModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aHasilSurveyModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aHasilSurveyModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
