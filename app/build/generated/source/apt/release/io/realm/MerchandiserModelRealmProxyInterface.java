package io.realm;


public interface MerchandiserModelRealmProxyInterface {
    public String realmGet$IDX();
    public void realmSet$IDX(String value);
    public String realmGet$NAME();
    public void realmSet$NAME(String value);
    public String realmGet$TYPE();
    public void realmSet$TYPE(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
}
