package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AssetModelRealmProxy extends com.bhn.sadix.Data.AssetModel
    implements RealmObjectProxy, AssetModelRealmProxyInterface {

    static final class AssetModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long AsetIDIndex;
        public long IdxIndex;
        public long NameIndex;
        public long GroupAsetIndex;
        public long TypeMntIndex;
        public long PosisiIndex;
        public long ValueIndex;
        public long CommonIDIndex;
        public long NameFileIndex;
        public long tipeIndex;
        public long JenisWidgetIndex;
        public long RandomIDIndex;

        AssetModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(12);
            this.AsetIDIndex = getValidColumnIndex(path, table, "AssetModel", "AsetID");
            indicesMap.put("AsetID", this.AsetIDIndex);
            this.IdxIndex = getValidColumnIndex(path, table, "AssetModel", "Idx");
            indicesMap.put("Idx", this.IdxIndex);
            this.NameIndex = getValidColumnIndex(path, table, "AssetModel", "Name");
            indicesMap.put("Name", this.NameIndex);
            this.GroupAsetIndex = getValidColumnIndex(path, table, "AssetModel", "GroupAset");
            indicesMap.put("GroupAset", this.GroupAsetIndex);
            this.TypeMntIndex = getValidColumnIndex(path, table, "AssetModel", "TypeMnt");
            indicesMap.put("TypeMnt", this.TypeMntIndex);
            this.PosisiIndex = getValidColumnIndex(path, table, "AssetModel", "Posisi");
            indicesMap.put("Posisi", this.PosisiIndex);
            this.ValueIndex = getValidColumnIndex(path, table, "AssetModel", "Value");
            indicesMap.put("Value", this.ValueIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "AssetModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.NameFileIndex = getValidColumnIndex(path, table, "AssetModel", "NameFile");
            indicesMap.put("NameFile", this.NameFileIndex);
            this.tipeIndex = getValidColumnIndex(path, table, "AssetModel", "tipe");
            indicesMap.put("tipe", this.tipeIndex);
            this.JenisWidgetIndex = getValidColumnIndex(path, table, "AssetModel", "JenisWidget");
            indicesMap.put("JenisWidget", this.JenisWidgetIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "AssetModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final AssetModelColumnInfo otherInfo = (AssetModelColumnInfo) other;
            this.AsetIDIndex = otherInfo.AsetIDIndex;
            this.IdxIndex = otherInfo.IdxIndex;
            this.NameIndex = otherInfo.NameIndex;
            this.GroupAsetIndex = otherInfo.GroupAsetIndex;
            this.TypeMntIndex = otherInfo.TypeMntIndex;
            this.PosisiIndex = otherInfo.PosisiIndex;
            this.ValueIndex = otherInfo.ValueIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.NameFileIndex = otherInfo.NameFileIndex;
            this.tipeIndex = otherInfo.tipeIndex;
            this.JenisWidgetIndex = otherInfo.JenisWidgetIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final AssetModelColumnInfo clone() {
            return (AssetModelColumnInfo) super.clone();
        }

    }
    private AssetModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.AssetModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("AsetID");
        fieldNames.add("Idx");
        fieldNames.add("Name");
        fieldNames.add("GroupAset");
        fieldNames.add("TypeMnt");
        fieldNames.add("Posisi");
        fieldNames.add("Value");
        fieldNames.add("CommonID");
        fieldNames.add("NameFile");
        fieldNames.add("tipe");
        fieldNames.add("JenisWidget");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    AssetModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (AssetModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.AssetModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$AsetID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.AsetIDIndex);
    }

    public void realmSet$AsetID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.AsetIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.AsetIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.AsetIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.AsetIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Idx() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.IdxIndex);
    }

    public void realmSet$Idx(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.IdxIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.IdxIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NameIndex);
    }

    public void realmSet$Name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NameIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GroupAset() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GroupAsetIndex);
    }

    public void realmSet$GroupAset(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GroupAsetIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GroupAsetIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GroupAsetIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GroupAsetIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$TypeMnt() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.TypeMntIndex);
    }

    public void realmSet$TypeMnt(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.TypeMntIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.TypeMntIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Posisi() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.PosisiIndex);
    }

    public void realmSet$Posisi(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.PosisiIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.PosisiIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Value() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ValueIndex);
    }

    public void realmSet$Value(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ValueIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ValueIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ValueIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ValueIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$NameFile() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NameFileIndex);
    }

    public void realmSet$NameFile(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NameFileIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NameFileIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NameFileIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NameFileIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$tipe() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.tipeIndex);
    }

    public void realmSet$tipe(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.tipeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.tipeIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$JenisWidget() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.JenisWidgetIndex);
    }

    public void realmSet$JenisWidget(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.JenisWidgetIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.JenisWidgetIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("AssetModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("AssetModel");
            realmObjectSchema.add(new Property("AsetID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Idx", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GroupAset", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("TypeMnt", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Posisi", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Value", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("NameFile", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tipe", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("JenisWidget", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("AssetModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_AssetModel")) {
            Table table = sharedRealm.getTable("class_AssetModel");
            table.addColumn(RealmFieldType.STRING, "AsetID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Idx", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Name", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GroupAset", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "TypeMnt", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Posisi", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Value", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "NameFile", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "tipe", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "JenisWidget", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_AssetModel");
    }

    public static AssetModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_AssetModel")) {
            Table table = sharedRealm.getTable("class_AssetModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 12) {
                if (columnCount < 12) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 12 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 12 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 12 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final AssetModelColumnInfo columnInfo = new AssetModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("AsetID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'AsetID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("AsetID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'AsetID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.AsetIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'AsetID' is required. Either set @Required to field 'AsetID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Idx")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Idx' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Idx") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Idx' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.IdxIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Idx' does support null values in the existing Realm file. Use corresponding boxed type for field 'Idx' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Name")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Name' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Name") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Name' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NameIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Name' is required. Either set @Required to field 'Name' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GroupAset")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GroupAset' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GroupAset") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GroupAset' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GroupAsetIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GroupAset' is required. Either set @Required to field 'GroupAset' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("TypeMnt")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'TypeMnt' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("TypeMnt") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'TypeMnt' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.TypeMntIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'TypeMnt' does support null values in the existing Realm file. Use corresponding boxed type for field 'TypeMnt' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Posisi")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Posisi' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Posisi") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Posisi' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PosisiIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Posisi' does support null values in the existing Realm file. Use corresponding boxed type for field 'Posisi' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Value")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Value' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Value") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Value' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ValueIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Value' is required. Either set @Required to field 'Value' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("NameFile")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'NameFile' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("NameFile") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'NameFile' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NameFileIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'NameFile' is required. Either set @Required to field 'NameFile' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("tipe")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tipe' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tipe") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'tipe' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.tipeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tipe' does support null values in the existing Realm file. Use corresponding boxed type for field 'tipe' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("JenisWidget")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'JenisWidget' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("JenisWidget") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'JenisWidget' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.JenisWidgetIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'JenisWidget' does support null values in the existing Realm file. Use corresponding boxed type for field 'JenisWidget' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'AssetModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_AssetModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.AssetModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.AssetModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.AssetModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.AssetModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.AssetModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.AssetModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.AssetModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("AsetID")) {
            if (json.isNull("AsetID")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$AsetID(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$AsetID((String) json.getString("AsetID"));
            }
        }
        if (json.has("Idx")) {
            if (json.isNull("Idx")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Idx' to null.");
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$Idx((int) json.getInt("Idx"));
            }
        }
        if (json.has("Name")) {
            if (json.isNull("Name")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$Name(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$Name((String) json.getString("Name"));
            }
        }
        if (json.has("GroupAset")) {
            if (json.isNull("GroupAset")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$GroupAset(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$GroupAset((String) json.getString("GroupAset"));
            }
        }
        if (json.has("TypeMnt")) {
            if (json.isNull("TypeMnt")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'TypeMnt' to null.");
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$TypeMnt((int) json.getInt("TypeMnt"));
            }
        }
        if (json.has("Posisi")) {
            if (json.isNull("Posisi")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Posisi' to null.");
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$Posisi((int) json.getInt("Posisi"));
            }
        }
        if (json.has("Value")) {
            if (json.isNull("Value")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$Value(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$Value((String) json.getString("Value"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("NameFile")) {
            if (json.isNull("NameFile")) {
                ((AssetModelRealmProxyInterface) obj).realmSet$NameFile(null);
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$NameFile((String) json.getString("NameFile"));
            }
        }
        if (json.has("tipe")) {
            if (json.isNull("tipe")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'tipe' to null.");
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$tipe((int) json.getInt("tipe"));
            }
        }
        if (json.has("JenisWidget")) {
            if (json.isNull("JenisWidget")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'JenisWidget' to null.");
            } else {
                ((AssetModelRealmProxyInterface) obj).realmSet$JenisWidget((int) json.getInt("JenisWidget"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.AssetModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.AssetModel obj = new com.bhn.sadix.Data.AssetModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("AsetID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$AsetID(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$AsetID((String) reader.nextString());
                }
            } else if (name.equals("Idx")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Idx' to null.");
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$Idx((int) reader.nextInt());
                }
            } else if (name.equals("Name")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$Name(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$Name((String) reader.nextString());
                }
            } else if (name.equals("GroupAset")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$GroupAset(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$GroupAset((String) reader.nextString());
                }
            } else if (name.equals("TypeMnt")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'TypeMnt' to null.");
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$TypeMnt((int) reader.nextInt());
                }
            } else if (name.equals("Posisi")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Posisi' to null.");
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$Posisi((int) reader.nextInt());
                }
            } else if (name.equals("Value")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$Value(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$Value((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("NameFile")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$NameFile(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$NameFile((String) reader.nextString());
                }
            } else if (name.equals("tipe")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'tipe' to null.");
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$tipe((int) reader.nextInt());
                }
            } else if (name.equals("JenisWidget")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'JenisWidget' to null.");
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$JenisWidget((int) reader.nextInt());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((AssetModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.AssetModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.AssetModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.AssetModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((AssetModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.AssetModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.AssetModel copy(Realm realm, com.bhn.sadix.Data.AssetModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.AssetModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.AssetModel.class, ((AssetModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((AssetModelRealmProxyInterface) realmObject).realmSet$AsetID(((AssetModelRealmProxyInterface) newObject).realmGet$AsetID());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$Idx(((AssetModelRealmProxyInterface) newObject).realmGet$Idx());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$Name(((AssetModelRealmProxyInterface) newObject).realmGet$Name());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$GroupAset(((AssetModelRealmProxyInterface) newObject).realmGet$GroupAset());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$TypeMnt(((AssetModelRealmProxyInterface) newObject).realmGet$TypeMnt());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$Posisi(((AssetModelRealmProxyInterface) newObject).realmGet$Posisi());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$Value(((AssetModelRealmProxyInterface) newObject).realmGet$Value());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$CommonID(((AssetModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$NameFile(((AssetModelRealmProxyInterface) newObject).realmGet$NameFile());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$tipe(((AssetModelRealmProxyInterface) newObject).realmGet$tipe());
            ((AssetModelRealmProxyInterface) realmObject).realmSet$JenisWidget(((AssetModelRealmProxyInterface) newObject).realmGet$JenisWidget());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.AssetModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetModelColumnInfo columnInfo = (AssetModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((AssetModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$AsetID = ((AssetModelRealmProxyInterface)object).realmGet$AsetID();
        if (realmGet$AsetID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.IdxIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Idx(), false);
        String realmGet$Name = ((AssetModelRealmProxyInterface)object).realmGet$Name();
        if (realmGet$Name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NameIndex, rowIndex, realmGet$Name, false);
        }
        String realmGet$GroupAset = ((AssetModelRealmProxyInterface)object).realmGet$GroupAset();
        if (realmGet$GroupAset != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, realmGet$GroupAset, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.TypeMntIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$TypeMnt(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.PosisiIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Posisi(), false);
        String realmGet$Value = ((AssetModelRealmProxyInterface)object).realmGet$Value();
        if (realmGet$Value != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ValueIndex, rowIndex, realmGet$Value, false);
        }
        String realmGet$CommonID = ((AssetModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$NameFile = ((AssetModelRealmProxyInterface)object).realmGet$NameFile();
        if (realmGet$NameFile != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NameFileIndex, rowIndex, realmGet$NameFile, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tipeIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$tipe(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.JenisWidgetIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$JenisWidget(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetModelColumnInfo columnInfo = (AssetModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.AssetModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((AssetModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$AsetID = ((AssetModelRealmProxyInterface)object).realmGet$AsetID();
                if (realmGet$AsetID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.IdxIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Idx(), false);
                String realmGet$Name = ((AssetModelRealmProxyInterface)object).realmGet$Name();
                if (realmGet$Name != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NameIndex, rowIndex, realmGet$Name, false);
                }
                String realmGet$GroupAset = ((AssetModelRealmProxyInterface)object).realmGet$GroupAset();
                if (realmGet$GroupAset != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, realmGet$GroupAset, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.TypeMntIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$TypeMnt(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.PosisiIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Posisi(), false);
                String realmGet$Value = ((AssetModelRealmProxyInterface)object).realmGet$Value();
                if (realmGet$Value != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ValueIndex, rowIndex, realmGet$Value, false);
                }
                String realmGet$CommonID = ((AssetModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$NameFile = ((AssetModelRealmProxyInterface)object).realmGet$NameFile();
                if (realmGet$NameFile != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NameFileIndex, rowIndex, realmGet$NameFile, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.tipeIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$tipe(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.JenisWidgetIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$JenisWidget(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.AssetModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetModelColumnInfo columnInfo = (AssetModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((AssetModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$AsetID = ((AssetModelRealmProxyInterface)object).realmGet$AsetID();
        if (realmGet$AsetID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.IdxIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Idx(), false);
        String realmGet$Name = ((AssetModelRealmProxyInterface)object).realmGet$Name();
        if (realmGet$Name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NameIndex, rowIndex, realmGet$Name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NameIndex, rowIndex, false);
        }
        String realmGet$GroupAset = ((AssetModelRealmProxyInterface)object).realmGet$GroupAset();
        if (realmGet$GroupAset != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, realmGet$GroupAset, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.TypeMntIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$TypeMnt(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.PosisiIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Posisi(), false);
        String realmGet$Value = ((AssetModelRealmProxyInterface)object).realmGet$Value();
        if (realmGet$Value != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ValueIndex, rowIndex, realmGet$Value, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ValueIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((AssetModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$NameFile = ((AssetModelRealmProxyInterface)object).realmGet$NameFile();
        if (realmGet$NameFile != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NameFileIndex, rowIndex, realmGet$NameFile, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NameFileIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tipeIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$tipe(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.JenisWidgetIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$JenisWidget(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetModelColumnInfo columnInfo = (AssetModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.AssetModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((AssetModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$AsetID = ((AssetModelRealmProxyInterface)object).realmGet$AsetID();
                if (realmGet$AsetID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.IdxIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Idx(), false);
                String realmGet$Name = ((AssetModelRealmProxyInterface)object).realmGet$Name();
                if (realmGet$Name != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NameIndex, rowIndex, realmGet$Name, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NameIndex, rowIndex, false);
                }
                String realmGet$GroupAset = ((AssetModelRealmProxyInterface)object).realmGet$GroupAset();
                if (realmGet$GroupAset != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, realmGet$GroupAset, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GroupAsetIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.TypeMntIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$TypeMnt(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.PosisiIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$Posisi(), false);
                String realmGet$Value = ((AssetModelRealmProxyInterface)object).realmGet$Value();
                if (realmGet$Value != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ValueIndex, rowIndex, realmGet$Value, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ValueIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((AssetModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$NameFile = ((AssetModelRealmProxyInterface)object).realmGet$NameFile();
                if (realmGet$NameFile != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NameFileIndex, rowIndex, realmGet$NameFile, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NameFileIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.tipeIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$tipe(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.JenisWidgetIndex, rowIndex, ((AssetModelRealmProxyInterface)object).realmGet$JenisWidget(), false);
            }
        }
    }

    public static com.bhn.sadix.Data.AssetModel createDetachedCopy(com.bhn.sadix.Data.AssetModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.AssetModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.AssetModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.AssetModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.AssetModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$AsetID(((AssetModelRealmProxyInterface) realmObject).realmGet$AsetID());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$Idx(((AssetModelRealmProxyInterface) realmObject).realmGet$Idx());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$Name(((AssetModelRealmProxyInterface) realmObject).realmGet$Name());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$GroupAset(((AssetModelRealmProxyInterface) realmObject).realmGet$GroupAset());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$TypeMnt(((AssetModelRealmProxyInterface) realmObject).realmGet$TypeMnt());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$Posisi(((AssetModelRealmProxyInterface) realmObject).realmGet$Posisi());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$Value(((AssetModelRealmProxyInterface) realmObject).realmGet$Value());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((AssetModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$NameFile(((AssetModelRealmProxyInterface) realmObject).realmGet$NameFile());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$tipe(((AssetModelRealmProxyInterface) realmObject).realmGet$tipe());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$JenisWidget(((AssetModelRealmProxyInterface) realmObject).realmGet$JenisWidget());
        ((AssetModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((AssetModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.AssetModel update(Realm realm, com.bhn.sadix.Data.AssetModel realmObject, com.bhn.sadix.Data.AssetModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((AssetModelRealmProxyInterface) realmObject).realmSet$AsetID(((AssetModelRealmProxyInterface) newObject).realmGet$AsetID());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$Idx(((AssetModelRealmProxyInterface) newObject).realmGet$Idx());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$Name(((AssetModelRealmProxyInterface) newObject).realmGet$Name());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$GroupAset(((AssetModelRealmProxyInterface) newObject).realmGet$GroupAset());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$TypeMnt(((AssetModelRealmProxyInterface) newObject).realmGet$TypeMnt());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$Posisi(((AssetModelRealmProxyInterface) newObject).realmGet$Posisi());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$Value(((AssetModelRealmProxyInterface) newObject).realmGet$Value());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$CommonID(((AssetModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$NameFile(((AssetModelRealmProxyInterface) newObject).realmGet$NameFile());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$tipe(((AssetModelRealmProxyInterface) newObject).realmGet$tipe());
        ((AssetModelRealmProxyInterface) realmObject).realmSet$JenisWidget(((AssetModelRealmProxyInterface) newObject).realmGet$JenisWidget());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("AssetModel = [");
        stringBuilder.append("{AsetID:");
        stringBuilder.append(realmGet$AsetID() != null ? realmGet$AsetID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Idx:");
        stringBuilder.append(realmGet$Idx());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Name:");
        stringBuilder.append(realmGet$Name() != null ? realmGet$Name() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GroupAset:");
        stringBuilder.append(realmGet$GroupAset() != null ? realmGet$GroupAset() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{TypeMnt:");
        stringBuilder.append(realmGet$TypeMnt());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Posisi:");
        stringBuilder.append(realmGet$Posisi());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Value:");
        stringBuilder.append(realmGet$Value() != null ? realmGet$Value() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{NameFile:");
        stringBuilder.append(realmGet$NameFile() != null ? realmGet$NameFile() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tipe:");
        stringBuilder.append(realmGet$tipe());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{JenisWidget:");
        stringBuilder.append(realmGet$JenisWidget());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssetModelRealmProxy aAssetModel = (AssetModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aAssetModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAssetModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aAssetModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
