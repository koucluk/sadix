package io.realm;


public interface AssetMutasiModelRealmProxyInterface {
    public String realmGet$AsetName();
    public void realmSet$AsetName(String value);
    public String realmGet$AsetID();
    public void realmSet$AsetID(String value);
    public int realmGet$MerkID();
    public void realmSet$MerkID(int value);
    public int realmGet$TypeID();
    public void realmSet$TypeID(int value);
    public String realmGet$Remark();
    public void realmSet$Remark(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
    public String realmGet$NoSeri();
    public void realmSet$NoSeri(String value);
}
