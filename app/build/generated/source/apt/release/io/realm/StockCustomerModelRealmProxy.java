package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StockCustomerModelRealmProxy extends com.bhn.sadix.Data.StockCustomerModel
    implements RealmObjectProxy, StockCustomerModelRealmProxyInterface {

    static final class StockCustomerModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CustIdIndex;
        public long tanggalIndex;
        public long SKUIdIndex;
        public long SKUNameIndex;
        public long QTY_BIndex;
        public long QTY_KIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        StockCustomerModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(8);
            this.CustIdIndex = getValidColumnIndex(path, table, "StockCustomerModel", "CustId");
            indicesMap.put("CustId", this.CustIdIndex);
            this.tanggalIndex = getValidColumnIndex(path, table, "StockCustomerModel", "tanggal");
            indicesMap.put("tanggal", this.tanggalIndex);
            this.SKUIdIndex = getValidColumnIndex(path, table, "StockCustomerModel", "SKUId");
            indicesMap.put("SKUId", this.SKUIdIndex);
            this.SKUNameIndex = getValidColumnIndex(path, table, "StockCustomerModel", "SKUName");
            indicesMap.put("SKUName", this.SKUNameIndex);
            this.QTY_BIndex = getValidColumnIndex(path, table, "StockCustomerModel", "QTY_B");
            indicesMap.put("QTY_B", this.QTY_BIndex);
            this.QTY_KIndex = getValidColumnIndex(path, table, "StockCustomerModel", "QTY_K");
            indicesMap.put("QTY_K", this.QTY_KIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "StockCustomerModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "StockCustomerModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final StockCustomerModelColumnInfo otherInfo = (StockCustomerModelColumnInfo) other;
            this.CustIdIndex = otherInfo.CustIdIndex;
            this.tanggalIndex = otherInfo.tanggalIndex;
            this.SKUIdIndex = otherInfo.SKUIdIndex;
            this.SKUNameIndex = otherInfo.SKUNameIndex;
            this.QTY_BIndex = otherInfo.QTY_BIndex;
            this.QTY_KIndex = otherInfo.QTY_KIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final StockCustomerModelColumnInfo clone() {
            return (StockCustomerModelColumnInfo) super.clone();
        }

    }
    private StockCustomerModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.StockCustomerModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CustId");
        fieldNames.add("tanggal");
        fieldNames.add("SKUId");
        fieldNames.add("SKUName");
        fieldNames.add("QTY_B");
        fieldNames.add("QTY_K");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    StockCustomerModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (StockCustomerModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.StockCustomerModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$CustId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustIdIndex);
    }

    public void realmSet$CustId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$tanggal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tanggalIndex);
    }

    public void realmSet$tanggal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tanggalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tanggalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tanggalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tanggalIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SKUId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SKUIdIndex);
    }

    public void realmSet$SKUId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SKUIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SKUIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SKUIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SKUIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SKUName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SKUNameIndex);
    }

    public void realmSet$SKUName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SKUNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SKUNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SKUNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SKUNameIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_B() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_BIndex);
    }

    public void realmSet$QTY_B(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_BIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_BIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_K() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_KIndex);
    }

    public void realmSet$QTY_K(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_KIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_KIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("StockCustomerModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("StockCustomerModel");
            realmObjectSchema.add(new Property("CustId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tanggal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SKUId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SKUName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_B", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_K", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("StockCustomerModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_StockCustomerModel")) {
            Table table = sharedRealm.getTable("class_StockCustomerModel");
            table.addColumn(RealmFieldType.STRING, "CustId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "tanggal", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SKUId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SKUName", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_B", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_K", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_StockCustomerModel");
    }

    public static StockCustomerModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_StockCustomerModel")) {
            Table table = sharedRealm.getTable("class_StockCustomerModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 8) {
                if (columnCount < 8) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 8 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 8 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 8 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final StockCustomerModelColumnInfo columnInfo = new StockCustomerModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("CustId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustId' is required. Either set @Required to field 'CustId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("tanggal")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tanggal' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tanggal") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'tanggal' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.tanggalIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tanggal' is required. Either set @Required to field 'tanggal' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SKUId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SKUId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SKUId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SKUId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SKUIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SKUId' is required. Either set @Required to field 'SKUId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SKUName")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SKUName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SKUName") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SKUName' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SKUNameIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SKUName' is required. Either set @Required to field 'SKUName' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_B")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_B' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_B") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_B' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_BIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_B' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_B' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_K")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_K' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_K") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_K' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_KIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_K' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_K' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'StockCustomerModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_StockCustomerModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.StockCustomerModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.StockCustomerModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.StockCustomerModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.StockCustomerModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.StockCustomerModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.StockCustomerModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.StockCustomerModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("CustId")) {
            if (json.isNull("CustId")) {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$CustId(null);
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$CustId((String) json.getString("CustId"));
            }
        }
        if (json.has("tanggal")) {
            if (json.isNull("tanggal")) {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$tanggal(null);
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$tanggal((String) json.getString("tanggal"));
            }
        }
        if (json.has("SKUId")) {
            if (json.isNull("SKUId")) {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUId(null);
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUId((String) json.getString("SKUId"));
            }
        }
        if (json.has("SKUName")) {
            if (json.isNull("SKUName")) {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUName(null);
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUName((String) json.getString("SKUName"));
            }
        }
        if (json.has("QTY_B")) {
            if (json.isNull("QTY_B")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$QTY_B((int) json.getInt("QTY_B"));
            }
        }
        if (json.has("QTY_K")) {
            if (json.isNull("QTY_K")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$QTY_K((int) json.getInt("QTY_K"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((StockCustomerModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.StockCustomerModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.StockCustomerModel obj = new com.bhn.sadix.Data.StockCustomerModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CustId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$CustId(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$CustId((String) reader.nextString());
                }
            } else if (name.equals("tanggal")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$tanggal(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$tanggal((String) reader.nextString());
                }
            } else if (name.equals("SKUId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUId(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUId((String) reader.nextString());
                }
            } else if (name.equals("SKUName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUName(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$SKUName((String) reader.nextString());
                }
            } else if (name.equals("QTY_B")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$QTY_B((int) reader.nextInt());
                }
            } else if (name.equals("QTY_K")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$QTY_K((int) reader.nextInt());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((StockCustomerModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.StockCustomerModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.StockCustomerModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockCustomerModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.StockCustomerModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((StockCustomerModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.StockCustomerModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.StockCustomerModel copy(Realm realm, com.bhn.sadix.Data.StockCustomerModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockCustomerModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.StockCustomerModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.StockCustomerModel.class, ((StockCustomerModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$CustId(((StockCustomerModelRealmProxyInterface) newObject).realmGet$CustId());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$tanggal(((StockCustomerModelRealmProxyInterface) newObject).realmGet$tanggal());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$SKUId(((StockCustomerModelRealmProxyInterface) newObject).realmGet$SKUId());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$SKUName(((StockCustomerModelRealmProxyInterface) newObject).realmGet$SKUName());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$QTY_B(((StockCustomerModelRealmProxyInterface) newObject).realmGet$QTY_B());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$QTY_K(((StockCustomerModelRealmProxyInterface) newObject).realmGet$QTY_K());
            ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$CommonID(((StockCustomerModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.StockCustomerModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockCustomerModelColumnInfo columnInfo = (StockCustomerModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((StockCustomerModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((StockCustomerModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        }
        String realmGet$tanggal = ((StockCustomerModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        }
        String realmGet$SKUId = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        }
        String realmGet$SKUName = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUName();
        if (realmGet$SKUName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, realmGet$SKUName, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        String realmGet$CommonID = ((StockCustomerModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockCustomerModelColumnInfo columnInfo = (StockCustomerModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.StockCustomerModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockCustomerModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((StockCustomerModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((StockCustomerModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                }
                String realmGet$tanggal = ((StockCustomerModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                }
                String realmGet$SKUId = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                }
                String realmGet$SKUName = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUName();
                if (realmGet$SKUName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, realmGet$SKUName, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                String realmGet$CommonID = ((StockCustomerModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.StockCustomerModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockCustomerModelColumnInfo columnInfo = (StockCustomerModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((StockCustomerModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((StockCustomerModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
        }
        String realmGet$tanggal = ((StockCustomerModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
        }
        String realmGet$SKUId = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
        }
        String realmGet$SKUName = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUName();
        if (realmGet$SKUName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, realmGet$SKUName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        String realmGet$CommonID = ((StockCustomerModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockCustomerModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockCustomerModelColumnInfo columnInfo = (StockCustomerModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockCustomerModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.StockCustomerModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockCustomerModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((StockCustomerModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((StockCustomerModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
                }
                String realmGet$tanggal = ((StockCustomerModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
                }
                String realmGet$SKUId = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
                }
                String realmGet$SKUName = ((StockCustomerModelRealmProxyInterface)object).realmGet$SKUName();
                if (realmGet$SKUName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, realmGet$SKUName, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SKUNameIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((StockCustomerModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                String realmGet$CommonID = ((StockCustomerModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.StockCustomerModel createDetachedCopy(com.bhn.sadix.Data.StockCustomerModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.StockCustomerModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.StockCustomerModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.StockCustomerModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.StockCustomerModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$CustId(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$CustId());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$tanggal(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$tanggal());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$SKUId(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$SKUId());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$SKUName(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$SKUName());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$QTY_B(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$QTY_B());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$QTY_K(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$QTY_K());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((StockCustomerModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((StockCustomerModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.StockCustomerModel update(Realm realm, com.bhn.sadix.Data.StockCustomerModel realmObject, com.bhn.sadix.Data.StockCustomerModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$CustId(((StockCustomerModelRealmProxyInterface) newObject).realmGet$CustId());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$tanggal(((StockCustomerModelRealmProxyInterface) newObject).realmGet$tanggal());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$SKUId(((StockCustomerModelRealmProxyInterface) newObject).realmGet$SKUId());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$SKUName(((StockCustomerModelRealmProxyInterface) newObject).realmGet$SKUName());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$QTY_B(((StockCustomerModelRealmProxyInterface) newObject).realmGet$QTY_B());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$QTY_K(((StockCustomerModelRealmProxyInterface) newObject).realmGet$QTY_K());
        ((StockCustomerModelRealmProxyInterface) realmObject).realmSet$CommonID(((StockCustomerModelRealmProxyInterface) newObject).realmGet$CommonID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("StockCustomerModel = [");
        stringBuilder.append("{CustId:");
        stringBuilder.append(realmGet$CustId() != null ? realmGet$CustId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tanggal:");
        stringBuilder.append(realmGet$tanggal() != null ? realmGet$tanggal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SKUId:");
        stringBuilder.append(realmGet$SKUId() != null ? realmGet$SKUId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SKUName:");
        stringBuilder.append(realmGet$SKUName() != null ? realmGet$SKUName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_B:");
        stringBuilder.append(realmGet$QTY_B());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_K:");
        stringBuilder.append(realmGet$QTY_K());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockCustomerModelRealmProxy aStockCustomerModel = (StockCustomerModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aStockCustomerModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aStockCustomerModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aStockCustomerModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
