package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DiscountPromoModelRealmProxy extends com.bhn.sadix.Data.DiscountPromoModel
    implements RealmObjectProxy, DiscountPromoModelRealmProxyInterface {

    static final class DiscountPromoModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long DiscountPromoIDIndex;
        public long CustomerIdIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        DiscountPromoModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(4);
            this.DiscountPromoIDIndex = getValidColumnIndex(path, table, "DiscountPromoModel", "DiscountPromoID");
            indicesMap.put("DiscountPromoID", this.DiscountPromoIDIndex);
            this.CustomerIdIndex = getValidColumnIndex(path, table, "DiscountPromoModel", "CustomerId");
            indicesMap.put("CustomerId", this.CustomerIdIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "DiscountPromoModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "DiscountPromoModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final DiscountPromoModelColumnInfo otherInfo = (DiscountPromoModelColumnInfo) other;
            this.DiscountPromoIDIndex = otherInfo.DiscountPromoIDIndex;
            this.CustomerIdIndex = otherInfo.CustomerIdIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final DiscountPromoModelColumnInfo clone() {
            return (DiscountPromoModelColumnInfo) super.clone();
        }

    }
    private DiscountPromoModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.DiscountPromoModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("DiscountPromoID");
        fieldNames.add("CustomerId");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    DiscountPromoModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (DiscountPromoModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.DiscountPromoModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public int realmGet$DiscountPromoID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.DiscountPromoIDIndex);
    }

    public void realmSet$DiscountPromoID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.DiscountPromoIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.DiscountPromoIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$CustomerId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.CustomerIdIndex);
    }

    public void realmSet$CustomerId(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.CustomerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.CustomerIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.RandomIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.RandomIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.RandomIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.RandomIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("DiscountPromoModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("DiscountPromoModel");
            realmObjectSchema.add(new Property("DiscountPromoID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerId", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("DiscountPromoModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_DiscountPromoModel")) {
            Table table = sharedRealm.getTable("class_DiscountPromoModel");
            table.addColumn(RealmFieldType.INTEGER, "DiscountPromoID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "CustomerId", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_DiscountPromoModel");
    }

    public static DiscountPromoModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_DiscountPromoModel")) {
            Table table = sharedRealm.getTable("class_DiscountPromoModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 4) {
                if (columnCount < 4) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 4 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 4 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 4 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final DiscountPromoModelColumnInfo columnInfo = new DiscountPromoModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("DiscountPromoID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DiscountPromoID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DiscountPromoID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'DiscountPromoID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DiscountPromoIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DiscountPromoID' does support null values in the existing Realm file. Use corresponding boxed type for field 'DiscountPromoID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerId") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'CustomerId' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.CustomerIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerId' does support null values in the existing Realm file. Use corresponding boxed type for field 'CustomerId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'RandomID' is required. Either set @Required to field 'RandomID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'DiscountPromoModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_DiscountPromoModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.DiscountPromoModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.DiscountPromoModel obj = realm.createObjectInternal(com.bhn.sadix.Data.DiscountPromoModel.class, true, excludeFields);
        if (json.has("DiscountPromoID")) {
            if (json.isNull("DiscountPromoID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountPromoID' to null.");
            } else {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$DiscountPromoID((int) json.getInt("DiscountPromoID"));
            }
        }
        if (json.has("CustomerId")) {
            if (json.isNull("CustomerId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'CustomerId' to null.");
            } else {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CustomerId((int) json.getInt("CustomerId"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("RandomID")) {
            if (json.isNull("RandomID")) {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$RandomID(null);
            } else {
                ((DiscountPromoModelRealmProxyInterface) obj).realmSet$RandomID((String) json.getString("RandomID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.DiscountPromoModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.DiscountPromoModel obj = new com.bhn.sadix.Data.DiscountPromoModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("DiscountPromoID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DiscountPromoID' to null.");
                } else {
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$DiscountPromoID((int) reader.nextInt());
                }
            } else if (name.equals("CustomerId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'CustomerId' to null.");
                } else {
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CustomerId((int) reader.nextInt());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((DiscountPromoModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.DiscountPromoModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.DiscountPromoModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.DiscountPromoModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.DiscountPromoModel copy(Realm realm, com.bhn.sadix.Data.DiscountPromoModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.DiscountPromoModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.DiscountPromoModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.DiscountPromoModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((DiscountPromoModelRealmProxyInterface) realmObject).realmSet$DiscountPromoID(((DiscountPromoModelRealmProxyInterface) newObject).realmGet$DiscountPromoID());
            ((DiscountPromoModelRealmProxyInterface) realmObject).realmSet$CustomerId(((DiscountPromoModelRealmProxyInterface) newObject).realmGet$CustomerId());
            ((DiscountPromoModelRealmProxyInterface) realmObject).realmSet$CommonID(((DiscountPromoModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((DiscountPromoModelRealmProxyInterface) realmObject).realmSet$RandomID(((DiscountPromoModelRealmProxyInterface) newObject).realmGet$RandomID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.DiscountPromoModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountPromoModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountPromoModelColumnInfo columnInfo = (DiscountPromoModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountPromoModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountPromoIDIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$DiscountPromoID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$CustomerId(), false);
        String realmGet$CommonID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$RandomID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountPromoModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountPromoModelColumnInfo columnInfo = (DiscountPromoModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountPromoModel.class);
        com.bhn.sadix.Data.DiscountPromoModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.DiscountPromoModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountPromoIDIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$DiscountPromoID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$CustomerId(), false);
                String realmGet$CommonID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$RandomID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.DiscountPromoModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountPromoModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountPromoModelColumnInfo columnInfo = (DiscountPromoModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountPromoModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.DiscountPromoIDIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$DiscountPromoID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$CustomerId(), false);
        String realmGet$CommonID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$RandomID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.DiscountPromoModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        DiscountPromoModelColumnInfo columnInfo = (DiscountPromoModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.DiscountPromoModel.class);
        com.bhn.sadix.Data.DiscountPromoModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.DiscountPromoModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.DiscountPromoIDIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$DiscountPromoID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, ((DiscountPromoModelRealmProxyInterface)object).realmGet$CustomerId(), false);
                String realmGet$CommonID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$RandomID = ((DiscountPromoModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.DiscountPromoModel createDetachedCopy(com.bhn.sadix.Data.DiscountPromoModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.DiscountPromoModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.DiscountPromoModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.DiscountPromoModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.DiscountPromoModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((DiscountPromoModelRealmProxyInterface) unmanagedObject).realmSet$DiscountPromoID(((DiscountPromoModelRealmProxyInterface) realmObject).realmGet$DiscountPromoID());
        ((DiscountPromoModelRealmProxyInterface) unmanagedObject).realmSet$CustomerId(((DiscountPromoModelRealmProxyInterface) realmObject).realmGet$CustomerId());
        ((DiscountPromoModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((DiscountPromoModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((DiscountPromoModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((DiscountPromoModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("DiscountPromoModel = [");
        stringBuilder.append("{DiscountPromoID:");
        stringBuilder.append(realmGet$DiscountPromoID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerId:");
        stringBuilder.append(realmGet$CustomerId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscountPromoModelRealmProxy aDiscountPromoModel = (DiscountPromoModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aDiscountPromoModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aDiscountPromoModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aDiscountPromoModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
