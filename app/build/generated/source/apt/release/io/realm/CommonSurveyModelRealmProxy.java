package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CommonSurveyModelRealmProxy extends com.bhn.sadix.Data.CommonSurveyModel
    implements RealmObjectProxy, CommonSurveyModelRealmProxyInterface {

    static final class CommonSurveyModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CommonIdIndex;
        public long CustomerIdIndex;
        public long SalesIdIndex;
        public long GeoLatIndex;
        public long GeoLongIndex;
        public long BeginSurveyTimeIndex;
        public long EndSurveyTimeIndex;
        public long FormIdIndex;
        public long nmCustomerIndex;
        public long nmFormIndex;
        public long doneIndex;

        CommonSurveyModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(11);
            this.CommonIdIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "CommonId");
            indicesMap.put("CommonId", this.CommonIdIndex);
            this.CustomerIdIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "CustomerId");
            indicesMap.put("CustomerId", this.CustomerIdIndex);
            this.SalesIdIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "SalesId");
            indicesMap.put("SalesId", this.SalesIdIndex);
            this.GeoLatIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "GeoLat");
            indicesMap.put("GeoLat", this.GeoLatIndex);
            this.GeoLongIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "GeoLong");
            indicesMap.put("GeoLong", this.GeoLongIndex);
            this.BeginSurveyTimeIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "BeginSurveyTime");
            indicesMap.put("BeginSurveyTime", this.BeginSurveyTimeIndex);
            this.EndSurveyTimeIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "EndSurveyTime");
            indicesMap.put("EndSurveyTime", this.EndSurveyTimeIndex);
            this.FormIdIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "FormId");
            indicesMap.put("FormId", this.FormIdIndex);
            this.nmCustomerIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "nmCustomer");
            indicesMap.put("nmCustomer", this.nmCustomerIndex);
            this.nmFormIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "nmForm");
            indicesMap.put("nmForm", this.nmFormIndex);
            this.doneIndex = getValidColumnIndex(path, table, "CommonSurveyModel", "done");
            indicesMap.put("done", this.doneIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final CommonSurveyModelColumnInfo otherInfo = (CommonSurveyModelColumnInfo) other;
            this.CommonIdIndex = otherInfo.CommonIdIndex;
            this.CustomerIdIndex = otherInfo.CustomerIdIndex;
            this.SalesIdIndex = otherInfo.SalesIdIndex;
            this.GeoLatIndex = otherInfo.GeoLatIndex;
            this.GeoLongIndex = otherInfo.GeoLongIndex;
            this.BeginSurveyTimeIndex = otherInfo.BeginSurveyTimeIndex;
            this.EndSurveyTimeIndex = otherInfo.EndSurveyTimeIndex;
            this.FormIdIndex = otherInfo.FormIdIndex;
            this.nmCustomerIndex = otherInfo.nmCustomerIndex;
            this.nmFormIndex = otherInfo.nmFormIndex;
            this.doneIndex = otherInfo.doneIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final CommonSurveyModelColumnInfo clone() {
            return (CommonSurveyModelColumnInfo) super.clone();
        }

    }
    private CommonSurveyModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.CommonSurveyModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CommonId");
        fieldNames.add("CustomerId");
        fieldNames.add("SalesId");
        fieldNames.add("GeoLat");
        fieldNames.add("GeoLong");
        fieldNames.add("BeginSurveyTime");
        fieldNames.add("EndSurveyTime");
        fieldNames.add("FormId");
        fieldNames.add("nmCustomer");
        fieldNames.add("nmForm");
        fieldNames.add("done");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    CommonSurveyModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CommonSurveyModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.CommonSurveyModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIdIndex);
    }

    public void realmSet$CommonId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CustomerId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustomerIdIndex);
    }

    public void realmSet$CustomerId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustomerIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustomerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustomerIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustomerIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SalesId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SalesIdIndex);
    }

    public void realmSet$SalesId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SalesIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SalesIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SalesIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SalesIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLat() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLatIndex);
    }

    public void realmSet$GeoLat(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLatIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLatIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLatIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLatIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLong() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLongIndex);
    }

    public void realmSet$GeoLong(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLongIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLongIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLongIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLongIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$BeginSurveyTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.BeginSurveyTimeIndex);
    }

    public void realmSet$BeginSurveyTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.BeginSurveyTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.BeginSurveyTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.BeginSurveyTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.BeginSurveyTimeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$EndSurveyTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.EndSurveyTimeIndex);
    }

    public void realmSet$EndSurveyTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.EndSurveyTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.EndSurveyTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.EndSurveyTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.EndSurveyTimeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$FormId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.FormIdIndex);
    }

    public void realmSet$FormId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.FormIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.FormIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.FormIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.FormIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$nmCustomer() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nmCustomerIndex);
    }

    public void realmSet$nmCustomer(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nmCustomerIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nmCustomerIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nmCustomerIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nmCustomerIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$nmForm() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nmFormIndex);
    }

    public void realmSet$nmForm(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.nmFormIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.nmFormIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.nmFormIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.nmFormIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$done() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.doneIndex);
    }

    public void realmSet$done(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.doneIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.doneIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("CommonSurveyModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("CommonSurveyModel");
            realmObjectSchema.add(new Property("CommonId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SalesId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLat", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLong", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("BeginSurveyTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("EndSurveyTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("FormId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("nmCustomer", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("nmForm", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("done", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("CommonSurveyModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_CommonSurveyModel")) {
            Table table = sharedRealm.getTable("class_CommonSurveyModel");
            table.addColumn(RealmFieldType.STRING, "CommonId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CustomerId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SalesId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLat", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLong", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "BeginSurveyTime", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "EndSurveyTime", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "FormId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "nmCustomer", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "nmForm", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "done", Table.NOT_NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_CommonSurveyModel");
    }

    public static CommonSurveyModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_CommonSurveyModel")) {
            Table table = sharedRealm.getTable("class_CommonSurveyModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 11) {
                if (columnCount < 11) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 11 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 11 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 11 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final CommonSurveyModelColumnInfo columnInfo = new CommonSurveyModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("CommonId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonId' is required. Either set @Required to field 'CommonId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustomerId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustomerIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerId' is required. Either set @Required to field 'CustomerId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SalesId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SalesId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SalesId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SalesId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SalesIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SalesId' is required. Either set @Required to field 'SalesId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLat")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLat") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLat' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLatIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLat' is required. Either set @Required to field 'GeoLat' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLong")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLong' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLong") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLong' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLongIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLong' is required. Either set @Required to field 'GeoLong' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("BeginSurveyTime")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'BeginSurveyTime' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("BeginSurveyTime") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'BeginSurveyTime' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.BeginSurveyTimeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'BeginSurveyTime' is required. Either set @Required to field 'BeginSurveyTime' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("EndSurveyTime")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'EndSurveyTime' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("EndSurveyTime") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'EndSurveyTime' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.EndSurveyTimeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'EndSurveyTime' is required. Either set @Required to field 'EndSurveyTime' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("FormId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'FormId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("FormId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'FormId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.FormIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'FormId' is required. Either set @Required to field 'FormId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("nmCustomer")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'nmCustomer' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("nmCustomer") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'nmCustomer' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.nmCustomerIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'nmCustomer' is required. Either set @Required to field 'nmCustomer' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("nmForm")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'nmForm' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("nmForm") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'nmForm' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.nmFormIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'nmForm' is required. Either set @Required to field 'nmForm' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("done")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'done' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("done") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'done' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.doneIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'done' does support null values in the existing Realm file. Use corresponding boxed type for field 'done' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'CommonSurveyModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_CommonSurveyModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.CommonSurveyModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.CommonSurveyModel obj = realm.createObjectInternal(com.bhn.sadix.Data.CommonSurveyModel.class, true, excludeFields);
        if (json.has("CommonId")) {
            if (json.isNull("CommonId")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CommonId(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CommonId((String) json.getString("CommonId"));
            }
        }
        if (json.has("CustomerId")) {
            if (json.isNull("CustomerId")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CustomerId(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CustomerId((String) json.getString("CustomerId"));
            }
        }
        if (json.has("SalesId")) {
            if (json.isNull("SalesId")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$SalesId(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$SalesId((String) json.getString("SalesId"));
            }
        }
        if (json.has("GeoLat")) {
            if (json.isNull("GeoLat")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLat(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLat((String) json.getString("GeoLat"));
            }
        }
        if (json.has("GeoLong")) {
            if (json.isNull("GeoLong")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLong(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLong((String) json.getString("GeoLong"));
            }
        }
        if (json.has("BeginSurveyTime")) {
            if (json.isNull("BeginSurveyTime")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$BeginSurveyTime(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$BeginSurveyTime((String) json.getString("BeginSurveyTime"));
            }
        }
        if (json.has("EndSurveyTime")) {
            if (json.isNull("EndSurveyTime")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$EndSurveyTime(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$EndSurveyTime((String) json.getString("EndSurveyTime"));
            }
        }
        if (json.has("FormId")) {
            if (json.isNull("FormId")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$FormId(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$FormId((String) json.getString("FormId"));
            }
        }
        if (json.has("nmCustomer")) {
            if (json.isNull("nmCustomer")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmCustomer(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmCustomer((String) json.getString("nmCustomer"));
            }
        }
        if (json.has("nmForm")) {
            if (json.isNull("nmForm")) {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmForm(null);
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmForm((String) json.getString("nmForm"));
            }
        }
        if (json.has("done")) {
            if (json.isNull("done")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'done' to null.");
            } else {
                ((CommonSurveyModelRealmProxyInterface) obj).realmSet$done((int) json.getInt("done"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.CommonSurveyModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.CommonSurveyModel obj = new com.bhn.sadix.Data.CommonSurveyModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CommonId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CommonId(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CommonId((String) reader.nextString());
                }
            } else if (name.equals("CustomerId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CustomerId(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$CustomerId((String) reader.nextString());
                }
            } else if (name.equals("SalesId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$SalesId(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$SalesId((String) reader.nextString());
                }
            } else if (name.equals("GeoLat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLat(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLat((String) reader.nextString());
                }
            } else if (name.equals("GeoLong")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLong(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$GeoLong((String) reader.nextString());
                }
            } else if (name.equals("BeginSurveyTime")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$BeginSurveyTime(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$BeginSurveyTime((String) reader.nextString());
                }
            } else if (name.equals("EndSurveyTime")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$EndSurveyTime(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$EndSurveyTime((String) reader.nextString());
                }
            } else if (name.equals("FormId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$FormId(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$FormId((String) reader.nextString());
                }
            } else if (name.equals("nmCustomer")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmCustomer(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmCustomer((String) reader.nextString());
                }
            } else if (name.equals("nmForm")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmForm(null);
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$nmForm((String) reader.nextString());
                }
            } else if (name.equals("done")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'done' to null.");
                } else {
                    ((CommonSurveyModelRealmProxyInterface) obj).realmSet$done((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.CommonSurveyModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.CommonSurveyModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CommonSurveyModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.CommonSurveyModel copy(Realm realm, com.bhn.sadix.Data.CommonSurveyModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CommonSurveyModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.CommonSurveyModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.CommonSurveyModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$CommonId(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$CommonId());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$CustomerId(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$CustomerId());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$SalesId(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$SalesId());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$GeoLat(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$GeoLat());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$GeoLong(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$GeoLong());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$BeginSurveyTime(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$BeginSurveyTime());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$EndSurveyTime(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$EndSurveyTime());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$FormId(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$FormId());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$nmCustomer(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$nmCustomer());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$nmForm(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$nmForm());
            ((CommonSurveyModelRealmProxyInterface) realmObject).realmSet$done(((CommonSurveyModelRealmProxyInterface) newObject).realmGet$done());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.CommonSurveyModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CommonSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonSurveyModelColumnInfo columnInfo = (CommonSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonSurveyModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$CommonId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CommonId();
        if (realmGet$CommonId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
        }
        String realmGet$CustomerId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        }
        String realmGet$SalesId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$SalesId();
        if (realmGet$SalesId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
        }
        String realmGet$GeoLat = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        }
        String realmGet$GeoLong = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        }
        String realmGet$BeginSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$BeginSurveyTime();
        if (realmGet$BeginSurveyTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, realmGet$BeginSurveyTime, false);
        }
        String realmGet$EndSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$EndSurveyTime();
        if (realmGet$EndSurveyTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, realmGet$EndSurveyTime, false);
        }
        String realmGet$FormId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$FormId();
        if (realmGet$FormId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
        }
        String realmGet$nmCustomer = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmCustomer();
        if (realmGet$nmCustomer != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, realmGet$nmCustomer, false);
        }
        String realmGet$nmForm = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmForm();
        if (realmGet$nmForm != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nmFormIndex, rowIndex, realmGet$nmForm, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.doneIndex, rowIndex, ((CommonSurveyModelRealmProxyInterface)object).realmGet$done(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CommonSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonSurveyModelColumnInfo columnInfo = (CommonSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonSurveyModel.class);
        com.bhn.sadix.Data.CommonSurveyModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CommonSurveyModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$CommonId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CommonId();
                if (realmGet$CommonId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
                }
                String realmGet$CustomerId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                }
                String realmGet$SalesId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$SalesId();
                if (realmGet$SalesId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
                }
                String realmGet$GeoLat = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                }
                String realmGet$GeoLong = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                }
                String realmGet$BeginSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$BeginSurveyTime();
                if (realmGet$BeginSurveyTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, realmGet$BeginSurveyTime, false);
                }
                String realmGet$EndSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$EndSurveyTime();
                if (realmGet$EndSurveyTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, realmGet$EndSurveyTime, false);
                }
                String realmGet$FormId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$FormId();
                if (realmGet$FormId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
                }
                String realmGet$nmCustomer = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmCustomer();
                if (realmGet$nmCustomer != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, realmGet$nmCustomer, false);
                }
                String realmGet$nmForm = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmForm();
                if (realmGet$nmForm != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.nmFormIndex, rowIndex, realmGet$nmForm, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.doneIndex, rowIndex, ((CommonSurveyModelRealmProxyInterface)object).realmGet$done(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.CommonSurveyModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CommonSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonSurveyModelColumnInfo columnInfo = (CommonSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonSurveyModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$CommonId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CommonId();
        if (realmGet$CommonId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, false);
        }
        String realmGet$CustomerId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
        }
        String realmGet$SalesId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$SalesId();
        if (realmGet$SalesId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, false);
        }
        String realmGet$GeoLat = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
        }
        String realmGet$GeoLong = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
        }
        String realmGet$BeginSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$BeginSurveyTime();
        if (realmGet$BeginSurveyTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, realmGet$BeginSurveyTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, false);
        }
        String realmGet$EndSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$EndSurveyTime();
        if (realmGet$EndSurveyTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, realmGet$EndSurveyTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, false);
        }
        String realmGet$FormId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$FormId();
        if (realmGet$FormId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.FormIdIndex, rowIndex, false);
        }
        String realmGet$nmCustomer = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmCustomer();
        if (realmGet$nmCustomer != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, realmGet$nmCustomer, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, false);
        }
        String realmGet$nmForm = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmForm();
        if (realmGet$nmForm != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nmFormIndex, rowIndex, realmGet$nmForm, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nmFormIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.doneIndex, rowIndex, ((CommonSurveyModelRealmProxyInterface)object).realmGet$done(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CommonSurveyModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonSurveyModelColumnInfo columnInfo = (CommonSurveyModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonSurveyModel.class);
        com.bhn.sadix.Data.CommonSurveyModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CommonSurveyModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$CommonId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CommonId();
                if (realmGet$CommonId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, realmGet$CommonId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIdIndex, rowIndex, false);
                }
                String realmGet$CustomerId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
                }
                String realmGet$SalesId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$SalesId();
                if (realmGet$SalesId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, false);
                }
                String realmGet$GeoLat = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
                }
                String realmGet$GeoLong = ((CommonSurveyModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
                }
                String realmGet$BeginSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$BeginSurveyTime();
                if (realmGet$BeginSurveyTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, realmGet$BeginSurveyTime, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.BeginSurveyTimeIndex, rowIndex, false);
                }
                String realmGet$EndSurveyTime = ((CommonSurveyModelRealmProxyInterface)object).realmGet$EndSurveyTime();
                if (realmGet$EndSurveyTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, realmGet$EndSurveyTime, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.EndSurveyTimeIndex, rowIndex, false);
                }
                String realmGet$FormId = ((CommonSurveyModelRealmProxyInterface)object).realmGet$FormId();
                if (realmGet$FormId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.FormIdIndex, rowIndex, realmGet$FormId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.FormIdIndex, rowIndex, false);
                }
                String realmGet$nmCustomer = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmCustomer();
                if (realmGet$nmCustomer != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, realmGet$nmCustomer, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.nmCustomerIndex, rowIndex, false);
                }
                String realmGet$nmForm = ((CommonSurveyModelRealmProxyInterface)object).realmGet$nmForm();
                if (realmGet$nmForm != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.nmFormIndex, rowIndex, realmGet$nmForm, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.nmFormIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.doneIndex, rowIndex, ((CommonSurveyModelRealmProxyInterface)object).realmGet$done(), false);
            }
        }
    }

    public static com.bhn.sadix.Data.CommonSurveyModel createDetachedCopy(com.bhn.sadix.Data.CommonSurveyModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.CommonSurveyModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.CommonSurveyModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.CommonSurveyModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.CommonSurveyModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$CommonId(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$CommonId());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$CustomerId(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$CustomerId());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$SalesId(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$SalesId());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$GeoLat(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$GeoLat());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$GeoLong(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$GeoLong());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$BeginSurveyTime(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$BeginSurveyTime());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$EndSurveyTime(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$EndSurveyTime());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$FormId(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$FormId());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$nmCustomer(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$nmCustomer());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$nmForm(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$nmForm());
        ((CommonSurveyModelRealmProxyInterface) unmanagedObject).realmSet$done(((CommonSurveyModelRealmProxyInterface) realmObject).realmGet$done());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("CommonSurveyModel = [");
        stringBuilder.append("{CommonId:");
        stringBuilder.append(realmGet$CommonId() != null ? realmGet$CommonId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerId:");
        stringBuilder.append(realmGet$CustomerId() != null ? realmGet$CustomerId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SalesId:");
        stringBuilder.append(realmGet$SalesId() != null ? realmGet$SalesId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLat:");
        stringBuilder.append(realmGet$GeoLat() != null ? realmGet$GeoLat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLong:");
        stringBuilder.append(realmGet$GeoLong() != null ? realmGet$GeoLong() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{BeginSurveyTime:");
        stringBuilder.append(realmGet$BeginSurveyTime() != null ? realmGet$BeginSurveyTime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{EndSurveyTime:");
        stringBuilder.append(realmGet$EndSurveyTime() != null ? realmGet$EndSurveyTime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{FormId:");
        stringBuilder.append(realmGet$FormId() != null ? realmGet$FormId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nmCustomer:");
        stringBuilder.append(realmGet$nmCustomer() != null ? realmGet$nmCustomer() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{nmForm:");
        stringBuilder.append(realmGet$nmForm() != null ? realmGet$nmForm() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{done:");
        stringBuilder.append(realmGet$done());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonSurveyModelRealmProxy aCommonSurveyModel = (CommonSurveyModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCommonSurveyModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCommonSurveyModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCommonSurveyModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
