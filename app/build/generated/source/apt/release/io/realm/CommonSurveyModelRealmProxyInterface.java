package io.realm;


public interface CommonSurveyModelRealmProxyInterface {
    public String realmGet$CommonId();
    public void realmSet$CommonId(String value);
    public String realmGet$CustomerId();
    public void realmSet$CustomerId(String value);
    public String realmGet$SalesId();
    public void realmSet$SalesId(String value);
    public String realmGet$GeoLat();
    public void realmSet$GeoLat(String value);
    public String realmGet$GeoLong();
    public void realmSet$GeoLong(String value);
    public String realmGet$BeginSurveyTime();
    public void realmSet$BeginSurveyTime(String value);
    public String realmGet$EndSurveyTime();
    public void realmSet$EndSurveyTime(String value);
    public String realmGet$FormId();
    public void realmSet$FormId(String value);
    public String realmGet$nmCustomer();
    public void realmSet$nmCustomer(String value);
    public String realmGet$nmForm();
    public void realmSet$nmForm(String value);
    public int realmGet$done();
    public void realmSet$done(int value);
}
