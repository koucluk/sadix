package io.realm;


public interface DiscountPromoModelRealmProxyInterface {
    public int realmGet$DiscountPromoID();
    public void realmSet$DiscountPromoID(int value);
    public int realmGet$CustomerId();
    public void realmSet$CustomerId(int value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
