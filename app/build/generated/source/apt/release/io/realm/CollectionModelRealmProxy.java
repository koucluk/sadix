package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CollectionModelRealmProxy extends com.bhn.sadix.Data.CollectionModel
    implements RealmObjectProxy, CollectionModelRealmProxyInterface {

    static final class CollectionModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CustIdIndex;
        public long statusIndex;
        public long data1Index;
        public long data2Index;
        public long data3Index;
        public long data4Index;
        public long data5Index;
        public long data6Index;
        public long tanggalIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;
        public long payMethodIndex;

        CollectionModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(12);
            this.CustIdIndex = getValidColumnIndex(path, table, "CollectionModel", "CustId");
            indicesMap.put("CustId", this.CustIdIndex);
            this.statusIndex = getValidColumnIndex(path, table, "CollectionModel", "status");
            indicesMap.put("status", this.statusIndex);
            this.data1Index = getValidColumnIndex(path, table, "CollectionModel", "data1");
            indicesMap.put("data1", this.data1Index);
            this.data2Index = getValidColumnIndex(path, table, "CollectionModel", "data2");
            indicesMap.put("data2", this.data2Index);
            this.data3Index = getValidColumnIndex(path, table, "CollectionModel", "data3");
            indicesMap.put("data3", this.data3Index);
            this.data4Index = getValidColumnIndex(path, table, "CollectionModel", "data4");
            indicesMap.put("data4", this.data4Index);
            this.data5Index = getValidColumnIndex(path, table, "CollectionModel", "data5");
            indicesMap.put("data5", this.data5Index);
            this.data6Index = getValidColumnIndex(path, table, "CollectionModel", "data6");
            indicesMap.put("data6", this.data6Index);
            this.tanggalIndex = getValidColumnIndex(path, table, "CollectionModel", "tanggal");
            indicesMap.put("tanggal", this.tanggalIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "CollectionModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "CollectionModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);
            this.payMethodIndex = getValidColumnIndex(path, table, "CollectionModel", "payMethod");
            indicesMap.put("payMethod", this.payMethodIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final CollectionModelColumnInfo otherInfo = (CollectionModelColumnInfo) other;
            this.CustIdIndex = otherInfo.CustIdIndex;
            this.statusIndex = otherInfo.statusIndex;
            this.data1Index = otherInfo.data1Index;
            this.data2Index = otherInfo.data2Index;
            this.data3Index = otherInfo.data3Index;
            this.data4Index = otherInfo.data4Index;
            this.data5Index = otherInfo.data5Index;
            this.data6Index = otherInfo.data6Index;
            this.tanggalIndex = otherInfo.tanggalIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;
            this.payMethodIndex = otherInfo.payMethodIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final CollectionModelColumnInfo clone() {
            return (CollectionModelColumnInfo) super.clone();
        }

    }
    private CollectionModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.CollectionModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CustId");
        fieldNames.add("status");
        fieldNames.add("data1");
        fieldNames.add("data2");
        fieldNames.add("data3");
        fieldNames.add("data4");
        fieldNames.add("data5");
        fieldNames.add("data6");
        fieldNames.add("tanggal");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        fieldNames.add("payMethod");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    CollectionModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CollectionModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.CollectionModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$CustId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustIdIndex);
    }

    public void realmSet$CustId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$status() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.statusIndex);
    }

    public void realmSet$status(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.statusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.statusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.statusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.statusIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data1() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data1Index);
    }

    public void realmSet$data1(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data1Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data1Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data1Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data1Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data2() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data2Index);
    }

    public void realmSet$data2(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data2Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data2Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data2Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data2Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data3() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data3Index);
    }

    public void realmSet$data3(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data3Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data3Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data3Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data3Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data4() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data4Index);
    }

    public void realmSet$data4(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data4Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data4Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data4Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data4Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data5() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data5Index);
    }

    public void realmSet$data5(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data5Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data5Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data5Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data5Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$data6() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.data6Index);
    }

    public void realmSet$data6(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.data6Index, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.data6Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.data6Index);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.data6Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$tanggal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tanggalIndex);
    }

    public void realmSet$tanggal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tanggalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tanggalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tanggalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tanggalIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public String realmGet$payMethod() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.payMethodIndex);
    }

    public void realmSet$payMethod(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.payMethodIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.payMethodIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.payMethodIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.payMethodIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("CollectionModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("CollectionModel");
            realmObjectSchema.add(new Property("CustId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("status", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data1", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data2", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data3", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data4", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data5", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("data6", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tanggal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("payMethod", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("CollectionModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_CollectionModel")) {
            Table table = sharedRealm.getTable("class_CollectionModel");
            table.addColumn(RealmFieldType.STRING, "CustId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "status", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data1", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data2", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data3", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data4", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data5", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "data6", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "tanggal", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "payMethod", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_CollectionModel");
    }

    public static CollectionModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_CollectionModel")) {
            Table table = sharedRealm.getTable("class_CollectionModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 12) {
                if (columnCount < 12) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 12 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 12 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 12 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final CollectionModelColumnInfo columnInfo = new CollectionModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("CustId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustId' is required. Either set @Required to field 'CustId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("status")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'status' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("status") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'status' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.statusIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'status' is required. Either set @Required to field 'status' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data1")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data1' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data1") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data1' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data1Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data1' is required. Either set @Required to field 'data1' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data2")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data2' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data2") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data2' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data2Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data2' is required. Either set @Required to field 'data2' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data3")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data3' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data3") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data3' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data3Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data3' is required. Either set @Required to field 'data3' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data4")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data4' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data4") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data4' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data4Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data4' is required. Either set @Required to field 'data4' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data5")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data5' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data5") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data5' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data5Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data5' is required. Either set @Required to field 'data5' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("data6")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'data6' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("data6") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'data6' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.data6Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'data6' is required. Either set @Required to field 'data6' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("tanggal")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tanggal' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tanggal") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'tanggal' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.tanggalIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tanggal' is required. Either set @Required to field 'tanggal' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("payMethod")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'payMethod' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("payMethod") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'payMethod' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.payMethodIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'payMethod' is required. Either set @Required to field 'payMethod' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'CollectionModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_CollectionModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.CollectionModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.CollectionModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.CollectionModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.CollectionModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CollectionModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.CollectionModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CollectionModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("CustId")) {
            if (json.isNull("CustId")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$CustId(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$CustId((String) json.getString("CustId"));
            }
        }
        if (json.has("status")) {
            if (json.isNull("status")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$status(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$status((String) json.getString("status"));
            }
        }
        if (json.has("data1")) {
            if (json.isNull("data1")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data1(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data1((String) json.getString("data1"));
            }
        }
        if (json.has("data2")) {
            if (json.isNull("data2")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data2(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data2((String) json.getString("data2"));
            }
        }
        if (json.has("data3")) {
            if (json.isNull("data3")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data3(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data3((String) json.getString("data3"));
            }
        }
        if (json.has("data4")) {
            if (json.isNull("data4")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data4(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data4((String) json.getString("data4"));
            }
        }
        if (json.has("data5")) {
            if (json.isNull("data5")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data5(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data5((String) json.getString("data5"));
            }
        }
        if (json.has("data6")) {
            if (json.isNull("data6")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data6(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$data6((String) json.getString("data6"));
            }
        }
        if (json.has("tanggal")) {
            if (json.isNull("tanggal")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$tanggal(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$tanggal((String) json.getString("tanggal"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("payMethod")) {
            if (json.isNull("payMethod")) {
                ((CollectionModelRealmProxyInterface) obj).realmSet$payMethod(null);
            } else {
                ((CollectionModelRealmProxyInterface) obj).realmSet$payMethod((String) json.getString("payMethod"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.CollectionModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.CollectionModel obj = new com.bhn.sadix.Data.CollectionModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CustId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$CustId(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$CustId((String) reader.nextString());
                }
            } else if (name.equals("status")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$status(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$status((String) reader.nextString());
                }
            } else if (name.equals("data1")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data1(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data1((String) reader.nextString());
                }
            } else if (name.equals("data2")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data2(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data2((String) reader.nextString());
                }
            } else if (name.equals("data3")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data3(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data3((String) reader.nextString());
                }
            } else if (name.equals("data4")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data4(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data4((String) reader.nextString());
                }
            } else if (name.equals("data5")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data5(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data5((String) reader.nextString());
                }
            } else if (name.equals("data6")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data6(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$data6((String) reader.nextString());
                }
            } else if (name.equals("tanggal")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$tanggal(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$tanggal((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("payMethod")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CollectionModelRealmProxyInterface) obj).realmSet$payMethod(null);
                } else {
                    ((CollectionModelRealmProxyInterface) obj).realmSet$payMethod((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.CollectionModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.CollectionModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CollectionModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.CollectionModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((CollectionModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.CollectionModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.CollectionModel copy(Realm realm, com.bhn.sadix.Data.CollectionModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CollectionModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.CollectionModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.CollectionModel.class, ((CollectionModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$CustId(((CollectionModelRealmProxyInterface) newObject).realmGet$CustId());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$status(((CollectionModelRealmProxyInterface) newObject).realmGet$status());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data1(((CollectionModelRealmProxyInterface) newObject).realmGet$data1());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data2(((CollectionModelRealmProxyInterface) newObject).realmGet$data2());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data3(((CollectionModelRealmProxyInterface) newObject).realmGet$data3());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data4(((CollectionModelRealmProxyInterface) newObject).realmGet$data4());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data5(((CollectionModelRealmProxyInterface) newObject).realmGet$data5());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$data6(((CollectionModelRealmProxyInterface) newObject).realmGet$data6());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$tanggal(((CollectionModelRealmProxyInterface) newObject).realmGet$tanggal());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$CommonID(((CollectionModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((CollectionModelRealmProxyInterface) realmObject).realmSet$payMethod(((CollectionModelRealmProxyInterface) newObject).realmGet$payMethod());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.CollectionModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CollectionModelColumnInfo columnInfo = (CollectionModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CollectionModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((CollectionModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        }
        String realmGet$status = ((CollectionModelRealmProxyInterface)object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        }
        String realmGet$data1 = ((CollectionModelRealmProxyInterface)object).realmGet$data1();
        if (realmGet$data1 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data1Index, rowIndex, realmGet$data1, false);
        }
        String realmGet$data2 = ((CollectionModelRealmProxyInterface)object).realmGet$data2();
        if (realmGet$data2 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data2Index, rowIndex, realmGet$data2, false);
        }
        String realmGet$data3 = ((CollectionModelRealmProxyInterface)object).realmGet$data3();
        if (realmGet$data3 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data3Index, rowIndex, realmGet$data3, false);
        }
        String realmGet$data4 = ((CollectionModelRealmProxyInterface)object).realmGet$data4();
        if (realmGet$data4 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data4Index, rowIndex, realmGet$data4, false);
        }
        String realmGet$data5 = ((CollectionModelRealmProxyInterface)object).realmGet$data5();
        if (realmGet$data5 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data5Index, rowIndex, realmGet$data5, false);
        }
        String realmGet$data6 = ((CollectionModelRealmProxyInterface)object).realmGet$data6();
        if (realmGet$data6 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data6Index, rowIndex, realmGet$data6, false);
        }
        String realmGet$tanggal = ((CollectionModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        }
        String realmGet$CommonID = ((CollectionModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$payMethod = ((CollectionModelRealmProxyInterface)object).realmGet$payMethod();
        if (realmGet$payMethod != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.payMethodIndex, rowIndex, realmGet$payMethod, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CollectionModelColumnInfo columnInfo = (CollectionModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CollectionModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CollectionModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CollectionModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((CollectionModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                }
                String realmGet$status = ((CollectionModelRealmProxyInterface)object).realmGet$status();
                if (realmGet$status != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
                }
                String realmGet$data1 = ((CollectionModelRealmProxyInterface)object).realmGet$data1();
                if (realmGet$data1 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data1Index, rowIndex, realmGet$data1, false);
                }
                String realmGet$data2 = ((CollectionModelRealmProxyInterface)object).realmGet$data2();
                if (realmGet$data2 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data2Index, rowIndex, realmGet$data2, false);
                }
                String realmGet$data3 = ((CollectionModelRealmProxyInterface)object).realmGet$data3();
                if (realmGet$data3 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data3Index, rowIndex, realmGet$data3, false);
                }
                String realmGet$data4 = ((CollectionModelRealmProxyInterface)object).realmGet$data4();
                if (realmGet$data4 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data4Index, rowIndex, realmGet$data4, false);
                }
                String realmGet$data5 = ((CollectionModelRealmProxyInterface)object).realmGet$data5();
                if (realmGet$data5 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data5Index, rowIndex, realmGet$data5, false);
                }
                String realmGet$data6 = ((CollectionModelRealmProxyInterface)object).realmGet$data6();
                if (realmGet$data6 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data6Index, rowIndex, realmGet$data6, false);
                }
                String realmGet$tanggal = ((CollectionModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                }
                String realmGet$CommonID = ((CollectionModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$payMethod = ((CollectionModelRealmProxyInterface)object).realmGet$payMethod();
                if (realmGet$payMethod != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.payMethodIndex, rowIndex, realmGet$payMethod, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.CollectionModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CollectionModelColumnInfo columnInfo = (CollectionModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CollectionModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((CollectionModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
        }
        String realmGet$status = ((CollectionModelRealmProxyInterface)object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
        }
        String realmGet$data1 = ((CollectionModelRealmProxyInterface)object).realmGet$data1();
        if (realmGet$data1 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data1Index, rowIndex, realmGet$data1, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data1Index, rowIndex, false);
        }
        String realmGet$data2 = ((CollectionModelRealmProxyInterface)object).realmGet$data2();
        if (realmGet$data2 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data2Index, rowIndex, realmGet$data2, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data2Index, rowIndex, false);
        }
        String realmGet$data3 = ((CollectionModelRealmProxyInterface)object).realmGet$data3();
        if (realmGet$data3 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data3Index, rowIndex, realmGet$data3, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data3Index, rowIndex, false);
        }
        String realmGet$data4 = ((CollectionModelRealmProxyInterface)object).realmGet$data4();
        if (realmGet$data4 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data4Index, rowIndex, realmGet$data4, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data4Index, rowIndex, false);
        }
        String realmGet$data5 = ((CollectionModelRealmProxyInterface)object).realmGet$data5();
        if (realmGet$data5 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data5Index, rowIndex, realmGet$data5, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data5Index, rowIndex, false);
        }
        String realmGet$data6 = ((CollectionModelRealmProxyInterface)object).realmGet$data6();
        if (realmGet$data6 != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.data6Index, rowIndex, realmGet$data6, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.data6Index, rowIndex, false);
        }
        String realmGet$tanggal = ((CollectionModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((CollectionModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$payMethod = ((CollectionModelRealmProxyInterface)object).realmGet$payMethod();
        if (realmGet$payMethod != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.payMethodIndex, rowIndex, realmGet$payMethod, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.payMethodIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CollectionModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CollectionModelColumnInfo columnInfo = (CollectionModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CollectionModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CollectionModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CollectionModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CollectionModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((CollectionModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
                }
                String realmGet$status = ((CollectionModelRealmProxyInterface)object).realmGet$status();
                if (realmGet$status != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
                }
                String realmGet$data1 = ((CollectionModelRealmProxyInterface)object).realmGet$data1();
                if (realmGet$data1 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data1Index, rowIndex, realmGet$data1, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data1Index, rowIndex, false);
                }
                String realmGet$data2 = ((CollectionModelRealmProxyInterface)object).realmGet$data2();
                if (realmGet$data2 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data2Index, rowIndex, realmGet$data2, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data2Index, rowIndex, false);
                }
                String realmGet$data3 = ((CollectionModelRealmProxyInterface)object).realmGet$data3();
                if (realmGet$data3 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data3Index, rowIndex, realmGet$data3, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data3Index, rowIndex, false);
                }
                String realmGet$data4 = ((CollectionModelRealmProxyInterface)object).realmGet$data4();
                if (realmGet$data4 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data4Index, rowIndex, realmGet$data4, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data4Index, rowIndex, false);
                }
                String realmGet$data5 = ((CollectionModelRealmProxyInterface)object).realmGet$data5();
                if (realmGet$data5 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data5Index, rowIndex, realmGet$data5, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data5Index, rowIndex, false);
                }
                String realmGet$data6 = ((CollectionModelRealmProxyInterface)object).realmGet$data6();
                if (realmGet$data6 != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.data6Index, rowIndex, realmGet$data6, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.data6Index, rowIndex, false);
                }
                String realmGet$tanggal = ((CollectionModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((CollectionModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$payMethod = ((CollectionModelRealmProxyInterface)object).realmGet$payMethod();
                if (realmGet$payMethod != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.payMethodIndex, rowIndex, realmGet$payMethod, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.payMethodIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.CollectionModel createDetachedCopy(com.bhn.sadix.Data.CollectionModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.CollectionModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.CollectionModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.CollectionModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.CollectionModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$CustId(((CollectionModelRealmProxyInterface) realmObject).realmGet$CustId());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$status(((CollectionModelRealmProxyInterface) realmObject).realmGet$status());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data1(((CollectionModelRealmProxyInterface) realmObject).realmGet$data1());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data2(((CollectionModelRealmProxyInterface) realmObject).realmGet$data2());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data3(((CollectionModelRealmProxyInterface) realmObject).realmGet$data3());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data4(((CollectionModelRealmProxyInterface) realmObject).realmGet$data4());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data5(((CollectionModelRealmProxyInterface) realmObject).realmGet$data5());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$data6(((CollectionModelRealmProxyInterface) realmObject).realmGet$data6());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$tanggal(((CollectionModelRealmProxyInterface) realmObject).realmGet$tanggal());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((CollectionModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((CollectionModelRealmProxyInterface) realmObject).realmGet$RandomID());
        ((CollectionModelRealmProxyInterface) unmanagedObject).realmSet$payMethod(((CollectionModelRealmProxyInterface) realmObject).realmGet$payMethod());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.CollectionModel update(Realm realm, com.bhn.sadix.Data.CollectionModel realmObject, com.bhn.sadix.Data.CollectionModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$CustId(((CollectionModelRealmProxyInterface) newObject).realmGet$CustId());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$status(((CollectionModelRealmProxyInterface) newObject).realmGet$status());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data1(((CollectionModelRealmProxyInterface) newObject).realmGet$data1());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data2(((CollectionModelRealmProxyInterface) newObject).realmGet$data2());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data3(((CollectionModelRealmProxyInterface) newObject).realmGet$data3());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data4(((CollectionModelRealmProxyInterface) newObject).realmGet$data4());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data5(((CollectionModelRealmProxyInterface) newObject).realmGet$data5());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$data6(((CollectionModelRealmProxyInterface) newObject).realmGet$data6());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$tanggal(((CollectionModelRealmProxyInterface) newObject).realmGet$tanggal());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$CommonID(((CollectionModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((CollectionModelRealmProxyInterface) realmObject).realmSet$payMethod(((CollectionModelRealmProxyInterface) newObject).realmGet$payMethod());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("CollectionModel = [");
        stringBuilder.append("{CustId:");
        stringBuilder.append(realmGet$CustId() != null ? realmGet$CustId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{status:");
        stringBuilder.append(realmGet$status() != null ? realmGet$status() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data1:");
        stringBuilder.append(realmGet$data1() != null ? realmGet$data1() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data2:");
        stringBuilder.append(realmGet$data2() != null ? realmGet$data2() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data3:");
        stringBuilder.append(realmGet$data3() != null ? realmGet$data3() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data4:");
        stringBuilder.append(realmGet$data4() != null ? realmGet$data4() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data5:");
        stringBuilder.append(realmGet$data5() != null ? realmGet$data5() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{data6:");
        stringBuilder.append(realmGet$data6() != null ? realmGet$data6() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tanggal:");
        stringBuilder.append(realmGet$tanggal() != null ? realmGet$tanggal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{payMethod:");
        stringBuilder.append(realmGet$payMethod() != null ? realmGet$payMethod() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CollectionModelRealmProxy aCollectionModel = (CollectionModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCollectionModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCollectionModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCollectionModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
