package io.realm;


public interface PriceMonitoringModelRealmProxyInterface {
    public String realmGet$SKUId();
    public void realmSet$SKUId(String value);
    public int realmGet$ProductID();
    public void realmSet$ProductID(int value);
    public String realmGet$ProductName();
    public void realmSet$ProductName(String value);
    public long realmGet$Qty();
    public void realmSet$Qty(long value);
    public long realmGet$BuyPrice();
    public void realmSet$BuyPrice(long value);
    public long realmGet$SellPrice();
    public void realmSet$SellPrice(long value);
    public int realmGet$LevelID();
    public void realmSet$LevelID(int value);
    public String realmGet$Note();
    public void realmSet$Note(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
