package io.realm;


import android.util.JsonReader;
import io.realm.RealmObjectSchema;
import io.realm.internal.ColumnInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>();
        modelClasses.add(com.bhn.sadix.Data.DiscountPromoModel.class);
        modelClasses.add(com.bhn.sadix.Data.StockGroupModel.class);
        modelClasses.add(com.bhn.sadix.Data.MerchandiserModel.class);
        modelClasses.add(com.bhn.sadix.Data.AssetMutasiModel.class);
        modelClasses.add(com.bhn.sadix.Data.StockCompetitorModel.class);
        modelClasses.add(com.bhn.sadix.Data.DiscountResultModel.class);
        modelClasses.add(com.bhn.sadix.Data.CustomerBrandingModel.class);
        modelClasses.add(com.bhn.sadix.Data.StockOutletModel.class);
        modelClasses.add(com.bhn.sadix.Data.SurveyModel.class);
        modelClasses.add(com.bhn.sadix.Data.PriceMonitoringModel.class);
        modelClasses.add(com.bhn.sadix.Data.TakingOrderModel.class);
        modelClasses.add(com.bhn.sadix.Data.AssetModel.class);
        modelClasses.add(com.bhn.sadix.Data.CommonSurveyModel.class);
        modelClasses.add(com.bhn.sadix.Data.PrincipleModel.class);
        modelClasses.add(com.bhn.sadix.Data.AssetServiceModel.class);
        modelClasses.add(com.bhn.sadix.Data.PrincipleOrderModel.class);
        modelClasses.add(com.bhn.sadix.Data.HasilSurveyModel.class);
        modelClasses.add(com.bhn.sadix.Data.ReturnModel.class);
        modelClasses.add(com.bhn.sadix.Data.CollectionModel.class);
        modelClasses.add(com.bhn.sadix.Data.StockCustomerModel.class);
        modelClasses.add(com.bhn.sadix.Data.CommonModel.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Table createTable(Class<? extends RealmModel> clazz, SharedRealm sharedRealm) {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return io.realm.DiscountPromoModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return io.realm.StockGroupModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return io.realm.MerchandiserModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return io.realm.AssetMutasiModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return io.realm.StockCompetitorModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return io.realm.DiscountResultModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return io.realm.CustomerBrandingModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return io.realm.StockOutletModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return io.realm.SurveyModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return io.realm.PriceMonitoringModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return io.realm.TakingOrderModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return io.realm.AssetModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return io.realm.CommonSurveyModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return io.realm.PrincipleModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return io.realm.AssetServiceModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return io.realm.PrincipleOrderModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return io.realm.HasilSurveyModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return io.realm.ReturnModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return io.realm.CollectionModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return io.realm.StockCustomerModelRealmProxy.initTable(sharedRealm);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return io.realm.CommonModelRealmProxy.initTable(sharedRealm);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public RealmObjectSchema createRealmObjectSchema(Class<? extends RealmModel> clazz, RealmSchema realmSchema) {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return io.realm.DiscountPromoModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return io.realm.StockGroupModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return io.realm.MerchandiserModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return io.realm.AssetMutasiModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return io.realm.StockCompetitorModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return io.realm.DiscountResultModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return io.realm.CustomerBrandingModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return io.realm.StockOutletModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return io.realm.SurveyModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return io.realm.PriceMonitoringModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return io.realm.TakingOrderModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return io.realm.AssetModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return io.realm.CommonSurveyModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return io.realm.PrincipleModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return io.realm.AssetServiceModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return io.realm.PrincipleOrderModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return io.realm.HasilSurveyModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return io.realm.ReturnModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return io.realm.CollectionModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return io.realm.StockCustomerModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return io.realm.CommonModelRealmProxy.createRealmObjectSchema(realmSchema);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public ColumnInfo validateTable(Class<? extends RealmModel> clazz, SharedRealm sharedRealm, boolean allowExtraColumns) {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return io.realm.DiscountPromoModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return io.realm.StockGroupModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return io.realm.MerchandiserModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return io.realm.AssetMutasiModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return io.realm.StockCompetitorModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return io.realm.DiscountResultModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return io.realm.CustomerBrandingModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return io.realm.StockOutletModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return io.realm.SurveyModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return io.realm.PriceMonitoringModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return io.realm.TakingOrderModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return io.realm.AssetModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return io.realm.CommonSurveyModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return io.realm.PrincipleModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return io.realm.AssetServiceModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return io.realm.PrincipleOrderModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return io.realm.HasilSurveyModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return io.realm.ReturnModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return io.realm.CollectionModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return io.realm.StockCustomerModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return io.realm.CommonModelRealmProxy.validateTable(sharedRealm, allowExtraColumns);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return io.realm.DiscountPromoModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return io.realm.StockGroupModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return io.realm.MerchandiserModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return io.realm.AssetMutasiModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return io.realm.StockCompetitorModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return io.realm.DiscountResultModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return io.realm.CustomerBrandingModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return io.realm.StockOutletModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return io.realm.SurveyModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return io.realm.PriceMonitoringModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return io.realm.TakingOrderModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return io.realm.AssetModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return io.realm.CommonSurveyModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return io.realm.PrincipleModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return io.realm.AssetServiceModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return io.realm.PrincipleOrderModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return io.realm.HasilSurveyModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return io.realm.ReturnModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return io.realm.CollectionModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return io.realm.StockCustomerModelRealmProxy.getFieldNames();
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return io.realm.CommonModelRealmProxy.getFieldNames();
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public String getTableName(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return io.realm.DiscountPromoModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return io.realm.StockGroupModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return io.realm.MerchandiserModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return io.realm.AssetMutasiModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return io.realm.StockCompetitorModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return io.realm.DiscountResultModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return io.realm.CustomerBrandingModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return io.realm.StockOutletModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return io.realm.SurveyModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return io.realm.PriceMonitoringModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return io.realm.TakingOrderModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return io.realm.AssetModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return io.realm.CommonSurveyModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return io.realm.PrincipleModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return io.realm.AssetServiceModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return io.realm.PrincipleOrderModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return io.realm.HasilSurveyModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return io.realm.ReturnModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return io.realm.CollectionModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return io.realm.StockCustomerModelRealmProxy.getTableName();
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return io.realm.CommonModelRealmProxy.getTableName();
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
                return clazz.cast(new io.realm.DiscountPromoModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
                return clazz.cast(new io.realm.StockGroupModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
                return clazz.cast(new io.realm.MerchandiserModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
                return clazz.cast(new io.realm.AssetMutasiModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
                return clazz.cast(new io.realm.StockCompetitorModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
                return clazz.cast(new io.realm.DiscountResultModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
                return clazz.cast(new io.realm.CustomerBrandingModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
                return clazz.cast(new io.realm.StockOutletModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
                return clazz.cast(new io.realm.SurveyModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
                return clazz.cast(new io.realm.PriceMonitoringModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
                return clazz.cast(new io.realm.TakingOrderModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
                return clazz.cast(new io.realm.AssetModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
                return clazz.cast(new io.realm.CommonSurveyModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
                return clazz.cast(new io.realm.PrincipleModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
                return clazz.cast(new io.realm.AssetServiceModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
                return clazz.cast(new io.realm.PrincipleOrderModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
                return clazz.cast(new io.realm.HasilSurveyModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
                return clazz.cast(new io.realm.ReturnModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
                return clazz.cast(new io.realm.CollectionModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
                return clazz.cast(new io.realm.StockCustomerModelRealmProxy());
            } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
                return clazz.cast(new io.realm.CommonModelRealmProxy());
            } else {
                throw getMissingProxyClassException(clazz);
            }
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return clazz.cast(io.realm.DiscountPromoModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.DiscountPromoModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return clazz.cast(io.realm.StockGroupModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.StockGroupModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return clazz.cast(io.realm.MerchandiserModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.MerchandiserModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return clazz.cast(io.realm.AssetMutasiModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.AssetMutasiModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return clazz.cast(io.realm.StockCompetitorModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.StockCompetitorModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return clazz.cast(io.realm.DiscountResultModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.DiscountResultModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return clazz.cast(io.realm.CustomerBrandingModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.CustomerBrandingModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return clazz.cast(io.realm.StockOutletModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.StockOutletModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return clazz.cast(io.realm.SurveyModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.SurveyModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return clazz.cast(io.realm.PriceMonitoringModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.PriceMonitoringModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return clazz.cast(io.realm.TakingOrderModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.TakingOrderModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return clazz.cast(io.realm.AssetModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.AssetModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return clazz.cast(io.realm.CommonSurveyModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.CommonSurveyModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return clazz.cast(io.realm.PrincipleModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.PrincipleModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return clazz.cast(io.realm.AssetServiceModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.AssetServiceModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return clazz.cast(io.realm.PrincipleOrderModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.PrincipleOrderModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return clazz.cast(io.realm.HasilSurveyModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.HasilSurveyModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return clazz.cast(io.realm.ReturnModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.ReturnModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return clazz.cast(io.realm.CollectionModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.CollectionModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return clazz.cast(io.realm.StockCustomerModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.StockCustomerModel) obj, update, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return clazz.cast(io.realm.CommonModelRealmProxy.copyOrUpdate(realm, (com.bhn.sadix.Data.CommonModel) obj, update, cache));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            io.realm.DiscountPromoModelRealmProxy.insert(realm, (com.bhn.sadix.Data.DiscountPromoModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            io.realm.StockGroupModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockGroupModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            io.realm.MerchandiserModelRealmProxy.insert(realm, (com.bhn.sadix.Data.MerchandiserModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            io.realm.AssetMutasiModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetMutasiModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            io.realm.StockCompetitorModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockCompetitorModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            io.realm.DiscountResultModelRealmProxy.insert(realm, (com.bhn.sadix.Data.DiscountResultModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            io.realm.CustomerBrandingModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CustomerBrandingModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            io.realm.StockOutletModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockOutletModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            io.realm.SurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.SurveyModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            io.realm.PriceMonitoringModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PriceMonitoringModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            io.realm.TakingOrderModelRealmProxy.insert(realm, (com.bhn.sadix.Data.TakingOrderModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            io.realm.AssetModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            io.realm.CommonSurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CommonSurveyModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            io.realm.PrincipleModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PrincipleModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            io.realm.AssetServiceModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetServiceModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            io.realm.PrincipleOrderModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PrincipleOrderModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            io.realm.HasilSurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.HasilSurveyModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            io.realm.ReturnModelRealmProxy.insert(realm, (com.bhn.sadix.Data.ReturnModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            io.realm.CollectionModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CollectionModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            io.realm.StockCustomerModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockCustomerModel) object, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            io.realm.CommonModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CommonModel) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
                io.realm.DiscountPromoModelRealmProxy.insert(realm, (com.bhn.sadix.Data.DiscountPromoModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
                io.realm.StockGroupModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockGroupModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
                io.realm.MerchandiserModelRealmProxy.insert(realm, (com.bhn.sadix.Data.MerchandiserModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
                io.realm.AssetMutasiModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetMutasiModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
                io.realm.StockCompetitorModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockCompetitorModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
                io.realm.DiscountResultModelRealmProxy.insert(realm, (com.bhn.sadix.Data.DiscountResultModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
                io.realm.CustomerBrandingModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CustomerBrandingModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
                io.realm.StockOutletModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockOutletModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
                io.realm.SurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.SurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
                io.realm.PriceMonitoringModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PriceMonitoringModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
                io.realm.TakingOrderModelRealmProxy.insert(realm, (com.bhn.sadix.Data.TakingOrderModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
                io.realm.AssetModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
                io.realm.CommonSurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CommonSurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
                io.realm.PrincipleModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PrincipleModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
                io.realm.AssetServiceModelRealmProxy.insert(realm, (com.bhn.sadix.Data.AssetServiceModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
                io.realm.PrincipleOrderModelRealmProxy.insert(realm, (com.bhn.sadix.Data.PrincipleOrderModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
                io.realm.HasilSurveyModelRealmProxy.insert(realm, (com.bhn.sadix.Data.HasilSurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
                io.realm.ReturnModelRealmProxy.insert(realm, (com.bhn.sadix.Data.ReturnModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
                io.realm.CollectionModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CollectionModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
                io.realm.StockCustomerModelRealmProxy.insert(realm, (com.bhn.sadix.Data.StockCustomerModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
                io.realm.CommonModelRealmProxy.insert(realm, (com.bhn.sadix.Data.CommonModel) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
                    io.realm.DiscountPromoModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
                    io.realm.StockGroupModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
                    io.realm.MerchandiserModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
                    io.realm.AssetMutasiModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
                    io.realm.StockCompetitorModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
                    io.realm.DiscountResultModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
                    io.realm.CustomerBrandingModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
                    io.realm.StockOutletModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
                    io.realm.SurveyModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
                    io.realm.PriceMonitoringModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
                    io.realm.TakingOrderModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
                    io.realm.AssetModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
                    io.realm.CommonSurveyModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
                    io.realm.PrincipleModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
                    io.realm.AssetServiceModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
                    io.realm.PrincipleOrderModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
                    io.realm.HasilSurveyModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
                    io.realm.ReturnModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
                    io.realm.CollectionModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
                    io.realm.StockCustomerModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
                    io.realm.CommonModelRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            io.realm.DiscountPromoModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.DiscountPromoModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            io.realm.StockGroupModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockGroupModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            io.realm.MerchandiserModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.MerchandiserModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            io.realm.AssetMutasiModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetMutasiModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            io.realm.StockCompetitorModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockCompetitorModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            io.realm.DiscountResultModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.DiscountResultModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            io.realm.CustomerBrandingModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CustomerBrandingModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            io.realm.StockOutletModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockOutletModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            io.realm.SurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.SurveyModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            io.realm.PriceMonitoringModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PriceMonitoringModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            io.realm.TakingOrderModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.TakingOrderModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            io.realm.AssetModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            io.realm.CommonSurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CommonSurveyModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            io.realm.PrincipleModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PrincipleModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            io.realm.AssetServiceModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetServiceModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            io.realm.PrincipleOrderModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PrincipleOrderModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            io.realm.HasilSurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.HasilSurveyModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            io.realm.ReturnModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.ReturnModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            io.realm.CollectionModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CollectionModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            io.realm.StockCustomerModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockCustomerModel) obj, cache);
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            io.realm.CommonModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CommonModel) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
                io.realm.DiscountPromoModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.DiscountPromoModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
                io.realm.StockGroupModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockGroupModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
                io.realm.MerchandiserModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.MerchandiserModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
                io.realm.AssetMutasiModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetMutasiModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
                io.realm.StockCompetitorModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockCompetitorModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
                io.realm.DiscountResultModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.DiscountResultModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
                io.realm.CustomerBrandingModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CustomerBrandingModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
                io.realm.StockOutletModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockOutletModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
                io.realm.SurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.SurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
                io.realm.PriceMonitoringModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PriceMonitoringModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
                io.realm.TakingOrderModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.TakingOrderModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
                io.realm.AssetModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
                io.realm.CommonSurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CommonSurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
                io.realm.PrincipleModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PrincipleModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
                io.realm.AssetServiceModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.AssetServiceModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
                io.realm.PrincipleOrderModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.PrincipleOrderModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
                io.realm.HasilSurveyModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.HasilSurveyModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
                io.realm.ReturnModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.ReturnModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
                io.realm.CollectionModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CollectionModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
                io.realm.StockCustomerModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.StockCustomerModel) object, cache);
            } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
                io.realm.CommonModelRealmProxy.insertOrUpdate(realm, (com.bhn.sadix.Data.CommonModel) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
                    io.realm.DiscountPromoModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
                    io.realm.StockGroupModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
                    io.realm.MerchandiserModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
                    io.realm.AssetMutasiModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
                    io.realm.StockCompetitorModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
                    io.realm.DiscountResultModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
                    io.realm.CustomerBrandingModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
                    io.realm.StockOutletModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
                    io.realm.SurveyModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
                    io.realm.PriceMonitoringModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
                    io.realm.TakingOrderModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
                    io.realm.AssetModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
                    io.realm.CommonSurveyModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
                    io.realm.PrincipleModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
                    io.realm.AssetServiceModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
                    io.realm.PrincipleOrderModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
                    io.realm.HasilSurveyModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
                    io.realm.ReturnModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
                    io.realm.CollectionModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
                    io.realm.StockCustomerModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
                    io.realm.CommonModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return clazz.cast(io.realm.DiscountPromoModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return clazz.cast(io.realm.StockGroupModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return clazz.cast(io.realm.MerchandiserModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return clazz.cast(io.realm.AssetMutasiModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return clazz.cast(io.realm.StockCompetitorModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return clazz.cast(io.realm.DiscountResultModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return clazz.cast(io.realm.CustomerBrandingModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return clazz.cast(io.realm.StockOutletModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return clazz.cast(io.realm.SurveyModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return clazz.cast(io.realm.PriceMonitoringModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return clazz.cast(io.realm.TakingOrderModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return clazz.cast(io.realm.AssetModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return clazz.cast(io.realm.CommonSurveyModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return clazz.cast(io.realm.PrincipleModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return clazz.cast(io.realm.AssetServiceModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return clazz.cast(io.realm.PrincipleOrderModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return clazz.cast(io.realm.HasilSurveyModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return clazz.cast(io.realm.ReturnModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return clazz.cast(io.realm.CollectionModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return clazz.cast(io.realm.StockCustomerModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return clazz.cast(io.realm.CommonModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return clazz.cast(io.realm.DiscountPromoModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return clazz.cast(io.realm.StockGroupModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return clazz.cast(io.realm.MerchandiserModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return clazz.cast(io.realm.AssetMutasiModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return clazz.cast(io.realm.StockCompetitorModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return clazz.cast(io.realm.DiscountResultModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return clazz.cast(io.realm.CustomerBrandingModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return clazz.cast(io.realm.StockOutletModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return clazz.cast(io.realm.SurveyModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return clazz.cast(io.realm.PriceMonitoringModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return clazz.cast(io.realm.TakingOrderModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return clazz.cast(io.realm.AssetModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return clazz.cast(io.realm.CommonSurveyModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return clazz.cast(io.realm.PrincipleModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return clazz.cast(io.realm.AssetServiceModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return clazz.cast(io.realm.PrincipleOrderModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return clazz.cast(io.realm.HasilSurveyModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return clazz.cast(io.realm.ReturnModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return clazz.cast(io.realm.CollectionModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return clazz.cast(io.realm.StockCustomerModelRealmProxy.createUsingJsonStream(realm, reader));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return clazz.cast(io.realm.CommonModelRealmProxy.createUsingJsonStream(realm, reader));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(com.bhn.sadix.Data.DiscountPromoModel.class)) {
            return clazz.cast(io.realm.DiscountPromoModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.DiscountPromoModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockGroupModel.class)) {
            return clazz.cast(io.realm.StockGroupModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.StockGroupModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.MerchandiserModel.class)) {
            return clazz.cast(io.realm.MerchandiserModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.MerchandiserModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetMutasiModel.class)) {
            return clazz.cast(io.realm.AssetMutasiModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.AssetMutasiModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCompetitorModel.class)) {
            return clazz.cast(io.realm.StockCompetitorModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.StockCompetitorModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.DiscountResultModel.class)) {
            return clazz.cast(io.realm.DiscountResultModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.DiscountResultModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CustomerBrandingModel.class)) {
            return clazz.cast(io.realm.CustomerBrandingModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.CustomerBrandingModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockOutletModel.class)) {
            return clazz.cast(io.realm.StockOutletModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.StockOutletModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.SurveyModel.class)) {
            return clazz.cast(io.realm.SurveyModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.SurveyModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PriceMonitoringModel.class)) {
            return clazz.cast(io.realm.PriceMonitoringModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.PriceMonitoringModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.TakingOrderModel.class)) {
            return clazz.cast(io.realm.TakingOrderModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.TakingOrderModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetModel.class)) {
            return clazz.cast(io.realm.AssetModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.AssetModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonSurveyModel.class)) {
            return clazz.cast(io.realm.CommonSurveyModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.CommonSurveyModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleModel.class)) {
            return clazz.cast(io.realm.PrincipleModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.PrincipleModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.AssetServiceModel.class)) {
            return clazz.cast(io.realm.AssetServiceModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.AssetServiceModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.PrincipleOrderModel.class)) {
            return clazz.cast(io.realm.PrincipleOrderModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.PrincipleOrderModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.HasilSurveyModel.class)) {
            return clazz.cast(io.realm.HasilSurveyModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.HasilSurveyModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.ReturnModel.class)) {
            return clazz.cast(io.realm.ReturnModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.ReturnModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CollectionModel.class)) {
            return clazz.cast(io.realm.CollectionModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.CollectionModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.StockCustomerModel.class)) {
            return clazz.cast(io.realm.StockCustomerModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.StockCustomerModel) realmObject, 0, maxDepth, cache));
        } else if (clazz.equals(com.bhn.sadix.Data.CommonModel.class)) {
            return clazz.cast(io.realm.CommonModelRealmProxy.createDetachedCopy((com.bhn.sadix.Data.CommonModel) realmObject, 0, maxDepth, cache));
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

}
