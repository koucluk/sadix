package io.realm;


public interface AssetModelRealmProxyInterface {
    public String realmGet$AsetID();
    public void realmSet$AsetID(String value);
    public int realmGet$Idx();
    public void realmSet$Idx(int value);
    public String realmGet$Name();
    public void realmSet$Name(String value);
    public String realmGet$GroupAset();
    public void realmSet$GroupAset(String value);
    public int realmGet$TypeMnt();
    public void realmSet$TypeMnt(int value);
    public int realmGet$Posisi();
    public void realmSet$Posisi(int value);
    public String realmGet$Value();
    public void realmSet$Value(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$NameFile();
    public void realmSet$NameFile(String value);
    public int realmGet$tipe();
    public void realmSet$tipe(int value);
    public int realmGet$JenisWidget();
    public void realmSet$JenisWidget(int value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
