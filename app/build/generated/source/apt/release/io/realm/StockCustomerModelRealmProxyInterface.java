package io.realm;


public interface StockCustomerModelRealmProxyInterface {
    public String realmGet$CustId();
    public void realmSet$CustId(String value);
    public String realmGet$tanggal();
    public void realmSet$tanggal(String value);
    public String realmGet$SKUId();
    public void realmSet$SKUId(String value);
    public String realmGet$SKUName();
    public void realmSet$SKUName(String value);
    public int realmGet$QTY_B();
    public void realmSet$QTY_B(int value);
    public int realmGet$QTY_K();
    public void realmSet$QTY_K(int value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
