package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReturnModelRealmProxy extends com.bhn.sadix.Data.ReturnModel
    implements RealmObjectProxy, ReturnModelRealmProxyInterface {

    static final class ReturnModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CustIdIndex;
        public long SKUIdIndex;
        public long tanggalIndex;
        public long QTY_BIndex;
        public long QTY_KIndex;
        public long noteIndex;
        public long tipeIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        ReturnModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(9);
            this.CustIdIndex = getValidColumnIndex(path, table, "ReturnModel", "CustId");
            indicesMap.put("CustId", this.CustIdIndex);
            this.SKUIdIndex = getValidColumnIndex(path, table, "ReturnModel", "SKUId");
            indicesMap.put("SKUId", this.SKUIdIndex);
            this.tanggalIndex = getValidColumnIndex(path, table, "ReturnModel", "tanggal");
            indicesMap.put("tanggal", this.tanggalIndex);
            this.QTY_BIndex = getValidColumnIndex(path, table, "ReturnModel", "QTY_B");
            indicesMap.put("QTY_B", this.QTY_BIndex);
            this.QTY_KIndex = getValidColumnIndex(path, table, "ReturnModel", "QTY_K");
            indicesMap.put("QTY_K", this.QTY_KIndex);
            this.noteIndex = getValidColumnIndex(path, table, "ReturnModel", "note");
            indicesMap.put("note", this.noteIndex);
            this.tipeIndex = getValidColumnIndex(path, table, "ReturnModel", "tipe");
            indicesMap.put("tipe", this.tipeIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "ReturnModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "ReturnModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final ReturnModelColumnInfo otherInfo = (ReturnModelColumnInfo) other;
            this.CustIdIndex = otherInfo.CustIdIndex;
            this.SKUIdIndex = otherInfo.SKUIdIndex;
            this.tanggalIndex = otherInfo.tanggalIndex;
            this.QTY_BIndex = otherInfo.QTY_BIndex;
            this.QTY_KIndex = otherInfo.QTY_KIndex;
            this.noteIndex = otherInfo.noteIndex;
            this.tipeIndex = otherInfo.tipeIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final ReturnModelColumnInfo clone() {
            return (ReturnModelColumnInfo) super.clone();
        }

    }
    private ReturnModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.ReturnModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CustId");
        fieldNames.add("SKUId");
        fieldNames.add("tanggal");
        fieldNames.add("QTY_B");
        fieldNames.add("QTY_K");
        fieldNames.add("note");
        fieldNames.add("tipe");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    ReturnModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ReturnModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.ReturnModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$CustId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustIdIndex);
    }

    public void realmSet$CustId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SKUId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SKUIdIndex);
    }

    public void realmSet$SKUId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SKUIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SKUIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SKUIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SKUIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$tanggal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tanggalIndex);
    }

    public void realmSet$tanggal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tanggalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tanggalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tanggalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tanggalIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_B() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_BIndex);
    }

    public void realmSet$QTY_B(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_BIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_BIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_K() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_KIndex);
    }

    public void realmSet$QTY_K(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_KIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_KIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$note() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.noteIndex);
    }

    public void realmSet$note(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.noteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.noteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.noteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.noteIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$tipe() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tipeIndex);
    }

    public void realmSet$tipe(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tipeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tipeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tipeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tipeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("ReturnModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("ReturnModel");
            realmObjectSchema.add(new Property("CustId", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SKUId", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tanggal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_B", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_K", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("note", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tipe", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("ReturnModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_ReturnModel")) {
            Table table = sharedRealm.getTable("class_ReturnModel");
            table.addColumn(RealmFieldType.STRING, "CustId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SKUId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "tanggal", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_B", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_K", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "note", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "tipe", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("CustId"));
            table.addSearchIndex(table.getColumnIndex("SKUId"));
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_ReturnModel");
    }

    public static ReturnModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_ReturnModel")) {
            Table table = sharedRealm.getTable("class_ReturnModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 9) {
                if (columnCount < 9) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 9 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 9 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 9 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final ReturnModelColumnInfo columnInfo = new ReturnModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("CustId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustId' is required. Either set @Required to field 'CustId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("CustId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'CustId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("SKUId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SKUId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SKUId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SKUId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SKUIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SKUId' is required. Either set @Required to field 'SKUId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("SKUId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'SKUId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("tanggal")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tanggal' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tanggal") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'tanggal' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.tanggalIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tanggal' is required. Either set @Required to field 'tanggal' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_B")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_B' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_B") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_B' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_BIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_B' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_B' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_K")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_K' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_K") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_K' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_KIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_K' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_K' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("note")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'note' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("note") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'note' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.noteIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'note' is required. Either set @Required to field 'note' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("tipe")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tipe' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tipe") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'tipe' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.tipeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tipe' is required. Either set @Required to field 'tipe' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'ReturnModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_ReturnModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.ReturnModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.ReturnModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.ReturnModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.ReturnModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.ReturnModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.ReturnModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.ReturnModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("CustId")) {
            if (json.isNull("CustId")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$CustId(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$CustId((String) json.getString("CustId"));
            }
        }
        if (json.has("SKUId")) {
            if (json.isNull("SKUId")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$SKUId(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$SKUId((String) json.getString("SKUId"));
            }
        }
        if (json.has("tanggal")) {
            if (json.isNull("tanggal")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$tanggal(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$tanggal((String) json.getString("tanggal"));
            }
        }
        if (json.has("QTY_B")) {
            if (json.isNull("QTY_B")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$QTY_B((int) json.getInt("QTY_B"));
            }
        }
        if (json.has("QTY_K")) {
            if (json.isNull("QTY_K")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$QTY_K((int) json.getInt("QTY_K"));
            }
        }
        if (json.has("note")) {
            if (json.isNull("note")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$note(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$note((String) json.getString("note"));
            }
        }
        if (json.has("tipe")) {
            if (json.isNull("tipe")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$tipe(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$tipe((String) json.getString("tipe"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((ReturnModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((ReturnModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.ReturnModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.ReturnModel obj = new com.bhn.sadix.Data.ReturnModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CustId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$CustId(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$CustId((String) reader.nextString());
                }
            } else if (name.equals("SKUId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$SKUId(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$SKUId((String) reader.nextString());
                }
            } else if (name.equals("tanggal")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$tanggal(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$tanggal((String) reader.nextString());
                }
            } else if (name.equals("QTY_B")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$QTY_B((int) reader.nextInt());
                }
            } else if (name.equals("QTY_K")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$QTY_K((int) reader.nextInt());
                }
            } else if (name.equals("note")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$note(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$note((String) reader.nextString());
                }
            } else if (name.equals("tipe")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$tipe(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$tipe((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ReturnModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((ReturnModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.ReturnModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.ReturnModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.ReturnModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.ReturnModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((ReturnModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.ReturnModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.ReturnModel copy(Realm realm, com.bhn.sadix.Data.ReturnModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.ReturnModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.ReturnModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.ReturnModel.class, ((ReturnModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$CustId(((ReturnModelRealmProxyInterface) newObject).realmGet$CustId());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$SKUId(((ReturnModelRealmProxyInterface) newObject).realmGet$SKUId());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$tanggal(((ReturnModelRealmProxyInterface) newObject).realmGet$tanggal());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$QTY_B(((ReturnModelRealmProxyInterface) newObject).realmGet$QTY_B());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$QTY_K(((ReturnModelRealmProxyInterface) newObject).realmGet$QTY_K());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$note(((ReturnModelRealmProxyInterface) newObject).realmGet$note());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$tipe(((ReturnModelRealmProxyInterface) newObject).realmGet$tipe());
            ((ReturnModelRealmProxyInterface) realmObject).realmSet$CommonID(((ReturnModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.ReturnModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        ReturnModelColumnInfo columnInfo = (ReturnModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((ReturnModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((ReturnModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        }
        String realmGet$SKUId = ((ReturnModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        }
        String realmGet$tanggal = ((ReturnModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        String realmGet$note = ((ReturnModelRealmProxyInterface)object).realmGet$note();
        if (realmGet$note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
        }
        String realmGet$tipe = ((ReturnModelRealmProxyInterface)object).realmGet$tipe();
        if (realmGet$tipe != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tipeIndex, rowIndex, realmGet$tipe, false);
        }
        String realmGet$CommonID = ((ReturnModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        ReturnModelColumnInfo columnInfo = (ReturnModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.ReturnModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.ReturnModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((ReturnModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((ReturnModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                }
                String realmGet$SKUId = ((ReturnModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                }
                String realmGet$tanggal = ((ReturnModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                String realmGet$note = ((ReturnModelRealmProxyInterface)object).realmGet$note();
                if (realmGet$note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
                }
                String realmGet$tipe = ((ReturnModelRealmProxyInterface)object).realmGet$tipe();
                if (realmGet$tipe != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tipeIndex, rowIndex, realmGet$tipe, false);
                }
                String realmGet$CommonID = ((ReturnModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.ReturnModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        ReturnModelColumnInfo columnInfo = (ReturnModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((ReturnModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((ReturnModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
        }
        String realmGet$SKUId = ((ReturnModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
        }
        String realmGet$tanggal = ((ReturnModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        String realmGet$note = ((ReturnModelRealmProxyInterface)object).realmGet$note();
        if (realmGet$note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.noteIndex, rowIndex, false);
        }
        String realmGet$tipe = ((ReturnModelRealmProxyInterface)object).realmGet$tipe();
        if (realmGet$tipe != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tipeIndex, rowIndex, realmGet$tipe, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tipeIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((ReturnModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.ReturnModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        ReturnModelColumnInfo columnInfo = (ReturnModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.ReturnModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.ReturnModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.ReturnModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((ReturnModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((ReturnModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
                }
                String realmGet$SKUId = ((ReturnModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
                }
                String realmGet$tanggal = ((ReturnModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((ReturnModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                String realmGet$note = ((ReturnModelRealmProxyInterface)object).realmGet$note();
                if (realmGet$note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.noteIndex, rowIndex, false);
                }
                String realmGet$tipe = ((ReturnModelRealmProxyInterface)object).realmGet$tipe();
                if (realmGet$tipe != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tipeIndex, rowIndex, realmGet$tipe, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.tipeIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((ReturnModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.ReturnModel createDetachedCopy(com.bhn.sadix.Data.ReturnModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.ReturnModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.ReturnModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.ReturnModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.ReturnModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$CustId(((ReturnModelRealmProxyInterface) realmObject).realmGet$CustId());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$SKUId(((ReturnModelRealmProxyInterface) realmObject).realmGet$SKUId());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$tanggal(((ReturnModelRealmProxyInterface) realmObject).realmGet$tanggal());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$QTY_B(((ReturnModelRealmProxyInterface) realmObject).realmGet$QTY_B());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$QTY_K(((ReturnModelRealmProxyInterface) realmObject).realmGet$QTY_K());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$note(((ReturnModelRealmProxyInterface) realmObject).realmGet$note());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$tipe(((ReturnModelRealmProxyInterface) realmObject).realmGet$tipe());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((ReturnModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((ReturnModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((ReturnModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.ReturnModel update(Realm realm, com.bhn.sadix.Data.ReturnModel realmObject, com.bhn.sadix.Data.ReturnModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$CustId(((ReturnModelRealmProxyInterface) newObject).realmGet$CustId());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$SKUId(((ReturnModelRealmProxyInterface) newObject).realmGet$SKUId());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$tanggal(((ReturnModelRealmProxyInterface) newObject).realmGet$tanggal());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$QTY_B(((ReturnModelRealmProxyInterface) newObject).realmGet$QTY_B());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$QTY_K(((ReturnModelRealmProxyInterface) newObject).realmGet$QTY_K());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$note(((ReturnModelRealmProxyInterface) newObject).realmGet$note());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$tipe(((ReturnModelRealmProxyInterface) newObject).realmGet$tipe());
        ((ReturnModelRealmProxyInterface) realmObject).realmSet$CommonID(((ReturnModelRealmProxyInterface) newObject).realmGet$CommonID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ReturnModel = [");
        stringBuilder.append("{CustId:");
        stringBuilder.append(realmGet$CustId() != null ? realmGet$CustId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SKUId:");
        stringBuilder.append(realmGet$SKUId() != null ? realmGet$SKUId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tanggal:");
        stringBuilder.append(realmGet$tanggal() != null ? realmGet$tanggal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_B:");
        stringBuilder.append(realmGet$QTY_B());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_K:");
        stringBuilder.append(realmGet$QTY_K());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{note:");
        stringBuilder.append(realmGet$note() != null ? realmGet$note() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tipe:");
        stringBuilder.append(realmGet$tipe() != null ? realmGet$tipe() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReturnModelRealmProxy aReturnModel = (ReturnModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aReturnModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aReturnModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aReturnModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
