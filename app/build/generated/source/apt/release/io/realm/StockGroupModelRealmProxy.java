package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StockGroupModelRealmProxy extends com.bhn.sadix.Data.StockGroupModel
    implements RealmObjectProxy, StockGroupModelRealmProxyInterface {

    static final class StockGroupModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long GroupIdIndex;
        public long CustomerIdIndex;
        public long QuantityIndex;
        public long Quantity2Index;
        public long CommonIDIndex;
        public long RandomIDIndex;

        StockGroupModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(6);
            this.GroupIdIndex = getValidColumnIndex(path, table, "StockGroupModel", "GroupId");
            indicesMap.put("GroupId", this.GroupIdIndex);
            this.CustomerIdIndex = getValidColumnIndex(path, table, "StockGroupModel", "CustomerId");
            indicesMap.put("CustomerId", this.CustomerIdIndex);
            this.QuantityIndex = getValidColumnIndex(path, table, "StockGroupModel", "Quantity");
            indicesMap.put("Quantity", this.QuantityIndex);
            this.Quantity2Index = getValidColumnIndex(path, table, "StockGroupModel", "Quantity2");
            indicesMap.put("Quantity2", this.Quantity2Index);
            this.CommonIDIndex = getValidColumnIndex(path, table, "StockGroupModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "StockGroupModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final StockGroupModelColumnInfo otherInfo = (StockGroupModelColumnInfo) other;
            this.GroupIdIndex = otherInfo.GroupIdIndex;
            this.CustomerIdIndex = otherInfo.CustomerIdIndex;
            this.QuantityIndex = otherInfo.QuantityIndex;
            this.Quantity2Index = otherInfo.Quantity2Index;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final StockGroupModelColumnInfo clone() {
            return (StockGroupModelColumnInfo) super.clone();
        }

    }
    private StockGroupModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.StockGroupModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("GroupId");
        fieldNames.add("CustomerId");
        fieldNames.add("Quantity");
        fieldNames.add("Quantity2");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    StockGroupModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (StockGroupModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.StockGroupModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$GroupId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GroupIdIndex);
    }

    public void realmSet$GroupId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GroupIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GroupIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GroupIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GroupIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CustomerId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustomerIdIndex);
    }

    public void realmSet$CustomerId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustomerIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustomerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustomerIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustomerIdIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Quantity() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QuantityIndex);
    }

    public void realmSet$Quantity(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QuantityIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QuantityIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Quantity2() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.Quantity2Index);
    }

    public void realmSet$Quantity2(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.Quantity2Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.Quantity2Index, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("StockGroupModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("StockGroupModel");
            realmObjectSchema.add(new Property("GroupId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Quantity", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Quantity2", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("StockGroupModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_StockGroupModel")) {
            Table table = sharedRealm.getTable("class_StockGroupModel");
            table.addColumn(RealmFieldType.STRING, "GroupId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CustomerId", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Quantity", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Quantity2", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_StockGroupModel");
    }

    public static StockGroupModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_StockGroupModel")) {
            Table table = sharedRealm.getTable("class_StockGroupModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 6) {
                if (columnCount < 6) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 6 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 6 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 6 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final StockGroupModelColumnInfo columnInfo = new StockGroupModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("GroupId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GroupId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GroupId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GroupId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GroupIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GroupId' is required. Either set @Required to field 'GroupId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustomerId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustomerIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerId' is required. Either set @Required to field 'CustomerId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Quantity")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Quantity' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Quantity") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Quantity' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QuantityIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Quantity' does support null values in the existing Realm file. Use corresponding boxed type for field 'Quantity' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Quantity2")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Quantity2' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Quantity2") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Quantity2' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.Quantity2Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Quantity2' does support null values in the existing Realm file. Use corresponding boxed type for field 'Quantity2' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'StockGroupModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_StockGroupModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.StockGroupModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.StockGroupModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.StockGroupModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.StockGroupModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.StockGroupModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.StockGroupModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.StockGroupModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("GroupId")) {
            if (json.isNull("GroupId")) {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$GroupId(null);
            } else {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$GroupId((String) json.getString("GroupId"));
            }
        }
        if (json.has("CustomerId")) {
            if (json.isNull("CustomerId")) {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$CustomerId(null);
            } else {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$CustomerId((String) json.getString("CustomerId"));
            }
        }
        if (json.has("Quantity")) {
            if (json.isNull("Quantity")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Quantity' to null.");
            } else {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$Quantity((int) json.getInt("Quantity"));
            }
        }
        if (json.has("Quantity2")) {
            if (json.isNull("Quantity2")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Quantity2' to null.");
            } else {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$Quantity2((int) json.getInt("Quantity2"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((StockGroupModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.StockGroupModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.StockGroupModel obj = new com.bhn.sadix.Data.StockGroupModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("GroupId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$GroupId(null);
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$GroupId((String) reader.nextString());
                }
            } else if (name.equals("CustomerId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$CustomerId(null);
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$CustomerId((String) reader.nextString());
                }
            } else if (name.equals("Quantity")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Quantity' to null.");
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$Quantity((int) reader.nextInt());
                }
            } else if (name.equals("Quantity2")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Quantity2' to null.");
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$Quantity2((int) reader.nextInt());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((StockGroupModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.StockGroupModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.StockGroupModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockGroupModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.StockGroupModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((StockGroupModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.StockGroupModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.StockGroupModel copy(Realm realm, com.bhn.sadix.Data.StockGroupModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockGroupModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.StockGroupModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.StockGroupModel.class, ((StockGroupModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((StockGroupModelRealmProxyInterface) realmObject).realmSet$GroupId(((StockGroupModelRealmProxyInterface) newObject).realmGet$GroupId());
            ((StockGroupModelRealmProxyInterface) realmObject).realmSet$CustomerId(((StockGroupModelRealmProxyInterface) newObject).realmGet$CustomerId());
            ((StockGroupModelRealmProxyInterface) realmObject).realmSet$Quantity(((StockGroupModelRealmProxyInterface) newObject).realmGet$Quantity());
            ((StockGroupModelRealmProxyInterface) realmObject).realmSet$Quantity2(((StockGroupModelRealmProxyInterface) newObject).realmGet$Quantity2());
            ((StockGroupModelRealmProxyInterface) realmObject).realmSet$CommonID(((StockGroupModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.StockGroupModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockGroupModelColumnInfo columnInfo = (StockGroupModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((StockGroupModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$GroupId = ((StockGroupModelRealmProxyInterface)object).realmGet$GroupId();
        if (realmGet$GroupId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, realmGet$GroupId, false);
        }
        String realmGet$CustomerId = ((StockGroupModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QuantityIndex, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.Quantity2Index, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity2(), false);
        String realmGet$CommonID = ((StockGroupModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockGroupModelColumnInfo columnInfo = (StockGroupModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.StockGroupModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockGroupModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((StockGroupModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$GroupId = ((StockGroupModelRealmProxyInterface)object).realmGet$GroupId();
                if (realmGet$GroupId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, realmGet$GroupId, false);
                }
                String realmGet$CustomerId = ((StockGroupModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QuantityIndex, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.Quantity2Index, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity2(), false);
                String realmGet$CommonID = ((StockGroupModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.StockGroupModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockGroupModelColumnInfo columnInfo = (StockGroupModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((StockGroupModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$GroupId = ((StockGroupModelRealmProxyInterface)object).realmGet$GroupId();
        if (realmGet$GroupId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, realmGet$GroupId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, false);
        }
        String realmGet$CustomerId = ((StockGroupModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QuantityIndex, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.Quantity2Index, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity2(), false);
        String realmGet$CommonID = ((StockGroupModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockGroupModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockGroupModelColumnInfo columnInfo = (StockGroupModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockGroupModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.StockGroupModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockGroupModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((StockGroupModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$GroupId = ((StockGroupModelRealmProxyInterface)object).realmGet$GroupId();
                if (realmGet$GroupId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, realmGet$GroupId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GroupIdIndex, rowIndex, false);
                }
                String realmGet$CustomerId = ((StockGroupModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QuantityIndex, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.Quantity2Index, rowIndex, ((StockGroupModelRealmProxyInterface)object).realmGet$Quantity2(), false);
                String realmGet$CommonID = ((StockGroupModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.StockGroupModel createDetachedCopy(com.bhn.sadix.Data.StockGroupModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.StockGroupModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.StockGroupModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.StockGroupModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.StockGroupModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$GroupId(((StockGroupModelRealmProxyInterface) realmObject).realmGet$GroupId());
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$CustomerId(((StockGroupModelRealmProxyInterface) realmObject).realmGet$CustomerId());
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$Quantity(((StockGroupModelRealmProxyInterface) realmObject).realmGet$Quantity());
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$Quantity2(((StockGroupModelRealmProxyInterface) realmObject).realmGet$Quantity2());
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((StockGroupModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((StockGroupModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((StockGroupModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.StockGroupModel update(Realm realm, com.bhn.sadix.Data.StockGroupModel realmObject, com.bhn.sadix.Data.StockGroupModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((StockGroupModelRealmProxyInterface) realmObject).realmSet$GroupId(((StockGroupModelRealmProxyInterface) newObject).realmGet$GroupId());
        ((StockGroupModelRealmProxyInterface) realmObject).realmSet$CustomerId(((StockGroupModelRealmProxyInterface) newObject).realmGet$CustomerId());
        ((StockGroupModelRealmProxyInterface) realmObject).realmSet$Quantity(((StockGroupModelRealmProxyInterface) newObject).realmGet$Quantity());
        ((StockGroupModelRealmProxyInterface) realmObject).realmSet$Quantity2(((StockGroupModelRealmProxyInterface) newObject).realmGet$Quantity2());
        ((StockGroupModelRealmProxyInterface) realmObject).realmSet$CommonID(((StockGroupModelRealmProxyInterface) newObject).realmGet$CommonID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("StockGroupModel = [");
        stringBuilder.append("{GroupId:");
        stringBuilder.append(realmGet$GroupId() != null ? realmGet$GroupId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerId:");
        stringBuilder.append(realmGet$CustomerId() != null ? realmGet$CustomerId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Quantity:");
        stringBuilder.append(realmGet$Quantity());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Quantity2:");
        stringBuilder.append(realmGet$Quantity2());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockGroupModelRealmProxy aStockGroupModel = (StockGroupModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aStockGroupModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aStockGroupModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aStockGroupModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
