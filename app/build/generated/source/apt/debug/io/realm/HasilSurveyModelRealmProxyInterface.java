package io.realm;


public interface HasilSurveyModelRealmProxyInterface {
    public String realmGet$FormId();
    public void realmSet$FormId(String value);
    public String realmGet$CommonId();
    public void realmSet$CommonId(String value);
    public String realmGet$ListID();
    public void realmSet$ListID(String value);
    public String realmGet$ListValue();
    public void realmSet$ListValue(String value);
    public String realmGet$SurveyId();
    public void realmSet$SurveyId(String value);
}
