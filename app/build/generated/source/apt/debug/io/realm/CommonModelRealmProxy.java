package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CommonModelRealmProxy extends com.bhn.sadix.Data.CommonModel
    implements RealmObjectProxy, CommonModelRealmProxyInterface {

    static final class CommonModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long SCIndex;
        public long CustomerIdIndex;
        public long BeginVisitTimeIndex;
        public long EndVisitTimeIndex;
        public long LogTimeIndex;
        public long SalesIdIndex;
        public long S_AMOUNTIndex;
        public long DescriptionIndex;
        public long GeoLatIndex;
        public long GeoLongIndex;
        public long QTY_BIndex;
        public long QTY_KIndex;
        public long PRICEIndex;
        public long DISCOUNTIndex;
        public long VisitIndex;
        public long RVisitIDIndex;
        public long InvNOIndex;
        public long StatusIndex;
        public long CommonIDIndex;
        public long DIndex;
        public long ROrderIDIndex;
        public long StatusIDIndex;
        public long CustomerRIDIndex;
        public long DoneIndex;
        public long takingOrderModelIndex;
        public long returnModelIndex;
        public long priceMonitoringModelIndex;
        public long collectionModelIndex;
        public long stockCustomerModelIndex;
        public long stockCompetitorModelIndex;
        public long assetModelIndex;
        public long assetMutasiModelIndex;

        CommonModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(32);
            this.SCIndex = getValidColumnIndex(path, table, "CommonModel", "SC");
            indicesMap.put("SC", this.SCIndex);
            this.CustomerIdIndex = getValidColumnIndex(path, table, "CommonModel", "CustomerId");
            indicesMap.put("CustomerId", this.CustomerIdIndex);
            this.BeginVisitTimeIndex = getValidColumnIndex(path, table, "CommonModel", "BeginVisitTime");
            indicesMap.put("BeginVisitTime", this.BeginVisitTimeIndex);
            this.EndVisitTimeIndex = getValidColumnIndex(path, table, "CommonModel", "EndVisitTime");
            indicesMap.put("EndVisitTime", this.EndVisitTimeIndex);
            this.LogTimeIndex = getValidColumnIndex(path, table, "CommonModel", "LogTime");
            indicesMap.put("LogTime", this.LogTimeIndex);
            this.SalesIdIndex = getValidColumnIndex(path, table, "CommonModel", "SalesId");
            indicesMap.put("SalesId", this.SalesIdIndex);
            this.S_AMOUNTIndex = getValidColumnIndex(path, table, "CommonModel", "S_AMOUNT");
            indicesMap.put("S_AMOUNT", this.S_AMOUNTIndex);
            this.DescriptionIndex = getValidColumnIndex(path, table, "CommonModel", "Description");
            indicesMap.put("Description", this.DescriptionIndex);
            this.GeoLatIndex = getValidColumnIndex(path, table, "CommonModel", "GeoLat");
            indicesMap.put("GeoLat", this.GeoLatIndex);
            this.GeoLongIndex = getValidColumnIndex(path, table, "CommonModel", "GeoLong");
            indicesMap.put("GeoLong", this.GeoLongIndex);
            this.QTY_BIndex = getValidColumnIndex(path, table, "CommonModel", "QTY_B");
            indicesMap.put("QTY_B", this.QTY_BIndex);
            this.QTY_KIndex = getValidColumnIndex(path, table, "CommonModel", "QTY_K");
            indicesMap.put("QTY_K", this.QTY_KIndex);
            this.PRICEIndex = getValidColumnIndex(path, table, "CommonModel", "PRICE");
            indicesMap.put("PRICE", this.PRICEIndex);
            this.DISCOUNTIndex = getValidColumnIndex(path, table, "CommonModel", "DISCOUNT");
            indicesMap.put("DISCOUNT", this.DISCOUNTIndex);
            this.VisitIndex = getValidColumnIndex(path, table, "CommonModel", "Visit");
            indicesMap.put("Visit", this.VisitIndex);
            this.RVisitIDIndex = getValidColumnIndex(path, table, "CommonModel", "RVisitID");
            indicesMap.put("RVisitID", this.RVisitIDIndex);
            this.InvNOIndex = getValidColumnIndex(path, table, "CommonModel", "InvNO");
            indicesMap.put("InvNO", this.InvNOIndex);
            this.StatusIndex = getValidColumnIndex(path, table, "CommonModel", "Status");
            indicesMap.put("Status", this.StatusIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "CommonModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.DIndex = getValidColumnIndex(path, table, "CommonModel", "D");
            indicesMap.put("D", this.DIndex);
            this.ROrderIDIndex = getValidColumnIndex(path, table, "CommonModel", "ROrderID");
            indicesMap.put("ROrderID", this.ROrderIDIndex);
            this.StatusIDIndex = getValidColumnIndex(path, table, "CommonModel", "StatusID");
            indicesMap.put("StatusID", this.StatusIDIndex);
            this.CustomerRIDIndex = getValidColumnIndex(path, table, "CommonModel", "CustomerRID");
            indicesMap.put("CustomerRID", this.CustomerRIDIndex);
            this.DoneIndex = getValidColumnIndex(path, table, "CommonModel", "Done");
            indicesMap.put("Done", this.DoneIndex);
            this.takingOrderModelIndex = getValidColumnIndex(path, table, "CommonModel", "takingOrderModel");
            indicesMap.put("takingOrderModel", this.takingOrderModelIndex);
            this.returnModelIndex = getValidColumnIndex(path, table, "CommonModel", "returnModel");
            indicesMap.put("returnModel", this.returnModelIndex);
            this.priceMonitoringModelIndex = getValidColumnIndex(path, table, "CommonModel", "priceMonitoringModel");
            indicesMap.put("priceMonitoringModel", this.priceMonitoringModelIndex);
            this.collectionModelIndex = getValidColumnIndex(path, table, "CommonModel", "collectionModel");
            indicesMap.put("collectionModel", this.collectionModelIndex);
            this.stockCustomerModelIndex = getValidColumnIndex(path, table, "CommonModel", "stockCustomerModel");
            indicesMap.put("stockCustomerModel", this.stockCustomerModelIndex);
            this.stockCompetitorModelIndex = getValidColumnIndex(path, table, "CommonModel", "stockCompetitorModel");
            indicesMap.put("stockCompetitorModel", this.stockCompetitorModelIndex);
            this.assetModelIndex = getValidColumnIndex(path, table, "CommonModel", "assetModel");
            indicesMap.put("assetModel", this.assetModelIndex);
            this.assetMutasiModelIndex = getValidColumnIndex(path, table, "CommonModel", "assetMutasiModel");
            indicesMap.put("assetMutasiModel", this.assetMutasiModelIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final CommonModelColumnInfo otherInfo = (CommonModelColumnInfo) other;
            this.SCIndex = otherInfo.SCIndex;
            this.CustomerIdIndex = otherInfo.CustomerIdIndex;
            this.BeginVisitTimeIndex = otherInfo.BeginVisitTimeIndex;
            this.EndVisitTimeIndex = otherInfo.EndVisitTimeIndex;
            this.LogTimeIndex = otherInfo.LogTimeIndex;
            this.SalesIdIndex = otherInfo.SalesIdIndex;
            this.S_AMOUNTIndex = otherInfo.S_AMOUNTIndex;
            this.DescriptionIndex = otherInfo.DescriptionIndex;
            this.GeoLatIndex = otherInfo.GeoLatIndex;
            this.GeoLongIndex = otherInfo.GeoLongIndex;
            this.QTY_BIndex = otherInfo.QTY_BIndex;
            this.QTY_KIndex = otherInfo.QTY_KIndex;
            this.PRICEIndex = otherInfo.PRICEIndex;
            this.DISCOUNTIndex = otherInfo.DISCOUNTIndex;
            this.VisitIndex = otherInfo.VisitIndex;
            this.RVisitIDIndex = otherInfo.RVisitIDIndex;
            this.InvNOIndex = otherInfo.InvNOIndex;
            this.StatusIndex = otherInfo.StatusIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.DIndex = otherInfo.DIndex;
            this.ROrderIDIndex = otherInfo.ROrderIDIndex;
            this.StatusIDIndex = otherInfo.StatusIDIndex;
            this.CustomerRIDIndex = otherInfo.CustomerRIDIndex;
            this.DoneIndex = otherInfo.DoneIndex;
            this.takingOrderModelIndex = otherInfo.takingOrderModelIndex;
            this.returnModelIndex = otherInfo.returnModelIndex;
            this.priceMonitoringModelIndex = otherInfo.priceMonitoringModelIndex;
            this.collectionModelIndex = otherInfo.collectionModelIndex;
            this.stockCustomerModelIndex = otherInfo.stockCustomerModelIndex;
            this.stockCompetitorModelIndex = otherInfo.stockCompetitorModelIndex;
            this.assetModelIndex = otherInfo.assetModelIndex;
            this.assetMutasiModelIndex = otherInfo.assetMutasiModelIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final CommonModelColumnInfo clone() {
            return (CommonModelColumnInfo) super.clone();
        }

    }
    private CommonModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.CommonModel> proxyState;
    private RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelRealmList;
    private RealmList<com.bhn.sadix.Data.ReturnModel> returnModelRealmList;
    private RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelRealmList;
    private RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelRealmList;
    private RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelRealmList;
    private RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelRealmList;
    private RealmList<com.bhn.sadix.Data.AssetModel> assetModelRealmList;
    private RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelRealmList;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("SC");
        fieldNames.add("CustomerId");
        fieldNames.add("BeginVisitTime");
        fieldNames.add("EndVisitTime");
        fieldNames.add("LogTime");
        fieldNames.add("SalesId");
        fieldNames.add("S_AMOUNT");
        fieldNames.add("Description");
        fieldNames.add("GeoLat");
        fieldNames.add("GeoLong");
        fieldNames.add("QTY_B");
        fieldNames.add("QTY_K");
        fieldNames.add("PRICE");
        fieldNames.add("DISCOUNT");
        fieldNames.add("Visit");
        fieldNames.add("RVisitID");
        fieldNames.add("InvNO");
        fieldNames.add("Status");
        fieldNames.add("CommonID");
        fieldNames.add("D");
        fieldNames.add("ROrderID");
        fieldNames.add("StatusID");
        fieldNames.add("CustomerRID");
        fieldNames.add("Done");
        fieldNames.add("takingOrderModel");
        fieldNames.add("returnModel");
        fieldNames.add("priceMonitoringModel");
        fieldNames.add("collectionModel");
        fieldNames.add("stockCustomerModel");
        fieldNames.add("stockCompetitorModel");
        fieldNames.add("assetModel");
        fieldNames.add("assetMutasiModel");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    CommonModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CommonModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.CommonModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$SC() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SCIndex);
    }

    public void realmSet$SC(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SCIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SCIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SCIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SCIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CustomerId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustomerIdIndex);
    }

    public void realmSet$CustomerId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustomerIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustomerIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustomerIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustomerIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$BeginVisitTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.BeginVisitTimeIndex);
    }

    public void realmSet$BeginVisitTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.BeginVisitTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.BeginVisitTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.BeginVisitTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.BeginVisitTimeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$EndVisitTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.EndVisitTimeIndex);
    }

    public void realmSet$EndVisitTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.EndVisitTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.EndVisitTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.EndVisitTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.EndVisitTimeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$LogTime() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.LogTimeIndex);
    }

    public void realmSet$LogTime(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.LogTimeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.LogTimeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.LogTimeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.LogTimeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SalesId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SalesIdIndex);
    }

    public void realmSet$SalesId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SalesIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SalesIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SalesIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SalesIdIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$S_AMOUNT() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.S_AMOUNTIndex);
    }

    public void realmSet$S_AMOUNT(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.S_AMOUNTIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.S_AMOUNTIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Description() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.DescriptionIndex);
    }

    public void realmSet$Description(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.DescriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.DescriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.DescriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.DescriptionIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLat() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLatIndex);
    }

    public void realmSet$GeoLat(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLatIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLatIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLatIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLatIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLong() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLongIndex);
    }

    public void realmSet$GeoLong(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLongIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLongIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLongIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLongIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_B() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_BIndex);
    }

    public void realmSet$QTY_B(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_BIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_BIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_K() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_KIndex);
    }

    public void realmSet$QTY_K(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_KIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_KIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$PRICE() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.PRICEIndex);
    }

    public void realmSet$PRICE(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.PRICEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.PRICEIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$DISCOUNT() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.DISCOUNTIndex);
    }

    public void realmSet$DISCOUNT(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.DISCOUNTIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.DISCOUNTIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Visit() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.VisitIndex);
    }

    public void realmSet$Visit(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.VisitIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.VisitIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$RVisitID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.RVisitIDIndex);
    }

    public void realmSet$RVisitID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.RVisitIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.RVisitIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$InvNO() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.InvNOIndex);
    }

    public void realmSet$InvNO(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.InvNOIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.InvNOIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.InvNOIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.InvNOIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Status() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.StatusIndex);
    }

    public void realmSet$Status(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.StatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.StatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.StatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.StatusIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'CommonID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public String realmGet$D() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.DIndex);
    }

    public void realmSet$D(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.DIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.DIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.DIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.DIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ROrderID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ROrderIDIndex);
    }

    public void realmSet$ROrderID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ROrderIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ROrderIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ROrderIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ROrderIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$StatusID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.StatusIDIndex);
    }

    public void realmSet$StatusID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.StatusIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.StatusIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CustomerRID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustomerRIDIndex);
    }

    public void realmSet$CustomerRID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustomerRIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustomerRIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustomerRIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustomerRIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Done() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.DoneIndex);
    }

    public void realmSet$Done(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.DoneIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.DoneIndex, value);
    }

    public RealmList<com.bhn.sadix.Data.TakingOrderModel> realmGet$takingOrderModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (takingOrderModelRealmList != null) {
            return takingOrderModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.takingOrderModelIndex);
            takingOrderModelRealmList = new RealmList<com.bhn.sadix.Data.TakingOrderModel>(com.bhn.sadix.Data.TakingOrderModel.class, linkView, proxyState.getRealm$realm());
            return takingOrderModelRealmList;
        }
    }

    public void realmSet$takingOrderModel(RealmList<com.bhn.sadix.Data.TakingOrderModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("takingOrderModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.TakingOrderModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.TakingOrderModel>();
                for (com.bhn.sadix.Data.TakingOrderModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.takingOrderModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.ReturnModel> realmGet$returnModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (returnModelRealmList != null) {
            return returnModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.returnModelIndex);
            returnModelRealmList = new RealmList<com.bhn.sadix.Data.ReturnModel>(com.bhn.sadix.Data.ReturnModel.class, linkView, proxyState.getRealm$realm());
            return returnModelRealmList;
        }
    }

    public void realmSet$returnModel(RealmList<com.bhn.sadix.Data.ReturnModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("returnModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.ReturnModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.ReturnModel>();
                for (com.bhn.sadix.Data.ReturnModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.returnModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.PriceMonitoringModel> realmGet$priceMonitoringModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (priceMonitoringModelRealmList != null) {
            return priceMonitoringModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.priceMonitoringModelIndex);
            priceMonitoringModelRealmList = new RealmList<com.bhn.sadix.Data.PriceMonitoringModel>(com.bhn.sadix.Data.PriceMonitoringModel.class, linkView, proxyState.getRealm$realm());
            return priceMonitoringModelRealmList;
        }
    }

    public void realmSet$priceMonitoringModel(RealmList<com.bhn.sadix.Data.PriceMonitoringModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("priceMonitoringModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.PriceMonitoringModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.PriceMonitoringModel>();
                for (com.bhn.sadix.Data.PriceMonitoringModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.priceMonitoringModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.CollectionModel> realmGet$collectionModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (collectionModelRealmList != null) {
            return collectionModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.collectionModelIndex);
            collectionModelRealmList = new RealmList<com.bhn.sadix.Data.CollectionModel>(com.bhn.sadix.Data.CollectionModel.class, linkView, proxyState.getRealm$realm());
            return collectionModelRealmList;
        }
    }

    public void realmSet$collectionModel(RealmList<com.bhn.sadix.Data.CollectionModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("collectionModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.CollectionModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.CollectionModel>();
                for (com.bhn.sadix.Data.CollectionModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.collectionModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.StockCustomerModel> realmGet$stockCustomerModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (stockCustomerModelRealmList != null) {
            return stockCustomerModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.stockCustomerModelIndex);
            stockCustomerModelRealmList = new RealmList<com.bhn.sadix.Data.StockCustomerModel>(com.bhn.sadix.Data.StockCustomerModel.class, linkView, proxyState.getRealm$realm());
            return stockCustomerModelRealmList;
        }
    }

    public void realmSet$stockCustomerModel(RealmList<com.bhn.sadix.Data.StockCustomerModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("stockCustomerModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.StockCustomerModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.StockCustomerModel>();
                for (com.bhn.sadix.Data.StockCustomerModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.stockCustomerModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.StockCompetitorModel> realmGet$stockCompetitorModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (stockCompetitorModelRealmList != null) {
            return stockCompetitorModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.stockCompetitorModelIndex);
            stockCompetitorModelRealmList = new RealmList<com.bhn.sadix.Data.StockCompetitorModel>(com.bhn.sadix.Data.StockCompetitorModel.class, linkView, proxyState.getRealm$realm());
            return stockCompetitorModelRealmList;
        }
    }

    public void realmSet$stockCompetitorModel(RealmList<com.bhn.sadix.Data.StockCompetitorModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("stockCompetitorModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.StockCompetitorModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.StockCompetitorModel>();
                for (com.bhn.sadix.Data.StockCompetitorModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.stockCompetitorModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.AssetModel> realmGet$assetModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (assetModelRealmList != null) {
            return assetModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.assetModelIndex);
            assetModelRealmList = new RealmList<com.bhn.sadix.Data.AssetModel>(com.bhn.sadix.Data.AssetModel.class, linkView, proxyState.getRealm$realm());
            return assetModelRealmList;
        }
    }

    public void realmSet$assetModel(RealmList<com.bhn.sadix.Data.AssetModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("assetModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.AssetModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.AssetModel>();
                for (com.bhn.sadix.Data.AssetModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.assetModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.AssetMutasiModel> realmGet$assetMutasiModel() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (assetMutasiModelRealmList != null) {
            return assetMutasiModelRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.assetMutasiModelIndex);
            assetMutasiModelRealmList = new RealmList<com.bhn.sadix.Data.AssetMutasiModel>(com.bhn.sadix.Data.AssetMutasiModel.class, linkView, proxyState.getRealm$realm());
            return assetMutasiModelRealmList;
        }
    }

    public void realmSet$assetMutasiModel(RealmList<com.bhn.sadix.Data.AssetMutasiModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("assetMutasiModel")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.AssetMutasiModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.AssetMutasiModel>();
                for (com.bhn.sadix.Data.AssetMutasiModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.assetMutasiModelIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("CommonModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("CommonModel");
            realmObjectSchema.add(new Property("SC", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerId", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("BeginVisitTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("EndVisitTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("LogTime", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SalesId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("S_AMOUNT", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Description", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLat", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLong", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_B", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_K", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("PRICE", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DISCOUNT", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Visit", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("RVisitID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("InvNO", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Status", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("D", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ROrderID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("StatusID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerRID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Done", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            if (!realmSchema.contains("TakingOrderModel")) {
                TakingOrderModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("takingOrderModel", RealmFieldType.LIST, realmSchema.get("TakingOrderModel")));
            if (!realmSchema.contains("ReturnModel")) {
                ReturnModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("returnModel", RealmFieldType.LIST, realmSchema.get("ReturnModel")));
            if (!realmSchema.contains("PriceMonitoringModel")) {
                PriceMonitoringModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("priceMonitoringModel", RealmFieldType.LIST, realmSchema.get("PriceMonitoringModel")));
            if (!realmSchema.contains("CollectionModel")) {
                CollectionModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("collectionModel", RealmFieldType.LIST, realmSchema.get("CollectionModel")));
            if (!realmSchema.contains("StockCustomerModel")) {
                StockCustomerModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("stockCustomerModel", RealmFieldType.LIST, realmSchema.get("StockCustomerModel")));
            if (!realmSchema.contains("StockCompetitorModel")) {
                StockCompetitorModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("stockCompetitorModel", RealmFieldType.LIST, realmSchema.get("StockCompetitorModel")));
            if (!realmSchema.contains("AssetModel")) {
                AssetModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("assetModel", RealmFieldType.LIST, realmSchema.get("AssetModel")));
            if (!realmSchema.contains("AssetMutasiModel")) {
                AssetMutasiModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("assetMutasiModel", RealmFieldType.LIST, realmSchema.get("AssetMutasiModel")));
            return realmObjectSchema;
        }
        return realmSchema.get("CommonModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_CommonModel")) {
            Table table = sharedRealm.getTable("class_CommonModel");
            table.addColumn(RealmFieldType.STRING, "SC", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CustomerId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "BeginVisitTime", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "EndVisitTime", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "LogTime", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SalesId", Table.NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "S_AMOUNT", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Description", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLat", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLong", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_B", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_K", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "PRICE", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "DISCOUNT", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Visit", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "RVisitID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "InvNO", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Status", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "D", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ROrderID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "StatusID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CustomerRID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Done", Table.NOT_NULLABLE);
            if (!sharedRealm.hasTable("class_TakingOrderModel")) {
                TakingOrderModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "takingOrderModel", sharedRealm.getTable("class_TakingOrderModel"));
            if (!sharedRealm.hasTable("class_ReturnModel")) {
                ReturnModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "returnModel", sharedRealm.getTable("class_ReturnModel"));
            if (!sharedRealm.hasTable("class_PriceMonitoringModel")) {
                PriceMonitoringModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "priceMonitoringModel", sharedRealm.getTable("class_PriceMonitoringModel"));
            if (!sharedRealm.hasTable("class_CollectionModel")) {
                CollectionModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "collectionModel", sharedRealm.getTable("class_CollectionModel"));
            if (!sharedRealm.hasTable("class_StockCustomerModel")) {
                StockCustomerModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "stockCustomerModel", sharedRealm.getTable("class_StockCustomerModel"));
            if (!sharedRealm.hasTable("class_StockCompetitorModel")) {
                StockCompetitorModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "stockCompetitorModel", sharedRealm.getTable("class_StockCompetitorModel"));
            if (!sharedRealm.hasTable("class_AssetModel")) {
                AssetModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "assetModel", sharedRealm.getTable("class_AssetModel"));
            if (!sharedRealm.hasTable("class_AssetMutasiModel")) {
                AssetMutasiModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "assetMutasiModel", sharedRealm.getTable("class_AssetMutasiModel"));
            table.addSearchIndex(table.getColumnIndex("CustomerId"));
            table.addSearchIndex(table.getColumnIndex("CommonID"));
            table.setPrimaryKey("CommonID");
            return table;
        }
        return sharedRealm.getTable("class_CommonModel");
    }

    public static CommonModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_CommonModel")) {
            Table table = sharedRealm.getTable("class_CommonModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 32) {
                if (columnCount < 32) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 32 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 32 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 32 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final CommonModelColumnInfo columnInfo = new CommonModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'CommonID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.CommonIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field CommonID");
                }
            }

            if (!columnTypes.containsKey("SC")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SC' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SC") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SC' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SCIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SC' is required. Either set @Required to field 'SC' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustomerId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustomerIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerId' is required. Either set @Required to field 'CustomerId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("CustomerId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'CustomerId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("BeginVisitTime")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'BeginVisitTime' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("BeginVisitTime") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'BeginVisitTime' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.BeginVisitTimeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'BeginVisitTime' is required. Either set @Required to field 'BeginVisitTime' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("EndVisitTime")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'EndVisitTime' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("EndVisitTime") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'EndVisitTime' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.EndVisitTimeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'EndVisitTime' is required. Either set @Required to field 'EndVisitTime' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("LogTime")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'LogTime' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("LogTime") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'LogTime' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.LogTimeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'LogTime' is required. Either set @Required to field 'LogTime' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SalesId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SalesId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SalesId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SalesId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SalesIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SalesId' is required. Either set @Required to field 'SalesId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("S_AMOUNT")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'S_AMOUNT' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("S_AMOUNT") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'S_AMOUNT' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.S_AMOUNTIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'S_AMOUNT' does support null values in the existing Realm file. Use corresponding boxed type for field 'S_AMOUNT' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Description")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Description' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Description") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Description' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.DescriptionIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Description' is required. Either set @Required to field 'Description' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLat")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLat") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLat' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLatIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLat' is required. Either set @Required to field 'GeoLat' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLong")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLong' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLong") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLong' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLongIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLong' is required. Either set @Required to field 'GeoLong' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_B")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_B' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_B") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_B' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_BIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_B' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_B' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_K")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_K' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_K") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_K' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_KIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_K' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_K' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PRICE")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PRICE' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PRICE") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'PRICE' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PRICEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PRICE' does support null values in the existing Realm file. Use corresponding boxed type for field 'PRICE' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DISCOUNT")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DISCOUNT' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DISCOUNT") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'DISCOUNT' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DISCOUNTIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DISCOUNT' does support null values in the existing Realm file. Use corresponding boxed type for field 'DISCOUNT' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Visit")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Visit' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Visit") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Visit' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.VisitIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Visit' does support null values in the existing Realm file. Use corresponding boxed type for field 'Visit' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RVisitID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RVisitID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RVisitID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'RVisitID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.RVisitIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'RVisitID' does support null values in the existing Realm file. Use corresponding boxed type for field 'RVisitID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("InvNO")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'InvNO' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("InvNO") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'InvNO' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.InvNOIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'InvNO' is required. Either set @Required to field 'InvNO' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Status")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Status' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Status") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Status' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.StatusIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Status' is required. Either set @Required to field 'Status' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'CommonID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("CommonID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'CommonID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("D")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'D' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("D") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'D' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.DIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'D' is required. Either set @Required to field 'D' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ROrderID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ROrderID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ROrderID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ROrderID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ROrderIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ROrderID' is required. Either set @Required to field 'ROrderID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("StatusID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'StatusID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("StatusID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'StatusID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.StatusIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'StatusID' does support null values in the existing Realm file. Use corresponding boxed type for field 'StatusID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerRID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerRID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerRID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustomerRID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustomerRIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerRID' is required. Either set @Required to field 'CustomerRID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Done")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Done' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Done") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Done' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DoneIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Done' does support null values in the existing Realm file. Use corresponding boxed type for field 'Done' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("takingOrderModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'takingOrderModel'");
            }
            if (columnTypes.get("takingOrderModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'TakingOrderModel' for field 'takingOrderModel'");
            }
            if (!sharedRealm.hasTable("class_TakingOrderModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_TakingOrderModel' for field 'takingOrderModel'");
            }
            Table table_24 = sharedRealm.getTable("class_TakingOrderModel");
            if (!table.getLinkTarget(columnInfo.takingOrderModelIndex).hasSameSchema(table_24)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'takingOrderModel': '" + table.getLinkTarget(columnInfo.takingOrderModelIndex).getName() + "' expected - was '" + table_24.getName() + "'");
            }
            if (!columnTypes.containsKey("returnModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'returnModel'");
            }
            if (columnTypes.get("returnModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'ReturnModel' for field 'returnModel'");
            }
            if (!sharedRealm.hasTable("class_ReturnModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_ReturnModel' for field 'returnModel'");
            }
            Table table_25 = sharedRealm.getTable("class_ReturnModel");
            if (!table.getLinkTarget(columnInfo.returnModelIndex).hasSameSchema(table_25)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'returnModel': '" + table.getLinkTarget(columnInfo.returnModelIndex).getName() + "' expected - was '" + table_25.getName() + "'");
            }
            if (!columnTypes.containsKey("priceMonitoringModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'priceMonitoringModel'");
            }
            if (columnTypes.get("priceMonitoringModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'PriceMonitoringModel' for field 'priceMonitoringModel'");
            }
            if (!sharedRealm.hasTable("class_PriceMonitoringModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_PriceMonitoringModel' for field 'priceMonitoringModel'");
            }
            Table table_26 = sharedRealm.getTable("class_PriceMonitoringModel");
            if (!table.getLinkTarget(columnInfo.priceMonitoringModelIndex).hasSameSchema(table_26)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'priceMonitoringModel': '" + table.getLinkTarget(columnInfo.priceMonitoringModelIndex).getName() + "' expected - was '" + table_26.getName() + "'");
            }
            if (!columnTypes.containsKey("collectionModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'collectionModel'");
            }
            if (columnTypes.get("collectionModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'CollectionModel' for field 'collectionModel'");
            }
            if (!sharedRealm.hasTable("class_CollectionModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_CollectionModel' for field 'collectionModel'");
            }
            Table table_27 = sharedRealm.getTable("class_CollectionModel");
            if (!table.getLinkTarget(columnInfo.collectionModelIndex).hasSameSchema(table_27)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'collectionModel': '" + table.getLinkTarget(columnInfo.collectionModelIndex).getName() + "' expected - was '" + table_27.getName() + "'");
            }
            if (!columnTypes.containsKey("stockCustomerModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'stockCustomerModel'");
            }
            if (columnTypes.get("stockCustomerModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'StockCustomerModel' for field 'stockCustomerModel'");
            }
            if (!sharedRealm.hasTable("class_StockCustomerModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_StockCustomerModel' for field 'stockCustomerModel'");
            }
            Table table_28 = sharedRealm.getTable("class_StockCustomerModel");
            if (!table.getLinkTarget(columnInfo.stockCustomerModelIndex).hasSameSchema(table_28)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'stockCustomerModel': '" + table.getLinkTarget(columnInfo.stockCustomerModelIndex).getName() + "' expected - was '" + table_28.getName() + "'");
            }
            if (!columnTypes.containsKey("stockCompetitorModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'stockCompetitorModel'");
            }
            if (columnTypes.get("stockCompetitorModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'StockCompetitorModel' for field 'stockCompetitorModel'");
            }
            if (!sharedRealm.hasTable("class_StockCompetitorModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_StockCompetitorModel' for field 'stockCompetitorModel'");
            }
            Table table_29 = sharedRealm.getTable("class_StockCompetitorModel");
            if (!table.getLinkTarget(columnInfo.stockCompetitorModelIndex).hasSameSchema(table_29)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'stockCompetitorModel': '" + table.getLinkTarget(columnInfo.stockCompetitorModelIndex).getName() + "' expected - was '" + table_29.getName() + "'");
            }
            if (!columnTypes.containsKey("assetModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'assetModel'");
            }
            if (columnTypes.get("assetModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'AssetModel' for field 'assetModel'");
            }
            if (!sharedRealm.hasTable("class_AssetModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_AssetModel' for field 'assetModel'");
            }
            Table table_30 = sharedRealm.getTable("class_AssetModel");
            if (!table.getLinkTarget(columnInfo.assetModelIndex).hasSameSchema(table_30)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'assetModel': '" + table.getLinkTarget(columnInfo.assetModelIndex).getName() + "' expected - was '" + table_30.getName() + "'");
            }
            if (!columnTypes.containsKey("assetMutasiModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'assetMutasiModel'");
            }
            if (columnTypes.get("assetMutasiModel") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'AssetMutasiModel' for field 'assetMutasiModel'");
            }
            if (!sharedRealm.hasTable("class_AssetMutasiModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_AssetMutasiModel' for field 'assetMutasiModel'");
            }
            Table table_31 = sharedRealm.getTable("class_AssetMutasiModel");
            if (!table.getLinkTarget(columnInfo.assetMutasiModelIndex).hasSameSchema(table_31)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'assetMutasiModel': '" + table.getLinkTarget(columnInfo.assetMutasiModelIndex).getName() + "' expected - was '" + table_31.getName() + "'");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'CommonModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_CommonModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.CommonModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(8);
        com.bhn.sadix.Data.CommonModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("CommonID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("CommonID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.CommonModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("takingOrderModel")) {
                excludeFields.add("takingOrderModel");
            }
            if (json.has("returnModel")) {
                excludeFields.add("returnModel");
            }
            if (json.has("priceMonitoringModel")) {
                excludeFields.add("priceMonitoringModel");
            }
            if (json.has("collectionModel")) {
                excludeFields.add("collectionModel");
            }
            if (json.has("stockCustomerModel")) {
                excludeFields.add("stockCustomerModel");
            }
            if (json.has("stockCompetitorModel")) {
                excludeFields.add("stockCompetitorModel");
            }
            if (json.has("assetModel")) {
                excludeFields.add("assetModel");
            }
            if (json.has("assetMutasiModel")) {
                excludeFields.add("assetMutasiModel");
            }
            if (json.has("CommonID")) {
                if (json.isNull("CommonID")) {
                    obj = (io.realm.CommonModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CommonModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.CommonModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CommonModel.class, json.getString("CommonID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'CommonID'.");
            }
        }
        if (json.has("SC")) {
            if (json.isNull("SC")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$SC(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$SC((String) json.getString("SC"));
            }
        }
        if (json.has("CustomerId")) {
            if (json.isNull("CustomerId")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$CustomerId(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$CustomerId((String) json.getString("CustomerId"));
            }
        }
        if (json.has("BeginVisitTime")) {
            if (json.isNull("BeginVisitTime")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$BeginVisitTime(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$BeginVisitTime((String) json.getString("BeginVisitTime"));
            }
        }
        if (json.has("EndVisitTime")) {
            if (json.isNull("EndVisitTime")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$EndVisitTime(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$EndVisitTime((String) json.getString("EndVisitTime"));
            }
        }
        if (json.has("LogTime")) {
            if (json.isNull("LogTime")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$LogTime(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$LogTime((String) json.getString("LogTime"));
            }
        }
        if (json.has("SalesId")) {
            if (json.isNull("SalesId")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$SalesId(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$SalesId((String) json.getString("SalesId"));
            }
        }
        if (json.has("S_AMOUNT")) {
            if (json.isNull("S_AMOUNT")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'S_AMOUNT' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$S_AMOUNT((double) json.getDouble("S_AMOUNT"));
            }
        }
        if (json.has("Description")) {
            if (json.isNull("Description")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$Description(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$Description((String) json.getString("Description"));
            }
        }
        if (json.has("GeoLat")) {
            if (json.isNull("GeoLat")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$GeoLat(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$GeoLat((String) json.getString("GeoLat"));
            }
        }
        if (json.has("GeoLong")) {
            if (json.isNull("GeoLong")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$GeoLong(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$GeoLong((String) json.getString("GeoLong"));
            }
        }
        if (json.has("QTY_B")) {
            if (json.isNull("QTY_B")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$QTY_B((int) json.getInt("QTY_B"));
            }
        }
        if (json.has("QTY_K")) {
            if (json.isNull("QTY_K")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$QTY_K((int) json.getInt("QTY_K"));
            }
        }
        if (json.has("PRICE")) {
            if (json.isNull("PRICE")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$PRICE((double) json.getDouble("PRICE"));
            }
        }
        if (json.has("DISCOUNT")) {
            if (json.isNull("DISCOUNT")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$DISCOUNT((double) json.getDouble("DISCOUNT"));
            }
        }
        if (json.has("Visit")) {
            if (json.isNull("Visit")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Visit' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$Visit((int) json.getInt("Visit"));
            }
        }
        if (json.has("RVisitID")) {
            if (json.isNull("RVisitID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'RVisitID' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$RVisitID((int) json.getInt("RVisitID"));
            }
        }
        if (json.has("InvNO")) {
            if (json.isNull("InvNO")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$InvNO(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$InvNO((String) json.getString("InvNO"));
            }
        }
        if (json.has("Status")) {
            if (json.isNull("Status")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$Status(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$Status((String) json.getString("Status"));
            }
        }
        if (json.has("D")) {
            if (json.isNull("D")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$D(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$D((String) json.getString("D"));
            }
        }
        if (json.has("ROrderID")) {
            if (json.isNull("ROrderID")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$ROrderID(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$ROrderID((String) json.getString("ROrderID"));
            }
        }
        if (json.has("StatusID")) {
            if (json.isNull("StatusID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'StatusID' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$StatusID((int) json.getInt("StatusID"));
            }
        }
        if (json.has("CustomerRID")) {
            if (json.isNull("CustomerRID")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$CustomerRID(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$CustomerRID((String) json.getString("CustomerRID"));
            }
        }
        if (json.has("Done")) {
            if (json.isNull("Done")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Done' to null.");
            } else {
                ((CommonModelRealmProxyInterface) obj).realmSet$Done((int) json.getInt("Done"));
            }
        }
        if (json.has("takingOrderModel")) {
            if (json.isNull("takingOrderModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$takingOrderModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$takingOrderModel().clear();
                JSONArray array = json.getJSONArray("takingOrderModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$takingOrderModel().add(item);
                }
            }
        }
        if (json.has("returnModel")) {
            if (json.isNull("returnModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$returnModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$returnModel().clear();
                JSONArray array = json.getJSONArray("returnModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.ReturnModel item = ReturnModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$returnModel().add(item);
                }
            }
        }
        if (json.has("priceMonitoringModel")) {
            if (json.isNull("priceMonitoringModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$priceMonitoringModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$priceMonitoringModel().clear();
                JSONArray array = json.getJSONArray("priceMonitoringModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.PriceMonitoringModel item = PriceMonitoringModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$priceMonitoringModel().add(item);
                }
            }
        }
        if (json.has("collectionModel")) {
            if (json.isNull("collectionModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$collectionModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$collectionModel().clear();
                JSONArray array = json.getJSONArray("collectionModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.CollectionModel item = CollectionModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$collectionModel().add(item);
                }
            }
        }
        if (json.has("stockCustomerModel")) {
            if (json.isNull("stockCustomerModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$stockCustomerModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$stockCustomerModel().clear();
                JSONArray array = json.getJSONArray("stockCustomerModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.StockCustomerModel item = StockCustomerModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$stockCustomerModel().add(item);
                }
            }
        }
        if (json.has("stockCompetitorModel")) {
            if (json.isNull("stockCompetitorModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$stockCompetitorModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$stockCompetitorModel().clear();
                JSONArray array = json.getJSONArray("stockCompetitorModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.StockCompetitorModel item = StockCompetitorModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$stockCompetitorModel().add(item);
                }
            }
        }
        if (json.has("assetModel")) {
            if (json.isNull("assetModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$assetModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$assetModel().clear();
                JSONArray array = json.getJSONArray("assetModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.AssetModel item = AssetModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$assetModel().add(item);
                }
            }
        }
        if (json.has("assetMutasiModel")) {
            if (json.isNull("assetMutasiModel")) {
                ((CommonModelRealmProxyInterface) obj).realmSet$assetMutasiModel(null);
            } else {
                ((CommonModelRealmProxyInterface) obj).realmGet$assetMutasiModel().clear();
                JSONArray array = json.getJSONArray("assetMutasiModel");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.AssetMutasiModel item = AssetMutasiModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((CommonModelRealmProxyInterface) obj).realmGet$assetMutasiModel().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.CommonModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.CommonModel obj = new com.bhn.sadix.Data.CommonModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("SC")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$SC(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$SC((String) reader.nextString());
                }
            } else if (name.equals("CustomerId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$CustomerId(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$CustomerId((String) reader.nextString());
                }
            } else if (name.equals("BeginVisitTime")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$BeginVisitTime(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$BeginVisitTime((String) reader.nextString());
                }
            } else if (name.equals("EndVisitTime")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$EndVisitTime(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$EndVisitTime((String) reader.nextString());
                }
            } else if (name.equals("LogTime")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$LogTime(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$LogTime((String) reader.nextString());
                }
            } else if (name.equals("SalesId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$SalesId(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$SalesId((String) reader.nextString());
                }
            } else if (name.equals("S_AMOUNT")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'S_AMOUNT' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$S_AMOUNT((double) reader.nextDouble());
                }
            } else if (name.equals("Description")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$Description(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$Description((String) reader.nextString());
                }
            } else if (name.equals("GeoLat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$GeoLat(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$GeoLat((String) reader.nextString());
                }
            } else if (name.equals("GeoLong")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$GeoLong(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$GeoLong((String) reader.nextString());
                }
            } else if (name.equals("QTY_B")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$QTY_B((int) reader.nextInt());
                }
            } else if (name.equals("QTY_K")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$QTY_K((int) reader.nextInt());
                }
            } else if (name.equals("PRICE")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$PRICE((double) reader.nextDouble());
                }
            } else if (name.equals("DISCOUNT")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$DISCOUNT((double) reader.nextDouble());
                }
            } else if (name.equals("Visit")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Visit' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$Visit((int) reader.nextInt());
                }
            } else if (name.equals("RVisitID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'RVisitID' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$RVisitID((int) reader.nextInt());
                }
            } else if (name.equals("InvNO")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$InvNO(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$InvNO((String) reader.nextString());
                }
            } else if (name.equals("Status")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$Status(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$Status((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("D")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$D(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$D((String) reader.nextString());
                }
            } else if (name.equals("ROrderID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$ROrderID(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$ROrderID((String) reader.nextString());
                }
            } else if (name.equals("StatusID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'StatusID' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$StatusID((int) reader.nextInt());
                }
            } else if (name.equals("CustomerRID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$CustomerRID(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$CustomerRID((String) reader.nextString());
                }
            } else if (name.equals("Done")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Done' to null.");
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$Done((int) reader.nextInt());
                }
            } else if (name.equals("takingOrderModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$takingOrderModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$takingOrderModel(new RealmList<com.bhn.sadix.Data.TakingOrderModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$takingOrderModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("returnModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$returnModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$returnModel(new RealmList<com.bhn.sadix.Data.ReturnModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.ReturnModel item = ReturnModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$returnModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("priceMonitoringModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$priceMonitoringModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$priceMonitoringModel(new RealmList<com.bhn.sadix.Data.PriceMonitoringModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.PriceMonitoringModel item = PriceMonitoringModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$priceMonitoringModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("collectionModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$collectionModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$collectionModel(new RealmList<com.bhn.sadix.Data.CollectionModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.CollectionModel item = CollectionModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$collectionModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("stockCustomerModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$stockCustomerModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$stockCustomerModel(new RealmList<com.bhn.sadix.Data.StockCustomerModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.StockCustomerModel item = StockCustomerModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$stockCustomerModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("stockCompetitorModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$stockCompetitorModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$stockCompetitorModel(new RealmList<com.bhn.sadix.Data.StockCompetitorModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.StockCompetitorModel item = StockCompetitorModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$stockCompetitorModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("assetModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$assetModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$assetModel(new RealmList<com.bhn.sadix.Data.AssetModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.AssetModel item = AssetModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$assetModel().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("assetMutasiModel")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CommonModelRealmProxyInterface) obj).realmSet$assetMutasiModel(null);
                } else {
                    ((CommonModelRealmProxyInterface) obj).realmSet$assetMutasiModel(new RealmList<com.bhn.sadix.Data.AssetMutasiModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.AssetMutasiModel item = AssetMutasiModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((CommonModelRealmProxyInterface) obj).realmGet$assetMutasiModel().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'CommonID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.CommonModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.CommonModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CommonModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.CommonModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((CommonModelRealmProxyInterface) object).realmGet$CommonID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.CommonModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.CommonModel copy(Realm realm, com.bhn.sadix.Data.CommonModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CommonModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.CommonModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.CommonModel.class, ((CommonModelRealmProxyInterface) newObject).realmGet$CommonID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((CommonModelRealmProxyInterface) realmObject).realmSet$SC(((CommonModelRealmProxyInterface) newObject).realmGet$SC());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$CustomerId(((CommonModelRealmProxyInterface) newObject).realmGet$CustomerId());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$BeginVisitTime(((CommonModelRealmProxyInterface) newObject).realmGet$BeginVisitTime());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$EndVisitTime(((CommonModelRealmProxyInterface) newObject).realmGet$EndVisitTime());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$LogTime(((CommonModelRealmProxyInterface) newObject).realmGet$LogTime());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$SalesId(((CommonModelRealmProxyInterface) newObject).realmGet$SalesId());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$S_AMOUNT(((CommonModelRealmProxyInterface) newObject).realmGet$S_AMOUNT());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$Description(((CommonModelRealmProxyInterface) newObject).realmGet$Description());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$GeoLat(((CommonModelRealmProxyInterface) newObject).realmGet$GeoLat());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$GeoLong(((CommonModelRealmProxyInterface) newObject).realmGet$GeoLong());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$QTY_B(((CommonModelRealmProxyInterface) newObject).realmGet$QTY_B());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$QTY_K(((CommonModelRealmProxyInterface) newObject).realmGet$QTY_K());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$PRICE(((CommonModelRealmProxyInterface) newObject).realmGet$PRICE());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$DISCOUNT(((CommonModelRealmProxyInterface) newObject).realmGet$DISCOUNT());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$Visit(((CommonModelRealmProxyInterface) newObject).realmGet$Visit());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$RVisitID(((CommonModelRealmProxyInterface) newObject).realmGet$RVisitID());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$InvNO(((CommonModelRealmProxyInterface) newObject).realmGet$InvNO());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$Status(((CommonModelRealmProxyInterface) newObject).realmGet$Status());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$D(((CommonModelRealmProxyInterface) newObject).realmGet$D());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$ROrderID(((CommonModelRealmProxyInterface) newObject).realmGet$ROrderID());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$StatusID(((CommonModelRealmProxyInterface) newObject).realmGet$StatusID());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$CustomerRID(((CommonModelRealmProxyInterface) newObject).realmGet$CustomerRID());
            ((CommonModelRealmProxyInterface) realmObject).realmSet$Done(((CommonModelRealmProxyInterface) newObject).realmGet$Done());

            RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$takingOrderModel();
            if (takingOrderModelList != null) {
                RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$takingOrderModel();
                for (int i = 0; i < takingOrderModelList.size(); i++) {
                    com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem = takingOrderModelList.get(i);
                    com.bhn.sadix.Data.TakingOrderModel cachetakingOrderModel = (com.bhn.sadix.Data.TakingOrderModel) cache.get(takingOrderModelItem);
                    if (cachetakingOrderModel != null) {
                        takingOrderModelRealmList.add(cachetakingOrderModel);
                    } else {
                        takingOrderModelRealmList.add(TakingOrderModelRealmProxy.copyOrUpdate(realm, takingOrderModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$returnModel();
            if (returnModelList != null) {
                RealmList<com.bhn.sadix.Data.ReturnModel> returnModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$returnModel();
                for (int i = 0; i < returnModelList.size(); i++) {
                    com.bhn.sadix.Data.ReturnModel returnModelItem = returnModelList.get(i);
                    com.bhn.sadix.Data.ReturnModel cachereturnModel = (com.bhn.sadix.Data.ReturnModel) cache.get(returnModelItem);
                    if (cachereturnModel != null) {
                        returnModelRealmList.add(cachereturnModel);
                    } else {
                        returnModelRealmList.add(ReturnModelRealmProxy.copyOrUpdate(realm, returnModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$priceMonitoringModel();
            if (priceMonitoringModelList != null) {
                RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$priceMonitoringModel();
                for (int i = 0; i < priceMonitoringModelList.size(); i++) {
                    com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem = priceMonitoringModelList.get(i);
                    com.bhn.sadix.Data.PriceMonitoringModel cachepriceMonitoringModel = (com.bhn.sadix.Data.PriceMonitoringModel) cache.get(priceMonitoringModelItem);
                    if (cachepriceMonitoringModel != null) {
                        priceMonitoringModelRealmList.add(cachepriceMonitoringModel);
                    } else {
                        priceMonitoringModelRealmList.add(PriceMonitoringModelRealmProxy.copyOrUpdate(realm, priceMonitoringModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$collectionModel();
            if (collectionModelList != null) {
                RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$collectionModel();
                for (int i = 0; i < collectionModelList.size(); i++) {
                    com.bhn.sadix.Data.CollectionModel collectionModelItem = collectionModelList.get(i);
                    com.bhn.sadix.Data.CollectionModel cachecollectionModel = (com.bhn.sadix.Data.CollectionModel) cache.get(collectionModelItem);
                    if (cachecollectionModel != null) {
                        collectionModelRealmList.add(cachecollectionModel);
                    } else {
                        collectionModelRealmList.add(CollectionModelRealmProxy.copyOrUpdate(realm, collectionModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$stockCustomerModel();
            if (stockCustomerModelList != null) {
                RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCustomerModel();
                for (int i = 0; i < stockCustomerModelList.size(); i++) {
                    com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem = stockCustomerModelList.get(i);
                    com.bhn.sadix.Data.StockCustomerModel cachestockCustomerModel = (com.bhn.sadix.Data.StockCustomerModel) cache.get(stockCustomerModelItem);
                    if (cachestockCustomerModel != null) {
                        stockCustomerModelRealmList.add(cachestockCustomerModel);
                    } else {
                        stockCustomerModelRealmList.add(StockCustomerModelRealmProxy.copyOrUpdate(realm, stockCustomerModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$stockCompetitorModel();
            if (stockCompetitorModelList != null) {
                RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCompetitorModel();
                for (int i = 0; i < stockCompetitorModelList.size(); i++) {
                    com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem = stockCompetitorModelList.get(i);
                    com.bhn.sadix.Data.StockCompetitorModel cachestockCompetitorModel = (com.bhn.sadix.Data.StockCompetitorModel) cache.get(stockCompetitorModelItem);
                    if (cachestockCompetitorModel != null) {
                        stockCompetitorModelRealmList.add(cachestockCompetitorModel);
                    } else {
                        stockCompetitorModelRealmList.add(StockCompetitorModelRealmProxy.copyOrUpdate(realm, stockCompetitorModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$assetModel();
            if (assetModelList != null) {
                RealmList<com.bhn.sadix.Data.AssetModel> assetModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetModel();
                for (int i = 0; i < assetModelList.size(); i++) {
                    com.bhn.sadix.Data.AssetModel assetModelItem = assetModelList.get(i);
                    com.bhn.sadix.Data.AssetModel cacheassetModel = (com.bhn.sadix.Data.AssetModel) cache.get(assetModelItem);
                    if (cacheassetModel != null) {
                        assetModelRealmList.add(cacheassetModel);
                    } else {
                        assetModelRealmList.add(AssetModelRealmProxy.copyOrUpdate(realm, assetModelList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$assetMutasiModel();
            if (assetMutasiModelList != null) {
                RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetMutasiModel();
                for (int i = 0; i < assetMutasiModelList.size(); i++) {
                    com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem = assetMutasiModelList.get(i);
                    com.bhn.sadix.Data.AssetMutasiModel cacheassetMutasiModel = (com.bhn.sadix.Data.AssetMutasiModel) cache.get(assetMutasiModelItem);
                    if (cacheassetMutasiModel != null) {
                        assetMutasiModelRealmList.add(cacheassetMutasiModel);
                    } else {
                        assetMutasiModelRealmList.add(AssetMutasiModelRealmProxy.copyOrUpdate(realm, assetMutasiModelList.get(i), update, cache));
                    }
                }
            }

            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.CommonModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonModelColumnInfo columnInfo = (CommonModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CommonModelRealmProxyInterface) object).realmGet$CommonID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$SC = ((CommonModelRealmProxyInterface)object).realmGet$SC();
        if (realmGet$SC != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SCIndex, rowIndex, realmGet$SC, false);
        }
        String realmGet$CustomerId = ((CommonModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        }
        String realmGet$BeginVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$BeginVisitTime();
        if (realmGet$BeginVisitTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, realmGet$BeginVisitTime, false);
        }
        String realmGet$EndVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$EndVisitTime();
        if (realmGet$EndVisitTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, realmGet$EndVisitTime, false);
        }
        String realmGet$LogTime = ((CommonModelRealmProxyInterface)object).realmGet$LogTime();
        if (realmGet$LogTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, realmGet$LogTime, false);
        }
        String realmGet$SalesId = ((CommonModelRealmProxyInterface)object).realmGet$SalesId();
        if (realmGet$SalesId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
        }
        Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
        String realmGet$Description = ((CommonModelRealmProxyInterface)object).realmGet$Description();
        if (realmGet$Description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
        }
        String realmGet$GeoLat = ((CommonModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        }
        String realmGet$GeoLong = ((CommonModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICEIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$PRICE(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$DISCOUNT(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.VisitIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Visit(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.RVisitIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$RVisitID(), false);
        String realmGet$InvNO = ((CommonModelRealmProxyInterface)object).realmGet$InvNO();
        if (realmGet$InvNO != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.InvNOIndex, rowIndex, realmGet$InvNO, false);
        }
        String realmGet$Status = ((CommonModelRealmProxyInterface)object).realmGet$Status();
        if (realmGet$Status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.StatusIndex, rowIndex, realmGet$Status, false);
        }
        String realmGet$D = ((CommonModelRealmProxyInterface)object).realmGet$D();
        if (realmGet$D != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DIndex, rowIndex, realmGet$D, false);
        }
        String realmGet$ROrderID = ((CommonModelRealmProxyInterface)object).realmGet$ROrderID();
        if (realmGet$ROrderID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, realmGet$ROrderID, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.StatusIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$StatusID(), false);
        String realmGet$CustomerRID = ((CommonModelRealmProxyInterface)object).realmGet$CustomerRID();
        if (realmGet$CustomerRID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, realmGet$CustomerRID, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Done(), false);

        RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) object).realmGet$takingOrderModel();
        if (takingOrderModelList != null) {
            long takingOrderModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.takingOrderModelIndex, rowIndex);
            for (com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem : takingOrderModelList) {
                Long cacheItemIndextakingOrderModel = cache.get(takingOrderModelItem);
                if (cacheItemIndextakingOrderModel == null) {
                    cacheItemIndextakingOrderModel = TakingOrderModelRealmProxy.insert(realm, takingOrderModelItem, cache);
                }
                LinkView.nativeAdd(takingOrderModelNativeLinkViewPtr, cacheItemIndextakingOrderModel);
            }
        }


        RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) object).realmGet$returnModel();
        if (returnModelList != null) {
            long returnModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.returnModelIndex, rowIndex);
            for (com.bhn.sadix.Data.ReturnModel returnModelItem : returnModelList) {
                Long cacheItemIndexreturnModel = cache.get(returnModelItem);
                if (cacheItemIndexreturnModel == null) {
                    cacheItemIndexreturnModel = ReturnModelRealmProxy.insert(realm, returnModelItem, cache);
                }
                LinkView.nativeAdd(returnModelNativeLinkViewPtr, cacheItemIndexreturnModel);
            }
        }


        RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) object).realmGet$priceMonitoringModel();
        if (priceMonitoringModelList != null) {
            long priceMonitoringModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.priceMonitoringModelIndex, rowIndex);
            for (com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem : priceMonitoringModelList) {
                Long cacheItemIndexpriceMonitoringModel = cache.get(priceMonitoringModelItem);
                if (cacheItemIndexpriceMonitoringModel == null) {
                    cacheItemIndexpriceMonitoringModel = PriceMonitoringModelRealmProxy.insert(realm, priceMonitoringModelItem, cache);
                }
                LinkView.nativeAdd(priceMonitoringModelNativeLinkViewPtr, cacheItemIndexpriceMonitoringModel);
            }
        }


        RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) object).realmGet$collectionModel();
        if (collectionModelList != null) {
            long collectionModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.collectionModelIndex, rowIndex);
            for (com.bhn.sadix.Data.CollectionModel collectionModelItem : collectionModelList) {
                Long cacheItemIndexcollectionModel = cache.get(collectionModelItem);
                if (cacheItemIndexcollectionModel == null) {
                    cacheItemIndexcollectionModel = CollectionModelRealmProxy.insert(realm, collectionModelItem, cache);
                }
                LinkView.nativeAdd(collectionModelNativeLinkViewPtr, cacheItemIndexcollectionModel);
            }
        }


        RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCustomerModel();
        if (stockCustomerModelList != null) {
            long stockCustomerModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCustomerModelIndex, rowIndex);
            for (com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem : stockCustomerModelList) {
                Long cacheItemIndexstockCustomerModel = cache.get(stockCustomerModelItem);
                if (cacheItemIndexstockCustomerModel == null) {
                    cacheItemIndexstockCustomerModel = StockCustomerModelRealmProxy.insert(realm, stockCustomerModelItem, cache);
                }
                LinkView.nativeAdd(stockCustomerModelNativeLinkViewPtr, cacheItemIndexstockCustomerModel);
            }
        }


        RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCompetitorModel();
        if (stockCompetitorModelList != null) {
            long stockCompetitorModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCompetitorModelIndex, rowIndex);
            for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem : stockCompetitorModelList) {
                Long cacheItemIndexstockCompetitorModel = cache.get(stockCompetitorModelItem);
                if (cacheItemIndexstockCompetitorModel == null) {
                    cacheItemIndexstockCompetitorModel = StockCompetitorModelRealmProxy.insert(realm, stockCompetitorModelItem, cache);
                }
                LinkView.nativeAdd(stockCompetitorModelNativeLinkViewPtr, cacheItemIndexstockCompetitorModel);
            }
        }


        RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetModel();
        if (assetModelList != null) {
            long assetModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetModelIndex, rowIndex);
            for (com.bhn.sadix.Data.AssetModel assetModelItem : assetModelList) {
                Long cacheItemIndexassetModel = cache.get(assetModelItem);
                if (cacheItemIndexassetModel == null) {
                    cacheItemIndexassetModel = AssetModelRealmProxy.insert(realm, assetModelItem, cache);
                }
                LinkView.nativeAdd(assetModelNativeLinkViewPtr, cacheItemIndexassetModel);
            }
        }


        RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetMutasiModel();
        if (assetMutasiModelList != null) {
            long assetMutasiModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetMutasiModelIndex, rowIndex);
            for (com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem : assetMutasiModelList) {
                Long cacheItemIndexassetMutasiModel = cache.get(assetMutasiModelItem);
                if (cacheItemIndexassetMutasiModel == null) {
                    cacheItemIndexassetMutasiModel = AssetMutasiModelRealmProxy.insert(realm, assetMutasiModelItem, cache);
                }
                LinkView.nativeAdd(assetMutasiModelNativeLinkViewPtr, cacheItemIndexassetMutasiModel);
            }
        }

        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonModelColumnInfo columnInfo = (CommonModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CommonModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CommonModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CommonModelRealmProxyInterface) object).realmGet$CommonID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$SC = ((CommonModelRealmProxyInterface)object).realmGet$SC();
                if (realmGet$SC != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SCIndex, rowIndex, realmGet$SC, false);
                }
                String realmGet$CustomerId = ((CommonModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                }
                String realmGet$BeginVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$BeginVisitTime();
                if (realmGet$BeginVisitTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, realmGet$BeginVisitTime, false);
                }
                String realmGet$EndVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$EndVisitTime();
                if (realmGet$EndVisitTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, realmGet$EndVisitTime, false);
                }
                String realmGet$LogTime = ((CommonModelRealmProxyInterface)object).realmGet$LogTime();
                if (realmGet$LogTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, realmGet$LogTime, false);
                }
                String realmGet$SalesId = ((CommonModelRealmProxyInterface)object).realmGet$SalesId();
                if (realmGet$SalesId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
                }
                Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
                String realmGet$Description = ((CommonModelRealmProxyInterface)object).realmGet$Description();
                if (realmGet$Description != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
                }
                String realmGet$GeoLat = ((CommonModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                }
                String realmGet$GeoLong = ((CommonModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICEIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$PRICE(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$DISCOUNT(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.VisitIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Visit(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.RVisitIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$RVisitID(), false);
                String realmGet$InvNO = ((CommonModelRealmProxyInterface)object).realmGet$InvNO();
                if (realmGet$InvNO != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.InvNOIndex, rowIndex, realmGet$InvNO, false);
                }
                String realmGet$Status = ((CommonModelRealmProxyInterface)object).realmGet$Status();
                if (realmGet$Status != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.StatusIndex, rowIndex, realmGet$Status, false);
                }
                String realmGet$D = ((CommonModelRealmProxyInterface)object).realmGet$D();
                if (realmGet$D != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DIndex, rowIndex, realmGet$D, false);
                }
                String realmGet$ROrderID = ((CommonModelRealmProxyInterface)object).realmGet$ROrderID();
                if (realmGet$ROrderID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, realmGet$ROrderID, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.StatusIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$StatusID(), false);
                String realmGet$CustomerRID = ((CommonModelRealmProxyInterface)object).realmGet$CustomerRID();
                if (realmGet$CustomerRID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, realmGet$CustomerRID, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Done(), false);

                RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) object).realmGet$takingOrderModel();
                if (takingOrderModelList != null) {
                    long takingOrderModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.takingOrderModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem : takingOrderModelList) {
                        Long cacheItemIndextakingOrderModel = cache.get(takingOrderModelItem);
                        if (cacheItemIndextakingOrderModel == null) {
                            cacheItemIndextakingOrderModel = TakingOrderModelRealmProxy.insert(realm, takingOrderModelItem, cache);
                        }
                        LinkView.nativeAdd(takingOrderModelNativeLinkViewPtr, cacheItemIndextakingOrderModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) object).realmGet$returnModel();
                if (returnModelList != null) {
                    long returnModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.returnModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.ReturnModel returnModelItem : returnModelList) {
                        Long cacheItemIndexreturnModel = cache.get(returnModelItem);
                        if (cacheItemIndexreturnModel == null) {
                            cacheItemIndexreturnModel = ReturnModelRealmProxy.insert(realm, returnModelItem, cache);
                        }
                        LinkView.nativeAdd(returnModelNativeLinkViewPtr, cacheItemIndexreturnModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) object).realmGet$priceMonitoringModel();
                if (priceMonitoringModelList != null) {
                    long priceMonitoringModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.priceMonitoringModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem : priceMonitoringModelList) {
                        Long cacheItemIndexpriceMonitoringModel = cache.get(priceMonitoringModelItem);
                        if (cacheItemIndexpriceMonitoringModel == null) {
                            cacheItemIndexpriceMonitoringModel = PriceMonitoringModelRealmProxy.insert(realm, priceMonitoringModelItem, cache);
                        }
                        LinkView.nativeAdd(priceMonitoringModelNativeLinkViewPtr, cacheItemIndexpriceMonitoringModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) object).realmGet$collectionModel();
                if (collectionModelList != null) {
                    long collectionModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.collectionModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.CollectionModel collectionModelItem : collectionModelList) {
                        Long cacheItemIndexcollectionModel = cache.get(collectionModelItem);
                        if (cacheItemIndexcollectionModel == null) {
                            cacheItemIndexcollectionModel = CollectionModelRealmProxy.insert(realm, collectionModelItem, cache);
                        }
                        LinkView.nativeAdd(collectionModelNativeLinkViewPtr, cacheItemIndexcollectionModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCustomerModel();
                if (stockCustomerModelList != null) {
                    long stockCustomerModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCustomerModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem : stockCustomerModelList) {
                        Long cacheItemIndexstockCustomerModel = cache.get(stockCustomerModelItem);
                        if (cacheItemIndexstockCustomerModel == null) {
                            cacheItemIndexstockCustomerModel = StockCustomerModelRealmProxy.insert(realm, stockCustomerModelItem, cache);
                        }
                        LinkView.nativeAdd(stockCustomerModelNativeLinkViewPtr, cacheItemIndexstockCustomerModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCompetitorModel();
                if (stockCompetitorModelList != null) {
                    long stockCompetitorModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCompetitorModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem : stockCompetitorModelList) {
                        Long cacheItemIndexstockCompetitorModel = cache.get(stockCompetitorModelItem);
                        if (cacheItemIndexstockCompetitorModel == null) {
                            cacheItemIndexstockCompetitorModel = StockCompetitorModelRealmProxy.insert(realm, stockCompetitorModelItem, cache);
                        }
                        LinkView.nativeAdd(stockCompetitorModelNativeLinkViewPtr, cacheItemIndexstockCompetitorModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetModel();
                if (assetModelList != null) {
                    long assetModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.AssetModel assetModelItem : assetModelList) {
                        Long cacheItemIndexassetModel = cache.get(assetModelItem);
                        if (cacheItemIndexassetModel == null) {
                            cacheItemIndexassetModel = AssetModelRealmProxy.insert(realm, assetModelItem, cache);
                        }
                        LinkView.nativeAdd(assetModelNativeLinkViewPtr, cacheItemIndexassetModel);
                    }
                }


                RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetMutasiModel();
                if (assetMutasiModelList != null) {
                    long assetMutasiModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetMutasiModelIndex, rowIndex);
                    for (com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem : assetMutasiModelList) {
                        Long cacheItemIndexassetMutasiModel = cache.get(assetMutasiModelItem);
                        if (cacheItemIndexassetMutasiModel == null) {
                            cacheItemIndexassetMutasiModel = AssetMutasiModelRealmProxy.insert(realm, assetMutasiModelItem, cache);
                        }
                        LinkView.nativeAdd(assetMutasiModelNativeLinkViewPtr, cacheItemIndexassetMutasiModel);
                    }
                }

            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.CommonModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonModelColumnInfo columnInfo = (CommonModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CommonModelRealmProxyInterface) object).realmGet$CommonID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$SC = ((CommonModelRealmProxyInterface)object).realmGet$SC();
        if (realmGet$SC != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SCIndex, rowIndex, realmGet$SC, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SCIndex, rowIndex, false);
        }
        String realmGet$CustomerId = ((CommonModelRealmProxyInterface)object).realmGet$CustomerId();
        if (realmGet$CustomerId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
        }
        String realmGet$BeginVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$BeginVisitTime();
        if (realmGet$BeginVisitTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, realmGet$BeginVisitTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, false);
        }
        String realmGet$EndVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$EndVisitTime();
        if (realmGet$EndVisitTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, realmGet$EndVisitTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, false);
        }
        String realmGet$LogTime = ((CommonModelRealmProxyInterface)object).realmGet$LogTime();
        if (realmGet$LogTime != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, realmGet$LogTime, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, false);
        }
        String realmGet$SalesId = ((CommonModelRealmProxyInterface)object).realmGet$SalesId();
        if (realmGet$SalesId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, false);
        }
        Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
        String realmGet$Description = ((CommonModelRealmProxyInterface)object).realmGet$Description();
        if (realmGet$Description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, false);
        }
        String realmGet$GeoLat = ((CommonModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
        }
        String realmGet$GeoLong = ((CommonModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICEIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$PRICE(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$DISCOUNT(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.VisitIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Visit(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.RVisitIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$RVisitID(), false);
        String realmGet$InvNO = ((CommonModelRealmProxyInterface)object).realmGet$InvNO();
        if (realmGet$InvNO != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.InvNOIndex, rowIndex, realmGet$InvNO, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.InvNOIndex, rowIndex, false);
        }
        String realmGet$Status = ((CommonModelRealmProxyInterface)object).realmGet$Status();
        if (realmGet$Status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.StatusIndex, rowIndex, realmGet$Status, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.StatusIndex, rowIndex, false);
        }
        String realmGet$D = ((CommonModelRealmProxyInterface)object).realmGet$D();
        if (realmGet$D != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DIndex, rowIndex, realmGet$D, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.DIndex, rowIndex, false);
        }
        String realmGet$ROrderID = ((CommonModelRealmProxyInterface)object).realmGet$ROrderID();
        if (realmGet$ROrderID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, realmGet$ROrderID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.StatusIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$StatusID(), false);
        String realmGet$CustomerRID = ((CommonModelRealmProxyInterface)object).realmGet$CustomerRID();
        if (realmGet$CustomerRID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, realmGet$CustomerRID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Done(), false);

        long takingOrderModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.takingOrderModelIndex, rowIndex);
        LinkView.nativeClear(takingOrderModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) object).realmGet$takingOrderModel();
        if (takingOrderModelList != null) {
            for (com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem : takingOrderModelList) {
                Long cacheItemIndextakingOrderModel = cache.get(takingOrderModelItem);
                if (cacheItemIndextakingOrderModel == null) {
                    cacheItemIndextakingOrderModel = TakingOrderModelRealmProxy.insertOrUpdate(realm, takingOrderModelItem, cache);
                }
                LinkView.nativeAdd(takingOrderModelNativeLinkViewPtr, cacheItemIndextakingOrderModel);
            }
        }


        long returnModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.returnModelIndex, rowIndex);
        LinkView.nativeClear(returnModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) object).realmGet$returnModel();
        if (returnModelList != null) {
            for (com.bhn.sadix.Data.ReturnModel returnModelItem : returnModelList) {
                Long cacheItemIndexreturnModel = cache.get(returnModelItem);
                if (cacheItemIndexreturnModel == null) {
                    cacheItemIndexreturnModel = ReturnModelRealmProxy.insertOrUpdate(realm, returnModelItem, cache);
                }
                LinkView.nativeAdd(returnModelNativeLinkViewPtr, cacheItemIndexreturnModel);
            }
        }


        long priceMonitoringModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.priceMonitoringModelIndex, rowIndex);
        LinkView.nativeClear(priceMonitoringModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) object).realmGet$priceMonitoringModel();
        if (priceMonitoringModelList != null) {
            for (com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem : priceMonitoringModelList) {
                Long cacheItemIndexpriceMonitoringModel = cache.get(priceMonitoringModelItem);
                if (cacheItemIndexpriceMonitoringModel == null) {
                    cacheItemIndexpriceMonitoringModel = PriceMonitoringModelRealmProxy.insertOrUpdate(realm, priceMonitoringModelItem, cache);
                }
                LinkView.nativeAdd(priceMonitoringModelNativeLinkViewPtr, cacheItemIndexpriceMonitoringModel);
            }
        }


        long collectionModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.collectionModelIndex, rowIndex);
        LinkView.nativeClear(collectionModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) object).realmGet$collectionModel();
        if (collectionModelList != null) {
            for (com.bhn.sadix.Data.CollectionModel collectionModelItem : collectionModelList) {
                Long cacheItemIndexcollectionModel = cache.get(collectionModelItem);
                if (cacheItemIndexcollectionModel == null) {
                    cacheItemIndexcollectionModel = CollectionModelRealmProxy.insertOrUpdate(realm, collectionModelItem, cache);
                }
                LinkView.nativeAdd(collectionModelNativeLinkViewPtr, cacheItemIndexcollectionModel);
            }
        }


        long stockCustomerModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCustomerModelIndex, rowIndex);
        LinkView.nativeClear(stockCustomerModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCustomerModel();
        if (stockCustomerModelList != null) {
            for (com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem : stockCustomerModelList) {
                Long cacheItemIndexstockCustomerModel = cache.get(stockCustomerModelItem);
                if (cacheItemIndexstockCustomerModel == null) {
                    cacheItemIndexstockCustomerModel = StockCustomerModelRealmProxy.insertOrUpdate(realm, stockCustomerModelItem, cache);
                }
                LinkView.nativeAdd(stockCustomerModelNativeLinkViewPtr, cacheItemIndexstockCustomerModel);
            }
        }


        long stockCompetitorModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCompetitorModelIndex, rowIndex);
        LinkView.nativeClear(stockCompetitorModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCompetitorModel();
        if (stockCompetitorModelList != null) {
            for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem : stockCompetitorModelList) {
                Long cacheItemIndexstockCompetitorModel = cache.get(stockCompetitorModelItem);
                if (cacheItemIndexstockCompetitorModel == null) {
                    cacheItemIndexstockCompetitorModel = StockCompetitorModelRealmProxy.insertOrUpdate(realm, stockCompetitorModelItem, cache);
                }
                LinkView.nativeAdd(stockCompetitorModelNativeLinkViewPtr, cacheItemIndexstockCompetitorModel);
            }
        }


        long assetModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetModelIndex, rowIndex);
        LinkView.nativeClear(assetModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetModel();
        if (assetModelList != null) {
            for (com.bhn.sadix.Data.AssetModel assetModelItem : assetModelList) {
                Long cacheItemIndexassetModel = cache.get(assetModelItem);
                if (cacheItemIndexassetModel == null) {
                    cacheItemIndexassetModel = AssetModelRealmProxy.insertOrUpdate(realm, assetModelItem, cache);
                }
                LinkView.nativeAdd(assetModelNativeLinkViewPtr, cacheItemIndexassetModel);
            }
        }


        long assetMutasiModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetMutasiModelIndex, rowIndex);
        LinkView.nativeClear(assetMutasiModelNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetMutasiModel();
        if (assetMutasiModelList != null) {
            for (com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem : assetMutasiModelList) {
                Long cacheItemIndexassetMutasiModel = cache.get(assetMutasiModelItem);
                if (cacheItemIndexassetMutasiModel == null) {
                    cacheItemIndexassetMutasiModel = AssetMutasiModelRealmProxy.insertOrUpdate(realm, assetMutasiModelItem, cache);
                }
                LinkView.nativeAdd(assetMutasiModelNativeLinkViewPtr, cacheItemIndexassetMutasiModel);
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CommonModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CommonModelColumnInfo columnInfo = (CommonModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CommonModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CommonModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CommonModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CommonModelRealmProxyInterface) object).realmGet$CommonID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$SC = ((CommonModelRealmProxyInterface)object).realmGet$SC();
                if (realmGet$SC != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SCIndex, rowIndex, realmGet$SC, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SCIndex, rowIndex, false);
                }
                String realmGet$CustomerId = ((CommonModelRealmProxyInterface)object).realmGet$CustomerId();
                if (realmGet$CustomerId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, realmGet$CustomerId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIdIndex, rowIndex, false);
                }
                String realmGet$BeginVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$BeginVisitTime();
                if (realmGet$BeginVisitTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, realmGet$BeginVisitTime, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.BeginVisitTimeIndex, rowIndex, false);
                }
                String realmGet$EndVisitTime = ((CommonModelRealmProxyInterface)object).realmGet$EndVisitTime();
                if (realmGet$EndVisitTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, realmGet$EndVisitTime, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.EndVisitTimeIndex, rowIndex, false);
                }
                String realmGet$LogTime = ((CommonModelRealmProxyInterface)object).realmGet$LogTime();
                if (realmGet$LogTime != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, realmGet$LogTime, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.LogTimeIndex, rowIndex, false);
                }
                String realmGet$SalesId = ((CommonModelRealmProxyInterface)object).realmGet$SalesId();
                if (realmGet$SalesId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, realmGet$SalesId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SalesIdIndex, rowIndex, false);
                }
                Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
                String realmGet$Description = ((CommonModelRealmProxyInterface)object).realmGet$Description();
                if (realmGet$Description != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, false);
                }
                String realmGet$GeoLat = ((CommonModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
                }
                String realmGet$GeoLong = ((CommonModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICEIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$PRICE(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNTIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$DISCOUNT(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.VisitIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Visit(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.RVisitIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$RVisitID(), false);
                String realmGet$InvNO = ((CommonModelRealmProxyInterface)object).realmGet$InvNO();
                if (realmGet$InvNO != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.InvNOIndex, rowIndex, realmGet$InvNO, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.InvNOIndex, rowIndex, false);
                }
                String realmGet$Status = ((CommonModelRealmProxyInterface)object).realmGet$Status();
                if (realmGet$Status != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.StatusIndex, rowIndex, realmGet$Status, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.StatusIndex, rowIndex, false);
                }
                String realmGet$D = ((CommonModelRealmProxyInterface)object).realmGet$D();
                if (realmGet$D != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DIndex, rowIndex, realmGet$D, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.DIndex, rowIndex, false);
                }
                String realmGet$ROrderID = ((CommonModelRealmProxyInterface)object).realmGet$ROrderID();
                if (realmGet$ROrderID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, realmGet$ROrderID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ROrderIDIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.StatusIDIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$StatusID(), false);
                String realmGet$CustomerRID = ((CommonModelRealmProxyInterface)object).realmGet$CustomerRID();
                if (realmGet$CustomerRID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, realmGet$CustomerRID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustomerRIDIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CommonModelRealmProxyInterface)object).realmGet$Done(), false);

                long takingOrderModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.takingOrderModelIndex, rowIndex);
                LinkView.nativeClear(takingOrderModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) object).realmGet$takingOrderModel();
                if (takingOrderModelList != null) {
                    for (com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem : takingOrderModelList) {
                        Long cacheItemIndextakingOrderModel = cache.get(takingOrderModelItem);
                        if (cacheItemIndextakingOrderModel == null) {
                            cacheItemIndextakingOrderModel = TakingOrderModelRealmProxy.insertOrUpdate(realm, takingOrderModelItem, cache);
                        }
                        LinkView.nativeAdd(takingOrderModelNativeLinkViewPtr, cacheItemIndextakingOrderModel);
                    }
                }


                long returnModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.returnModelIndex, rowIndex);
                LinkView.nativeClear(returnModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) object).realmGet$returnModel();
                if (returnModelList != null) {
                    for (com.bhn.sadix.Data.ReturnModel returnModelItem : returnModelList) {
                        Long cacheItemIndexreturnModel = cache.get(returnModelItem);
                        if (cacheItemIndexreturnModel == null) {
                            cacheItemIndexreturnModel = ReturnModelRealmProxy.insertOrUpdate(realm, returnModelItem, cache);
                        }
                        LinkView.nativeAdd(returnModelNativeLinkViewPtr, cacheItemIndexreturnModel);
                    }
                }


                long priceMonitoringModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.priceMonitoringModelIndex, rowIndex);
                LinkView.nativeClear(priceMonitoringModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) object).realmGet$priceMonitoringModel();
                if (priceMonitoringModelList != null) {
                    for (com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem : priceMonitoringModelList) {
                        Long cacheItemIndexpriceMonitoringModel = cache.get(priceMonitoringModelItem);
                        if (cacheItemIndexpriceMonitoringModel == null) {
                            cacheItemIndexpriceMonitoringModel = PriceMonitoringModelRealmProxy.insertOrUpdate(realm, priceMonitoringModelItem, cache);
                        }
                        LinkView.nativeAdd(priceMonitoringModelNativeLinkViewPtr, cacheItemIndexpriceMonitoringModel);
                    }
                }


                long collectionModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.collectionModelIndex, rowIndex);
                LinkView.nativeClear(collectionModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) object).realmGet$collectionModel();
                if (collectionModelList != null) {
                    for (com.bhn.sadix.Data.CollectionModel collectionModelItem : collectionModelList) {
                        Long cacheItemIndexcollectionModel = cache.get(collectionModelItem);
                        if (cacheItemIndexcollectionModel == null) {
                            cacheItemIndexcollectionModel = CollectionModelRealmProxy.insertOrUpdate(realm, collectionModelItem, cache);
                        }
                        LinkView.nativeAdd(collectionModelNativeLinkViewPtr, cacheItemIndexcollectionModel);
                    }
                }


                long stockCustomerModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCustomerModelIndex, rowIndex);
                LinkView.nativeClear(stockCustomerModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCustomerModel();
                if (stockCustomerModelList != null) {
                    for (com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem : stockCustomerModelList) {
                        Long cacheItemIndexstockCustomerModel = cache.get(stockCustomerModelItem);
                        if (cacheItemIndexstockCustomerModel == null) {
                            cacheItemIndexstockCustomerModel = StockCustomerModelRealmProxy.insertOrUpdate(realm, stockCustomerModelItem, cache);
                        }
                        LinkView.nativeAdd(stockCustomerModelNativeLinkViewPtr, cacheItemIndexstockCustomerModel);
                    }
                }


                long stockCompetitorModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.stockCompetitorModelIndex, rowIndex);
                LinkView.nativeClear(stockCompetitorModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) object).realmGet$stockCompetitorModel();
                if (stockCompetitorModelList != null) {
                    for (com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem : stockCompetitorModelList) {
                        Long cacheItemIndexstockCompetitorModel = cache.get(stockCompetitorModelItem);
                        if (cacheItemIndexstockCompetitorModel == null) {
                            cacheItemIndexstockCompetitorModel = StockCompetitorModelRealmProxy.insertOrUpdate(realm, stockCompetitorModelItem, cache);
                        }
                        LinkView.nativeAdd(stockCompetitorModelNativeLinkViewPtr, cacheItemIndexstockCompetitorModel);
                    }
                }


                long assetModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetModelIndex, rowIndex);
                LinkView.nativeClear(assetModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetModel();
                if (assetModelList != null) {
                    for (com.bhn.sadix.Data.AssetModel assetModelItem : assetModelList) {
                        Long cacheItemIndexassetModel = cache.get(assetModelItem);
                        if (cacheItemIndexassetModel == null) {
                            cacheItemIndexassetModel = AssetModelRealmProxy.insertOrUpdate(realm, assetModelItem, cache);
                        }
                        LinkView.nativeAdd(assetModelNativeLinkViewPtr, cacheItemIndexassetModel);
                    }
                }


                long assetMutasiModelNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.assetMutasiModelIndex, rowIndex);
                LinkView.nativeClear(assetMutasiModelNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) object).realmGet$assetMutasiModel();
                if (assetMutasiModelList != null) {
                    for (com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem : assetMutasiModelList) {
                        Long cacheItemIndexassetMutasiModel = cache.get(assetMutasiModelItem);
                        if (cacheItemIndexassetMutasiModel == null) {
                            cacheItemIndexassetMutasiModel = AssetMutasiModelRealmProxy.insertOrUpdate(realm, assetMutasiModelItem, cache);
                        }
                        LinkView.nativeAdd(assetMutasiModelNativeLinkViewPtr, cacheItemIndexassetMutasiModel);
                    }
                }

            }
        }
    }

    public static com.bhn.sadix.Data.CommonModel createDetachedCopy(com.bhn.sadix.Data.CommonModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.CommonModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.CommonModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.CommonModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.CommonModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$SC(((CommonModelRealmProxyInterface) realmObject).realmGet$SC());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$CustomerId(((CommonModelRealmProxyInterface) realmObject).realmGet$CustomerId());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$BeginVisitTime(((CommonModelRealmProxyInterface) realmObject).realmGet$BeginVisitTime());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$EndVisitTime(((CommonModelRealmProxyInterface) realmObject).realmGet$EndVisitTime());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$LogTime(((CommonModelRealmProxyInterface) realmObject).realmGet$LogTime());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$SalesId(((CommonModelRealmProxyInterface) realmObject).realmGet$SalesId());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$S_AMOUNT(((CommonModelRealmProxyInterface) realmObject).realmGet$S_AMOUNT());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$Description(((CommonModelRealmProxyInterface) realmObject).realmGet$Description());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$GeoLat(((CommonModelRealmProxyInterface) realmObject).realmGet$GeoLat());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$GeoLong(((CommonModelRealmProxyInterface) realmObject).realmGet$GeoLong());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$QTY_B(((CommonModelRealmProxyInterface) realmObject).realmGet$QTY_B());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$QTY_K(((CommonModelRealmProxyInterface) realmObject).realmGet$QTY_K());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$PRICE(((CommonModelRealmProxyInterface) realmObject).realmGet$PRICE());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$DISCOUNT(((CommonModelRealmProxyInterface) realmObject).realmGet$DISCOUNT());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$Visit(((CommonModelRealmProxyInterface) realmObject).realmGet$Visit());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$RVisitID(((CommonModelRealmProxyInterface) realmObject).realmGet$RVisitID());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$InvNO(((CommonModelRealmProxyInterface) realmObject).realmGet$InvNO());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$Status(((CommonModelRealmProxyInterface) realmObject).realmGet$Status());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((CommonModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$D(((CommonModelRealmProxyInterface) realmObject).realmGet$D());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$ROrderID(((CommonModelRealmProxyInterface) realmObject).realmGet$ROrderID());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$StatusID(((CommonModelRealmProxyInterface) realmObject).realmGet$StatusID());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$CustomerRID(((CommonModelRealmProxyInterface) realmObject).realmGet$CustomerRID());
        ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$Done(((CommonModelRealmProxyInterface) realmObject).realmGet$Done());

        // Deep copy of takingOrderModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$takingOrderModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.TakingOrderModel> managedtakingOrderModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$takingOrderModel();
            RealmList<com.bhn.sadix.Data.TakingOrderModel> unmanagedtakingOrderModelList = new RealmList<com.bhn.sadix.Data.TakingOrderModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$takingOrderModel(unmanagedtakingOrderModelList);
            int nextDepth = currentDepth + 1;
            int size = managedtakingOrderModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createDetachedCopy(managedtakingOrderModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedtakingOrderModelList.add(item);
            }
        }

        // Deep copy of returnModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$returnModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.ReturnModel> managedreturnModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$returnModel();
            RealmList<com.bhn.sadix.Data.ReturnModel> unmanagedreturnModelList = new RealmList<com.bhn.sadix.Data.ReturnModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$returnModel(unmanagedreturnModelList);
            int nextDepth = currentDepth + 1;
            int size = managedreturnModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.ReturnModel item = ReturnModelRealmProxy.createDetachedCopy(managedreturnModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedreturnModelList.add(item);
            }
        }

        // Deep copy of priceMonitoringModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$priceMonitoringModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.PriceMonitoringModel> managedpriceMonitoringModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$priceMonitoringModel();
            RealmList<com.bhn.sadix.Data.PriceMonitoringModel> unmanagedpriceMonitoringModelList = new RealmList<com.bhn.sadix.Data.PriceMonitoringModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$priceMonitoringModel(unmanagedpriceMonitoringModelList);
            int nextDepth = currentDepth + 1;
            int size = managedpriceMonitoringModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.PriceMonitoringModel item = PriceMonitoringModelRealmProxy.createDetachedCopy(managedpriceMonitoringModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedpriceMonitoringModelList.add(item);
            }
        }

        // Deep copy of collectionModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$collectionModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.CollectionModel> managedcollectionModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$collectionModel();
            RealmList<com.bhn.sadix.Data.CollectionModel> unmanagedcollectionModelList = new RealmList<com.bhn.sadix.Data.CollectionModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$collectionModel(unmanagedcollectionModelList);
            int nextDepth = currentDepth + 1;
            int size = managedcollectionModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.CollectionModel item = CollectionModelRealmProxy.createDetachedCopy(managedcollectionModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedcollectionModelList.add(item);
            }
        }

        // Deep copy of stockCustomerModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$stockCustomerModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.StockCustomerModel> managedstockCustomerModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCustomerModel();
            RealmList<com.bhn.sadix.Data.StockCustomerModel> unmanagedstockCustomerModelList = new RealmList<com.bhn.sadix.Data.StockCustomerModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$stockCustomerModel(unmanagedstockCustomerModelList);
            int nextDepth = currentDepth + 1;
            int size = managedstockCustomerModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.StockCustomerModel item = StockCustomerModelRealmProxy.createDetachedCopy(managedstockCustomerModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedstockCustomerModelList.add(item);
            }
        }

        // Deep copy of stockCompetitorModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$stockCompetitorModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.StockCompetitorModel> managedstockCompetitorModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCompetitorModel();
            RealmList<com.bhn.sadix.Data.StockCompetitorModel> unmanagedstockCompetitorModelList = new RealmList<com.bhn.sadix.Data.StockCompetitorModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$stockCompetitorModel(unmanagedstockCompetitorModelList);
            int nextDepth = currentDepth + 1;
            int size = managedstockCompetitorModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.StockCompetitorModel item = StockCompetitorModelRealmProxy.createDetachedCopy(managedstockCompetitorModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedstockCompetitorModelList.add(item);
            }
        }

        // Deep copy of assetModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$assetModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.AssetModel> managedassetModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetModel();
            RealmList<com.bhn.sadix.Data.AssetModel> unmanagedassetModelList = new RealmList<com.bhn.sadix.Data.AssetModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$assetModel(unmanagedassetModelList);
            int nextDepth = currentDepth + 1;
            int size = managedassetModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.AssetModel item = AssetModelRealmProxy.createDetachedCopy(managedassetModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedassetModelList.add(item);
            }
        }

        // Deep copy of assetMutasiModel
        if (currentDepth == maxDepth) {
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$assetMutasiModel(null);
        } else {
            RealmList<com.bhn.sadix.Data.AssetMutasiModel> managedassetMutasiModelList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetMutasiModel();
            RealmList<com.bhn.sadix.Data.AssetMutasiModel> unmanagedassetMutasiModelList = new RealmList<com.bhn.sadix.Data.AssetMutasiModel>();
            ((CommonModelRealmProxyInterface) unmanagedObject).realmSet$assetMutasiModel(unmanagedassetMutasiModelList);
            int nextDepth = currentDepth + 1;
            int size = managedassetMutasiModelList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.AssetMutasiModel item = AssetMutasiModelRealmProxy.createDetachedCopy(managedassetMutasiModelList.get(i), nextDepth, maxDepth, cache);
                unmanagedassetMutasiModelList.add(item);
            }
        }
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.CommonModel update(Realm realm, com.bhn.sadix.Data.CommonModel realmObject, com.bhn.sadix.Data.CommonModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((CommonModelRealmProxyInterface) realmObject).realmSet$SC(((CommonModelRealmProxyInterface) newObject).realmGet$SC());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$CustomerId(((CommonModelRealmProxyInterface) newObject).realmGet$CustomerId());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$BeginVisitTime(((CommonModelRealmProxyInterface) newObject).realmGet$BeginVisitTime());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$EndVisitTime(((CommonModelRealmProxyInterface) newObject).realmGet$EndVisitTime());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$LogTime(((CommonModelRealmProxyInterface) newObject).realmGet$LogTime());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$SalesId(((CommonModelRealmProxyInterface) newObject).realmGet$SalesId());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$S_AMOUNT(((CommonModelRealmProxyInterface) newObject).realmGet$S_AMOUNT());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$Description(((CommonModelRealmProxyInterface) newObject).realmGet$Description());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$GeoLat(((CommonModelRealmProxyInterface) newObject).realmGet$GeoLat());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$GeoLong(((CommonModelRealmProxyInterface) newObject).realmGet$GeoLong());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$QTY_B(((CommonModelRealmProxyInterface) newObject).realmGet$QTY_B());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$QTY_K(((CommonModelRealmProxyInterface) newObject).realmGet$QTY_K());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$PRICE(((CommonModelRealmProxyInterface) newObject).realmGet$PRICE());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$DISCOUNT(((CommonModelRealmProxyInterface) newObject).realmGet$DISCOUNT());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$Visit(((CommonModelRealmProxyInterface) newObject).realmGet$Visit());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$RVisitID(((CommonModelRealmProxyInterface) newObject).realmGet$RVisitID());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$InvNO(((CommonModelRealmProxyInterface) newObject).realmGet$InvNO());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$Status(((CommonModelRealmProxyInterface) newObject).realmGet$Status());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$D(((CommonModelRealmProxyInterface) newObject).realmGet$D());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$ROrderID(((CommonModelRealmProxyInterface) newObject).realmGet$ROrderID());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$StatusID(((CommonModelRealmProxyInterface) newObject).realmGet$StatusID());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$CustomerRID(((CommonModelRealmProxyInterface) newObject).realmGet$CustomerRID());
        ((CommonModelRealmProxyInterface) realmObject).realmSet$Done(((CommonModelRealmProxyInterface) newObject).realmGet$Done());
        RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$takingOrderModel();
        RealmList<com.bhn.sadix.Data.TakingOrderModel> takingOrderModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$takingOrderModel();
        takingOrderModelRealmList.clear();
        if (takingOrderModelList != null) {
            for (int i = 0; i < takingOrderModelList.size(); i++) {
                com.bhn.sadix.Data.TakingOrderModel takingOrderModelItem = takingOrderModelList.get(i);
                com.bhn.sadix.Data.TakingOrderModel cachetakingOrderModel = (com.bhn.sadix.Data.TakingOrderModel) cache.get(takingOrderModelItem);
                if (cachetakingOrderModel != null) {
                    takingOrderModelRealmList.add(cachetakingOrderModel);
                } else {
                    takingOrderModelRealmList.add(TakingOrderModelRealmProxy.copyOrUpdate(realm, takingOrderModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.ReturnModel> returnModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$returnModel();
        RealmList<com.bhn.sadix.Data.ReturnModel> returnModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$returnModel();
        returnModelRealmList.clear();
        if (returnModelList != null) {
            for (int i = 0; i < returnModelList.size(); i++) {
                com.bhn.sadix.Data.ReturnModel returnModelItem = returnModelList.get(i);
                com.bhn.sadix.Data.ReturnModel cachereturnModel = (com.bhn.sadix.Data.ReturnModel) cache.get(returnModelItem);
                if (cachereturnModel != null) {
                    returnModelRealmList.add(cachereturnModel);
                } else {
                    returnModelRealmList.add(ReturnModelRealmProxy.copyOrUpdate(realm, returnModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$priceMonitoringModel();
        RealmList<com.bhn.sadix.Data.PriceMonitoringModel> priceMonitoringModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$priceMonitoringModel();
        priceMonitoringModelRealmList.clear();
        if (priceMonitoringModelList != null) {
            for (int i = 0; i < priceMonitoringModelList.size(); i++) {
                com.bhn.sadix.Data.PriceMonitoringModel priceMonitoringModelItem = priceMonitoringModelList.get(i);
                com.bhn.sadix.Data.PriceMonitoringModel cachepriceMonitoringModel = (com.bhn.sadix.Data.PriceMonitoringModel) cache.get(priceMonitoringModelItem);
                if (cachepriceMonitoringModel != null) {
                    priceMonitoringModelRealmList.add(cachepriceMonitoringModel);
                } else {
                    priceMonitoringModelRealmList.add(PriceMonitoringModelRealmProxy.copyOrUpdate(realm, priceMonitoringModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$collectionModel();
        RealmList<com.bhn.sadix.Data.CollectionModel> collectionModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$collectionModel();
        collectionModelRealmList.clear();
        if (collectionModelList != null) {
            for (int i = 0; i < collectionModelList.size(); i++) {
                com.bhn.sadix.Data.CollectionModel collectionModelItem = collectionModelList.get(i);
                com.bhn.sadix.Data.CollectionModel cachecollectionModel = (com.bhn.sadix.Data.CollectionModel) cache.get(collectionModelItem);
                if (cachecollectionModel != null) {
                    collectionModelRealmList.add(cachecollectionModel);
                } else {
                    collectionModelRealmList.add(CollectionModelRealmProxy.copyOrUpdate(realm, collectionModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$stockCustomerModel();
        RealmList<com.bhn.sadix.Data.StockCustomerModel> stockCustomerModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCustomerModel();
        stockCustomerModelRealmList.clear();
        if (stockCustomerModelList != null) {
            for (int i = 0; i < stockCustomerModelList.size(); i++) {
                com.bhn.sadix.Data.StockCustomerModel stockCustomerModelItem = stockCustomerModelList.get(i);
                com.bhn.sadix.Data.StockCustomerModel cachestockCustomerModel = (com.bhn.sadix.Data.StockCustomerModel) cache.get(stockCustomerModelItem);
                if (cachestockCustomerModel != null) {
                    stockCustomerModelRealmList.add(cachestockCustomerModel);
                } else {
                    stockCustomerModelRealmList.add(StockCustomerModelRealmProxy.copyOrUpdate(realm, stockCustomerModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$stockCompetitorModel();
        RealmList<com.bhn.sadix.Data.StockCompetitorModel> stockCompetitorModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$stockCompetitorModel();
        stockCompetitorModelRealmList.clear();
        if (stockCompetitorModelList != null) {
            for (int i = 0; i < stockCompetitorModelList.size(); i++) {
                com.bhn.sadix.Data.StockCompetitorModel stockCompetitorModelItem = stockCompetitorModelList.get(i);
                com.bhn.sadix.Data.StockCompetitorModel cachestockCompetitorModel = (com.bhn.sadix.Data.StockCompetitorModel) cache.get(stockCompetitorModelItem);
                if (cachestockCompetitorModel != null) {
                    stockCompetitorModelRealmList.add(cachestockCompetitorModel);
                } else {
                    stockCompetitorModelRealmList.add(StockCompetitorModelRealmProxy.copyOrUpdate(realm, stockCompetitorModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.AssetModel> assetModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$assetModel();
        RealmList<com.bhn.sadix.Data.AssetModel> assetModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetModel();
        assetModelRealmList.clear();
        if (assetModelList != null) {
            for (int i = 0; i < assetModelList.size(); i++) {
                com.bhn.sadix.Data.AssetModel assetModelItem = assetModelList.get(i);
                com.bhn.sadix.Data.AssetModel cacheassetModel = (com.bhn.sadix.Data.AssetModel) cache.get(assetModelItem);
                if (cacheassetModel != null) {
                    assetModelRealmList.add(cacheassetModel);
                } else {
                    assetModelRealmList.add(AssetModelRealmProxy.copyOrUpdate(realm, assetModelList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelList = ((CommonModelRealmProxyInterface) newObject).realmGet$assetMutasiModel();
        RealmList<com.bhn.sadix.Data.AssetMutasiModel> assetMutasiModelRealmList = ((CommonModelRealmProxyInterface) realmObject).realmGet$assetMutasiModel();
        assetMutasiModelRealmList.clear();
        if (assetMutasiModelList != null) {
            for (int i = 0; i < assetMutasiModelList.size(); i++) {
                com.bhn.sadix.Data.AssetMutasiModel assetMutasiModelItem = assetMutasiModelList.get(i);
                com.bhn.sadix.Data.AssetMutasiModel cacheassetMutasiModel = (com.bhn.sadix.Data.AssetMutasiModel) cache.get(assetMutasiModelItem);
                if (cacheassetMutasiModel != null) {
                    assetMutasiModelRealmList.add(cacheassetMutasiModel);
                } else {
                    assetMutasiModelRealmList.add(AssetMutasiModelRealmProxy.copyOrUpdate(realm, assetMutasiModelList.get(i), true, cache));
                }
            }
        }
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("CommonModel = [");
        stringBuilder.append("{SC:");
        stringBuilder.append(realmGet$SC() != null ? realmGet$SC() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerId:");
        stringBuilder.append(realmGet$CustomerId() != null ? realmGet$CustomerId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{BeginVisitTime:");
        stringBuilder.append(realmGet$BeginVisitTime() != null ? realmGet$BeginVisitTime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{EndVisitTime:");
        stringBuilder.append(realmGet$EndVisitTime() != null ? realmGet$EndVisitTime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{LogTime:");
        stringBuilder.append(realmGet$LogTime() != null ? realmGet$LogTime() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SalesId:");
        stringBuilder.append(realmGet$SalesId() != null ? realmGet$SalesId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{S_AMOUNT:");
        stringBuilder.append(realmGet$S_AMOUNT());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Description:");
        stringBuilder.append(realmGet$Description() != null ? realmGet$Description() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLat:");
        stringBuilder.append(realmGet$GeoLat() != null ? realmGet$GeoLat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLong:");
        stringBuilder.append(realmGet$GeoLong() != null ? realmGet$GeoLong() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_B:");
        stringBuilder.append(realmGet$QTY_B());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_K:");
        stringBuilder.append(realmGet$QTY_K());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PRICE:");
        stringBuilder.append(realmGet$PRICE());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DISCOUNT:");
        stringBuilder.append(realmGet$DISCOUNT());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Visit:");
        stringBuilder.append(realmGet$Visit());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RVisitID:");
        stringBuilder.append(realmGet$RVisitID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{InvNO:");
        stringBuilder.append(realmGet$InvNO() != null ? realmGet$InvNO() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Status:");
        stringBuilder.append(realmGet$Status() != null ? realmGet$Status() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{D:");
        stringBuilder.append(realmGet$D() != null ? realmGet$D() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ROrderID:");
        stringBuilder.append(realmGet$ROrderID() != null ? realmGet$ROrderID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{StatusID:");
        stringBuilder.append(realmGet$StatusID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerRID:");
        stringBuilder.append(realmGet$CustomerRID() != null ? realmGet$CustomerRID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Done:");
        stringBuilder.append(realmGet$Done());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{takingOrderModel:");
        stringBuilder.append("RealmList<TakingOrderModel>[").append(realmGet$takingOrderModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{returnModel:");
        stringBuilder.append("RealmList<ReturnModel>[").append(realmGet$returnModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{priceMonitoringModel:");
        stringBuilder.append("RealmList<PriceMonitoringModel>[").append(realmGet$priceMonitoringModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{collectionModel:");
        stringBuilder.append("RealmList<CollectionModel>[").append(realmGet$collectionModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{stockCustomerModel:");
        stringBuilder.append("RealmList<StockCustomerModel>[").append(realmGet$stockCustomerModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{stockCompetitorModel:");
        stringBuilder.append("RealmList<StockCompetitorModel>[").append(realmGet$stockCompetitorModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{assetModel:");
        stringBuilder.append("RealmList<AssetModel>[").append(realmGet$assetModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{assetMutasiModel:");
        stringBuilder.append("RealmList<AssetMutasiModel>[").append(realmGet$assetMutasiModel().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommonModelRealmProxy aCommonModel = (CommonModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCommonModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCommonModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCommonModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
