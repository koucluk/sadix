package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AssetServiceModelRealmProxy extends com.bhn.sadix.Data.AssetServiceModel
    implements RealmObjectProxy, AssetServiceModelRealmProxyInterface {

    static final class AssetServiceModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long ASETNAMEIndex;
        public long ASETTYPEIndex;
        public long ASETNOIndex;
        public long ASETIDIndex;
        public long CommonIDIndex;

        AssetServiceModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(5);
            this.ASETNAMEIndex = getValidColumnIndex(path, table, "AssetServiceModel", "ASETNAME");
            indicesMap.put("ASETNAME", this.ASETNAMEIndex);
            this.ASETTYPEIndex = getValidColumnIndex(path, table, "AssetServiceModel", "ASETTYPE");
            indicesMap.put("ASETTYPE", this.ASETTYPEIndex);
            this.ASETNOIndex = getValidColumnIndex(path, table, "AssetServiceModel", "ASETNO");
            indicesMap.put("ASETNO", this.ASETNOIndex);
            this.ASETIDIndex = getValidColumnIndex(path, table, "AssetServiceModel", "ASETID");
            indicesMap.put("ASETID", this.ASETIDIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "AssetServiceModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final AssetServiceModelColumnInfo otherInfo = (AssetServiceModelColumnInfo) other;
            this.ASETNAMEIndex = otherInfo.ASETNAMEIndex;
            this.ASETTYPEIndex = otherInfo.ASETTYPEIndex;
            this.ASETNOIndex = otherInfo.ASETNOIndex;
            this.ASETIDIndex = otherInfo.ASETIDIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final AssetServiceModelColumnInfo clone() {
            return (AssetServiceModelColumnInfo) super.clone();
        }

    }
    private AssetServiceModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.AssetServiceModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("ASETNAME");
        fieldNames.add("ASETTYPE");
        fieldNames.add("ASETNO");
        fieldNames.add("ASETID");
        fieldNames.add("CommonID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    AssetServiceModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (AssetServiceModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.AssetServiceModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$ASETNAME() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ASETNAMEIndex);
    }

    public void realmSet$ASETNAME(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ASETNAMEIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ASETNAMEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ASETNAMEIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ASETNAMEIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ASETTYPE() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ASETTYPEIndex);
    }

    public void realmSet$ASETTYPE(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ASETTYPEIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ASETTYPEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ASETTYPEIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ASETTYPEIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ASETNO() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ASETNOIndex);
    }

    public void realmSet$ASETNO(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ASETNOIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ASETNOIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ASETNOIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ASETNOIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ASETID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ASETIDIndex);
    }

    public void realmSet$ASETID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ASETIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ASETIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ASETIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ASETIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("AssetServiceModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("AssetServiceModel");
            realmObjectSchema.add(new Property("ASETNAME", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ASETTYPE", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ASETNO", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ASETID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("AssetServiceModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_AssetServiceModel")) {
            Table table = sharedRealm.getTable("class_AssetServiceModel");
            table.addColumn(RealmFieldType.STRING, "ASETNAME", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ASETTYPE", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ASETNO", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ASETID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_AssetServiceModel");
    }

    public static AssetServiceModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_AssetServiceModel")) {
            Table table = sharedRealm.getTable("class_AssetServiceModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 5) {
                if (columnCount < 5) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 5 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 5 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 5 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final AssetServiceModelColumnInfo columnInfo = new AssetServiceModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("ASETNAME")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ASETNAME' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ASETNAME") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ASETNAME' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ASETNAMEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ASETNAME' is required. Either set @Required to field 'ASETNAME' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ASETTYPE")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ASETTYPE' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ASETTYPE") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ASETTYPE' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ASETTYPEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ASETTYPE' is required. Either set @Required to field 'ASETTYPE' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ASETNO")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ASETNO' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ASETNO") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ASETNO' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ASETNOIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ASETNO' is required. Either set @Required to field 'ASETNO' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ASETID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ASETID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ASETID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ASETID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ASETIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ASETID' is required. Either set @Required to field 'ASETID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'AssetServiceModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_AssetServiceModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.AssetServiceModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.AssetServiceModel obj = realm.createObjectInternal(com.bhn.sadix.Data.AssetServiceModel.class, true, excludeFields);
        if (json.has("ASETNAME")) {
            if (json.isNull("ASETNAME")) {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNAME(null);
            } else {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNAME((String) json.getString("ASETNAME"));
            }
        }
        if (json.has("ASETTYPE")) {
            if (json.isNull("ASETTYPE")) {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETTYPE(null);
            } else {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETTYPE((String) json.getString("ASETTYPE"));
            }
        }
        if (json.has("ASETNO")) {
            if (json.isNull("ASETNO")) {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNO(null);
            } else {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNO((String) json.getString("ASETNO"));
            }
        }
        if (json.has("ASETID")) {
            if (json.isNull("ASETID")) {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETID(null);
            } else {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETID((String) json.getString("ASETID"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((AssetServiceModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.AssetServiceModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.AssetServiceModel obj = new com.bhn.sadix.Data.AssetServiceModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("ASETNAME")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNAME(null);
                } else {
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNAME((String) reader.nextString());
                }
            } else if (name.equals("ASETTYPE")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETTYPE(null);
                } else {
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETTYPE((String) reader.nextString());
                }
            } else if (name.equals("ASETNO")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNO(null);
                } else {
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETNO((String) reader.nextString());
                }
            } else if (name.equals("ASETID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETID(null);
                } else {
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$ASETID((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((AssetServiceModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.AssetServiceModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.AssetServiceModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetServiceModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.AssetServiceModel copy(Realm realm, com.bhn.sadix.Data.AssetServiceModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetServiceModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.AssetServiceModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.AssetServiceModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((AssetServiceModelRealmProxyInterface) realmObject).realmSet$ASETNAME(((AssetServiceModelRealmProxyInterface) newObject).realmGet$ASETNAME());
            ((AssetServiceModelRealmProxyInterface) realmObject).realmSet$ASETTYPE(((AssetServiceModelRealmProxyInterface) newObject).realmGet$ASETTYPE());
            ((AssetServiceModelRealmProxyInterface) realmObject).realmSet$ASETNO(((AssetServiceModelRealmProxyInterface) newObject).realmGet$ASETNO());
            ((AssetServiceModelRealmProxyInterface) realmObject).realmSet$ASETID(((AssetServiceModelRealmProxyInterface) newObject).realmGet$ASETID());
            ((AssetServiceModelRealmProxyInterface) realmObject).realmSet$CommonID(((AssetServiceModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.AssetServiceModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetServiceModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetServiceModelColumnInfo columnInfo = (AssetServiceModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetServiceModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$ASETNAME = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNAME();
        if (realmGet$ASETNAME != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, realmGet$ASETNAME, false);
        }
        String realmGet$ASETTYPE = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETTYPE();
        if (realmGet$ASETTYPE != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, realmGet$ASETTYPE, false);
        }
        String realmGet$ASETNO = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNO();
        if (realmGet$ASETNO != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, realmGet$ASETNO, false);
        }
        String realmGet$ASETID = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETID();
        if (realmGet$ASETID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, realmGet$ASETID, false);
        }
        String realmGet$CommonID = ((AssetServiceModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetServiceModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetServiceModelColumnInfo columnInfo = (AssetServiceModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetServiceModel.class);
        com.bhn.sadix.Data.AssetServiceModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetServiceModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$ASETNAME = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNAME();
                if (realmGet$ASETNAME != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, realmGet$ASETNAME, false);
                }
                String realmGet$ASETTYPE = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETTYPE();
                if (realmGet$ASETTYPE != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, realmGet$ASETTYPE, false);
                }
                String realmGet$ASETNO = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNO();
                if (realmGet$ASETNO != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, realmGet$ASETNO, false);
                }
                String realmGet$ASETID = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETID();
                if (realmGet$ASETID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, realmGet$ASETID, false);
                }
                String realmGet$CommonID = ((AssetServiceModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.AssetServiceModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetServiceModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetServiceModelColumnInfo columnInfo = (AssetServiceModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetServiceModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$ASETNAME = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNAME();
        if (realmGet$ASETNAME != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, realmGet$ASETNAME, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, false);
        }
        String realmGet$ASETTYPE = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETTYPE();
        if (realmGet$ASETTYPE != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, realmGet$ASETTYPE, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, false);
        }
        String realmGet$ASETNO = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNO();
        if (realmGet$ASETNO != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, realmGet$ASETNO, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, false);
        }
        String realmGet$ASETID = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETID();
        if (realmGet$ASETID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, realmGet$ASETID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((AssetServiceModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetServiceModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetServiceModelColumnInfo columnInfo = (AssetServiceModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetServiceModel.class);
        com.bhn.sadix.Data.AssetServiceModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetServiceModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$ASETNAME = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNAME();
                if (realmGet$ASETNAME != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, realmGet$ASETNAME, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ASETNAMEIndex, rowIndex, false);
                }
                String realmGet$ASETTYPE = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETTYPE();
                if (realmGet$ASETTYPE != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, realmGet$ASETTYPE, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ASETTYPEIndex, rowIndex, false);
                }
                String realmGet$ASETNO = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETNO();
                if (realmGet$ASETNO != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, realmGet$ASETNO, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ASETNOIndex, rowIndex, false);
                }
                String realmGet$ASETID = ((AssetServiceModelRealmProxyInterface)object).realmGet$ASETID();
                if (realmGet$ASETID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, realmGet$ASETID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ASETIDIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((AssetServiceModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.AssetServiceModel createDetachedCopy(com.bhn.sadix.Data.AssetServiceModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.AssetServiceModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.AssetServiceModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.AssetServiceModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.AssetServiceModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((AssetServiceModelRealmProxyInterface) unmanagedObject).realmSet$ASETNAME(((AssetServiceModelRealmProxyInterface) realmObject).realmGet$ASETNAME());
        ((AssetServiceModelRealmProxyInterface) unmanagedObject).realmSet$ASETTYPE(((AssetServiceModelRealmProxyInterface) realmObject).realmGet$ASETTYPE());
        ((AssetServiceModelRealmProxyInterface) unmanagedObject).realmSet$ASETNO(((AssetServiceModelRealmProxyInterface) realmObject).realmGet$ASETNO());
        ((AssetServiceModelRealmProxyInterface) unmanagedObject).realmSet$ASETID(((AssetServiceModelRealmProxyInterface) realmObject).realmGet$ASETID());
        ((AssetServiceModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((AssetServiceModelRealmProxyInterface) realmObject).realmGet$CommonID());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("AssetServiceModel = [");
        stringBuilder.append("{ASETNAME:");
        stringBuilder.append(realmGet$ASETNAME() != null ? realmGet$ASETNAME() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ASETTYPE:");
        stringBuilder.append(realmGet$ASETTYPE() != null ? realmGet$ASETTYPE() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ASETNO:");
        stringBuilder.append(realmGet$ASETNO() != null ? realmGet$ASETNO() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ASETID:");
        stringBuilder.append(realmGet$ASETID() != null ? realmGet$ASETID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssetServiceModelRealmProxy aAssetServiceModel = (AssetServiceModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aAssetServiceModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAssetServiceModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aAssetServiceModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
