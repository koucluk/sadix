package io.realm;


public interface CustomerBrandingModelRealmProxyInterface {
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
    public String realmGet$SalesID();
    public void realmSet$SalesID(String value);
    public String realmGet$CustomerID();
    public void realmSet$CustomerID(String value);
    public String realmGet$BrandingID();
    public void realmSet$BrandingID(String value);
    public String realmGet$PhotoPic();
    public void realmSet$PhotoPic(String value);
    public String realmGet$GeoLong();
    public void realmSet$GeoLong(String value);
    public String realmGet$GeoLat();
    public void realmSet$GeoLat(String value);
    public String realmGet$Description();
    public void realmSet$Description(String value);
    public String realmGet$NmCust();
    public void realmSet$NmCust(String value);
    public String realmGet$NmBranding();
    public void realmSet$NmBranding(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public int realmGet$Done();
    public void realmSet$Done(int value);
}
