package io.realm;


public interface SurveyModelRealmProxyInterface {
    public String realmGet$test();
    public void realmSet$test(String value);
}
