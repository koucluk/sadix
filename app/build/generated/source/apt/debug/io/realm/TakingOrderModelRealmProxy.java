package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TakingOrderModelRealmProxy extends com.bhn.sadix.Data.TakingOrderModel
    implements RealmObjectProxy, TakingOrderModelRealmProxyInterface {

    static final class TakingOrderModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long CustIdIndex;
        public long SKUIdIndex;
        public long tanggalIndex;
        public long SalesProgramIdIndex;
        public long QTY_BIndex;
        public long QTY_KIndex;
        public long DISCOUNT1Index;
        public long DISCOUNT2Index;
        public long DISCOUNT3Index;
        public long TOTAL_PRICEIndex;
        public long PAYMENT_TERMIndex;
        public long noteIndex;
        public long PRICE_BIndex;
        public long PRICE_KIndex;
        public long LAMA_KREDITIndex;
        public long DiscountTypeIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;
        public long PrinsipleIDIndex;

        TakingOrderModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(19);
            this.CustIdIndex = getValidColumnIndex(path, table, "TakingOrderModel", "CustId");
            indicesMap.put("CustId", this.CustIdIndex);
            this.SKUIdIndex = getValidColumnIndex(path, table, "TakingOrderModel", "SKUId");
            indicesMap.put("SKUId", this.SKUIdIndex);
            this.tanggalIndex = getValidColumnIndex(path, table, "TakingOrderModel", "tanggal");
            indicesMap.put("tanggal", this.tanggalIndex);
            this.SalesProgramIdIndex = getValidColumnIndex(path, table, "TakingOrderModel", "SalesProgramId");
            indicesMap.put("SalesProgramId", this.SalesProgramIdIndex);
            this.QTY_BIndex = getValidColumnIndex(path, table, "TakingOrderModel", "QTY_B");
            indicesMap.put("QTY_B", this.QTY_BIndex);
            this.QTY_KIndex = getValidColumnIndex(path, table, "TakingOrderModel", "QTY_K");
            indicesMap.put("QTY_K", this.QTY_KIndex);
            this.DISCOUNT1Index = getValidColumnIndex(path, table, "TakingOrderModel", "DISCOUNT1");
            indicesMap.put("DISCOUNT1", this.DISCOUNT1Index);
            this.DISCOUNT2Index = getValidColumnIndex(path, table, "TakingOrderModel", "DISCOUNT2");
            indicesMap.put("DISCOUNT2", this.DISCOUNT2Index);
            this.DISCOUNT3Index = getValidColumnIndex(path, table, "TakingOrderModel", "DISCOUNT3");
            indicesMap.put("DISCOUNT3", this.DISCOUNT3Index);
            this.TOTAL_PRICEIndex = getValidColumnIndex(path, table, "TakingOrderModel", "TOTAL_PRICE");
            indicesMap.put("TOTAL_PRICE", this.TOTAL_PRICEIndex);
            this.PAYMENT_TERMIndex = getValidColumnIndex(path, table, "TakingOrderModel", "PAYMENT_TERM");
            indicesMap.put("PAYMENT_TERM", this.PAYMENT_TERMIndex);
            this.noteIndex = getValidColumnIndex(path, table, "TakingOrderModel", "note");
            indicesMap.put("note", this.noteIndex);
            this.PRICE_BIndex = getValidColumnIndex(path, table, "TakingOrderModel", "PRICE_B");
            indicesMap.put("PRICE_B", this.PRICE_BIndex);
            this.PRICE_KIndex = getValidColumnIndex(path, table, "TakingOrderModel", "PRICE_K");
            indicesMap.put("PRICE_K", this.PRICE_KIndex);
            this.LAMA_KREDITIndex = getValidColumnIndex(path, table, "TakingOrderModel", "LAMA_KREDIT");
            indicesMap.put("LAMA_KREDIT", this.LAMA_KREDITIndex);
            this.DiscountTypeIndex = getValidColumnIndex(path, table, "TakingOrderModel", "DiscountType");
            indicesMap.put("DiscountType", this.DiscountTypeIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "TakingOrderModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "TakingOrderModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);
            this.PrinsipleIDIndex = getValidColumnIndex(path, table, "TakingOrderModel", "PrinsipleID");
            indicesMap.put("PrinsipleID", this.PrinsipleIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final TakingOrderModelColumnInfo otherInfo = (TakingOrderModelColumnInfo) other;
            this.CustIdIndex = otherInfo.CustIdIndex;
            this.SKUIdIndex = otherInfo.SKUIdIndex;
            this.tanggalIndex = otherInfo.tanggalIndex;
            this.SalesProgramIdIndex = otherInfo.SalesProgramIdIndex;
            this.QTY_BIndex = otherInfo.QTY_BIndex;
            this.QTY_KIndex = otherInfo.QTY_KIndex;
            this.DISCOUNT1Index = otherInfo.DISCOUNT1Index;
            this.DISCOUNT2Index = otherInfo.DISCOUNT2Index;
            this.DISCOUNT3Index = otherInfo.DISCOUNT3Index;
            this.TOTAL_PRICEIndex = otherInfo.TOTAL_PRICEIndex;
            this.PAYMENT_TERMIndex = otherInfo.PAYMENT_TERMIndex;
            this.noteIndex = otherInfo.noteIndex;
            this.PRICE_BIndex = otherInfo.PRICE_BIndex;
            this.PRICE_KIndex = otherInfo.PRICE_KIndex;
            this.LAMA_KREDITIndex = otherInfo.LAMA_KREDITIndex;
            this.DiscountTypeIndex = otherInfo.DiscountTypeIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;
            this.PrinsipleIDIndex = otherInfo.PrinsipleIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final TakingOrderModelColumnInfo clone() {
            return (TakingOrderModelColumnInfo) super.clone();
        }

    }
    private TakingOrderModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.TakingOrderModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("CustId");
        fieldNames.add("SKUId");
        fieldNames.add("tanggal");
        fieldNames.add("SalesProgramId");
        fieldNames.add("QTY_B");
        fieldNames.add("QTY_K");
        fieldNames.add("DISCOUNT1");
        fieldNames.add("DISCOUNT2");
        fieldNames.add("DISCOUNT3");
        fieldNames.add("TOTAL_PRICE");
        fieldNames.add("PAYMENT_TERM");
        fieldNames.add("note");
        fieldNames.add("PRICE_B");
        fieldNames.add("PRICE_K");
        fieldNames.add("LAMA_KREDIT");
        fieldNames.add("DiscountType");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        fieldNames.add("PrinsipleID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    TakingOrderModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (TakingOrderModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.TakingOrderModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$CustId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustIdIndex);
    }

    public void realmSet$CustId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SKUId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SKUIdIndex);
    }

    public void realmSet$SKUId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SKUIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SKUIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SKUIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SKUIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$tanggal() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tanggalIndex);
    }

    public void realmSet$tanggal(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.tanggalIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.tanggalIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.tanggalIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.tanggalIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$SalesProgramId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SalesProgramIdIndex);
    }

    public void realmSet$SalesProgramId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SalesProgramIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SalesProgramIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SalesProgramIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SalesProgramIdIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_B() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_BIndex);
    }

    public void realmSet$QTY_B(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_BIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_BIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$QTY_K() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.QTY_KIndex);
    }

    public void realmSet$QTY_K(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QTY_KIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QTY_KIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$DISCOUNT1() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.DISCOUNT1Index);
    }

    public void realmSet$DISCOUNT1(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.DISCOUNT1Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.DISCOUNT1Index, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$DISCOUNT2() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.DISCOUNT2Index);
    }

    public void realmSet$DISCOUNT2(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.DISCOUNT2Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.DISCOUNT2Index, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$DISCOUNT3() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.DISCOUNT3Index);
    }

    public void realmSet$DISCOUNT3(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.DISCOUNT3Index, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.DISCOUNT3Index, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$TOTAL_PRICE() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.TOTAL_PRICEIndex);
    }

    public void realmSet$TOTAL_PRICE(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.TOTAL_PRICEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.TOTAL_PRICEIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$PAYMENT_TERM() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.PAYMENT_TERMIndex);
    }

    public void realmSet$PAYMENT_TERM(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.PAYMENT_TERMIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.PAYMENT_TERMIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.PAYMENT_TERMIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.PAYMENT_TERMIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$note() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.noteIndex);
    }

    public void realmSet$note(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.noteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.noteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.noteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.noteIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$PRICE_B() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.PRICE_BIndex);
    }

    public void realmSet$PRICE_B(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.PRICE_BIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.PRICE_BIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$PRICE_K() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.PRICE_KIndex);
    }

    public void realmSet$PRICE_K(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.PRICE_KIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.PRICE_KIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$LAMA_KREDIT() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.LAMA_KREDITIndex);
    }

    public void realmSet$LAMA_KREDIT(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.LAMA_KREDITIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.LAMA_KREDITIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$DiscountType() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.DiscountTypeIndex);
    }

    public void realmSet$DiscountType(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.DiscountTypeIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.DiscountTypeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.DiscountTypeIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.DiscountTypeIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public String realmGet$PrinsipleID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.PrinsipleIDIndex);
    }

    public void realmSet$PrinsipleID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.PrinsipleIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.PrinsipleIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.PrinsipleIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.PrinsipleIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("TakingOrderModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("TakingOrderModel");
            realmObjectSchema.add(new Property("CustId", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SKUId", RealmFieldType.STRING, !Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("tanggal", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SalesProgramId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_B", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("QTY_K", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DISCOUNT1", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DISCOUNT2", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DISCOUNT3", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("TOTAL_PRICE", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("PAYMENT_TERM", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("note", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("PRICE_B", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("PRICE_K", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("LAMA_KREDIT", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("DiscountType", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("PrinsipleID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("TakingOrderModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_TakingOrderModel")) {
            Table table = sharedRealm.getTable("class_TakingOrderModel");
            table.addColumn(RealmFieldType.STRING, "CustId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SKUId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "tanggal", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SalesProgramId", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_B", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "QTY_K", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "DISCOUNT1", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "DISCOUNT2", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "DISCOUNT3", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "TOTAL_PRICE", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "PAYMENT_TERM", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "note", Table.NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "PRICE_B", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "PRICE_K", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "LAMA_KREDIT", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "DiscountType", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "PrinsipleID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("CustId"));
            table.addSearchIndex(table.getColumnIndex("SKUId"));
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_TakingOrderModel");
    }

    public static TakingOrderModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_TakingOrderModel")) {
            Table table = sharedRealm.getTable("class_TakingOrderModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 19) {
                if (columnCount < 19) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 19 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 19 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 19 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final TakingOrderModelColumnInfo columnInfo = new TakingOrderModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("CustId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustId' is required. Either set @Required to field 'CustId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("CustId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'CustId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("SKUId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SKUId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SKUId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SKUId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SKUIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SKUId' is required. Either set @Required to field 'SKUId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("SKUId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'SKUId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("tanggal")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'tanggal' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("tanggal") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'tanggal' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.tanggalIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'tanggal' is required. Either set @Required to field 'tanggal' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SalesProgramId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SalesProgramId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SalesProgramId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SalesProgramId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SalesProgramIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SalesProgramId' is required. Either set @Required to field 'SalesProgramId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_B")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_B' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_B") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_B' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_BIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_B' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_B' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("QTY_K")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'QTY_K' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("QTY_K") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'QTY_K' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QTY_KIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'QTY_K' does support null values in the existing Realm file. Use corresponding boxed type for field 'QTY_K' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DISCOUNT1")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DISCOUNT1' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DISCOUNT1") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'DISCOUNT1' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DISCOUNT1Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DISCOUNT1' does support null values in the existing Realm file. Use corresponding boxed type for field 'DISCOUNT1' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DISCOUNT2")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DISCOUNT2' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DISCOUNT2") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'DISCOUNT2' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DISCOUNT2Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DISCOUNT2' does support null values in the existing Realm file. Use corresponding boxed type for field 'DISCOUNT2' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DISCOUNT3")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DISCOUNT3' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DISCOUNT3") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'DISCOUNT3' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DISCOUNT3Index)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DISCOUNT3' does support null values in the existing Realm file. Use corresponding boxed type for field 'DISCOUNT3' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("TOTAL_PRICE")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'TOTAL_PRICE' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("TOTAL_PRICE") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'TOTAL_PRICE' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.TOTAL_PRICEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'TOTAL_PRICE' does support null values in the existing Realm file. Use corresponding boxed type for field 'TOTAL_PRICE' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PAYMENT_TERM")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PAYMENT_TERM' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PAYMENT_TERM") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'PAYMENT_TERM' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.PAYMENT_TERMIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PAYMENT_TERM' is required. Either set @Required to field 'PAYMENT_TERM' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("note")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'note' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("note") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'note' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.noteIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'note' is required. Either set @Required to field 'note' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PRICE_B")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PRICE_B' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PRICE_B") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'PRICE_B' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PRICE_BIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PRICE_B' does support null values in the existing Realm file. Use corresponding boxed type for field 'PRICE_B' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PRICE_K")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PRICE_K' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PRICE_K") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'PRICE_K' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PRICE_KIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PRICE_K' does support null values in the existing Realm file. Use corresponding boxed type for field 'PRICE_K' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("LAMA_KREDIT")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'LAMA_KREDIT' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("LAMA_KREDIT") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'LAMA_KREDIT' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.LAMA_KREDITIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'LAMA_KREDIT' does support null values in the existing Realm file. Use corresponding boxed type for field 'LAMA_KREDIT' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("DiscountType")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'DiscountType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("DiscountType") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'DiscountType' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.DiscountTypeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'DiscountType' is required. Either set @Required to field 'DiscountType' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("PrinsipleID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PrinsipleID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PrinsipleID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'PrinsipleID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.PrinsipleIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PrinsipleID' is required. Either set @Required to field 'PrinsipleID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'TakingOrderModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_TakingOrderModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.TakingOrderModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.TakingOrderModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.TakingOrderModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.TakingOrderModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.TakingOrderModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.TakingOrderModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.TakingOrderModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("CustId")) {
            if (json.isNull("CustId")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$CustId(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$CustId((String) json.getString("CustId"));
            }
        }
        if (json.has("SKUId")) {
            if (json.isNull("SKUId")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$SKUId(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$SKUId((String) json.getString("SKUId"));
            }
        }
        if (json.has("tanggal")) {
            if (json.isNull("tanggal")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$tanggal(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$tanggal((String) json.getString("tanggal"));
            }
        }
        if (json.has("SalesProgramId")) {
            if (json.isNull("SalesProgramId")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$SalesProgramId(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$SalesProgramId((String) json.getString("SalesProgramId"));
            }
        }
        if (json.has("QTY_B")) {
            if (json.isNull("QTY_B")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$QTY_B((int) json.getInt("QTY_B"));
            }
        }
        if (json.has("QTY_K")) {
            if (json.isNull("QTY_K")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$QTY_K((int) json.getInt("QTY_K"));
            }
        }
        if (json.has("DISCOUNT1")) {
            if (json.isNull("DISCOUNT1")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT1' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT1((double) json.getDouble("DISCOUNT1"));
            }
        }
        if (json.has("DISCOUNT2")) {
            if (json.isNull("DISCOUNT2")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT2' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT2((double) json.getDouble("DISCOUNT2"));
            }
        }
        if (json.has("DISCOUNT3")) {
            if (json.isNull("DISCOUNT3")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT3' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT3((double) json.getDouble("DISCOUNT3"));
            }
        }
        if (json.has("TOTAL_PRICE")) {
            if (json.isNull("TOTAL_PRICE")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'TOTAL_PRICE' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$TOTAL_PRICE((double) json.getDouble("TOTAL_PRICE"));
            }
        }
        if (json.has("PAYMENT_TERM")) {
            if (json.isNull("PAYMENT_TERM")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PAYMENT_TERM(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PAYMENT_TERM((String) json.getString("PAYMENT_TERM"));
            }
        }
        if (json.has("note")) {
            if (json.isNull("note")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$note(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$note((String) json.getString("note"));
            }
        }
        if (json.has("PRICE_B")) {
            if (json.isNull("PRICE_B")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE_B' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PRICE_B((double) json.getDouble("PRICE_B"));
            }
        }
        if (json.has("PRICE_K")) {
            if (json.isNull("PRICE_K")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE_K' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PRICE_K((double) json.getDouble("PRICE_K"));
            }
        }
        if (json.has("LAMA_KREDIT")) {
            if (json.isNull("LAMA_KREDIT")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'LAMA_KREDIT' to null.");
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$LAMA_KREDIT((int) json.getInt("LAMA_KREDIT"));
            }
        }
        if (json.has("DiscountType")) {
            if (json.isNull("DiscountType")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$DiscountType(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$DiscountType((String) json.getString("DiscountType"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("PrinsipleID")) {
            if (json.isNull("PrinsipleID")) {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PrinsipleID(null);
            } else {
                ((TakingOrderModelRealmProxyInterface) obj).realmSet$PrinsipleID((String) json.getString("PrinsipleID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.TakingOrderModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.TakingOrderModel obj = new com.bhn.sadix.Data.TakingOrderModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("CustId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$CustId(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$CustId((String) reader.nextString());
                }
            } else if (name.equals("SKUId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$SKUId(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$SKUId((String) reader.nextString());
                }
            } else if (name.equals("tanggal")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$tanggal(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$tanggal((String) reader.nextString());
                }
            } else if (name.equals("SalesProgramId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$SalesProgramId(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$SalesProgramId((String) reader.nextString());
                }
            } else if (name.equals("QTY_B")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_B' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$QTY_B((int) reader.nextInt());
                }
            } else if (name.equals("QTY_K")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'QTY_K' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$QTY_K((int) reader.nextInt());
                }
            } else if (name.equals("DISCOUNT1")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT1' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT1((double) reader.nextDouble());
                }
            } else if (name.equals("DISCOUNT2")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT2' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT2((double) reader.nextDouble());
                }
            } else if (name.equals("DISCOUNT3")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'DISCOUNT3' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$DISCOUNT3((double) reader.nextDouble());
                }
            } else if (name.equals("TOTAL_PRICE")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'TOTAL_PRICE' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$TOTAL_PRICE((double) reader.nextDouble());
                }
            } else if (name.equals("PAYMENT_TERM")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PAYMENT_TERM(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PAYMENT_TERM((String) reader.nextString());
                }
            } else if (name.equals("note")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$note(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$note((String) reader.nextString());
                }
            } else if (name.equals("PRICE_B")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE_B' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PRICE_B((double) reader.nextDouble());
                }
            } else if (name.equals("PRICE_K")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'PRICE_K' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PRICE_K((double) reader.nextDouble());
                }
            } else if (name.equals("LAMA_KREDIT")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'LAMA_KREDIT' to null.");
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$LAMA_KREDIT((int) reader.nextInt());
                }
            } else if (name.equals("DiscountType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$DiscountType(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$DiscountType((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("PrinsipleID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PrinsipleID(null);
                } else {
                    ((TakingOrderModelRealmProxyInterface) obj).realmSet$PrinsipleID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.TakingOrderModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.TakingOrderModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.TakingOrderModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.TakingOrderModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((TakingOrderModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.TakingOrderModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.TakingOrderModel copy(Realm realm, com.bhn.sadix.Data.TakingOrderModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.TakingOrderModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.TakingOrderModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.TakingOrderModel.class, ((TakingOrderModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$CustId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$CustId());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$SKUId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$SKUId());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$tanggal(((TakingOrderModelRealmProxyInterface) newObject).realmGet$tanggal());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$SalesProgramId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$SalesProgramId());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$QTY_B(((TakingOrderModelRealmProxyInterface) newObject).realmGet$QTY_B());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$QTY_K(((TakingOrderModelRealmProxyInterface) newObject).realmGet$QTY_K());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT1(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT1());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT2(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT2());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT3(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT3());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$TOTAL_PRICE(((TakingOrderModelRealmProxyInterface) newObject).realmGet$TOTAL_PRICE());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PAYMENT_TERM(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PAYMENT_TERM());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$note(((TakingOrderModelRealmProxyInterface) newObject).realmGet$note());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PRICE_B(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PRICE_B());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PRICE_K(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PRICE_K());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$LAMA_KREDIT(((TakingOrderModelRealmProxyInterface) newObject).realmGet$LAMA_KREDIT());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DiscountType(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DiscountType());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$CommonID(((TakingOrderModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PrinsipleID(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PrinsipleID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.TakingOrderModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        TakingOrderModelColumnInfo columnInfo = (TakingOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((TakingOrderModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((TakingOrderModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        }
        String realmGet$SKUId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        }
        String realmGet$tanggal = ((TakingOrderModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        }
        String realmGet$SalesProgramId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SalesProgramId();
        if (realmGet$SalesProgramId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, realmGet$SalesProgramId, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT1Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT1(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT2Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT2(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT3Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT3(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.TOTAL_PRICEIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$TOTAL_PRICE(), false);
        String realmGet$PAYMENT_TERM = ((TakingOrderModelRealmProxyInterface)object).realmGet$PAYMENT_TERM();
        if (realmGet$PAYMENT_TERM != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, realmGet$PAYMENT_TERM, false);
        }
        String realmGet$note = ((TakingOrderModelRealmProxyInterface)object).realmGet$note();
        if (realmGet$note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
        }
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_B(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_K(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.LAMA_KREDITIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$LAMA_KREDIT(), false);
        String realmGet$DiscountType = ((TakingOrderModelRealmProxyInterface)object).realmGet$DiscountType();
        if (realmGet$DiscountType != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, realmGet$DiscountType, false);
        }
        String realmGet$CommonID = ((TakingOrderModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$PrinsipleID = ((TakingOrderModelRealmProxyInterface)object).realmGet$PrinsipleID();
        if (realmGet$PrinsipleID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, realmGet$PrinsipleID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        TakingOrderModelColumnInfo columnInfo = (TakingOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.TakingOrderModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.TakingOrderModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((TakingOrderModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((TakingOrderModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                }
                String realmGet$SKUId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                }
                String realmGet$tanggal = ((TakingOrderModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                }
                String realmGet$SalesProgramId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SalesProgramId();
                if (realmGet$SalesProgramId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, realmGet$SalesProgramId, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT1Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT1(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT2Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT2(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT3Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT3(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.TOTAL_PRICEIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$TOTAL_PRICE(), false);
                String realmGet$PAYMENT_TERM = ((TakingOrderModelRealmProxyInterface)object).realmGet$PAYMENT_TERM();
                if (realmGet$PAYMENT_TERM != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, realmGet$PAYMENT_TERM, false);
                }
                String realmGet$note = ((TakingOrderModelRealmProxyInterface)object).realmGet$note();
                if (realmGet$note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
                }
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_B(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_K(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.LAMA_KREDITIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$LAMA_KREDIT(), false);
                String realmGet$DiscountType = ((TakingOrderModelRealmProxyInterface)object).realmGet$DiscountType();
                if (realmGet$DiscountType != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, realmGet$DiscountType, false);
                }
                String realmGet$CommonID = ((TakingOrderModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$PrinsipleID = ((TakingOrderModelRealmProxyInterface)object).realmGet$PrinsipleID();
                if (realmGet$PrinsipleID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, realmGet$PrinsipleID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.TakingOrderModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        TakingOrderModelColumnInfo columnInfo = (TakingOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((TakingOrderModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$CustId = ((TakingOrderModelRealmProxyInterface)object).realmGet$CustId();
        if (realmGet$CustId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
        }
        String realmGet$SKUId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
        }
        String realmGet$tanggal = ((TakingOrderModelRealmProxyInterface)object).realmGet$tanggal();
        if (realmGet$tanggal != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
        }
        String realmGet$SalesProgramId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SalesProgramId();
        if (realmGet$SalesProgramId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, realmGet$SalesProgramId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_B(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_K(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT1Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT1(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT2Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT2(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT3Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT3(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.TOTAL_PRICEIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$TOTAL_PRICE(), false);
        String realmGet$PAYMENT_TERM = ((TakingOrderModelRealmProxyInterface)object).realmGet$PAYMENT_TERM();
        if (realmGet$PAYMENT_TERM != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, realmGet$PAYMENT_TERM, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, false);
        }
        String realmGet$note = ((TakingOrderModelRealmProxyInterface)object).realmGet$note();
        if (realmGet$note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.noteIndex, rowIndex, false);
        }
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_B(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_K(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.LAMA_KREDITIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$LAMA_KREDIT(), false);
        String realmGet$DiscountType = ((TakingOrderModelRealmProxyInterface)object).realmGet$DiscountType();
        if (realmGet$DiscountType != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, realmGet$DiscountType, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((TakingOrderModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$PrinsipleID = ((TakingOrderModelRealmProxyInterface)object).realmGet$PrinsipleID();
        if (realmGet$PrinsipleID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, realmGet$PrinsipleID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.TakingOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        TakingOrderModelColumnInfo columnInfo = (TakingOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.TakingOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.TakingOrderModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.TakingOrderModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((TakingOrderModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$CustId = ((TakingOrderModelRealmProxyInterface)object).realmGet$CustId();
                if (realmGet$CustId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustIdIndex, rowIndex, realmGet$CustId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustIdIndex, rowIndex, false);
                }
                String realmGet$SKUId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
                }
                String realmGet$tanggal = ((TakingOrderModelRealmProxyInterface)object).realmGet$tanggal();
                if (realmGet$tanggal != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.tanggalIndex, rowIndex, realmGet$tanggal, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.tanggalIndex, rowIndex, false);
                }
                String realmGet$SalesProgramId = ((TakingOrderModelRealmProxyInterface)object).realmGet$SalesProgramId();
                if (realmGet$SalesProgramId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, realmGet$SalesProgramId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SalesProgramIdIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_B(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.QTY_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$QTY_K(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT1Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT1(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT2Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT2(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.DISCOUNT3Index, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$DISCOUNT3(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.TOTAL_PRICEIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$TOTAL_PRICE(), false);
                String realmGet$PAYMENT_TERM = ((TakingOrderModelRealmProxyInterface)object).realmGet$PAYMENT_TERM();
                if (realmGet$PAYMENT_TERM != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, realmGet$PAYMENT_TERM, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.PAYMENT_TERMIndex, rowIndex, false);
                }
                String realmGet$note = ((TakingOrderModelRealmProxyInterface)object).realmGet$note();
                if (realmGet$note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.noteIndex, rowIndex, realmGet$note, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.noteIndex, rowIndex, false);
                }
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_BIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_B(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.PRICE_KIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$PRICE_K(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.LAMA_KREDITIndex, rowIndex, ((TakingOrderModelRealmProxyInterface)object).realmGet$LAMA_KREDIT(), false);
                String realmGet$DiscountType = ((TakingOrderModelRealmProxyInterface)object).realmGet$DiscountType();
                if (realmGet$DiscountType != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, realmGet$DiscountType, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.DiscountTypeIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((TakingOrderModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$PrinsipleID = ((TakingOrderModelRealmProxyInterface)object).realmGet$PrinsipleID();
                if (realmGet$PrinsipleID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, realmGet$PrinsipleID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.TakingOrderModel createDetachedCopy(com.bhn.sadix.Data.TakingOrderModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.TakingOrderModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.TakingOrderModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.TakingOrderModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.TakingOrderModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$CustId(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$CustId());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$SKUId(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$SKUId());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$tanggal(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$tanggal());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$SalesProgramId(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$SalesProgramId());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$QTY_B(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$QTY_B());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$QTY_K(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$QTY_K());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$DISCOUNT1(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$DISCOUNT1());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$DISCOUNT2(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$DISCOUNT2());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$DISCOUNT3(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$DISCOUNT3());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$TOTAL_PRICE(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$TOTAL_PRICE());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$PAYMENT_TERM(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$PAYMENT_TERM());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$note(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$note());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$PRICE_B(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$PRICE_B());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$PRICE_K(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$PRICE_K());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$LAMA_KREDIT(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$LAMA_KREDIT());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$DiscountType(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$DiscountType());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$RandomID());
        ((TakingOrderModelRealmProxyInterface) unmanagedObject).realmSet$PrinsipleID(((TakingOrderModelRealmProxyInterface) realmObject).realmGet$PrinsipleID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.TakingOrderModel update(Realm realm, com.bhn.sadix.Data.TakingOrderModel realmObject, com.bhn.sadix.Data.TakingOrderModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$CustId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$CustId());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$SKUId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$SKUId());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$tanggal(((TakingOrderModelRealmProxyInterface) newObject).realmGet$tanggal());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$SalesProgramId(((TakingOrderModelRealmProxyInterface) newObject).realmGet$SalesProgramId());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$QTY_B(((TakingOrderModelRealmProxyInterface) newObject).realmGet$QTY_B());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$QTY_K(((TakingOrderModelRealmProxyInterface) newObject).realmGet$QTY_K());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT1(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT1());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT2(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT2());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DISCOUNT3(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DISCOUNT3());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$TOTAL_PRICE(((TakingOrderModelRealmProxyInterface) newObject).realmGet$TOTAL_PRICE());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PAYMENT_TERM(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PAYMENT_TERM());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$note(((TakingOrderModelRealmProxyInterface) newObject).realmGet$note());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PRICE_B(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PRICE_B());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PRICE_K(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PRICE_K());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$LAMA_KREDIT(((TakingOrderModelRealmProxyInterface) newObject).realmGet$LAMA_KREDIT());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$DiscountType(((TakingOrderModelRealmProxyInterface) newObject).realmGet$DiscountType());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$CommonID(((TakingOrderModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((TakingOrderModelRealmProxyInterface) realmObject).realmSet$PrinsipleID(((TakingOrderModelRealmProxyInterface) newObject).realmGet$PrinsipleID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("TakingOrderModel = [");
        stringBuilder.append("{CustId:");
        stringBuilder.append(realmGet$CustId() != null ? realmGet$CustId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SKUId:");
        stringBuilder.append(realmGet$SKUId() != null ? realmGet$SKUId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tanggal:");
        stringBuilder.append(realmGet$tanggal() != null ? realmGet$tanggal() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SalesProgramId:");
        stringBuilder.append(realmGet$SalesProgramId() != null ? realmGet$SalesProgramId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_B:");
        stringBuilder.append(realmGet$QTY_B());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{QTY_K:");
        stringBuilder.append(realmGet$QTY_K());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DISCOUNT1:");
        stringBuilder.append(realmGet$DISCOUNT1());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DISCOUNT2:");
        stringBuilder.append(realmGet$DISCOUNT2());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DISCOUNT3:");
        stringBuilder.append(realmGet$DISCOUNT3());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{TOTAL_PRICE:");
        stringBuilder.append(realmGet$TOTAL_PRICE());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PAYMENT_TERM:");
        stringBuilder.append(realmGet$PAYMENT_TERM() != null ? realmGet$PAYMENT_TERM() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{note:");
        stringBuilder.append(realmGet$note() != null ? realmGet$note() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PRICE_B:");
        stringBuilder.append(realmGet$PRICE_B());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PRICE_K:");
        stringBuilder.append(realmGet$PRICE_K());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{LAMA_KREDIT:");
        stringBuilder.append(realmGet$LAMA_KREDIT());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{DiscountType:");
        stringBuilder.append(realmGet$DiscountType() != null ? realmGet$DiscountType() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PrinsipleID:");
        stringBuilder.append(realmGet$PrinsipleID() != null ? realmGet$PrinsipleID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

}
