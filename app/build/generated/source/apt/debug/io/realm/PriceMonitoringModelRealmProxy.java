package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PriceMonitoringModelRealmProxy extends com.bhn.sadix.Data.PriceMonitoringModel
    implements RealmObjectProxy, PriceMonitoringModelRealmProxyInterface {

    static final class PriceMonitoringModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long SKUIdIndex;
        public long ProductIDIndex;
        public long ProductNameIndex;
        public long QtyIndex;
        public long BuyPriceIndex;
        public long SellPriceIndex;
        public long LevelIDIndex;
        public long NoteIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        PriceMonitoringModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(10);
            this.SKUIdIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "SKUId");
            indicesMap.put("SKUId", this.SKUIdIndex);
            this.ProductIDIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "ProductID");
            indicesMap.put("ProductID", this.ProductIDIndex);
            this.ProductNameIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "ProductName");
            indicesMap.put("ProductName", this.ProductNameIndex);
            this.QtyIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "Qty");
            indicesMap.put("Qty", this.QtyIndex);
            this.BuyPriceIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "BuyPrice");
            indicesMap.put("BuyPrice", this.BuyPriceIndex);
            this.SellPriceIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "SellPrice");
            indicesMap.put("SellPrice", this.SellPriceIndex);
            this.LevelIDIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "LevelID");
            indicesMap.put("LevelID", this.LevelIDIndex);
            this.NoteIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "Note");
            indicesMap.put("Note", this.NoteIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "PriceMonitoringModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final PriceMonitoringModelColumnInfo otherInfo = (PriceMonitoringModelColumnInfo) other;
            this.SKUIdIndex = otherInfo.SKUIdIndex;
            this.ProductIDIndex = otherInfo.ProductIDIndex;
            this.ProductNameIndex = otherInfo.ProductNameIndex;
            this.QtyIndex = otherInfo.QtyIndex;
            this.BuyPriceIndex = otherInfo.BuyPriceIndex;
            this.SellPriceIndex = otherInfo.SellPriceIndex;
            this.LevelIDIndex = otherInfo.LevelIDIndex;
            this.NoteIndex = otherInfo.NoteIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final PriceMonitoringModelColumnInfo clone() {
            return (PriceMonitoringModelColumnInfo) super.clone();
        }

    }
    private PriceMonitoringModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.PriceMonitoringModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("SKUId");
        fieldNames.add("ProductID");
        fieldNames.add("ProductName");
        fieldNames.add("Qty");
        fieldNames.add("BuyPrice");
        fieldNames.add("SellPrice");
        fieldNames.add("LevelID");
        fieldNames.add("Note");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    PriceMonitoringModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (PriceMonitoringModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.PriceMonitoringModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$SKUId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SKUIdIndex);
    }

    public void realmSet$SKUId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SKUIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SKUIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SKUIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SKUIdIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$ProductID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.ProductIDIndex);
    }

    public void realmSet$ProductID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.ProductIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.ProductIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$ProductName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ProductNameIndex);
    }

    public void realmSet$ProductName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.ProductNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.ProductNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.ProductNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.ProductNameIndex, value);
    }

    @SuppressWarnings("cast")
    public long realmGet$Qty() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.QtyIndex);
    }

    public void realmSet$Qty(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.QtyIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.QtyIndex, value);
    }

    @SuppressWarnings("cast")
    public long realmGet$BuyPrice() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.BuyPriceIndex);
    }

    public void realmSet$BuyPrice(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.BuyPriceIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.BuyPriceIndex, value);
    }

    @SuppressWarnings("cast")
    public long realmGet$SellPrice() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.SellPriceIndex);
    }

    public void realmSet$SellPrice(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.SellPriceIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.SellPriceIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$LevelID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.LevelIDIndex);
    }

    public void realmSet$LevelID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.LevelIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.LevelIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Note() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NoteIndex);
    }

    public void realmSet$Note(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NoteIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NoteIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NoteIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NoteIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("PriceMonitoringModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("PriceMonitoringModel");
            realmObjectSchema.add(new Property("SKUId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("ProductID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("ProductName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Qty", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("BuyPrice", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("SellPrice", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("LevelID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Note", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("PriceMonitoringModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_PriceMonitoringModel")) {
            Table table = sharedRealm.getTable("class_PriceMonitoringModel");
            table.addColumn(RealmFieldType.STRING, "SKUId", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "ProductID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "ProductName", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Qty", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "BuyPrice", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "SellPrice", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "LevelID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Note", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_PriceMonitoringModel");
    }

    public static PriceMonitoringModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_PriceMonitoringModel")) {
            Table table = sharedRealm.getTable("class_PriceMonitoringModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 10) {
                if (columnCount < 10) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 10 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 10 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 10 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final PriceMonitoringModelColumnInfo columnInfo = new PriceMonitoringModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("SKUId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SKUId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SKUId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SKUId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SKUIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SKUId' is required. Either set @Required to field 'SKUId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ProductID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ProductID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ProductID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'ProductID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.ProductIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ProductID' does support null values in the existing Realm file. Use corresponding boxed type for field 'ProductID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("ProductName")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'ProductName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("ProductName") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'ProductName' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.ProductNameIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'ProductName' is required. Either set @Required to field 'ProductName' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Qty")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Qty' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Qty") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'long' for field 'Qty' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.QtyIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Qty' does support null values in the existing Realm file. Use corresponding boxed type for field 'Qty' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("BuyPrice")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'BuyPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("BuyPrice") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'long' for field 'BuyPrice' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.BuyPriceIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'BuyPrice' does support null values in the existing Realm file. Use corresponding boxed type for field 'BuyPrice' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("SellPrice")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SellPrice' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SellPrice") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'long' for field 'SellPrice' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.SellPriceIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SellPrice' does support null values in the existing Realm file. Use corresponding boxed type for field 'SellPrice' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("LevelID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'LevelID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("LevelID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'LevelID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.LevelIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'LevelID' does support null values in the existing Realm file. Use corresponding boxed type for field 'LevelID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Note")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Note' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Note") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Note' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NoteIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Note' is required. Either set @Required to field 'Note' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'PriceMonitoringModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_PriceMonitoringModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.PriceMonitoringModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.PriceMonitoringModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.PriceMonitoringModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.PriceMonitoringModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PriceMonitoringModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.PriceMonitoringModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PriceMonitoringModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("SKUId")) {
            if (json.isNull("SKUId")) {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SKUId(null);
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SKUId((String) json.getString("SKUId"));
            }
        }
        if (json.has("ProductID")) {
            if (json.isNull("ProductID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'ProductID' to null.");
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductID((int) json.getInt("ProductID"));
            }
        }
        if (json.has("ProductName")) {
            if (json.isNull("ProductName")) {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductName(null);
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductName((String) json.getString("ProductName"));
            }
        }
        if (json.has("Qty")) {
            if (json.isNull("Qty")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Qty' to null.");
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Qty((long) json.getLong("Qty"));
            }
        }
        if (json.has("BuyPrice")) {
            if (json.isNull("BuyPrice")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'BuyPrice' to null.");
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$BuyPrice((long) json.getLong("BuyPrice"));
            }
        }
        if (json.has("SellPrice")) {
            if (json.isNull("SellPrice")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'SellPrice' to null.");
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SellPrice((long) json.getLong("SellPrice"));
            }
        }
        if (json.has("LevelID")) {
            if (json.isNull("LevelID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'LevelID' to null.");
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$LevelID((int) json.getInt("LevelID"));
            }
        }
        if (json.has("Note")) {
            if (json.isNull("Note")) {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Note(null);
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Note((String) json.getString("Note"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.PriceMonitoringModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.PriceMonitoringModel obj = new com.bhn.sadix.Data.PriceMonitoringModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("SKUId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SKUId(null);
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SKUId((String) reader.nextString());
                }
            } else if (name.equals("ProductID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'ProductID' to null.");
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductID((int) reader.nextInt());
                }
            } else if (name.equals("ProductName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductName(null);
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$ProductName((String) reader.nextString());
                }
            } else if (name.equals("Qty")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Qty' to null.");
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Qty((long) reader.nextLong());
                }
            } else if (name.equals("BuyPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'BuyPrice' to null.");
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$BuyPrice((long) reader.nextLong());
                }
            } else if (name.equals("SellPrice")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'SellPrice' to null.");
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$SellPrice((long) reader.nextLong());
                }
            } else if (name.equals("LevelID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'LevelID' to null.");
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$LevelID((int) reader.nextInt());
                }
            } else if (name.equals("Note")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Note(null);
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$Note((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((PriceMonitoringModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.PriceMonitoringModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.PriceMonitoringModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PriceMonitoringModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.PriceMonitoringModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((PriceMonitoringModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.PriceMonitoringModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.PriceMonitoringModel copy(Realm realm, com.bhn.sadix.Data.PriceMonitoringModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PriceMonitoringModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.PriceMonitoringModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.PriceMonitoringModel.class, ((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$SKUId(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$SKUId());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$ProductID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$ProductID());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$ProductName(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$ProductName());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$Qty(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$Qty());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$BuyPrice(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$BuyPrice());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$SellPrice(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$SellPrice());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$LevelID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$LevelID());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$Note(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$Note());
            ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$CommonID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.PriceMonitoringModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PriceMonitoringModelColumnInfo columnInfo = (PriceMonitoringModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((PriceMonitoringModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$SKUId = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.ProductIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductID(), false);
        String realmGet$ProductName = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductName();
        if (realmGet$ProductName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, realmGet$ProductName, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QtyIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Qty(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.BuyPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$BuyPrice(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.SellPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SellPrice(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.LevelIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$LevelID(), false);
        String realmGet$Note = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Note();
        if (realmGet$Note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NoteIndex, rowIndex, realmGet$Note, false);
        }
        String realmGet$CommonID = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PriceMonitoringModelColumnInfo columnInfo = (PriceMonitoringModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PriceMonitoringModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PriceMonitoringModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((PriceMonitoringModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$SKUId = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.ProductIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductID(), false);
                String realmGet$ProductName = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductName();
                if (realmGet$ProductName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, realmGet$ProductName, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QtyIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Qty(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.BuyPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$BuyPrice(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.SellPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SellPrice(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.LevelIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$LevelID(), false);
                String realmGet$Note = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Note();
                if (realmGet$Note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NoteIndex, rowIndex, realmGet$Note, false);
                }
                String realmGet$CommonID = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.PriceMonitoringModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PriceMonitoringModelColumnInfo columnInfo = (PriceMonitoringModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((PriceMonitoringModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$SKUId = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SKUId();
        if (realmGet$SKUId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.ProductIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductID(), false);
        String realmGet$ProductName = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductName();
        if (realmGet$ProductName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, realmGet$ProductName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.QtyIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Qty(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.BuyPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$BuyPrice(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.SellPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SellPrice(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.LevelIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$LevelID(), false);
        String realmGet$Note = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Note();
        if (realmGet$Note != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NoteIndex, rowIndex, realmGet$Note, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NoteIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PriceMonitoringModelColumnInfo columnInfo = (PriceMonitoringModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PriceMonitoringModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PriceMonitoringModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PriceMonitoringModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((PriceMonitoringModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$SKUId = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SKUId();
                if (realmGet$SKUId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, realmGet$SKUId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SKUIdIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.ProductIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductID(), false);
                String realmGet$ProductName = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$ProductName();
                if (realmGet$ProductName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, realmGet$ProductName, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.ProductNameIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.QtyIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Qty(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.BuyPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$BuyPrice(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.SellPriceIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$SellPrice(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.LevelIDIndex, rowIndex, ((PriceMonitoringModelRealmProxyInterface)object).realmGet$LevelID(), false);
                String realmGet$Note = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$Note();
                if (realmGet$Note != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NoteIndex, rowIndex, realmGet$Note, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NoteIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((PriceMonitoringModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.PriceMonitoringModel createDetachedCopy(com.bhn.sadix.Data.PriceMonitoringModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.PriceMonitoringModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.PriceMonitoringModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.PriceMonitoringModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.PriceMonitoringModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$SKUId(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$SKUId());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$ProductID(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$ProductID());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$ProductName(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$ProductName());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$Qty(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$Qty());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$BuyPrice(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$BuyPrice());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$SellPrice(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$SellPrice());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$LevelID(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$LevelID());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$Note(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$Note());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((PriceMonitoringModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((PriceMonitoringModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.PriceMonitoringModel update(Realm realm, com.bhn.sadix.Data.PriceMonitoringModel realmObject, com.bhn.sadix.Data.PriceMonitoringModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$SKUId(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$SKUId());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$ProductID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$ProductID());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$ProductName(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$ProductName());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$Qty(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$Qty());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$BuyPrice(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$BuyPrice());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$SellPrice(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$SellPrice());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$LevelID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$LevelID());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$Note(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$Note());
        ((PriceMonitoringModelRealmProxyInterface) realmObject).realmSet$CommonID(((PriceMonitoringModelRealmProxyInterface) newObject).realmGet$CommonID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("PriceMonitoringModel = [");
        stringBuilder.append("{SKUId:");
        stringBuilder.append(realmGet$SKUId() != null ? realmGet$SKUId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ProductID:");
        stringBuilder.append(realmGet$ProductID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ProductName:");
        stringBuilder.append(realmGet$ProductName() != null ? realmGet$ProductName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Qty:");
        stringBuilder.append(realmGet$Qty());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{BuyPrice:");
        stringBuilder.append(realmGet$BuyPrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SellPrice:");
        stringBuilder.append(realmGet$SellPrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{LevelID:");
        stringBuilder.append(realmGet$LevelID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Note:");
        stringBuilder.append(realmGet$Note() != null ? realmGet$Note() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceMonitoringModelRealmProxy aPriceMonitoringModel = (PriceMonitoringModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aPriceMonitoringModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aPriceMonitoringModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aPriceMonitoringModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
