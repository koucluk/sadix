package io.realm;


public interface TakingOrderModelRealmProxyInterface {
    public String realmGet$CustId();
    public void realmSet$CustId(String value);
    public String realmGet$SKUId();
    public void realmSet$SKUId(String value);
    public String realmGet$tanggal();
    public void realmSet$tanggal(String value);
    public String realmGet$SalesProgramId();
    public void realmSet$SalesProgramId(String value);
    public int realmGet$QTY_B();
    public void realmSet$QTY_B(int value);
    public int realmGet$QTY_K();
    public void realmSet$QTY_K(int value);
    public double realmGet$DISCOUNT1();
    public void realmSet$DISCOUNT1(double value);
    public double realmGet$DISCOUNT2();
    public void realmSet$DISCOUNT2(double value);
    public double realmGet$DISCOUNT3();
    public void realmSet$DISCOUNT3(double value);
    public double realmGet$TOTAL_PRICE();
    public void realmSet$TOTAL_PRICE(double value);
    public String realmGet$PAYMENT_TERM();
    public void realmSet$PAYMENT_TERM(String value);
    public String realmGet$note();
    public void realmSet$note(String value);
    public double realmGet$PRICE_B();
    public void realmSet$PRICE_B(double value);
    public double realmGet$PRICE_K();
    public void realmSet$PRICE_K(double value);
    public int realmGet$LAMA_KREDIT();
    public void realmSet$LAMA_KREDIT(int value);
    public String realmGet$DiscountType();
    public void realmSet$DiscountType(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
    public String realmGet$PrinsipleID();
    public void realmSet$PrinsipleID(String value);
}
