package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PrincipleModelRealmProxy extends com.bhn.sadix.Data.PrincipleModel
    implements RealmObjectProxy, PrincipleModelRealmProxyInterface {

    static final class PrincipleModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long PrinsipleIdIndex;
        public long PrinsipleNameIndex;
        public long RandomIDIndex;
        public long total_qty_bIndex;
        public long total_qty_kIndex;
        public long priceIndex;
        public long discountIndex;
        public long total_priceIndex;
        public long PrinsipleTypeIndex;
        public long CreditLimitIndex;
        public long CreditTOPIndex;
        public long CommonIDIndex;
        public long listIndex;
        public long listPromoIndex;
        public long listPromoResultIndex;

        PrincipleModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(15);
            this.PrinsipleIdIndex = getValidColumnIndex(path, table, "PrincipleModel", "PrinsipleId");
            indicesMap.put("PrinsipleId", this.PrinsipleIdIndex);
            this.PrinsipleNameIndex = getValidColumnIndex(path, table, "PrincipleModel", "PrinsipleName");
            indicesMap.put("PrinsipleName", this.PrinsipleNameIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "PrincipleModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);
            this.total_qty_bIndex = getValidColumnIndex(path, table, "PrincipleModel", "total_qty_b");
            indicesMap.put("total_qty_b", this.total_qty_bIndex);
            this.total_qty_kIndex = getValidColumnIndex(path, table, "PrincipleModel", "total_qty_k");
            indicesMap.put("total_qty_k", this.total_qty_kIndex);
            this.priceIndex = getValidColumnIndex(path, table, "PrincipleModel", "price");
            indicesMap.put("price", this.priceIndex);
            this.discountIndex = getValidColumnIndex(path, table, "PrincipleModel", "discount");
            indicesMap.put("discount", this.discountIndex);
            this.total_priceIndex = getValidColumnIndex(path, table, "PrincipleModel", "total_price");
            indicesMap.put("total_price", this.total_priceIndex);
            this.PrinsipleTypeIndex = getValidColumnIndex(path, table, "PrincipleModel", "PrinsipleType");
            indicesMap.put("PrinsipleType", this.PrinsipleTypeIndex);
            this.CreditLimitIndex = getValidColumnIndex(path, table, "PrincipleModel", "CreditLimit");
            indicesMap.put("CreditLimit", this.CreditLimitIndex);
            this.CreditTOPIndex = getValidColumnIndex(path, table, "PrincipleModel", "CreditTOP");
            indicesMap.put("CreditTOP", this.CreditTOPIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "PrincipleModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.listIndex = getValidColumnIndex(path, table, "PrincipleModel", "list");
            indicesMap.put("list", this.listIndex);
            this.listPromoIndex = getValidColumnIndex(path, table, "PrincipleModel", "listPromo");
            indicesMap.put("listPromo", this.listPromoIndex);
            this.listPromoResultIndex = getValidColumnIndex(path, table, "PrincipleModel", "listPromoResult");
            indicesMap.put("listPromoResult", this.listPromoResultIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final PrincipleModelColumnInfo otherInfo = (PrincipleModelColumnInfo) other;
            this.PrinsipleIdIndex = otherInfo.PrinsipleIdIndex;
            this.PrinsipleNameIndex = otherInfo.PrinsipleNameIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;
            this.total_qty_bIndex = otherInfo.total_qty_bIndex;
            this.total_qty_kIndex = otherInfo.total_qty_kIndex;
            this.priceIndex = otherInfo.priceIndex;
            this.discountIndex = otherInfo.discountIndex;
            this.total_priceIndex = otherInfo.total_priceIndex;
            this.PrinsipleTypeIndex = otherInfo.PrinsipleTypeIndex;
            this.CreditLimitIndex = otherInfo.CreditLimitIndex;
            this.CreditTOPIndex = otherInfo.CreditTOPIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.listIndex = otherInfo.listIndex;
            this.listPromoIndex = otherInfo.listPromoIndex;
            this.listPromoResultIndex = otherInfo.listPromoResultIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final PrincipleModelColumnInfo clone() {
            return (PrincipleModelColumnInfo) super.clone();
        }

    }
    private PrincipleModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.PrincipleModel> proxyState;
    private RealmList<com.bhn.sadix.Data.TakingOrderModel> listRealmList;
    private RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoRealmList;
    private RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultRealmList;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("PrinsipleId");
        fieldNames.add("PrinsipleName");
        fieldNames.add("RandomID");
        fieldNames.add("total_qty_b");
        fieldNames.add("total_qty_k");
        fieldNames.add("price");
        fieldNames.add("discount");
        fieldNames.add("total_price");
        fieldNames.add("PrinsipleType");
        fieldNames.add("CreditLimit");
        fieldNames.add("CreditTOP");
        fieldNames.add("CommonID");
        fieldNames.add("list");
        fieldNames.add("listPromo");
        fieldNames.add("listPromoResult");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    PrincipleModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (PrincipleModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.PrincipleModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$PrinsipleId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.PrinsipleIdIndex);
    }

    public void realmSet$PrinsipleId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.PrinsipleIdIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.PrinsipleIdIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.PrinsipleIdIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.PrinsipleIdIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$PrinsipleName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.PrinsipleNameIndex);
    }

    public void realmSet$PrinsipleName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.PrinsipleNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.PrinsipleNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.PrinsipleNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.PrinsipleNameIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public int realmGet$total_qty_b() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.total_qty_bIndex);
    }

    public void realmSet$total_qty_b(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.total_qty_bIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.total_qty_bIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$total_qty_k() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.total_qty_kIndex);
    }

    public void realmSet$total_qty_k(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.total_qty_kIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.total_qty_kIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$price() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.priceIndex);
    }

    public void realmSet$price(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.priceIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.priceIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$discount() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.discountIndex);
    }

    public void realmSet$discount(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.discountIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.discountIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$total_price() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.total_priceIndex);
    }

    public void realmSet$total_price(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.total_priceIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.total_priceIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$PrinsipleType() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.PrinsipleTypeIndex);
    }

    public void realmSet$PrinsipleType(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.PrinsipleTypeIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.PrinsipleTypeIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$CreditLimit() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.CreditLimitIndex);
    }

    public void realmSet$CreditLimit(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.CreditLimitIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.CreditLimitIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$CreditTOP() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.CreditTOPIndex);
    }

    public void realmSet$CreditTOP(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.CreditTOPIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.CreditTOPIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    public RealmList<com.bhn.sadix.Data.TakingOrderModel> realmGet$list() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (listRealmList != null) {
            return listRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.listIndex);
            listRealmList = new RealmList<com.bhn.sadix.Data.TakingOrderModel>(com.bhn.sadix.Data.TakingOrderModel.class, linkView, proxyState.getRealm$realm());
            return listRealmList;
        }
    }

    public void realmSet$list(RealmList<com.bhn.sadix.Data.TakingOrderModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("list")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.TakingOrderModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.TakingOrderModel>();
                for (com.bhn.sadix.Data.TakingOrderModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.listIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.DiscountPromoModel> realmGet$listPromo() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (listPromoRealmList != null) {
            return listPromoRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.listPromoIndex);
            listPromoRealmList = new RealmList<com.bhn.sadix.Data.DiscountPromoModel>(com.bhn.sadix.Data.DiscountPromoModel.class, linkView, proxyState.getRealm$realm());
            return listPromoRealmList;
        }
    }

    public void realmSet$listPromo(RealmList<com.bhn.sadix.Data.DiscountPromoModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("listPromo")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.DiscountPromoModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.DiscountPromoModel>();
                for (com.bhn.sadix.Data.DiscountPromoModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.listPromoIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public RealmList<com.bhn.sadix.Data.DiscountResultModel> realmGet$listPromoResult() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (listPromoResultRealmList != null) {
            return listPromoResultRealmList;
        } else {
            LinkView linkView = proxyState.getRow$realm().getLinkList(columnInfo.listPromoResultIndex);
            listPromoResultRealmList = new RealmList<com.bhn.sadix.Data.DiscountResultModel>(com.bhn.sadix.Data.DiscountResultModel.class, linkView, proxyState.getRealm$realm());
            return listPromoResultRealmList;
        }
    }

    public void realmSet$listPromoResult(RealmList<com.bhn.sadix.Data.DiscountResultModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("listPromoResult")) {
                return;
            }
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.bhn.sadix.Data.DiscountResultModel> original = value;
                value = new RealmList<com.bhn.sadix.Data.DiscountResultModel>();
                for (com.bhn.sadix.Data.DiscountResultModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        LinkView links = proxyState.getRow$realm().getLinkList(columnInfo.listPromoResultIndex);
        links.clear();
        if (value == null) {
            return;
        }
        for (RealmModel linkedObject : (RealmList<? extends RealmModel>) value) {
            if (!(RealmObject.isManaged(linkedObject) && RealmObject.isValid(linkedObject))) {
                throw new IllegalArgumentException("Each element of 'value' must be a valid managed object.");
            }
            if (((RealmObjectProxy)linkedObject).realmGet$proxyState().getRealm$realm() != proxyState.getRealm$realm()) {
                throw new IllegalArgumentException("Each element of 'value' must belong to the same Realm.");
            }
            links.add(((RealmObjectProxy)linkedObject).realmGet$proxyState().getRow$realm().getIndex());
        }
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("PrincipleModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("PrincipleModel");
            realmObjectSchema.add(new Property("PrinsipleId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("PrinsipleName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("total_qty_b", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("total_qty_k", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("price", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("discount", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("total_price", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("PrinsipleType", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CreditLimit", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CreditTOP", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            if (!realmSchema.contains("TakingOrderModel")) {
                TakingOrderModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("list", RealmFieldType.LIST, realmSchema.get("TakingOrderModel")));
            if (!realmSchema.contains("DiscountPromoModel")) {
                DiscountPromoModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("listPromo", RealmFieldType.LIST, realmSchema.get("DiscountPromoModel")));
            if (!realmSchema.contains("DiscountResultModel")) {
                DiscountResultModelRealmProxy.createRealmObjectSchema(realmSchema);
            }
            realmObjectSchema.add(new Property("listPromoResult", RealmFieldType.LIST, realmSchema.get("DiscountResultModel")));
            return realmObjectSchema;
        }
        return realmSchema.get("PrincipleModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_PrincipleModel")) {
            Table table = sharedRealm.getTable("class_PrincipleModel");
            table.addColumn(RealmFieldType.STRING, "PrinsipleId", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "PrinsipleName", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "total_qty_b", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "total_qty_k", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "price", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "discount", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "total_price", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "PrinsipleType", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "CreditLimit", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "CreditTOP", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            if (!sharedRealm.hasTable("class_TakingOrderModel")) {
                TakingOrderModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "list", sharedRealm.getTable("class_TakingOrderModel"));
            if (!sharedRealm.hasTable("class_DiscountPromoModel")) {
                DiscountPromoModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "listPromo", sharedRealm.getTable("class_DiscountPromoModel"));
            if (!sharedRealm.hasTable("class_DiscountResultModel")) {
                DiscountResultModelRealmProxy.initTable(sharedRealm);
            }
            table.addColumnLink(RealmFieldType.LIST, "listPromoResult", sharedRealm.getTable("class_DiscountResultModel"));
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_PrincipleModel");
    }

    public static PrincipleModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_PrincipleModel")) {
            Table table = sharedRealm.getTable("class_PrincipleModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 15) {
                if (columnCount < 15) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 15 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 15 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 15 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final PrincipleModelColumnInfo columnInfo = new PrincipleModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("PrinsipleId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PrinsipleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PrinsipleId") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'PrinsipleId' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.PrinsipleIdIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PrinsipleId' is required. Either set @Required to field 'PrinsipleId' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PrinsipleName")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PrinsipleName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PrinsipleName") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'PrinsipleName' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.PrinsipleNameIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PrinsipleName' is required. Either set @Required to field 'PrinsipleName' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("total_qty_b")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'total_qty_b' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("total_qty_b") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'total_qty_b' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.total_qty_bIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'total_qty_b' does support null values in the existing Realm file. Use corresponding boxed type for field 'total_qty_b' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("total_qty_k")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'total_qty_k' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("total_qty_k") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'total_qty_k' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.total_qty_kIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'total_qty_k' does support null values in the existing Realm file. Use corresponding boxed type for field 'total_qty_k' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("price")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'price' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("price") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'price' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.priceIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'price' does support null values in the existing Realm file. Use corresponding boxed type for field 'price' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("discount")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'discount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("discount") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'discount' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.discountIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'discount' does support null values in the existing Realm file. Use corresponding boxed type for field 'discount' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("total_price")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'total_price' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("total_price") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'total_price' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.total_priceIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'total_price' does support null values in the existing Realm file. Use corresponding boxed type for field 'total_price' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PrinsipleType")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PrinsipleType' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PrinsipleType") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'PrinsipleType' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PrinsipleTypeIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PrinsipleType' does support null values in the existing Realm file. Use corresponding boxed type for field 'PrinsipleType' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CreditLimit")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CreditLimit' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CreditLimit") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'CreditLimit' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.CreditLimitIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CreditLimit' does support null values in the existing Realm file. Use corresponding boxed type for field 'CreditLimit' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CreditTOP")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CreditTOP' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CreditTOP") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'CreditTOP' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.CreditTOPIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CreditTOP' does support null values in the existing Realm file. Use corresponding boxed type for field 'CreditTOP' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("list")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'list'");
            }
            if (columnTypes.get("list") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'TakingOrderModel' for field 'list'");
            }
            if (!sharedRealm.hasTable("class_TakingOrderModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_TakingOrderModel' for field 'list'");
            }
            Table table_12 = sharedRealm.getTable("class_TakingOrderModel");
            if (!table.getLinkTarget(columnInfo.listIndex).hasSameSchema(table_12)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'list': '" + table.getLinkTarget(columnInfo.listIndex).getName() + "' expected - was '" + table_12.getName() + "'");
            }
            if (!columnTypes.containsKey("listPromo")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'listPromo'");
            }
            if (columnTypes.get("listPromo") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'DiscountPromoModel' for field 'listPromo'");
            }
            if (!sharedRealm.hasTable("class_DiscountPromoModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_DiscountPromoModel' for field 'listPromo'");
            }
            Table table_13 = sharedRealm.getTable("class_DiscountPromoModel");
            if (!table.getLinkTarget(columnInfo.listPromoIndex).hasSameSchema(table_13)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'listPromo': '" + table.getLinkTarget(columnInfo.listPromoIndex).getName() + "' expected - was '" + table_13.getName() + "'");
            }
            if (!columnTypes.containsKey("listPromoResult")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'listPromoResult'");
            }
            if (columnTypes.get("listPromoResult") != RealmFieldType.LIST) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'DiscountResultModel' for field 'listPromoResult'");
            }
            if (!sharedRealm.hasTable("class_DiscountResultModel")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing class 'class_DiscountResultModel' for field 'listPromoResult'");
            }
            Table table_14 = sharedRealm.getTable("class_DiscountResultModel");
            if (!table.getLinkTarget(columnInfo.listPromoResultIndex).hasSameSchema(table_14)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid RealmList type for field 'listPromoResult': '" + table.getLinkTarget(columnInfo.listPromoResultIndex).getName() + "' expected - was '" + table_14.getName() + "'");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'PrincipleModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_PrincipleModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.PrincipleModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(3);
        com.bhn.sadix.Data.PrincipleModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.PrincipleModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("list")) {
                excludeFields.add("list");
            }
            if (json.has("listPromo")) {
                excludeFields.add("listPromo");
            }
            if (json.has("listPromoResult")) {
                excludeFields.add("listPromoResult");
            }
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.PrincipleModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PrincipleModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.PrincipleModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PrincipleModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("PrinsipleId")) {
            if (json.isNull("PrinsipleId")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleId(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleId((String) json.getString("PrinsipleId"));
            }
        }
        if (json.has("PrinsipleName")) {
            if (json.isNull("PrinsipleName")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleName(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleName((String) json.getString("PrinsipleName"));
            }
        }
        if (json.has("total_qty_b")) {
            if (json.isNull("total_qty_b")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total_qty_b' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$total_qty_b((int) json.getInt("total_qty_b"));
            }
        }
        if (json.has("total_qty_k")) {
            if (json.isNull("total_qty_k")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total_qty_k' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$total_qty_k((int) json.getInt("total_qty_k"));
            }
        }
        if (json.has("price")) {
            if (json.isNull("price")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'price' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$price((double) json.getDouble("price"));
            }
        }
        if (json.has("discount")) {
            if (json.isNull("discount")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'discount' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$discount((double) json.getDouble("discount"));
            }
        }
        if (json.has("total_price")) {
            if (json.isNull("total_price")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total_price' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$total_price((double) json.getDouble("total_price"));
            }
        }
        if (json.has("PrinsipleType")) {
            if (json.isNull("PrinsipleType")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'PrinsipleType' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleType((int) json.getInt("PrinsipleType"));
            }
        }
        if (json.has("CreditLimit")) {
            if (json.isNull("CreditLimit")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'CreditLimit' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$CreditLimit((double) json.getDouble("CreditLimit"));
            }
        }
        if (json.has("CreditTOP")) {
            if (json.isNull("CreditTOP")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'CreditTOP' to null.");
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$CreditTOP((double) json.getDouble("CreditTOP"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("list")) {
            if (json.isNull("list")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$list(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmGet$list().clear();
                JSONArray array = json.getJSONArray("list");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((PrincipleModelRealmProxyInterface) obj).realmGet$list().add(item);
                }
            }
        }
        if (json.has("listPromo")) {
            if (json.isNull("listPromo")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromo(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromo().clear();
                JSONArray array = json.getJSONArray("listPromo");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.DiscountPromoModel item = DiscountPromoModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromo().add(item);
                }
            }
        }
        if (json.has("listPromoResult")) {
            if (json.isNull("listPromoResult")) {
                ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromoResult(null);
            } else {
                ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromoResult().clear();
                JSONArray array = json.getJSONArray("listPromoResult");
                for (int i = 0; i < array.length(); i++) {
                    com.bhn.sadix.Data.DiscountResultModel item = DiscountResultModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromoResult().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.PrincipleModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.PrincipleModel obj = new com.bhn.sadix.Data.PrincipleModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("PrinsipleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleId(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleId((String) reader.nextString());
                }
            } else if (name.equals("PrinsipleName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleName(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleName((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("total_qty_b")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total_qty_b' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$total_qty_b((int) reader.nextInt());
                }
            } else if (name.equals("total_qty_k")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total_qty_k' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$total_qty_k((int) reader.nextInt());
                }
            } else if (name.equals("price")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'price' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$price((double) reader.nextDouble());
                }
            } else if (name.equals("discount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'discount' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$discount((double) reader.nextDouble());
                }
            } else if (name.equals("total_price")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total_price' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$total_price((double) reader.nextDouble());
                }
            } else if (name.equals("PrinsipleType")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'PrinsipleType' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$PrinsipleType((int) reader.nextInt());
                }
            } else if (name.equals("CreditLimit")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'CreditLimit' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$CreditLimit((double) reader.nextDouble());
                }
            } else if (name.equals("CreditTOP")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'CreditTOP' to null.");
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$CreditTOP((double) reader.nextDouble());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("list")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$list(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$list(new RealmList<com.bhn.sadix.Data.TakingOrderModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((PrincipleModelRealmProxyInterface) obj).realmGet$list().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("listPromo")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromo(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromo(new RealmList<com.bhn.sadix.Data.DiscountPromoModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.DiscountPromoModel item = DiscountPromoModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromo().add(item);
                    }
                    reader.endArray();
                }
            } else if (name.equals("listPromoResult")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromoResult(null);
                } else {
                    ((PrincipleModelRealmProxyInterface) obj).realmSet$listPromoResult(new RealmList<com.bhn.sadix.Data.DiscountResultModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.bhn.sadix.Data.DiscountResultModel item = DiscountResultModelRealmProxy.createUsingJsonStream(realm, reader);
                        ((PrincipleModelRealmProxyInterface) obj).realmGet$listPromoResult().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.PrincipleModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.PrincipleModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PrincipleModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.PrincipleModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((PrincipleModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.PrincipleModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.PrincipleModel copy(Realm realm, com.bhn.sadix.Data.PrincipleModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PrincipleModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.PrincipleModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.PrincipleModel.class, ((PrincipleModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleId(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleId());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleName(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleName());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_qty_b(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_qty_b());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_qty_k(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_qty_k());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$price(((PrincipleModelRealmProxyInterface) newObject).realmGet$price());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$discount(((PrincipleModelRealmProxyInterface) newObject).realmGet$discount());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_price(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_price());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleType(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleType());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CreditLimit(((PrincipleModelRealmProxyInterface) newObject).realmGet$CreditLimit());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CreditTOP(((PrincipleModelRealmProxyInterface) newObject).realmGet$CreditTOP());
            ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CommonID(((PrincipleModelRealmProxyInterface) newObject).realmGet$CommonID());

            RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$list();
            if (listList != null) {
                RealmList<com.bhn.sadix.Data.TakingOrderModel> listRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$list();
                for (int i = 0; i < listList.size(); i++) {
                    com.bhn.sadix.Data.TakingOrderModel listItem = listList.get(i);
                    com.bhn.sadix.Data.TakingOrderModel cachelist = (com.bhn.sadix.Data.TakingOrderModel) cache.get(listItem);
                    if (cachelist != null) {
                        listRealmList.add(cachelist);
                    } else {
                        listRealmList.add(TakingOrderModelRealmProxy.copyOrUpdate(realm, listList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$listPromo();
            if (listPromoList != null) {
                RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromo();
                for (int i = 0; i < listPromoList.size(); i++) {
                    com.bhn.sadix.Data.DiscountPromoModel listPromoItem = listPromoList.get(i);
                    com.bhn.sadix.Data.DiscountPromoModel cachelistPromo = (com.bhn.sadix.Data.DiscountPromoModel) cache.get(listPromoItem);
                    if (cachelistPromo != null) {
                        listPromoRealmList.add(cachelistPromo);
                    } else {
                        listPromoRealmList.add(DiscountPromoModelRealmProxy.copyOrUpdate(realm, listPromoList.get(i), update, cache));
                    }
                }
            }


            RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$listPromoResult();
            if (listPromoResultList != null) {
                RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromoResult();
                for (int i = 0; i < listPromoResultList.size(); i++) {
                    com.bhn.sadix.Data.DiscountResultModel listPromoResultItem = listPromoResultList.get(i);
                    com.bhn.sadix.Data.DiscountResultModel cachelistPromoResult = (com.bhn.sadix.Data.DiscountResultModel) cache.get(listPromoResultItem);
                    if (cachelistPromoResult != null) {
                        listPromoResultRealmList.add(cachelistPromoResult);
                    } else {
                        listPromoResultRealmList.add(DiscountResultModelRealmProxy.copyOrUpdate(realm, listPromoResultList.get(i), update, cache));
                    }
                }
            }

            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.PrincipleModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleModelColumnInfo columnInfo = (PrincipleModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((PrincipleModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$PrinsipleId = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleId();
        if (realmGet$PrinsipleId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, realmGet$PrinsipleId, false);
        }
        String realmGet$PrinsipleName = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleName();
        if (realmGet$PrinsipleName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, realmGet$PrinsipleName, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_bIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_b(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_kIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_k(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$price(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.discountIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$discount(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.total_priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_price(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.PrinsipleTypeIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleType(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.CreditLimitIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditLimit(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.CreditTOPIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditTOP(), false);
        String realmGet$CommonID = ((PrincipleModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }

        RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) object).realmGet$list();
        if (listList != null) {
            long listNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listIndex, rowIndex);
            for (com.bhn.sadix.Data.TakingOrderModel listItem : listList) {
                Long cacheItemIndexlist = cache.get(listItem);
                if (cacheItemIndexlist == null) {
                    cacheItemIndexlist = TakingOrderModelRealmProxy.insert(realm, listItem, cache);
                }
                LinkView.nativeAdd(listNativeLinkViewPtr, cacheItemIndexlist);
            }
        }


        RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromo();
        if (listPromoList != null) {
            long listPromoNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoIndex, rowIndex);
            for (com.bhn.sadix.Data.DiscountPromoModel listPromoItem : listPromoList) {
                Long cacheItemIndexlistPromo = cache.get(listPromoItem);
                if (cacheItemIndexlistPromo == null) {
                    cacheItemIndexlistPromo = DiscountPromoModelRealmProxy.insert(realm, listPromoItem, cache);
                }
                LinkView.nativeAdd(listPromoNativeLinkViewPtr, cacheItemIndexlistPromo);
            }
        }


        RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromoResult();
        if (listPromoResultList != null) {
            long listPromoResultNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoResultIndex, rowIndex);
            for (com.bhn.sadix.Data.DiscountResultModel listPromoResultItem : listPromoResultList) {
                Long cacheItemIndexlistPromoResult = cache.get(listPromoResultItem);
                if (cacheItemIndexlistPromoResult == null) {
                    cacheItemIndexlistPromoResult = DiscountResultModelRealmProxy.insert(realm, listPromoResultItem, cache);
                }
                LinkView.nativeAdd(listPromoResultNativeLinkViewPtr, cacheItemIndexlistPromoResult);
            }
        }

        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleModelColumnInfo columnInfo = (PrincipleModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PrincipleModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PrincipleModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((PrincipleModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$PrinsipleId = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleId();
                if (realmGet$PrinsipleId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, realmGet$PrinsipleId, false);
                }
                String realmGet$PrinsipleName = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleName();
                if (realmGet$PrinsipleName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, realmGet$PrinsipleName, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_bIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_b(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_kIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_k(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$price(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.discountIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$discount(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.total_priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_price(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.PrinsipleTypeIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleType(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.CreditLimitIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditLimit(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.CreditTOPIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditTOP(), false);
                String realmGet$CommonID = ((PrincipleModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }

                RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) object).realmGet$list();
                if (listList != null) {
                    long listNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listIndex, rowIndex);
                    for (com.bhn.sadix.Data.TakingOrderModel listItem : listList) {
                        Long cacheItemIndexlist = cache.get(listItem);
                        if (cacheItemIndexlist == null) {
                            cacheItemIndexlist = TakingOrderModelRealmProxy.insert(realm, listItem, cache);
                        }
                        LinkView.nativeAdd(listNativeLinkViewPtr, cacheItemIndexlist);
                    }
                }


                RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromo();
                if (listPromoList != null) {
                    long listPromoNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoIndex, rowIndex);
                    for (com.bhn.sadix.Data.DiscountPromoModel listPromoItem : listPromoList) {
                        Long cacheItemIndexlistPromo = cache.get(listPromoItem);
                        if (cacheItemIndexlistPromo == null) {
                            cacheItemIndexlistPromo = DiscountPromoModelRealmProxy.insert(realm, listPromoItem, cache);
                        }
                        LinkView.nativeAdd(listPromoNativeLinkViewPtr, cacheItemIndexlistPromo);
                    }
                }


                RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromoResult();
                if (listPromoResultList != null) {
                    long listPromoResultNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoResultIndex, rowIndex);
                    for (com.bhn.sadix.Data.DiscountResultModel listPromoResultItem : listPromoResultList) {
                        Long cacheItemIndexlistPromoResult = cache.get(listPromoResultItem);
                        if (cacheItemIndexlistPromoResult == null) {
                            cacheItemIndexlistPromoResult = DiscountResultModelRealmProxy.insert(realm, listPromoResultItem, cache);
                        }
                        LinkView.nativeAdd(listPromoResultNativeLinkViewPtr, cacheItemIndexlistPromoResult);
                    }
                }

            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.PrincipleModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleModelColumnInfo columnInfo = (PrincipleModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((PrincipleModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$PrinsipleId = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleId();
        if (realmGet$PrinsipleId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, realmGet$PrinsipleId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, false);
        }
        String realmGet$PrinsipleName = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleName();
        if (realmGet$PrinsipleName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, realmGet$PrinsipleName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_bIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_b(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_kIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_k(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$price(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.discountIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$discount(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.total_priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_price(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.PrinsipleTypeIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleType(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.CreditLimitIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditLimit(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.CreditTOPIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditTOP(), false);
        String realmGet$CommonID = ((PrincipleModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }

        long listNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listIndex, rowIndex);
        LinkView.nativeClear(listNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) object).realmGet$list();
        if (listList != null) {
            for (com.bhn.sadix.Data.TakingOrderModel listItem : listList) {
                Long cacheItemIndexlist = cache.get(listItem);
                if (cacheItemIndexlist == null) {
                    cacheItemIndexlist = TakingOrderModelRealmProxy.insertOrUpdate(realm, listItem, cache);
                }
                LinkView.nativeAdd(listNativeLinkViewPtr, cacheItemIndexlist);
            }
        }


        long listPromoNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoIndex, rowIndex);
        LinkView.nativeClear(listPromoNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromo();
        if (listPromoList != null) {
            for (com.bhn.sadix.Data.DiscountPromoModel listPromoItem : listPromoList) {
                Long cacheItemIndexlistPromo = cache.get(listPromoItem);
                if (cacheItemIndexlistPromo == null) {
                    cacheItemIndexlistPromo = DiscountPromoModelRealmProxy.insertOrUpdate(realm, listPromoItem, cache);
                }
                LinkView.nativeAdd(listPromoNativeLinkViewPtr, cacheItemIndexlistPromo);
            }
        }


        long listPromoResultNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoResultIndex, rowIndex);
        LinkView.nativeClear(listPromoResultNativeLinkViewPtr);
        RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromoResult();
        if (listPromoResultList != null) {
            for (com.bhn.sadix.Data.DiscountResultModel listPromoResultItem : listPromoResultList) {
                Long cacheItemIndexlistPromoResult = cache.get(listPromoResultItem);
                if (cacheItemIndexlistPromoResult == null) {
                    cacheItemIndexlistPromoResult = DiscountResultModelRealmProxy.insertOrUpdate(realm, listPromoResultItem, cache);
                }
                LinkView.nativeAdd(listPromoResultNativeLinkViewPtr, cacheItemIndexlistPromoResult);
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleModelColumnInfo columnInfo = (PrincipleModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PrincipleModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PrincipleModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((PrincipleModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$PrinsipleId = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleId();
                if (realmGet$PrinsipleId != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, realmGet$PrinsipleId, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleIdIndex, rowIndex, false);
                }
                String realmGet$PrinsipleName = ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleName();
                if (realmGet$PrinsipleName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, realmGet$PrinsipleName, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.PrinsipleNameIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_bIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_b(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_qty_kIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_qty_k(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$price(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.discountIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$discount(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.total_priceIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$total_price(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.PrinsipleTypeIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$PrinsipleType(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.CreditLimitIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditLimit(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.CreditTOPIndex, rowIndex, ((PrincipleModelRealmProxyInterface)object).realmGet$CreditTOP(), false);
                String realmGet$CommonID = ((PrincipleModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }

                long listNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listIndex, rowIndex);
                LinkView.nativeClear(listNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) object).realmGet$list();
                if (listList != null) {
                    for (com.bhn.sadix.Data.TakingOrderModel listItem : listList) {
                        Long cacheItemIndexlist = cache.get(listItem);
                        if (cacheItemIndexlist == null) {
                            cacheItemIndexlist = TakingOrderModelRealmProxy.insertOrUpdate(realm, listItem, cache);
                        }
                        LinkView.nativeAdd(listNativeLinkViewPtr, cacheItemIndexlist);
                    }
                }


                long listPromoNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoIndex, rowIndex);
                LinkView.nativeClear(listPromoNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromo();
                if (listPromoList != null) {
                    for (com.bhn.sadix.Data.DiscountPromoModel listPromoItem : listPromoList) {
                        Long cacheItemIndexlistPromo = cache.get(listPromoItem);
                        if (cacheItemIndexlistPromo == null) {
                            cacheItemIndexlistPromo = DiscountPromoModelRealmProxy.insertOrUpdate(realm, listPromoItem, cache);
                        }
                        LinkView.nativeAdd(listPromoNativeLinkViewPtr, cacheItemIndexlistPromo);
                    }
                }


                long listPromoResultNativeLinkViewPtr = Table.nativeGetLinkView(tableNativePtr, columnInfo.listPromoResultIndex, rowIndex);
                LinkView.nativeClear(listPromoResultNativeLinkViewPtr);
                RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) object).realmGet$listPromoResult();
                if (listPromoResultList != null) {
                    for (com.bhn.sadix.Data.DiscountResultModel listPromoResultItem : listPromoResultList) {
                        Long cacheItemIndexlistPromoResult = cache.get(listPromoResultItem);
                        if (cacheItemIndexlistPromoResult == null) {
                            cacheItemIndexlistPromoResult = DiscountResultModelRealmProxy.insertOrUpdate(realm, listPromoResultItem, cache);
                        }
                        LinkView.nativeAdd(listPromoResultNativeLinkViewPtr, cacheItemIndexlistPromoResult);
                    }
                }

            }
        }
    }

    public static com.bhn.sadix.Data.PrincipleModel createDetachedCopy(com.bhn.sadix.Data.PrincipleModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.PrincipleModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.PrincipleModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.PrincipleModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.PrincipleModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$PrinsipleId(((PrincipleModelRealmProxyInterface) realmObject).realmGet$PrinsipleId());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$PrinsipleName(((PrincipleModelRealmProxyInterface) realmObject).realmGet$PrinsipleName());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((PrincipleModelRealmProxyInterface) realmObject).realmGet$RandomID());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$total_qty_b(((PrincipleModelRealmProxyInterface) realmObject).realmGet$total_qty_b());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$total_qty_k(((PrincipleModelRealmProxyInterface) realmObject).realmGet$total_qty_k());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$price(((PrincipleModelRealmProxyInterface) realmObject).realmGet$price());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$discount(((PrincipleModelRealmProxyInterface) realmObject).realmGet$discount());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$total_price(((PrincipleModelRealmProxyInterface) realmObject).realmGet$total_price());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$PrinsipleType(((PrincipleModelRealmProxyInterface) realmObject).realmGet$PrinsipleType());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$CreditLimit(((PrincipleModelRealmProxyInterface) realmObject).realmGet$CreditLimit());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$CreditTOP(((PrincipleModelRealmProxyInterface) realmObject).realmGet$CreditTOP());
        ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((PrincipleModelRealmProxyInterface) realmObject).realmGet$CommonID());

        // Deep copy of list
        if (currentDepth == maxDepth) {
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$list(null);
        } else {
            RealmList<com.bhn.sadix.Data.TakingOrderModel> managedlistList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$list();
            RealmList<com.bhn.sadix.Data.TakingOrderModel> unmanagedlistList = new RealmList<com.bhn.sadix.Data.TakingOrderModel>();
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$list(unmanagedlistList);
            int nextDepth = currentDepth + 1;
            int size = managedlistList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.TakingOrderModel item = TakingOrderModelRealmProxy.createDetachedCopy(managedlistList.get(i), nextDepth, maxDepth, cache);
                unmanagedlistList.add(item);
            }
        }

        // Deep copy of listPromo
        if (currentDepth == maxDepth) {
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$listPromo(null);
        } else {
            RealmList<com.bhn.sadix.Data.DiscountPromoModel> managedlistPromoList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromo();
            RealmList<com.bhn.sadix.Data.DiscountPromoModel> unmanagedlistPromoList = new RealmList<com.bhn.sadix.Data.DiscountPromoModel>();
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$listPromo(unmanagedlistPromoList);
            int nextDepth = currentDepth + 1;
            int size = managedlistPromoList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.DiscountPromoModel item = DiscountPromoModelRealmProxy.createDetachedCopy(managedlistPromoList.get(i), nextDepth, maxDepth, cache);
                unmanagedlistPromoList.add(item);
            }
        }

        // Deep copy of listPromoResult
        if (currentDepth == maxDepth) {
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$listPromoResult(null);
        } else {
            RealmList<com.bhn.sadix.Data.DiscountResultModel> managedlistPromoResultList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromoResult();
            RealmList<com.bhn.sadix.Data.DiscountResultModel> unmanagedlistPromoResultList = new RealmList<com.bhn.sadix.Data.DiscountResultModel>();
            ((PrincipleModelRealmProxyInterface) unmanagedObject).realmSet$listPromoResult(unmanagedlistPromoResultList);
            int nextDepth = currentDepth + 1;
            int size = managedlistPromoResultList.size();
            for (int i = 0; i < size; i++) {
                com.bhn.sadix.Data.DiscountResultModel item = DiscountResultModelRealmProxy.createDetachedCopy(managedlistPromoResultList.get(i), nextDepth, maxDepth, cache);
                unmanagedlistPromoResultList.add(item);
            }
        }
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.PrincipleModel update(Realm realm, com.bhn.sadix.Data.PrincipleModel realmObject, com.bhn.sadix.Data.PrincipleModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleId(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleId());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleName(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleName());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_qty_b(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_qty_b());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_qty_k(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_qty_k());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$price(((PrincipleModelRealmProxyInterface) newObject).realmGet$price());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$discount(((PrincipleModelRealmProxyInterface) newObject).realmGet$discount());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$total_price(((PrincipleModelRealmProxyInterface) newObject).realmGet$total_price());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$PrinsipleType(((PrincipleModelRealmProxyInterface) newObject).realmGet$PrinsipleType());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CreditLimit(((PrincipleModelRealmProxyInterface) newObject).realmGet$CreditLimit());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CreditTOP(((PrincipleModelRealmProxyInterface) newObject).realmGet$CreditTOP());
        ((PrincipleModelRealmProxyInterface) realmObject).realmSet$CommonID(((PrincipleModelRealmProxyInterface) newObject).realmGet$CommonID());
        RealmList<com.bhn.sadix.Data.TakingOrderModel> listList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$list();
        RealmList<com.bhn.sadix.Data.TakingOrderModel> listRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$list();
        listRealmList.clear();
        if (listList != null) {
            for (int i = 0; i < listList.size(); i++) {
                com.bhn.sadix.Data.TakingOrderModel listItem = listList.get(i);
                com.bhn.sadix.Data.TakingOrderModel cachelist = (com.bhn.sadix.Data.TakingOrderModel) cache.get(listItem);
                if (cachelist != null) {
                    listRealmList.add(cachelist);
                } else {
                    listRealmList.add(TakingOrderModelRealmProxy.copyOrUpdate(realm, listList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$listPromo();
        RealmList<com.bhn.sadix.Data.DiscountPromoModel> listPromoRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromo();
        listPromoRealmList.clear();
        if (listPromoList != null) {
            for (int i = 0; i < listPromoList.size(); i++) {
                com.bhn.sadix.Data.DiscountPromoModel listPromoItem = listPromoList.get(i);
                com.bhn.sadix.Data.DiscountPromoModel cachelistPromo = (com.bhn.sadix.Data.DiscountPromoModel) cache.get(listPromoItem);
                if (cachelistPromo != null) {
                    listPromoRealmList.add(cachelistPromo);
                } else {
                    listPromoRealmList.add(DiscountPromoModelRealmProxy.copyOrUpdate(realm, listPromoList.get(i), true, cache));
                }
            }
        }
        RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultList = ((PrincipleModelRealmProxyInterface) newObject).realmGet$listPromoResult();
        RealmList<com.bhn.sadix.Data.DiscountResultModel> listPromoResultRealmList = ((PrincipleModelRealmProxyInterface) realmObject).realmGet$listPromoResult();
        listPromoResultRealmList.clear();
        if (listPromoResultList != null) {
            for (int i = 0; i < listPromoResultList.size(); i++) {
                com.bhn.sadix.Data.DiscountResultModel listPromoResultItem = listPromoResultList.get(i);
                com.bhn.sadix.Data.DiscountResultModel cachelistPromoResult = (com.bhn.sadix.Data.DiscountResultModel) cache.get(listPromoResultItem);
                if (cachelistPromoResult != null) {
                    listPromoResultRealmList.add(cachelistPromoResult);
                } else {
                    listPromoResultRealmList.add(DiscountResultModelRealmProxy.copyOrUpdate(realm, listPromoResultList.get(i), true, cache));
                }
            }
        }
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("PrincipleModel = [");
        stringBuilder.append("{PrinsipleId:");
        stringBuilder.append(realmGet$PrinsipleId() != null ? realmGet$PrinsipleId() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PrinsipleName:");
        stringBuilder.append(realmGet$PrinsipleName() != null ? realmGet$PrinsipleName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{total_qty_b:");
        stringBuilder.append(realmGet$total_qty_b());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{total_qty_k:");
        stringBuilder.append(realmGet$total_qty_k());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{price:");
        stringBuilder.append(realmGet$price());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{discount:");
        stringBuilder.append(realmGet$discount());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{total_price:");
        stringBuilder.append(realmGet$total_price());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PrinsipleType:");
        stringBuilder.append(realmGet$PrinsipleType());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CreditLimit:");
        stringBuilder.append(realmGet$CreditLimit());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CreditTOP:");
        stringBuilder.append(realmGet$CreditTOP());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{list:");
        stringBuilder.append("RealmList<TakingOrderModel>[").append(realmGet$list().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{listPromo:");
        stringBuilder.append("RealmList<DiscountPromoModel>[").append(realmGet$listPromo().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{listPromoResult:");
        stringBuilder.append("RealmList<DiscountResultModel>[").append(realmGet$listPromoResult().size()).append("]");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrincipleModelRealmProxy aPrincipleModel = (PrincipleModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aPrincipleModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aPrincipleModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aPrincipleModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
