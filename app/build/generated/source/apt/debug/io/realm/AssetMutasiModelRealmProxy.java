package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AssetMutasiModelRealmProxy extends com.bhn.sadix.Data.AssetMutasiModel
    implements RealmObjectProxy, AssetMutasiModelRealmProxyInterface {

    static final class AssetMutasiModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long AsetNameIndex;
        public long AsetIDIndex;
        public long MerkIDIndex;
        public long TypeIDIndex;
        public long RemarkIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;
        public long NoSeriIndex;

        AssetMutasiModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(8);
            this.AsetNameIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "AsetName");
            indicesMap.put("AsetName", this.AsetNameIndex);
            this.AsetIDIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "AsetID");
            indicesMap.put("AsetID", this.AsetIDIndex);
            this.MerkIDIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "MerkID");
            indicesMap.put("MerkID", this.MerkIDIndex);
            this.TypeIDIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "TypeID");
            indicesMap.put("TypeID", this.TypeIDIndex);
            this.RemarkIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "Remark");
            indicesMap.put("Remark", this.RemarkIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);
            this.NoSeriIndex = getValidColumnIndex(path, table, "AssetMutasiModel", "NoSeri");
            indicesMap.put("NoSeri", this.NoSeriIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final AssetMutasiModelColumnInfo otherInfo = (AssetMutasiModelColumnInfo) other;
            this.AsetNameIndex = otherInfo.AsetNameIndex;
            this.AsetIDIndex = otherInfo.AsetIDIndex;
            this.MerkIDIndex = otherInfo.MerkIDIndex;
            this.TypeIDIndex = otherInfo.TypeIDIndex;
            this.RemarkIndex = otherInfo.RemarkIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;
            this.NoSeriIndex = otherInfo.NoSeriIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final AssetMutasiModelColumnInfo clone() {
            return (AssetMutasiModelColumnInfo) super.clone();
        }

    }
    private AssetMutasiModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.AssetMutasiModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("AsetName");
        fieldNames.add("AsetID");
        fieldNames.add("MerkID");
        fieldNames.add("TypeID");
        fieldNames.add("Remark");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        fieldNames.add("NoSeri");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    AssetMutasiModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (AssetMutasiModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.AssetMutasiModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$AsetName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.AsetNameIndex);
    }

    public void realmSet$AsetName(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.AsetNameIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.AsetNameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.AsetNameIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.AsetNameIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$AsetID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.AsetIDIndex);
    }

    public void realmSet$AsetID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.AsetIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.AsetIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.AsetIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.AsetIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$MerkID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.MerkIDIndex);
    }

    public void realmSet$MerkID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.MerkIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.MerkIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$TypeID() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.TypeIDIndex);
    }

    public void realmSet$TypeID(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.TypeIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.TypeIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Remark() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RemarkIndex);
    }

    public void realmSet$Remark(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.RemarkIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.RemarkIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.RemarkIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.RemarkIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public String realmGet$NoSeri() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NoSeriIndex);
    }

    public void realmSet$NoSeri(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NoSeriIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NoSeriIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NoSeriIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NoSeriIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("AssetMutasiModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("AssetMutasiModel");
            realmObjectSchema.add(new Property("AsetName", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("AsetID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("MerkID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("TypeID", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("Remark", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("NoSeri", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("AssetMutasiModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_AssetMutasiModel")) {
            Table table = sharedRealm.getTable("class_AssetMutasiModel");
            table.addColumn(RealmFieldType.STRING, "AsetName", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "AsetID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "MerkID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "TypeID", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Remark", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "NoSeri", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_AssetMutasiModel");
    }

    public static AssetMutasiModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_AssetMutasiModel")) {
            Table table = sharedRealm.getTable("class_AssetMutasiModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 8) {
                if (columnCount < 8) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 8 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 8 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 8 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final AssetMutasiModelColumnInfo columnInfo = new AssetMutasiModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("AsetName")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'AsetName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("AsetName") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'AsetName' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.AsetNameIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'AsetName' is required. Either set @Required to field 'AsetName' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("AsetID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'AsetID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("AsetID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'AsetID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.AsetIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'AsetID' is required. Either set @Required to field 'AsetID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("MerkID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'MerkID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("MerkID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'MerkID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.MerkIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'MerkID' does support null values in the existing Realm file. Use corresponding boxed type for field 'MerkID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("TypeID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'TypeID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("TypeID") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'TypeID' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.TypeIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'TypeID' does support null values in the existing Realm file. Use corresponding boxed type for field 'TypeID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Remark")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Remark' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Remark") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Remark' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RemarkIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Remark' is required. Either set @Required to field 'Remark' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("NoSeri")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'NoSeri' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("NoSeri") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'NoSeri' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NoSeriIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'NoSeri' is required. Either set @Required to field 'NoSeri' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'AssetMutasiModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_AssetMutasiModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.AssetMutasiModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.AssetMutasiModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.AssetMutasiModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.AssetMutasiModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.AssetMutasiModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.AssetMutasiModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.AssetMutasiModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("AsetName")) {
            if (json.isNull("AsetName")) {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetName(null);
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetName((String) json.getString("AsetName"));
            }
        }
        if (json.has("AsetID")) {
            if (json.isNull("AsetID")) {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetID(null);
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetID((String) json.getString("AsetID"));
            }
        }
        if (json.has("MerkID")) {
            if (json.isNull("MerkID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'MerkID' to null.");
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$MerkID((int) json.getInt("MerkID"));
            }
        }
        if (json.has("TypeID")) {
            if (json.isNull("TypeID")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'TypeID' to null.");
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$TypeID((int) json.getInt("TypeID"));
            }
        }
        if (json.has("Remark")) {
            if (json.isNull("Remark")) {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$Remark(null);
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$Remark((String) json.getString("Remark"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("NoSeri")) {
            if (json.isNull("NoSeri")) {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$NoSeri(null);
            } else {
                ((AssetMutasiModelRealmProxyInterface) obj).realmSet$NoSeri((String) json.getString("NoSeri"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.AssetMutasiModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.AssetMutasiModel obj = new com.bhn.sadix.Data.AssetMutasiModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("AsetName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetName(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetName((String) reader.nextString());
                }
            } else if (name.equals("AsetID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetID(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$AsetID((String) reader.nextString());
                }
            } else if (name.equals("MerkID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'MerkID' to null.");
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$MerkID((int) reader.nextInt());
                }
            } else if (name.equals("TypeID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'TypeID' to null.");
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$TypeID((int) reader.nextInt());
                }
            } else if (name.equals("Remark")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$Remark(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$Remark((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("NoSeri")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$NoSeri(null);
                } else {
                    ((AssetMutasiModelRealmProxyInterface) obj).realmSet$NoSeri((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.AssetMutasiModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.AssetMutasiModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetMutasiModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.AssetMutasiModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((AssetMutasiModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.AssetMutasiModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.AssetMutasiModel copy(Realm realm, com.bhn.sadix.Data.AssetMutasiModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.AssetMutasiModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.AssetMutasiModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.AssetMutasiModel.class, ((AssetMutasiModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$AsetName(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$AsetName());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$AsetID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$AsetID());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$MerkID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$MerkID());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$TypeID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$TypeID());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$Remark(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$Remark());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$CommonID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$NoSeri(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$NoSeri());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.AssetMutasiModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetMutasiModelColumnInfo columnInfo = (AssetMutasiModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((AssetMutasiModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$AsetName = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetName();
        if (realmGet$AsetName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, realmGet$AsetName, false);
        }
        String realmGet$AsetID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetID();
        if (realmGet$AsetID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.MerkIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$MerkID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.TypeIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$TypeID(), false);
        String realmGet$Remark = ((AssetMutasiModelRealmProxyInterface)object).realmGet$Remark();
        if (realmGet$Remark != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RemarkIndex, rowIndex, realmGet$Remark, false);
        }
        String realmGet$CommonID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$NoSeri = ((AssetMutasiModelRealmProxyInterface)object).realmGet$NoSeri();
        if (realmGet$NoSeri != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, realmGet$NoSeri, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetMutasiModelColumnInfo columnInfo = (AssetMutasiModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.AssetMutasiModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetMutasiModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((AssetMutasiModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$AsetName = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetName();
                if (realmGet$AsetName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, realmGet$AsetName, false);
                }
                String realmGet$AsetID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetID();
                if (realmGet$AsetID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.MerkIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$MerkID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.TypeIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$TypeID(), false);
                String realmGet$Remark = ((AssetMutasiModelRealmProxyInterface)object).realmGet$Remark();
                if (realmGet$Remark != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RemarkIndex, rowIndex, realmGet$Remark, false);
                }
                String realmGet$CommonID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$NoSeri = ((AssetMutasiModelRealmProxyInterface)object).realmGet$NoSeri();
                if (realmGet$NoSeri != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, realmGet$NoSeri, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.AssetMutasiModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetMutasiModelColumnInfo columnInfo = (AssetMutasiModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((AssetMutasiModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$AsetName = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetName();
        if (realmGet$AsetName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, realmGet$AsetName, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, false);
        }
        String realmGet$AsetID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetID();
        if (realmGet$AsetID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.MerkIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$MerkID(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.TypeIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$TypeID(), false);
        String realmGet$Remark = ((AssetMutasiModelRealmProxyInterface)object).realmGet$Remark();
        if (realmGet$Remark != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RemarkIndex, rowIndex, realmGet$Remark, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.RemarkIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$NoSeri = ((AssetMutasiModelRealmProxyInterface)object).realmGet$NoSeri();
        if (realmGet$NoSeri != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, realmGet$NoSeri, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.AssetMutasiModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        AssetMutasiModelColumnInfo columnInfo = (AssetMutasiModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.AssetMutasiModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.AssetMutasiModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.AssetMutasiModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((AssetMutasiModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$AsetName = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetName();
                if (realmGet$AsetName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, realmGet$AsetName, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.AsetNameIndex, rowIndex, false);
                }
                String realmGet$AsetID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$AsetID();
                if (realmGet$AsetID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, realmGet$AsetID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.AsetIDIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.MerkIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$MerkID(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.TypeIDIndex, rowIndex, ((AssetMutasiModelRealmProxyInterface)object).realmGet$TypeID(), false);
                String realmGet$Remark = ((AssetMutasiModelRealmProxyInterface)object).realmGet$Remark();
                if (realmGet$Remark != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RemarkIndex, rowIndex, realmGet$Remark, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.RemarkIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((AssetMutasiModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$NoSeri = ((AssetMutasiModelRealmProxyInterface)object).realmGet$NoSeri();
                if (realmGet$NoSeri != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, realmGet$NoSeri, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NoSeriIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.AssetMutasiModel createDetachedCopy(com.bhn.sadix.Data.AssetMutasiModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.AssetMutasiModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.AssetMutasiModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.AssetMutasiModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.AssetMutasiModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$AsetName(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$AsetName());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$AsetID(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$AsetID());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$MerkID(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$MerkID());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$TypeID(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$TypeID());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$Remark(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$Remark());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$RandomID());
        ((AssetMutasiModelRealmProxyInterface) unmanagedObject).realmSet$NoSeri(((AssetMutasiModelRealmProxyInterface) realmObject).realmGet$NoSeri());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.AssetMutasiModel update(Realm realm, com.bhn.sadix.Data.AssetMutasiModel realmObject, com.bhn.sadix.Data.AssetMutasiModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$AsetName(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$AsetName());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$AsetID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$AsetID());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$MerkID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$MerkID());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$TypeID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$TypeID());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$Remark(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$Remark());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$CommonID(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((AssetMutasiModelRealmProxyInterface) realmObject).realmSet$NoSeri(((AssetMutasiModelRealmProxyInterface) newObject).realmGet$NoSeri());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("AssetMutasiModel = [");
        stringBuilder.append("{AsetName:");
        stringBuilder.append(realmGet$AsetName() != null ? realmGet$AsetName() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{AsetID:");
        stringBuilder.append(realmGet$AsetID() != null ? realmGet$AsetID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{MerkID:");
        stringBuilder.append(realmGet$MerkID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{TypeID:");
        stringBuilder.append(realmGet$TypeID());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Remark:");
        stringBuilder.append(realmGet$Remark() != null ? realmGet$Remark() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{NoSeri:");
        stringBuilder.append(realmGet$NoSeri() != null ? realmGet$NoSeri() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssetMutasiModelRealmProxy aAssetMutasiModel = (AssetMutasiModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aAssetMutasiModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAssetMutasiModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aAssetMutasiModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
