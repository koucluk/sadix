package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PrincipleOrderModelRealmProxy extends com.bhn.sadix.Data.PrincipleOrderModel
    implements RealmObjectProxy, PrincipleOrderModelRealmProxyInterface {

    static final class PrincipleOrderModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long PrinsipleIdIndex;
        public long G_AMOUNTIndex;
        public long D_VALUEIndex;
        public long S_AMOUNTIndex;
        public long total_kIndex;
        public long total_bIndex;
        public long CommonIDIndex;
        public long RandomIDIndex;

        PrincipleOrderModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(8);
            this.PrinsipleIdIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "PrinsipleId");
            indicesMap.put("PrinsipleId", this.PrinsipleIdIndex);
            this.G_AMOUNTIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "G_AMOUNT");
            indicesMap.put("G_AMOUNT", this.G_AMOUNTIndex);
            this.D_VALUEIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "D_VALUE");
            indicesMap.put("D_VALUE", this.D_VALUEIndex);
            this.S_AMOUNTIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "S_AMOUNT");
            indicesMap.put("S_AMOUNT", this.S_AMOUNTIndex);
            this.total_kIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "total_k");
            indicesMap.put("total_k", this.total_kIndex);
            this.total_bIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "total_b");
            indicesMap.put("total_b", this.total_bIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.RandomIDIndex = getValidColumnIndex(path, table, "PrincipleOrderModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final PrincipleOrderModelColumnInfo otherInfo = (PrincipleOrderModelColumnInfo) other;
            this.PrinsipleIdIndex = otherInfo.PrinsipleIdIndex;
            this.G_AMOUNTIndex = otherInfo.G_AMOUNTIndex;
            this.D_VALUEIndex = otherInfo.D_VALUEIndex;
            this.S_AMOUNTIndex = otherInfo.S_AMOUNTIndex;
            this.total_kIndex = otherInfo.total_kIndex;
            this.total_bIndex = otherInfo.total_bIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.RandomIDIndex = otherInfo.RandomIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final PrincipleOrderModelColumnInfo clone() {
            return (PrincipleOrderModelColumnInfo) super.clone();
        }

    }
    private PrincipleOrderModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.PrincipleOrderModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("PrinsipleId");
        fieldNames.add("G_AMOUNT");
        fieldNames.add("D_VALUE");
        fieldNames.add("S_AMOUNT");
        fieldNames.add("total_k");
        fieldNames.add("total_b");
        fieldNames.add("CommonID");
        fieldNames.add("RandomID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    PrincipleOrderModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (PrincipleOrderModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.PrincipleOrderModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public int realmGet$PrinsipleId() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.PrinsipleIdIndex);
    }

    public void realmSet$PrinsipleId(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'PrinsipleId' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public double realmGet$G_AMOUNT() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.G_AMOUNTIndex);
    }

    public void realmSet$G_AMOUNT(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.G_AMOUNTIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.G_AMOUNTIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$D_VALUE() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.D_VALUEIndex);
    }

    public void realmSet$D_VALUE(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.D_VALUEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.D_VALUEIndex, value);
    }

    @SuppressWarnings("cast")
    public double realmGet$S_AMOUNT() {
        proxyState.getRealm$realm().checkIfValid();
        return (double) proxyState.getRow$realm().getDouble(columnInfo.S_AMOUNTIndex);
    }

    public void realmSet$S_AMOUNT(double value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setDouble(columnInfo.S_AMOUNTIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setDouble(columnInfo.S_AMOUNTIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$total_k() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.total_kIndex);
    }

    public void realmSet$total_k(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.total_kIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.total_kIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$total_b() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.total_bIndex);
    }

    public void realmSet$total_b(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.total_bIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.total_bIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.RandomIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.RandomIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.RandomIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.RandomIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("PrincipleOrderModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("PrincipleOrderModel");
            realmObjectSchema.add(new Property("PrinsipleId", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("G_AMOUNT", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("D_VALUE", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("S_AMOUNT", RealmFieldType.DOUBLE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("total_k", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("total_b", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("PrincipleOrderModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_PrincipleOrderModel")) {
            Table table = sharedRealm.getTable("class_PrincipleOrderModel");
            table.addColumn(RealmFieldType.INTEGER, "PrinsipleId", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "G_AMOUNT", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "D_VALUE", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.DOUBLE, "S_AMOUNT", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "total_k", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "total_b", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addSearchIndex(table.getColumnIndex("PrinsipleId"));
            table.setPrimaryKey("PrinsipleId");
            return table;
        }
        return sharedRealm.getTable("class_PrincipleOrderModel");
    }

    public static PrincipleOrderModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_PrincipleOrderModel")) {
            Table table = sharedRealm.getTable("class_PrincipleOrderModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 8) {
                if (columnCount < 8) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 8 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 8 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 8 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final PrincipleOrderModelColumnInfo columnInfo = new PrincipleOrderModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'PrinsipleId' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.PrinsipleIdIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field PrinsipleId");
                }
            }

            if (!columnTypes.containsKey("PrinsipleId")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PrinsipleId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PrinsipleId") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'PrinsipleId' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.PrinsipleIdIndex) && table.findFirstNull(columnInfo.PrinsipleIdIndex) != Table.NO_MATCH) {
                throw new IllegalStateException("Cannot migrate an object with null value in field 'PrinsipleId'. Either maintain the same type for primary key field 'PrinsipleId', or remove the object with null value before migration.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("PrinsipleId"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'PrinsipleId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("G_AMOUNT")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'G_AMOUNT' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("G_AMOUNT") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'G_AMOUNT' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.G_AMOUNTIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'G_AMOUNT' does support null values in the existing Realm file. Use corresponding boxed type for field 'G_AMOUNT' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("D_VALUE")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'D_VALUE' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("D_VALUE") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'D_VALUE' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.D_VALUEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'D_VALUE' does support null values in the existing Realm file. Use corresponding boxed type for field 'D_VALUE' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("S_AMOUNT")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'S_AMOUNT' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("S_AMOUNT") != RealmFieldType.DOUBLE) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'double' for field 'S_AMOUNT' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.S_AMOUNTIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'S_AMOUNT' does support null values in the existing Realm file. Use corresponding boxed type for field 'S_AMOUNT' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("total_k")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'total_k' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("total_k") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'total_k' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.total_kIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'total_k' does support null values in the existing Realm file. Use corresponding boxed type for field 'total_k' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("total_b")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'total_b' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("total_b") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'total_b' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.total_bIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'total_b' does support null values in the existing Realm file. Use corresponding boxed type for field 'total_b' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'RandomID' is required. Either set @Required to field 'RandomID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'PrincipleOrderModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_PrincipleOrderModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.PrincipleOrderModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.PrincipleOrderModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("PrinsipleId")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("PrinsipleId"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.PrincipleOrderModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("PrinsipleId")) {
                if (json.isNull("PrinsipleId")) {
                    obj = (io.realm.PrincipleOrderModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PrincipleOrderModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.PrincipleOrderModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.PrincipleOrderModel.class, json.getInt("PrinsipleId"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'PrinsipleId'.");
            }
        }
        if (json.has("G_AMOUNT")) {
            if (json.isNull("G_AMOUNT")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'G_AMOUNT' to null.");
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$G_AMOUNT((double) json.getDouble("G_AMOUNT"));
            }
        }
        if (json.has("D_VALUE")) {
            if (json.isNull("D_VALUE")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'D_VALUE' to null.");
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$D_VALUE((double) json.getDouble("D_VALUE"));
            }
        }
        if (json.has("S_AMOUNT")) {
            if (json.isNull("S_AMOUNT")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'S_AMOUNT' to null.");
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$S_AMOUNT((double) json.getDouble("S_AMOUNT"));
            }
        }
        if (json.has("total_k")) {
            if (json.isNull("total_k")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total_k' to null.");
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$total_k((int) json.getInt("total_k"));
            }
        }
        if (json.has("total_b")) {
            if (json.isNull("total_b")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'total_b' to null.");
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$total_b((int) json.getInt("total_b"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("RandomID")) {
            if (json.isNull("RandomID")) {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$RandomID(null);
            } else {
                ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$RandomID((String) json.getString("RandomID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.PrincipleOrderModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.PrincipleOrderModel obj = new com.bhn.sadix.Data.PrincipleOrderModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("PrinsipleId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'PrinsipleId' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$PrinsipleId((int) reader.nextInt());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("G_AMOUNT")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'G_AMOUNT' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$G_AMOUNT((double) reader.nextDouble());
                }
            } else if (name.equals("D_VALUE")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'D_VALUE' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$D_VALUE((double) reader.nextDouble());
                }
            } else if (name.equals("S_AMOUNT")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'S_AMOUNT' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$S_AMOUNT((double) reader.nextDouble());
                }
            } else if (name.equals("total_k")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total_k' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$total_k((int) reader.nextInt());
                }
            } else if (name.equals("total_b")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'total_b' to null.");
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$total_b((int) reader.nextInt());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((PrincipleOrderModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'PrinsipleId'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.PrincipleOrderModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.PrincipleOrderModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PrincipleOrderModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.PrincipleOrderModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                long rowIndex = table.findFirstLong(pkColumnIndex, ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId());
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.PrincipleOrderModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.PrincipleOrderModel copy(Realm realm, com.bhn.sadix.Data.PrincipleOrderModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.PrincipleOrderModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.PrincipleOrderModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.PrincipleOrderModel.class, ((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$PrinsipleId(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$G_AMOUNT(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$G_AMOUNT());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$D_VALUE(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$D_VALUE());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$S_AMOUNT(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$S_AMOUNT());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$total_k(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$total_k());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$total_b(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$total_b());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$CommonID(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$RandomID(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$RandomID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.PrincipleOrderModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleOrderModelColumnInfo columnInfo = (PrincipleOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId(), false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        Table.nativeSetDouble(tableNativePtr, columnInfo.G_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$G_AMOUNT(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.D_VALUEIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$D_VALUE(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_kIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_k(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_bIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_b(), false);
        String realmGet$CommonID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        String realmGet$RandomID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleOrderModelColumnInfo columnInfo = (PrincipleOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PrincipleOrderModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PrincipleOrderModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.NO_MATCH;
                Object primaryKeyValue = ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId();
                if (primaryKeyValue != null) {
                    rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId());
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId(), false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                Table.nativeSetDouble(tableNativePtr, columnInfo.G_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$G_AMOUNT(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.D_VALUEIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$D_VALUE(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_kIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_k(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_bIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_b(), false);
                String realmGet$CommonID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                String realmGet$RandomID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.PrincipleOrderModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleOrderModelColumnInfo columnInfo = (PrincipleOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId());
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId(), false);
        }
        cache.put(object, rowIndex);
        Table.nativeSetDouble(tableNativePtr, columnInfo.G_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$G_AMOUNT(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.D_VALUEIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$D_VALUE(), false);
        Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_kIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_k(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.total_bIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_b(), false);
        String realmGet$CommonID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        String realmGet$RandomID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$RandomID();
        if (realmGet$RandomID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        PrincipleOrderModelColumnInfo columnInfo = (PrincipleOrderModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.PrincipleOrderModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.PrincipleOrderModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.PrincipleOrderModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.NO_MATCH;
                Object primaryKeyValue = ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId();
                if (primaryKeyValue != null) {
                    rowIndex = Table.nativeFindFirstInt(tableNativePtr, pkColumnIndex, ((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId());
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(((PrincipleOrderModelRealmProxyInterface) object).realmGet$PrinsipleId(), false);
                }
                cache.put(object, rowIndex);
                Table.nativeSetDouble(tableNativePtr, columnInfo.G_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$G_AMOUNT(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.D_VALUEIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$D_VALUE(), false);
                Table.nativeSetDouble(tableNativePtr, columnInfo.S_AMOUNTIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$S_AMOUNT(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_kIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_k(), false);
                Table.nativeSetLong(tableNativePtr, columnInfo.total_bIndex, rowIndex, ((PrincipleOrderModelRealmProxyInterface)object).realmGet$total_b(), false);
                String realmGet$CommonID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                String realmGet$RandomID = ((PrincipleOrderModelRealmProxyInterface)object).realmGet$RandomID();
                if (realmGet$RandomID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, realmGet$RandomID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.RandomIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.PrincipleOrderModel createDetachedCopy(com.bhn.sadix.Data.PrincipleOrderModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.PrincipleOrderModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.PrincipleOrderModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.PrincipleOrderModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.PrincipleOrderModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$PrinsipleId(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$PrinsipleId());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$G_AMOUNT(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$G_AMOUNT());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$D_VALUE(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$D_VALUE());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$S_AMOUNT(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$S_AMOUNT());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$total_k(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$total_k());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$total_b(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$total_b());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((PrincipleOrderModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((PrincipleOrderModelRealmProxyInterface) realmObject).realmGet$RandomID());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.PrincipleOrderModel update(Realm realm, com.bhn.sadix.Data.PrincipleOrderModel realmObject, com.bhn.sadix.Data.PrincipleOrderModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$G_AMOUNT(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$G_AMOUNT());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$D_VALUE(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$D_VALUE());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$S_AMOUNT(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$S_AMOUNT());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$total_k(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$total_k());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$total_b(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$total_b());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$CommonID(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((PrincipleOrderModelRealmProxyInterface) realmObject).realmSet$RandomID(((PrincipleOrderModelRealmProxyInterface) newObject).realmGet$RandomID());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("PrincipleOrderModel = [");
        stringBuilder.append("{PrinsipleId:");
        stringBuilder.append(realmGet$PrinsipleId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{G_AMOUNT:");
        stringBuilder.append(realmGet$G_AMOUNT());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{D_VALUE:");
        stringBuilder.append(realmGet$D_VALUE());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{S_AMOUNT:");
        stringBuilder.append(realmGet$S_AMOUNT());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{total_k:");
        stringBuilder.append(realmGet$total_k());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{total_b:");
        stringBuilder.append(realmGet$total_b());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrincipleOrderModelRealmProxy aPrincipleOrderModel = (PrincipleOrderModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aPrincipleOrderModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aPrincipleOrderModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aPrincipleOrderModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
