package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MerchandiserModelRealmProxy extends com.bhn.sadix.Data.MerchandiserModel
    implements RealmObjectProxy, MerchandiserModelRealmProxyInterface {

    static final class MerchandiserModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long IDXIndex;
        public long NAMEIndex;
        public long TYPEIndex;
        public long CommonIDIndex;

        MerchandiserModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(4);
            this.IDXIndex = getValidColumnIndex(path, table, "MerchandiserModel", "IDX");
            indicesMap.put("IDX", this.IDXIndex);
            this.NAMEIndex = getValidColumnIndex(path, table, "MerchandiserModel", "NAME");
            indicesMap.put("NAME", this.NAMEIndex);
            this.TYPEIndex = getValidColumnIndex(path, table, "MerchandiserModel", "TYPE");
            indicesMap.put("TYPE", this.TYPEIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "MerchandiserModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final MerchandiserModelColumnInfo otherInfo = (MerchandiserModelColumnInfo) other;
            this.IDXIndex = otherInfo.IDXIndex;
            this.NAMEIndex = otherInfo.NAMEIndex;
            this.TYPEIndex = otherInfo.TYPEIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final MerchandiserModelColumnInfo clone() {
            return (MerchandiserModelColumnInfo) super.clone();
        }

    }
    private MerchandiserModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.MerchandiserModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("IDX");
        fieldNames.add("NAME");
        fieldNames.add("TYPE");
        fieldNames.add("CommonID");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    MerchandiserModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (MerchandiserModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.MerchandiserModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$IDX() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.IDXIndex);
    }

    public void realmSet$IDX(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.IDXIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.IDXIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.IDXIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.IDXIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$NAME() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NAMEIndex);
    }

    public void realmSet$NAME(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NAMEIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NAMEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NAMEIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NAMEIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$TYPE() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.TYPEIndex);
    }

    public void realmSet$TYPE(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.TYPEIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.TYPEIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.TYPEIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.TYPEIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("MerchandiserModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("MerchandiserModel");
            realmObjectSchema.add(new Property("IDX", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("NAME", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("TYPE", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("MerchandiserModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_MerchandiserModel")) {
            Table table = sharedRealm.getTable("class_MerchandiserModel");
            table.addColumn(RealmFieldType.STRING, "IDX", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "NAME", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "TYPE", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_MerchandiserModel");
    }

    public static MerchandiserModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_MerchandiserModel")) {
            Table table = sharedRealm.getTable("class_MerchandiserModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 4) {
                if (columnCount < 4) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 4 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 4 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 4 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final MerchandiserModelColumnInfo columnInfo = new MerchandiserModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("IDX")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'IDX' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("IDX") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'IDX' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.IDXIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'IDX' is required. Either set @Required to field 'IDX' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("NAME")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'NAME' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("NAME") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'NAME' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NAMEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'NAME' is required. Either set @Required to field 'NAME' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("TYPE")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'TYPE' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("TYPE") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'TYPE' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.TYPEIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'TYPE' is required. Either set @Required to field 'TYPE' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'MerchandiserModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_MerchandiserModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.MerchandiserModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.MerchandiserModel obj = realm.createObjectInternal(com.bhn.sadix.Data.MerchandiserModel.class, true, excludeFields);
        if (json.has("IDX")) {
            if (json.isNull("IDX")) {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$IDX(null);
            } else {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$IDX((String) json.getString("IDX"));
            }
        }
        if (json.has("NAME")) {
            if (json.isNull("NAME")) {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$NAME(null);
            } else {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$NAME((String) json.getString("NAME"));
            }
        }
        if (json.has("TYPE")) {
            if (json.isNull("TYPE")) {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$TYPE(null);
            } else {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$TYPE((String) json.getString("TYPE"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((MerchandiserModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.MerchandiserModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.MerchandiserModel obj = new com.bhn.sadix.Data.MerchandiserModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("IDX")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$IDX(null);
                } else {
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$IDX((String) reader.nextString());
                }
            } else if (name.equals("NAME")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$NAME(null);
                } else {
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$NAME((String) reader.nextString());
                }
            } else if (name.equals("TYPE")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$TYPE(null);
                } else {
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$TYPE((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((MerchandiserModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.MerchandiserModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.MerchandiserModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.MerchandiserModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.MerchandiserModel copy(Realm realm, com.bhn.sadix.Data.MerchandiserModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.MerchandiserModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.MerchandiserModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.MerchandiserModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((MerchandiserModelRealmProxyInterface) realmObject).realmSet$IDX(((MerchandiserModelRealmProxyInterface) newObject).realmGet$IDX());
            ((MerchandiserModelRealmProxyInterface) realmObject).realmSet$NAME(((MerchandiserModelRealmProxyInterface) newObject).realmGet$NAME());
            ((MerchandiserModelRealmProxyInterface) realmObject).realmSet$TYPE(((MerchandiserModelRealmProxyInterface) newObject).realmGet$TYPE());
            ((MerchandiserModelRealmProxyInterface) realmObject).realmSet$CommonID(((MerchandiserModelRealmProxyInterface) newObject).realmGet$CommonID());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.MerchandiserModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.MerchandiserModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        MerchandiserModelColumnInfo columnInfo = (MerchandiserModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.MerchandiserModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$IDX = ((MerchandiserModelRealmProxyInterface)object).realmGet$IDX();
        if (realmGet$IDX != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.IDXIndex, rowIndex, realmGet$IDX, false);
        }
        String realmGet$NAME = ((MerchandiserModelRealmProxyInterface)object).realmGet$NAME();
        if (realmGet$NAME != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NAMEIndex, rowIndex, realmGet$NAME, false);
        }
        String realmGet$TYPE = ((MerchandiserModelRealmProxyInterface)object).realmGet$TYPE();
        if (realmGet$TYPE != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.TYPEIndex, rowIndex, realmGet$TYPE, false);
        }
        String realmGet$CommonID = ((MerchandiserModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.MerchandiserModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        MerchandiserModelColumnInfo columnInfo = (MerchandiserModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.MerchandiserModel.class);
        com.bhn.sadix.Data.MerchandiserModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.MerchandiserModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$IDX = ((MerchandiserModelRealmProxyInterface)object).realmGet$IDX();
                if (realmGet$IDX != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.IDXIndex, rowIndex, realmGet$IDX, false);
                }
                String realmGet$NAME = ((MerchandiserModelRealmProxyInterface)object).realmGet$NAME();
                if (realmGet$NAME != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NAMEIndex, rowIndex, realmGet$NAME, false);
                }
                String realmGet$TYPE = ((MerchandiserModelRealmProxyInterface)object).realmGet$TYPE();
                if (realmGet$TYPE != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.TYPEIndex, rowIndex, realmGet$TYPE, false);
                }
                String realmGet$CommonID = ((MerchandiserModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.MerchandiserModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.MerchandiserModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        MerchandiserModelColumnInfo columnInfo = (MerchandiserModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.MerchandiserModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$IDX = ((MerchandiserModelRealmProxyInterface)object).realmGet$IDX();
        if (realmGet$IDX != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.IDXIndex, rowIndex, realmGet$IDX, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.IDXIndex, rowIndex, false);
        }
        String realmGet$NAME = ((MerchandiserModelRealmProxyInterface)object).realmGet$NAME();
        if (realmGet$NAME != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NAMEIndex, rowIndex, realmGet$NAME, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NAMEIndex, rowIndex, false);
        }
        String realmGet$TYPE = ((MerchandiserModelRealmProxyInterface)object).realmGet$TYPE();
        if (realmGet$TYPE != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.TYPEIndex, rowIndex, realmGet$TYPE, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.TYPEIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((MerchandiserModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.MerchandiserModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        MerchandiserModelColumnInfo columnInfo = (MerchandiserModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.MerchandiserModel.class);
        com.bhn.sadix.Data.MerchandiserModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.MerchandiserModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$IDX = ((MerchandiserModelRealmProxyInterface)object).realmGet$IDX();
                if (realmGet$IDX != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.IDXIndex, rowIndex, realmGet$IDX, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.IDXIndex, rowIndex, false);
                }
                String realmGet$NAME = ((MerchandiserModelRealmProxyInterface)object).realmGet$NAME();
                if (realmGet$NAME != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NAMEIndex, rowIndex, realmGet$NAME, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NAMEIndex, rowIndex, false);
                }
                String realmGet$TYPE = ((MerchandiserModelRealmProxyInterface)object).realmGet$TYPE();
                if (realmGet$TYPE != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.TYPEIndex, rowIndex, realmGet$TYPE, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.TYPEIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((MerchandiserModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.MerchandiserModel createDetachedCopy(com.bhn.sadix.Data.MerchandiserModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.MerchandiserModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.MerchandiserModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.MerchandiserModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.MerchandiserModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((MerchandiserModelRealmProxyInterface) unmanagedObject).realmSet$IDX(((MerchandiserModelRealmProxyInterface) realmObject).realmGet$IDX());
        ((MerchandiserModelRealmProxyInterface) unmanagedObject).realmSet$NAME(((MerchandiserModelRealmProxyInterface) realmObject).realmGet$NAME());
        ((MerchandiserModelRealmProxyInterface) unmanagedObject).realmSet$TYPE(((MerchandiserModelRealmProxyInterface) realmObject).realmGet$TYPE());
        ((MerchandiserModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((MerchandiserModelRealmProxyInterface) realmObject).realmGet$CommonID());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("MerchandiserModel = [");
        stringBuilder.append("{IDX:");
        stringBuilder.append(realmGet$IDX() != null ? realmGet$IDX() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{NAME:");
        stringBuilder.append(realmGet$NAME() != null ? realmGet$NAME() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{TYPE:");
        stringBuilder.append(realmGet$TYPE() != null ? realmGet$TYPE() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MerchandiserModelRealmProxy aMerchandiserModel = (MerchandiserModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aMerchandiserModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aMerchandiserModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aMerchandiserModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
