package io.realm;


public interface StockOutletModelRealmProxyInterface {
    public String realmGet$test();
    public void realmSet$test(String value);
}
