package io.realm;


public interface CollectionModelRealmProxyInterface {
    public String realmGet$CustId();
    public void realmSet$CustId(String value);
    public String realmGet$status();
    public void realmSet$status(String value);
    public String realmGet$data1();
    public void realmSet$data1(String value);
    public String realmGet$data2();
    public void realmSet$data2(String value);
    public String realmGet$data3();
    public void realmSet$data3(String value);
    public String realmGet$data4();
    public void realmSet$data4(String value);
    public String realmGet$data5();
    public void realmSet$data5(String value);
    public String realmGet$data6();
    public void realmSet$data6(String value);
    public String realmGet$tanggal();
    public void realmSet$tanggal(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
    public String realmGet$payMethod();
    public void realmSet$payMethod(String value);
}
