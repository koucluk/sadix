package io.realm;


public interface PrincipleOrderModelRealmProxyInterface {
    public int realmGet$PrinsipleId();
    public void realmSet$PrinsipleId(int value);
    public double realmGet$G_AMOUNT();
    public void realmSet$G_AMOUNT(double value);
    public double realmGet$D_VALUE();
    public void realmSet$D_VALUE(double value);
    public double realmGet$S_AMOUNT();
    public void realmSet$S_AMOUNT(double value);
    public int realmGet$total_k();
    public void realmSet$total_k(int value);
    public int realmGet$total_b();
    public void realmSet$total_b(int value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
