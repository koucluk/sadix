package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomerBrandingModelRealmProxy extends com.bhn.sadix.Data.CustomerBrandingModel
    implements RealmObjectProxy, CustomerBrandingModelRealmProxyInterface {

    static final class CustomerBrandingModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long RandomIDIndex;
        public long SalesIDIndex;
        public long CustomerIDIndex;
        public long BrandingIDIndex;
        public long PhotoPicIndex;
        public long GeoLongIndex;
        public long GeoLatIndex;
        public long DescriptionIndex;
        public long NmCustIndex;
        public long NmBrandingIndex;
        public long CommonIDIndex;
        public long DoneIndex;

        CustomerBrandingModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(12);
            this.RandomIDIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "RandomID");
            indicesMap.put("RandomID", this.RandomIDIndex);
            this.SalesIDIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "SalesID");
            indicesMap.put("SalesID", this.SalesIDIndex);
            this.CustomerIDIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "CustomerID");
            indicesMap.put("CustomerID", this.CustomerIDIndex);
            this.BrandingIDIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "BrandingID");
            indicesMap.put("BrandingID", this.BrandingIDIndex);
            this.PhotoPicIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "PhotoPic");
            indicesMap.put("PhotoPic", this.PhotoPicIndex);
            this.GeoLongIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "GeoLong");
            indicesMap.put("GeoLong", this.GeoLongIndex);
            this.GeoLatIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "GeoLat");
            indicesMap.put("GeoLat", this.GeoLatIndex);
            this.DescriptionIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "Description");
            indicesMap.put("Description", this.DescriptionIndex);
            this.NmCustIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "NmCust");
            indicesMap.put("NmCust", this.NmCustIndex);
            this.NmBrandingIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "NmBranding");
            indicesMap.put("NmBranding", this.NmBrandingIndex);
            this.CommonIDIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "CommonID");
            indicesMap.put("CommonID", this.CommonIDIndex);
            this.DoneIndex = getValidColumnIndex(path, table, "CustomerBrandingModel", "Done");
            indicesMap.put("Done", this.DoneIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final CustomerBrandingModelColumnInfo otherInfo = (CustomerBrandingModelColumnInfo) other;
            this.RandomIDIndex = otherInfo.RandomIDIndex;
            this.SalesIDIndex = otherInfo.SalesIDIndex;
            this.CustomerIDIndex = otherInfo.CustomerIDIndex;
            this.BrandingIDIndex = otherInfo.BrandingIDIndex;
            this.PhotoPicIndex = otherInfo.PhotoPicIndex;
            this.GeoLongIndex = otherInfo.GeoLongIndex;
            this.GeoLatIndex = otherInfo.GeoLatIndex;
            this.DescriptionIndex = otherInfo.DescriptionIndex;
            this.NmCustIndex = otherInfo.NmCustIndex;
            this.NmBrandingIndex = otherInfo.NmBrandingIndex;
            this.CommonIDIndex = otherInfo.CommonIDIndex;
            this.DoneIndex = otherInfo.DoneIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final CustomerBrandingModelColumnInfo clone() {
            return (CustomerBrandingModelColumnInfo) super.clone();
        }

    }
    private CustomerBrandingModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.CustomerBrandingModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("RandomID");
        fieldNames.add("SalesID");
        fieldNames.add("CustomerID");
        fieldNames.add("BrandingID");
        fieldNames.add("PhotoPic");
        fieldNames.add("GeoLong");
        fieldNames.add("GeoLat");
        fieldNames.add("Description");
        fieldNames.add("NmCust");
        fieldNames.add("NmBranding");
        fieldNames.add("CommonID");
        fieldNames.add("Done");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    CustomerBrandingModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CustomerBrandingModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.CustomerBrandingModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$RandomID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.RandomIDIndex);
    }

    public void realmSet$RandomID(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'RandomID' cannot be changed after object was created.");
    }

    @SuppressWarnings("cast")
    public String realmGet$SalesID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.SalesIDIndex);
    }

    public void realmSet$SalesID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.SalesIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.SalesIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.SalesIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.SalesIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CustomerID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CustomerIDIndex);
    }

    public void realmSet$CustomerID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CustomerIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CustomerIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CustomerIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CustomerIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$BrandingID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.BrandingIDIndex);
    }

    public void realmSet$BrandingID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.BrandingIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.BrandingIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.BrandingIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.BrandingIDIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$PhotoPic() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.PhotoPicIndex);
    }

    public void realmSet$PhotoPic(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.PhotoPicIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.PhotoPicIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.PhotoPicIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.PhotoPicIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLong() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLongIndex);
    }

    public void realmSet$GeoLong(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLongIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLongIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLongIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLongIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$GeoLat() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.GeoLatIndex);
    }

    public void realmSet$GeoLat(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.GeoLatIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.GeoLatIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.GeoLatIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.GeoLatIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$Description() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.DescriptionIndex);
    }

    public void realmSet$Description(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.DescriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.DescriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.DescriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.DescriptionIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$NmCust() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NmCustIndex);
    }

    public void realmSet$NmCust(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NmCustIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NmCustIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NmCustIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NmCustIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$NmBranding() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.NmBrandingIndex);
    }

    public void realmSet$NmBranding(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.NmBrandingIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.NmBrandingIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.NmBrandingIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.NmBrandingIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$CommonID() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.CommonIDIndex);
    }

    public void realmSet$CommonID(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.CommonIDIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.CommonIDIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.CommonIDIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.CommonIDIndex, value);
    }

    @SuppressWarnings("cast")
    public int realmGet$Done() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.DoneIndex);
    }

    public void realmSet$Done(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.DoneIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.DoneIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("CustomerBrandingModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("CustomerBrandingModel");
            realmObjectSchema.add(new Property("RandomID", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("SalesID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CustomerID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("BrandingID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("PhotoPic", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLong", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("GeoLat", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Description", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("NmCust", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("NmBranding", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("CommonID", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            realmObjectSchema.add(new Property("Done", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("CustomerBrandingModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_CustomerBrandingModel")) {
            Table table = sharedRealm.getTable("class_CustomerBrandingModel");
            table.addColumn(RealmFieldType.STRING, "RandomID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "SalesID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CustomerID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "BrandingID", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "PhotoPic", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLong", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "GeoLat", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "Description", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "NmCust", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "NmBranding", Table.NULLABLE);
            table.addColumn(RealmFieldType.STRING, "CommonID", Table.NULLABLE);
            table.addColumn(RealmFieldType.INTEGER, "Done", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("RandomID"));
            table.setPrimaryKey("RandomID");
            return table;
        }
        return sharedRealm.getTable("class_CustomerBrandingModel");
    }

    public static CustomerBrandingModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_CustomerBrandingModel")) {
            Table table = sharedRealm.getTable("class_CustomerBrandingModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 12) {
                if (columnCount < 12) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 12 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 12 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 12 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final CustomerBrandingModelColumnInfo columnInfo = new CustomerBrandingModelColumnInfo(sharedRealm.getPath(), table);

            if (!table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary key not defined for field 'RandomID' in existing Realm file. @PrimaryKey was added.");
            } else {
                if (table.getPrimaryKey() != columnInfo.RandomIDIndex) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key annotation definition was changed, from field " + table.getColumnName(table.getPrimaryKey()) + " to field RandomID");
                }
            }

            if (!columnTypes.containsKey("RandomID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'RandomID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("RandomID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'RandomID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.RandomIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(),"@PrimaryKey field 'RandomID' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("RandomID"))) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Index not defined for field 'RandomID' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("SalesID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'SalesID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("SalesID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'SalesID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.SalesIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'SalesID' is required. Either set @Required to field 'SalesID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CustomerID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CustomerID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CustomerID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CustomerID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CustomerIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CustomerID' is required. Either set @Required to field 'CustomerID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("BrandingID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'BrandingID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("BrandingID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'BrandingID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.BrandingIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'BrandingID' is required. Either set @Required to field 'BrandingID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("PhotoPic")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'PhotoPic' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("PhotoPic") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'PhotoPic' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.PhotoPicIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'PhotoPic' is required. Either set @Required to field 'PhotoPic' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLong")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLong' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLong") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLong' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLongIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLong' is required. Either set @Required to field 'GeoLong' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("GeoLat")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'GeoLat' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("GeoLat") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'GeoLat' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.GeoLatIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'GeoLat' is required. Either set @Required to field 'GeoLat' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Description")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Description' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Description") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'Description' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.DescriptionIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Description' is required. Either set @Required to field 'Description' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("NmCust")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'NmCust' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("NmCust") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'NmCust' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NmCustIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'NmCust' is required. Either set @Required to field 'NmCust' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("NmBranding")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'NmBranding' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("NmBranding") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'NmBranding' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.NmBrandingIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'NmBranding' is required. Either set @Required to field 'NmBranding' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("CommonID")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'CommonID' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("CommonID") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'CommonID' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.CommonIDIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'CommonID' is required. Either set @Required to field 'CommonID' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("Done")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'Done' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("Done") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'Done' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.DoneIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'Done' does support null values in the existing Realm file. Use corresponding boxed type for field 'Done' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'CustomerBrandingModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_CustomerBrandingModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.CustomerBrandingModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.CustomerBrandingModel obj = null;
        if (update) {
            Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("RandomID")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("RandomID"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.CustomerBrandingModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("RandomID")) {
                if (json.isNull("RandomID")) {
                    obj = (io.realm.CustomerBrandingModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CustomerBrandingModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.CustomerBrandingModelRealmProxy) realm.createObjectInternal(com.bhn.sadix.Data.CustomerBrandingModel.class, json.getString("RandomID"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
            }
        }
        if (json.has("SalesID")) {
            if (json.isNull("SalesID")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$SalesID(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$SalesID((String) json.getString("SalesID"));
            }
        }
        if (json.has("CustomerID")) {
            if (json.isNull("CustomerID")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CustomerID(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CustomerID((String) json.getString("CustomerID"));
            }
        }
        if (json.has("BrandingID")) {
            if (json.isNull("BrandingID")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$BrandingID(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$BrandingID((String) json.getString("BrandingID"));
            }
        }
        if (json.has("PhotoPic")) {
            if (json.isNull("PhotoPic")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$PhotoPic(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$PhotoPic((String) json.getString("PhotoPic"));
            }
        }
        if (json.has("GeoLong")) {
            if (json.isNull("GeoLong")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLong(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLong((String) json.getString("GeoLong"));
            }
        }
        if (json.has("GeoLat")) {
            if (json.isNull("GeoLat")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLat(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLat((String) json.getString("GeoLat"));
            }
        }
        if (json.has("Description")) {
            if (json.isNull("Description")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Description(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Description((String) json.getString("Description"));
            }
        }
        if (json.has("NmCust")) {
            if (json.isNull("NmCust")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmCust(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmCust((String) json.getString("NmCust"));
            }
        }
        if (json.has("NmBranding")) {
            if (json.isNull("NmBranding")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmBranding(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmBranding((String) json.getString("NmBranding"));
            }
        }
        if (json.has("CommonID")) {
            if (json.isNull("CommonID")) {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CommonID(null);
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CommonID((String) json.getString("CommonID"));
            }
        }
        if (json.has("Done")) {
            if (json.isNull("Done")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'Done' to null.");
            } else {
                ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Done((int) json.getInt("Done"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.CustomerBrandingModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        com.bhn.sadix.Data.CustomerBrandingModel obj = new com.bhn.sadix.Data.CustomerBrandingModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("RandomID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$RandomID(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$RandomID((String) reader.nextString());
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("SalesID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$SalesID(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$SalesID((String) reader.nextString());
                }
            } else if (name.equals("CustomerID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CustomerID(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CustomerID((String) reader.nextString());
                }
            } else if (name.equals("BrandingID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$BrandingID(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$BrandingID((String) reader.nextString());
                }
            } else if (name.equals("PhotoPic")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$PhotoPic(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$PhotoPic((String) reader.nextString());
                }
            } else if (name.equals("GeoLong")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLong(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLong((String) reader.nextString());
                }
            } else if (name.equals("GeoLat")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLat(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$GeoLat((String) reader.nextString());
                }
            } else if (name.equals("Description")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Description(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Description((String) reader.nextString());
                }
            } else if (name.equals("NmCust")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmCust(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmCust((String) reader.nextString());
                }
            } else if (name.equals("NmBranding")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmBranding(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$NmBranding((String) reader.nextString());
                }
            } else if (name.equals("CommonID")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CommonID(null);
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$CommonID((String) reader.nextString());
                }
            } else if (name.equals("Done")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'Done' to null.");
                } else {
                    ((CustomerBrandingModelRealmProxyInterface) obj).realmSet$Done((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'RandomID'.");
        }
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.CustomerBrandingModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.CustomerBrandingModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CustomerBrandingModel) cachedRealmObject;
        } else {
            com.bhn.sadix.Data.CustomerBrandingModel realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
                long pkColumnIndex = table.getPrimaryKey();
                String value = ((CustomerBrandingModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (value == null) {
                    rowIndex = table.findFirstNull(pkColumnIndex);
                } else {
                    rowIndex = table.findFirstString(pkColumnIndex, value);
                }
                if (rowIndex != Table.NO_MATCH) {
                    try {
                        objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class), false, Collections.<String> emptyList());
                        realmObject = new io.realm.CustomerBrandingModelRealmProxy();
                        cache.put(object, (RealmObjectProxy) realmObject);
                    } finally {
                        objectContext.clear();
                    }
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.bhn.sadix.Data.CustomerBrandingModel copy(Realm realm, com.bhn.sadix.Data.CustomerBrandingModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.CustomerBrandingModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.CustomerBrandingModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.CustomerBrandingModel.class, ((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$RandomID(), false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$SalesID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$SalesID());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$CustomerID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$CustomerID());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$BrandingID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$BrandingID());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$PhotoPic(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$PhotoPic());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$GeoLong(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$GeoLong());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$GeoLat(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$GeoLat());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$Description(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$Description());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$NmCust(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$NmCust());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$NmBranding(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$NmBranding());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$CommonID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$CommonID());
            ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$Done(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$Done());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.CustomerBrandingModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CustomerBrandingModelColumnInfo columnInfo = (CustomerBrandingModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CustomerBrandingModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$SalesID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$SalesID();
        if (realmGet$SalesID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, realmGet$SalesID, false);
        }
        String realmGet$CustomerID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CustomerID();
        if (realmGet$CustomerID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, realmGet$CustomerID, false);
        }
        String realmGet$BrandingID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$BrandingID();
        if (realmGet$BrandingID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, realmGet$BrandingID, false);
        }
        String realmGet$PhotoPic = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$PhotoPic();
        if (realmGet$PhotoPic != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, realmGet$PhotoPic, false);
        }
        String realmGet$GeoLong = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        }
        String realmGet$GeoLat = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        }
        String realmGet$Description = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Description();
        if (realmGet$Description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
        }
        String realmGet$NmCust = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmCust();
        if (realmGet$NmCust != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NmCustIndex, rowIndex, realmGet$NmCust, false);
        }
        String realmGet$NmBranding = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmBranding();
        if (realmGet$NmBranding != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, realmGet$NmBranding, false);
        }
        String realmGet$CommonID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Done(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CustomerBrandingModelColumnInfo columnInfo = (CustomerBrandingModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CustomerBrandingModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CustomerBrandingModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CustomerBrandingModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                } else {
                    Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
                }
                cache.put(object, rowIndex);
                String realmGet$SalesID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$SalesID();
                if (realmGet$SalesID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, realmGet$SalesID, false);
                }
                String realmGet$CustomerID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CustomerID();
                if (realmGet$CustomerID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, realmGet$CustomerID, false);
                }
                String realmGet$BrandingID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$BrandingID();
                if (realmGet$BrandingID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, realmGet$BrandingID, false);
                }
                String realmGet$PhotoPic = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$PhotoPic();
                if (realmGet$PhotoPic != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, realmGet$PhotoPic, false);
                }
                String realmGet$GeoLong = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                }
                String realmGet$GeoLat = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                }
                String realmGet$Description = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Description();
                if (realmGet$Description != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
                }
                String realmGet$NmCust = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmCust();
                if (realmGet$NmCust != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NmCustIndex, rowIndex, realmGet$NmCust, false);
                }
                String realmGet$NmBranding = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmBranding();
                if (realmGet$NmBranding != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, realmGet$NmBranding, false);
                }
                String realmGet$CommonID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Done(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.CustomerBrandingModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CustomerBrandingModelColumnInfo columnInfo = (CustomerBrandingModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        String primaryKeyValue = ((CustomerBrandingModelRealmProxyInterface) object).realmGet$RandomID();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
        }
        cache.put(object, rowIndex);
        String realmGet$SalesID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$SalesID();
        if (realmGet$SalesID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, realmGet$SalesID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, false);
        }
        String realmGet$CustomerID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CustomerID();
        if (realmGet$CustomerID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, realmGet$CustomerID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, false);
        }
        String realmGet$BrandingID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$BrandingID();
        if (realmGet$BrandingID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, realmGet$BrandingID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, false);
        }
        String realmGet$PhotoPic = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$PhotoPic();
        if (realmGet$PhotoPic != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, realmGet$PhotoPic, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, false);
        }
        String realmGet$GeoLong = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLong();
        if (realmGet$GeoLong != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
        }
        String realmGet$GeoLat = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLat();
        if (realmGet$GeoLat != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
        }
        String realmGet$Description = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Description();
        if (realmGet$Description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, false);
        }
        String realmGet$NmCust = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmCust();
        if (realmGet$NmCust != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NmCustIndex, rowIndex, realmGet$NmCust, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NmCustIndex, rowIndex, false);
        }
        String realmGet$NmBranding = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmBranding();
        if (realmGet$NmBranding != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, realmGet$NmBranding, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, false);
        }
        String realmGet$CommonID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CommonID();
        if (realmGet$CommonID != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Done(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        CustomerBrandingModelColumnInfo columnInfo = (CustomerBrandingModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.CustomerBrandingModel.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.bhn.sadix.Data.CustomerBrandingModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.CustomerBrandingModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                String primaryKeyValue = ((CustomerBrandingModelRealmProxyInterface) object).realmGet$RandomID();
                long rowIndex = Table.NO_MATCH;
                if (primaryKeyValue == null) {
                    rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
                } else {
                    rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
                }
                if (rowIndex == Table.NO_MATCH) {
                    rowIndex = table.addEmptyRowWithPrimaryKey(primaryKeyValue, false);
                }
                cache.put(object, rowIndex);
                String realmGet$SalesID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$SalesID();
                if (realmGet$SalesID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, realmGet$SalesID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.SalesIDIndex, rowIndex, false);
                }
                String realmGet$CustomerID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CustomerID();
                if (realmGet$CustomerID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, realmGet$CustomerID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CustomerIDIndex, rowIndex, false);
                }
                String realmGet$BrandingID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$BrandingID();
                if (realmGet$BrandingID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, realmGet$BrandingID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.BrandingIDIndex, rowIndex, false);
                }
                String realmGet$PhotoPic = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$PhotoPic();
                if (realmGet$PhotoPic != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, realmGet$PhotoPic, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.PhotoPicIndex, rowIndex, false);
                }
                String realmGet$GeoLong = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLong();
                if (realmGet$GeoLong != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, realmGet$GeoLong, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLongIndex, rowIndex, false);
                }
                String realmGet$GeoLat = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$GeoLat();
                if (realmGet$GeoLat != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, realmGet$GeoLat, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.GeoLatIndex, rowIndex, false);
                }
                String realmGet$Description = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Description();
                if (realmGet$Description != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, realmGet$Description, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.DescriptionIndex, rowIndex, false);
                }
                String realmGet$NmCust = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmCust();
                if (realmGet$NmCust != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NmCustIndex, rowIndex, realmGet$NmCust, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NmCustIndex, rowIndex, false);
                }
                String realmGet$NmBranding = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$NmBranding();
                if (realmGet$NmBranding != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, realmGet$NmBranding, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.NmBrandingIndex, rowIndex, false);
                }
                String realmGet$CommonID = ((CustomerBrandingModelRealmProxyInterface)object).realmGet$CommonID();
                if (realmGet$CommonID != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, realmGet$CommonID, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.CommonIDIndex, rowIndex, false);
                }
                Table.nativeSetLong(tableNativePtr, columnInfo.DoneIndex, rowIndex, ((CustomerBrandingModelRealmProxyInterface)object).realmGet$Done(), false);
            }
        }
    }

    public static com.bhn.sadix.Data.CustomerBrandingModel createDetachedCopy(com.bhn.sadix.Data.CustomerBrandingModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.CustomerBrandingModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.CustomerBrandingModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.CustomerBrandingModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.CustomerBrandingModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$RandomID(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$RandomID());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$SalesID(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$SalesID());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$CustomerID(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$CustomerID());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$BrandingID(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$BrandingID());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$PhotoPic(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$PhotoPic());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$GeoLong(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$GeoLong());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$GeoLat(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$GeoLat());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$Description(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$Description());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$NmCust(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$NmCust());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$NmBranding(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$NmBranding());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$CommonID(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$CommonID());
        ((CustomerBrandingModelRealmProxyInterface) unmanagedObject).realmSet$Done(((CustomerBrandingModelRealmProxyInterface) realmObject).realmGet$Done());
        return unmanagedObject;
    }

    static com.bhn.sadix.Data.CustomerBrandingModel update(Realm realm, com.bhn.sadix.Data.CustomerBrandingModel realmObject, com.bhn.sadix.Data.CustomerBrandingModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$SalesID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$SalesID());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$CustomerID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$CustomerID());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$BrandingID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$BrandingID());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$PhotoPic(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$PhotoPic());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$GeoLong(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$GeoLong());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$GeoLat(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$GeoLat());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$Description(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$Description());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$NmCust(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$NmCust());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$NmBranding(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$NmBranding());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$CommonID(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$CommonID());
        ((CustomerBrandingModelRealmProxyInterface) realmObject).realmSet$Done(((CustomerBrandingModelRealmProxyInterface) newObject).realmGet$Done());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("CustomerBrandingModel = [");
        stringBuilder.append("{RandomID:");
        stringBuilder.append(realmGet$RandomID() != null ? realmGet$RandomID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{SalesID:");
        stringBuilder.append(realmGet$SalesID() != null ? realmGet$SalesID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CustomerID:");
        stringBuilder.append(realmGet$CustomerID() != null ? realmGet$CustomerID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{BrandingID:");
        stringBuilder.append(realmGet$BrandingID() != null ? realmGet$BrandingID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{PhotoPic:");
        stringBuilder.append(realmGet$PhotoPic() != null ? realmGet$PhotoPic() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLong:");
        stringBuilder.append(realmGet$GeoLong() != null ? realmGet$GeoLong() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{GeoLat:");
        stringBuilder.append(realmGet$GeoLat() != null ? realmGet$GeoLat() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Description:");
        stringBuilder.append(realmGet$Description() != null ? realmGet$Description() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{NmCust:");
        stringBuilder.append(realmGet$NmCust() != null ? realmGet$NmCust() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{NmBranding:");
        stringBuilder.append(realmGet$NmBranding() != null ? realmGet$NmBranding() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{CommonID:");
        stringBuilder.append(realmGet$CommonID() != null ? realmGet$CommonID() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{Done:");
        stringBuilder.append(realmGet$Done());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerBrandingModelRealmProxy aCustomerBrandingModel = (CustomerBrandingModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCustomerBrandingModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCustomerBrandingModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCustomerBrandingModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
