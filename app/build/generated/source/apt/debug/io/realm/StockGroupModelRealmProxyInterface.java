package io.realm;


public interface StockGroupModelRealmProxyInterface {
    public String realmGet$GroupId();
    public void realmSet$GroupId(String value);
    public String realmGet$CustomerId();
    public void realmSet$CustomerId(String value);
    public int realmGet$Quantity();
    public void realmSet$Quantity(int value);
    public int realmGet$Quantity2();
    public void realmSet$Quantity2(int value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
}
