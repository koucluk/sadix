package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StockOutletModelRealmProxy extends com.bhn.sadix.Data.StockOutletModel
    implements RealmObjectProxy, StockOutletModelRealmProxyInterface {

    static final class StockOutletModelColumnInfo extends ColumnInfo
        implements Cloneable {

        public long testIndex;

        StockOutletModelColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(1);
            this.testIndex = getValidColumnIndex(path, table, "StockOutletModel", "test");
            indicesMap.put("test", this.testIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final StockOutletModelColumnInfo otherInfo = (StockOutletModelColumnInfo) other;
            this.testIndex = otherInfo.testIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final StockOutletModelColumnInfo clone() {
            return (StockOutletModelColumnInfo) super.clone();
        }

    }
    private StockOutletModelColumnInfo columnInfo;
    private ProxyState<com.bhn.sadix.Data.StockOutletModel> proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("test");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    StockOutletModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (StockOutletModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.bhn.sadix.Data.StockOutletModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public String realmGet$test() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.testIndex);
    }

    public void realmSet$test(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.testIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.testIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.testIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.testIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("StockOutletModel")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("StockOutletModel");
            realmObjectSchema.add(new Property("test", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("StockOutletModel");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_StockOutletModel")) {
            Table table = sharedRealm.getTable("class_StockOutletModel");
            table.addColumn(RealmFieldType.STRING, "test", Table.NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_StockOutletModel");
    }

    public static StockOutletModelColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_StockOutletModel")) {
            Table table = sharedRealm.getTable("class_StockOutletModel");
            final long columnCount = table.getColumnCount();
            if (columnCount != 1) {
                if (columnCount < 1) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 1 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 1 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 1 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final StockOutletModelColumnInfo columnInfo = new StockOutletModelColumnInfo(sharedRealm.getPath(), table);

            if (table.hasPrimaryKey()) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Primary Key defined for field " + table.getColumnName(table.getPrimaryKey()) + " was removed.");
            }

            if (!columnTypes.containsKey("test")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'test' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("test") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'String' for field 'test' in existing Realm file.");
            }
            if (!table.isColumnNullable(columnInfo.testIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'test' is required. Either set @Required to field 'test' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'StockOutletModel' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_StockOutletModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.bhn.sadix.Data.StockOutletModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.bhn.sadix.Data.StockOutletModel obj = realm.createObjectInternal(com.bhn.sadix.Data.StockOutletModel.class, true, excludeFields);
        if (json.has("test")) {
            if (json.isNull("test")) {
                ((StockOutletModelRealmProxyInterface) obj).realmSet$test(null);
            } else {
                ((StockOutletModelRealmProxyInterface) obj).realmSet$test((String) json.getString("test"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.bhn.sadix.Data.StockOutletModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.bhn.sadix.Data.StockOutletModel obj = new com.bhn.sadix.Data.StockOutletModel();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("test")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((StockOutletModelRealmProxyInterface) obj).realmSet$test(null);
                } else {
                    ((StockOutletModelRealmProxyInterface) obj).realmSet$test((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.bhn.sadix.Data.StockOutletModel copyOrUpdate(Realm realm, com.bhn.sadix.Data.StockOutletModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockOutletModel) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.bhn.sadix.Data.StockOutletModel copy(Realm realm, com.bhn.sadix.Data.StockOutletModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.bhn.sadix.Data.StockOutletModel) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.bhn.sadix.Data.StockOutletModel realmObject = realm.createObjectInternal(com.bhn.sadix.Data.StockOutletModel.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((StockOutletModelRealmProxyInterface) realmObject).realmSet$test(((StockOutletModelRealmProxyInterface) newObject).realmGet$test());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.bhn.sadix.Data.StockOutletModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockOutletModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockOutletModelColumnInfo columnInfo = (StockOutletModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockOutletModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$test = ((StockOutletModelRealmProxyInterface)object).realmGet$test();
        if (realmGet$test != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.testIndex, rowIndex, realmGet$test, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockOutletModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockOutletModelColumnInfo columnInfo = (StockOutletModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockOutletModel.class);
        com.bhn.sadix.Data.StockOutletModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockOutletModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$test = ((StockOutletModelRealmProxyInterface)object).realmGet$test();
                if (realmGet$test != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.testIndex, rowIndex, realmGet$test, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.bhn.sadix.Data.StockOutletModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.bhn.sadix.Data.StockOutletModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockOutletModelColumnInfo columnInfo = (StockOutletModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockOutletModel.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        String realmGet$test = ((StockOutletModelRealmProxyInterface)object).realmGet$test();
        if (realmGet$test != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.testIndex, rowIndex, realmGet$test, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.testIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.bhn.sadix.Data.StockOutletModel.class);
        long tableNativePtr = table.getNativeTablePointer();
        StockOutletModelColumnInfo columnInfo = (StockOutletModelColumnInfo) realm.schema.getColumnInfo(com.bhn.sadix.Data.StockOutletModel.class);
        com.bhn.sadix.Data.StockOutletModel object = null;
        while (objects.hasNext()) {
            object = (com.bhn.sadix.Data.StockOutletModel) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                String realmGet$test = ((StockOutletModelRealmProxyInterface)object).realmGet$test();
                if (realmGet$test != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.testIndex, rowIndex, realmGet$test, false);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.testIndex, rowIndex, false);
                }
            }
        }
    }

    public static com.bhn.sadix.Data.StockOutletModel createDetachedCopy(com.bhn.sadix.Data.StockOutletModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.bhn.sadix.Data.StockOutletModel unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.bhn.sadix.Data.StockOutletModel)cachedObject.object;
            } else {
                unmanagedObject = (com.bhn.sadix.Data.StockOutletModel)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.bhn.sadix.Data.StockOutletModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        }
        ((StockOutletModelRealmProxyInterface) unmanagedObject).realmSet$test(((StockOutletModelRealmProxyInterface) realmObject).realmGet$test());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("StockOutletModel = [");
        stringBuilder.append("{test:");
        stringBuilder.append(realmGet$test() != null ? realmGet$test() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockOutletModelRealmProxy aStockOutletModel = (StockOutletModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aStockOutletModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aStockOutletModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aStockOutletModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
