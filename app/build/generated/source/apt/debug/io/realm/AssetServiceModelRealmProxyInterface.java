package io.realm;


public interface AssetServiceModelRealmProxyInterface {
    public String realmGet$ASETNAME();
    public void realmSet$ASETNAME(String value);
    public String realmGet$ASETTYPE();
    public void realmSet$ASETTYPE(String value);
    public String realmGet$ASETNO();
    public void realmSet$ASETNO(String value);
    public String realmGet$ASETID();
    public void realmSet$ASETID(String value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
}
