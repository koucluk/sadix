package io.realm;


public interface PrincipleModelRealmProxyInterface {
    public String realmGet$PrinsipleId();
    public void realmSet$PrinsipleId(String value);
    public String realmGet$PrinsipleName();
    public void realmSet$PrinsipleName(String value);
    public String realmGet$RandomID();
    public void realmSet$RandomID(String value);
    public int realmGet$total_qty_b();
    public void realmSet$total_qty_b(int value);
    public int realmGet$total_qty_k();
    public void realmSet$total_qty_k(int value);
    public double realmGet$price();
    public void realmSet$price(double value);
    public double realmGet$discount();
    public void realmSet$discount(double value);
    public double realmGet$total_price();
    public void realmSet$total_price(double value);
    public int realmGet$PrinsipleType();
    public void realmSet$PrinsipleType(int value);
    public double realmGet$CreditLimit();
    public void realmSet$CreditLimit(double value);
    public double realmGet$CreditTOP();
    public void realmSet$CreditTOP(double value);
    public String realmGet$CommonID();
    public void realmSet$CommonID(String value);
    public RealmList<com.bhn.sadix.Data.TakingOrderModel> realmGet$list();
    public void realmSet$list(RealmList<com.bhn.sadix.Data.TakingOrderModel> value);
    public RealmList<com.bhn.sadix.Data.DiscountPromoModel> realmGet$listPromo();
    public void realmSet$listPromo(RealmList<com.bhn.sadix.Data.DiscountPromoModel> value);
    public RealmList<com.bhn.sadix.Data.DiscountResultModel> realmGet$listPromoResult();
    public void realmSet$listPromoResult(RealmList<com.bhn.sadix.Data.DiscountResultModel> value);
}
